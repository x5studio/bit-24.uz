<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Для компании Russiadiscovery внерена и доработана CRM Битрикс24. Автоматизирована работа отдела продаж. Разработана система бюджетирования для туристической компании. Система интегрирована с Битрикс24");
$APPLICATION->SetPageProperty("keywords", "CRM Битрикс24, CRM Битрикс 24, Битрикс243, Битрикс24, Битрикс24 для турима, CRM для туризма, СРМ для туризма, црм для туризма,");
$APPLICATION->SetPageProperty("title", "CRM-система Битрикс24 для Рашадискавери");
$APPLICATION->SetTitle("CRM-система Битрикс24 и система бюджетирования для компании Рашадискавери");
$APPLICATION->SetAdditionalCSS("/nashi-klienty/russiadiscovery/style.css");
?>

<div class="page-wrapper page-russia-discovery">
	<section>
		<div class="ico_logo"></div>
		<div class="container">
			<div class="banner-title">CRM-система Битрикс24 и система бюджетирования для компании Рашадискавери</div>
		</div>
		<div class="ico_scroll"></div>
	</section>

	<section class="section-client">
		<div class="container clearfix">
				<div class="col-sm-6 left-pull-sm col-xs-12 clients-desc">
					<span class="clients-title">Заказчик</span>
					<p>Компания ООО «Рашадискавери» существует с 2005 года и на сегодняшний день является одним из лидеров в сфере активного отдыха в России. Компания выступае в качестве  туроператора и агентства, подбирая подходящие туры у других операторов.</p>
					<a href="http://www.russiadiscovery.ru" rel="nofollow">www.russiadiscovery.ru</a>
				</div>
				<div class="col-sm-6 right-pull-sm col-xs-12 visible-img-xs">
					<img src="/nashi-klienty/russiadiscovery/images/clients2.png" alt="">
				</div>
		</div>
	</section>

	<section class="project-tasks">
		<div class="container">
			<div class="clearfix">
				<h2 class="text-left">задачи проекта</h2>
				<div class="clearfix tasks-wrap">
					<div class="col-sm-6 task-wrap">
						<div class="single-task">
							<span class="num">01</span>
							<span class="title">Построение схемы работы <br/>отдела продаж</span>
							<p>Систематизировать входящие запросы с сайта, наладить обрбаотку заказов отделом продаж. Обеспечить прозрачность поступления оплат по сделкам, по срокам их закрытия в зависимости от дат тура.</p>
						</div>
					</div>
					<div class="col-sm-6 task-wrap">
						<div class="single-task">
							<span class="num">02</span>
							<span class="title">Разработка системы бюджетирования и ведения расходов</span>
							<p>Разработать функционал планирования бюджета на год (по месяцам) с учетом статей раходов. Налаживание процессов проверки наличия средств в запланированном бюджете на запрашиваемый расход с возможностью согласовать траты с руководством. Ведение ежедневной системы расходов с возможностью разбирать выписку из банка по статьям.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="main-func">

		<div class="single-func clearfix">
			<div class="container">
				<div class="right-pull-sm col-sm-6 text-right-sm">
					<img src="/nashi-klienty/russiadiscovery/images/screen_func_1.png" alt="">
				</div>
				<div class="left-pull-sm col-sm-6">
					<span class="main-title">Основной функционал</span>
					<span class="title">Настройка CRM</span>
					<p>Настройка типовых и дополнительных полей CRM, налаживание
		взаимосвязей полей в зависимости от выбранных значений в лиде,
		контатке, компании, сделке. Доработка отображения полей масками ввода.
		Доработка системы проверки на дубли в форму просмотра
		и редактирования карточек лидов, контактов, компаний.</p>
				</div>
			</div>
		</div>


		<div class="single-func clearfix grey-bg func-move-data">
			<div class="container">
				<div class="left-pull-sm col-sm-2">
					<img src="/nashi-klienty/russiadiscovery/images/ico_move_data.png" alt="">
				</div>
				<div class="right-pull-sm col-sm-10 func-desc">
					<span class="title">Перенос данных из старой CRM</span>
					<p>Перенести данные о существующих клиентах из AmoCRM. Потребовалось делать обработчик файлов выгрузки из AmoCRM и настраивать скрипт обработки данных для загрузки данных. Множество полей не соответствовало стандартам загрузки в Битрикс24. На момент переезда не было готовых модулей переноса данных.</p>
				</div>
			</div>
		</div>

		<div class="single-func clearfix">
			<div class="container">
				<div class="left-pull-sm col-sm-6">
					<img src="/nashi-klienty/russiadiscovery/images/screen_func_2.png" alt="">
				</div>
				<div class="right-pull-sm col-sm-6">
					<span class="title">Подключение телефонии клиента к CRM</span>
					<p>Настройка интеграции с телефонией Uiscom. Для фиксации всех входящих
	и исходящих звонков в CRM, ведения статистики звонков.<br/><br/>Сбор статистики сколько в среднем звонков приходится на сделку, на менеджера.<br/><br/>Настройка нескольких линий для сбора статистики с рекламных компаний.</p>
				</div>
			</div>
		</div>

		<div class="single-func grey-bg">
			<div class="container">

				<span class="title">Ведение учета взаиморасчетов с Туроператорами по клиентам.<br/>Разработка системы учета счетов от Туроператорам с привязкой к сделкам</span>
				<div class="clearfix">
					<div class="right-pull-sm col-sm-6 text-right-sm">
						<img src="/nashi-klienty/russiadiscovery/images/screen_func_3.png" alt="">
					</div>
					<div class="left-pull-sm col-sm-6">
						<ul>
							<li>Ведение списков счетов, получаемых от Туроператоров, с привязкой к заказчику и сделке, отражение статуса оплаты, напоминания по оплате счетов.</li>
							<li>Настройка системы учета взаиморасчетов с операторами. Ведутся счета для перевода средств по турам. Возможно указать по каким сделкам (счетам для клиентов) идут данные расчеты.</li>
							<li>Так же учитываются возвраты сумм, если кто-то из участников отказался от части услуг или не может участвовать в поездке.</li>
							<li>Настроена сводная таблица поступлений и оплат по сделкам и операторам.</li>
							<li>Ранее данные взаиморасчеты вели в таблице через google docs. Всегда возникали проблемы с сортировкой и фильтрацией данных, если с таблицей одновременно работало несколько сотрудников.</li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="single-func clearfix green-bg">
			<div class="container">
				<div class="left-pull-sm col-sm-6">
					<img src="/nashi-klienty/russiadiscovery/images/screen_func_4.png" alt="">
				</div>
				<div class="right-pull-sm col-sm-6">
					<span class="title">Разработка сложных отчетов по сделкам</span>
					<p>Формирование файлов в разрешении *.xls за разный период со сложной
выборкой по сделкам, счетам с разбивкой по месяцам. Система отчетов учитывает возвраты денег, при отказе клиента от поездки или дополнительных условий. </p>
				</div>
			</div>
		</div>

		<div class="single-func clearfix">
			<div class="container">
				<div class="right-pull-sm col-sm-6 text-right-sm">
					<img src="/nashi-klienty/russiadiscovery/images/setup_template.png" alt="">
				</div>
				<div class="left-pull-sm col-sm-6">
					<span class="title">Настройка шаблонов документов</span>
					<p>Для каждой сделки возможно сформировать пакет документов:
договор для юр. и физ. лиц, приложения к договору, туристическая путевка, заявка на бронирование и подтверждение бронирования.</p>
					<div class="templates_doc_wrap">
						<span class="subtitle">Шаблоны документов заполняются данными по:</span>
						<span>—  Заказчику, </span>
						<span>—  Участникам тура, </span>
						<span>—  Менеджеру, ведущему сделку.</span>
					</div>
				</div>
			</div>
		</div>

		<div class="single-func clearfix grey-bg">
			<div class="container">
				<span class="title">Разработка калькулятора тура / расчета по сделке</span>
				<p class="full-width">Нет необходимости считать вручную, искать размер комиссии туроператора (она автоматически подтягивается из карточки компании), есть возможность указать отдельную стоимость тура для детей, рассчитать скидку клиента. Или вести обратный расчет от стоимости индивидуального тура (полученной от туроператора) к стоимости для клиента на всю группу или на каждого участника в частности.</p>
				<div class="right-pull-sm col-sm-6 text-right-sm">
					<img src="/nashi-klienty/russiadiscovery/images/screen_func_5.png" alt="">
				</div>
				<div class="left-pull-sm col-sm-6">
					<ul>
						<li>Для сделок настроен автоматический расчет в зависимости от типа тура (стандартный, индивидуальный, коллективный), от количества людей, доп.услуг предоставляемых участникам, с учетом комиссии туроператора.</li>
						<li>Считает объем вознаграждения компании, среднюю стоимость тура.</li>
						<li>Средняя стоимость каждый раз пересчитывается и записывается в профиле заказчика.</li>
						<li>В зависимости от выбранного тура автоматически подтягивается информация о туроператоре.</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="single-func clearfix">
			<div class="container">
				<span class="title">Разработка системы бюджетирования на год и системы ведения ежедневных расходов.</span>
				<p class="full-width">Разработка справочника статей расходов. Настройка срока действия статьи в справочнике. Возможность указать ответственного за статью (права доступа к добавлению записей).</p>
				<div class="left-pull-sm col-sm-6">
					<img src="/nashi-klienty/russiadiscovery/images/screen_func_6.png" alt="">
				</div>
				<div class="right-pull-sm col-sm-6">
					<ul>
						<li>Разработка системы хранения бюджета</li>
						<li>Разработка функционала хранения расходов</li>
						<li>Обработчик выписки из банка</li>
						<li>Отчет по расходам. Выгрузка бюджета в excel.</li>
						<li>Связать процессы согласования наличных и оплаты счетов с бюджетом и расходами</li>
						<li>Сводный отчет по приходам и расходам в разрезе туров, туроператоров.</li>
					</ul>
				</div>
			</div>
		</div>

	</section>

	<section class="results-section">
		<div class="container footbtn">
			<h2 class="text-center">Результаты проекта</h2>
			<div class="clearfix">
				<div class="col-sm-4 col-xs-12 single-result-wrap">
					<div class="single-result">
						<p>Уменьшились потери входящих запросов от клиентов, повысилась конверсия</p>
					</div>
				</div>

				<div class="col-sm-4 col-xs-12 single-result-wrap">
					<div class="single-result">
						<p>Стало проще вносить данные по счетам Туропеаторов и контролировать оплату клиентских счетов</p>
					</div>
				</div>

				<div class="col-sm-4 col-xs-12 single-result-wrap">
					<div class="single-result">
						<p>Упростился расчет комиссии и прибыли компании</p>
					</div>
				</div>

			</div>
			<a class="center-block btn" onclick="yaCounter45927957.reachGoal('feedback-click'); gtag('send', 'event', { 'event_category':  'form', 'click'}); return true;" data-form-field-type="Оставить заявку 1">Сделать заказ</a>
		</div>
	</section>

</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<!-- section.main-func>.container>(.left-pull-sm>span.main-title+span.title+p)+(.right-pull-sm>img) -->