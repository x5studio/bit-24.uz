<?php

namespace Bit\AdditionalOptimizer\Image\Optimizer\Optimizers;

use Bit\AdditionalOptimizer\Image\Optimizer\Image;

class Jpegtran extends BaseOptimizer
{
    public $binaryName = 'jpegtran';

    public function canHandle(Image $image): bool
    {
        return $image->mime() === 'image/jpeg';
    }

    public function getCommand(): string
    {
        $optionString = implode(' ', $this->options);

        $OutputFile = '-outfile ' . $this->imagePath;

        foreach ($this->options as $key => $option) {
            
            if(strpos($option, '-outfile') !== false) {

                $OutputFile = $option;
                unset($this->options[$key]);
                
            }

        }

        return "\"{$this->binaryPath}{$this->binaryName}\" {$optionString}"
            .' '. $OutputFile .' '.escapeshellarg($this->imagePath);
    }
}
