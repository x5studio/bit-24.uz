<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>

    <?foreach ($arResult["ITEMS"] as $cell => $arElement):
        ?>
        <?= ($globalCell % 2) ? '<div class="row">' : ''; ?>
            <div class="col-xs-12 col-sm-6 <?= ($globalCell % 2) ? 'info_left' : ''; ?>" >
                <div class="row">
                    <div class="col-xs-12 col-sm-5 info_img">
                        <?if (!empty($arElement["IMAGE"])):?>
                            <img src="<?= $arElement["IMAGE"] ?>" alt="<?= $arElement["NAME"] ?>"/>
                        <?endif;?>
                    </div>
                    <div class="col-xs-12 col-sm-7 info_title">
                        <h2><?=$arElement["NAME"]?></h2>
                        <p><?=$arElement["PREVIEW_TEXT"]?> </p>
                        <a href="<?=$arElement["DETAIL_PAGE_URL"]?>">Подробнее</a>
                    </div>
                </div>
            </div>
        <?= ($globalCell % 2) ? '</div>' : ''; ?>
       <!-- <div class="wr_information infor_<?/*= ($globalCell % 2) ? 'right' : 'left'; */?>">
            <div class="info_title">
                <h2><?/*= $arElement["NAME"] */?></h2>
                <div class="div_line"></div>
                <p><?/*= $arElement["PREVIEW_TEXT"] */?></p>
                <div class="div_flex">
                    <a href="<?/*= $arElement["DETAIL_PAGE_URL"] */?>">Подробнее</a>
                    <?/*if($arElement["IMAGE"] ):*/?><img src="<?/*= $arElement["IMAGE"] */?>" alt="<?/*= $arElement["NAME"] */?>"/><?/*endif;*/?>
                </div>
            </div>
        </div>-->

        <?
        $globalCell ++;
    endforeach;?>

<br/><br/>
