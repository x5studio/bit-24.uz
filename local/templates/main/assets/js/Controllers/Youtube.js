/**
 * Created by Павел on 30.09.2015.
 */
if (typeof define === "function" && define.amd) {
    define(['router'], function () {

        function Youtube(p) {
            this.container = $(p);
            this.items = $();

            this.init();
        }

        Youtube.prototype.resetItems = function () {
            var o = this;
            o.items = $();

            o.container.find('iframe[src*="youtube.com"]').each(function () {
                var item = $(this);

                if (item.attr('src').match(/^https:\/\/www\.youtube\.com.*enablejsapi=1/i)) {
                    o.items = o.items.add(item);
                }
                else if (item.attr('src').match(/www\.youtube\.com/i)) {
                    var arSrc = item.attr('src').match(/^(.*)(www\.youtube\.com)([^\?]*)\??(.*)/i);
                    arSrc[1] = 'https://';
                    if (!arSrc[4].match(/enablejsapi=1/i)) {
                        if (arSrc[4].length > 0) {
                            arSrc[4] += '&';
                        }
                        arSrc[4] += "enablejsapi=1";
                    }
                    var newSrc = arSrc[1] + arSrc[2] + arSrc[3];
                    if (arSrc[4].length > 0) {
                        newSrc += '?' + arSrc[4];
                    }
                    item.attr('src', newSrc);
                    o.items = o.items.add(item);
                }
            });
        };

        Youtube.prototype.addEventListenerYT = function () {
            var item = $(this);

            function onPlayerStateChange(event) {
                if (event.data == 1) {
                    var vData = event.target.getVideoData();
                    window.track(["YouTube", "Начало просмотра", vData.title]);
                }
            }

            if (typeof YT == 'object') {
                var player = new YT.Player(this, {
                    events: {
                        'onStateChange': onPlayerStateChange
                    }
                });
                item.data('player', player);
            }
        };

        Youtube.prototype.loadScript = function (src) {
            var tag = document.createElement('script');
            tag.src = src;
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        };

        Youtube.prototype.init = function () {
            var o = this;
            o.resetItems();

            if (o.items.length > 0) {
                o.loadScript("https://www.youtube.com/iframe_api");
            }

            window.onYouTubeIframeAPIReady = function () {
                o.items.each(o.addEventListenerYT);
            }
        };

        return Youtube;
    });
}