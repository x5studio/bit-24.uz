/**
 * Created by Павел on 29.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/Contact/AContactBoxView'], function ($, AContactBoxView) {
        function ContactBoxView_4(item, options) {
            AContactBoxView.apply(this, arguments);
            this.options = $.extend(true, {}, this.options, ContactBoxView_4.defaultOptions, options);
        }

        extend(ContactBoxView_4, AContactBoxView);
        ContactBoxView_4.defaultOptions = {};

        ContactBoxView_4.prototype.compileOfficeItem = function (item, mOffice) {
            ContactBoxView_4.superclass.compileOfficeItem.apply(this, arguments);
            var o = this;
            var realOffice = o.getContactManager().getRealOfficeByID(mOffice['ID']);
            try {
                if (realOffice["PROPERTIES"]["ICON_METRO"]["VALUE"]) {
                    item.find('.box-office-name').addClass('metro metro-' + realOffice["PROPERTIES"]["ICON_METRO"]["VALUE"]);
                }
            } catch (e) {
            }
        };

        ContactBoxView_4.prototype.render = function () {
            ContactBoxView_4.superclass.render.apply(this, arguments);
            var o = this;
            o.getView().find('.box-offices-control').removeClass('hidden');
        };

        return ContactBoxView_4;
    });
}