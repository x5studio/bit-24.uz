<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
$arParams["TAG"] = intval($arParams["TAG"]);

$arParams["COUNT"] = intval($arParams["COUNT"]);
if ($arParams["COUNT"] <= 0)
	$arParams["COUNT"] = 12;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"] == "Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"] != "N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] != "N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"] == "Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"] !== "N";

if ($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if ($arNavigation["PAGEN"] == 0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] > 0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
} else
{
	$arNavParams = array(
		"nTopCount" => $arParams["COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

if ($this->StartResultCache(false, array($arNavigation)))
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",

	);

	if (strlen($arParams["SECTION_CODE"]))
	{
		$rsSection = \CIBlockSection::GetList(
			array(),
			array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ACTIVE" => "Y",
				"CODE" => $arParams["SECTION_CODE"]
			),
			false,
			array("ID", "NAME", "SECTION_PAGE_URL", "DESCRIPTION"));
		if ($arResult = $rsSection->GetNext())
		{
			$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arResult["ID"]);
			$arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();
			$arFilter["SECTION_ID"] = $arResult["ID"];
		}
	}

	if ($arParams["TAG"])
	{
		$arFilter["PROPERTY_TAG"] = $arParams["TAG"];
	}

	$arFile = array();

	$arResult["SECTIONS"] = array();
	$arResult["ITEMS"] = array();

	$rsSection = \CIBlockSection::GetList(
		array(),
		$arFilter,
		false,
		array("ID", "NAME", "IBLOCK_SECTION_ID", "SECTION_PAGE_URL", "DESCRIPTION", "DETAIL_PICTURE", "UF_SHORT_DESCRIPTION"));

	while ($arSec = $rsSection->GetNext())
	{	
		$arFile[] = $arSec["DETAIL_PICTURE"];
		$arResult["SECTIONS"][] = $arSec;
	}

	$arFilter[] = array(
		'LOGIC' => 'OR',
		array('PROPERTY_CITY' => $_SESSION["REGION"]["ID"]),
		array('PROPERTY_CITY' => false),
	);

	$rsElement = \CIBlockElement::GetList(
		array("SORT" => "ASC"),
		$arFilter,
		false,
		$arNavParams,
		array("ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PREVIEW_TEXT", "PREVIEW_PICTURE")
	);
	while ($arElement = $rsElement->GetNext())
	{

		$arFile[] = $arElement["PREVIEW_PICTURE"];

		$arResult["ITEMS"][] = $arElement;
	}
	
	$arResult["IMAGES"] = Helper::getImages($arFile);

	$arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
	$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();
	
	$this->SetResultCacheKeys(array("ID", "IPROPERTY_VALUES", "NAV_CACHED_DATA", "NAME", "SECTION_PAGE_URL"));
	$this->IncludeComponentTemplate();
}
if (isset($arResult["ID"]))
{
	$arTitleOptions = null;

	if (!empty($arResult["IPROPERTY_VALUES"]))
	{
		if ($arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"] != "")
			$APPLICATION->SetPageProperty("title", $arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"], $arTitleOptions);
		else
			$APPLICATION->SetPageProperty("title", $arResult["NAME"], $arTitleOptions);

		if ($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
			$APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arTitleOptions);
		else
			$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);

		if ($arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"] != "")
			$APPLICATION->SetPageProperty("keywords", $arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"], $arTitleOptions);
		if ($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"] != "")
			$APPLICATION->SetPageProperty("description", $arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"], $arTitleOptions);
	}

	$APPLICATION->AddChainItem($arResult["NAME"], $arResult["SECTION_PAGE_URL"]);
}