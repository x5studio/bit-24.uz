<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use PB\Main\Loc;

if($arParams['LOAD_CITY_AJAX']) {
    $mskOblName = Loc::getMessage("PB_GEO_TEMPLATE_CITY_POPUP_CHOISE_REGION_MSK");
    $firstCountryCode = 'ru';
    $idRussia = 0;
    $russiaArray = Array();
    $cityPrioritet = array();

    $cityMskObl = array();
    $idTypeRegionMsk = null;
    $rsDataUserFieldType = CUserTypeEntity::GetList( array(), array('ENTITY_ID'=>'IBLOCK_'.IB_CONTACTS.'_SECTION','FIELD_NAME'=>'UF_REGION') );
    $arDataUserFieldType = $rsDataUserFieldType->Fetch();
    if($arDataUserFieldType['ID'] > 0) {
        if($arDataUserFieldType['USER_TYPE_ID'] == 'string') {
            $idTypeRegionMsk = $mskOblName;
        }
        else {
            $rsTypeRegion = CUserFieldEnum::GetList(array(), array("USER_FIELD_ID" => $arDataUserFieldType,'VALUE' => $mskOblName));
            $arTypeRegion = $rsTypeRegion->GetNext();
            if($arTypeRegion['ID'] > 0) {
                $idTypeRegionMsk = $arTypeRegion['ID'];
            }
        }
    }

    foreach ($arResult['SELECT'] as $id => $country) {
        if(count($country['CITY']) == 0) {
            unset($arResult['SELECT'][$id]);
        }
        else {
            if($country['CODE'] == $firstCountryCode) {
                $idRussia = $country['ID'];
            }
            foreach ($country['CITY'] as $city_code => $city) {
                /*получаем массив приоритетных городов для первой колонки*/
                if(in_array($city['ID'],$arParams['UI_CITY_PRIORITET'])) {
                    $cityPrioritet[$city['ID']] = $city;
                    unset($arResult['SELECT'][$id]['CITY'][$city_code]);
                }
                /*--------------------------------------------------------*/

                /*получаем массив городов московской области*/
                if($idTypeRegionMsk !== null) {
                    if($city['UF_REGION'] == $idTypeRegionMsk) {
                        if($city['NAME'] != $mskOblName && !array_key_exists($city['ID'],$cityPrioritet)) {
                            $cityMskObl[$city['ID']]=$city;
                        }
                        elseif($city['NAME'] == $mskOblName) $arResult['MSK_OBL'] = $city;
                        unset($arResult['SELECT'][$id]['CITY'][$city_code]);
                    }
                }
                /*-----------------------------------------------*/

            }
        }
    }
    if($idRussia > 0) {
        $russiaArray = $arResult['SELECT'][$idRussia];
        unset($arResult['SELECT'][$idRussia]);
    }
    $countRussiaColumn = count($russiaArray['CITY']);
    if($countRussiaColumn == 0) {
        $countRussiaColumn = 4;
    }

    $arResult['CITY_PRIORITET'] = $cityPrioritet;
    $arResult['CITY_MSK_OBL'] = $cityMskObl;
    $arResult['RUSSIA_ARRAY'] = $russiaArray;
    $arResult['COUNT_RUSSIA_COLUMN'] = $countRussiaColumn;
    $arResult['MSK_OBL_NAME'] = $mskOblName;
    $arResult['FIRST_COUNTRY_CODE'] = $firstCountryCode;
    $arResult['COUNTRY_ARRAY'] = $countryArray;
}