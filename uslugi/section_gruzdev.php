<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<div class="container">
	<div class="row row-offcanvas row-offcanvas-left">
		<div class="col-xs-12 col-sm-6 col-md-3 sidebar-offcanvas" id="sidebar">
			<div class="side-menu">
				<nav class="navbar navbar-default" role="navigation">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header visible-xs visible-sm">
						<div class="brand-wrapper">
							<h2>Фильтр услуг </h2>
							<!-- Hamburger -->
							<button type="button" class="btn btn-close hidden-lg hidden-md" data-toggle="offcanvas" onclick="demoVisibility()">
								<i class="fa fa-times" aria-hidden="true"></i>
							</button>
						</div>
					</div>
					<!-- Left Menu -->
					<div class="side-menu-container">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "service.sidebar.gruzdev", Array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "serviceinner",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						),
							false
						);?>
					</div><!-- /.navbar-collapse -->
				</nav>
			</div>
		</div>
		<!-- Main-content -->
		<div class="hidden-lg hidden-md col-sm-6 col-xs-12 filter">
			<button type="button" class="nav-toggle nav-toggle_1" data-toggle="offcanvas" onclick="demoDisplay()"><a href="#" title="link">Услуги</a><i class="fa fa-filter" aria-hidden="true"></i></button>
		</div>
		<div class="hidden-lg hidden-md col-sm-6 hidden-xs">
			<form action="#">
				<div class="input-group search">
					<input type="text" class="form-control" placeholder="Поиск">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
				</div>
			</form>
		</div>
		<div class="col-md-9 col-sm-12 col-xs-12">
			<div class="main-service">
				<div class="service1 pull-right">

					<?$APPLICATION->IncludeComponent(
						"service.section",
						".default",
						Array(
                            "CACHE_TIME" => "3600",
                            "CACHE_TYPE" => "A",
                            "COUNT" => "50",
                            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                            "SECTION_CODE" => $arParams["SECTION_CODE"],
						)
					);?>
				</div>
			</div>
		</div>
	</div>
</div>

