<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arElementId = array("first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eight", "nine", "ten", "eleven", "twelve");
?>
<? if (!empty($arResult["ITEMS"])): ?>
	<section class="Bottom-Slider">
		<div class="row">
			<div id="text-carousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
				<div class="ind-line ">
					<div class="container">
						<ol class="carousel-indicators">
							<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
								<li id="<?= $arElementId[$cell] ?>" data-target="#text-carousel" data-slide-to="<?= $cell ?>"<?
								if (!$cell) echo ' class="active"'; ?>><i class="fa <?= $arItem["PROPERTY_CLASS_VALUE"] ?>"<?
									if ($cell) echo ' aria-hidden="true"'; ?>></i></li>
							<? endforeach; ?>
						</ol>
					</div>
				</div>
				<!-- Wrapper for slides -->
				<div class="container">
					<div class="row">
						<div class="slider col-md-8">
							<div class="carousel-inner">
								<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
									<div class="item<? if (!$cell): ?> active<? endif; ?>">
										<div class="slide1">
											<h2>Возможности Битрикс24</h2>
											<h4><?= $arItem["NAME"] ?></h4>
											<p><?= $arItem["PREVIEW_TEXT"] ?></p>
											<? if (strlen($arItem["CODE"]) > 0): ?>
												<button type="button" class="btn btn-more"><a href="<?= $arItem["CODE"] ?>">Подробнее</a>
												</button>
											<? endif; ?>
										</div>
									</div>
								<? endforeach; ?>
								<!-- Controls -->
								<div class="clcontrols">
									<a class="left carousel-control" href="#text-carousel" role="button" data-slide="prev">
										<i class="fa fa-angle-left" aria-hidden="true"></i>
										<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#text-carousel" role="button" data-slide="next">
										<i class="fa fa-angle-right" aria-hidden="true"></i>
										<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>