<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($arResult['ITEMS'])
{
    foreach ($arResult['ITEMS'] as $i => $arItem)
    {
        $arResult['ITEMS'][$i]["DATE"] = ToLower(FormatDate('d F Y, D. H:i', MakeTimeStamp($arItem['DATE_ACTIVE_TO'])));
    }
}
?>
