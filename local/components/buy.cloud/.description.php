<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Облачная версия",
	"DESCRIPTION" => "Облачная версия",
	"ICON" => "/images/news_line.gif",
	"SORT" => 100,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "buy",
			"NAME" => "Купить лицензию"
		)
	),
);

?>