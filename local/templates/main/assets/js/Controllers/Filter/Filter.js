function Filter(container) {
    this.container = container;

    this.model = $('.filter-model', this.container);
    this.select = $('.filter-select', this.container);
    this.defaultOptions = this.findAllOptions().clone();
}

Filter.prototype.findAllOptions = function () {
    return $();
}

Filter.prototype.init = function () {
    var o = this;

    this.model.on('change', function () {
        o.do(this.value);
    });
}

if (typeof define === "function" && define.amd) {
    define([], function () {
        return Filter;
    });
}