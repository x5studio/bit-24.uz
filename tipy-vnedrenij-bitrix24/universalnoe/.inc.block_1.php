<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="col-md-7 col-sm-12 col-xs-12">
    <div class="title-page">
        <h1 class="title-page__h1"><? $APPLICATION->ShowTitle(true) ?></h1>
    </div>
	<p>
		 Опираясь на многолетний опыт запуска платформы Битрикс24 CRM в различных сферах мы разработали универсальный план внедрения CRM. Он позволяет настроить базовый функционал Битрикс24 в рекордно короткие сроки и тем самым довольно быстро повысить эффективность работы с клиентами.
	</p>
</div>
<div class="col-md-5 col-sm-12 col-xs-12">
 <img src="/local/templates/main/assets/img/impl/001.png" class="img-responsive center-block">
</div>
 <br>
