<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult['TAG'])): ?>
	<div class="tagspopular">
		<? foreach ($arResult['TAG'] as $arItem): ?>
			<a href="<?= $arResult["URL"] ?>?tag=<?= $arItem["CODE"] ?>" class="btn btn-sm<?
			if ($arResult["ID"] == $arItem["ID"]): ?> is-active<? endif ?>"><?= $arItem["NAME"] ?></a>
		<? endforeach; ?>
	</div>
<? endif;