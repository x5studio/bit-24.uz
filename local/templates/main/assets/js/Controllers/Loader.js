if (typeof define === "function" && define.amd) {
    define([], function () {
        function Loader(p) {
            var o = this;
            this.el = p.el;
            this.loader = $('<div class="loader loader17"></div>');

            this.defLoader = $.Deferred(function (def) {
                def.progress(function (p) {
                    switch (p) {
                        case 'start':
                            o.showLoader();
                            break;
                    }
                });
                def.always(function () {
                    o.hideLoader();
                });

            });
        }

        Loader.prototype.showLoader = function () {
            var o = this;
            o.el.hide();
            o.el.after(o.loader);
        };
        Loader.prototype.hideLoader = function () {
            var o = this;
            o.loader.remove();
            o.el.show();
        };

        return Loader;
    });
}