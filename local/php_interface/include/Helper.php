<?php

namespace Proman;

use \Bitrix\Main\Loader;
use \Bitrix\Main\Data\Cache;
use \Bitrix\Main\Entity\ExpressionField;

class Helper
{

	public static function getEndWord($number, $suffix)
	{
		$keys = array(2, 0, 1, 1, 1, 2);
		$mod = $number % 100;
		$suffix_key = ($mod > 7 && $mod < 20) ? 2 : $keys[min($mod % 10, 5)];
		return $suffix[$suffix_key];
	}

	public static function FormatPrice($price)
	{
		return number_format(DoubleVal($price), 0, '', ' ');
	}

	public static function getImages($arFileId)
	{
		$result = array();
		if (is_array($arFileId) && !empty($arFileId))
		{
			$upload = \COption::GetOptionString("main", "upload_dir");
			$rsFile = \CFile::GetList(array(), array("@ID" => implode(",", $arFileId)));
			while ($arFile = $rsFile->Fetch())
				$result[$arFile["ID"]] = "/" . $upload . "/" . $arFile["SUBDIR"] . "/" . $arFile["FILE_NAME"];
		}
		return $result;
	}

	public static function getFilesExt($arFileId)
	{
		$result = array();
		if (is_array($arFileId) && !empty($arFileId))
		{
			$upload = \COption::GetOptionString("main", "upload_dir");
			$rsFile = \CFile::GetList(array(), array("@ID" => implode(",", $arFileId)));
			while ($arFile = $rsFile->Fetch())
			{
				$result[$arFile["ID"]] = array(
					"SRC" => "/" . $upload . "/" . $arFile["SUBDIR"] . "/" . $arFile["FILE_NAME"],
					"SIZE" => \CFile::FormatSize($arFile["FILE_SIZE"], 0),
					"EXT" => ToLower(substr(strrchr($arFile["FILE_NAME"], '.'), 1))
				);
			}
		}
		return $result;
	}

}