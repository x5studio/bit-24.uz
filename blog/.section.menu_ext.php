<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
global $APPLICATION;

$cacheId = 'menu_iblock_2';
$cache_dir = SITE_ID . '/bitrix/menu/topmenu';
$obCache = new CPHPCache;

if ($obCache->InitCache(3600, $cacheId, $cache_dir)) {
    $aMenuLinksExt = $obCache->GetVars();
}
elseif ($obCache->StartDataCache()) {
    $rsItems = CIBlockSection::GetList(
        ['SORT' => 'ASC'],
        [
            'IBLOCK_ID' => 2,
            'ACTIVE' => 'Y'
        ],
        false,
        ['ID', 'NAME', 'CODE', 'SECTION_PAGE_URL']
    );

    while($arItem = $rsItems->GetNext()) {
        $menuItem = [$arItem['NAME'], $arItem['DETAIL_PAGE_URL'], [], [], ""];

        $aMenuLinksExt[] = $menuItem;
    }

    global $CACHE_MANAGER;
    $CACHE_MANAGER->StartTagCache($cache_dir);
    $CACHE_MANAGER->RegisterTag('iblock_id_2');
    $CACHE_MANAGER->RegisterTag('bitrix:menu');
    $CACHE_MANAGER->EndTagCache();
    $obCache->EndDataCache($aMenuLinksExt);
}
else {
    $aMenuLinksExt = [];
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>