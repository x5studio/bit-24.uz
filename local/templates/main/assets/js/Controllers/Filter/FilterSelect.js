function FilterSelect(container) {
    Filter.apply(this, arguments);
    this.init();
}

if (typeof define === "function" && define.amd) {
    define(['Controls/Filter/Filter'], function (Filter) {

        extend(FilterSelect, Filter);

        FilterSelect.prototype.findAllOptions = function () {
            var o = this;
            return o.select.find('option');
        }

        FilterSelect.prototype.do = function (val) {
            var o = this;
            var defaultOptionExp= new RegExp("^--.+--$", 'i');
            var filterOptionExp  = new RegExp(val, 'i');
            var filteredOptions = o.defaultOptions.filter(function (index, element) {
                var item = $(element);
                if (item.text().match(filterOptionExp) || item.text().match(defaultOptionExp)) {
                    return true;
                }
            });
            o.select.html(filteredOptions);
        }

        return FilterSelect;
    });
}