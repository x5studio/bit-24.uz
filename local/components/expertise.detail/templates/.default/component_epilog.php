<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Page\Asset::getInstance()->addString('<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>');

if($arResult['~UF_FORM_TITLE']) {
    global $APPLICATION;
    $APPLICATION->SetPageProperty('form_title', $arResult['~UF_FORM_TITLE']);
}
?>

<div class="j-deffered">
    <div to="WIDGET_CONSULTATION_FORM" class="consultation">
        <div class="container">
            <div class="consultation__wrapper">
                <div class="consultation__inner">
                    <div class="consultation__title">Настроим Битрикс24 для вашего бизнеса</div>
                    <div class="consultation__subtitle">Ответим на любые вопросы и дадим полную консультацию. Бесплатно.</div>
                    <div class="consultation__form">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bezr:form.result.new.befsend",
                            "inline",
                            array(
                                "WEB_FORM_ID" => FORM_CONSULTATION,	// ID веб-формы
                                "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
                                "COMPANY_IBLOCK_ID" => IB_CONTACTS,
                                "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
                                "EMAIL_QUESTION_CODE" => "EMAIL",
                                "CITY_QUESTION_CODE" => "CITY",
                                "OFFICE_QUESTION_CODE" => "OFFICE",
                                "TO_QUESTION_RESIPIENTS" => "",
                                "EMAIL_ONLY_FROM_RESIPIENTS" => "",
                                "TO_QUESTION_CODE" => "TO",
                                "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
                                "USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
                                "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                                "CACHE_TYPE" => "A",	// Тип кеширования
                                "CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                "LIST_URL" => "",	// Страница со списком результатов
                                "EDIT_URL" => "",	// Страница редактирования результата
                                "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
                                "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
                                "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
                                "SHOW_TITLE" => "N",
                                "PLACEHOLDER_MODE" => 'Y',
                                "VARIABLE_ALIASES" => array(
                                    "WEB_FORM_ID" => "WEB_FORM_ID",
                                    "RESULT_ID" => "RESULT_ID",
                                ),
                                "ALERT_ADD_SHARE" => "N",
                                "HIDE_PRIVACY_POLICE" => "N",
                                "HIDE_FIELDS" => array(
                                    0 => "CITY",
                                    1 => "OFFICE",
                                ),
                                "COMPONENT_MARKER" => "subscribe-inline",
                                "SHOW_FORM_DESCRIPTION" => "N",
                                "MESS" => array(
                                    "THANK_YOU" => "Спасибо, Ваш запрос принят.",
                                    "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
                                    "TITLE_FORM" => "",
                                    "SUBMIT_BTN" => "Отправить",
                                ),
                                'KEEP_ERRORS' => 'Y',
                            ),
                            false,
                            array(
                                "HIDE_ICONS" => "Y"
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
