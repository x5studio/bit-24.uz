if (typeof define === "function" && define.amd) {
    define(['Controls/Select2Select', 'cusel'], function (Select2Select) {
        function RelatedSelects(p) {
            this.p = p;
            var o = this;

            o.select1 = $(p.select1);
            if (p.root) {
                o.root = $(p.root);
            }
            if (!(p.root && o.root.length > 0)) {
                var Form = o.select1.closest('form');
                if (Form.length > 0) {
                    o.root = Form.first();
                } else {
                    o.root = $(document);
                }
            }

            o.select2 = o.findSelect2();
            o.select2wrap = o.select2.find(".cusel-scroll-wrap");
            o.select2pane = o.select2.find(".cusel-scroll-pane");

            if (o.select1.length > 0 && o.select2.length > 0) {
                o.init();
            }
        }

        RelatedSelects.prototype.init = function () {
            var o = this;
            o.obS2S = new Select2Select({
                root: o.root,
                select1: o.getSelectID(o.select1),
                select2: o.getSelectID(o.select2),
                attrInherit: "parent-id",
                cuselLists: true
            });
            o.obS2S.init();

            $(document).on("afterLoad", function () {
                setTimeout(function () {
                    var select2items = o.select2wrap.find('span');
                    var maxItems = 5;
                    if (o.select2.hasClass('j-select-smart')) {
                        maxItems = o.select2.data('visRows') || select2items.length || maxItems;
                    }
                    var visRows = (select2items.length > maxItems) ? maxItems : select2items.length;
                    cuSelRefresh({
                        refreshEl: '#' + o.getSelectID(o.select2),
                        visRows: visRows,
                        scrollArrows: true
                    });
                    if (select2items.length <= maxItems) {
                        o.select2wrap.css({height: 'auto', display: 'block'});
                        o.select2pane.jScrollPaneRemoveCusel();
                        o.select2pane.css({height: select2items.not('.empty').eq(0).outerHeight() * select2items.length + "px"});
                        o.select2wrap.css({display: 'none'});
                    }
                }, 0);
            });
        };

        RelatedSelects.prototype.getSelectID = function (el) {
            if (el.hasClass('cusel')) {
                return el.find('input[type=hidden]').attr('id');
            } else {
                return el.attr('id');
            }
        };

        RelatedSelects.prototype.findSelect2 = function () {
            var o = this;
            var groupName = o.getGroupName();
            if (groupName) {
                return o.root.find('.' + groupName).not('#' + o.getSelectID(o.select1)).not('#cuselFrame-' + o.getSelectID(o.select1));
            }
        };

        RelatedSelects.prototype.getGroupName = function () {
            var o = this;
            var reRSG = new RegExp("^rsg-[\\w-]+", 'i');
            var arClass = o.select1.attr('class').split(/\s+/);
            var relatedGroupClass = '';
            for (var index in arClass) {
                var itemClass = arClass[index]
                if (itemClass.match(reRSG)) {
                    relatedGroupClass = itemClass;
                    break;
                }
            }
            return (relatedGroupClass.length > 0) ? relatedGroupClass : null;
        };

        return RelatedSelects;
    });
}