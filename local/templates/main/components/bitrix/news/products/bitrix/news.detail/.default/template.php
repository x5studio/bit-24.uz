<? use Bitrix\Main\Loader;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$city = $request->getCookieRaw('BITRIX_SM_SMART_CHANGE_CITY');
$uri = new \Bitrix\Main\Web\Uri($request->getRequestUri());
if(empty($city) || $city==NULL){
    $city = $request->getCookieRaw('BITRIX_SM_CONFIRM');
    if(empty($city) || $city==NULL){
        $city=$_SESSION['GEOIP']['country']['id'];
    }
}
global $arrFilter;
$arrFilter['PROPERTY_CITY']=$city;
$arrFilter['ID']=$arResult['PROPERTIES']['SIMILAR']['VALUE'];
?>




<main>
    <section class="offer offer--cloud">
        <div class="offer__inner container">
            <div class="offer__img">
                <?if(!empty($arResult['DETAIL_PICTURE']['src'])){?>
                <img src="<?= $arResult['DETAIL_PICTURE']['src'] ?>" alt="<?= $arParams['PRODUCT_VERSION'] ?>">
                <?}elseif(!empty($arResult['PREVIEW_PICTURE']['src'])){?>
                <img src="<?= $arResult['PREVIEW_PICTURE']['src'] ?>" alt="<?= $arParams['PRODUCT_VERSION'] ?>">
                <?}else{?>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/bitrix-box-big.png" alt="<?= $arParams['PRODUCT_VERSION'] ?>">
                <?}?>
            </div>
            <div class="offer__content">
                <h1 class="offer__title" id="product-name"><?= $arResult['NAME'] ?></h1>
                <!--      <h2>Битрикс24 CRM+</h2>-->
                <?if($arParams['PRODUCT_VERSION']=='Облачная'){?>
                <div class="offer__period">
                    <p>Период лицензии</p>
                    <ul class="offer__period-list">
                        <? if (!empty($arResult['PROPERTIES']['PERIOD_PRICE_1MONTH']['VALUE'])) { ?>
                            <li class="offer__period-item">
                                <button class="offer__period-btn" id="1-month">1 месяц
                                    <? if (!empty($arResult['PROPERTIES']['PERIOD_NEW_PRICE_1MONTH']['VALUE'])) { ?>
                                        <span>
                                        –<?= round(((int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_PRICE_1MONTH']['VALUE'])) - (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_NEW_PRICE_1MONTH']['VALUE']))) / (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_PRICE_1MONTH']['VALUE'])) * 100) ?>%</span>
                                    <? } ?>
                                </button>
                            </li>
                        <? } ?>
                        <? if (!empty($arResult['PROPERTIES']['PERIOD_PRICE_3MONTH']['VALUE'])) { ?>
                            <li class="offer__period-item">
                                <button class="offer__period-btn" id="3-month">3 месяца
                                    <? if (!empty($arResult['PROPERTIES']['PERIOD_NEW_PRICE_3MONTH']['VALUE'])) { ?>
                                        <span>
                                        –<?= round(((int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_PRICE_3MONTH']['VALUE'])) - (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_NEW_PRICE_3MONTH']['VALUE']))) / (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_PRICE_3MONTH']['VALUE'])) * 100) ?>%</span>
                                    <? } ?>
                                </button>
                            </li>
                        <? } ?>
                        <? if (!empty($arResult['PROPERTIES']['PERIOD_PRICE_1YEAR']['VALUE']) || !empty($arResult['PROPERTIES']['PRICE']['VALUE'])) { ?>
                            <li class="offer__period-item">
                                <button class="offer__period-btn <?if($arParams['PRODUCT_VERSION']!='Облачная'){?>offer__period-btn--active<?}?>" id="1-year">1 год
                                    <? if (!empty($arResult['PROPERTIES']['PERIOD_NEW_PRICE_1YEAR']['VALUE'])) { ?>
                                        <span>
                                        –<?= round(((int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_PRICE_1YEAR']['VALUE'])) - (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_NEW_PRICE_1YEAR']['VALUE']))) / (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_PRICE_1YEAR']['VALUE'])) * 100) ?>%</span>
                                    <? } elseif (!empty($arResult['PROPERTIES']['PRICE_NEW']['VALUE'])) { ?><span>
                                        –<?= round(((int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PRICE']['VALUE'])) - (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PRICE_NEW']['VALUE']))) / (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PRICE']['VALUE'])) * 100) ?>%</span>
                                    <? } ?>
                                </button>
                            </li>
                        <? } ?>
                        <? if (!empty($arResult['PROPERTIES']['PERIOD_PRICE_2YEAR']['VALUE'])) { ?>
                            <li class="offer__period-item">
                                <button class="offer__period-btn <?if($arParams['PRODUCT_VERSION']=='Облачная'){?>offer__period-btn--active<?}?>" id="2-year">2 года
                                    <? if (!empty($arResult['PROPERTIES']['PERIOD_NEW_PRICE_2YEAR']['VALUE'])) { ?>
                                        <span>
                                        –<?= round(((int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_PRICE_2YEAR']['VALUE'])) - (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_NEW_PRICE_2YEAR']['VALUE']))) / (int)trim(str_replace(" ",'',$arResult['PROPERTIES']['PERIOD_PRICE_2YEAR']['VALUE'])) * 100) ?>%</span>
                                    <? } ?>
                                </button>
                            </li>
                        <? } ?>
                    </ul>
                </div>
                <?}?>
                <div class="offer__containers">
                    <div class="offer__container">
                        <div class="offer__version">
                            <p>Версия:<span
                                        class="offer__version-result"> <?= $arParams['PRODUCT_VERSION'] ?></span>
                            </p>
                            <div class="question-icon">
                                <button class="question-icon__btn" type="button">
                                    <span class="question-icon__img"></span>
                                </button>
                                <div class="question-icon__content">
                                    <button class="question-icon__close-btn" type="button">
                                        <span class="visually-hidden">Закрыть подсказку</span>
                                    </button>
                                    <p><?= $arParams['TIP_DESCR'] ?></p>
                                </div>
                            </div>
                        </div>
                        <p class="offer__user-count">
                            Пользователи: <?= $arResult['PROPERTIES']['USER']['VALUE'] ?>
                        </p>
                        <?if($arParams['PRODUCT_VERSION']=='Облачная' && !empty($arResult['PROPERTIES']['CLOUD_DISK_SPACE']['VALUE'])){?>
                        <p class="offer__user-count">
                            Место на диске: <?= $arResult['PROPERTIES']['CLOUD_DISK_SPACE']['VALUE'] ?>
                        </p>
                        <?}?>
                        <?if($arParams['PRODUCT_VERSION']=='Облачная'){?>
                            <ul class="offer__options">
                                <li class="offer__option-item <? if ($arResult['PROPERTIES']['CLOUD_CRM']['VALUE'] == 1) { ?> offer__option-item--available<? } ?>">
                                    CRM
                                </li>
                                <li class="offer__option-item <? if ($arResult['PROPERTIES']['CLOUD_TASKS']['VALUE'] == 1) { ?> offer__option-item--available<? } ?>">
                                    Задачи и проекты
                                </li>
                                <li class="offer__option-item <? if ($arResult['PROPERTIES']['CLOUD_ANALITICS']['VALUE'] == 1) { ?> offer__option-item--available<? } ?>">
                                    Сквозная аналитика
                                </li>
                                <li class="offer__option-item <? if ($arResult['PROPERTIES']['COUD_MARKETING']['VALUE'] == 1) { ?> offer__option-item--available<? } ?>">
                                    CRM-Маркетинг
                                </li>
                            </ul>
                        <?}else{?>
                        <ul class="offer__options">
                            <li class="offer__option-item <? if ($arResult['PROPERTIES']['CHECK_1']['VALUE'] == 1) { ?> offer__option-item--available<? } ?>">
                                Экстранет
                            </li>
                            <li class="offer__option-item <? if ($arResult['PROPERTIES']['CHECK_4']['VALUE'] == 1) { ?> offer__option-item--available<? } ?>">
                                eCommerce-платформа
                            </li>
                            <li class="offer__option-item <? if ($arResult['PROPERTIES']['CHECK_2']['VALUE'] == 1) { ?> offer__option-item--available<? } ?>">
                                Многодепартаментность
                            </li>
                            <li class="offer__option-item <? if ($arResult['PROPERTIES']['CHECK_3']['VALUE'] == 1) { ?> offer__option-item--available<? } ?>">
                                Веб-кластер
                            </li>
                        </ul>
                        <?}?>
                    </div>
                    <div class="offer__wrapper-price-btn">
                        <?if($arParams['PRODUCT_VERSION']=='Облачная'){?>
                        <div class="offer__wrapper-price" style="display: none" id="1-month-price">
                            <? if (!empty($arResult['PROPERTIES']['PERIOD_NEW_PRICE_1MONTH']['VALUE'])) { ?>
                                <p class="offer__old-price"><?= $arResult['PROPERTIES']['PERIOD_PRICE_1MONTH']['VALUE'] ?></p>
                            <? } ?>
                            <p class="offer__price"><?= $arResult['PROPERTIES']['PERIOD_NEW_PRICE_1MONTH']['VALUE'] ? $arResult['PROPERTIES']['PERIOD_NEW_PRICE_1MONTH']['VALUE'] : $arResult['PROPERTIES']['PERIOD_PRICE_1MONTH']['VALUE'] ?>
                                / 1 мес.</p>

                        </div>
                        <div class="offer__wrapper-price" style="display: none" id="3-month-price">
                            <? if (!empty($arResult['PROPERTIES']['PERIOD_NEW_PRICE_3MONTH']['VALUE'])) { ?>
                                <p class="offer__old-price"><?= $arResult['PROPERTIES']['PERIOD_PRICE_3MONTH']['VALUE'] ?></p>
                            <? } ?>
                            <p class="offer__price"><?= $arResult['PROPERTIES']['PERIOD_NEW_PRICE_3MONTH']['VALUE'] ? $arResult['PROPERTIES']['PERIOD_NEW_PRICE_3MONTH']['VALUE'] : $arResult['PROPERTIES']['PERIOD_PRICE_3MONTH']['VALUE'] ?>
                                / 1 мес.</p>

                        </div>
                        <div class="offer__wrapper-price" style="display: none"  id="1-year-price">
                                <? if (!empty($arResult['PROPERTIES']['PERIOD_NEW_PRICE_1YEAR']['VALUE'])) { ?>
                                    <p class="offer__old-price"><?= $arResult['PROPERTIES']['PERIOD_PRICE_1YEAR']['VALUE'] ?></p>
                                <? } ?>
                                <p class="offer__price"><?= $arResult['PROPERTIES']['PERIOD_NEW_PRICE_1YEAR']['VALUE'] ? $arResult['PROPERTIES']['PERIOD_NEW_PRICE_1YEAR']['VALUE'] : $arResult['PROPERTIES']['PERIOD_PRICE_1YEAR']['VALUE'] ?>
                                    / 1 мес.</p>
                        </div>
                        <div class="offer__wrapper-price" id="2-year-price">
                            <? if (!empty($arResult['PROPERTIES']['PERIOD_NEW_PRICE_2YEAR']['VALUE'])) { ?>
                                <p class="offer__old-price"><?= $arResult['PROPERTIES']['PERIOD_PRICE_2YEAR']['VALUE'] ?></p>
                            <? } ?>
                            <p class="offer__price"><?= $arResult['PROPERTIES']['PERIOD_NEW_PRICE_2YEAR']['VALUE'] ? $arResult['PROPERTIES']['PERIOD_NEW_PRICE_2YEAR']['VALUE'] : $arResult['PROPERTIES']['PERIOD_PRICE_2YEAR']['VALUE'] ?>
                                / 1 мес.</p>
                        </div>
                        <?}else{?>
                            <div class="offer__wrapper-price"  id="2-year-price">
                                <? if (!empty($arResult['PROPERTIES']['KOROBKA_PRICE_NEW']['VALUE'])) { ?>
                                    <p class="offer__old-price"><?= $arResult['PROPERTIES']['KOROBKA_PRICE']['VALUE'] ?></p>
                                <? } ?>
                                <p class="offer__price"><?= $arResult['PROPERTIES']['KOROBKA_PRICE_NEW']['VALUE'] ? $arResult['PROPERTIES']['KOROBKA_PRICE_NEW']['VALUE'] : $arResult['PROPERTIES']['KOROBKA_PRICE']['VALUE'] ?> / 12 мес.</p>
                            </div>
                        <?}?>
                        <div class="footbtn">
                            <button type="button" onclick=" return true;" id="do-order" class="btn offer__link-order"
                                    data-toggle="modal" data-target="#advanced"
                                    <?if($arParams['PRODUCT_VERSION']=='Облачная'){?>
                                    data-form-field-type="Купить <?= $arResult['NAME'].' '.$arParams['PRODUCT_VERSION'] ?> на срок 2 года">Заказать
                                    <?}else{?>
                                    data-form-field-type="Купить <?= $arResult['NAME'].' '.$arParams['PRODUCT_VERSION'] ?> на срок 1 год">Заказать
                                <?}?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="offer__description">
                    <div class="offer__description-title">Описание</div>
                    <p>
                        <?= $arResult['DETAIL_TEXT'] ? $arResult['DETAIL_TEXT'] : $arResult['PREVIEW_TEXT'] ?>
                    </p>
                </div>
                <div class="offer__following-service">
                    <div class="offer__following-service-title">Сопутствующие услуги</div>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:main.include",
                        "",
                        Array(
                            "AREA_FILE_SHOW" => "file",
                            "AREA_FILE_SUFFIX" => "inc",
                            "EDIT_TEMPLATE" => "",
                            "PATH" => "/include/detail_product.php"
                        )
                    );?>
                </div>
            </div>
        </div>
    </section>
<?
if(!empty($arResult['PROPERTIES']['SIMILAR']['VALUE'])) {
    $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "similar_products",
        array(
            "IBLOCK_TYPE"                     => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID"                       => $arParams["IBLOCK_ID"],
            "NEWS_COUNT"                      => $arParams["NEWS_COUNT"],
//        "SORT_BY1" => $sort,
//        "SORT_ORDER1" => $order,
//		"SORT_BY2" => $arParams["SORT_BY2"],
//		"SORT_ORDER2" => $arParams["SORT_ORDER2"],
            "FIELD_CODE"                      => $arParams["LIST_FIELD_CODE"],
            "PROPERTY_CODE"                   => array('*'),
            "DETAIL_URL"                      => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["detail"],
            "SECTION_URL"                     => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
            "IBLOCK_URL"                      => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["news"],
            "DISPLAY_PANEL"                   => $arParams["DISPLAY_PANEL"],
            "SET_TITLE"                       => $arParams["SET_TITLE"],
            "SET_LAST_MODIFIED"               => $arParams["SET_LAST_MODIFIED"],
            "MESSAGE_404"                     => $arParams["MESSAGE_404"],
            "SET_STATUS_404"                  => $arParams["SET_STATUS_404"],
            "SHOW_404"                        => $arParams["SHOW_404"],
            "FILE_404"                        => $arParams["FILE_404"],
            "INCLUDE_IBLOCK_INTO_CHAIN"       =>'N',
            "CACHE_TYPE"                      => $arParams["CACHE_TYPE"],
            "CACHE_TIME"                      => $arParams["CACHE_TIME"],
            "CACHE_FILTER"                    => $arParams["CACHE_FILTER"],
            "CACHE_GROUPS"                    => $arParams["CACHE_GROUPS"],
            "DISPLAY_TOP_PAGER"               => $arParams["DISPLAY_TOP_PAGER"],
            "DISPLAY_BOTTOM_PAGER"            => $arParams["DISPLAY_BOTTOM_PAGER"],
            "PAGER_TITLE"                     => $arParams["PAGER_TITLE"],
            "PAGER_TEMPLATE"                  => $arParams["PAGER_TEMPLATE"],
            "PAGER_SHOW_ALWAYS"               => $arParams["PAGER_SHOW_ALWAYS"],
            "PAGER_DESC_NUMBERING"            => $arParams["PAGER_DESC_NUMBERING"],
            "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
            "PAGER_SHOW_ALL"                  => $arParams["PAGER_SHOW_ALL"],
            "PAGER_BASE_LINK_ENABLE"          => $arParams["PAGER_BASE_LINK_ENABLE"],
            "PAGER_BASE_LINK"                 => $arParams["PAGER_BASE_LINK"],
            "PAGER_PARAMS_NAME"               => $arParams["PAGER_PARAMS_NAME"],
            "DISPLAY_DATE"                    => $arParams["DISPLAY_DATE"],
            "DISPLAY_NAME"                    => "Y",
            "DISPLAY_PICTURE"                 => $arParams["DISPLAY_PICTURE"],
            "DISPLAY_PREVIEW_TEXT"            => $arParams["DISPLAY_PREVIEW_TEXT"],
            "PREVIEW_TRUNCATE_LEN"            => $arParams["PREVIEW_TRUNCATE_LEN"],
            "ACTIVE_DATE_FORMAT"              => $arParams["LIST_ACTIVE_DATE_FORMAT"],
            "USE_PERMISSIONS"                 => $arParams["USE_PERMISSIONS"],
            "GROUP_PERMISSIONS"               => $arParams["GROUP_PERMISSIONS"],
            "FILTER_NAME"                     => 'arrFilter',
            "HIDE_LINK_WHEN_NO_DETAIL"        => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
            "CHECK_DATES"                     => $arParams["CHECK_DATES"],
            "PARENT_SECTION"                  => $arParams['PARENT_SECTION'],
            'TIP_DESCR'                       => $arParams['TIP_DESCR'],
            'PRODUCT_VERSION'                 => $arParams['PRODUCT_VERSION']
        ),
        $component
    );
}?>
</main>



<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
<!--<script src="/test_folder/build/js/main.js"></script>-->