$(function () {
    require(['main'], function () {
        require(['Controls/ComponentEngine', 'tools/tools', 'cookie'], function (ComponentEngine) {
            function CityDropdown(container) {
                this.container = container;
                this.options = this.prepareOptions(this.container.data());
                this.containerHref = $(container).find(".citymenu a");
                this.containerTitle = this.container.prev("div.html_title");
                this.componentEngine = new ComponentEngine();
                this.active_flag = false;
                this.aliase_city = "CITY_CODE";
                this.pathUrl = window.location.pathname;
                this.smartRedirectClass = 'j-city-smart';
                this.init();
            }

            CityDropdown.prototype.prepareOptions = function (obData) {
                if (typeof obData == 'object') {
                    var jsonExp = new RegExp("^[\[\{\'\"]");
                    for (var i in obData) {
                        var item = obData[i];
                        if (typeof item == 'string' && jsonExp.test(item)) {
                            obData[i] = dataString2Json(item);
                        }
                    }
                    return obData;
                }
            };

            CityDropdown.prototype.init = function () {
                this.SetPath();
                this.CompileLinks();
                var o = this;
                //if(this.options.smartRedirect == 'Y' && this.options.isConfirmDropdown != 'Y') {
	                this.containerHref.click(function(){
	                	var _this = $(this);
	                	var cookiePrefix = 'BITRIX_SM_';
	                	if(o.options.smartRedirect == 'Y' && o.options.isConfirmDropdown != 'Y') {
					        setCookie(cookiePrefix + 'SMART_CHANGE_CITY', _this.attr("item-id"), null, '/');
					        window.location.reload();
	                	}
	                	if(o.options.linkBaseurl == 'Y' && o.options.sefMode == 'N') {
	                		var valueSmartChangeCity = 0;
	                		if(_this.data('smart-city') != '' || o.container.find('a[data-smart-city="'+_this.attr("item-id")+'"]').length > 0) {
	                			valueSmartChangeCity = _this.attr("item-id");
	                		}
	                		var firstDot = window.location.hostname.indexOf('.');
							domainForCook = window.location.hostname.substring(firstDot == -1 ? 0 : firstDot + 1);
	                		setCookie(cookiePrefix + 'SMART_CHANGE_CITY', valueSmartChangeCity, null, '/', domainForCook);
	                		
	                		var expires = new Date();
        					expires.setDate(expires.getDate() + 3);
	                		setCookie(cookiePrefix + 'SAVE_CHANGE_CITY', _this.attr("item-id"), expires, '/', domainForCook);
	                	}
	                });
	                
                //}
            };

            CityDropdown.prototype.SetPath = function () {
                var o = this;
                var temp = window.location.pathname.split("/");
                if (o.container.find(".city-list-column a[data-code='" + temp[temp.length - 2] + "']").length > 0) {
                    temp = temp.slice(0, temp.length - 2);
                    o.pathUrl = temp.join("/");
                    if (temp[temp.length - 1] != '/')    o.pathUrl += "/";
                }
                //TODO ����� ����� ���������� �� �������.
                if (o.pathUrl.indexOf('/school/education/') >= 0) {
                    o.pathUrl = "/school/education/";
                }
            };
            CityDropdown.prototype.CompileLinks = function () {
                var o = this;
                o.GetAliaseCity();
                o.containerHref.each(function () {
                    o.SetLink($(this));
                });

            };
            CityDropdown.prototype.SetLink = function (el) {
                var href_text = this.CompileLink(el);
                var cssClass = this.CompileCssClass(el);
                el.attr("href", href_text);
                el.addClass(cssClass);
            };
            CityDropdown.prototype.CompileLink = function (el) {
                var o = this;
                var code = el.data('code');
                var countryCode = el.data('country-code');
                var smartCity = parseInt(el.data('smart-city'));
                var codeForSefModeLink = code;
                var subDomain = "";
                var cityCode2Www = o.options['cityCodeToWww'];
                var href = '';
                if (o.options.localChoose == 'Y') {
                    href = 'javascript:void(0)';
                } else {
                    if(smartCity > 0) {
                    	var smartCityElem = o.container.find('a[item-id="'+smartCity+'"]');
                    	subDomain = smartCityElem.data('code');
                    	countryCode = smartCityElem.data('country-code');
                    	code = smartCityElem.data('code');
                    	codeForSefModeLink = el.data('code');
                    }
                	if ($.inArray(code, cityCode2Www) >= 0) {
                        subDomain = "www";
                    }
                    else {
                    		subDomain = code;
                    }
                    if (o.options.linkSubdomain == 'Y') {
                        href = this.GetSubDomainHref(countryCode, subDomain);
                    }
                    if(o.options.linkBaseurl == 'Y') {
                    	if(o.options.sefMode == 'Y' && o.options.sefFolder.length > 0) {
                    		href += this.componentEngine.makePathFromTemplate(o.options.sefFolder + o.options.urlTemplate, o.aliase_city, codeForSefModeLink);
                    	}
                    	else {
                    		var urlPath = window.location.pathname;
                    		if(urlPath.indexOf('/school/education/') >= 0) {
								urlPath = '/school/education/';
							}
                    		href += urlPath;
                    	}
                    }
                }
                return href;
            };
            CityDropdown.prototype.CompileCssClass = function (el) {
            	var o = this;
                var cssClass = '';
                if (o.options.localChoose == 'Y') {
                    cssClass += 'j-new-choose';
                }
                if (o.options.smartRedirect == 'Y') {
                	cssClass += ' '+o.smartRedirectClass;
                }
                return cssClass;
            }
            CityDropdown.prototype.GetSubDomainHref = function (countryCode, subDomain) {
                var o = this;
                var arHost = o.options.regionHost.split(".");

                var baseDomains = o.options['baseDomains'];
                if (baseDomains.hasOwnProperty(countryCode)) {
                    arHost[arHost.length - 1] = baseDomains[countryCode];
                } else {
                    arHost[arHost.length - 1] = "ru";
                }

                var newHost = arHost.join(".");
                newHost = subDomain + "." + newHost;
                newHost = o.getDomainAlias(newHost);
                return location.protocol + "//" + newHost;
            };
            CityDropdown.prototype.GetAliaseCity = function () {
                var o = this;
                var defaultAliase = {
                    CITY_CODE: "CITY_CODE"
                };
                var customAliase = o.options['aliaseVars'];

                if (!o.active_flag && o.options.sefMode == 'Y') {
                    var newAliase = o.componentEngine.MakeComponentVariableAliases(defaultAliase, customAliase);
                    o.aliase_city = newAliase.CITY_CODE;
                    o.active_flag = true;
                }
            };
            CityDropdown.prototype.getDomainAlias = function (host) {
                var o = this;
                var aliasDomain = o.options['domainAlias'];
                if (typeof aliasDomain == 'object') {
                    for (var domain in aliasDomain) {
                        var alias = aliasDomain[domain];
                        if (domain == host) {
                            return alias;
                        }
                    }
                }
                return host;
            };
            
            
            CityDropdown.prototype.GetFormData = function (event) {
            	if (event !== undefined) {
            	 var context = event.contextContainer;
	             	$(".city-list-popup.j-city-list",context).each(function () {
	        	    	if($(this).data("load-city-ajax") == 'N') {
	        	        	new CityDropdown($(this));
	        	        	var o = $(this);
	        	        	new popupCityList($("a[href='#"+o.attr("id")+"']",context));
	        	    	}
	        	    	else {
	        	    		var o = $(this);
	        				$.ajax({
	        	                url: o.data("ajax-file"),
	        	                type: "POST",
	        	                data: "action=city-load&idcitypopup="+$(this).attr("id")+"&ui_prioritet="+o.data("ui-city-prioritet")+"&smart_redirect="+o.data("smart-redirect"),
	        	                dataType: "html",
	        	                success: function (content) {
	        	                	$(".loader-city-img",context).remove();
	        	                	o.find(".content-popup-city",context).append(content);
	        	                },
	        	                error: function () {
	        	                },
	        	                complete: function () {    	                	
	        	                    new CityDropdown(o);
	        	                    $(document).trigger("loadCityCimple",[o]);
	        	                    if ($('.city-list-popup.j-city-list .j-deffered',context).length > 0) {
	        	                    	require(['Controls/DeferredDataMng'], function (DeferredDataMng) {
	        					        	new DeferredDataMng('.city-list-popup.j-city-list .j-deffered',context);
	        	                    	});
	        					    }
	        	                }
	        	            });
	        	    	}
	        	    	});
            	}
            };
   
            
            $(document).on("onMainInit", function (event) {              
                CityDropdown.prototype.GetFormData(event);
            });
            $(document).ready(function (event) {                
                CityDropdown.prototype.GetFormData(event);
            });
            
        })
    });
    
    
});