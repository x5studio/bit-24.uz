<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}

if ($this->StartResultCache())
{
	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFile = array();
	$rsElement = \CIBlockElement::GetList(
		array("SORT" => "ASC"),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y"
		),
		false,
		array("nTopCount"=>4),
		array("ID", "NAME", "CODE", "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_LINK"));
	while ($arElement = $rsElement->GetNext())
	{
		$arElement['LINK'] = $arElement['PROPERTY_LINK_VALUE'];
		$rsFile = \CFile::GetList(array(), array("ID" => $arElement['PREVIEW_PICTURE']));
		while ($arFile = $rsFile->Fetch()) {
			$arElement['PREVIEW_PICTURE'] = "/upload/" . $arFile["SUBDIR"] . "/" . $arFile["FILE_NAME"];
		}
		$arResult["ITEMS"][] = $arElement;
	}

	$this->IncludeComponentTemplate();
}