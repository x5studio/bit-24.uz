/**
 * Created by psbezrukov on 22.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(["Controls/Contact/ContactManagerFilter"], function (ContactManagerFilter) {
        var ContactManagerFilterPool = (function () {

            var instances = [];

            function ConstructSingletone(instance) {

                if (instance == "undefined") {
                    throw new ReferenceError("Attribute instance is not passed to the constructor.")
                }

                if (instances[instance]) {
                    return instances[instance];
                }
                if (this && this.constructor === ConstructSingletone) {
                    instances[instance] = this;
                } else {
                    return new ConstructSingletone();
                }

                ContactManagerFilter.apply(this, arguments);
            }

            extend(ConstructSingletone, ContactManagerFilter);

            return ConstructSingletone;
        }());

        return ContactManagerFilterPool;
    });
}