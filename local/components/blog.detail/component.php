<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use \Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
$arParams["TAG_IBLOCK_ID"] = 1;

$arParams["ELEMENT_CODE"] = trim($arParams["ELEMENT_CODE"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
if (empty($arParams["SECTION_CODE"]) || empty($arParams["ELEMENT_CODE"]))
{
	$this->AbortResultCache();
	if (Loader::includeModule('iblock'))
	{
		\Bitrix\Iblock\Component\Tools::process404(
			""
			, true
			, true
			, true
			, ""
		);
	}
	return;
}

if ($this->StartResultCache())
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$rsElement = CIBlockElement::GetList(
		array(),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"CODE" => $arParams["ELEMENT_CODE"],
			"SECTION_CODE" => $arParams["SECTION_CODE"],
			"SECTION_GLOBAL_ACTIVE" => "Y"
		),
		false,
		array("nTopCount" => 1),
		$arParams['SELECT_PROP'] ? :array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "PREVIEW_PICTURE", "DETAIL_PICTURE", "DETAIL_PAGE_URL",
            "SHOW_COUNTER", "ACTIVE_FROM", "IBLOCK_SECTION_ID", "PROPERTY_FEEDBACK_BTN_TITLE",
            "PROPERTY_FEEDBACK_TOP_TEXT", "PROPERTY_TAG", "PROPERTY_AUTHOR", "PROPERTY_AUTHOR_ROLE",
            "PROPERTY_AUTHOR_IMG", "PROPERTY_SIMILAR_ARTICLES", "PROPERTY_FORM_TITLE",'TAG')
	);
	if ($arResult = $rsElement->GetNext())
	{
		$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arParams["IBLOCK_ID"], $arResult["ID"]);
		$arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();

		$arResult["DISPLAY_ACTIVE_FROM"] = ToLower(FormatDate("d F Y", MakeTimeStamp($arResult["ACTIVE_FROM"])));

		$arFile = array();
		$arFile[] = $arResult["PREVIEW_PICTURE"];
		if ($arResult["DETAIL_PICTURE"])
			$arFile[] = $arResult["DETAIL_PICTURE"];


        if ($arResult["PROPERTY_AUTHOR_IMG_VALUE"]) {
            $arResult["PROPERTY_AUTHOR_IMG_VALUE"] = CFile::GetPath($arResult["PROPERTY_AUTHOR_IMG_VALUE"]);
        }
        if ($arResult["PROPERTY_AUTHOR_IMG_VALUE"])
            $arFile[] = $arResult["PROPERTY_AUTHOR_IMG_VALUE"];
		$arResult["SECTION"] = \CIBlockSection::GetList(
			array(),
			array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ACTIVE" => "Y",
				"CODE" => $arParams["SECTION_CODE"]
			),
			false,
			array("ID", "NAME", "SECTION_PAGE_URL"))->GetNext();

		if (!empty($arResult["PROPERTY_TAG_VALUE"]))
		{
			$rsTag = $rsGallery = CIBlockElement::GetList(
				array("ID" => "ASC"),
				array("IBLOCK_ID" => 1, "ID" => $arResult["PROPERTY_TAG_VALUE"]),
				false,
				false,
				array("ID", "NAME", "CODE")
			);
			while ($arTag = $rsTag->GetNext())
			{
				$arTag["URL"] = $arResult["SECTION"]["SECTION_PAGE_URL"].'?='.$arTag["CODE"];
				$arResult["TAG"][] = $arTag;
			}
		}
        if (!empty($arResult["PROPERTY_SIMILAR_ARTICLES_VALUE"])){
            $rsArticles = $rsGallery = CIBlockElement::GetList(
                array("ID" => "ASC"),
                array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ID" => $arResult["PROPERTY_SIMILAR_ARTICLES_VALUE"]),
                false,
                false,
                array("ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_PICTURE")
            );
            while ($arArticle = $rsArticles->GetNext())
            {
                if (!empty($arArticle['PREVIEW_PICTURE'])){
                    $arArticle['PICTURE'] = CFile::GetFileArray($arArticle['PREVIEW_PICTURE'])['SRC'];
                }
                $arResult["ARTICLES"][] = $arArticle;

            }
        }

		$arResult["IMAGES"] = Helper::getImages($arFile);

		if($arResult["PREVIEW_PICTURE"])
			$arResult["PICTURE"] = $arResult["IMAGES"][$arResult["PREVIEW_PICTURE"]];

	} else
	{
		$this->AbortResultCache();
		if (Loader::includeModule('iblock'))
		{
			\Bitrix\Iblock\Component\Tools::process404(
				""
				, true
				, true
				, true
				, ""
			);
		}
		return;
	}

	$this->SetResultCacheKeys(array("ID", "NAME", "IPROPERTY_VALUES", "PICTURE", "SECTION", "PREVIEW_TEXT", "DETAIL_PAGE_URL", "~PROPERTY_FORM_TITLE_VALUE"));
	$this->IncludeComponentTemplate();
}

if (isset($arResult["ID"]))
{

	$arTitleOptions = null;

	if (CModule::IncludeModule("iblock"))
		CIBlockElement::CounterInc($arResult["ID"]);

	if ($arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"] != "")
		$APPLICATION->SetPageProperty("title", $arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"], $arTitleOptions);
	else
		$APPLICATION->SetPageProperty("title", $arResult["NAME"], $arTitleOptions);

	if ($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != "" && $arParams['H1_AS_NAME'] != 'Y')
		$APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"], $arTitleOptions);
	else
        if($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]){
            $APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"], $arTitleOptions);
        }
        else{
            $APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
        }

	if ($arResult["IPROPERTY_VALUES"]["ELEMENT_META_KEYWORDS"] != "")
		$APPLICATION->SetPageProperty("keywords", $arResult["IPROPERTY_VALUES"]["ELEMENT_META_KEYWORDS"], $arTitleOptions);
	if ($arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"] != "")
		$APPLICATION->SetPageProperty("description", $arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"], $arTitleOptions);

	$asset = Asset::getInstance();
	$asset->addString('<meta property="og:type" content="article" />');
	$asset->addString('<meta property="og:url" content= "https://'.SITE_SERVER_NAME.$arResult["DETAIL_PAGE_URL"].'" />');
	$asset->addString('<meta property="og:title" content="'.$arResult["NAME"].'" />');
	$asset->addString('<meta property="og:description" content="'.strip_tags($arResult["PREVIEW_TEXT"]).'" />');
	if(!empty($arResult["PICTURE"]))
		$asset->addString('<meta property="og:image" content="https://'.SITE_SERVER_NAME.$arResult["PICTURE"].'" />');

	$APPLICATION->AddChainItem($arResult["SECTION"]["NAME"], $arResult["SECTION"]["SECTION_PAGE_URL"]);
	$APPLICATION->AddChainItem($arResult["NAME"], $arResult["DETAIL_PAGE_URL"]);
}
