if (typeof define === "function" && define.amd) {
    define(["Controls/Form/FormValidate",
        "Controls/Form/FormRuleMng",
        "Controls/Form/AbandonedForm",
        'Controls/Form/FormFileMng',
        'tools/tools'], function (FormValidate, obFormRuleMng, AbandonedForm, FormFileMng) {

        function FormMng(p) {
            this.p = p;
            var o = this;

            this.root = $(p.root);
            if (this.root.get(0).tagName == 'FORM') {
                this.Form = o.root
            } else {
                this.Form = $('form', this.root);
            }

            this.saltForm = o.root.data('saltForm');
            if (!o.saltForm) {
                throw o.root.id + ": data-salt-form is empty"
            }

            this.relatedSelects = [];

        }

        FormMng.prototype.initMaskedInput = function () {
            var o = this;

            // add phone mask
            $('input[phone] + label').remove();
            var phoneFields = $("input[phone], input[data-type='PHONE'], input[data-type~='PHONE_NO_REQUIRED']", o.Form);
            if (phoneFields.length > 0) {
              maskedInput(phoneFields);

              if(window.maskVer == "2") {
                phoneFields.parent().append("<span class='maskhelper' data-tooltip='Очистить формат телефoна'></span>");

                $('.maskhelper').off('click').on('click', function () {
                  $(this).siblings('input').unmask();
                  $(this).siblings('input').rules('remove');

                  var placeholderAttr = $(this).siblings('input').attr('placeholder');
            			if (typeof placeholderAttr !== typeof undefined && placeholderAttr !== false) {
            				$(this).siblings('input').removeAttr('placeholder');
            			}

                  $(this).addClass('green');
                  $('.inf-tooltip').hide();
                  $(this).attr('data-tooltip','Очищено');
                  $(this).unbind('click');
              });

      			}
        }
      }

        FormMng.prototype.trimEmail = function () {
            var o = this;

			//убирает пробелы в email
			var emailField = $("input[email], input[data-type='EMAIL'], input[data-type~='EMAIL_NO_REQUIRED']", o.Form);
			$(emailField).on("keyup", function(){
				emailField.val(emailField.val().trim());
				//ставит фокус в конец
				emailField.selectionStart = emailField.val().length;
			})
        }

        FormMng.prototype.initValidate = function () {
            var o = this;
            o.obValidate = new FormValidate({
                root: o.Form,
                obRules: obFormRuleMng,
                obForm: o
            });
            o.obValidate.init();
        }

        FormMng.prototype.initCheckHiddenOffice = function(o) {

        	$(document).on("changeCuselSelect", function (event, selectCity, selectOffice) {
        		var checkBlock = selectOffice.parent().find(".selectOfficeFormHidden");
            	if (checkBlock.length > 0) {
            		selectOffice.hide();
            		 if (checkBlock.parent(".text").prev("label").length > 0) checkBlock.parent(".text").prev("label").hide();
            	}
          	});
          	var linkCkeckOffice =  o.find(".selectOfficeFormHidden a");
        	linkCkeckOffice.on("click",function(e) {

        		$(this).parents(".text").find(".cusel").toggle();
        		return false;
        	});
        }

        FormMng.prototype.initCheckOfficeFromQueue = function () {
            var
                officeInput = $('#userOffice'+this.saltForm, this.root);

            this.officeFromQueue = officeInput.length && (officeInput.val() === 'queue');

        }

        FormMng.prototype.checkOfficeFromQueue = function () {
            if(this.officeFromQueue){
                var
                    officeInput = $('#userOffice'+this.saltForm, this.root);

                if(officeInput.length){
                    var
                        officeId = officeInput.val();

                    if(officeId !== 'queue'){
                        var
                            officeOption = $(this.root).find('.cusel [val="'+officeId+'"]'),
                            msgData = {
                                'officeId' : officeId,
                                'obForm' : this
                            };

                        if(!officeOption.length){
                            officeOption = $(this.root).find('option [value="'+officeId+'"]');
                        };

                        if(officeOption.length) {
                            msgData.officeName = officeOption.html();
                        }else{
                            console.error('не найдено название офиса');
                        };
                        $(document).trigger('OnManualOfficeSelect', msgData);
                    }
                }
            }
        }

        FormMng.prototype.checkFromDocumentFields = function () {
            var
                formCode = this.Form.prop('name'),
                fieldsValuesElements = $('.j-form-from-document-field[data-form-code="'+formCode+'"]'),
                root = this.root
            ;

            fieldsValuesElements.each(function (i, domEl) {
                var
                    el = $(domEl),
                    field = root.find('[data-code="'+el.attr('data-field-code')+'"]'),
                    value;

                if(el.prop('tagName') === 'INPUT')
                {
                    value = el.val();
                }else{
                    value = el.html()
                }

                field.val(value);
            })
        }

        FormMng.prototype.onSubmit = function(){
            this.checkOfficeFromQueue();
            this.checkFromDocumentFields();
        }

        FormMng.prototype.afterLoad = function(btn)
        {
            // для совместимости.
            // раньше при необходимости взять данные с формы
            // в объект конкретной формы записывалась функция вместо этой
            // и при открытие формы выполняля нужные действия по заполнению формы
        }

        FormMng.prototype.init = function()
        {
            var o = this;
        	var abandonFormFlag=true;
        	if($("body").data("abandon-form")=='N') {
        		abandonFormFlag = false;
        	}
        	if(abandonFormFlag) {
	        	o.abandon=new AbandonedForm(o.Form,["NAME","TEXT","SIMPLE_QUESTION_965"]);
	    		o.abandon.init();
        	}

            if(o.root.find('.j-file-custom').length > 0){
                $('.j-file-custom').each(function () {
                    var item = $(this);
                    var obFormFile = new FormFileMng({
                        root: item,
                        btn: '.file_upload'
                    });
                    obFormFile.init();
                });
            }
            FormMng.prototype.initCheckHiddenOffice(o.root);

            // add title page in hide input form
            title = $('title').html();
            $TitleInputs = o.Form.find('[data-code="PAGE_TITLE"]');
            if ($TitleInputs.length > 0) {
                $TitleInputs.val(title)
            }

            // hide antispam input field
            $antispam = o.Form.find('[data-type="FANTOME"]');
            if ($antispam.length > 0) {
                $antispam.parent().closest('div').hide();
            }
            // ================================

            this.initCheckOfficeFromQueue();
        }

        return FormMng;
    });
}
