if (typeof define === "function" && define.amd) {
    define(['flexslider'], function () {
        function SliderPhotoDescription(container) {
            var _this = this;
            this.container = $(container);

            $(this.container).flexslider({
                pauseOnHover: true,
                animation: "slide",
                useCSS: false,
                controlNav: false,
                directionNav: true,
                slideshow: false,
                start: function (slider) {
                    slider.find(".flex-direction-nav").append("<li class='photo-description inline-block'>" + slider.find("ul.slides li:first-child img").attr("alt") + "</li>");
                },
                before: function (slider) {
                    $(".photo-description").animate({opacity: 0});
                },
                after: function (slider) {
                    $(".photo-description").text($("ul.slides li:eq(" + slider.currentSlide + ") img").attr("alt"));
                    $(".photo-description").animate({opacity: 1});
                }
            });
        }

        return SliderPhotoDescription;
    });
}