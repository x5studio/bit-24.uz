if (typeof define === "function" && define.amd) {
    define(['jquery', 'tools/tools'], function ($) {

        function InlinePaging(pageMng, prop) {
            this.root = prop.root;
            this.activePageItems = 0;

            this.setPageMng(pageMng);
        }

        InlinePaging.prototype.getPageMng = function () {
            return this.pageMng;
        };

        InlinePaging.prototype.setPageMng = function (pageMng) {
            this.pageMng = pageMng;
        };

        InlinePaging.prototype.changeListener = function (item) {

            var self = this;
            var render = false;

            if (self.activePageItems === undefined) {
                self.activePageItems = item;
                render = true;
            } else if (self.activePageItems !== item) {
                self.activePageItems = item;
                render = true;
            }

            if (render) {
                self.triggerEvent(
                    'renderPage',
                    [self.activePageItems]
                );
            }
        };

        InlinePaging.prototype.renderList = function () {
            var self = this;
            var htmlPagenation = '';
            var item = 0;

            var pageCount = parseInt(self.root.attr('data-pagecount'));
            var endPage = parseInt(self.root.attr('data-endpage'));
            var startPage = parseInt(self.root.attr('data-startpage'));
            var arlPath = self.root.attr('data-url-path');

            var numPrev = self.activePage - 1;
            var pagePrev = 'page-' + numPrev + '/';

            if (numPrev === 1) {
                pagePrev = '';
            }

            if (self.activePage > 1 && self.activePage <= 3) {
                htmlPagenation += '<li><a class="page-prev" href="'+ arlPath + pagePrev + '"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>';
                htmlPagenation += '<li><a href="'+arlPath+'">1</a></li>';
            } else if (self.activePage > 3) {
                htmlPagenation += '<li><a class="page-prev" href="'+ arlPath + pagePrev + '"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>';
                htmlPagenation += '<li><a href="'+arlPath+'">1</a></li>';
                htmlPagenation += '<li><span>...</span></li>';
            }

            for (item = startPage + 1; item <= endPage + 1; item++) {
                if (item > pageCount) {
                    break;
                }

                var classActive = '';
                var valuePage = '<a href="'+arlPath+'page-' + item + '/">' + item + '</a>';

                if (item === self.activePage) {
                    classActive = 'class="active"';
                    valuePage = '<span>' + item + '</span>';
                }

                htmlPagenation += '<li ' + classActive + '>' + valuePage + '</li>';
            }

            if ((pageCount - self.activePage) > 2 && item) {
                htmlPagenation += '<li><span>...</span></li>';
                htmlPagenation += '<li><a href="'+arlPath+'page-' + pageCount + '/">'+pageCount+'</a></li>';
                htmlPagenation += '<li><a class="page-next" href="'+arlPath+'page-' + (self.activePage + 1) + '/"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>';
            } else if (self.activePage < pageCount) {
                htmlPagenation += '<li><a class="page-next" href="'+arlPath+'page-' + (self.activePage + 1) + '/"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>';
            }

            if (!!htmlPagenation) {
                if(self.activePage > 3) {
                    self.root.attr('data-endpage', endPage + 1);
                    self.root.attr('data-startpage', startPage + 1);
                }
                $('ul', self.root).html(htmlPagenation);
            }

        };

        InlinePaging.prototype.initStorage = function () {
            var self = this;
            var pageMng = self.getPageMng();

            pageMng.addListener('update', self.changeListener);
            pageMng.addListener('insert', self.changeListener);

            pageMng.addListener('renderPage', function (param) {
                if (!!param) {
                    self.activePage = param;
                    self.renderList();
                }
            });
        };

        InlinePaging.prototype.init = function () {
            this.initStorage();
        };

        return InlinePaging;
    });
}