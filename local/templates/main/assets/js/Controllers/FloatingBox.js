if (typeof define === "function" && define.amd) {
    define([], function () {
        
        function FloatingBox(container) {
            var _this = this;
            this.container = $(container);
            this.floatingBox = $('.floating-box', container);
            this.containerHeight = this.container.height() || this.floatingBox.height();
            this.initialOffsetTop = _this.floatingBox.offset().top;
            this.document = $(document);
            this.limitBox = $(container).data('limit') ? {
                 container : $($(container).data('limit')),
                 initialOffsetTop : $($(container).data('limit')).offset().top
            } : false;


            this.toggleFloating();

            $(window).on('scroll', function () {
                _this.toggleFloating.apply(_this);
            });
            this.init();
        }

        FloatingBox.prototype.isBoxInactive = function () {
            if (this.container.data('limit') !== undefined) {
                this.limitBox.initialOffsetTop = $(this.container.data('limit')).offset().top;
            }
            var topLimitFlag = this.initialOffsetTop > this.document.scrollTop();

            return !this.limitBox ? 
                    topLimitFlag : 
                    topLimitFlag || (this.document.scrollTop() + this.containerHeight >= this.limitBox.initialOffsetTop);
        };

        FloatingBox.prototype.toggleFloating = function () {
            var _this = this;

            if (_this.isBoxInactive()) {
                if (_this.floatingBox.hasClass('floating-box-active')) {
                    _this.floatingBox.removeClass('floating-box-active');
                }
            }
            else {
                if (!_this.floatingBox.hasClass('floating-box-active')) {
                    _this.floatingBox.addClass('floating-box-active');
                }
            }
        };

        FloatingBox.prototype.init = function () {
            var _this = this,
                fBoxHeight = _this.floatingBox.height();

            _this.container.height(fBoxHeight || _this.containerHeight);

            _this.toggleFloating();
        };

        return FloatingBox;
    });
}