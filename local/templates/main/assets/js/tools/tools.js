if (typeof extend != 'function') {
    function extend(Child, Parent) {
        var F = function () {
        };
        F.prototype = Parent.prototype;
        Child.prototype = new F();
        Child.prototype.constructor = Child;
        Child.superclass = Parent.prototype;
    }
}

if (typeof getInitial != 'function') {
    function getInitial(el, defName, eventTrigger, dataTrigger) {
        if (!(el instanceof jQuery)) {
            throw new Error("Invalid argument - is not jquery object");
        }
        if (typeof defName == 'undefined') {
            defName = 'initDeferred';
        }
        var defer = el.data(defName);
        if (!defer) {
            defer = $.Deferred();
            el.data(defName, defer);
        }
        el.on(eventTrigger, function () {
            defer.resolve();
        });
        if (el.data(dataTrigger)) {
            defer.resolve();
        }
        return defer;
    }
}
if (typeof lazyLoadReceived != 'function') {
    function lazyLoadReceived(fn, context) {
        if ($('.j-lazy-load').length > 0) {
            $(document).on("lazyLoadReceived", function () {
                return fn.apply(context || window);
            });
        } else {
            return fn.apply(context || window);
        }
    }
}

/**
 * Падежи для числа
 * Пример: 'Ещё ' + declOfNum(CNT, ['офис', 'офиса', 'офисов']);
 *
 *
 *
 * @param number
 * @param titles
 *
 * @return string
 */
function declOfNum(number, titles) {
    var cases = [2, 0, 1, 1, 1, 2];
    return '' + number + " " + titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[Math.min(number % 10, 5)]];
}

function ucwords(str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}

function array_chunk(input, size) {
    for (var x, i = 0, c = -1, l = input.length, n = []; i < l; i++) {
        (x = i % size) ? n[c][x] = input[i] : n[++c] = [input[i]];
    }
    return n;
}

/**
 * @requires - завязка на гео модуль ($geo->getRegionalMask), нужно добавить в тег body ата аттрибут phone-mask
 * @param input
 * @param par
 */

function maskedInput(input, par) {
    if (!window.disableMask) {
        // add phone mask
        if (input.length > 0) {
            require(['jquery', "maskedinput"], function ($) {
                if (window.maskVer == "2.1") {
                    applyMaskExtended(input, par);
                } else {
                    applyMaskSimple(input, par);
                }
            });
        }
    }
}

function applyMaskSimple(input, par) {

    if (typeof $.mask.definitions['d'] != 'string') {
        $.mask.definitions['d'] = '[0-9]';
        delete $.mask.definitions['9'];
    }

    if ($('body').data('phone-mask')) {
        endMask = $('body').data('phone-mask');
    } else {
        var arHost = location.host.split('.');
        var subDomain = arHost[0];
        var baseDomain = arHost[arHost.length - 1];

        if (subDomain == 'dubai' || $('body').data('countryCode') == 'uae') {
            endMask = "+971 ddddddddd";
        } else if (subDomain == 'montreal' || subDomain == 'toronto' || $('body').data('countryCode') == 'canada') {
            endMask = "+1 ddd ddd - dddd";
        } else if (baseDomain == 'ua' || $('body').data('countryCode') == 'ua') {
            endMask = "+38 (ddd) ddd-dddd";
        } else {
            endMask = '+7 (ddd) ddd-dddd';
        }
    }
    input.mask(endMask, par);
}

function applyMaskExtended(input, par) {
    function setCurrentCursorPosition(inp, pos) {
        if ($(inp).get(0).setSelectionRange) {
            $(inp).get(0).setSelectionRange(pos, pos);
        } else if ($(inp).get(0).createTextRange) {
            var range = $(inp).get(0).createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    }

    var arHost = location.host.split('.');
    var subDomain = arHost[0];
    var baseDomain = arHost[arHost.length - 1];

    if ($('body').data('phone-mask')) {
        var endMask = $('body').data('phone-mask');
        var prefix = endMask.substring(0, endMask.indexOf("d"));

    } else {
        if (subDomain == 'dubai' || $('body').data('countryCode') == 'uae') {
            var endMask = "+971 ddddddddd";
            var prefix = endMask.substring(0, endMask.indexOf("d"));
        } else if (subDomain == 'montreal' || subDomain == 'toronto' || $('body').data('countryCode') == 'canada') {
            var endMask = "+1 ddd ddd - dddd";
            var prefix = endMask.substring(0, endMask.indexOf("d"));
        } else if (baseDomain == 'ua' || $('body').data('countryCode') == 'ua') {
            var endMask = "+38 (ddd) ddd-dddd";
            var prefix = endMask.substring(0, endMask.indexOf("d"));
        } else if (baseDomain == 'kz' || $('body').data('countryCode') == 'kazakhstan') {
            var endMask = "+7 (ddd) ddd-dddd";
            var prefix = endMask.substring(0, endMask.indexOf("d"));

        }
        else {
            var endMask = '+7 (ddd) ddd-dddd';
            var prefix = endMask.substring(0, endMask.indexOf("d"));
        }
    }

    input.parent().append("<span class='maskhelper' title='Очистить формат телефoна'></span>");

    require(['tooltipster'], function () {
        input.parent().find('.maskhelper').tooltipster({
            theme: ['tooltipster-light', 'tooltipster-light-customized'],
            contentCloning: true,
            maxWidth: 250,
            functionInit: function (origin, content) {
                origin._$origin.parents('form').on('mouseleave', function (e) {
                    if ($('.tooltipstered').length) $('.maskhelper').tooltipster('close');
                });
            },
            functionReady: function(instance, helper){
                document.onscroll = function (e) {
                    if ($('.tooltipstered').length) $('.maskhelper').tooltipster('close');
                };

                document.ontouchmove = function (e) {
                    if ($('.tooltipstered').length) $('.maskhelper').tooltipster('close');
                };
            }
        });

        input.parent().find('.maskhelper').on('click', function(){
            $(this).prev('input').val('');
            $(this).prev('input').blur();
            $(this).prev('input').unmask();
            $(this).prev('input').removeAttr('data-type', 'phone');
            $(this).prev('input').rules('remove');
            $(this).addClass('cleared');
            m = $(this);

            var extendedMaskBehaviour = function (val) {
                    return 'dddddddddddddddd';
                },
                extendedOptions = { //здесь указываем опции маски
                    placeholder: prefix,
                    translation : {
                        d: { pattern: /[0-9a-zA-Z+\-_\(\)]/ },
                    },
                    onKeyPress: function(val, e, field, options) {
                        m.tooltipster('close');
                        if(val.replace(/\D/g, '').length > '11') {
                            m.tooltipster('content', 'Вы пытаетесь ввести больше 11 символов, проверьте номер телефона или введите в произвольном формате');
                            m.tooltipster('open');
                        }else{
                            m.tooltipster('content', 'Очистить');
                        }
                        field.mask(extendedMaskBehaviour.apply({}, arguments), options);
                    }
                };
            $(this).prev('input').mask(extendedMaskBehaviour, extendedOptions);

            var placeholderAttr = $(this).siblings('input').attr('placeholder');
            if (typeof placeholderAttr !== typeof undefined && placeholderAttr !== false) {
                $(this).siblings('input').removeAttr('placeholder');
            }
            $(this).siblings('input').attr("placeholder", "Телефон");

            $(this).tooltipster('content', 'Очищено');
            $(this).tooltipster('open');
        });
    });


    var valueMaxLength = endMask.replace(/[-\/\\^$*+?.()|[\]{}\s]/g, '');//Убирает все лишние масочные символы, оставляет только префикс и символы маски
    var maskStartPosition = Number(prefix.length);
    var endMaskWhide = endMask + 'dd';

    var extendedMaskBehaviour = function (val) { //Расширенная маска подключается ко всем, кроме Эмиратов
        if (subDomain == 'dubai' || $('body').data('countryCode') == 'uae') {
            return endMask;
        }
        return (val.replace(/\D/g, '').length < valueMaxLength.length) ? endMask : endMaskWhide; //Здесь подменяем маску
    },
        extendedOptions = { //здесь указываем опции маски
            placeholder: endMask.replace(/d/g, "_"),
            translation: {
                d: { pattern: /[0-9]/ },
            },
            onKeyPress: function (val, e, field, options) {//Здесь обрабатываем вывод
                $(field).next('.maskhelper').tooltipster('close');
                if (val.replace(/\D/g, '').length > valueMaxLength.length) {
                    $(field).next('.maskhelper').tooltipster('content', 'Вы пытаетесь ввести больше ' + valueMaxLength.length + ' символов, проверьте номер телефона или введите в произвольном формате');
                    $(field).next('.maskhelper').tooltipster('open');
                }else{
                $(field).next('.maskhelper').tooltipster('content', 'Очистить');
            }
                if (val.length <= prefix.length) {
                    $(field).val(prefix);
                }
                maskStartPosition = val.length;
                field.mask(extendedMaskBehaviour.apply({}, arguments), options);
            }
        };

    input.mask(extendedMaskBehaviour, extendedOptions);

    // Ставим префикс при фокусе на поле
    // Убираем префикс вне фокуса при пустом поле
    input.on("blur", function () {
        if ($(this).val().length <= prefix.length) {
            $(this).val("");
        }
    });

    // Убираем префикс вне фокуса при пустом поле
    input.on("click", function (event) {
        var isCleared = (input.siblings('.maskhelper').is('.cleared')) ? true : false;
        if (!isCleared) {
            if (!$(this).val().length) {
                $(this).val(prefix);
            }
        } else {
            $(this).val("");
        }

        maskStartPosition = $(this).val().length;
        setCurrentCursorPosition($(this), maskStartPosition);
    });

    $(document).on('click', '.maskhelper', function (event) {
        input.unmask();
        input.val("");
    })
}


/**
 * подготавливаем строку для конвертации в регулярку
 * @param string like phone number +973-dddd-dddd
 * @returns {void | string | never | *}
 */
RegExp.escape = function (string) {
    string = string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    return string.replace(new RegExp('d', 'g'), '\\d');
};
/**
 * тултипсы, срабатывают для элементов у которых есть дата аттрибут tooltip
 * используется например для подсказки маски телефона в формах
 */
var showingTooltip;

document.onmouseover = function (e) {
    var target = e.target;

    var tooltip = target.getAttribute('data-tooltip');
    if (!tooltip) return;

    var tooltipElem = document.createElement('div');

    if (target.className != 'maskhelper green') {
        tooltipElem.className = 'inf-tooltip';
    } else {
        tooltipElem.className = 'inf-tooltip green';
    }

    tooltipElem.innerHTML = tooltip;
    document.body.appendChild(tooltipElem);

    var coords = target.getBoundingClientRect();

    var left = coords.left + (target.offsetWidth - tooltipElem.offsetWidth) / 2;
    if (left < 0) left = 0; // не вылезать за левую границу окна

    var top = coords.top - tooltipElem.offsetHeight - 5;
    if (top < 0) { // не вылезать за верхнюю границу окна
        top = coords.top + target.offsetHeight + 5;
    }

    tooltipElem.style.left = left + 'px';
    tooltipElem.style.top = top + 'px';

    showingTooltip = tooltipElem;
};

document.onmouseout = function (e) {
    hideTooltip();
};

document.onscroll = function (e) {
    hideTooltip();
};

document.ontouchmove = function (e) {
    hideTooltip();
};

/*end tooltips*/
function hideTooltip() {
    if (showingTooltip) {
        document.body.removeChild(showingTooltip);
        showingTooltip = null;
    }
}

function dataString2Json(data) {
    if (typeof data == 'string') {
        return BX.parseJSON(data);
    }
    return data;
}

function count(obj) {
    var cnt = 0;
    for (var i in obj) {
        if (obj.hasOwnProperty(i)) {
            cnt++;
        }
    }
    return cnt;
}

function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
        selector = $this.attr('href')
        selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
}

function findParentClass(obj, className) {
    if (obj.constructor.name == className) {
        return obj;
    }
    else {
        return findParentClass(obj.constructor.superclass, className);
    }
}

function findItem(items, id, callable) {
    for (var i in items) {
        var item = items[i];
        if (typeof callable == 'function' && callable(item, id) || item['ID'] == id) {
            return item;
        }
    }
    return false;
}

function deleteItem(items, id, callable) {
    for (var i in items) {
        var obItem = items[i];
        if (typeof callable == 'function' && callable(obItem, id) || obItem['ID'] == id) {
            return items.splice(i, 1)[0];
        }
    }
}

if (typeof remove != 'function') {
    function remove(arr, item, callable) {
        for (var i = arr.length; i--;) {
            if (typeof callable == 'function' && callable(arr[i], i) || arr[i] === item) {
                arr.splice(i, 1);
            }
        }
    }
}

function deepEqual(a, b) {
    if (a === b) {
        return true;
    }

    if (a == null || typeof (a) != "object" ||
        b == null || typeof (b) != "object") {
        return false;
    }

    var propertiesInA = 0, propertiesInB = 0;
    for (var property in a) {
        propertiesInA += 1;
    }
    for (var property in b) {
        propertiesInB += 1;
        if (!(property in a) || !deepEqual(a[property], b[property])) {
            return false;
        }
    }
    return propertiesInA == propertiesInB;
}

function dayDiff(first, second) {
    return (second - first) / (1000 * 60 * 60 * 24);
}

/**
 * Форматирование размера файла
 *
 * @param bytes
 *
 * @return string
 */
function byteConvert(bytes) {
    var symbol = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var exp = Math.floor(Math.log(bytes) / Math.log(1024));
    var result = (bytes / Math.pow(1024, exp)).toFixed(2);
    var resultStr = result + ' ' + symbol[exp];

    return resultStr;
}
if (typeof window.track != 'function') {
    window.track = function (p) {
        return true;
    }
}
if (typeof window.trackStep != 'function') {
    window.trackStep = function (path) {
        return true;
    }
}

/**
 * Форматирование размера файла
 *
 * @param parameter, defaultvalue
 *
 * @return string
 */
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function getUrlParam(parameter, defaultvalue) {
    var urlparameter = defaultvalue;
    if (window.location.href.indexOf(parameter) > -1) {
        urlparameter = getUrlVars()[parameter];
    }
    return urlparameter;
}
