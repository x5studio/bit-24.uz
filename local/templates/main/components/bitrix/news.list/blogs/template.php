<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?
$blogs = [];
$i = 1;
foreach ($arResult["SECTIONS"] as $arSection) {
    $blog = [];
    $j = 0;
    foreach ($arSection["ITEMS"] as $arItem) {
        $blog[] = [
            'href' => $arItem["DETAIL_PAGE_URL"],
            'main' => ($j%6==0 ? 'yes' : 'no'),
            'title' => $arItem["NAME"],
            'src' => $arItem["PREVIEW_PICTURE"]["SRC"],
            'srcset' => $arItem["PREVIEW_PICTURE"]["SRC"],
            'width' => $arItem["PREVIEW_PICTURE"]["WIDTH"],
            'height' => $arItem["PREVIEW_PICTURE"]["HEIGHT"],
            'alt' => $arItem["PREVIEW_PICTURE"]["ALT"],
        ];
        $j++;
    }
    $blogs['blog'.$i] = $blog;
    $i++;
}

echo json_encode($blogs);
?>

