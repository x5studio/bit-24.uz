<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult['SECTION']['PATH']) && count($arResult["ITEMS"])):?>
    <section class="opportunities">
        <div class="container">
            <div class="row tabs-wrapper">
                <div class="col-md-5">
                    <h2 class="why-heading"><?=reset($arResult['SECTION']['PATH'])['NAME']?></h2>
                    <ul class="tabs">
                        <?$i = 0;?>
                        <?foreach($arResult["ITEMS"] as $arItem):?>
                            <?
                            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                            ?>
                            <li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="tab"><span><?=str_pad($i+1, 2, '0', STR_PAD_LEFT)?>.</span><?=$arItem["NAME"];?></li>
                            <?$i++?>
                        <?endforeach;?>
                    </ul>
                </div>
                <div class="col-md-7">
                    <div class="tab-outer">
                        <div class="tab-content">
                            <?$i = 0;?>
                            <?foreach($arResult["ITEMS"] as $arItem):?>
                                <div class="tab-item" style="<?=($i==0 ? '' : 'display:none;')?>">
                                    <div class="card">
                                        <div class="card-num"><?=str_pad($i+1, 2, '0', STR_PAD_LEFT)?>.</div>
                                        <div class="card-heading"><?=$arItem["NAME"];?></div>
                                        <div class="desc"><?=$arItem["PREVIEW_TEXT"];?></div>
                                    </div>
                                </div>
                                <?$i++?>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?endif;?>

