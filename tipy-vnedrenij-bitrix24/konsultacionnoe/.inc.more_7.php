<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>



<div class="modal-header"><i class="fa fa-times" style="float: right;" data-dismiss="modal" aria-hidden="true"></i>
	<h4>Директор</h4>
</div>
<div class="modal-body">
	<div class="material">
		<div class="questions">
			<h3>Для кого?</h3>
			<p>Для руководителей всех уровней.</p>
		</div>
		<div class="questions">
			<h3>Что делаем?</h3>
			<p>Данный формат обучения носит индивидуальный характер. Эта услуга для клиентов, которые проходили у нас этап аналитики, постановки и настройки бизнес-процессов. Обучение проводится по ролям пользователей и включает в себя теоретическую и практическую части, исходя из бизнес-логики работы каждого отдельного клиента.

Предшествующий обучению этап аналитики, позволяет нашим специалистам дать локальные советы по работе с системой, "за руку" провести клиента по его бизнес-процессам, настроенным на Битрикс24, а так же сформировать для Ваших сотрудников бизнес-задачки для оттачивания практических навыков работы с учётом специфики Вашего бизнеса прямо во время обучения.
			</p></div>
		<div class="questions">
			<h3>Результаты</h3>
			<p>Приобретаемые навыки и инструменты, которыми мы научим Вас пользоваться: Работа со структурой компании, Настройка прав доступа CRM, Настройка пользовательских полей CRM, Настройка стадий работ с лидами, сделками, счетами, коммерческими предложениями, Функция автоматического создания лидов с вашего общего E-mail и формы на сайте, Собрания и планёрки, Автоматизация регулярных отчётов сотрудников, Добавление/увольнение сотрудников.

</p>
		</div>
		<div class="service-bottom">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Формат:<img src="/local/templates/main/assets/img/service/com.png"
													class="img-responsive"></h3>
					<p>ВСТРЕЧА/SKYPE</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Стоимость<img src="/local/templates/main/assets/img/service/wallet.png"
														class="img-responsive"></h3>
					<h4>от 9 960 р.</h4>
				</div>
			</div>
			<div class="buy">
				<button type="button" class="btn btn-buy" data-target="#advanced"
								data-form-field-type="Консультационное внедрение: Директор"
								data-toggle="modal" data-dismiss="modal"><a href="#" title="link">Купить</a></button>
			</div>
		</div>
	</div>
</div>