if (typeof define === "function" && define.amd) {
    define(["Controls/Responsive"], function (obResponsive) {
        function Phone(item) {
            this.item = $(item);
            var phone = this.item.text();
            this.icon = this.item.find('i');
            this.mobile = 'tel:' + phone.replace(/[^+0-9]/g, '');
            this.skype = 'skype:' + phone + '?call';
            this.resizes = (typeof (this.item.data('phoneResize')) !== 'undefined') ? this.item.data('phoneResize') : "xsmall";
            this.init();
        }

        Phone.prototype.getPhoneResize = function () {
            return this.resizes.split(' ');
        };
        Phone.prototype.SwitchPhone = function () {
            this.item.wrapAll('<a href="#" class="a-phone"></a>');
            this.item.parent().attr('href', this.mobile);
        };
        Phone.prototype.SwitchSkype = function () {
            this.item.parent().attr('href', this.skype);
            this.icon.removeClass('fa-phone');
            this.icon.addClass('fa-skype');
        };
        Phone.prototype.unwrapLink = function () {
            if (this.item.parent().hasClass('a-phone')) {
                this.item.unwrap();
            }
        };

        Phone.prototype.init = function () {
            var _this = this;
            obResponsive.on(['xsmall', 'small', 'medium', 'large'], 'in', function () {
                var phoneResizes = _this.getPhoneResize();
                for (var i in phoneResizes) {
                    if (phoneResizes[i] == obResponsive.getCurType()) {
                        _this.SwitchPhone();
                    }
                    else {
                        _this.unwrapLink();
                    }
                }
            });
        };

        return Phone;
    });
}
