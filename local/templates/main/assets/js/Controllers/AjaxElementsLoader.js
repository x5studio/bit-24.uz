if (typeof define === "function" && define.amd) {
    define([], function () {
        function AjaxElementsLoader(p)
        {
            var o = this;
            this.root = $(p.root);
            this.controlsBox = $(p.controlsBox).filter('[data-navnum="' + this.root.data('navnum') + '"]');
            this.elementsShower = $(p.elementsShower, this.controlsBox);
            this.elementsHider = $(p.elementsHider, this.controlsBox);
            this.elementsShowerHtml = this.elementsShower.html();
            this.processImg = $(p.processImg, this.controlsBox);

            this.showCount = o.controlsBox.data('showCount');

            this.elLoadedCSS = p.elLoadedCSS;

            this.loadSett = {
                endPage : o.controlsBox.data('endpage'),
                navNum : o.controlsBox.data('navnum'),
                curPage : o.controlsBox.data('curpage'),
                navRecordCount : o.controlsBox.data('navRecordCount'),
                navPageSize : o.controlsBox.data('navPageSize')
            };

            this.curPage = this.loadSett.curPage||1;

            this.resetPagination = function(){
                o.curPage = this.loadSett.curPage||1;

                if(o.root.data('qpages') == 1){
                    o.controlsBox.hide();
                }

                if(o.root.data('qpages') > 1){
                    o.loadSett.endPage = o.root.data('qpages');
                    o.controlsBox.show();
                }
            };

            this.loadMoreElements = function(){

                o.states.setState('loading');

                var loadUrl = location.pathname + location.search;
                loadUrl = loadUrl.replace('page-'+ o.loadSett.curPage +'/', '');

                if(location.search != ''){
                    loadUrl += '&';
                }
                else{
                    loadUrl += '?';
                }
                loadUrl  += 'PAGEN_'+ o.loadSett.navNum +'=' + (++o.curPage);

                var ajaxData = {AJAX: 'Y'};
                var dataType = 'html';

                if(typeof o.root.data('filter') == 'object'){

                    $.extend(ajaxData, o.root.data('filter'));

                    dataType = 'json';
                }


                $.ajax({
                    url: loadUrl,
                    type: "POST",
                    dataType: dataType,
                    data: ajaxData
                })
                    .success(function(html){

                        if(dataType == 'html'){
                            o.root.append(html);
                        }
                        else{
                            o.root.append(html.html);
                        }

                        o.root.trigger('updateList', {page:o.curPage});

                        if(o.curPage < o.loadSett.endPage){
                            o.states.setState('normal');
                        }
                        else{
                            o.states.setState('closing');
                        }
                    });
            };

            this.init = function(){
                o.elementsShower.click(function(event){

                    o.loadMoreElements();
                    event.preventDefault();
                    return false;
                });

                o.elementsHider.click(function(event){
                    o.root.find(o.elLoadedCSS).remove();
                    o.curPage = 1;
                    o.states.setState('normal');
                    event.preventDefault();
                    return false;
                });

                o.root.on('resetPagination', o.resetPagination);

            };

            this.showRemainCount = function()
            {
                var remainCount = o.loadSett.navRecordCount - o.loadSett.navPageSize * o.curPage;
                if(remainCount > 0)
                {
                    var countElem = !!this.showCount ? ' (' + remainCount + ')' : '';
                    var replaceShowerText = o.elementsShowerHtml + countElem;
                }
                else
                {
                    var replaceShowerText = o.elementsShowerHtml;
                }
                o.elementsShower.html(replaceShowerText);
            };

            require(['Controls/ManagerState'], function (ManagerState) {
                o.states = new ManagerState({
                    root: o.root,
                    state:{
                        init: function(){
                            o.showRemainCount();
                            this.elementsShower = o.elementsShower;
                            this.processImg = o.processImg;
                            this.elementsHider = o.elementsHider;
                        },
                        normal: function(){
                            o.showRemainCount();
                            this.elementsShower.removeClass('hidden');
                            this.processImg.addClass('hidden');
                            this.elementsHider.addClass('hidden');
                        },
                        loading: function(){
                            this.elementsShower.addClass('hidden');
                            this.processImg.removeClass('hidden');
                            this.elementsHider.addClass('hidden');
                        },
                        closing: function(){
                            this.elementsShower.addClass('hidden');
                            this.processImg.addClass('hidden');
                            this.elementsHider.removeClass('hidden');
                        }
                    }
                });
            });

        }
        return AjaxElementsLoader;
    });
}