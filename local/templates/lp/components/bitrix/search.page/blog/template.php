<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<div class="main-blog">
	<div class="content">
		<? if ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false): ?>
		<? elseif ($arResult["ERROR_CODE"] != 0): ?>
			<p><?= GetMessage("CT_BSP_ERROR") ?></p>
			<? ShowError($arResult["ERROR_TEXT"]); ?>
			<p><?= GetMessage("CT_BSP_CORRECT_AND_CONTINUE") ?></p>
		<? elseif (count($arResult["SEARCH"]) > 0): ?>
			<div class="row">
				<? foreach ($arResult["SEARCH"] as $cell => $arSearch):
				$arItem = $arResult["ITEMS"][$arSearch["ITEM_ID"]]
				?>
				<? if ($cell > 0 && $cell % 2 == 0): ?></div>
					<div class="row"><? endif; ?>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="blog1 pull-right">
						<div class="b-img effect">
							<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive" alt="<?=
							$arItem["NAME"]
							?>"/>
							<div class="mask">
								<a href="<? echo $arSearch["URL"] ?>" class="info"></a>
							</div>
						</div>
						<h4 class="text-left"><?= $arItem["NAME"] ?></h4>
						<div class="row row-text vertical-align">
							<div class="col-md-6 col-sm-6 col-xs-6 text text-left">
								<h5><a href="<?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["SECTION_PAGE_URL"] ?>"
									><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></a></h5>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-6 date text-right">
							<span class="date"><i class="fa fa-clock-o"
																		aria-hidden="true"></i><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
							</div>
						</div>
						<p><?= $arItem["PREVIEW_TEXT"] ?></p>
						<a href="<? echo $arSearch["URL"] ?>" title="<?= $arItem["NAME"] ?>">Подробнее</a>
					</div>
				</div>
			<? endforeach; ?>
			</div>
			<? if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N") echo $arResult["NAV_STRING"] ?>
		<? else: ?>
			<? ShowNote(GetMessage("CT_BSP_NOTHING_TO_FOUND")); ?>
		<? endif; ?>
	</div>
</div>