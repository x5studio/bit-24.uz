<?php

//Теги

$arFilter = [
    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
    'ACTIVE' => 'Y'
];
$rsSection = CIBlockSection::GetList(
    ['SORT' => 'ASC'],
    $arFilter,
    false,
    ['*'],
    false
);
$arSections = [];
while ($arSection = $rsSection->GetNext(true, false)) {
    $arSections[$arSection['ID']] = $arSection;
    if ($arResult['SECTION']['SECTION_PAGE_URL'] == $arSection['SECTION_PAGE_URL']){
        $arSections[$arSection['ID']]['CURRENT'] = 'Y';
    }
}
$arResult['SECTIONS'] = $arSections;



/*Парсер текста*/
/*
 * aside format: ##id:977##
 * */

$pattern = "!##id:(\d+)##!si";
$matches = array();
preg_match_all($pattern, $arResult["DETAIL_TEXT"], $matches, PREG_SET_ORDER);
$arItemsIds = [];
$arMatches = [];
foreach ($matches as $match){
    if (intval($match[1]) > 0 ){
        $arItemsIds[] = $match[1];
        $arMatches[intval($match[1])] = $match[0];
    }
}

$arFilter['ID'] = $arItemsIds;
$rsItems = CIBlockElement::GetList(
    ["SORT"=>"ASC"],
    $arFilter,
    false,
    false,
    ['ID', 'PREVIEW_PICTURE', 'NAME', 'DETAIL_PAGE_URL']
);
$arItems = [];
while ($arItem = $rsItems->GetNext(true, false)) {
    $arItems[$arItem['ID']] = $arItem;
    $picture = CFile::GetFileArray($arItem['PREVIEW_PICTURE'])['SRC'];
    $html = render($arItem['NAME'], $picture, $arItem['DETAIL_PAGE_URL']);
    $arResult['DETAIL_TEXT'] = str_replace($arMatches[$arItem['ID']], $html, $arResult['DETAIL_TEXT']);
}

function render($name, $image, $url){
    return '<div class="aside">
<a href="'.$url.'">
<img src="'.$image.'" alt="'.$name.'">
<div class="aside_title">
<span>'.$name.'</span>
</div>
</a>
</div>';
}

