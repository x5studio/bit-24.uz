<div class="box-offices box-office-double">
	<!-- Выпадающий список городов -->
	<?
	$NAME = $_SESSION['REGION']['CODE'] == 'nizhniy' ? PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_N_NOVGORODE') : '#NAME_DATIVE#';
	$APPLICATION->IncludeComponent("vitams:city.dropdown", "city_simple",
		Array(
			"IBLOCK_TYPE" => IBT_CONTACTS,
			"IBLOCK_ID" => IB_CONTACTS,
			"SECTION_CODE" => "",
			"SECTION_URL" => "",
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "1",
			"SECTION_FIELDS" => "",
			"FILTER_NAME" => "cityFilter",
			"SECTION_USER_FIELDS" => "",
			"ADD_SECTIONS_CHAIN" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_NOTES" => "",
			"CACHE_GROUPS" => "Y",
			"LINK_IN_BASEURL" => "Y",
			"LINK_WITH_SUBDOMAIN" => "Y",
			"SEF_MODE" => "N",
			"SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
			"HTML_LIST_ID" => "header-city-list-inner",
			"HTML_TITLE" => '<div class="box-title h2"><div class="short">' . \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_CONTACTS') . '</div><div class="full">' . \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_OFFICE_IN') . ' <a href="##HTML_LIST_ID#" class="city j-city"><b>'.$NAME.'</b></a></div></div>',
			"BY_CACHE_SUBDOMAIN_PAGE" => $_SESSION["REGION"]["CODE"],
			"SHOW_MOSCOW" => $_SESSION['SHOW_MOSCOW'],
			"COUNTRY_CODE" => $_SESSION['GEOIP']['country_code'],
			'UI_CITY_STRONG' => $arParams['CITY_DROPDOWN_UI_CITY_STRONG'],
			'UI_CITY_SPACE' => $arParams['CITY_DROPDOWN_UI_CITY_SPACE'],
		)
	);?>
	<div class="box-content<?if ($arParams['IS_STOP_RANDOM'] == 'N') {?> j-sync-offices-items<?}?>">
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
			CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => PB\Main\Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
			<div class="box-offices-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-item-id="<?=$arItem['ID']?>">
				<div class="place float-left"></div><div class="office-name"><?=$arItem['NAME']?></div>
				<span class="strong box-office-phone"><?=$arItem['PROPERTIES']['phone']['VALUE']?></span>
			</div>
		</a>
		<?endforeach;?>
	</div>
</div>