<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
global $APPLICATION;

$cacheId = 'menu_iblock_31';
$cache_dir = SITE_ID . '/bitrix/menu/topmenu';
$obCache = new CPHPCache;

if ($obCache->InitCache(3600, $cacheId, $cache_dir)) {
    $aMenuLinksExt = $obCache->GetVars();
}
elseif ($obCache->StartDataCache()) {
    $aMenuLinksExt = [];

    $siteId = CIBlockPropertyEnum::GetList([], ['IBLOCK_ID' => 31, 'CODE' => 'RELATED_SITE', 'XML_ID' => SITE_ID])->Fetch()['ID'];

    $rsItems = CIBlockElement::GetList(
        ['SORT' => 'ASC'],
        [
            'IBLOCK_ID' => 31,
            'ACTIVE' => 'Y',
            'PROPERTY_RELATED_SITE' => $siteId,
        ],
        false,
        false,
        ['ID', 'NAME', 'CODE', 'DETAIL_PAGE_URL', 'PROPERTY_MENU_ICON', 'PROPERTY_CLASS_ICON']
    );

    while($arItem = $rsItems->GetNext()) {
        $menuItem = [$arItem['NAME'], $arItem['DETAIL_PAGE_URL'], [], [], ""];

        if (!empty($arItem['PROPERTY_MENU_ICON_VALUE'])) {
            $menuItem[3]['ICON'] = CFile::GetPath($arItem['PROPERTY_MENU_ICON_VALUE']);
        }

        if (!empty($arItem['PROPERTY_CLASS_ICON_VALUE'])) {
            $menuItem[3]['ICON_CLASS'] = $arItem['PROPERTY_CLASS_ICON_VALUE'];
        }

        $aMenuLinksExt[] = $menuItem;
    }

    global $CACHE_MANAGER;
    $CACHE_MANAGER->StartTagCache($cache_dir);
    $CACHE_MANAGER->RegisterTag('iblock_id_31');
    $CACHE_MANAGER->RegisterTag('bitrix:menu');
    $CACHE_MANAGER->EndTagCache();
    $obCache->EndDataCache($aMenuLinksExt);
}
else {
    $aMenuLinksExt = [];
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>