<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); 
?>
	<table class="table table-responsive col-md-12 hidden-sm hidden-xs">
		<thead>
		<tr>
			<th class="red" style="border-left:0px;">Редакция 1с-битрикс24</th>
			<? foreach ($arResult["SECTION"] as $arSection): ?>
				<th colspan="<?= count($arSection["ITEMS"]) ?>"
						style="border-top:1px solid #eeedf2;"><?= $arSection["NAME"] ?></th>
			<? endforeach; ?>
		</tr>
		</thead>
		<tbody>
		<? foreach ($arResult["PROPERTY"] as $arProperty): ?>

            <?if ($arProperty["CODE"] != "PRICE_NEW" && $arProperty["CODE"] != "CITY"):?>
			<tr>
				<th scope="row"><?= $arProperty["NAME"] ?></th>
				<?
				foreach ($arResult["SECTION"] as $arSection):
					foreach ($arSection["ITEMS"] as $arItem):
                            $value = $arItem["PROPERTY"][$arProperty["CODE"]]["VALUE"];
                            if ($arProperty["USER_TYPE"] == 'SASDCheckboxNum'): ?>
                                <td><i class="fa fa-<?= ($value) ? 'plus' : 'minus'; ?>-circle" aria-hidden="true"></i></td>
                            <? else: ?>
								<?if ($arProperty["CODE"] == "PRICE"):?>
                                    <?if($arItem["PROPERTY"]["PRICE_NEW"]["VALUE"] != ""):?>
                                        <td><s><?= $value ?></s><br />
                                            <span style="color: #324abd; font-weight: bold;"><?=$arItem["PROPERTY"]["PRICE_NEW"]["VALUE"]?></span></td>
										<?else:?>
											<td><?= $value ?></td>
									<?endif;?>
                                <?else:?>
                                <td>
									<?
									/*if($arProperty["CODE"] == "CITY"){	
											$arCity = array();
											$arFilter = array("ID"=>$value);
											$arSelect = array("ID","NAME");
											if(is_array($value)){
												$obSection = CIBlockSection::GetList(array(), $arFilter, false, $arSelect);
												while($arSection = $obSection->Fetch()) $arCity[$arSection["ID"]] = $arSection["NAME"];	
											}
											$value = implode(", ", $arCity);
									}*/
									?>
									<?= $value ?></td>
                                <?endif;?>
                            <? endif;
					endforeach;
				endforeach;
				?>
			</tr>
        <?endif;?>
		<? endforeach; ?>
		<tr>
			<th scope="row"></th>
			<?
			foreach ($arResult["SECTION"] as $arSection):
				foreach ($arSection["ITEMS"] as $arItem):
					?>
					<td>
						<button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Купить коробку : <?= $arItem["NAME"]
						?>" data-toggle="modal">Купить</button>
					</td>
					<?
				endforeach;
			endforeach;
			?>
		</tr>
		</tbody>
	</table>
	<!-- Tablet version-->
	<table class="table table-responsive hidden-lg hidden-md col-sm-12 hidden-xs">
		<thead>
		<tr>
			<? foreach ($arResult["SECTION"] as $arSection): ?>
				<th><?= $arSection["NAME"] ?></th>
			<? endforeach; ?>
		</tr>
		</thead>
		<tbody>
		<?
		$half = ceil(count($arSection) / 2);
		?>
		<? foreach ($arResult["PROPERTY"] as $arProperty): ?>
			<tr>
				<? $cnt = 1;
				foreach ($arResult["SECTION"] as $arSection): ?>
					<td><?
					if ($half == $cnt++):?><p><?= $arProperty["NAME"] ?></p><?endif;
					if ($arProperty["USER_TYPE"] == 'SASDCheckboxNum'): ?>
						<i class="fa fa-<?= ($arSection["UF_" . $arProperty["CODE"]]) ? 'plus' : 'minus'; ?>-circle"
							 aria-hidden="true"></i>
					<? else: ?>
						<?= $arSection["UF_" . $arProperty["CODE"]] ?>
					<? endif;
					?>
					</td><?
				endforeach; ?>
			</tr>
		<? endforeach; ?>
		<? foreach ($arResult["SECTION"] as $arSection): ?>
			<td>
				<button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Купить коробку : <?= $arSection["NAME"]
				?>" data-toggle="modal">Купить</button>
			</td>
		<? endforeach; ?>
		</tr>
		</tbody>
	</table>
	<!-- Mobile version-->
<? foreach ($arResult["SECTION"] as $arSection): ?>
	<table class="table table-responsive hidden-lg hidden-md hidden-sm col-xs-12">
		<thead>
		<tr>
			<th><?= $arSection["NAME"] ?></th>
		</tr>
		</thead>
		<tbody>
		<? foreach ($arResult["PROPERTY"] as $arProperty): 
			if ($arProperty["CODE"] == "PRICE_NEW" || $arProperty["CODE"] == "CITY") continue;
			?>
			<tr>
				<th scope="row"><?= $arProperty["NAME"] ?></th>
				<td>
					<? if ($arProperty["USER_TYPE"] == 'SASDCheckboxNum'): ?>
						<i class="fa fa-<?= ($arSection["UF_" . $arProperty["CODE"]]) ? 'plus' : 'minus'; ?>-circle"
							 aria-hidden="true"></i>
					<? else: ?>
						<?= $arSection["UF_" . $arProperty["CODE"]] ?>
					<? endif; ?>
				</td>
			</tr>
		<? endforeach; ?>
		<tr>
			<td colspan="2">
				<button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Купить коробку : <?= $arSection["NAME"]
				?>" data-toggle="modal">Купить</button>
			</td>
		</tr>
		</tbody>
	</table>
<? endforeach; ?>