<?

use Bitrix\Main\Loader;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$show =$request->getQuery('show');
?>
<div class="news-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
</div>
            <ul class="offer-list <?if ($show=='list'){?>offer-list--detailed<?}?>">
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <li id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="offer-list__item" >
                        <div class="offer-card">
                          <?if($arItem['PROPERTIES']['IN_STOCK']['VALUE']==1){?>  <p class="offer-card__available">В наличии</p><?}?>
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="offer-card__img">
                                <?if(!empty($arItem['PREVIEW_PICTURE']['SRC'])){?>
                                    <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arParams['PRODUCT_VERSION'] ?>">
                                <?}elseif(!empty($arItem['DETAIL_PICTURE']['SRC'])){?>
                                    <img src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>" alt="<?= $arParams['PRODUCT_VERSION'] ?>">
                                <?}else{?>
                                    <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/bitrix-box.png" alt="<?= $arParams['PRODUCT_VERSION'] ?>">
                                <?}?>
                            </a>
                            <div class="offer-card__wrapper-text">
                                <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="offer-card__title">
                                    <span class="visually-hidden">Лицензия </span><?=$arItem['NAME']?><span class="visually-hidden"> Лицензия
                Битрикс24. <?=$arItem['NAME']?></span>
                                </a>
                                <p class="offer-card__description"><?=$arItem['PREVIEW_TEXT']?></p>
                                <p class="offer-card__user-count">Пользователи: <?=$arItem['PROPERTIES']['USER']['VALUE']?></p>
                                <ul class="offer-card__options">
                                    <?if($arItem['PROPERTIES']['CHECK_1']['VALUE']==1){?>
                                    <li class="offer-card__option-item">Экстранет</li>
                                    <?}?>
                                    <?if($arItem['PROPERTIES']['CHECK_4']['VALUE']==1){?>
                                    <li class="offer-card__option-item">eCommerce-платформа</li>
                                    <?}?>
                                    <?if($arItem['PROPERTIES']['CLOUD_CRM']['VALUE']==1){?>
                                    <li class="offer-card__option-item">CRM</li>
                                    <?}?>
                                    <?if($arItem['PROPERTIES']['CLOUD_ANALITICS']['VALUE']==1){?>
                                    <li class="offer-card__option-item">Сквозная аналитика</li>
                                    <?}?>
                                </ul>
                                <div class="offer-card__wrapper-price-btn">
                                    <div class="offer-card__wrapper-price">
                                        <?if($arParams['PRODUCT_VERSION']=='Коробочная'){?>
                                            <?if(!empty($arItem['PROPERTIES']['KOROBKA_PRICE_NEW']['VALUE'])){?>
                                                <p class="offer-card__old-price "><span><?=$arItem['PROPERTIES']['KOROBKA_PRICE']['VALUE']?></span></p>
                                                <p class="offer-card__price"><?=$arItem['PROPERTIES']['KOROBKA_PRICE_NEW']['VALUE']?> / 12 мес.</p>
                                            <?}else if(!empty($arItem['PROPERTIES']['KOROBKA_PRICE']['VALUE'])){?>
                                                <p class="offer-card__price"><?=$arItem['PROPERTIES']['KOROBKA_PRICE']['VALUE']?> / 12 мес.</p>
                                            <?}
                                        }else{?>
                                        <?if(!empty($arItem['PROPERTIES']['PERIOD_NEW_PRICE_2YEAR']['VALUE'])){?>
                                        <p class="offer-card__old-price "><span><?=$arItem['PROPERTIES']['PERIOD_PRICE_2YEAR']['VALUE']?></span></p>
                                        <p class="offer-card__price"><?=$arItem['PROPERTIES']['PERIOD_NEW_PRICE_2YEAR']['VALUE']?> / 1 мес.</p>
                                        <?}else{?>
                                         <p class="offer-card__price"><?=$arItem['PROPERTIES']['PERIOD_PRICE_2YEAR']['VALUE']?> / 1 мес.</p>
                                         <?}?>
                                            <p class="offer-card__old-price " ><span style="text-decoration: unset; font-size: 11px;">При покупке лицензии на 2 года</span></p>
                                        <?}?>

                                    </div>
                                    <div class="offer-card__wrapper-order">
                                        <div class="footbtn">
                                        <button type="button" onclick=" return true;" id="do-order <?=$arItem['ID']?>" class="btn offer-card__link-order"
                                                data-toggle="modal" data-target="#advanced"
                                                 <?if($arParams['PRODUCT_VERSION']=='Облачная'){?>
                                                data-form-field-type="Купить <?= $arItem['NAME'].' '.$arParams['PRODUCT_VERSION'] ?> на срок 2 года">Заказать
                                                <?}else{?>
                                                    data-form-field-type="Купить <?= $arItem['NAME'].' '.$arParams['PRODUCT_VERSION'] ?> на срок 1 год">Заказать
                                                <?}?>
                                        </button>
                                        </div>
<!--                                        <div class="offer-card__link-order">Заказать</div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?endforeach;?>
            </ul>
        </div>
    </section>
    <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
        <br /><?=$arResult["NAV_STRING"]?>
    <?endif;?>
    <section class="description">
        <h2 class="visually-hidden">Описание</h2>
        <div class="description__inner container">
            <p>
<!--                --><?//$APPLICATION->IncludeComponent(
//                    "bitrix:main.include",
//                    "",
//                    Array(
//                        "AREA_FILE_SHOW" => "file",
//                        "AREA_FILE_SUFFIX" => "inc",
//                        "EDIT_TEMPLATE" => "",
//                        "PATH" => "/include/list_product.php"
//                    )
//                );?>
			<?=(current($arResult['SECTION']['PATH'])['DESCRIPTION']);?>
            </p>
        </div>
    </section>
</main>
<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/vendor.js"></script>
<?Loader::includeModule('form');
$WEB_FORM_QUESTION_ID = 0;
$FORM_SID = 'QUESTION';
$rsForm = CForm::GetBySID($FORM_SID);
if($arForm = $rsForm->fetch())
    $WEB_FORM_QUESTION_ID = intval($arForm['ID']);


$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "question", Array(
    "WEB_FORM_ID" => $WEB_FORM_QUESTION_ID,  // ID веб-формы
    "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
    "COMPANY_IBLOCK_ID" => IB_CONTACTS,
    "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
    "EMAIL_QUESTION_CODE" => "EMAIL",
    "CITY_QUESTION_CODE" => "CITY",
    "OFFICE_QUESTION_CODE" => "OFFICE",
    "TO_QUESTION_RESIPIENTS" => "",
    "EMAIL_ONLY_FROM_RESIPIENTS" => "",
    "PRODUCT_QUESTION_CODE" => "PRODUCT",
    "TO_QUESTION_CODE" => "TO",
    "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
    "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
    "SEF_MODE" => "N",  // Включить поддержку ЧПУ
    "CACHE_TYPE" => "N",  // Тип кеширования
    "CACHE_TIME" => "3600",  // Время кеширования (сек.)
    "LIST_URL" => "",  // Страница со списком результатов
    "EDIT_URL" => "",  // Страница редактирования результата
    "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
    "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
    "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
    "VARIABLE_ALIASES" => array(
        "WEB_FORM_ID" => "WEB_FORM_ID",
        "RESULT_ID" => "RESULT_ID",
    ),
    "ALERT_ADD_SHARE" => "N",
    "HIDE_PRIVACY_POLICE" => "Y",
    "BTN_CALL_FORM" => ".btn_order",
    "HIDE_FIELDS" => array(
        0 => "CITY",
        1 => "OFFICE",
    ),
    "COMPONENT_MARKER" => "question",
    "SHOW_FORM_DESCRIPTION" => "N",
    "MESS" => array(
        "THANK_YOU" => "Спасибо, Ваша заявка принята.",
        "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
        "TITLE_FORM" => "Остались вопросы?",
    )
),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
);
?>

