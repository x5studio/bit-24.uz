if (typeof define === "function" && define.amd) {
    define(['jquery', "Controls/Paging/PagingStore"], function ($, PagingStore) {
        var PagingStoreSingle = (function () {
            var instance;

            function ConstructSingletone() {
                if (instance) {
                    return instance;
                }
                if (this && this.constructor === ConstructSingletone) {
                    instance = this;
                } else {
                    return new ConstructSingletone();
                }

                PagingStore.apply(this, arguments);
            }

            extend(ConstructSingletone, PagingStore);

            return ConstructSingletone;
        }());

        return PagingStoreSingle;
    });
}