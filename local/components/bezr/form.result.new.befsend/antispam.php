<?php
if( $arParams['USE_ANTISPAM']  == 'Y' ) {

    // ставим скрытому полю уникальный ID поля
    $arIdsAnswers = array();
    foreach ($arResult["arAnswers"] as $key => $arAnswer) {
        array_push($arIdsAnswers, $arAnswer[0]['ID']);
    }
    $ANTISPAM_ID = max($arIdsAnswers) + 1;
    $ANTISPAM_FIELD_ID = max($arIdsAnswers) + 2;

    $arResult["arQuestions"]['FANTOME'] = Array(
        "ID" => $ANTISPAM_FIELD_ID,
        "ACTIVE" => "Y",
        "TITLE" => \PB\Main\Loc::getMessage("PB_MAIN_BEFSEND_FANTOME_LABEL"),
        "TITLE_TYPE" => "text",
        "SID" => "FANTOME",
        "C_SORT" => 500,
        "ADDITIONAL" => "N",
        "REQUIRED" => "N",
        "VARNAME" => "PHONE"
    );
    $arResult["arAnswers"]['FANTOME'] = Array([
        "ID" => $ANTISPAM_ID,
        "FIELD_ID" => $ANTISPAM_FIELD_ID,
        "QUESTION_ID" => $ANTISPAM_FIELD_ID,
        "ACTIVE" => "Y",
        "FIELD_TYPE" => "text",
        "SID" => "FANTOME",
        "C_SORT" => 500,
        "FIELD_PARAM" => "FANTOME"
    ]);

}