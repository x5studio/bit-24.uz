<?

use \Bitrix\Main\Loader;

class BitrixPriceUpdater {

    public static function getPrices($zone = 'ru', $type = 'b24') {

        $result = array();

        $bitrixPrices = file_get_contents('http://partners.1c-bitrix.'.$zone.'/catalog.php?type='.$type);

        if($bitrixPrices)
            $result = json_decode($bitrixPrices, true);



        return $result;

    }

    public static function updateCloudPrices() {

        $data['ru'] = self::getPrices('ru');
        $data['kz'] = self::getPrices('kz');
        $data['ua'] = self::getPrices('ua');

        if(Loader::includeModule('iblock')){
            // облако
            $rsPrices = CIBlockElement::GetList(
                array(),
                array(
                    'ACTIVE' => 'Y',
                    'IBLOCK_ID' => 10
                ),
                false,
                false,
                array('ID', 'IBLOCK_ID', 'PROPERTY_CITY', 'PROPERTY_TYPE')
            );
            
            while($arPrice = $rsPrices->GetNext()){

                $priceLocation = '';
                // получаем верхнюю категорию любого города

                $CityID = !empty($arPrice['PROPERTY_CITY_VALUE']) ? $arPrice['PROPERTY_CITY_VALUE'][0] : 0;

                if($CityID){

                    $rsChain = CIBlockSection::GetNavChain(17, $CityID, array('ID', 'CODE'));

                    if($arChain = $rsChain->GetNext()){

                        $priceLocation = $arChain['CODE'];

                    }


                }
                
                
                if($priceLocation && isset($data[$priceLocation]) && !empty($data[$priceLocation])){

                    $newPrices = array();

                    foreach($data[$priceLocation] as $newPrice){

                        
                        
                        if ($newPrice['EDITION'] == $arPrice['PROPERTY_TYPE_VALUE']) {

                            switch ($newPrice['B24_PERIOD']) {
                                case '1':
                                    
                                    $newPrices['PRICE'] = self::PrettyPriceFormat($newPrice['PRICE']/$newPrice['B24_PERIOD'], $priceLocation);
                                    $newPrices['PRICEFORMONTH'] = self::PrettyPriceFormat($newPrice['PRICE']/$newPrice['B24_PERIOD'], $priceLocation);

                                    break;
                                case '3':

                                    $newPrices['PRICEFORTHREEMONTHS'] = self::PrettyPriceFormat($newPrice['PRICE']/$newPrice['B24_PERIOD'], $priceLocation);

                                    break;  
                                case '6':
                                    
                                    $newPrices['PRICEFORHALFAYEAR'] = self::PrettyPriceFormat($newPrice['PRICE']/$newPrice['B24_PERIOD'], $priceLocation);

                                    break;
                                case '12':
                                    
                                    $newPrices['PRICEFORYEAR'] = self::PrettyPriceFormat($newPrice['PRICE']/$newPrice['B24_PERIOD'], $priceLocation);

                                    break;
                                case '24':
                                    
                                    $newPrices['PRICEFORTWOYEARS'] = self::PrettyPriceFormat($newPrice['PRICE']/$newPrice['B24_PERIOD'], $priceLocation);

                                    break;
                            }

                        }


                    }

                    if($newPrices){
                        
                        CIBlockElement::SetPropertyValuesEx($arPrice['ID'], 10, $newPrices);


                    }

                    
                }
                

            }


        }
    
        return 'BitrixPriceUpdater::updateCloudPrices();';
    }

    public static function updateBoxPrices() {

        $data['ru'] = self::getPrices('ru','cp');
        $data['kz'] = self::getPrices('kz','cp');
        $data['ua'] = self::getPrices('ua','cp');


        if(Loader::includeModule('iblock')){
            // коробка
            $rsPrices = CIBlockElement::GetList(
                array(),
                array(
                    'ACTIVE' => 'Y',
                    'IBLOCK_ID' => 11
                ),
                false,
                false,
                array('ID', 'IBLOCK_ID', 'PROPERTY_TYPE', 'PROPERTY_CITY')
            );
            
            while($arPrice = $rsPrices->GetNext()){

                $priceLocation = '';
                // получаем верхнюю категорию любого города

                $CityID = !empty($arPrice['PROPERTY_CITY_VALUE']) ? $arPrice['PROPERTY_CITY_VALUE'][0] : 0;

                if($CityID){

                    $rsChain = CIBlockSection::GetNavChain(17, $CityID, array('ID', 'CODE'));

                    if($arChain = $rsChain->GetNext()){

                        $priceLocation = $arChain['CODE'];

                    }


                }
                
                if($priceLocation && isset($data[$priceLocation]) && !empty($data[$priceLocation])){

                    $newPrices = array();

                    foreach($data[$priceLocation] as $newPrice){

                        
                        if ($newPrice['EDITION'] == $arPrice['PROPERTY_TYPE_VALUE'] && $newPrice['PRODUCT_TYPE'] == 'BUY') {
                            

                            //плюс к цене энтерпрайз
                            $plusSimbol = '';

                            if(strpos( $newPrice['EDITION'], 'ENT_') !== false) $plusSimbol = '+';

                            if(isset($newPrice['BASE_PRICE'])){

                                $newPrices['PRICE'] = self::PrettyPriceFormat($newPrice['BASE_PRICE'],$priceLocation, $plusSimbol);
                                $newPrices['PRICE_NEW'] = self::PrettyPriceFormat($newPrice['PRICE'],$priceLocation, $plusSimbol);

                            }else{

                                $newPrices['PRICE'] = self::PrettyPriceFormat($newPrice['PRICE'],$priceLocation, $plusSimbol);
                                $newPrices['PRICE_NEW'] = '';

                            } 

                        }


                    }

                    if($newPrices){

                        CIBlockElement::SetPropertyValuesEx($arPrice['ID'], 11, $newPrices);

                    }

                    
                }
                

            }


        }

        return 'BitrixPriceUpdater::updateBoxPrices();';
    }

    static function PrettyPriceFormat($price, $cur = 'ru', $plus = '') {

        $symbols = array(
            'ru' => ' ₽',
            'kz' => ' ₸',
            'ua' => ' ₴',
        );

        $price = ceil ($price);

        return number_format($price, 0, ',', ' ').$plus.$symbols[$cur];

    }

	static function loggerAgent(){

        self::logger(date(''));

        return 'BitrixPriceUpdater::loggerAgent();';

    }

	static function logger($data){

		file_put_contents(\Bitrix\Main\Application::getDocumentRoot().DIRECTORY_SEPARATOR.date('d-m-Y').'-'.__METHOD__.'.txt', print_r($data, true)."\n\n", FILE_APPEND);

	}
}