<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


?>

<div class="box-offices line-box-offices">
	<div class="box-content<?if ($arParams['IS_STOP_RANDOM'] == 'N') {?> j-sync-offices-items<?}?>">
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
			CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => PB\Main\Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		
			<div class="box-offices-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-item-id="<?=$arItem['ID']?>">				
				<span class="strong box-office-phone"><?=$arItem['PROPERTIES']['phone']['VALUE']?></span>
				<span class="line-box-email mail"><a href="mailto:<?=$arItem['PROPERTIES']['email']['VALUE']?>"><?=$arItem['PROPERTIES']['email']['VALUE']?></a></span>
			</div>
		
		<?endforeach;?>
	</div>
	<div class="container-btn-line-offices"><a class="btn btn-big btn-big-wide btn-lilac" id="general-form-<?=$arParams["WEB_FORM_ID"]?>-btn" href="javascript:void(0);"><?php echo PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_SR_ORDER'); ?></a></div>
</div>