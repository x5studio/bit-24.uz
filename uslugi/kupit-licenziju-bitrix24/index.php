<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Первый Бит - мы гарантируем лучшие условия приобретения облачных и коробочных лицензий Битрикс24. Ознакомиться с нашим предложением ...");
$APPLICATION->SetPageProperty("keywords", "лицензия, облако, коробка, битрикс24, bitrix24, компания, команда, корпоративный портал, проект, проект+, enterprise, первый бит");
$APPLICATION->SetPageProperty("title", "Купить лицензию Битрикс24 от крупнейшего партнера - Первый Бит");
$APPLICATION->SetTitle("Купить лицензию Битрикс24");
?>
    <!-- Marquiz script start -->
    <script src="//script.marquiz.ru/v1.js" type="application/javascript"></script>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            Marquiz.init({
                host: '//quiz.marquiz.ru',
                id: '5ef9e8c41afc680044ab2ade',
                autoOpen: false,
                autoOpenFreq: 'once',
                openOnExit: false
            });
        });
    </script>
    <!-- Marquiz script end -->
<section class="buy-license">
<div class="container">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#cloud" aria-controls="cloud" role="tab" data-toggle="tab">Облачная версия</a></li>
		<li role="presentation"><a href="#box" aria-controls="box" role="tab" data-toggle="tab">Коробочная версия</a> </li>
	</ul>
	 <!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="cloud">
            <div class="row oboznachenia oboznachenia-comp">
                <div class="col-md-3">
                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                    нет
                </div>
                <div class="col-md-3">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    базовый набор возможностей
                </div>
                <div class="col-md-3">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    расширенный
                </div>
                <div class="col-md-3">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    профессиональный
                </div>
            </div>
             <?$APPLICATION->IncludeComponent(
                "buy.tariff",
                "",
                Array(
                    "ACTIVE_PRICE" => "PRICEFORTWOYEARS",
                    "ADDITIONAL_PRICES" => array("PRICEFORMONTH"=>"на 1 месяц","PRICEFORTHREEMONTHS"=>"на 3 месяца","PRICEFORYEAR"=>"на год","PRICEFORTWOYEARS"=>"на 2 года",),
                    "BUTTON_TEXT" => "Купить",
                    "CACHE_TIME" => "360000",
                    "CACHE_TYPE" => "A",
                    "EXCLUDE_PROPS" => array('DESC','ICON','TYPE'),
                    "IBLOCK_ID" => "10",
                    "SHOW_ELEMENT_NAME" => true,
                    "SHOW_MOBILE_PROP" => [
                        'PRICE',
                        'USERS',
                        'OFIS',
                        'CRM',
                        'TASKS_PROJECTS'
                    ],

                    //'BUTTON_IS_LINK' => array('http://bitrix24.ru/create.php?p=27270')

                )
            );?>

            <div class="row oboznachenia oboznachenia-mob">
                <div class="col-md-3">
                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                    нет
                </div>
                <div class="col-md-3">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    базовый набор возможностей
                </div>
                <div class="col-md-3">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    расширенный
                </div>
                <div class="col-md-3">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    профессиональный
                </div>
            </div>

        </div>
		<div role="tabpanel" class="tab-pane clearfix" id="box">
			 <?$APPLICATION->IncludeComponent(
	"buy.tariff",
	"",
	Array(
		"ALL_TARIFFS_TITLE" => "Редакции",
		"CACHE_TIME" => "360000",
		"CACHE_TYPE" => "A",
		"EXCLUDE_PROPS" => array('TYPE'),
		"IBLOCK_ID" => "11"
	)
);?>
			<table class="table table-responsive col-md-12 hidden-sm hidden-xs" style="margin-top:20px">
			<thead>
			<tr>
				<th class="" colspan="4" style="border-top:1px solid #eeedf2;text-align: center; font-size: 36px;">
					Все возможности
				</th>
			</tr>
			<tr>
				<th colspan="1" class="red" style="border-left:0px;width:300px;">
				</th>
				<th colspan="1" style="border-top:1px solid #eeedf2;width:25%;">
					CRM
				</th>
				<th colspan="1" style="border-top:1px solid #eeedf2;width:25%;">
					Корпоративный портал
				</th>
				<th colspan="1" style="border-top:1px solid #eeedf2;width:25%;">
					Энтерпрайз
				</th>
			</tr>
			<tr>
				<td colspan="4" class="section-header" style="font-size: 24px; text-align: center;display:table-cell;">
					Для всех редакций
				</td>
			</tr>
			</thead>
			<tbody>
			<tr>
				<th colspan="4" class="table__toggle section-header" rel="1">
					Коммуникации
				</th>
			</tr>
			<tr class="table__toggle1">
				<th scope="row">
					Живая лента
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle1">
				<th scope="row">
					Чат и видеозвонки
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle1">
				<th scope="row">
					Чат с поддержкой 24
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle1">
				<th scope="row">
					Телефония
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle1">
				<th scope="row">
					Network
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle1">
				<th scope="row">
					HR
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle1">
				<th scope="row">
					Мобильное приложение
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="2" colspan="4">
					Бизнес-инструменты
				</th>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Бизнес-пользователи
				</th>
				<td>
					12
				</td>
				<td>
					от 50
				</td>
				<td>
					от 1000
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Задачи и Проекты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Календари
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Диск
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Открытые линии
				</th>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Бизнес-процессы
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Учет рабочего времени
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Отчеты по рабочему времени
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Рабочие отчеты руководителю
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Cобрания и планерки
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle2">
				<th scope="row">
					Внешние пользователи (экстранет)
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="3" colspan="4">
					Управление задачами и проектами
				</th>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Управление задачами, проектами
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Диаграмма Ганта
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Контроль сроков
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Делегирование
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Отчеты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Учет времени по задачам
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Шаблоны задач
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Чек-листы
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Задачи из Живой ленты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Занятость сотрудников
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Зависимости между задачами в диаграмме Ганта
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle3">
				<th scope="row">
					Пользовательские поля
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="4" colspan="4">
					CRM: клиенты и продажи
				</th>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Face-трекер
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Чат-трекер
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Канбан
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Старт CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Отчеты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					База контактов и клиентов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Управление сделками
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Отправка коммерческих предложений
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Работа со счетами
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Счета онлайн
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Каталог товаров
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Импорт данных
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Письма клиентам
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Планирование работы
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Аналитические отчеты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Интеграция с телефонией
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Интеграция с интернет-магазином
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					неограниченно
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					неограниченно
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					неограниченно
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Воронка продаж
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Мультиворонки (направления сделок)
				</th>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Email-трекер
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Загрузка истории писем в Email-трекере
				</th>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					CRM-формы
				</th>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Виджет на сайт: чат, обратный звонок, форма
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Роботы CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					везде
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					везде
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					везде
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Конвертация между сделками, счетами, предложениями
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Расширенный поиск дубликатов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					История всех изменений с возможностью восстановления
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Журнал доступа сотрудников к данным в CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Просмотр больше 5 000 записей в CRM <br>
					<small>Общее число записей неограничено во всех тарифах</small>
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Обзвон по списку клиентов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Редактирование прав доступа в Открытых линиях
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Автоматизация бизнес-процессов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Интеграция с 1С
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Рассылки по базе клиентов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					Продажа услуг
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle4">
				<th scope="row">
					И другие полезные возможности
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="5" colspan="4">
					Телефония
				</th>
			</tr>
			<tr class="table__toggle5">
				<th scope="row">
					Аренда виртуального номера телефона (городского и федерального 8 800)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Привязка собственного номера телефона (для исходящих звонков)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Неограниченное число входящих линий
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Маршрутизация звонков
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Перенаправление во время вызова
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Встроенная интеграция с CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Подключение своей АТС
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Подключение телефонного аппарта
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Внутренний номер для сотрудников
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Настройка по рабочему времени
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Голосовые сообщения для приветствия и автоответчика
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Запись разговора (до 100 в месяц)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Запись всех звонков без ограничений (больше 100 в месяц)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Одновременный звонок на всех свободных сотрудников в очереди
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Оценка разговора клиентом
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Распределение прав доступа к звонкам и настройкам телефонии
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Определение источника звонка по номеру телефона (для CRM)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Аналитика и отчеты по звонкам
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					Голосовое меню
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle5">
				<th scope="row">
					И другие полезные возможности скоро
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="6" colspan="4">
					Планирование и учет рабочего времени
				</th>
			</tr>
            <tr class="table__toggle6">
				<th scope="row">
					Календари (общие и персональные
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle6">
				<th scope="row">
					Планировщик коллективных встреч и собраний
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle6">
				<th scope="row">
					Синхронизация календарей с MS Outlook, Google, iOS, MacOS, Android
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle6">
				<th scope="row">
					Рабочие отчеты руководителю
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle6">
				<th scope="row">
					Учет рабочего времени
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle6">
				<th scope="row">
					Собрания и планерки
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle6">
				<th scope="row">
					Резервирование переговорных
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="7" colspan="4">
					HR: управление персоналом
				</th>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Визуальное управление структурой компании
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Список сотрудников, контакты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Синхронизация контактов с MS Outlook, Google, iOS, MacOS, Android
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Телефонный справочник
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Персональная страница сотрудника
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Права доступа сотрудников к информации
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					График отсутствий сотрудников
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Объявления с подтверждением прочтения
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Инструменты мотивации, геймификация
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Пульс компании (упрощает внедрение Битрикс24)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Обучение и тестирование сотрудников
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Новости компании
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Подписка на новости компании
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Сервис сбора идей
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Доска объявлений компании
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Внутренние заявки
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Просмотр зарплатных листков и отпусков
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle7">
				<th scope="row">
					Служба техподдержки сотрудников
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="8" colspan="4">
					Автоматизация бизнес-процессов компании
				</th>
			</tr>
            <tr class="table__toggle8">
				<th scope="row">
					Управление бизнес-процессами (визуальный конструктор)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle8">
				<th scope="row">
					Бизнес-процессы в CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle8">
				<th scope="row">
					Бизнес-процессы в общих документах
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle8">
				<th scope="row">
					Универсальные списки (для автоматизации своих рабочих процессов, например, документооборота)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle8">
				<th scope="row">
					Документооборот
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle8">
				<th scope="row">
					Командировки и отпуска
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle8">
				<th scope="row">
					Утверждение счетов
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle8">
				<th scope="row">
					Send&amp;Save. Архив почты на портале
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle8">
				<th scope="row">
					Многодепартаментность
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="9" colspan="4">
					Приложения для Битрикс24
				</th>
			</tr>
            <tr class="table__toggle9">
				<th scope="row">
					Мобильное приложение для iPhone, iPad
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle9">
				<th scope="row">
					Мобильное приложение для Android (телефон и планшет)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle9">
				<th scope="row">
					Десктоп-приложение для Windows и MacOS
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle9">
				<th scope="row">
					Каталог веб-приложений для «облака»
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle9">
				<th scope="row">
					Rest API для создания приложений
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle9">
				<th scope="row">
					Каталог веб-приложений для «коробки»
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle9">
				<th scope="row">
					Открытый API и исходный код
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="10" colspan="4">
					Интеграции
				</th>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Интеграция с MS Office, MS Office Online
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Интеграция с GoogleDocs
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Интеграция с MS Outlook (контакты, календари)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Интеграция с Google (контакты, календари)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Интеграция с MacOS, iOS, Android
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Подключение внешних АТС
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Коннектор к MS Exchange Server 2007/2010
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Интеграция с MS Exchange Web Mail
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Коннектор к MS SharePoint
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Интеграция с «1С:ЗУП»
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle10">
				<th scope="row">
					Active Directory/LDAP Интегратор + NTLM
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="11" colspan="4">
					Настройки
				</th>
			</tr>
            <tr class="table__toggle11">
				<th scope="row">
					Аудит, логи
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="12" colspan="4">
					Масштабирование
				</th>
			</tr>
            <tr class="table__toggle12">
				<th scope="row">
					Веб-кластер
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle12">
				<th scope="row">
					Виртуальная машина (полностью настроенное и протестированное серверное окружение для оптимальной работы Битрикс24)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="13" colspan="4">
					Безопасность и надежность
				</th>
			</tr>
            <tr class="table__toggle13">
				<th scope="row">
					SSL-сертификат
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle13">
				<th scope="row">
					Проактивная защита (WAF - Web Application Firewall)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle13">
				<th scope="row">
					Разграничение прав доступа пользователей
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle13">
				<th scope="row">
					Ограничение доступа по IP
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle13">
				<th scope="row">
					Персональный генератор одноразовых паролей (OTP)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle13">
				<th scope="row">
					Облачный бекап
				</th>
				<td>
					10 Гб
				</td>
				<td>
					от 20 Гб
				</td>
				<td>
					от 60 Гб
				</td>
			</tr>
			<tr>
				<th class="table__toggle section-header" rel="14" colspan="4">
					А также
				</th>
			</tr>
            <tr class="table__toggle14">
				<th scope="row">
					Свой домен
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle14">
				<th scope="row">
					Администрирование корпоративного портала
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle14">
				<th scope="row">
					Управление контентом (визуальный редактор страниц)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle14">
				<th scope="row">
					Расширенное управление правами доступа
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle14">
				<th scope="row">
					Варианты дизайна
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle14">
				<th scope="row">
					Кастомизация дизайна
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle14">
				<th scope="row">
					Разработка собственной бизнес-логики
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle14">
				<th scope="row">
					Контроллер для интеграции с внешним сайтом
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
            <tr class="table__toggle14">
				<th scope="row">
					Анализ и статистика посещений
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr style="display:none;">
				<th scope="row">
				</th>
				<td>
 <button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Купить коробку : CRM" data-toggle="modal">Купить</button>
				</td>
				<td>
 <button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Купить коробку : Корпоративный портал 1" data-toggle="modal">Купить</button>
				</td>
				<td>
 <button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Купить коробку : Корпоративный портал 2" data-toggle="modal">Купить</button>
				</td>
			</tr>
			</tbody>
			</table>
		</div>
        <div class="box-shadow">
            <div class="buy-license__text-content buy-license__text-content-nonshadowmob mbot30">
                <h2>Не знаете какая лицензия Битрикс24 нужна Вам?</h2>
                <p>Ответьте на несколько вопросов в окне ниже, мы с Вами свяжемся и бесплатно поможем в подборе лицензии.</p>
            </div>
            <div class="marquiz__container marquiz__container_inline">
                <a class="marquiz__button marquiz__button_blicked marquiz__button_rounded marquiz__button_shadow" href="#popup:marquiz_5ef9e8c41afc680044ab2ade" data-fixed-side="" data-alpha-color="rgba(211, 64, 133, 0.5)" data-color="#d34085" data-text-color="#ffffff">Пройти тест</a>
            </div>
        </div>

	</div>
    <div class="buy-license__text mtop20 mbot70">
        <h3>Облачные тарифы</h3>
        <ul>
            <li>«Базовый» — тариф для открытия небольшого бизнеса или организации отдела продаж до 5 человек.</li>
            <li>«Стандартный» — включает все необходимое для управления командой до 50 человек.</li>
            <li>«Профессиональный» — для работы с крупными проектами и неограниченным количеством сотрудников. Полная автоматизация процессов.</li>
        </ul>
        <br />
        <h3>Коробочные тарифы</h3>
        <ul>
            <li>«Интернет-магазин + CRM» - продукт, включающий в себя все необходимые инструменты для продаж компании, со штатом до 12 человек. Отлично подойдет для интернет магазинов и коммерческих сайтов.</li>
            <li>«Корпоративный портал на 50/100/250/500» - дает возможность подключения количества сотрудников, указанного в названии лицензии. Включает весь функционал коробочных версий.</li>
            <li>«Энтерпрайз» - подходит для крупных предприятий и бизнеса с высоким оборотом и большим количеством сотрудников. Обладает возможностью добавления неограниченного количества пользователей и максимальным функционалом.</li>
        </ul>
    </div>
 </div></section>
<?
$APPLICATION->IncludeComponent(
    "bezr:form.result.new.befsend",
    "request", Array(
        "WEB_FORM_ID" => 13,  // ID веб-формы
        "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
        "COMPANY_IBLOCK_ID" => IB_CONTACTS,
        "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
        "EMAIL_QUESTION_CODE" => "EMAIL",
        "CITY_QUESTION_CODE" => "CITY",
        "OFFICE_QUESTION_CODE" => "OFFICE",
        "TO_QUESTION_RESIPIENTS" => "",
        "EMAIL_ONLY_FROM_RESIPIENTS" => "",
        "PRODUCT_QUESTION_CODE" => "PRODUCT",
        "TO_QUESTION_CODE" => "TO",
        "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
        "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
        "CACHE_TYPE" => "A",  // Тип кеширования
        "CACHE_TIME" => "3600",  // Время кеширования (сек.)
        "LIST_URL" => "",  // Страница со списком результатов
        "EDIT_URL" => "",  // Страница редактирования результата
        "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
        "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
        "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        ),
        "ALERT_ADD_SHARE" => "N",
        "HIDE_PRIVACY_POLICE" => "Y",
        "BTN_CALL_FORM" => ".btn_order",
        "STRONG_SHOW_CITY" => 'N',
        "STRONG_SHOW_OFFICE" => 'N',
        "HIDE_FIELDS" => array(
            0 => "CITY",
            1 => "OFFICE",
        ),
        "COMPONENT_MARKER" => "request",
        "SHOW_FORM_DESCRIPTION" => "N",
        "MESS" => array(
            "THANK_YOU" => "Спасибо, Ваша заявка принята.",
            "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
            "TITLE_FORM" => "Оставить заявку",
        )
    ),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
);
?>
<div style="visibility: hidden;" class="marquiz-pops marquiz-pops_position_bottom-left marquiz-pops_blicked marquiz-pops_shadowed marquiz-pops_position" ><a class="marquiz-pops__body" href="#popup:marquiz_5de6172e4734030045bdb536" data-marquiz-pop-text-color="#ffffff" data-marquiz-pop-background-color="#d34085" data-marquiz-pop-svg-color="#fff" data-marquiz-pop-close-color="#fff" data-marquiz-pop-color-pulse="rgba(211, 64, 133, 0.4)" data-marquiz-pop-color-pulse-alpha="#d34085" data-marquiz-pop-delay="2s" data-marquiz-pop="true"><span class="marquiz-pops__icon"></span><span class="marquiz-pops__content"><span class="marquiz-pops__content-title">Пройти тест</span><span class="marquiz-pops__content-text">&laquo;Рассчитать стоимость внедрения&raquo;</span></span></a></div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>