<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
    <div class="blog-detail-wrap">
        <div class="container">

            <div class="col-sm-8 col-md-9 col-xs-12 blog-detail-content-wrap">
                <h1><?= $arResult["NAME"] ?></h1>

                <div class="tag-date-wrap">
                    <span class="date"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
					<? foreach ($arResult["TAG"] as $arTag): ?>
                        <span class="tag deep-blue"><?= $arTag["NAME"] ?></span>
					<? endforeach; ?>
                </div>

                <div class="blog-detail-content">
					<? /*if ($arResult["DETAIL_PICTURE"]): ?>
                        <img src="<?= $arResult["IMAGES"][$arResult["DETAIL_PICTURE"]] ?>" alt="<?= $arResult["NAME"] ?>">
					<? endif; */?>
					<?= $arResult["DETAIL_TEXT"] ?>
                </div>

            </div>

            <div class="col-sm-4 col-md-3 col-xs-12 blog-detail-sidebar">
                <?if ($arResult['PROPERTY_AUTHOR_VALUE'] != ""):?>
                <div class="author clearfix">
                    <img src="<?=$arResult["IMAGES"][$arResult['PROPERTY_AUTHOR_IMG_VALUE']]?>" alt="">
                    <div class="author-desc">
                        <span class="author-name"><?=$arResult['PROPERTY_AUTHOR_VALUE']?></span>
                        <span class="author-role"><?=$arResult['PROPERTY_AUTHOR_ROLE_VALUE']?></span>
                    </div>
                </div>
                <?endif;?>
                <?/*
                <div class="socnet-wrap">
                    <span class="title">Рассказать друзьям</span>
                </div>

                <div class="interesting-block">
                    <div class="title-block clearfix">
                        <span>Интересно</span>
                        <img src="/blog_new/images/detail/interesting_ico.png" alt="">
                    </div>
                    <div class="interesting-block-inner">
                        <p class="subtitle">Обзор всех функций нового Битрикс24.гонконг</p>
                        <p>Рассказываем о новых функциях, показанных 1 марта в рамках бизнес-встречи Битрикс24.Идея, в новой редакции Битркис24.Гонконг.</p>
                        <span class="date">30.12.2017</span>
                    </div>
                </div>
                */?>
				<?$APPLICATION->IncludeComponent("asd:subscribe.quick.form", "blog_detail", Array(
					"FORMAT" => "html",	// Формат подписки
					"INC_JQUERY" => "N",	// Подключить jQuery
					"NOT_CONFIRM" => "N",	// Подписывать без подтверждения
					"RUBRICS" => array(	// Подписывать на рубрики
						0 => "1",
					),
					"SHOW_RUBRICS" => "N",	// Показывать рубрики
				),
					false
				);?>

            </div>
        </div>

        <div class="content-bottom">
            <div class="container">
                <div class="col-sm-8 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-12 text text-left">
                        <a class="btn btn-sm" onclick="javascript:history.back(); return false;">Вернуться назад</a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 date text-right">
                        <script type="text/javascript">(function () {
                                if (window.pluso)if (typeof window.pluso.start == "function") return;
                                if (window.ifpluso == undefined) {
                                    window.ifpluso = 1;
                                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                    s.type = 'text/javascript';
                                    s.charset = 'UTF-8';
                                    s.async = true;
                                    s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                                    var h = d[g]('body')[0];
                                    h.appendChild(s);
                                }
                            })();</script>
                        <div class="pluso" data-background="transparent"
                             data-options="medium,square,line,horizontal,counter,theme=04"
                             data-services="vkontakte,facebook,odnoklassniki,moimir"></div>
                    </div>
                </div>
            </div>

        </div>


    </div>