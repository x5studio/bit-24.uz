<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
	<ul class="mdm-third">
		<li class="dropdown-header">Типы внедрения Битрикс24</li>
		<? foreach ($arResult as $arItem): ?>
			<li><a href="<?= $arItem["LINK"] ?>"><img src="<?= $arItem["PARAMS"]["ICON"] ?>" alt="<?= $arItem["TEXT"] ?>"><?=
					$arItem["TEXT"] ?></a></li>
		<? endforeach ?>
	</ul>
<? endif ?>