if (typeof define === "function" && define.amd) {
    define(['flexslider'], function () {
        function Slider1(container) {
            var _this = this;
            this.container = $(container);
            var controlsContainer = this.container.data('controlsContainer');
            var directionNavs = this.container.data('directionNavs') || false;
            _this.container.flexslider({
                pauseOnHover: true,
                pauseOnAction: true,
                animation: 'slide',
                controlsContainer: controlsContainer,
                animationLoop: true,
                itemMargin: 0,
                move: 1,
                useCSS: false,
                controlNav: true,
                directionNav: directionNavs,
                startAt: 0,
                slideshow: true,
                slideshowSpeed: 5000,
                start: function () {
					_this.fixFlexsliderHeight();
                    // При клике на переключатель остонавливаем слайдер
                    var self = this;
                    $('.flex-control-nav', _this.container).click(function () {
                        self.slideshowSpeed = 99999;
                    });
                }
            });

            $(window).resize(function() {
                _this.fixFlexsliderHeight();
            });
        }

        Slider1.prototype.fixFlexsliderHeight = function fixFlexsliderHeight() {
            var sliderHeight = 0;

            this.container.find('ul.slides > li').css({'height' : ''});
            this.container.find('ul.slides > li').each(function(){
                slideHeight = $(this).outerHeight();

                if (sliderHeight < slideHeight)
                {
                    sliderHeight = slideHeight;
                }
            });
            this.container.find('ul.slides, ul.slides > li').css({'height' : sliderHeight});
        }

        return Slider1;
    });
}
