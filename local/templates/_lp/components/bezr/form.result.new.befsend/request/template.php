<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$PARENT = $this->__component->__parent; ?>

<section class="Form-action">
	<div class="contact-section">
		<div class="container forma">
			<div class="col-md-4 col-md-offset-4 pull-right mail hidden-xs"><img src="<?=
				SITE_TEMPLATE_PATH ?>/assets/img/formbg.png" class="img-responsive"></div>
			<div id="general2page-form-<?= $arResult["SOULT_FORM"] ?>"
					 class="j-form-page fbox"
					 data-salt-form="<?= $arResult["SOULT_FORM"] ?>"
			>
				<?
				/**********************************************************************************
				 * form header
				 **********************************************************************************/
				if ($arResult["isFormTitle"])
				{
					?>
					<? if ($arParams["SHOW_TITLE"] != "N"):?>
					<h2 class="section-header"><?= $arResult["FORM_TITLE"] ?></h2>
				<? endif ?>
					<?
				} //endif ;
				?>

				<? if ($arResult["isFormErrors"] == "Y"): ?><?//= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>


				<form name="<?= $arResult["arForm"]["SID"] ?>" action="<?= POST_FORM_ACTION_URI ?>"
							method="POST" enctype="multipart/form-data" style="float:left; width: 100%;">
					<input type="hidden" name="sessid" id="sessid" value=""/>
					<input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>"/>

					<? $arHiddenID = Array(); ?>
					<div class="col-md-4 col-sm-4 col-xs-12 form-line">
						<? $k = 0;
						foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):$k++; ?>
							<?
							// TEXT EMAIL ...
							if (in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], array('text', 'email'))):?>
								<div class="form-group has-feedback<?if(strlen($arResult["FORM_ERRORS"][$FIELD_SID]) > 0):?> has-error<?endif;
								?>">
									<input
											name="form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
											type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE" ? "tel" : $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"<? if (!empty($arQuestion['STRUCTURE'][0]['FIELD_PARAM']))
									{ ?>
										data-type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] ?>"<?
									} ?>
											value="<?= $arResult['arrVALUES'][$FIELD_SID] ?: null ?>"
											placeholder="<?= $arQuestion["CAPTION"] ?>"
											class="form-control"
									/>
									<span class="glyphicon glyphicon-remove form-control-feedback"></span>
								</div>
								<?
							// DROPDOWN
							elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'dropdown')://__($arQuestion)
								?>
								<div class="text">
									<?= $arQuestion["HTML_CODE"] ?>
								</div>
								<?
							// TEXTAREA
							elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea'):?>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-12">
									<div class="form-group comment">
										<textarea class="form-control"
															id="description"
															placeholder="<?= $arQuestion["CAPTION"] ?>"
															name="form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"><?= $arResult['arrVALUES'][$FIELD_SID] ?: null ?></textarea>
									</div>
								<?
							// FILE
							elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'file'):?>
								<div class="row submit">
									<div class="attach col-xs-12 col-md-6 col-sm-6">
										<label><?= $arQuestion["HTML_CODE"] ?><i class="fa fa-paperclip" aria-hidden="true"></i><span
													class="file-name"><?= $arQuestion["CAPTION"] ?></span></label></div>
							<? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'):$k--; ?>
								<? array_push($arHiddenID, $FIELD_SID); ?>
							<? endif; ?>
						<? endforeach; ?>

					<? // SUBMIT ?>
									<div class="send col-xs-12 col-md-6 col-sm-6">
										<input type="submit" class="btn btn-default submit" name="web_form_submit"
													 value="<?= $arParams['MESS']['SUBMIT_BTN'] ?: (strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>">
									</div>
								</div>
					<?
					// HIDDENS
					if (!empty($arHiddenID))
					{
						$k--;
						foreach ($arHiddenID AS $hiddenID)
						{
							echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
						}
					} ?>
					<? if (!empty($arParams['COMPONENT_MARKER'])): ?>
						<input type="hidden" name="COMPONENT_MARKER" value="<?= $arParams['COMPONENT_MARKER'] ?>">
					<? endif; ?>
				</form>
			</div>
<div style="padding-left: 15px;float:left;clear:both;">Нажимая кнопку, я принимаю условия <a href="https://bit-24.ru/oferta/">Оферты</a> по использованию сайта и согласен с <a href="https://bit-24.ru/politika-konfidencialnosti/">Политикой конфиденциальности</a></div>
		</div>
	</div>
</section>