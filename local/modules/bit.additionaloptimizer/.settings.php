<?

return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\Bit\AdditionalOptimizer\Api' => 'api',
            ],
        ],
        'readonly' => true,
    ],
];
