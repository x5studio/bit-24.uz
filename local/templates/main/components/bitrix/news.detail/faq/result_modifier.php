<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

$cp = $this->__component; // объект компонента
if (is_object($cp)) {
    $cp->arResult["MY_CHIAN_LAST_ELEMENT"] = $arResult["NAME"];
    $cp->SetResultCacheKeys(array('MY_CHIAN_LAST_ELEMENT'));
    $arResult['MY_CHIAN_LAST_ELEMENT'] = $cp->arResult['MY_CHIAN_LAST_ELEMENT'];
}