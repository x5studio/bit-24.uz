<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<div class="modal-header"><i class="fa fa-times" style="float: right;" data-dismiss="modal" aria-hidden="true"></i>
	<h4>Менеджер CRM</h4>
</div>
<div class="modal-body">
	<div class="material">
		<div class="questions">
			<h3>Для кого?</h3>
			<p>Для тех, кто хочет настроить рабочий стол Битрикс24 для своих сотрудников и провести базовое обучение работе с основными инструментами портала, а так же освоить работу с CRM (полный цикл ведения продажи).</p>
		</div>
		<div class="questions">
			<h3>Что делаем?</h3>
			<p>Данный формат обучения носит индивидуальный характер. Эта услуга для клиентов, которые проходили у нас этап аналитики, постановки и настройки бизнес-процессов. Обучение проводится по ролям пользователей и включает в себя теоретическую и практическую части, исходя из бизнес-логики работы каждого отдельного клиента.<br><br>

Во время обучения наши специалисты, "за руку" проведут сотрудников по бизнес-процессу продажи, настроенному в Битрикс24 под вашу компанию, а так же сформировать для Ваших сотрудников бизнес-задачи для оттачивания практических навыков работы с учётом специфики Вашего бизнеса прямо во время обучения.
			</p></div>
		<div class="questions">
			<h3>Результаты</h3>
			<p>Приобретаемые навыки и инструменты, которыми мы научим Вас пользоваться: Все инструменты пакета «Базовый», Работа с лидом Конвертация лида, Работа со сделкой, Выставление коммерческих предложений, Выставление счетов, Конвертация сделки, Работа с Контактами, Работа с базой.</p>
		</div>
		<div class="service-bottom">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Формат:<img src="/local/templates/main/assets/img/service/com.png"
													class="img-responsive"></h3>
					<p>ВСТРЕЧА/SKYPE</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Стоимость<img src="/local/templates/main/assets/img/service/wallet.png"
														class="img-responsive"></h3>
					<h4>от 9 960 р.</h4>
				</div>
			</div>
			<div class="buy">
				<button type="button" class="btn btn-buy" data-target="#advanced"
								data-form-field-type="Консультационное внедрение: Менеджер CRM"
								data-toggle="modal" data-dismiss="modal"><a href="#" title="link">Купить</a></button>
			</div>
		</div>
	</div>
</div>