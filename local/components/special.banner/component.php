<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	Bitrix\Main\Page\Asset,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/branches.css");

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}

if ($this->StartResultCache())
{

	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError('Не установлен модуль iblock');
		return;
	}

	$arFile = array();

	$rsElement = \CIBlockElement::GetList(
		array("SORT" => "ASC"),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y"
		),
		false,
		false,
        array("ID", "NAME", "CODE", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_URL", "PROPERTY_ACCEPT_PAGE")
    );


    global $APPLICATION;

    //$request = \Bitrix\Main\Context::getCurrent()->getRequest();
    //$requestUri = $request->getRequestUri();  
    //preg_match('#^'.$arElement['ACCEPT_PAGE'].'$#', $requestUri)

    $arResult["ITEM"] = array();

    while ($arElement = $rsElement->GetNext())
	{
		if(CSite::InDir($arElement['PROPERTY_ACCEPT_PAGE_VALUE'])){
            $cookie = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getCookieRaw("close_banner_".$arElement["ID"]);
            if (empty($cookie)){
                $arResult["ITEM"] = $arElement;
            }
        }
	}

	$this->IncludeComponentTemplate();
}