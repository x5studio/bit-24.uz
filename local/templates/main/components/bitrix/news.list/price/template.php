<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<section class="price">
    <div class="container tabs-container-js">
        <h2 class="pfd-h2"><?$APPLICATION->IncludeFile($arResult['INCLUDE_DIR_LOCAL_PATH'].$arResult['H2_INCLUDE_FILE_NAME'])?></h2>
        <div class="price__buttons">
            <?$i = 0;?>
            <?foreach ($arResult["SECTIONS"] as $arSection):?>
                <button type="button" class="<?=($i==0 ? 'is-active' : '')?> tab-button-js" data-tab-button="<?=$i?>"><?=$arSection['NAME']?></button>
                <?$i++?>
            <?endforeach;?>
        </div>
        <div class="price__wrapper">
            <?$i = 0;?>
            <?foreach ($arResult["SECTIONS"] as $arSection):?>
                <ul class="price-table tab-js <?=($i==0 ? '' : 'visually-hidden')?>" data-tab="<?=$i?>">
                    <li>
                        <h3>Тариф</h3>
                        <div class="price-table__wrapper">
                            <p>Стоимость за месяц</p>
                        </div>
                    </li>
                    <?foreach ($arSection["ITEMS"] as $arItem):?>
                        <li>
                            <h3><?=$arItem["NAME"]?></h3>
                            <div class="price-table__wrapper">
                                <p class="price-table__desc">Стоимость за месяц</p>
                                <div class="price-table__price">
                                    <?=$arItem["DISPLAY_PROPERTIES"]["PRICE"]["DISPLAY_VALUE"]?>
                                    <svg width="9" height="20">
                                        <use xlink:href="#ruble"></use>
                                    </svg>
                                </div>
                                <a href="#" class="btn btn--border btn--buy" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Купить</a>
                            </div>
                        </li>
                    <?endforeach;?>
                </ul>
                <?$i++?>
            <?endforeach;?>
        </div>
    </div>
</section>

