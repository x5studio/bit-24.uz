if (typeof define === "function" && define.amd) {
    define([], function () {
        function ActionColor(container) {
            var _this = this;
            this.container = $(container).css({position: 'relative'});
            this.control = this.container.find('.j-action-control');
            this.control.bind({
                mouseenter: function () {
                    _this.control.stop().animate({opacity: 1}, {queue: false, duration: 300});
                },
                mouseleave: function () {
                    _this.control.stop().animate({opacity: 0}, {queue: false, duration: 200});
                },
                click: function () {
                    var href = _this.control.attr('href');
                    _this.control.parents('.boxes').siblings('.action-item').hide();
                    _this.control.parents('.boxes').siblings(href).fadeIn();
                    return false;
                }
            });
        }

        return ActionColor;
    });
}