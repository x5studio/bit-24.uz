<?
use Bitrix\Main\Localization\Loc;
use	Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;
use Bit\LeadsChatNotify\LeadMessageTable;

$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();
Loc::loadMessages($context->getServer()->getDocumentRoot()."/bitrix/modules/main/options.php");
Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

Loader::includeModule($module_id);

$aTabs = array(
	array(
		"DIV" 	  => "edit",
		"TAB" 	  => Loc::getMessage("BIT_ADO_OPTIONS_TAB_NAME"),
		"TITLE"   => Loc::getMessage("BIT_ADO_OPTIONS_TAB_NAME"),
		"OPTIONS" => array(
			Loc::getMessage("BIT_ADO_OPTIONS_TAB_COMMON"),
			array(
				"switch_on",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_SWITCH_ON"),
				"N",
				array("selectbox", array("Y" => Loc::getMessage("BIT_ADO_OPTIONS_YES"), "N" => Loc::getMessage("BIT_ADO_OPTIONS_NO")))
			),
			Loc::getMessage("BIT_ADO_OPTIONS_TAB_OPTIMIZE"),

			array(
				"image_optimize_on",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_OPTIMIZE_SWITCH"),
				"N",
				array("selectbox", array("Y" => Loc::getMessage("BIT_ADO_OPTIONS_YES"), "N" => Loc::getMessage("BIT_ADO_OPTIONS_NO")))
			),

			array(
				"image_optimize_mode",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_OPTIMIZE_MODE"),
				"OnGetFileSRC",
				array("selectbox", array("OnGetFileSRC" => "OnGetFileSRC", "OnEndBufferContent" => "OnEndBufferContent"))
			),

			array(
				"image_optimize_quality",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_OPTIMIZE_QUALITY"),
				"90",
				array("text", 20),
			),

			array(
				"image_optimize_maxwidth",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_OPTIMIZE_MAXWIDTH"),
				"",
				array("text", 20),
				'N',
			),
			array(
				"image_optimize_maxheight",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_OPTIMIZE_MAXHEIGHT"),
				"",
				array("text", 20),
				'N',
			),

			Loc::getMessage("BIT_ADO_OPTIONS_TAB_WEBP"),


			array(
				"image_webp_on",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_WEBP_SWITCH"),
				"",
				array("selectbox", array("Y" => Loc::getMessage("BIT_ADO_OPTIONS_YES"), "N" => Loc::getMessage("BIT_ADO_OPTIONS_NO")))
			),

			array(
				"image_webp_mode",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_WEBP_MODE"),
				"OnGetFileSRC",
				array("selectbox", array("OnGetFileSRC" => "OnGetFileSRC", "OnEndBufferContent" => "OnEndBufferContent"))
			),

			Loc::getMessage("BIT_ADO_OPTIONS_TAB_LAZYLOAD"),

			array(
				"image_lazyload_on",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_LAZYLOAD_SWITCH"),
				"",
				array("selectbox", array("Y" => Loc::getMessage("BIT_ADO_OPTIONS_YES"), "N" => Loc::getMessage("BIT_ADO_OPTIONS_NO")))
			),

			array(
				"image_lazyload_mode",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_LAZYLOAD_MODE"),
				"OnGetFileSRC",
				array("selectbox", array("OnGetFileSRC" => "OnGetFileSRC", "OnEndBufferContent" => "OnEndBufferContent"))
			),

			Loc::getMessage("BIT_ADO_OPTIONS_TAB_DEBUG"),

			array(
				"debug_on",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_DEBUG_SWITCH"),
				"",
				array("selectbox", array("Y" => Loc::getMessage("BIT_ADO_OPTIONS_YES"), "N" => Loc::getMessage("BIT_ADO_OPTIONS_NO")))
			),

			array(
				"manual_modif",
				Loc::getMessage("BIT_ADO_OPTIONS_TAB_MANUAL_MODIF"),
				"",
				array("text", 40),
			),

		)
	)
);


?>

<?
if(strlen($_POST['save']) > 0 && check_bitrix_sessid()){

	foreach ($aTabs as $aTab)
    {
        __AdmSettingsSaveOptions($module_id, $aTab['OPTIONS']);
    }
    LocalRedirect($APPLICATION->GetCurPageParam());
}
?>

<?
$tabControl = new CAdminTabControl(
	"tabControl",
	$aTabs
);

$tabControl->Begin();

?>
<style>

</style>
<form action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialcharsbx($module_id) ?>&lang=<?= LANGUAGE_ID ?>" method="post">

	<?
	foreach($aTabs as $aTab){

		if($aTab["OPTIONS"]){

			$tabControl->BeginNextTab();

			foreach($aTab["OPTIONS"] as $Option){

				__AdmSettingsDrawRow($module_id, $Option);

			}
		}
	}
	?>

    <? $tabControl->Buttons(array('btnApply' => false, 'btnCancel' => false, 'btnSaveAndAdd' => false)); ?>

	<?
	echo(bitrix_sessid_post());
	?>

</form>
<script>
	(function(e) {
		var matches = e.matches || e.matchesSelector || e.webkitMatchesSelector || e.mozMatchesSelector || e.msMatchesSelector || e.oMatchesSelector;
		!matches ? (e.matches = e.matchesSelector = function matches(selector) {
			var matches = document.querySelectorAll(selector);
			var th = this;
			return Array.prototype.some.call(matches, function(e) {
				return e === th;
			});
		}) : (e.matches = e.matchesSelector = matches);
	})(Element.prototype);

	(function(ELEMENT) {
		ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
		ELEMENT.closest = ELEMENT.closest || function closest(selector) {
			if (!this) return null;
			if (this.matches(selector)) return this;
			if (!this.parentElement) {return null}
			else return this.parentElement.closest(selector)
		};
	})(Element.prototype);

	(function(){

		var optimize = document.querySelector('input[name="manual_modif"]');
		var optimize_button = document.createElement('input');
		optimize_button.classList.add('adm-btn-save');
		optimize_button.type = 'button';
		optimize_button.value = 'Go';

		optimize.insertAdjacentElement('afterend', optimize_button);

		var optimize_info = document.createElement('span');
		optimize_button.insertAdjacentElement('afterend', optimize_info);

		var totalItems = 0;
		var limit = 5;

		var timeStart = null;

		function runTest () {

				var path = optimize.value;

				BX.ajax.runAction('bit:additionaloptimizer.api.Optimize.manual',
				{
					data:{	
							'path' : path,
							'limit': 2,
							'offcet': totalItems,
						}

				}).then(function(response) {

						if(response.status == 'success' && !!response.data){

							totalItems += response.data.optimized;
							
							if(response.data.optimized){

								optimize_info.innerText = 'Обработано: ' + totalItems + ' картинок';

								runTest();

							} else {

								optimize_info.innerText = 'Завершено. Обработано: ' + totalItems + ' картинок';

							}

						}

				});

		}

		optimize_button.addEventListener('click', runTest)

	})()
</script>
<?
$tabControl->End();
?>
