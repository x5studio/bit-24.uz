if (typeof define === "function" && define.amd) {
    define(['router'], function () {
        function SwitchMultiple(container) {
            var _this = this;
            this.container = $(container);
            this.filterItem = this.container.find('.switch-multiple-item');
            this.classes = '';
            this.filterItem.find('a').each(function (i) {
                if (i) _this.classes += ',';
                _this.classes += '.' + $(this).attr('href').substr(1); // удаляем решетку, потому что для отображения элементов в SwitchMultiple используются классы
            });

            this.filterItem.each(function (filterItemIndex) {
                var toggler = $(this).find('.switch3');
                var a = $(this).find('a'),
                    link = [2],
                    filterItem = $(this);
                link[0] = $(this).find('a').eq(0);
                link[1] = $(this).find('a').eq(1);

                var classes = '';
                a.each(function (i) {
                    if (i) classes += ',';
                    classes += '.' + $(this).attr('href').substr(1);
                });

                a.each(function (i) {
                    var switchClass = '.' + $(this).attr('href').substr(1);

                    $(this).bind('active', function () {

                        var siblingClass = '.' + _this.filterItem.not(filterItem).find('a:not(.switch-active)').attr('href').substr(1);
                        var siblingClassActive = '.' + _this.filterItem.not(filterItem).find('a.switch-active').attr('href').substr(1);

                        if (i == 0) {
                            filterItem.removeClass('toggle');
                        } else {
                            filterItem.addClass('toggle');
                        }

                        j = (i == 0) ? 1 : 0;

                        $(this).addClass('switch-active').data('active', true);
                        link[j].removeClass('switch-active').data('active', false);
                        rHelperData(link[j]);

                        $(classes).each(function () {
                            var param0 = true, param1 = true;

                            if (!$(this).is(switchClass)) param0 = false;

                            if (!$(this).is(siblingClassActive)) param1 = false;
                            if (!$(this).is(siblingClass) && !$(this).is(siblingClassActive)) param1 = true;

                            if (param0 & param1) {
                                $(this).show();
                            } else {
                                $(this).hide();
                            }
                        });
                    });
                    $(this).click(function () {
                        if (!$(this).is('.switch-active')) {
                            $(this).trigger('active');
                            rHelperClick($(this));
                        }
                        return false;
                    });
                });
                toggler.click(function () {
                    a.not('.switch-active').click();
                });
            });

            this.filterItem0 = _this.filterItem.eq(0);
            this.filterItem1 = _this.filterItem.eq(1);

            var a0Inactive = _this.filterItem0.find('a:not(.switch-active)'),
                a0Active = _this.filterItem0.find('a.switch-active'),
                a1Inactive = _this.filterItem1.find('a:not(.switch-active)'),
                a1Active = _this.filterItem1.find('a.switch-active');

            if (a0Inactive.data('active')) {
                var temp = a0Active.data('active', false).removeClass('switch-active');
                a0Active = a0Inactive.addClass('switch-active');
                a0Inactive = temp;
            } else {
                a0Active.data('active', true);
                a0Inactive.data('active', false);
            }
            if (a1Inactive.data('active')) {
                var temp = a1Active.data('active', false).removeClass('switch-active');
                a1Active = a1Inactive.addClass('switch-active');
                a1Inactive = temp;
            } else {
                a1Active.data('active', true);
                a1Inactive.data('active', false);
            }

            a0Active.trigger('active');
            a1Active.trigger('active');
        }

        return SwitchMultiple;
    });
}