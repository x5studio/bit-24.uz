<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Отраслевая экспертиза. Список",
	"DESCRIPTION" => "Отраслевая экспертиза. Список",
	"ICON" => "/images/news_line.gif",
	"SORT" => 110,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "expertise",
			"NAME" => "Отраслевая экспертиза"
		)
	),
);

?>