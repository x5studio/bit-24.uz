<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="modal-header">
	<i class="fa fa-times" style="float: right;" data-dismiss="modal" aria-hidden="true"></i>
	<h4>ЭКСПРЕСС-ВСТРЕЧА</h4>
</div>
<div class="modal-body">
	<div class="material">
		<div class="questions">
			<h3>Для кого?</h3>
			<p>
				Для тех, кто хочет понять, как получить для своей компании максимальную выгоду от использования Битрикс24.
			</p>
		</div>
		<div class="questions">
			<h3>Что делаем?</h3>
			<p>
				На Экспресс-встрече руководитель проектов WEB-студии «Первый БИТ» расскажет Вам о функционале Битрикс24, приведёт бизнес-кейсы по его использованию, раскроет лучшие практики внедрения корпоративного портала опираясь на наш большой опыт работы в данном направлении.<br>
 <br>
				 Вы узнаете как получить от Битрикс24 максимальную выгоду. Наш специалист проанализирует Ваши бизнес-потребности и предложит способы их реализации с использованием продукта Битрикс24. В итоге Вы получите зарегистрированный портал в «Бесплатной» редакции, а так же, мы сформируем для Вас индивидуальный предварительный план его внедрения в Вашей компании.<br>
 <br>
				Предоставлемый план содержит в себе описание конечного эффекта от внедрения портала и стоимость внедрения продукта.
			</p>
		</div>
<div class="questions">
			<h3>Результаты</h3>
			<p>Портал зарегистрирован (тариф «Бесплатный»); Предварительный план внедрения портала.</p>
		</div>
		<div class="service-bottom">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12 block">
					<h3>Формат:<img src="/local/templates/main/assets/img/service/com.png" class="img-responsive"></h3>
					<p>
						ВСТРЕЧА
					</p>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 block">
					<h3>Стоимость<img src="/local/templates/main/assets/img/service/wallet.png" class="img-responsive"></h3>
					<h4>от 9 960 р.</h4>
				</div>
			</div>
			<div class="buy">
				<button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Консультационное внедрение: Экспресс-встреча" data-toggle="modal" data-dismiss="modal"><a href="#" title="link">Купить</a></button>
			</div>
		</div>
	</div>
</div>
<br>