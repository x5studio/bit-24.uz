if (typeof define === "function" && define.amd) {
    define(['tools/tools'], function () {

        function FormFileMng(p) {
            var _this = $(this);
            this.root = $(p.root);
            this.btn = $(p.btn, _this.root);
            this.fileLoadedCtn = $('.j-file-custom-loaded', _this.root);
            this.fileInput = $('input[type=file]', _this.root);

            if($('.jf-files').length > 0){
                this.files = $('.jf-files', _this.fileLoadedCtn);
            }
        }

        FormFileMng.prototype.showFile = function () {
            var _this = this;
            if(_this.files){
                var fileName = _this.fileInput[0].files[0].name;
                var fileSize = byteConvert(_this.fileInput[0].files[0].size);
                var fileHTML = "<i class='fa fa-file-o'></i><b>"+fileName+"</b><span>"+fileSize+"</span>";
                _this.files.html(fileHTML);
                _this.fileLoadedCtn.show().animate({opacity: 1}, 1000);
            }
        }

        FormFileMng.prototype.init = function () {
            var _this = this;
            _this.fileInput.on('change', function () {
                _this.showFile();
            });
        }

        return FormFileMng;
    });
}