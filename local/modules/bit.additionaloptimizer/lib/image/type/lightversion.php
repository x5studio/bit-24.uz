<?

namespace Bit\AdditionalOptimizer\Image\Type;

use Bit\AdditionalOptimizer\Image\Optimizer\OptimizerChain;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Jpegoptim;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Jpegtran;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Optipng;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Magick;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Pngquant;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Cwebp;
use Bit\AdditionalOptimizer\Image\BinHelper;

class LightVersion extends Base  {

    protected $name = 'aolvmod.{name}';

    public function createImage($additionalParams = []) {

        if(!$this->pathToImage) throw new \Exception("Original image not set");

        $optimizerChain = new OptimizerChain;

        $additionalParams = !$additionalParams ? $this->additionalParams : $additionalParams;

        if(isset($additionalParams['max_width']) || isset($additionalParams['max_height'])){
                    
            $maxParam = '-resize ';
            $maxParam .= isset($additionalParams['max_width']) ? (int)$additionalParams['max_width'] : '';
            $maxParam .= 'x';
            $maxParam .= isset($additionalParams['max_height']) ? (int)$additionalParams['max_height'] : '';
            $maxParam .= '\>';

            $optimizerChain->addOptimizer((new Magick([
                $maxParam,
            ]))->setBinaryPath(BinHelper::getExecBinPath('magick')));
        }

        $optimizerChain->addOptimizer((new Magick([
            '-quality 1',
            '-density 72x72',
            $this->getImagePath()
        ]))->setBinaryPath(BinHelper::getExecBinPath('magick')));

        $optimizerChain->addOptimizer((new Cwebp([
            '-m 6',
            '-pass 10',
            '-mt',
            '-q 5',
            $this->pathToImage.' -o '.$this->getImagePath()
        ]))->setBinaryPath(BinHelper::getExecBinPath('cwebp')));

        $optimizerChain->optimize($this->pathToImage);

        $this->cache = [];


        return $this->getImageExist();

    }


}