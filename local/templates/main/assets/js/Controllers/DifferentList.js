if (typeof define === "function" && define.amd) {
    define([], function () {
        function DifferentList(container, contextContainer) {
            var _this = this;

            this.control = $(container);
            this.contextContainer = $(contextContainer || document);
            this.list = $(_this.control.attr('href'));
            this.listParent = this.list.parent();
            this.parentIsBody = this.listParent.is('body');
            this.close = this.list.find('.j-city-close, .j-product-close');
            this.choose_link_selector = '.j-new-choose';
            this.choose_link = this.list.find(this.choose_link_selector);

            this.width = this.list.outerWidth();

            this.listShow = function (control) {
                _this.listPosition(control);

                if (!_this.parentIsBody) {
                    _this.list.appendTo('body');
                }

                _this.list.show().css({opacity: 0}).toggleClass('city-list-active').stop().animate({opacity: 1}, {
                    queue: false,
                    duration: 300
                });
                $("body").bind({
                    mouseup: function (e) {
                        if (!$(e.target).parents().is(_this.list) && !$(e.target).is(_this.list) && !$(e.target).is(_this.control) && !$(e.target).parents().is(_this.control)) {
                            _this.control.filter('.city-active').removeClass('city-active');
                            _this.listHide();
                        }
                    },
                    keyup: function (e) {
                        if (e.keyCode == '27') {
                            _this.control.filter('.city-active').removeClass('city-active');
                            _this.listHide();
                        }
                    }
                });
            }
            this.listHide = function () {
                _this.list.stop().animate({opacity: 0}, {
                    queue: false, duration: 200, complete: function () {
                        _this.list.hide();
                        if (!_this.parentIsBody) {
                            _this.list.appendTo(_this.listParent);
                        }
                    }
                });
                $("body").unbind('mouseup', 'keyup');
            }
            this.listPosition = function (control) {
                var windowWidth, top, left;
                if(this.contextContainer.offset())
                    windowWidth = this.contextContainer.offset().left + this.contextContainer.width();
                else
                    windowWidth = this.contextContainer.width();
                
                left = control.offset().left;
                if (left + _this.width > windowWidth) {
                    left = windowWidth - _this.width;
                }
                if (control.is('.header-city')) {
                    top = 39;
                } else if (control.parents('.section-title').length) {
                    top = control.parents('.section-title').offset().top + control.parents('.section-title').outerHeight();
                    left = control.parents('.section-title').offset().left;
                } else if (control.parents('.j-popup-select').length) {
                    top = control.parents('.j-popup-select').offset().top + 49;
                    left = control.parents('.j-popup-select').offset().left - 60;
                } else {
                    top = control.offset().top + control.height();
                }
                _this.list.css({top: top, left: left});
            }

            $(window).resize(function () {
                var cityActive = _this.control.filter('.city-active');
                if (cityActive.length) {
                    _this.listPosition(cityActive);
                }
            });

            this.control.each(function () {
                var control = $(this);
                control.click(function () {
                    if (control.is('.city-active')) {
                        control.removeClass('city-active');
                        _this.listHide();
                    } else {
                        _this.control.filter('.city-active').removeClass('city-active');
                        control.addClass('city-active');
                        _this.listShow(control);
                    }
                    return false;
                });
            });
            this.close.click(function () {
                _this.control.filter('.city-active').removeClass('city-active');
                _this.listHide();
            });
            if (this.choose_link.length > 0) {
                this.list.on("click", this.choose_link_selector, function (event) {
                    var $event_target = $(event.target);

	                _this.control.find("span").text($event_target.text());
	                _this.control.find("span").attr("title", $event_target.text());
	                _this.control.find("span").attr("item-id", $event_target.attr("item-id"));
	                _this.control.filter('.city-active').removeClass('city-active');
	                _this.listHide();
	            });
            }
        }

        return DifferentList;
    });
}