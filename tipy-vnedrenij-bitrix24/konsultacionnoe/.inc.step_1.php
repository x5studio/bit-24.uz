<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="mattop">
	<h2>Шаг 1 — Моделирование</h2>
	<h3>Проанализируем ваш бизнес и докопаемся до его сути</h3>
</div>
<div class="waranty">
	<h3>Гарантируем:</h3>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<ul>
				<li>Максимальную реализацию бизнес-процессов через базовый функционал Битрикс24;</li>
				<li>Реальное начало работы и использование Битрикс24;</li>
				<li>Определение чётких рамок дальнейшего развития проекта.</li>
			</ul>
		</div>
	</div>
</div>
<div class="include">
	<h3>Что входит:</h3>
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<ul>
				<li>Обзор продукта от нашего сертифицированного специалиста;</li>
				<li>План внедрения в Вашу компанию с понятными сроками и стоимостью доработок;</li>
			</ul>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12">
			<ul>
				<li>Подключение сервиса Битрикс24 в облаке (тариф "Проект");</li>
				<li>Установка логотипа, заполнение информации о компании и настройка основных параметров портала.&nbsp;</li>
			</ul>
		</div>
	</div>
</div>
<div class="step-bottom hidden-xs">
	<div class="row">
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Экспресс-встреча</h3>
 <strong>Для кого подходит</strong>
			<p>
				 Для тех, кто хочет понять, как получить для своей компании максимальную выгоду от использования Битрикс24.
			</p>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h4>от 9 960 руб.</h4>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
 					<button type="button" class="btn btn-default submit" data-target="#modal-more-1"
									data-type="" data-toggle="modal">ПОДРОБНЕЕ</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Моделирование</h3>
 <strong>Для тех, кто хочет</strong>
			<p>
				 Быстро и грамотно запустить в своей компании работу с Битрикс24, а также интегрироваться с учётными системами по &nbsp;оптимальным сценариям.
			</p>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h4>от 48 000 руб.</h4>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
 					<button type="button" class="btn btn-default submit" data-target="#modal-more-2"
									data-type="" data-toggle="modal">ПОДРОБНЕЕ</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Бизнес-моделирование</h3>
 <strong>Для кого подходит</strong>
			<p>
				 Для тех, кто хочет повысить эффективность своего бизнеса с помощью аудита и корректировок бизнес-процессов компании с учётом опыта и лучших практик&nbsp;в своей отрасли.
			</p>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h4>от 96 000 руб.</h4>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
 					<button type="button" class="btn btn-default submit" data-target="#modal-more-3"
									data-type="" data-toggle="modal">ПОДРОБНЕЕ</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="step1-slider">
	<div class="row">
		<div class="hidden-lg hidden-md hidden-sm col-xs-12">
			<div id="1Carousel" class="carousel carousel-showmanymoveone slide" data-ride="carousel">
				 <!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#1Carousel" data-slide-to="0" class="active"></li>
					<li data-target="#1Carousel" data-slide-to="1"></li>
					<li data-target="#1Carousel" data-slide-to="2"></li>
				</ol>
				 <!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<div class="col-lg-4 col-xs-12 col-md-4 col-sm-6">
							<div class="project1">
								<h3>Экспресс-встреча</h3>
 <strong>Для кого</strong>
								<p>
									 Для тех, кто хочет понять, как получить для своей компании максимальную выгоду от использования Битрикс24
								</p>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<h4>от 9 960 руб</h4>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
 										<button type="button" class="btn btn-default submit" data-target="#modal-more-1"
														data-type="" data-toggle="modal">ПОДРОБНЕЕ</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="col-lg-4 col-xs-12 col-md-4 col-sm-6">
							<div class="project1">
								<h3>Моделирование</h3>
 <strong>Для тех, кто хочет</strong>
								<p>
									 Быстро и правильно запустить в своей компании работу с Битрикс24. Интегрироваться с учётными системами по максимально эффективным/ оптимальным сценариям.
								</p>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<h4>от 48 000 руб</h4>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
 										<button type="button" class="btn btn-default submit" data-target="#modal-more-2"
														data-type="" data-toggle="modal">ПОДРОБНЕЕ</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="col-lg-4 col-xs-12 col-md-4 hidden-sm">
							<div class="project1">
								<h3>Бизнес-моделирование</h3>
 <strong>Для кого</strong>
								<p>
									 Для тех, кто хочет повысить эффективность своего бизнеса за счет аудита и корректировок бизнес-процессов своей компании, используя опыт и лучшие практики в своей отрасли
								</p>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<h4>от 96 000 руб</h4>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
 										<button type="button" class="btn btn-default submit" data-target="#modal-more-3"
														data-type="" data-toggle="modal">ПОДРОБНЕЕ</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				 <!-- Left and right controls --> <a class="left carousel-control hidden-sm hidden-lg hidden-md col-xs-6" href="#1Carousel" role="button" data-slide="prev"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> <span class="sr-only">Previous</span> </a> <a class="right carousel-control hidden-sm hidden-lg hidden-md col-xs-6" href="#1Carousel" role="button" data-slide="next"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span class="sr-only">Next</span> </a>
			</div>
		</div>
	</div>
</div>
 <br>