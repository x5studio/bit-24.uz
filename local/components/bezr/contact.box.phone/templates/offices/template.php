<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


?>

<div class="boxes<?if($arParams['FLOATING_BOX'] == 'Y'):?> j-floating<?endif?>">
	<div id="contact-right-box-container" class="box box-thin box-1 floating-box">
		<?
		$frame = $this->createFrame("contact-right-box-container", false)->begin();
		$count = count($arResult["ITEMS"]);
		if($arParams['SHOW_FREE_REGION'] == 'Y') {
			$count = 'FREE';
		}
		if($count == 0) {
			$template = 'item-n';
		} elseif($count == 1) {
			$template = 'item-1';
		} elseif($count == 2) {
			$template = 'item-2';
		} elseif($count == 3) {
			$template = 'item-3';
		} elseif($count > 3 && $count < 10) {
			$template = 'item-4';
		} else {
			$template = 'item-n';
		}
		require($_SERVER['DOCUMENT_ROOT'].$this->GetFolder().'/'.$template.'.php');
		$frame->beginStub();?>
		<div class="loader loader17 loader-easy float-right"></div>
		<?$frame->end();?>
	</div>
	<script type="text/javascript">
		(function () {
			var boxOfficesContainer = $('#contact-right-box-container');
			var citySelectContainer = $('.header .header-city-select');
			var mobileHeaderContainer = $('.mobile-header-wrapper');
			var width = $(document).width();
			if (width < 768) {
				boxOfficesContainer.insertAfter(mobileHeaderContainer);
				citySelectContainer.insertAfter(boxOfficesContainer);
			}
		})();
	</script>
	<div class="clear"></div>
</div>