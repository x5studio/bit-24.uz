<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
	global $APPLICATION;
	$aMenuLinksExt = $APPLICATION->IncludeComponent(
		"bitrix:menu.sections",
		"",
		Array(
			"IBLOCK_ID" => "2",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600",
			"CACHE_NOTES" => ""
		),
	false
	);
	$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>