<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (method_exists($this, 'setFrameMode')) {
	$this->setFrameMode(true);
}

if ($arResult['ACTION']['status']=='error') {
	ShowError($arResult['ACTION']['message']);
} elseif ($arResult['ACTION']['status']=='ok') {
	ShowNote($arResult['ACTION']['message']);
}
?>
<section class="subscribe-news text-center">
    <div class="subscribe-news-form-wrap">
        <div class="corner-bg">
            <span class="title">Бесплатная подписка на новости бит24.ru</span>
            <p>Никакого спама - только максимально полезные статьи, написанные с любовью</p>
            <p id="asd_subscribe_res" style="display: none;"></p>
	<form action="<?= POST_FORM_ACTION_URI?>" method="post" id="asd_subscribe_form" class="clearfix">
		<?= bitrix_sessid_post()?>
		<input type="hidden" name="asd_subscribe" value="Y" />
		<input type="hidden" name="charset" value="<?= SITE_CHARSET?>" />
		<input type="hidden" name="site_id" value="<?= SITE_ID?>" />
		<input type="hidden" name="asd_rubrics" value="<?= $arParams['RUBRICS_STR']?>" />
		<input type="hidden" name="asd_format" value="<?= $arParams['FORMAT']?>" />
		<input type="hidden" name="asd_show_rubrics" value="<?= $arParams['SHOW_RUBRICS']?>" />
		<input type="hidden" name="asd_not_confirm" value="<?= $arParams['NOT_CONFIRM']?>" />
		<input type="hidden" name="asd_key" value="<?= md5($arParams['JS_KEY'].$arParams['RUBRICS_STR'].$arParams['SHOW_RUBRICS'].$arParams['NOT_CONFIRM'])?>" />
			<input type="email" class="form-control" name="asd_email" placeholder="Ваше e-mail" value="" />
        <button type="submit" name="asd_submit" id="asd_subscribe_submit" class="btn btn-default submit"><?=GetMessage("ASD_SUBSCRIBEQUICK_PODPISATQSA")?></button>
		<?if (isset($arResult['RUBRICS'])):?>
			<br/>
			<?foreach($arResult['RUBRICS'] as $RID => $title):?>
				<input type="checkbox" name="asd_rub[]" id="rub<?= $RID?>" value="<?= $RID?>" />
				<label for="rub<?= $RID?>"><?= $title?></label><br/>
			<?endforeach;?>
		<?endif;?>
	</form>
            <p class="agree-notice">Нажимая кнопку Вы соглашаетесь получать от нас полезные письма о инструментах Битрикс24.</p>
        </div>
    </div>
</section>