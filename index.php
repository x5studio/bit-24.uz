<?

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("form_title", "Заказать внедрение CRM Битрикс24");

$APPLICATION->SetPageProperty("description", "Внедрение корпоративных порталов и CRM Битрикс24. Решения под разные типы бизнеса. Первый Бит — крупнейший федеральный партнер 1С-Битрикс. Работаем с 1997 года.");
$APPLICATION->SetPageProperty("keywords", "внедрение битрикс24, стоимость внедрения битрикс24, внедрение битрикс 24, внедрение битрикс, внедрение bitrix, внедрение битрикс 24 под ключ, внедрить битрикс 24, внедрение 1с битрикс 24, внедрение crm битрикс24, внедрение crm системы битрикс 24, внедрение битрикс 24 цена");
$APPLICATION->SetPageProperty("title", "Внедрение Битрикс24 и поддержка | Первый Бит");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Внедрение корпоративных порталов Битрикс24");
$APPLICATION->AddHeadString('
<!-- Marquiz script start --> <script src="//script.marquiz.ru/v1.js" type="application/javascript"></script>  <!-- Marquiz script end -->
');

?><?$APPLICATION->IncludeComponent(
	"main.slider",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "7"
	),
	false,
	["ACTIVE_COMPONENT"=>"N" ]
);?> <section class="intro">
<div class="container clearfix">
	<div class="intro_descr">
		 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	".default",
	Array(
		"AREA_FILE_SHOW" => "file",
		"EDIT_TEMPLATE" => "",
		"PATH" => "include/main-intro.php"
	)
);?> <a href="#" class="btn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Получить консультацию</a>
	</div>
	<div class="icons_block hidden-xs hidden-sm hidden-md">
 <img src="/local/templates/main/assets/img/bubble_1.png" alt="" class="small_bubble bubble bubble_1" data-title="КОМПАНИЯ" data-content="Помогает работать вместе, улучшает коммуникацию и скорость обмена информацией">
 <img src="/local/templates/main/assets/img/bubble_2.png" alt="" class="small_bubble bubble bubble_2" data-title="ЗАДАЧИ И ПРОЕКТЫ" data-content="Помогают контролировать и успевать вовремя">
 <img src="/local/templates/main/assets/img/bubble_3.png" alt="" class="small_bubble bubble bubble_3" data-title="CRM" data-content="Помогает увеличить объем заказов">
 <img src="/local/templates/main/assets/img/bubble_4.png" alt="" class="small_bubble bubble bubble_4" data-title="ОТКРЫТЫЕ ЛИНИИ" data-content="Помогают общаться с клиентами. Все мессенджеры, чаты и почта в одном месте">
 <img src="/local/templates/main/assets/img/bubble_5.png" alt="" class="small_bubble bubble bubble_5" data-title="САЙТЫ" data-content="Новый канал продаж для вашего бизнеса">
		<div class="bubble big_bubble">
 <img src="/local/templates/main/assets/img/big_bubble.png" alt="">
			<div class="bubble_descr">
			</div>
		</div>
	</div>
	 <script>
		$(document).ready( function(){

			var bubbles = $('.small_bubble');


			var current = 0;
			var length = bubbles.length;

			var timeOut;
			function dtimeout (Func, Time) {
				timeOut = setTimeout(Func, Time);
	            return timeOut;
			}

			clearTimeout(timeOut);

			if(bubbles.length > 0){

				bubbles.mouseenter(function() {

					current = 0;
					clearTimeout(timeOut);

					bubbles.removeClass('hoverBubble')

					$('.big_bubble img').attr({ src: '/local/templates/main/assets/img/big_bubble_empty.png' });

					$('.bubble_descr').addClass('onHoverBigBubble').html('<span class="bubble_title">' + $(this).attr('data-title') +'</span><span class="bubble_content">' + $(this).attr('data-content') +'</span></div>');

				});

				bubbles.mouseleave(function() {

					clearTimeout(timeOut);

					bubbles.removeClass('hoverBubble')

					$('.big_bubble img').attr({ src: '/local/templates/main/assets/img/big_bubble.png' });

					$('.bubble_descr').removeClass('onHoverBigBubble').html('');


					dtimeout (bubblesHover, 2000)

				});



				function bubblesHover() {

					bubbles.removeClass('hoverBubble')

					$('.big_bubble img').attr({ src: '/local/templates/main/assets/img/big_bubble.png' });

					$('.bubble_descr').removeClass('onHoverBigBubble').html('');

					if($(bubbles[current]).length){

						$(bubbles[current]).addClass('hoverBubble')

						$('.big_bubble img').attr({ src: '/local/templates/main/assets/img/big_bubble_empty.png' });


						$('.bubble_descr').addClass('onHoverBigBubble').html('<span class="bubble_title">' + $(bubbles[current]).attr('data-title') +'</span><span class="bubble_content">' + $(bubbles[current]).attr('data-content') +'</span></div>');
					}

					if(current + 1 > length){

						current = 0;

					} else {

						current ++

					}

					dtimeout (bubblesHover, 2000)

				}

				dtimeout (bubblesHover, 2000)

			}


		});
	</script>
</div>
 </section>
<?$APPLICATION->IncludeComponent(
	"studiobit:main.services",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "18"
	)
);?>

<?$APPLICATION->IncludeComponent(
	"main.why",
	".default",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_ID" => "5",
		"LINK_ALL" => "/vozmozhnosti-bitrix24/crm/"
	)
);?>


<section class="buy-license" style="margin-bottom:20px;">
<h2 class="fields-heading">Тарифы и лицензии</h2>
<div class="container">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#cloud" aria-controls="cloud" role="tab" data-toggle="tab">Облачная версия</a></li>
		<li role="presentation"><a href="#box" aria-controls="box" role="tab" data-toggle="tab">Коробочная версия</a> </li>
	</ul>
	 <!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="cloud">
							 <?$APPLICATION->IncludeComponent(
                                "buy.tariff",
                                "",
                                Array(
									"CACHE_TIME" => "360000",
									"CACHE_TYPE" => "A",
									"IBLOCK_ID" => "10",
									"ACTIVE_PRICE" => "PRICEFORTWOYEARS",
									"EXCLUDE_PROPS" => array('DESC', 'ICON', 'TYPE'),
									"SHOW_ELEMENT_NAME" => true,
									"ADDITIONAL_PRICES" => array(
											"PRICEFORMONTH" => "на 1 месяц",
											"PRICEFORTHREEMONTHS" => "на 3 месяца",
											"PRICEFORHALFAYEAR" => "на полгода",
											"PRICEFORYEAR" => "на год",
											"PRICEFORTWOYEARS" => "на 2 года",
									),
									'BUTTON_TEXT' => 'Подробнее',
									'BUTTON_IS_LINK' => '/uslugi/kupit-licenziju-bitrix24/',
                                    "SHOW_MOBILE_PROP" => [
                                        'PRICE',
                                        'USERS',
                                        'OFIS',
                                        'CRM',
                                        'TASKS_PROJECTS'
                                    ],
                                )
                            );?>
			<div class="row oboznachenia">
				<div class="col-md-3">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
					нет
				</div>
				<div class="col-md-3">
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					базовый набор возможностей
				</div>
				<div class="col-md-3">
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					расширенный
				</div>
				<div class="col-md-3">
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					профессиональный
				</div>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane clearfix" id="box">
			 <?$APPLICATION->IncludeComponent(
				"buy.tariff",
				"",
				Array(
					"CACHE_TIME" => "360000",
					"CACHE_TYPE" => "A",
					"IBLOCK_ID" => "11",
					'BUTTON_TEXT' => 'Подробнее',
					'BUTTON_IS_LINK' => '/uslugi/kupit-licenziju-bitrix24/',
					'ALL_TARIFFS_TITLE' => 'Редакции',
					"EXCLUDE_PROPS" => array('TYPE'),



				)
			);?> 
		</div>
	</div>
	</div>
 </section>

<?$APPLICATION->IncludeComponent(
	"expertise.main",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "3"
	)
);?>



<section class="main-clients">
    <div class="container">
        <div class="row why-row">
            <div class="col-md-6 col-sm-10">
                <h2 class="why-heading pull-left">Наши клиенты</h2>
            </div>
            <div class="col-md-6 col-sm-2 hidden-xs">
                <a href="/nashi-klienty/" class="btn btn-why pull-right">Подробнее</a>
            </div>
        </div>
    </div>
    <?php
    $APPLICATION->IncludeComponent(
        "client.list",
        "",
        Array(
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "COUNT" => 6,
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "IBLOCK_ID" => "8",
            'FRONTPAGE_STYLE' => 'Y',
        )
    );
    ?>
    <div class="hidden-lg hidden-md hidden-sm col-xs-12 detline">
        <a class="btn btn-why" href="/nashi-klienty/">Подробнее</a>
    </div>
</section>

<?$APPLICATION->IncludeComponent(
	"main.services",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "4"
	)
);?>

<?$APPLICATION->IncludeComponent(
	"blog.main",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "2"
	)
);?>

<div style="visibility: hidden;" class="marquiz-pops marquiz-pops_position_bottom-left marquiz-pops_blicked marquiz-pops_shadowed marquiz-pops_position" ><a class="marquiz-pops__body" href="#popup:marquiz_5de6172e4734030045bdb536" data-marquiz-pop-text-color="#ffffff" data-marquiz-pop-background-color="#d34085" data-marquiz-pop-svg-color="#fff" data-marquiz-pop-close-color="#fff" data-marquiz-pop-color-pulse="rgba(211, 64, 133, 0.4)" data-marquiz-pop-color-pulse-alpha="#d34085" data-marquiz-pop-delay="2s" data-marquiz-pop="true"><span class="marquiz-pops__icon"></span><span class="marquiz-pops__content"><span class="marquiz-pops__content-title">Пройти тест</span><span class="marquiz-pops__content-text">&laquo;Рассчитать стоимость внедрения&raquo;</span></span></a></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>