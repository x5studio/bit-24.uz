require(['Controllers/Loc'], function (Loc) {
    var l = Loc.getWithPrefix('PB_CONTACTS_CT_CB_PHO_JS_');

    $(function () {
        track([l('CBLOCK'), l('OFFICE_SHOW'), $(".top-phone").data("name"), {'nonInteraction': 1}]);
    });
});