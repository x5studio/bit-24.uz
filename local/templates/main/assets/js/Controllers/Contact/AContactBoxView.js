/**
 * Created by Павел on 29.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(['jquery'], function ($) {
        function AContactBoxView(container, options) {
            this.container = $(container);

            this.options = $.extend(true, {}, AContactBoxView.defaultOptions, options);

            this.templateBox = this.container.find(this.options['templateBox']).detach();
            this.models = {};
            this.prototypes = {};
            this.contMng = null;
        }

        AContactBoxView.defaultOptions = {
            templateBox: '.box-offices-themes',
            themePrefix: '.box-offices-theme'
        };

        AContactBoxView.prototype.getContactManager = function () {
            return this.contMng;
        };

        AContactBoxView.prototype.setContactManager = function (contactManager) {
            this.contMng = contactManager;
        };

        AContactBoxView.prototype.setOptions = function (options) {
            if (typeof options != 'object') {
                throw new TypeError();
            }
            $.extend(this.options, options);
        };

        AContactBoxView.prototype.getModel = function (key) {
            return this.models[key];
        };

        AContactBoxView.prototype.setModel = function (key, item) {
            this.models[key] = item;
        };

        AContactBoxView.prototype.getView = function () {
            return this.container;
        };

        AContactBoxView.prototype.setView = function (container) {
            this.container = $(container);
        };

        AContactBoxView.prototype.getTemplate = function (name, original) {
            var o = this;
            if (!o.prototypes[name] || original) {
                var templateName = o.options['themePrefix'] + "-" + name;
                var $template = o.templateBox.find(templateName);
                if ($template.size() > 0) {
                    if (original) {
                        return $template;
                    } else {
                        o.prototypes[name] = $template;
                    }
                } else {
                    throw new ReferenceError("На странице отстутствует шаблон " + templateName);
                }
            }
            return o.prototypes[name].clone();
        };

        AContactBoxView.prototype.compileOfficeItem = function (container, mOffice) {
            container.filter('.box-offices-item').add(container.find('.box-offices-item')).data('itemId', parseInt(mOffice['ID']));
            container.find('.box-office-name').text(mOffice['NAME']);
            if (mOffice['VIEW_LINK'] != 'N' && mOffice['DETAIL_PAGE_URL']) {
                container.filter('.box-office-link').add(container.find('.box-office-link')).attr('href', mOffice['DETAIL_PAGE_URL']);
            }
            var containerOfficePhone = container.find('.box-office-phone');
            if(containerOfficePhone.find('a.box-phone-href').length > 0) {
            	containerOfficePhone = containerOfficePhone.find('a.box-phone-href');
            }
            containerOfficePhone.text(mOffice['PROPERTIES']['phone']['VALUE']);
            if(containerOfficePhone.hasClass('box-phone-href')) {
            	containerOfficePhone.attr('href', 'tel:+'+mOffice['PROPERTIES']['phone']['VALUE'].replace(/[^0-9]/g, ''));
            }
        };

        AContactBoxView.prototype.makeOfficeItem = function (mOffice) {
            var o = this;
            var templateItem = o.getTemplate('office');
            o.compileOfficeItem(templateItem, mOffice);
            return templateItem;
        };

        AContactBoxView.prototype.renderOfficeList = function (container) {
            var o = this;
            var mOffices = o.getModel('offices');
            container = $(container);

            for (var i in mOffices) {
                var mOffice = mOffices[i];
                var $officeItem = o.makeOfficeItem(mOffice);
                container.append($officeItem);
            }

            o.getView().trigger(new $.Event('contactBox.view.renderOfficeList', {}));
        };

        AContactBoxView.prototype.renderContent = function () {
            var o = this;
            var $content = o.getView().find('.box-content');
            var $officeList = $content.find('.offices-list');
            o.renderOfficeList($officeList);
            o.getView().trigger(new $.Event('contactBox.view.renderContent', {}));
        };

        AContactBoxView.prototype.render = function () {
            var o = this;
            o.renderContent();
            o.getView().trigger(new $.Event('contactBox.view.render', {}));
        };

        AContactBoxView.prototype.detach = function () {
            this.getView().detach();
        };

        AContactBoxView.prototype.remove = function () {
            this.getView().remove();
        };

        AContactBoxView.prototype.init = function () {
            var o = this;

            o.getView().trigger(new $.Event('contactBox.view.init', {}));
        };

        return AContactBoxView;
    });
}