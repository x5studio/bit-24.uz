if (typeof define === "function" && define.amd) {
    define([], function () {
        function NavHover(container) {
            var _this = this;
            this.container = $(container);
            this.tabs = $('.nav-tabs', this.container);
            this.tabContent = $('.tab-content', this.container);
            this.isDefActive = this.tabs.find('.active').length;

            this.showContent = function (href, speed) {
                if (typeof speed == 'undefined') {
                    speed = 0;
                }
                _this.tabContent.find('.tab-pane').hide();
                _this.tabContent.find(href).slideDown(speed);
            }

            this.tabs.on('click', 'li>a', function (e) {
                return false;
            });

            this.tabs.on('hover', 'li>a', function (e) {
                var el = $(this);
                isPrevActive = _this.tabs.find('.active').length;
                speed = isPrevActive ? 0 : 70;
                _this.tabs.find('li').removeClass('active');
                el.parent().addClass('active');
                _this.showContent(el.attr('href'), speed);
            });

            if (!this.isDefActive) {
                this.container.on('mouseleave', function () {
                    _this.tabs.find('li').removeClass('active');
                    _this.tabContent.find('.tab-pane').slideUp(70);
                });
            }

            this.showContent(this.tabs.find('.active a').attr('href'));

        }

        return NavHover;
    });
}