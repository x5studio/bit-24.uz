<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<div class="testimonials__list">
    <div class="content">
        <div class="container">
            <?php if ($arResult["ITEMS"]): ?>
                <div class="row">
                    <div class="testimonials__items">
                        <? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
                        <div class="testimonials__item<?php echo !empty($arItem["PROPERTY_YOUTUBE_CODE_VALUE"]) ? ' testimonials__item_video' : ''; ?>">
                            <div class="testimonials__item-info">
                                <div class="testimonials__item-logo">
                                    <img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive" alt="<?= $arItem["NAME"]; ?>">
                                </div>
                                <div class="testimonials__item-company-name"><?php echo $arItem["NAME"]; ?></div>
                                <div class="testimonials__item-author-name"><?php echo $arItem["PROPERTY_AUTHOR_FIO_VALUE"]; ?></div>
                                <div class="testimonials__item-author-position"><?php echo $arItem["PROPERTY_AUTHOR_POSITION_VALUE"]; ?></div>
                            </div>
                            <div class="testimonials__item-content">
                                <?php if (!empty($arItem["PROPERTY_YOUTUBE_CODE_VALUE"])): ?>
                                    <div class="testimonials__item-video">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $arItem["PROPERTY_YOUTUBE_CODE_VALUE"] ?>?autoplay=0&showinfo=0&rel=0"></iframe>
                                    </div>
                                <?php endif; ?>
                                <div class="testimonials__item-text-wrapper">
                                    <div class="testimonials__item-text">
                                        <?= $arItem["PREVIEW_TEXT"] ?>
                                    </div>
                                    <div class="testimonials__item-date"><?= $arItem["DISPLAY_ACTIVE_FROM"]; ?></div>
                                    <?php if ($arItem["PROPERTY_TESTIMONIAL_FILE_SRC"]): ?>
                                        <div class="testimonials__item-file">
                                            <a href="<?php echo $arItem["PROPERTY_TESTIMONIAL_FILE_SRC"]; ?>" download>Скачать бланк отзыва</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <? endforeach; ?>
                    </div>
                    <?php if ($arResult["NAV_STRING"]): ?>
                        <div class="testimonial__pagination">
                            <?php echo $arResult["NAV_STRING"]; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="row">
                    Раздел находится в стадии наполнения
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
