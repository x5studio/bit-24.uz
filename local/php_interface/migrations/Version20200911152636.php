<?php

namespace Sprint\Migration;


class Version20200911152636 extends Version
{
    protected $description = "task_20614, веб-форма Остались вопросы, почтовый шаблон для веб-формы";

    protected $moduleVersion = "3.16.3";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $helper = $this->getHelperManager();

        $eventHelper = $helper->Event();
        $eventHelper->saveEventType('FORM_FILLING_QUESTION', array (
            'LID' => 'ru',
            'EVENT_TYPE' => 'email',
            'NAME' => 'Заполнена web-форма "QUESTION"',
            'DESCRIPTION' => '#RS_FORM_ID# - ID формы
            #RS_FORM_NAME# - Имя формы
            #RS_FORM_SID# - SID формы
            #RS_RESULT_ID# - ID результата
            #RS_DATE_CREATE# - Дата заполнения формы
            #RS_USER_ID# - ID пользователя
            #RS_USER_EMAIL# - EMail пользователя
            #RS_USER_NAME# - Фамилия, имя пользователя
            #RS_USER_AUTH# - Пользователь был авторизован?
            #RS_STAT_GUEST_ID# - ID посетителя
            #RS_STAT_SESSION_ID# - ID сессии
            #NAME# - Имя
            #NAME_RAW# - Имя (оригинальное значение)
            #PHONE# - Телефон
            #PHONE_RAW# - Телефон (оригинальное значение)
            #EMAIL# - E-mail
            #EMAIL_RAW# - E-mail (оригинальное значение)
            #CITY# - Город
            #CITY_RAW# - Город (оригинальное значение)
            #OFFICE# - Офис
            #OFFICE_RAW# - Офис (оригинальное значение)
            #UNIONRESULT# - Ответ Union
            #UNIONRESULT_RAW# - Ответ Union (оригинальное значение)
            #TO# - Адресаты
            #TO_RAW# - Адресаты (оригинальное значение)
            #PAGE# - Страница
            #PAGE_RAW# - Страница (оригинальное значение)
            #GA_CLIENT_ID# - Ид клиента аналитики
            #GA_CLIENT_ID_RAW# - Ид клиента аналитики (оригинальное значение)
            #FORM_TITLE# - Заголовок формы
            #FORM_TITLE_RAW# - Заголовок формы (оригинальное значение)
            #CLIENT_TAGS# - СЕО метки клиента
            #CLIENT_TAGS_RAW# - СЕО метки клиента (оригинальное значение)
            ',
            'SORT' => '100',
        ));

        $eventHelper->saveEventType('FORM_FILLING_QUESTION', array (
            'LID' => 'en',
            'EVENT_TYPE' => 'email',
            'NAME' => 'Web form filled "QUESTION"',
            'DESCRIPTION' => '#RS_FORM_ID# - Form ID
            #RS_FORM_NAME# - Form name
            #RS_FORM_SID# - Form SID
            #RS_RESULT_ID# - Result ID
            #RS_DATE_CREATE# - Form filling date
            #RS_USER_ID# - User ID
            #RS_USER_EMAIL# - User e-mail
            #RS_USER_NAME# - First and last user names
            #RS_USER_AUTH# - User authorized?
            #RS_STAT_GUEST_ID# - Visitor ID
            #RS_STAT_SESSION_ID# - Session ID
            #NAME# - Имя
            #NAME_RAW# - Имя (original value)
            #PHONE# - Телефон
            #PHONE_RAW# - Телефон (original value)
            #EMAIL# - E-mail
            #EMAIL_RAW# - E-mail (original value)
            #CITY# - Город
            #CITY_RAW# - Город (original value)
            #OFFICE# - Офис
            #OFFICE_RAW# - Офис (original value)
            #UNIONRESULT# - Ответ Union
            #UNIONRESULT_RAW# - Ответ Union (original value)
            #TO# - Адресаты
            #TO_RAW# - Адресаты (original value)
            #PAGE# - Страница
            #PAGE_RAW# - Страница (original value)
            #GA_CLIENT_ID# - Ид клиента аналитики
            #GA_CLIENT_ID_RAW# - Ид клиента аналитики (original value)
            #FORM_TITLE# - Заголовок формы
            #FORM_TITLE_RAW# - Заголовок формы (original value)
            #CLIENT_TAGS# - СЕО метки клиента
            #CLIENT_TAGS_RAW# - СЕО метки клиента (original value)
            ',
            'SORT' => '100',
        ));

        $eventHelper->saveEventMessage('FORM_FILLING_QUESTION', array (
            'LID' =>
                array (
                    0 => 's1',
                ),
            'ACTIVE' => 'Y',
            'EMAIL_FROM' => '#DEFAULT_EMAIL_FROM#',
            'EMAIL_TO' => '#TO#',
            'SUBJECT' => '#SERVER_NAME#: заполнена web-форма [#RS_FORM_ID#] #RS_FORM_NAME#',
            'MESSAGE' => '#SERVER_NAME#
            Заполнена web-форма: [#RS_FORM_ID#] #RS_FORM_NAME#
            -------------------------------------------------------
            
            Дата - #RS_DATE_CREATE#
            Результат - #RS_RESULT_ID#
            Пользователь - [#RS_USER_ID#] #RS_USER_NAME# #RS_USER_AUTH#
            Посетитель - #RS_STAT_GUEST_ID#
            Сессия - #RS_STAT_SESSION_ID#
            
            Имя
            *******************************
            #NAME#
            
            Город
            *******************************
            #CITY#
            
            Офис
            *******************************
            #OFFICE#
            
            Ответ Юнион
            *******************************
            #UNIONRESULT#
            
            Адресаты
            *******************************
            #TO#
            
            Страница
            *******************************
            #PAGE#
            
            Телефон
            *******************************
            #PHONE#
            
            E-mail
            *******************************
            #EMAIL#
            
            Для просмотра воспользуйтесь ссылкой:
            http://#SERVER_NAME#/bitrix/admin/form_result_view.php?lang=ru&WEB_FORM_ID=#RS_FORM_ID#&RESULT_ID=#RS_RESULT_ID#
            -------------------------------------------------------
            Письмо сгенерировано автоматически.
            ',
            'BODY_TYPE' => 'text',
            'BCC' => '',
            'REPLY_TO' => '',
            'CC' => '',
            'IN_REPLY_TO' => '',
            'PRIORITY' => '',
            'FIELD1_NAME' => '',
            'FIELD1_VALUE' => '',
            'FIELD2_NAME' => '',
            'FIELD2_VALUE' => '',
            'SITE_TEMPLATE_ID' => '',
            'ADDITIONAL_FIELD' =>
                array (
                ),
            'LANGUAGE_ID' => '',
            'EVENT_TYPE' => '[ FORM_FILLING_QUESTION ] Заполнена web-форма "QUESTION"',
        ));
        $eventId = $eventHelper->getEventMessage('FORM_FILLING_QUESTION')['ID'];
        $eventId = intval($eventId);

        // Если создали почтовый шаблон, то создаём веб-форму
        if($eventId > 0) {
            $formHelper = $helper->Form();
            $formId = $formHelper->saveForm(array(
                'NAME' => 'Остались вопросы',
                'SID' => 'QUESTION',
                'BUTTON' => 'Оставить заявку',
                'C_SORT' => '300',
                'FIRST_SITE_ID' => NULL,
                'IMAGE_ID' => NULL,
                'USE_CAPTCHA' => 'N',
                'DESCRIPTION' => '',
                'DESCRIPTION_TYPE' => 'text',
                'FORM_TEMPLATE' => '',
                'USE_DEFAULT_TEMPLATE' => 'Y',
                'SHOW_TEMPLATE' => NULL,
                'MAIL_EVENT_TYPE' => 'FORM_FILLING_QUESTION',
                'SHOW_RESULT_TEMPLATE' => NULL,
                'PRINT_RESULT_TEMPLATE' => NULL,
                'EDIT_RESULT_TEMPLATE' => NULL,
                'FILTER_RESULT_TEMPLATE' => '',
                'TABLE_RESULT_TEMPLATE' => '',
                'USE_RESTRICTIONS' => 'N',
                'RESTRICT_USER' => '0',
                'RESTRICT_TIME' => '0',
                'RESTRICT_STATUS' => '',
                'STAT_EVENT1' => '',
                'STAT_EVENT2' => '',
                'STAT_EVENT3' => '',
                'LID' => NULL,
                'C_FIELDS' => '0',
                'QUESTIONS' => '11',
                'STATUSES' => '1',
                'arSITE' =>
                    array (
                        0 => 's1',
                    ),
                'arMENU' =>
                    array (
                        'ru' => 'Остались вопросы',
                        'en' => '',
                    ),
                'arGROUP' =>
                    array (
                    ),
                'arMAIL_TEMPLATE' =>
                    array (
                        0 => $eventId,
                    ),
            ));

            $formHelper->saveStatuses($formId, array (
                0 => array (
                    'CSS' => 'statusgreen',
                    'C_SORT' => '100',
                    'ACTIVE' => 'Y',
                    'TITLE' => 'Создана',
                    'DESCRIPTION' => '',
                    'DEFAULT_VALUE' => 'Y',
                    'HANDLER_OUT' => '',
                    'HANDLER_IN' => '',
                ),
            ));

            $formHelper->saveFields($formId, array (
                0 => array (
                    'ACTIVE' => 'Y',
                    'TITLE' => 'Имя *',
                    'TITLE_TYPE' => 'text',
                    'SID' => 'NAME',
                    'C_SORT' => '100',
                    'ADDITIONAL' => 'N',
                    'REQUIRED' => 'Y',
                    'IN_FILTER' => 'N',
                    'IN_RESULTS_TABLE' => 'Y',
                    'IN_EXCEL_TABLE' => 'Y',
                    'FIELD_TYPE' => '',
                    'IMAGE_ID' => NULL,
                    'COMMENTS' => '',
                    'FILTER_TITLE' => 'Имя',
                    'RESULTS_TABLE_TITLE' => 'Имя',
                    'ANSWERS' => array (
                        0 => array (
                            'MESSAGE' => ' ',
                            'VALUE' => '',
                            'FIELD_TYPE' => 'text',
                            'FIELD_WIDTH' => '0',
                            'FIELD_HEIGHT' => '0',
                            'FIELD_PARAM' => 'REQUIRED',
                            'C_SORT' => '0',
                            'ACTIVE' => 'Y',
                        ),
                    ),
                    'VALIDATORS' => array (),
                ),
                1 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'Телефон *',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'PHONE',
                        'C_SORT' => '200',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'Телефон',
                        'RESULTS_TABLE_TITLE' => 'Телефон',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'text',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'REQUIRED_FROM_GROUP PHONE_NO_REQUIRED',
                                        'C_SORT' => '0',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
                2 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'E-mail',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'EMAIL',
                        'C_SORT' => '300',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'E-mail',
                        'RESULTS_TABLE_TITLE' => 'E-mail',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'text',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'EMAIL_NO_REQUIRED',
                                        'C_SORT' => '100',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
                3 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'Город',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'CITY',
                        'C_SORT' => '400',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'Город',
                        'RESULTS_TABLE_TITLE' => 'Город',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'hidden',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'CITY',
                                        'C_SORT' => '0',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
                4 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'Офис',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'OFFICE',
                        'C_SORT' => '500',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'Офис',
                        'RESULTS_TABLE_TITLE' => 'Офис',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'hidden',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'OFFICE',
                                        'C_SORT' => '0',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
                5 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'Ответ Union',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'UNIONRESULT',
                        'C_SORT' => '600',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'Ответ Union',
                        'RESULTS_TABLE_TITLE' => 'Ответ Union',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'hidden',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'UNIONRESULT',
                                        'C_SORT' => '0',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
                6 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'Адресаты',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'TO',
                        'C_SORT' => '700',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'Адресаты',
                        'RESULTS_TABLE_TITLE' => 'Адресаты',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'hidden',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'TO',
                                        'C_SORT' => '0',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
                7 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'Страница',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'PAGE',
                        'C_SORT' => '800',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'Страница',
                        'RESULTS_TABLE_TITLE' => 'Страница',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'hidden',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'PAGE',
                                        'C_SORT' => '0',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
                8 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'Ид клиента аналитики',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'GA_CLIENT_ID',
                        'C_SORT' => '1000',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'Ид клиента аналитики',
                        'RESULTS_TABLE_TITLE' => 'Ид клиента аналитики',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'hidden',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'GA_CLIENT_ID',
                                        'C_SORT' => '0',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
                9 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'Заголовок формы',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'FORM_TITLE',
                        'C_SORT' => '1100',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'Заголовок формы',
                        'RESULTS_TABLE_TITLE' => 'Заголовок формы',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'hidden',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'FORM_TITLE',
                                        'C_SORT' => '0',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
                10 =>
                    array (
                        'ACTIVE' => 'Y',
                        'TITLE' => 'СЕО метки клиента',
                        'TITLE_TYPE' => 'text',
                        'SID' => 'CLIENT_TAGS',
                        'C_SORT' => '1200',
                        'ADDITIONAL' => 'N',
                        'REQUIRED' => 'N',
                        'IN_FILTER' => 'N',
                        'IN_RESULTS_TABLE' => 'Y',
                        'IN_EXCEL_TABLE' => 'Y',
                        'FIELD_TYPE' => '',
                        'IMAGE_ID' => NULL,
                        'COMMENTS' => '',
                        'FILTER_TITLE' => 'СЕО метки клиента',
                        'RESULTS_TABLE_TITLE' => 'СЕО метки клиента',
                        'ANSWERS' =>
                            array (
                                0 =>
                                    array (
                                        'MESSAGE' => ' ',
                                        'VALUE' => '',
                                        'FIELD_TYPE' => 'hidden',
                                        'FIELD_WIDTH' => '0',
                                        'FIELD_HEIGHT' => '0',
                                        'FIELD_PARAM' => 'CLIENT_TAGS',
                                        'C_SORT' => '100',
                                        'ACTIVE' => 'Y',
                                    ),
                            ),
                        'VALIDATORS' =>
                            array (
                            ),
                    ),
            ));
        }
    }

    public function down()
    {
        $helper = $this->getHelperManager();
        $eventHelper = $helper->Event();
        $formHelper = $helper->Form();

        $eventMessages = $eventHelper->getEventMessages('FORM_FILLING_QUESTION');
        foreach($eventMessages as $eventMessage)
            $eventHelper->deleteEventMessage(['SUBJECT' => $eventMessage['SUBJECT'], 'EVENT_NAME' => $eventMessage['EVENT_NAME']]);

        $eventTypes = $eventHelper->getEventTypes('FORM_FILLING_QUESTION');
        foreach($eventTypes as $eventType)
            $eventHelper->deleteEventType(['LID' => $eventType['LID'], 'EVENT_NAME' => $eventType['EVENT_NAME']]);

        $formHelper->deleteFormIfExists('QUESTION');
    }
}

