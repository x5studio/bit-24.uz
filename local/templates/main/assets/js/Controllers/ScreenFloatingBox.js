if (typeof define === "function" && define.amd) {
    define(['Controls/FloatingBox'], function (FloatingBox) {

        function ScreenFloatingBox(container) {
            FloatingBox.apply(this, container);
        }

        extend(ScreenFloatingBox, FloatingBox);

        ScreenFloatingBox.prototype.isBoxInactive = function () {
        	//bitfinance.ru events
        	if(window.pageYOffset < document.documentElement.clientHeight)
        		$(document).trigger("ScreenFloatingHide");
        	else {
        		$(document).trigger("ScreenFloatingShow");
        	}
        	//bitfinance.ru events
            return window.pageYOffset < document.documentElement.clientHeight;
        };

        return ScreenFloatingBox;
    });
}
