if (typeof define === "function" && define.amd) {
    define(['jscrollpane'], function () {
        function BoxOffices(container) {
            var _this = this;
            this.container = $(container);
            this.header = this.container.find('.box-title');
            this.footer = this.container.find('.box-offices-control');
            this.control = this.footer.find('span');
            this.controlText = this.control.text();
            this.controlTextClose = 'Свернуть';
            this.items = this.container.find('.box-offices-scroll');
            this.offices = this.items.find('.box-offices-item');
            this.officesCount = this.offices.length;
            if (this.officesCount > 8) {
                this.heightOpened = this.offices.eq(0).height() * 8;
            } else {
                this.heightOpened = this.items.height();
            }
            _this.items.bind('jsp-scroll-y', function (event, scrollPositionY, isAtTop, isAtBottom) {
                if (isAtBottom) {
                    _this.footer.css({'box-shadow': 'none'});
                } else {
                    _this.footer.removeAttr('style');
                }
            }).jScrollPane({verticalGutter: 2, contentWidth: 239});
            //_this.jsp = _this.items.jScrollPane().data().jsp;
            this.heightClosed = this.container.height() /*- this.footer.outerHeight()*/ - this.header.outerHeight() + 1;
            this.flagOpened = true;
            this.control.click(function () {
                if (_this.flagOpened) {
                    _this.items.height(_this.heightClosed);
                    _this.control.text(_this.controlText);
                    _this.container.removeClass('box-offices-open');
                } else {
                    _this.items.height(_this.heightOpened);
                    _this.control.text(_this.controlTextClose);
                    _this.container.addClass('box-offices-open');
                }
                if (_this.jsp) {
                    _this.jsp.reinitialise();
                }
                _this.flagOpened = !_this.flagOpened;
                return false;
            });
            this.items.height(this.heightClosed);
            this.flagOpened = !this.flagOpened;
        }

        return BoxOffices;
    });
}