<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):
    ?>
<ul class="nav navbar-nav topmenu topmenu-comp" >

<?
$previousLevel = 0;
foreach($arResult as $arItem):
    $icon = !empty($arItem['PARAMS']['ICON']) ? $arItem['PARAMS']['ICON'] : false;
    $iconClass = !empty($arItem['PARAMS']['ICON_CLASS']) ? $arItem['PARAMS']['ICON_CLASS'] : false;
    ?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li></ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

        <li class="item1 dropdown mega-dropdown">
            <a href="<?=$arItem["LINK"]?>" class="dropdown-toggle" data-toggle="dropdown"><?=$arItem["TEXT"]?></a>
            <ul class="dropdown-menu mega-dropdown-menu">
                <li>
                    <ul>

	<?else:?>

            <li>
                <a href="<?=$arItem["LINK"]?>">
                    <? if ($icon || $iconClass) { ?>
                        <span class="menu-ico <? if ($iconClass) echo $iconClass; ?>" <? if ($icon) { ?>style="background: url(<?= $icon ?>) no-repeat left center"<? } ?>>
                    <? } ?>
                    <?=$arItem["TEXT"]?>
                    <? if ($icon || $iconClass) { ?>
                        </span>
                    <? } ?>
                </a>
            </li>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li></ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
<?endif?>