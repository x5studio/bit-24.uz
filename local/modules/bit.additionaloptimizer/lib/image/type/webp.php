<?

namespace Bit\AdditionalOptimizer\Image\Type;

use Bit\AdditionalOptimizer\Image\Optimizer\OptimizerChain;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Cwebp;
use Bit\AdditionalOptimizer\Image\BinHelper;

class WebP extends Base {

    protected $name = '{name}.webp';

    public function createImage() {

        if(!$this->pathToImage) throw new \Exception("Original image not set");

        $optimizerChain = new OptimizerChain;

        $optimizerChain->addOptimizer((new Cwebp([
            '-m 6',
            '-pass 10',
            '-mt',
            '-q 80',
            $this->pathToImage.' -o '.$this->getImagePath()
        ]))->setBinaryPath(BinHelper::getExecBinPath('cwebp')));

        $optimizerChain->optimize($this->pathToImage);

        $this->cache = [];


        return $this->getImageExist();

    }

}