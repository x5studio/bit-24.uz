<?
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
define('MAIN_TEMPLATE_PATH', '/local/templates/main');

require_once __DIR__ . '/configs.php';
require $_SERVER["DOCUMENT_ROOT"].'/local/vendor/autoload.php';
require_once __DIR__ . '/firstbit/include.php';

\Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
		'\Proman\Helper' => '/local/php_interface/include/Helper.php'
	)
);

\Bitrix\Main\Loader::includeModule('pb.main');

if(\Bitrix\Main\Loader::includeModule('pb.contacts')){
    global $obOfficeFilter, $obCityFilter;

    $obCityFilter = new ContactManagerFilter();
    $obCityFilter->setGeneral(Array(
        'UF_HIDE_IN_CITY_LIST' => false,
    ));

    $obOfficeFilter = new ContactManagerFilter();
    $obOfficeFilter->setGeneral(Array('PROPERTY_HIDE_IN_CITY_LIST' => false));
}


define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
include_once("events.php");

//price auto update
include_once(\Bitrix\Main\Application::getDocumentRoot().'/local/php_interface/include/PriceUpdater.php');



// Удаляем лишние пробелы перед выводом и делаем lazy load картинок
//AddEventHandler("main", "OnEndBufferContent", "optimizeOutputContent");

function optimizeOutputContent(&$content){

    $content = preg_replace('#>\s*\n\s*<#', '><', $content);
    $content = preg_replace("#(<img\s[^>]*?)src(\s*=\s*['\"][^'\"]*?['\"][^>]*?>)#", '$1src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=" data-src$2', $content);


}

// редирект на url в нижем регистре
if ( $_SERVER['REQUEST_URI'] && $clearQuery = array_shift(explode('?',$_SERVER["REQUEST_URI"]))) {

    if($clearQuery != strtolower( $clearQuery)){

        header('Location: //'.$_SERVER['HTTP_HOST'] . strtolower($clearQuery) . '?' . $_SERVER["QUERY_STRING"] , true, 301);
        exit();

    }

}
