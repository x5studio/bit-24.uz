<?

namespace Bit\AdditionalOptimizer\Image\Type;

class Original extends Base {

    protected $name = '{name}.original';

    public function createImage() {

        if(!$this->pathToImage) throw new \Exception("Original image not set");

        $result = copy($this->pathToImage, $this->getImagePath());

        $this->cache = [];

        return $result;

    }

    public function resotreImage() {

        if(!$this->getImagePath()) throw new \Exception("Original image not found");

        $result = copy($this->getImagePath(), $this->pathToImage);

        $this->cache = [];

        return $result;
        
    }
}