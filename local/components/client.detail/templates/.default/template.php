<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="first-block">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12"><?= $arResult["~UF_DESC"] ?></div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<img src="<?= $arResult["IMAGES"][$arResult["DETAIL_PICTURE"]] ?>" class="img-responsive center-block">
			</div>
		</div>
	</div>
</section>
<? if (strlen($arResult["UF_TOP_DESC"])): ?>
	<section class="blue-block">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="ribbon">
						<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/realestate/ribbon.png" class="img-responsive">
					</div>
					<p><?= $arResult["UF_TOP_DESC"] ?></p>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>
<?
$cnt = 0;
foreach ($arResult["ITEMS"] as $arItem)
{
	switch ($arItem["PROPERTY_TYPE_ENUM_ID"])
	{
		case 6:
		case 7:
			$class = '';
			if ($arItem["PROPERTY_TYPE_ENUM_ID"] == 7)
			{
				$class = 'branches-blue';
			} elseif ($cnt % 2)
			{
				$class = ($cnt == 1) ? 'branches-white-2' : 'branches-white';
			} else
			{
				$class = ($cnt == 0) ? 'first-block' : 'branches-gray';
			}
			?>
			<? if (!($cnt % 2)): ?>
			<section class="<?= $class ?>">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h2><?= $arItem["~NAME"] ?></h2>
							<?= $arItem["PREVIEW_TEXT"] ?>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<? if (!empty($arItem["PROPERTY_YOUTUBE_VALUE"])): ?>
								<div class="videobg center-block">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=
									$arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
								</div>
							<? endif; ?>
							<? if ($arItem["PREVIEW_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive center-block">
							<? endif; ?>
							<? if ($arItem["DETAIL_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>" class="img-responsive center-block">
							<? endif; ?>
						</div>
						<? if (!empty($arItem["DETAIL_TEXT"])): ?>
							<?= $arItem["DETAIL_TEXT"] ?>
						<? endif; ?>
					</div>
				</div>
			</section>
		<? else: ?>
			<section class="<?= $class ?>">
				<div class="container">
					<div class="row">
						<div class="col-md-6 hidden-sm hidden-xs">
							<? if (!empty($arItem["PROPERTY_YOUTUBE_VALUE"])): ?>
								<div class="videobg center-block">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=
									$arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
								</div>
							<? endif; ?>
							<? if ($arItem["PREVIEW_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive center-block">
							<? endif; ?>
							<? if ($arItem["DETAIL_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>" class="img-responsive center-block">
							<? endif; ?>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h2><?= $arItem["~NAME"] ?></h2>
							<?= $arItem["PREVIEW_TEXT"] ?>
						</div>
						<div class="hidden-md col-sm-12 hidden-lg col-xs-12 center-block">
							<? if (!empty($arItem["PROPERTY_YOUTUBE_VALUE"])): ?>
								<div class="videobg center-block">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=
									$arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
								</div>
							<? endif; ?>
							<? if ($arItem["PREVIEW_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive center-block">
							<? endif; ?>
							<? if ($arItem["DETAIL_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>" class="img-responsive center-block">
							<? endif; ?>
						</div>
						<? if (!empty($arItem["DETAIL_TEXT"])): ?>
							<?= $arItem["DETAIL_TEXT"] ?>
						<? endif; ?>
					</div>
				</div>
			</section>
		<? endif;
			$cnt++; ?>
			<?
			break;
		case 8:
			?>
			<section class="blue-block-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<h2><?= $arItem["~NAME"] ?></h2>
						</div>
					</div>
				</div>
			</section>
			<?
			break;
		case 9:
			?>
			<section class="blue-block">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="ribbon">
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
							</div>
							<p><?= $arItem["PREVIEW_TEXT"] ?></p>
						</div>
					</div>
				</div>
			</section>
			<?
			break;
	}
}?>
<? if (strlen($arResult["UF_BOTTOM_DESC"])): ?>
	<section class="bottom-block"<?
	if ($arResult["UF_BOTTOM_BG"]): ?> style="background-image: url(<?= $arResult["IMAGES"][$arResult["UF_BOTTOM_BG"]] ?>);"<? endif; ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12"><?= $arResult["~UF_BOTTOM_DESC"] ?></div>
			</div>
		</div>
	</section>
<? endif; ?>