<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$APPLICATION->SetAdditionalCss("/blog_new/styles.css");
$APPLICATION->SetAdditionalCss("/blog_new/js/slick/slick.css");
$APPLICATION->AddHeadScript("/blog_new/js/slick/slick.min.js");
?>
<div class="blog-articles-wrap">
	<?
	global $TAG_ID;
	$APPLICATION->IncludeComponent("blog.list", "cards", Array(
	"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"CACHE_TYPE" => "A",	// Тип кеширования
		"COUNT" => "12",	// Количество элементов на странице
		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],	// Инфоблок
		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
		"PAGER_TEMPLATE" => "blog",	// Шаблон постраничной навигации
		"PAGER_TITLE" => "Новости",	// Название категорий
		"SECTION_CODE" => "",	// CODE раздела
		"TAG_ID" => $TAG_ID,	// ID тэга
        "H1_AS_NAME" => 'Y' // Устанавливать h1 как имя раздела, если есть seo
	),
	false
);?>
</div>

<script>
    $(document).ready( function(){
        var $status = $('.current-slide');
        var $slickElement = $('.blog-carousel');

        $slickElement.on('init reInit beforeChange', function(event, slick, currentSlide, nextSlide){
            var i = (currentSlide ? currentSlide : 0) + 1;
            $status.html('<span class="current-slide">' + i + '<span class="grey-color">/' + slick.slideCount + '</span></span>');
        });

        $('.blog-carousel').slick({
            slide: '.article-content',
            prevArrow: '.blog-carousel-control .left-arrow',
            nextArrow: '.blog-carousel-control .right-arrow'
        });
    } );
</script>