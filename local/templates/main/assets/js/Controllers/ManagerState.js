if (typeof define === "function" && define.amd) {
    define([], function () {
        function ManagerState(p) {
            var o = this;

            this.setState = function (state) {
                o.state = state;
                var State = state.replace(/^./, function (char) {
                    return char.toUpperCase();
                });
                if (p.state && p.state[state]) {
                    p.state[state].call(o);
                }
                else if (o['state' + State]) {
                    o['state' + State]();
                }
            };

            this.init = function () {
                o.setState('init');
            };

            this.init();

        }

        return ManagerState;
    });
}