<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<a class="simple-banner" href="#">
  <div class="simple-banner__image">
    <img class="large" src="<?= SITE_TEMPLATE_PATH ?>/assets/images/shopfront/banner-3.jpg"
         srcset="<?= SITE_TEMPLATE_PATH ?>/assets/images/shopfront/banner-3_2x.png"/>
    <img class="small" src="http://lorempixel.com/276/207?1639" srcset="http://lorempixel.com/552/414?1639"/>
  </div>
</a>