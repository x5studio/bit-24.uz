<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
	<div class="widget">
		<button type="button" class="btn btn-close hidden-lg hidden-md" data-toggle="offcanvas" onclick="demoVisibility()">
			<i class="fa fa-times" aria-hidden="true"></i>
		</button>
		<h2>Разделы блога</h2>
		<ul>
			<? foreach ($arResult as $arItem): ?>
				<li<? if ($arItem["SELECTED"]): ?> class="is-active"<? endif ?>>
					<a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
				</li>
			<? endforeach ?>
		</ul>
	</div>
<? endif ?>