<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
	<li class="col-md-3 col-sm-3 col-xs-12">
		<ul>
			<li class="dropdown-header hidden-xs">Наши услуги</li>
			<? foreach ($arResult as $cell=>$arItem):
				if($cell%2 == 1) continue;
				?>
				<li><a href="<?= $arItem["LINK"] ?>"><img src="<?= $arItem["PARAMS"]["ICON"] ?>" alt="<?= $arItem["TEXT"] ?>"><?=
						$arItem["TEXT"] ?></a></li>
			<? endforeach ?>
		</ul>
	</li>
	<li class="col-md-5 col-sm-5 col-xs-12">
		<ul class="mdm-second">
		<? foreach ($arResult as $cell=>$arItem):
			if($cell%2 == 0) continue;
			?>
			<li><a href="<?= $arItem["LINK"] ?>"><img src="<?= $arItem["PARAMS"]["ICON"] ?>" alt="<?= $arItem["TEXT"] ?>"><?=
					$arItem["TEXT"] ?></a></li>
		<? endforeach ?>
			</li>
		</ul>
	</li>
<? endif ?>