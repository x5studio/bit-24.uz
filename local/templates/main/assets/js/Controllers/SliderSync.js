if (typeof define === "function" && define.amd) {
    define(['flexslider'], function () {
        function SliderSync(container) {
            var _this = this;
            this.container = $(container);



            this.carouselId = '#' + this.container.find('.flexslider.carousel').attr('id');
            this.sliderId = '#' + this.container.find('.flexslider.slider').attr('id');

            this.carouselItemWidth = parseInt(this.container.find('.flexslider.carousel').attr('data-width'));
            this.carouselItemMargin = parseInt(this.container.find('.flexslider.carousel').attr('data-margin'));
            this.sliderItemWidth = parseInt(this.container.find('.flexslider.slider').attr('data-width'));

            if (isNaN(this.carouselItemMargin))
            {
                this.carouselItemMargin = 5;
            }

            $(this.carouselId).flexslider({
                pauseOnHover: true,
                useCSS: false,
                animation: "slide",
                controlNav: false,
                directionNav: true,
                animationLoop: false,
                slideshow: false,
                itemWidth: _this.carouselItemWidth,
                itemMargin: _this.carouselItemMargin,
                asNavFor: _this.sliderId
            });

            $(this.sliderId).flexslider({
                pauseOnHover: true,
                useCSS: false,
                animation: "slide",
                controlNav: false,
                directionNav: false,
                animationLoop: false,
                slideshow: false,
                itemWidth: _this.sliderItemWidth,
                sync: _this.carouselId,
                start: function (slider) {
                    if (slider.slides) {
                        slider.slides.css({height: 'auto', overflow: 'visible'});
                        var h = slider.slides.eq(slider.animatingTo).height();
                        slider.slides.css({height: h, overflow: 'hidden'});
                        slider.slides.siblings('.clone').css({height: h, overflow: 'hidden'});
                        slider.slides.eq(slider.currentSlide).css({height: 'auto', overflow: 'visible'});
                    }
                },
                before: function (slider) {
                    if (slider.slides) {
                        slider.slides.eq(slider.currentSlide).find('.j-spoiler-close:visible').click();
                        slideH = slider.slides.height();
                        slider.slides.css({height: 'auto', overflow: 'visible'});
                        var h = slider.slides.eq(slider.animatingTo).height();
                        slider.slides.css({height: slideH, overflow: 'hidden'});
                        slider.slides.animate({height: h, overflow: 'hidden'}, 400);
                        slider.slides.siblings('.clone').css({height: h, overflow: 'hidden'});
                        //slider.slides.eq(slider.animatingTo).css({height: 'auto', overflow: 'visible'});
                    }
                },
                after: function (slider) {
                    slider.slides.eq(slider.animatingTo).css({height: 'auto'});
                }
            });

        }

        return SliderSync;
    });
}