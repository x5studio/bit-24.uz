<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;


if (!isset($arParams["CACHE_TIME"])) {
    $arParams["CACHE_TIME"] = 3600;
}

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0) {
    $this->AbortResultCache();
    ShowError("Не выбран инфоблок");
    return;
}

if ($this->StartResultCache()) {
    if (!Loader::IncludeModule("iblock")) {
        $this->AbortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    $arFile = array();

    $arRelatedSite = FirstBit\Util::getArrEnumValueByXmlID(SITE_ID, 'RELATED_SITE', $arParams["IBLOCK_ID"]);
    $rsElement = \CIBlockElement::GetList(
        array("SORT" => "ASC"),
        array(
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "ACTIVE" => "Y",
            "PROPERTY_RELATED_SITE" => [$arRelatedSite['ID'], false]
        ),
        false, false,
        array("ID", 'IBLOCK_ID', "NAME", "DETAIL_PAGE_URL", "PROPERTY_CLASS_ICON", "PROPERTY_MENU_ICON"));
    while ($arElement = $rsElement->GetNext()) {
        $arResult[] = Array(
            $arElement["NAME"],
            $arElement["DETAIL_PAGE_URL"],
            Array(),
            Array("CLASS" => $arElement["~PROPERTY_CLASS_ICON_VALUE"], "ELEMENT" => $arElement),
            ""
        );
    }

    $this->endResultCache();
}

return $arResult;
