<?php

namespace Bit\AdditionalOptimizer\Image\Optimizer\Optimizers;

use Bit\AdditionalOptimizer\Image\Optimizer\Image;

class Cwebp extends BaseOptimizer
{
    public $binaryName = 'cwebp';

    public function canHandle(Image $image): bool
    {
        return $image->mime() === 'image/webp' || $image->mime() === 'image/png' || $image->mime() === 'image/jpeg';
    }

    public function getCommand(): string
    {

        $OutputString = $this->imagePath . ' -o ' . $this->imagePath;

        foreach ($this->options as $key => $option) {
            
            if(strpos($option, '-o') !== false) {

                $OutputString = $option;
                unset($this->options[$key]);
                
            }

        }


        $optionString = implode(' ', array_merge($this->options, [$OutputString]));


        return "\"{$this->binaryPath}{$this->binaryName}\" {$optionString}"
            .' '.escapeshellarg($this->imagePath);

    }
}
