<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

use \Proman\Helper;

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
$arParams["COUNT"] = 3;

if ($this->StartResultCache())
{
	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arEnumSection = \FirstBit\Util::getArrEnumValueUserFieldByXmlID(SITE_ID, ENUM_SECTION_ID_INDUSTRY_EXPERTISE_RELATED_SITE);
	$arFile = array();
	$rsSection = \CIBlockSection::GetList(
		array("SORT" => "ASC"),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
            "UF_RELATED_SITE" => [false, $arEnumSection['ID']]
		),
		false,
		array("ID", "NAME", "SECTION_PAGE_URL", "UF_ICON_MENU", "UF_CLASS_ICON", "UF_RELATED_SITE"));
	while ($arSection = $rsSection->GetNext())
	{
		$arFile[] = $arSection["UF_ICON_MENU"];
		$arResult["ITEMS"][] = $arSection;
	}
	$arResult["IMAGES"] = Helper::getImages($arFile);

	$this->IncludeComponentTemplate();
}
