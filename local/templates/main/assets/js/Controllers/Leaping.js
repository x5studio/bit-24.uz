if (typeof define === "function" && define.amd) {
    define(["Controls/Responsive"], function (obResponsive) {
        function Leaping(container) {
            this.container = $(container);
            this.origParent = this.container.parent();
            this.init();
        }

        Leaping.prototype.getTarget = function () {
            return this.container.data('target');
        };

        Leaping.prototype.init = function () {
            var o = this;
            obResponsive.on(['xsmall', 'small', 'medium', 'large'], 'in', function () {
                var target = o.getTarget();
                var respTarget = obResponsive.getCurType() + '-' + target;
                var $respTarget = $("." + respTarget);
                if ($respTarget.length) {
                    $respTarget.append(o.container);
                } else if (!$.contains(o.origParent[0], o.container[0])) {
                    o.origParent.append(o.container);
                }
            });
        };

        return Leaping;
    });
}