<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>	<section class="crm-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<nav class="navbar">
						<div class="navbar-header">
							<div class="hidden-lg hidden-md hidden-sm col-xs-12 filter">
								<button class="navbar-toggle" type="button" data-toggle="collapse"
												data-target=".js-navbar-collapse-crm"><i
											class="fa fa-filter" aria-hidden="true"></i></button>
							</div>
						</div>
						<div class="collapse navbar-collapse js-navbar-collapse-crm">
							<ul class="nav navbar-nav list-inline crmmenu">
								<? foreach ($arResult["SECTION"] as $arSection): ?>
									<li<? if ($arSection["ACTIVE"]): ?> class="active"<?endif;
									?>><a href="<?= $arSection["SECTION_PAGE_URL"] ?>" title="<?= $arSection["NAME"] ?>"><?=
											$arSection["NAME"] ?></a></li>
								<? endforeach; ?>
							</ul>
						</div><!-- /.nav-collapse -->
					</nav>
				</div>
			</div>
		</div>
	</section>
<?php if (!empty($arResult['UF_BLOCK_TITLE']) || !empty($arResult['UF_BLOCK_SUBTITLE'])): ?>
    <div class="capabilities__block-title-container">
        <div class="container">
            <?php if (!empty($arResult['UF_BLOCK_TITLE'])): ?>
                <div class="capabilities__block-title"><?php echo $arResult['~UF_BLOCK_TITLE']; ?></div>
            <?php endif; ?>
            <?php if (!empty($arResult['UF_BLOCK_SUBTITLE'])): ?>
                <div class="capabilities__block-subtitle"><?php echo $arResult['~UF_BLOCK_SUBTITLE']; ?></div>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<?
$cnt = 0;
foreach ($arResult["ITEMS"] as $itemNum => $arItem)
{
	switch ($arItem["PROPERTY_TYPE_ENUM_ID"])
	{
		case 1:
		case 2:
			$class = '';
			if ($arItem["PROPERTY_TYPE_ENUM_ID"] == 2)
			{
				$class = 'branches-blue';
			} elseif ($cnt % 2)
			{
				$class = ($cnt == 1) ? 'branches-white-2' : 'branches-white';
			} else
			{
				$class = ($cnt == 0) ? 'first-block' : 'branches-gray';
			}
			?>
			<? if (!($cnt % 2)): ?>
			<section class="<?= $class ?>">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h3><?= $arItem["~NAME"] ?></h3>
							<?= $arItem["PREVIEW_TEXT"] ?>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<? if (!empty($arItem["PROPERTY_YOUTUBE_VALUE"])): ?>
								<div class="videobg center-block">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=
									$arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
								</div>
							<? endif; ?>
							<? if ($arItem["PREVIEW_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive center-block" alt="">
							<? endif; ?>
							<? if ($arItem["DETAIL_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>" class="img-responsive center-block" alt="">
							<? endif; ?>
						</div>
						<? if (!empty($arItem["DETAIL_TEXT"])): ?>
							<?= $arItem["DETAIL_TEXT"] ?>
						<? endif; ?>
					</div>
				</div>
			</section>
		<? else: ?>
			<section class="<?= $class ?>">
				<div class="container">
					<div class="row">
						<div class="col-md-6 hidden-sm hidden-xs">
							<? if (!empty($arItem["PROPERTY_YOUTUBE_VALUE"])): ?>
								<div class="videobg center-block">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=
									$arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
								</div>
							<? endif; ?>
							<? if ($arItem["PREVIEW_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive center-block" alt="">
							<? endif; ?>
							<? if ($arItem["DETAIL_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>" class="img-responsive center-block" alt="">
							<? endif; ?>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h3><?= $arItem["~NAME"] ?></h3>
							<?= $arItem["PREVIEW_TEXT"] ?>
						</div>
						<div class="hidden-md col-sm-12 hidden-lg col-xs-12 center-block">
							<? if (!empty($arItem["PROPERTY_YOUTUBE_VALUE"])): ?>
								<div class="videobg center-block">
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=
									$arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
								</div>
							<? endif; ?>
							<? if ($arItem["PREVIEW_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive center-block" alt="">
							<? endif; ?>
							<? if ($arItem["DETAIL_PICTURE"]): ?>
								<img src="<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>" class="img-responsive center-block" alt="">
							<? endif; ?>
						</div>
						<? if (!empty($arItem["DETAIL_TEXT"])): ?>
							<?= $arItem["DETAIL_TEXT"] ?>
						<? endif; ?>
					</div>
				</div>
			</section>
		<? endif;
			$cnt++; ?>
			<?
			break;
		case 3:
			?>
			<section class="blue-block-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<h3><?= $arItem["~NAME"] ?></h3>
						</div>
					</div>
				</div>
			</section>
			<?
			break;
		case 4:
			?>
			<section class="blue-block">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="ribbon">
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive" alt="">
							</div>
							<p><?= $arItem["PREVIEW_TEXT"] ?></p>
						</div>
					</div>
				</div>
			</section>
			<?
			break;
	}

	if ($itemNum === 3):
    ?>
        <div id="WIDGET_CONSULTATION_FORM"></div>
    <?php
    endif;
}