<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["OFFICE_TITLE"] = "Офисы в ";
$MESS["ELSE_OFFICE"] = "Ещё CNT WORD";
$MESS["PB_CONTACTS_CT_CB_SR_ORDER"] = "Оставьте заявку";
$MESS["PB_CONTACTS_CT_CB_SR_JS_SLIDE_DOWN"] = "Свернуть";
$MESS["PB_CONTACTS_CT_CB_SR_JS_ACTIONS"] = "Действия";
$MESS["PB_CONTACTS_CT_CB_SR_JS_CLICK"] = "Клик";
$MESS["PB_CONTACTS_CT_CB_SR_JS_MOSCOW_CBLOCK"] = "Москва - контактный блок";
$MESS["PB_CONTACTS_CT_CB_SR_JS_CBLOCK"] = "Контактный блок";
$MESS["PB_CONTACTS_CT_CB_SR_JS_OFFICE_SHOW"] = "Показ офиса";