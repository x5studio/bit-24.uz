<?

namespace Bit\AdditionalOptimizer\Image\Type;

interface TypeInterface {

    public function getName();

    public function getImageExist();

    public function getImagePath();

    public function createImage();
    
    public function removeImage();

}