/*
  компонент реализуший сиглализацию о прошедших этапах загрузки страницы,
  грубо говоря присловутое $.ready() для нашего фронтенда

  <!!!> должен вызваться до генерации соответствуюших событий (например может быть указан как зависимость для main)

  ВОТ ТАК ПРОПИСЫВАТЬ В main.js БЕСПОЛЕЗНО:

      require(['Ready'], function(){})
      ...весь main.js...

  НАДО:

    if (typeof define === "function" && define.amd) {
      define(['jquery', 'Controls/Ready'], function () {

        ...весь main.js...

      })
    }
*/

if (typeof define === "function" && define.amd) {

	define(['jquery', 'pb.main/node_modules/babel-polyfill/browser'], function ($) {

		function Ready() {
			this.lazyLoadRecived = new Promise(function (resolve, reject) {
				$(document).on("lazyLoadReceived", function () {
					resolve();
				});
			});
		}

		return new Ready();
	})
}