requirejs.config({
    baseUrl: "/local/templates/main/assets/js",
    urlArgs: 'v=0.0.51', 
    waitSeconds: 0,
    config: {
        'Controls/Responsive': {
            borderTypes: {
                xsmall: [0, 479],
                small: [480, 767],
                medium: [768, 979],
                large: [980, 99999]
            }
        }
    },
    deps: ['jquery', 'modernizr', 'fancybox'],
    paths: {
        main: 'main',
        Controls: 'Controllers',
        fancybox: 'vendor/fancybox/dist/jquery.fancybox.min',
        flipclock: 'vendor/flipclock/compiled/flipclock.min',
        maskedinput: 'vendor/jquery.maskedinput/dist/jquery.maskedinput.min',
        jqueryRouter: 'vendor/jquery.router/index',
        easing: 'vendor/jquery-easing-original/jquery.easing.min',
        mousewheel: 'vendor/jquery-mousewheel/jquery.mousewheel.min',
        validate: 'vendor/jquery-validation/dist/jquery.validate.min',
        ouibounce: 'tools/ouibounce',
        flexslider: 'vendor/flexslider/jquery.flexslider-min',
        uniform: 'vendor/jquery.uniform/jquery.uniform.min',
        actual: 'vendor/jquery.actual/jquery.actual.min',
        modernizr: 'tools/modernizr.min',
        cookie: 'tools/cookie',
        generalTools: 'tools/tools',
        router: 'tools/router',
        bootstrap: 'vendor/bootstrap/dist/js/bootstrap',
        cusel: 'vendor/cusel.custom/index',
        slick: 'vendor/slick-carousel/slick/slick.min',
        owlcarousel2: 'vendor/owl.carousel/dist/owl.carousel.min',
        device: 'device.min',
        revealator:'vendor/revealator/fm.revealator.jquery.min',
        scrollTo: 'vendor/jquery.scrollTo/jquery.scrollTo.min'
    },
    shim: {
        'easing': {
            deps: ['jquery'],
            exports: 'jQuery.easing'
        },
        'fancybox': {
            deps: ['jquery'],
            exports: 'jQuery.fancybox'
        },
        'ouibounce': {
            deps: [],
            exports: 'ouibounce'
        },
        'jqueryRouter': {
            deps: ['tools/trash'],
            exports: 'jQuery.router'
        },
        'modernizr': {
            exports: 'Modernizr'
        },
        'validate':{
            exports: '$.validator'
        }
    }
});
define('jquery', [], function () {
    return window.jQuery
})