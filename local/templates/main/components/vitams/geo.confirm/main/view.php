<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use PB\Main\Loc;
?>
<div id="geo-confirm-wrapp">
    <div id="geo_confirm" <?if($arParams['SMART_REDIRECT'] == 'Y') {?>data-smart-confirm="true"<?}?> data-link-baseurl="<?=!empty($arParams["LINK_IN_BASEURL"]) ? $arParams["LINK_IN_BASEURL"] : 'Y'?>" data-sef-mode="<?=!empty($arParams["SEF_MODE"]) ? $arParams["SEF_MODE"] : "N" ?>">
        <div class="inner-shadow"></div>
        <div class="close j-close"></div>
        <p><?=Loc::getMessage("PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_YOUR_LOCATION");?><br>
            <b class="name"><?=$_SESSION['GEOIP']['city_name']?></b></p>
        <button class="btn btn-mini confirm-btn"><?=Loc::getMessage("PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_TRUE");?></button><span><?=Loc::getMessage("PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_OR");?></span>
        <?
        global $cityFilter;
        $cityFilter = Array(
            "CITY_CODE" => $_SESSION["REGION"]["CODE"],
        );
        $APPLICATION->IncludeComponent("vitams:city.dropdown", $arParams['TEMPLATE_CITY_DROPDOWN'],
            Array(
                "IBLOCK_TYPE" => IBT_CONTACTS,
                "IBLOCK_ID" => IB_CONTACTS,
                "SECTION_CODE" => "",
                "SECTION_URL" => "",
                "COUNT_ELEMENTS" => "Y",
                "TOP_DEPTH" => "1",
                "SECTION_FIELDS" => "",
                "FILTER_NAME" => "cityFilter",
                "SECTION_USER_FIELDS" => "",
                "ADD_SECTIONS_CHAIN" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "36000000",
                "CACHE_NOTES" => "",
                "CACHE_GROUPS" => "N",
                "CACHE_FILTER" => "Y",
                "LINK_IN_BASEURL" => "Y",
                "LINK_WITH_SUBDOMAIN" => "Y",
                "SEF_MODE" => "N",
                "SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
                "SEF_URL_TEMPLATES" => array(),
                "HTML_LIST_ID" => "header-city-list-confirm",
                "HTML_TITLE" => '<a href="##HTML_LIST_ID#" class="header-city '.$arParams['CLASS_JCITY_POPUP'].'"><strong>' . Loc::getMessage("PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_CHOISE_REGION_LOWER") .'</strong></a>',
                "SHOW_MOSCOW" => $_SESSION['SHOW_MOSCOW'],
                "COUNTRY_CODE" => $_SESSION['GEOIP']['country_code'],
                'LINK_LOCAL_CHOOSE' => COption::GetOptionString(\PB\Geo\Meta::MODULE_ID, 'SMART_REDIRECT', 'N'),
                'UI_CITY_STRONG' => $arParams['UI_CITY_STRONG'],
                'UI_CITY_SPACE' => $arParams['UI_CITY_SPACE'],
                'ID_RUSSIA' => $arParams['ID_RUSSIA'],
                'MSK_OBL_CITY_CODE'=>$arParams['MSK_OBL_CITY_CODE'],
                'MSK_OBL_CITY_IN_COL' => $arParams['MSK_OBL_CITY_IN_COL'],
                'UI_CITY_PRIORITET' => $arParams['DROPDOWN_UI_CITY_PRIORITET'],
                'SMART_REDIRECT' => $arParams['SMART_REDIRECT'],
                'LINK_LOCAL_CHOOSE' => $arParams['LINK_LOCAL_CHOOSE'],
                'IS_CONFIRM_DROPDOWN' => 'Y',
                'CACHE_BY_REAL_COUNTRY' => $_SESSION['GEOIP']['country']['iso']
            ), false,
            Array("HIDE_ICONS"=>"Y")
        );
        ?>
    </div>
</div>
<?=\PB\Main\Loc::getJsMessByNameHtml(array(
        'PB_GEO_CT_GC_DEFAULT_JS_TRACK_1',
        'PB_GEO_CT_GC_DEFAULT_JS_KEYUP_1',
        'PB_GEO_CT_GC_DEFAULT_JS_CHOISE_1',
    )
)?>