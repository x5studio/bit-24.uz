<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arParams['BEFORE_RESULT_FILE']) {
	$PATH = $arParams['BEFORE_RESULT_FILE'];
} else {
	$PATH = $this->GetPath()."/include/general.befsend.php";
}
include($_SERVER["DOCUMENT_ROOT"].$PATH);