<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("form_title", "Оставить заявку на внедрение Битрикс24");
$APPLICATION->SetPageProperty("description", "На счету компании Первый БИТ более 250 внедрений корпоративных порталов Битрикс24. Вы можете ознакомиться с нашими кейсами в разделе \"Наши клиенты\".");
$APPLICATION->SetPageProperty("keywords", "портфолио, наши клиенты, битрикс24, bitrix24, корпоративные порталы, crm");
$APPLICATION->SetPageProperty("title", "Портфолио внедрений корпоративных порталов Битрикс24 - Первый Бит");
$APPLICATION->SetTitle("Портфолио внедрений корпоративных порталов Битрикс24");
?><?$APPLICATION->IncludeComponent(
	"client.list",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"COUNT" => 10000,
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"IBLOCK_ID" => "8",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "blog",
		"PAGER_TITLE" => "Новости"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>