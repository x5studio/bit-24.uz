<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

if(isset($_REQUEST['TYPE']) && $_REQUEST['TYPE'] == 'CONFIRM' && isset($_REQUEST['CITY'])){
    $APPLICATION->RestartBuffer();
    require $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/pb.geo/geo/ajax.php';
    die();
}
