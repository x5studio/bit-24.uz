<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//delayed function must return a string
if (empty($arResult))
    return "";

$strReturn = '';

$strReturn .= '<section class="breadcrumb-block hidden-xs"><div class="container"><div class="row"><ol class="breadcrumb"  itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++)
{
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    if($index == $itemSize-1) {
        //если выводится последний элемент крошек, то выводим его без ссылки
        $strReturn .= '<li><span class="link-style"><span>' . $title . '</span></span><meta content="'.($index + 1).'" /></li>';
    } else {
        if ($arResult[$index]["LINK"] <> ""/* && $index != $itemSize - 1*/)
        {
            $strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="' . $arResult[$index]["LINK"] . '" itemprop="item"><span itemprop="name">' . $title . '</span></a><meta itemprop="position" content="'.($index + 1).'" /></li>';
        } else
        {
            $strReturn .= '<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><span itemprop="item"><span itemprop="name">' . $title . '</span></span><meta itemprop="position" content="'.($index + 1).'" /></li>';
        }
    }
}
$strReturn .= '</ol></div></div></section>';

return $strReturn;