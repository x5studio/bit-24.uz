if (typeof define === "function" && define.amd) {
    define(['flexslider'], function () {
        function SliderSimilar(container) {
            var _this = this;
            this.container = $(container);

            this.carouselId = '#' + this.container.find('.flexslider.carousel').attr('id');
            this.carouselItemWidth = parseInt(this.container.find('.flexslider.carousel').attr('data-width'));

            $(this.carouselId).flexslider({
                pauseOnHover: true,
                useCSS: false,
                animation: "slide",
                controlNav: false,
                directionNav: true,
                animationLoop: false,
                slideshow: false,
                itemWidth: _this.carouselItemWidth,
                itemMargin: 5
            });
        }

        return SliderSimilar;
    });
}