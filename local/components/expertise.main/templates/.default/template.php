<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
	<section class="fields">
		<div class="container">
			<h2 class="fields-heading">за 250 000 внедрений Битрикс24 мы накопили опыт<br>и разработали решения для различных отраслей бизнеса</h2>
			<div class="row row-centered">
				<? foreach ($arResult as $arItem): ?>
					<div class="col-md-25 col-sm-4 col-xs-12 ffli">
						<div class="<?= $arItem["UF_CLASS"] ?>">
							<a href="<?= $arItem["SECTION_PAGE_URL"] ?>" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a>
						</div>
					</div>
				<? endforeach; ?>
				<div class="col-md-25 col-sm-12 col-xs-12 ffli">
					<div class="icons-block-all">
						<a href="/otraslevaja-jekspertiza/" title="link"><p>Все отрасли</p></a>
					</div>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>