<?

namespace Bit\AdditionalOptimizer;

use Bitrix\Main\Config\Option;

class Main{


	static function logger($data){

		if(\Bitrix\Main\Config\Option::get('bit.additionaloptimizer','debug_on','N') == 'Y'){

			$time = ' ['.date('d-m-Y h:i:s').'] ';

			file_put_contents(\Bitrix\Main\Application::getDocumentRoot().DIRECTORY_SEPARATOR.date('d-m-Y').'-'.__FUNCTION__.'.txt', $time . print_r($data, true)."\n\n", FILE_APPEND);
		}

	}


}
