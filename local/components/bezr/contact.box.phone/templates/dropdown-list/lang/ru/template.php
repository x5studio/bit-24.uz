<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["OFFICE_TITLE"] = "Офисы в ";
$MESS["ELSE_OFFICE"] = "Ещё CNT WORD";
$MESS["PB_CONTACTS_CT_CB_DL_PHONES"] = "телефоны";
$MESS["PB_CONTACTS_CT_CB_DL_JS_ACTIONS"] = "Действия";
$MESS["PB_CONTACTS_CT_CB_DL_JS_CLICK"] = "Клик";
$MESS["PB_CONTACTS_CT_CB_DL_JS_MOSCOW_CBLOCK"] = "Москва - контактный блок";
$MESS["PB_CONTACTS_CT_CB_DL_JS_CBLOCK"] = "Контактный блок";
$MESS["PB_CONTACTS_CT_CB_DL_JS_OFFICE_SHOW"] = "Показ офиса";
$MESS["PB_CONTACTS_CT_CB_DL_JS_SLIDE_DOWN"] = "Свернуть";