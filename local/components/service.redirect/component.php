<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;


if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
if (strlen($arParams["SECTION_CODE"]) <= 0)
{
	$this->AbortResultCache();
	ShowError('Не указан раздел');
	return;
}

if ($this->StartResultCache())
{
	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$rsSection = \CIBlockSection::GetList(
		array("SORT" => "ASC"),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"CODE" => $arParams["SECTION_CODE"],
		),
		false,
		array("ID", "DEPTH_LEVEL"),
		array("nTopCount" => 1)
	);
	if ($arSection = $rsSection->GetNext())
	{
		if ($arSection["DEPTH_LEVEL"] == 1)
		{
			$rsSubSection = \CIBlockSection::GetList(
				array("SORT" => "ASC"),
				array(
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ACTIVE" => "Y",
					"SECTION_ID" => $arSection["ID"],
				),
				false,
				array("ID"),
				array("nTopCount" => 1)
			);
			$arSection = $rsSubSection->GetNext();
		}

		if ($arSection["ID"])
		{
			$rsElement = \CIBlockElement::GetList(
				array("SORT" => "ASC"),
				array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "SECTION_ID" => $arSection["ID"]),
				false,
				array("nTopCount" => 1),
				array("ID", "DETAIL_PAGE_URL")
			);
			if ($arElement = $rsElement->GetNext())
			{
				$arResult = $arElement["DETAIL_PAGE_URL"];
			}
		}

	}

	$this->endResultCache();
}
if (strlen($arResult) > 0)
{
	LocalRedirect($arResult);
} elseif (Loader::IncludeModule("iblock"))
{
	$this->AbortResultCache();
	\Bitrix\Iblock\Component\Tools::process404(
		""
		, true
		, true
		, true
		, "/404.php"
	);
	return;
}