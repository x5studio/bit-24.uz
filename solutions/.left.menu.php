<?
$aMenuLinks = Array(

    Array(
        "CRM Битрикс24",
        "/crm-bitrix24/",
        Array(),
        Array(
            'ICON_CLASS' => 'menu-bitrix24'
        ),
        ""
    ),
    Array(
        "Корпоративный портал Битрикс24",
        "/korporativniy-portal-bitrix24/",
        Array(),
        Array(
            'ICON_CLASS' => 'menu-ico18'
        ),
        ""
    ),
    Array(
        "Интернет-магазин + CRM",
        "/1c-bitrix24-store-crm/",
        Array(),
        Array(
            'ICON_CLASS' => 'menu-ico19'
        ),
        ""
    ),
    Array(
        "Возможности Битрикс24",
        "/vozmozhnosti-bitrix24/crm/",
        Array(),
        Array(
            'ICON_CLASS' => 'menu-ico20'
        ),
        ""
    ),
    Array(
        "Коробочная версия",
        "/korobochnaya-versiya-bitrix24/",
        Array(),
        Array(
            'ICON_CLASS' => 'menu-icobox'
        ),
        ""
    ),
    Array(
        "Облачная версия",
        "/oblachnaya-versiya-bitrix24/",
        Array(),
        Array(
           'ICON_CLASS' => 'menu-icocloud'
        ),
        ""
    ),

);
?>