<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Заголовок очердной новости");
$APPLICATION->SetTitle("Заголовок очердной новости");
$APPLICATION->SetAdditionalCss("/blog_new/styles.css");
?>
<div class="blog-detail-wrap">
	<div class="container">

		<div class="col-sm-8 col-md-9 col-xs-12 blog-detail-content-wrap">
			<h1>Новое весеннее Обновление для мобильного
приложения Битрикс24</h1>

			<div class="tag-date-wrap">
				<span class="date">30.12.2017</span>
				<span class="tag deep-blue">Новости</span>
			</div>
			
			<div class="blog-detail-content">
				<p>Об этом обновлении шла речь в редакции Битрикс24.Гонконг, представленной 1 марта.<br/>И вот, наконец, обновленное приложение было выпущено.</p>
				<img src="/blog_new/images/detail/1_screen.png" alt="" />
				<h2>CRM для сферы услуг</h2>
				<p>Теперь стало еще проще организовывать продажу услуг в Битркис24. Все сферы – от салонов красоты до школ иностранного языка – могут контролировать график работы специалистов, аренду помещения и все необходимые инструменты, вплоть до тонкой настройки графика каждого сотрудника. Теперь весь этот сложный функционал удобно и понятно представлен в новой функции «Бронирования ресурсов»!</p>
				<img src="/blog_new/images/detail/2_screen.png" alt="" />
			</div>

		</div>

		<div class="col-sm-4 col-md-3 col-xs-12 blog-detail-sidebar">
			<div class="author clearfix">
				<img src="/blog_new/images/detail/editor.png" alt="">
				<div class="author-desc">
					<span class="author-name">Жуковский Александр</span>
					<span class="author-role">Редактор</span>
				</div>
			</div>

			<div class="socnet-wrap">
				<span class="title">Рассказать друзьям</span>
			</div>

			<div class="interesting-block">
				<div class="title-block clearfix">
					<span>Интересно</span>
					<img src="/blog_new/images/detail/interesting_ico.png" alt="">
				</div>
				<div class="interesting-block-inner">
					<p class="subtitle">Обзор всех функций нового Битрикс24.гонконг</p>
					<p>Рассказываем о новых функциях, показанных 1 марта в рамках бизнес-встречи Битрикс24.Идея, в новой редакции Битркис24.Гонконг.</p>
					<span class="date">30.12.2017</span>
				</div>
			</div>

			<div class="free-subscribe-wrap">
				<span class="title">Бесплатная подписка</span>
				<p>Никакого спама - только качественный контент</p>
				<form class="clearfix" action="">
					<input type="text" class="form-control" placeholder="E-mail" />
					<button class="btn btn-default submit" type="submit">подписаться</button>
				</form>
			</div>

		</div>
	</div>

	<div class="content-bottom">
		<div class="container">
			<div class="col-sm-8 col-xs-12">
				<div class="col-md-6 col-sm-6 col-xs-12 text text-left">
					<a class="btn btn-sm" onclick="javascript:history.back(); return false;">Вернуться назад</a>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 date text-right">
						<script type="text/javascript">(function () {
		            if (window.pluso)if (typeof window.pluso.start == "function") return;
		            if (window.ifpluso == undefined) {
		                window.ifpluso = 1;
		                var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
		                s.type = 'text/javascript';
		                s.charset = 'UTF-8';
		                s.async = true;
		                s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
		                var h = d[g]('body')[0];
		                h.appendChild(s);
		            }
		        })();</script>
					<div class="pluso" data-background="transparent"
								 data-options="medium,square,line,horizontal,counter,theme=04"
								 data-services="vkontakte,facebook,odnoklassniki,moimir"></div>
				</div>
			</div>
		</div>
		
	</div>


</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>