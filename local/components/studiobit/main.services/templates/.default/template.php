<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult["ITEMS"])): ?>

    <section class="sevices">
        <div class="container">
            <h2 class="fields-heading">Когда нужен Битрикс24</h2>

            <div class="row">
	<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
                <div class="service_card col-xs-12 col-sm-6 col-lg-3 col-md-3">

                    <<?=$arItem["LINK"] ? 'a href="'.$arItem["LINK"].'"':'div' ?> class="service_card_content">
                        <span class="title"><?= $arItem["NAME"] ?></span>
                        <img src="<?= $arItem["PREVIEW_PICTURE"] ?>" alt="<?= $arItem["NAME"] ?>">
						<?= $arItem["PREVIEW_TEXT"] ?>
                        <!-- <span class="more">Подробнее</span> -->
                    </<?=$arItem["LINK"] ? 'a':'div' ?>>

                </div>
	<? endforeach; ?>
            </div>
        </div>

    </section>

<? endif; ?>