if (typeof define === "function" && define.amd) {
    define(['easing'], function () {
        function RightBoxSlide(p) {

            var _this = this;
            this.root = $(p.root);
            this.box = $(p.box, p.root);
            this.img = $(p.box + ' img', p.root);
            this.text = $(p.text, p.root);
            this.title = $(p.title, p.root);
            this.subscribe = $(p.subscribe, p.root);
            this.h2 = $(p.h2, p.root);

            // remove & add
            this.subscribe.css({position: 'relative'});
            this.box.unbind();
            this.img.hide();
            this.text.css({top: 0});
            this.title.css({height: 49, cursor: 'pointer'});
            $('ul, a', this.box).hide();
            this.title.click(function () {
                window.location.href = $('.box-img-text > a', this).attr('href');
            });

            // hover
            this.title.hover(
                function () {
                    var hover = $(this);
                    hover.addClass('hovers');
                    setTimeout(function () {
                        if (hover.hasClass('hovers')) {
                            $('h2', hover).animate({height: 59}, 200, 'easeOutCubic');
                            hover.animate({height: 59}, 200, 'easeOutCubic').removeClass(p.open);
                        }
                    }, 50);
                },
                function () {
                    $(this).removeClass('hovers');
                    $('h2', this).animate({height: 49}, 200, 'easeOutCubic');
                    $(this).animate({height: 49}, 200, 'easeOutCubic').removeClass(p.open);
                }
            );
        }

        return RightBoxSlide;
    });
}