<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<script>
    var blogCurSectionCode = '<?=$arParams['SECTION_CODE']?>';
</script>

<?
$GLOBALS['blogCurSectionFilter'] = [
    'CODE' => $arParams['SECTION_CODE'],
];
?>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.section.list",
    "blogs",
    array(
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COUNT_ELEMENTS" => "Y",
        "FILTER_NAME" => "blogCurSectionFilter",
        "IBLOCK_ID" => "2",
        "IBLOCK_TYPE" => "blog",
        "SECTION_CODE" => "",
        "SECTION_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "SECTION_ID" => "",
        "SECTION_URL" => "",
        "SECTION_USER_FIELDS" => array(
            0 => "",
            1 => "",
        ),
        "SHOW_PARENT_NAME" => "Y",
        "TOP_DEPTH" => "1",
        "VIEW_MODE" => "LINE",
        "COMPONENT_TEMPLATE" => "blogs"
    ),
    false
);?>
