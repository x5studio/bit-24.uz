if (typeof define === "function" && define.amd) {
    define(['flexslider'], function () {
        function SliderLoop(container) {
            var _this = this;
            this.container = $(container);
            var controlNav = this.container.data('controlNav') || false;
            _this.container.flexslider({
                pauseOnHover: true,
                animation: 'slide',
                animationLoop: true,
                //smoothHeight: true,
                itemMargin: 0,
                move: 1,
                useCSS: false,
                controlNav: controlNav,
                directionNav: true,
                startAt: 0,
                slideshow: false,
                slideshowSpeed: 5000,
                start: function (slider) {
                    if (typeof slider.container == 'object') {
                        slider.container.css({
                            overflow: 'hidden',
                            height: slider.slides.eq(slider.currentSlide).height()
                        });
                    }
                },
                before: function (slider) {
                    slider.container.animate({
                        height: slider.slides.eq(slider.animatingTo).height()
                    }, {
                        duration: 600,
                        queue: false
                    });
                }
            });
        }

        return SliderLoop;
    });
}