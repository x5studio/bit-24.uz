BX.ready(function () {
    $('.offer__period-btn').on('click', function (e) {
        var elems = $(".offer__wrapper-price");
        var elemsTotal = elems.length;
        for (var i = 0; i < elemsTotal; ++i) {
            $(elems[i]).hide();
        }
        $('#' + this.id + '-price').show();

        $('#do-order').attr('data-form-field-type', 'Купить ' + $('#product-name')[0].innerText + ' на срок ' + this.innerHTML.split('<span>')[0].trim());
        console.log($('#do-order').innerText);
    });
})
