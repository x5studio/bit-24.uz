if (typeof define === "function" && define.amd) {
    define(['cusel', 'mousewheel'], function () {
        function Select2Select(p) {

            var o = this;

            this.root = $(p.root);
            this.select1 = $(p.select1, this.root);
            this.select2 = $(p.select2, this.root);
            this.stage = "beginning";

            this.ChangeChildrenCuselSelect = function (parentID) {
            	var trueOptions = o.rootDOM.select2BaseList.find("span").filter("[" + p.attrInherit + " = '" + parentID + "']").clone();
                o.select2List.html(trueOptions);
                if (trueOptions.length == 0) {
                    if (o.rootDOM.select2BaseList.title.length > 0) {
                        o.select2List.prepend(o.rootDOM.select2BaseList.title.clone());
                    } else {
                        o.select2List.prepend($('<span val="" parent-id="0">Варианты отсутствуют</span>'));
                    }
                    o.select2.addClass("classDisCusel");
                    o.select2Input.val('');
                } else if (trueOptions.length > 1) {
                	if (o.rootDOM.select2BaseList.title.length > 0) {
                    	o.select2List.prepend(o.rootDOM.select2BaseList.title.clone().addClass('empty'));
                    	if(o.select2List.find("span.empty_span").length > 0 && o.select2List.find("span.empty_span").attr('val') !='') {
                    		o.select2Input.val(o.select2List.find("span.empty_span").attr('val'));
                    	}
                        else {
                        	o.select2Input.val('');
                        }
                    } else {
                        trueOptions.first().addClass('cuselActive');
                        o.select2Input.val(trueOptions.first().attr("val"));
                    }
                    if (o.select2.hasClass("classDisCusel")) {
                        o.select2.removeClass("classDisCusel");
                    }
                } else {
                    o.select2Input.val(trueOptions.attr("val"));
                    o.select2.addClass("classDisCusel");
                }
                var titleText = o.select2List.find('span.cuselActive').text() || o.select2List.children().first().text();
                o.select2.find(".cuselText").text(titleText);

                var maxItems = 5;
                if (o.select2.hasClass('j-select-smart')) {
                    maxItems = o.select2.data('visRows') || o.select2List.children().length || maxItems;
                }
                cuSelRefresh({
                    refreshEl: "#" + p.select2,
                    visRows: (o.select2List.children().length > maxItems) ? maxItems : o.select2List.children().length,
                    scrollArrows: true
                });
                if (o.select2.hasClass('j-select-smart')) {
                    o.select2List.find('.empty').addClass('hidden');
                    if (o.select2.hasClass('default-first-item')) {
                        if (o.select2List.find('.empty').hasClass('cuselActive')) {
                            o.select2List.find('.empty').removeClass('cuselActive').next('span').addClass('cuselActive');
                            o.select2Input.val(o.select2List.find('.empty').next('span').attr("val"));
                            var titleText = o.select2List.find('span.cuselActive').text() || o.select2List.children().first().text();
                            o.select2.find(".cuselText").text(titleText);
                        }
                    }
                }
                if(trueOptions.length>10){
                    if (!o.select2.hasClass("twoColumns")) {
                        o.select2.addClass('twoColumns');
                    }
                } else {
                    if (o.select2.hasClass("twoColumns")) {
                        o.select2.removeClass('twoColumns');
                    }
                }
                $(document).trigger("changeCuselSelect",[o.select1,o.select2]);
            }

            this.initByCusel = function () {
                o.select1 = $("#cuselFrame-" + p.select1, o.root);
                o.select1List = $("#cusel-scroll-" + p.select1, o.root);
                if (o.select1.length > 0) {
                    o.select1Input = $("#" + p.select1, o.select1);
                } else {
                    o.select1Input = $("#" + p.select1, o.root);
                }

                o.select2 = $("#cuselFrame-" + p.select2, o.root);
                o.select2List = $("#cusel-scroll-" + p.select2, o.root);
                o.rootDOM = o.root.get(0);
                if (!o.rootDOM.select2BaseList) {
                	o.rootDOM.select2BaseList = o.select2List.clone();
                    var firstItem = o.rootDOM.select2BaseList.find("span.empty_span");
                    //if (!firstItem.attr('val') || firstItem.attr('val') == "queue") {
                    if (firstItem.length > 0) {
                    	o.rootDOM.select2BaseList.title = firstItem;
                    }
                    else {
                    	o.rootDOM.select2BaseList.title = $();
                    }
                }
                o.select2Input = $("#" + p.select2, o.office);

                o.select1Input.change(function () {
                    var curList = $(this).val();
                    o.ChangeChildrenCuselSelect(curList);
                });
                o.ChangeChildrenCuselSelect(o.select1Input.val());

                o.stage = "init";
            };

            this.initGeneral = function () {
            };

            this.init = function () {            	
                if (p.cuselLists) {
                    o.initByCusel();
                } else {
                    o.initGeneral();
                }
            }
        }

        return Select2Select;
    });
}