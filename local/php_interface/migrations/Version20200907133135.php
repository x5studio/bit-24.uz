<?php

namespace Sprint\Migration;


class Version20200907133135 extends Version
{
    protected $description = "Создание 3х своств в инфоблок Блог";

    protected $moduleVersion = "3.16.3";

    /**
     * @return bool|void
     * @throws Exceptions\HelperException
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('BLOG', 'blog');
        $helper->Iblock()->saveProperty($iblockId, array(
            'NAME' => 'Сколько минут читать',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'TIME_READ',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => null,
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => null,
            'USER_TYPE_SETTINGS' => null,
            'HINT' => '',
        ));
        $helper->Iblock()->saveProperty($iblockId, array(
            'NAME' => 'Содержание',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'SB_LINK',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'Y',
            'XML_ID' => null,
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'Y',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => null,
            'USER_TYPE_SETTINGS' => null,
            'HINT' => '',
        ));
        $helper->Iblock()->saveProperty($iblockId, array(
            'NAME' => 'Содержание HTML',
            'ACTIVE' => 'Y',
            'SORT' => '500',
            'CODE' => 'SB_LINK_2',
            'DEFAULT_VALUE' =>
                array(
                    'TEXT' => '',
                    'TYPE' => 'HTML',
                ),
            'PROPERTY_TYPE' => 'S',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => null,
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => 'HTML',
            'USER_TYPE_SETTINGS' =>
                array(
                    'height' => 200,
                ),
            'HINT' => '',
        ));

    }

    public function down()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('BLOG', 'blog');
        $helper->Iblock()->deletePropertyIfExists($iblockId, 'TIME_READ');
        $helper->Iblock()->deletePropertyIfExists($iblockId, 'SB_LINK');
        $helper->Iblock()->deletePropertyIfExists($iblockId, 'SB_LINK_2');
    }
}
