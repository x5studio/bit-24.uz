<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


use PB\Contacts\ContactManager;

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    // ID веб-формы
    $arParams = array(
		"WEB_FORM_ID" => 1,  // ID веб-формы
		"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
		"COMPANY_IBLOCK_ID" => IB_CONTACTS,
		"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
		"EMAIL_QUESTION_CODE" => "EMAIL",
		"CITY_QUESTION_CODE" => "CITY",
        "OFFICE_QUESTION_CODE" => "OFFICE",
		"TO_QUESTION_RESIPIENTS" => "",
		"EMAIL_ONLY_FROM_RESIPIENTS" => "",
		"PRODUCT_QUESTION_CODE" => "PRODUCT",
		"TO_QUESTION_CODE" => "TO",
		"USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
		"ALERT_ADD_SHARE" => "N",
		"HIDE_PRIVACY_POLICE" => "Y",
		"COMPONENT_MARKER" => "request",
    );


    if (!empty($_POST) && CModule::IncludeModule("form")) {
        


        global $obOfficeFilter;

        $contMng = ContactManager::getInstance();

        $arOffice = $contMng->getQueueInstance()->getOfficeQueue($obOfficeFilter, array("IBLOCK_SECTION_ID" => $_SESSION['REGION']['ID']),null, '/lp/');


        $arForm = CForm::GetByID($arParams["WEB_FORM_ID"])->Fetch();
        $arParams["WEB_FORM_ID"] = CForm::GetDataByID($arParams["WEB_FORM_ID"], $arResult["arForm"], $arResult["arQuestions"], $arResult["arAnswers"], $arResult["arDropDown"], $arResult["arMultiSelect"], 'N');

        $comment = array(
            'Consult' => 'Консультация по тарифам Б24',
            'Kupit_Bitrix' => 'Купить Б24'
        );

        $arResult['arrVALUES'] = array(
            'form_text_1' => $_POST['Name'],//имя
            'form_text_2' => $_POST['Phone'],//телефон
            'form_text_3' => '',//почта
            'form_hidden_4' => $_SESSION['REGION']['ID'], // ид города
            'form_hidden_5' => 'queue', //офис
            'form_hidden_6' => '', //юнион
            'form_hidden_7' => ($arOffice) ? $arOffice['PROPERTIES']['email']['VALUE'] : '',//почта офиса
            'form_hidden_8' => '',
            'form_hidden_27' => isset($comment[$_POST['tildaspec-formname']]) ? $comment[$_POST['tildaspec-formname']] : 'Оставить заявку',
            'form_hidden_30' => '',
            'form_hidden_94' => '',
            'form_hidden_84' => '',
        );

        $arResult['arrOrigVALUES'] = $arResult['arrVALUES'];


        include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/bezr/form.result.new.befsend/include/general.befsend.php");

        $vCity = $arResult['arrOrigVALUES']["form_{$arResult["arAnswers"]["CITY"][0]['FIELD_TYPE']}_{$arResult["arAnswers"]["CITY"][0]['ID']}"];
        $vOffice = $arResult['arrOrigVALUES']["form_{$arResult["arAnswers"]["OFFICE"][0]['FIELD_TYPE']}_{$arResult["arAnswers"]["OFFICE"][0]['ID']}"];


        if ($RESULT_ID = CFormResult::Add($arParams["WEB_FORM_ID"], $arResult["arrVALUES"])) {
        
            // send email notifications
            CFormResult::SetEvent($RESULT_ID);

            if (CModule::IncludeModule("pb.union") && \Union\Form\FormResult::checkSendCrm($arResult)) {
                \Union\Form\FormResult::Mail($RESULT_ID, $arResult['SEND_MAIL_ONLY'], $arParams["WEB_FORM_ID"], $vCity, $APPLICATION->GetCurDir(), $arResult["arAnswers"]["UNIONRESULT"][0]["ID"]);
            } else {
                if (!empty($arResult['SEND_MAIL_ONLY'])) {
                    foreach ($arResult['SEND_MAIL_ONLY'] AS $mailTmplItem) {
                        CFormResult::Mail($RESULT_ID, $mailTmplItem);
                    }
                } else {
                    CFormResult::Mail($RESULT_ID);
                }
            }

            
            $arFilterSid = array("IBLOCK_SECTION_ID" => $vCity);
            
            $sid = $contMng->getQueueInstance()->getQueueSID($obOfficeFilter, $arFilterSid);
            
            $cursor = ($sid)?$contMng->getQueueInstance()->getCursorQueue($sid):"";

        }
    }

    if ($RESULT_ID)
    {
        echo json_encode(array('message' => "Благодарим за заявку, Мы скоро свяжемся с вами!"));
    }
    else
    {
        global $strError;

        if(!$strError) $strError = 'Что-то пошло не так!';

        echo json_encode(array('error' => $strError));
    }


    die();
}

localRedirect('/');
