if (typeof define === "function" && define.amd) {
    define(['Controls/Shuffle'], function (Shuffle) {
        function ShuffleWeight(p) {
            Shuffle.apply(this, arguments);

        }

        extend(ShuffleWeight, Shuffle);

        var rand = function (min, max) {
            return Math.random() * (max - min) + min;
        };

        var getRandomItem = function (list, weight) {
            var total_weight = weight.reduce(function (prev, cur, i, arr) {
                return prev + cur;
            });

            var random_num = rand(0, total_weight);
            var weight_sum = 0;

            for (var i = 0; i < list.length; i++) {
                weight_sum += weight[i];
                weight_sum = +weight_sum.toFixed(2);

                if (random_num <= weight_sum) {
                    return list[i];
                }
            }
        };

        ShuffleWeight.prototype.sort = function ($fieldName) {
            var o = this;

            var sortedItems = [];
            var itemsReduce = o.items.slice(0);
            var itemsLength = o.items.length;
            for (var i = 0; i < itemsLength; i++) {
                var list = [];
                var weight = [];
                for (var j in itemsReduce) {
                    var item = itemsReduce[j];
                    list.push(j);
                    if (typeof $fieldName == 'function') {
                        weight.push(parseFloat($fieldName(item)) || 0);
                    } else {
                        weight.push(parseFloat(item[$fieldName]) || 0);
                    }
                }
                var ndx = getRandomItem(list, weight);
                sortedItems.push(itemsReduce[ndx]);
                itemsReduce.splice(ndx, 1)
            }
            o.items = sortedItems;
        };

        return ShuffleWeight;
    });
}