<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Отраслевая экспертиза. Детально",
	"DESCRIPTION" => "Отраслевая экспертиза. Детально",
	"ICON" => "/images/news_line.gif",
	"SORT" => 140,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "expertise",
			"NAME" => "Отраслевая экспертиза"
		)
	),
);

?>