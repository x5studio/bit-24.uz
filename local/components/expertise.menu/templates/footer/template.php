<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult["ITEMS"])): ?>
	<span class="col-md-12 col-sm-12 hidden-xs h4">Отраслевая экспертиза</span>
	<nav class="col-md-12 hidden-sm hidden-xs">
		<ul class="list-unstyled">
			<? foreach ($arResult["ITEMS"] as $arItem): ?>
				<li><a href="<?= $arItem["SECTION_PAGE_URL"] ?>" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a></li>
			<? endforeach; ?>
		</ul>
	</nav>
<? endif; ?>