<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Внедрение корпоративного портала для ведущего мирового поставщика страховых и финансовых услуг - компании «Альянс»");
$APPLICATION->SetPageProperty("title", "Корпоративный портал Битрикс 24 для компании \"Альянс\" | Портфолио веб-студии Первый БИТ");
$APPLICATION->SetTitle("Allianz");
$APPLICATION->SetAdditionalCSS("/clients/alyans/style.css");
$APPLICATION->AddHeadScript('/clients/alyans/js/jquery.animateNumber.min.js');
?>
<link href="/local/templates/main/assets/css/css2.css" type="text/css"  rel="stylesheet" />
<div class="new2016">
<div class="page-wrapper">
	<section class="container-fluid portfolio-detail-top" style="background: url('images/alyans-top-bg.jpg') center center / cover no-repeat;">
		<div class="container">
			<div class="logo">
				<img src="images/alyans-logo.jpg" alt="">
			</div>
			<div class="title">Внедрение корпоративного портала<br>Битрикс24 для компании «Альянс»</div>	
			<div class="panel">
				<a href="images/alyans.pdf" target="_blank" class="pdf">Скачать кейс</a>
			</div>
		</div>	
		<div class="icon--mouse"></div>
	</section>
	<section class="container-fluid portfolio-detail-row">
		<div class="container">
			<div class="section-title-bt">Заказчик</div>
			<ul class="list-dash">
				<li>В 30 филиалах компании Allianz SE работают более 3000 сотрудников;</li>
				<li>С 2007 года АО СК «Альянс» ежегодно получает «Исключительно высокий уровень надежности» в рейтинге страховых компаний «Эксперт РА».</li>
			</ul>
		</div>	
	</section>
	<section class="container-fluid portfolio-detail-row portfolio-detail-row--grey tasks">
		<div class="container">
			<div class="row two-col-wrap">
				<div class="col-md-6 two-col-wrap__item two-col-wrap__item--top">
					<div class="section-title-bt">задачи</div>
					<p class="text-large">Внедрить систему управления продажами (CRM) на базе Битрикс24;</p>
					<p class="text">Реализовать действующие регламенты в виде автоматических процедур, систематизировать мероприятия, встречи с клиентами и продажу страховок. Обеспечить прозрачность сделок и наладить отчетность по делам сотрудников, лидам и сделкам, счетам и денежным потокам.</p>
				</div>
				<div class="col-md-6 two-col-wrap__item">
					<img src="images/section-image-1.png" alt="">
				</div>
			</div>
		</div>	
	</section>
	<section class="container-fluid portfolio-detail-row portfolio-detail-row--dark">
		<div class="container">
			<div class="row two-col-wrap">
				<div class="col-md-6 two-col-wrap__item">
					<img src="images/section-image-2.png" alt="">					
				</div>
				<div class="col-md-6 two-col-wrap__item">
					<div class="section-title-bb">Стартовый экран</div>
					<div class="text">
						<p>Правую часть занимает рабочая зона. В ней отображается в виде таблицы список дел работника на 5 дней, начиная с текущей даты.</p>
						<p>Дела ранжируются: сначала просроченные, затем сегодняшние, затем более ранние.</p>
						<p>Сотрудники меняют приоритеты дел самостоятельно.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid portfolio-detail-row">
		<div class="container">
			<div class="row two-col-wrap">
				<div class="col-md-6 two-col-wrap__item">
					<div class="section-title-bb">Мероприятия</div>
					<div class="text">
						<p>Пользователи CRM организуют мероприятия для клиентов, брокеров и сотрудников, выбирая приглашенных из списка. Настраивают описания, бюджет мероприятия, отметки о посещении, сроки и даты, назначаю исполнителей.</p>
						<p>Приглашенным автоматически рассылаются уведомления, с предложением подтвердить участие. Согласие на участие фиксируется в базе CRM. Организаторы мероприятий проводят информационны рассылки по списку участников.</p>
					</div>
				</div>
				<div class="col-md-6 two-col-wrap__item">
					<img src="images/section-image-3.png" alt="">					
				</div>				
			</div>
		</div>	
	</section>
	<section class="container-fluid portfolio-detail-row portfolio-detail-row--dark dashboards" style="background: url(images/section-bg-1.jpg) center center / cover no-repeat;">
		<div class="container">
			<div class="row two-col-wrap">
				<div class="col-md-6 two-col-wrap__item">
					<img src="images/section-image-4.png" alt="">					
				</div>
				<div class="col-md-6 two-col-wrap__item">
					<div class="section-title-bb">Отчеты и Dashboards</div>
					<div class="text">
						<p>Необходимые для работы отчеты выводятся на панели управления Dashboards. В том числе отчеты: по планируемым сделкам, незавершенным договорам, премиям планируемых договоров страхования</p>
						<p>Отчеты строятся с учетом служебных доступов сотрудников: рядовому менеджеру доступны только его сделки, начальник получает сведения о клиентах и сделках подчиненных.</p>
						<p>Отчетный период задается, готовый отчет выгружается в Excel или PDF, в виде таблицы и графика.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="container-fluid portfolio-detail-row portfolio-detail-dark">
		<div class="container">
			<div class="row two-col-wrap">
				<div class="col-md-6 two-col-wrap__item">
					<div class="section-title">Поиск</div>
					<img src="images/search.jpg" alt="">
					<div class="text">
						<p>Стандартный функционал поиска «Битрикс24» модифицирован.</p>
						<p>Выдача полных и частичных совпадений структурируется по категориям: компаниям, встречам, котировкам.</p>
					</div>
				</div>
				<div class="col-md-6 two-col-wrap__item">
					<img src="images/section-image-5.png" alt="">					
				</div>				
			</div>
		</div>	
	</section>
	<section class="container-fluid portfolio-detail-row portfolio-detail-row--dark results">	
		<div class="container">	
			<div class="section-title-bt">РЕЗУЛЬТАТЫ</div>	
			<div class="row results-tizers">			
				<div class="col-md-3">
					<div class="item">
						<div class="text">Повысилась эффективность и качество обслуживания клиентов</div>
						<img src="images/icon-1.png" alt="">
					</div>
				</div>
				<div class="col-md-3">
					<div class="item">
						<div class="text">Увеличилось количество клиентов посещающих мероприятия компании</div>
						<img src="images/icon-2.png" alt="">
					</div>
				</div>
				<div class="col-md-3">
					<div class="item">
						<div class="text">Повысилась продуктивность работы менеджеров, на одного менеджера стало приходиться большее число клиентов</div>
						<img src="images/icon-3.png" alt="">
					</div>
				</div>
				<div class="col-md-3">
					<div class="item">
						<div class="text">Снижены управленческие издержки.</div>
						<img src="images/icon-4.png" alt="">
					</div>
				</div>
			</div>
				</div>	
	</section>
</div>
</div>
<script>
	// $(window).load(function() {
	// 	var run = true;
	// 	var h = ($(window).height()) * 0.85;
	// 	setTimeout(function(){
	// 		$('.portfolio-detail-top').addClass('active');
	// 	}, 300);
	// 	$(window).scroll(function() {
	// 		if ( $(this).scrollTop() > $('.counters').offset().top - h) { 
	// 			if (run) {    
	// 		    	$('.counter-1').animateNumber({ number: 37 },1500);		
	// 				$('.counter-2').animateNumber({ number: 110 },1500);
	// 				run = false;
	// 			}
	// 	  	}
	// 	  	$('*[class*="block-animate"]').each(function(index, el) {
	// 	  		if ( $(window).scrollTop() > $(el).offset().top - h) { 
	// 		  		$(el).addClass('active');
	// 		  	}
	// 	  	});		
	// 	});
	// });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>