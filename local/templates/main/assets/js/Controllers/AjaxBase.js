if (typeof define === "function" && define.amd) {
    define([], function () {

        function AjaxBase(options) {
            this.options = $.extend(true, {}, AjaxBase.defaultOptions, options);
            this.init();
        }

        AjaxBase.defaultOptions = {
            controllerUrl: 'ajax.php',
            type: 'GET',
            dataType: 'json',
            crossDomain: false,
            global: false,
            data: {}
        };

        AjaxBase.prototype.send = function (options) {
            var o = this;
            var options = $.extend(true, {}, o.options, options);
            this.prepareOptions(options);
            $.ajax({
                url: options.controllerUrl,
                type: options.type,
                data: options.data,
                global: options.global,
                dataType: options.dataType,
                crossDomain: options.crossDomain,
                beforeSend: function () {
                    this.scope = o;
                    this.options = options;
                    (options.beforeSend || o.actionBeforeSend).apply(this, arguments);
                },
                success: function () {
                    this.scope = o;
                    this.options = options;
                    (options.success || o.actionSuccess).apply(this, arguments);
                },
                error: function () {
                    this.scope = o;
                    this.options = options;
                    (options.error || o.actionError).apply(this, arguments);
                },
                complete: function () {
                    this.scope = o;
                    this.options = options;
                    (options.complete || o.actionComplete).apply(this, arguments);
                }
            });
        };
        AjaxBase.prototype.prepareOptions = function (options) {
            if (options.data instanceof Array) {
                var arData = options.data.slice();
                options.data = {};
                for (var i in arData) {
                    var arItemData = arData[i];
                    options.data[arItemData.name] = arItemData.value;
                }
            }
        };

        AjaxBase.prototype.getOwner = function () {
            return this.owner;
        }

        AjaxBase.prototype.setOwner = function (owner) {
            if (typeof owner != 'object') {
                throw new TypeError("�� ���������� ��� ���������.");
            }
            this.owner = owner;
        }


        AjaxBase.prototype.actionBeforeSend = function () {
        };
        AjaxBase.prototype.actionSuccess = function () {
        };
        AjaxBase.prototype.actionError = function () {
        };
        AjaxBase.prototype.actionComplete = function () {
        };
        AjaxBase.prototype.init = function () {
        };

        return AjaxBase;
    });
}