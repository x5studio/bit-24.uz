<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>



<div class="modal-header"><i class="fa fa-times" style="float: right;" data-dismiss="modal" aria-hidden="true"></i>
	<h4>Подготовка видеоинструкции</h4>
</div>
<div class="modal-body">
	<div class="material">
		<div class="questions">
			<h3>Для кого?</h3>
			<p>Для руководителей, который хотят формализовать работу сотрудников компании.</p>
		</div>
		<div class="questions">
			<h3>Что делаем?</h3>
			<p>Данная услуга предназначена для всех, кто хочет получить от инструкций максимальную выгоду, ведь видеоинструкция, в отличии от обычной, задействует ещё и аудиальный канал восприятия, повышая эффективность обучения Ваших сотрудников.
			</p></div>
		<div class="questions">
			<h3>Результаты</h3>
			<p>Подготовленные видеозаписи в электронном формате

</p>
		</div>
		<div class="service-bottom">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Формат:<img src="/local/templates/main/assets/img/service/com.png"
													class="img-responsive"></h3>
					<p>ВИДЕОЗАПИСЬ</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Стоимость<img src="/local/templates/main/assets/img/service/wallet.png"
														class="img-responsive"></h3>
					<h4>от 8 000 р.</h4>
				</div>
			</div>
			<div class="buy">
				<button type="button" class="btn btn-buy" data-target="#advanced"
								data-form-field-type="Консультационное внедрение: Подготовка видеоинструкции"
								data-toggle="modal" data-dismiss="modal"><a href="#" title="link">Купить</a></button>
			</div>
		</div>
	</div>
</div>