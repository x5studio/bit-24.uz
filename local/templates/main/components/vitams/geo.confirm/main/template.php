<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(!isset($arParams['TEMPLATE_CITY_DROPDOWN'])) {
	$arParams['TEMPLATE_CITY_DROPDOWN'] = 'city_simple';
}
if(!isset($arParams['CLASS_JCITY_POPUP'])) {
	$arParams['CLASS_JCITY_POPUP'] = 'j-city';
}

$fileBaseName = $_SERVER['DOCUMENT_ROOT'].$templateFolder.'/';

$regionCode = $_SESSION['REGION']['CODE'];

if(
    $arParams['CITY_VIEW_SETTINGS']
    && ($regionFile = $arParams['CITY_VIEW_SETTINGS'][$regionCode])
)
{
    $viewFileName = $fileBaseName.$regionFile;
}else{
    $viewFileName = $fileBaseName."view";
}

include $viewFileName.'.php';
?>
<?=\PB\Main\Loc::getJsMessByNameHtml(array(
        'PB_GEO_CT_GC_DEFAULT_JS_TRACK_1',
        'PB_GEO_CT_GC_DEFAULT_JS_KEYUP_1',
        'PB_GEO_CT_GC_DEFAULT_JS_CHOISE_1',
    )
)?>