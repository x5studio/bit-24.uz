if (typeof define === "function" && define.amd) {
    define([], function () {
        function Dropdown(container) {
            var _this = this;
            this.container = $(container);
            this.control = this.container.find('.j-dropdown-control').eq(0);
            this.content = this.container.find('.j-dropdown-content').eq(0);

            this.container.on('active', function (event, action) {
                switch (action) {
                    case 'show':
                        _this.container.addClass('dropdown-active');
                        _this.control.addClass('dotted-active');
                        _this.content.show();
                        break;
                    case 'hide':
                        _this.container.removeClass('dropdown-active');
                        _this.control.removeClass('dotted-active');
                        _this.content.hide();
                        break;
                    default:
                        _this.container.toggleClass('dropdown-active');
                        _this.control.toggleClass('dotted-active');
                        _this.content.toggle();
                }
                if (_this.container.hasClass('dropdown-active')) {
                    $(document).on({
                        click: _this.handlerDocumentClick,
                        keyup: _this.handlerDocumentKeyup
                    });
                }
                else {
                    _this.removeDocumentHandlers();
                }
            });

            this.handlerDocumentClick = function (e) {
                if ($(e.target).parents('.j-dropdown-content').length === 0 && !$(e.target).is(_this.content) && $(e.target).parents('.j-dropdown-control').length === 0 && !$(e.target).is(_this.control)) {
                    _this.container.trigger('active', ['hide']);
                    _this.removeDocumentHandlers();
                }
            };

            this.handlerDocumentKeyup = function (e) {
                if (e.keyCode == '27') {
                    _this.container.trigger('active', ['hide']);
                    _this.removeDocumentHandlers();
                }
            };

            this.removeDocumentHandlers = function () {
                $(document).off('click', _this.handlerDocumentClick);
                $(document).off('keyup', _this.handlerDocumentKeyup);
            };

            this.init = function () {
                this.control.click(function () {
                    _this.container.trigger('active');
                    return false;
                });

                if (this.control.hasClass('dotted-active')) {
                    _this.container.trigger('active', ['show']);
                }
                else {
                    _this.container.trigger('active', ['hide']);
                }
            };

            this.init();
        }

        return Dropdown;
    });
}