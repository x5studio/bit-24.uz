<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;

Loader::includeModule('form');

$APPLICATION->SetTitle($arResult["NAME"]);
$ACTIVE_FROM = strtotime($arResult['ACTIVE_FROM']);
$this->setFrameMode(true);
$mainId = $this->GetEditAreaId($arResult['ID']);
?>
<div class="blog-detail blog-detailm" id="<?= $mainId ?>">
    <div class="blog-detail__header"
         <? if (!empty($arResult['PROPERTY_SB_PIC_BG_VALUE'])){ ?>style="background: url('<?= $arResult['PROPERTY_SB_PIC_BG_VALUE'] ?>') no-repeat center center;    background-size: cover;"<? } ?>>
        <div class="blog-detail__shadow"></div>
        <div class="container">
            <div class="row">
                <div class="blog-detail__header-caption"><div class="h1"><?= $arResult['NAME'] ?></div></div>
                <div class="blog-detail__header-dop">
                    <div class="blog-detail__header-date"><?= $arResult['DISPLAY_ACTIVE_FROM'] ?: FormatDateFromDB($ACTIVE_FROM,
                            'DD MMMM YYYY') ?></div>
                    <? if ($arResult['PROPERTY_TIME_READ_VALUE']): ?>
                        <div class="blog-detail__header-time"><?= $arResult['PROPERTY_TIME_READ_VALUE']; ?></div>
                    <? endif; ?>
                    <div class="blog-detail__header-view"><?= $arResult['SHOW_COUNTER'] ?></div>
                    <div class="blog-detail__header-button">
                        <div class="blog-detail__header-button-insert">
                            <div class="blog-detail__header-button-text">
                                Нет времени читать?
                            </div>
                            <div class="tariffs-wrapper">
                                <a href="#" class="btn btn-default btn-buy" data-toggle="modal" data-target="#advanced" data-form-field-type="Получить консультацию">Получить консультацию</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blog-detail__body">
        <div class="container">
            <div class="row">

                <div class="blog-detail__u-a">
                    <?
                    if (!empty($arResult['PROPERTY_SB_LINK_VALUE']) || !empty($arResult['PROPERTY_SB_LINK_2_VALUE'])) {
                        $emptyLinks = false;
                        ?>
                        <div class="col-sm-7 col-md-8 col-xs-12">
                            <div class="blog_anchors-caption">Содержание</div>
                            <? if ($arResult['PROPERTY_SB_LINK_VALUE']): ?>
                                <ul class="blog_anchors">
                                    <? foreach ($arResult['PROPERTY_SB_LINK_VALUE'] as $keyLink => $SB_LINK_VALUE) : ?>
                                        <li>
                                            <a href="<?= $arResult['PROPERTY_SB_LINK_DESCRIPTION'][$keyLink] ?>"><?= $SB_LINK_VALUE ?></a>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            <? else: ?>
                                <? if ($arResult['PROPERTY_SB_LINK_2_VALUE']): ?>
                                    <? echo htmlspecialchars_decode($arResult['PROPERTY_SB_LINK_2_VALUE']['TEXT']); ?>
                                <? endif; ?>
                            <? endif; ?>
                        </div>
                        <div class="col-sm-1 col-md-1 col-xs-12"></div>
                        <div class="col-sm-4 col-md-3 col-xs-12 fix178px">
                        <?
                    }
                    else {
                        $emptyLinks = true;
                        ?>
                        <div class="col-sm-12 col-md-12 col-xs-12 ">
                        <?
                    }
                    ?>

                        <div class="blog-detail__author <?= $emptyLinks ? '_woarticles' : null ?>">
                            <? if ($arResult['PROPERTY_AUTHOR_IMG_VALUE']): ?>
                                <div class="blog-detail__author-avatar">
                                    <img src="<?= $arResult['PROPERTY_AUTHOR_IMG_VALUE'] ?>">
                                    <div class="blog-detail__author-status"></div>
                                </div>
                            <? endif; ?>
                            <div class="blog-detail__author-avatar-wrapper">
                                <div class="blog-detail__author-name"><?= $arResult['PROPERTY_AUTHOR_VALUE'] ?></div>
                                <? if ($arResult['PROPERTY_AUTHOR_ROLE_VALUE']): ?>
                                    <div class="blog-detail__author-position"><?= $arResult['PROPERTY_AUTHOR_ROLE_VALUE'] ?></div>
                                <? endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="blog-detail__body-wrapp-text">
                <?= $arResult['DETAIL_TEXT'] ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?
// Получаем ID необходимой веб-формы.
$WEB_FORM_QUESTION_ID = 0;
$FORM_SID = 'QUESTION';

$rsForm = CForm::GetBySID($FORM_SID);
if($arForm = $rsForm->fetch())
    $WEB_FORM_QUESTION_ID = intval($arForm['ID']);
?>

<?if($WEB_FORM_QUESTION_ID > 0){?>
    <? $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "question", Array(
        "WEB_FORM_ID" => $WEB_FORM_QUESTION_ID,  // ID веб-формы
        "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
        "COMPANY_IBLOCK_ID" => IB_CONTACTS,
        "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
        "EMAIL_QUESTION_CODE" => "EMAIL",
        "CITY_QUESTION_CODE" => "CITY",
        "OFFICE_QUESTION_CODE" => "OFFICE",
        "TO_QUESTION_RESIPIENTS" => "",
        "EMAIL_ONLY_FROM_RESIPIENTS" => "",
        "PRODUCT_QUESTION_CODE" => "PRODUCT",
        "TO_QUESTION_CODE" => "TO",
        "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
        "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
        "CACHE_TYPE" => "N",  // Тип кеширования
        "CACHE_TIME" => "3600",  // Время кеширования (сек.)
        "LIST_URL" => "",  // Страница со списком результатов
        "EDIT_URL" => "",  // Страница редактирования результата
        "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
        "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
        "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        ),
        "ALERT_ADD_SHARE" => "N",
        "HIDE_PRIVACY_POLICE" => "Y",
        "BTN_CALL_FORM" => ".btn_order",
        "HIDE_FIELDS" => array(
            0 => "CITY",
            1 => "OFFICE",
        ),
        "COMPONENT_MARKER" => "question",
        "SHOW_FORM_DESCRIPTION" => "N",
        "MESS" => array(
            "THANK_YOU" => "Спасибо, Ваша заявка принята.",
            "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
            "TITLE_FORM" => "Остались вопросы?",
        )
    ),
        false,
        array(
            "HIDE_ICONS" => "Y"
        )
    );
    ?>
<?}?>

<div class="blog-detail">
    <div class="blog-detail__body">
        <div class="container">
            <div class="row">
                <div class="blog-detail__tags">
                    <? foreach ($arResult["SECTIONS"] as $arSECTIONS): ?>
                        <a href="<?= $arSECTIONS['SECTION_PAGE_URL'] ?>"><?= $arSECTIONS['NAME'] ?></a>
                    <? endforeach; ?>
                </div>
                <div class="blog-detail__share">

                    <div class="blog-detail__share-caption">Поделиться:</div>
                    <div class="blog-detail__share-block">
                        <script type="text/javascript">(function () {
                                if (window.pluso)
                                    if (typeof window.pluso.start == "function")
                                        return;
                                if (window.ifpluso == undefined) {
                                    window.ifpluso = 1;
                                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                                    s.type = 'text/javascript';
                                    s.charset = 'UTF-8';
                                    s.async = true;
                                    s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                                    var h = d[g]('body')[0];
                                    h.appendChild(s);
                                }
                            })();</script>
                        <div class="pluso" data-background="transparent"
                             data-options="medium,square,line,horizontal,counter,theme=04"
                             data-services="vkontakte,facebook,odnoklassniki,moimir"></div>
                    </div>
                    <div class="blog-detail__share-block" style="display: none">

                        <a class="blog-detail__share-url" href="#"><img
                                    src="/local/templates/main/assets/img/icons/fb.svg" alt=""></a>
                        <a class="blog-detail__share-url" href="#"><img
                                    src="/local/templates/main/assets/img/icons/tv.svg" alt=""></a>
                        <a class="blog-detail__share-url" href="#"><img
                                    src="/local/templates/main/assets/img/icons/ins.svg" alt=""></a>
                        <a class="blog-detail__share-url" href="#"><img
                                    src="/local/templates/main/assets/img/icons/vk.svg" alt=""></a>
                        <a class="blog-detail__share-url" href="#"><img
                                    src="/local/templates/main/assets/img/icons/yt.svg" alt=""></a>
                    </div>
                </div>
                <div class="blog-detail__back">
                    <a class="blog-detail__back-url" href="/blog/">вернуться в список материалов</a>
                </div>
            </div>
        </div>
    </div>
</div>

<? if ($arResult['ARTICLES']): ?>
    <div class="bslider articles-slider">
        <div class="container">
            <div class="row">
                <div class="articles-slider__heading">Предлагаемые статьи</div>
                <div class="articles-slider__counter"></div>
                <div class="owl-carousel articles-slider__items">
                    <? foreach ($arResult['ARTICLES'] as $ARTICLE):
                        ?>
                        <div class="articles-slider__item">
                            <div class="articles-slider__img">
                                <a href="<?= $ARTICLE['DETAIL_PAGE_URL'] ?>"><img class="articles-slider__image"
                                                                                  src="<?= $ARTICLE['PICTURE'] ?>"></a>
                            </div>
                            <div class="articles-slider__caption">
                                <a class="articles-slider__caption-url"
                                   href="<?= $ARTICLE['DETAIL_PAGE_URL'] ?>"><?= $ARTICLE['NAME'] ?></a>
                            </div>
                        </div>
                    <? endforeach; ?>

                </div>
            </div>
        </div>
    </div>
<? endif; ?>

<script>
    $(function () {
        var images = $(".blog-detail__body-wrapp-text img");
        var link = "";
        images.each(function (indx, element) {
            link = $(element).attr("src");
            if ($(element).closest("a").length < 1) {
                $(element).wrap("<a data-fancybox href='" + link + "'></a>");
            }
        });
    });
</script>