<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<!--sections_gruzdev-->
<div class="container">
    <div class="title-page">
        <h1 class="title-page__h1"><?$APPLICATION->ShowTitle(true)?></h1>
    </div>
	<div class="row">
		<div class="col-xs-12 heading">
			<? $APPLICATION->IncludeFile($arParams["SEF_FOLDER"].".inc.title.php", Array(), Array("MODE" =>
				"html")); ?>
		</div>
		<div class="col-xs-12 info">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<div class="row">
						<? $APPLICATION->IncludeFile($arParams["SEF_FOLDER"].".inc.buy_1.php", Array(), Array("MODE" =>
							"html")); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 info_left">
					<div class="row">
						<? $APPLICATION->IncludeFile($arParams["SEF_FOLDER"].".inc.buy_2.php", Array(), Array("MODE" =>
							"html")); ?>
					</div>
				</div>
			</div>
		</div>
<!--        service.list_gruzdev-->
		<div class="col-xs-12 info">
			<?$APPLICATION->IncludeComponent(
				"service.list_gruzdev",
				"",
				Array(
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"IBLOCK_ID" => $arParams["IBLOCK_ID"]
				)
			);?>
		</div>
	</div>
</div>
<br/><br/>
