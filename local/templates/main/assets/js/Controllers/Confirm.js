if (typeof define === "function" && define.amd) {
    define(['fancybox'], function () {
        function Confirm(p) {
            var o = this;

            p.title = p.title || 'Необходимо подтвердить действие';
            p.btnOk = p.btnOk || 'Подтвердить';
            p.btnCancel = p.btnCancel || 'Отклонить';

            this.p = p;
            this.el = undefined;

            this.init();

        }

        Confirm.prototype.init = function () {
            var o = this;
            o.create();
        };
        Confirm.prototype.create = function () {
            var o = this;

            o.el = $("<div class=\"confirm\">" +
                "<div class=\"h1\">" + o.p.title + "</div>" +
                "<div class=\"control-box\">" +
                "<input class=\"confirm-ok btn btn-big btn-yellow\" type=\"button\" value=\"" + o.p.btnOk + "\" >" +
                "<input class=\"confirm-cancel btn btn-big\" type=\"button\" value=\"" + o.p.btnCancel + "\" >" +
                "</div>" +
                "</div>");

            o.confirmOk = o.el.find('.confirm-ok');
            o.confirmCancel = o.el.find('.confirm-cancel');

            o.confirmOk.on('click', function () {
                o.ok();
            });
            o.confirmCancel.on('click', function () {
                o.cancel();
            });
        };
        Confirm.prototype.resetDeferred = function () {
            var o = this;
            o.deff = $.Deferred();
            if (typeof o.p.ok == 'function') {
                o.deff.done(o.p.ok);
            }
            if (typeof o.p.cancel == 'function') {
                o.deff.fail(o.p.cancel);
            }
        };
        Confirm.prototype.show = function () {
            var o = this;

            o.resetDeferred();

            if (typeof o.el == 'undefined') {
                o.create();
            }

            $.fancybox.open(o.el, {
                padding: 0,
                wrapCSS: 'inner-close',
                onClosed: function () {
                    if (o.deff.state() == 'pending') {
                        o.cancel();
                    }
                }
            });
        };
        Confirm.prototype.ok = function () {
            var o = this;
            this.deff.resolve();
            o.hide();
        };
        Confirm.prototype.cancel = function () {
            var o = this;
            o.deff.reject();
            o.hide();
        };
        Confirm.prototype.hide = function () {
            var o = this;
            $.fancybox.close();
        };

        return Confirm;
    });
}