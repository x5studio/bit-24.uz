<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
	<ul>
		<li class="dropdown-header">Эффективный запуск Битрикс24</li>
		<? foreach ($arResult as $arItem): ?>
			<li><a href="<?= $arItem["LINK"] ?>" class="top-menu-icon <?= $arItem["PARAMS"]["CLASS"] ?>"><?=
					$arItem["TEXT"] ?></a></li>
		<? endforeach ?>
	</ul>
<? endif ?>