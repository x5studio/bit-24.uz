<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}

if ($this->StartResultCache())
{
	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFile = array();
	$filterHelper = new \PB\Main\FilterHelper();
	$rsElement = \CIBlockElement::GetList(
		array("SORT" => "ASC"),
        $filterHelper->make(
            array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ACTIVE" => "Y",
            ),
		    array(
		        'IS_CITY' => 'Y',
            )
        ),
		false,
		false,
		array("ID", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_PRICE", "PROPERTY_SUBNAME", "PROPERTY_FORMAT", "PROPERTY_TIME"));
	while ($arElement = $rsElement->GetNext())
	{
		$arFile[] = $arElement["PREVIEW_PICTURE"];
		$arResult["ITEMS"][] = $arElement;
	}

	$arResult["IMAGES"] = Helper::getImages($arFile);
	$this->IncludeComponentTemplate();
}