<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("form_title", "Заказать внедрение Битрикс24");
$APPLICATION->SetPageProperty("description", "Более 300-т внедрений Корпоративного портала Битрикс24 в крупные организации с учетом индивидуальных особенностей каждой сферы бизнеса");
$APPLICATION->SetPageProperty("keywords", "битрикс24, bitrix24, строительство, стройка, девелопер, первый бит, внедрение, недвижимость, медицина, туризм, образование, crm для недвижимости, crm для для образования, crm для туризма, crm для финеса, crm для страхования, Битрикс24 для строительства, Битрикс24 для фитнеса, Битрикс24 для туризма, Битрикс24 для образования");
$APPLICATION->SetPageProperty("title", "Разработка и внедрение Битрикс24 для любого типа организаций");
$APPLICATION->SetTitle("Разработка и внедрение Битрикс24 для любого типа организаций ");
?>
<?$elementID = $APPLICATION->IncludeComponent(
	"expertise.detail",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "3",
		"SECTION_CODE" => $_REQUEST["section_code"],
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"client.by.id",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"EXPERTISE_ID" => $elementID,
		"IBLOCK_ID" => "8"
	)
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
