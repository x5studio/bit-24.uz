var rHelper;
var rArr = new Object();

function rHelperData(link, attr) {
    if (typeof attr == 'undefined') {
        attr = 'href';
    }
    if (typeof link != 'undefined' && link.length) {
        if (link.data('active')) {
            rArr[link.attr(attr)] = {
                link: link
            }
        } else if (rArr[link.attr(attr)]) {
            delete rArr[link.attr(attr)];
        }

        var href = '';
        var i = 0;
        for (key in rArr) {
            if (rArr[key].link.data('active')) {
                var data = (key.indexOf('#') === 0 ? key.substr(1) : key);
                if (href.length > 0 && data.length > 0) {
                    href += '&';
                }
                href += data;
                i++;
            }
        }
        if (href != '') {
            href = '/' + href;
        }
        rHelper.attr(attr, href);
    }
}
function rHelperClick(link, attr) {
    if (typeof attr == 'undefined') {
        attr = 'href';
    }
    if (typeof link != 'undefined' && link.length) {
        rHelperData(link, attr);
        hash = rHelper.attr(attr);
        var scrollPosition = $(document).scrollTop();
        if(rHelper.attr(attr).length > 0) {
        	window.location.hash = rHelper.attr(attr);
        }
        $(document).scrollTop(scrollPosition);
    }
}

define(['jqueryRouter'], function () {
    $.router(function (hash) {
        var regexp = /^[\w-_]+$/;
        if (typeof rHelper == 'undefined') {
            //console.log('F5 �������� ��� ����������.');
            rHelper = $('<a>&nbsp;</a>')
                .css({position: 'absolute', top: 0, left: 0, color: 'orange', 'text-decoration': 'none', opacity: 0})
                .appendTo('body');
            if (hash.length) {

                var links = hash.substr(1).split('&'); // ������� ���� � ��������� hash �� ������ �������� ������
                links = $.unique(links); // ������� ���������
                links.reverse();

                var algorithmInitialData = function(links, linkSel, elSel, i){
                    var link = $(linkSel);
                    var el = $(elSel);
                    if (link.length) {
                        link.data({active: true});
                        rHelperData(link);
                    } else if (el.length && links[i].indexOf('#') != 0) {
                        el.data({active: true});
                        rHelperData(el, el.get(0).id == links[i] ? 'id' : 'class');
                    }
                };

                for (i = 0; i < links.length && links[i] != ''; i++) {
                    if (!regexp.test(links[i])) continue;
                    var linkSel = 'a[href="#' + links[i] + '"]';
                    var elSel = '#' + links[i] + ', .' + links[i];
                    if ($(linkSel).length > 0 || $(elSel).length > 0) {
                        algorithmInitialData(links,linkSel, elSel, i);
                    } else {
                        $(document).on('lazyLoadReceived', {links: links, linkSel: linkSel, elSel: elSel, i: i}, function (event) {
                            algorithmInitialData(event.data.links, event.data.linkSel, event.data.elSel, event.data.i);
                        });
                    }
                }
            }
        } else if (rHelper && hash != rHelper.attr('href')) {
            //console.log('<- -> ������ ������ �������� �����/������ ��� ������� ������� hash.');

            var links = hash.substr(1).split('&'); // ������� ���� � ��������� hash �� ������ �������� ������
            links = $.unique(links); // ������� ���������

            //console.log('��� 1. ���������� �� ������� � ������������ ����������� ������. � ���� ������� ������, ������� ������ ���� �����������, �� ������ �� �����������.');
            var rArrElements;
            for (key in rArr) {
                var flag = false;
                for (i = 0; i < links.length && links[i] != ''; i++) {
                    if ('#' + links[i] == key) {
                        flag = true;
                    }
                }
                if (!flag && rArr[key].link.data('active')) {
                    if (typeof rArrElements == 'undefined') {
                        rArrElements = rArr[key].link;
                    } else {
                        rArrElements.add(rArr[key].link);
                    }
                }
            }

            if (typeof rArrElements != 'undefined' && rArrElements.length) {
                rArrElements.trigger('active');
            }

            //console.log('��� 2. ���������� �� ���� ������� � ������ hash � ���������� ������ ���������');
            var algorithmActivateLink = function (links, aSel, elSel, i) {
                var a = $(aSel);
                var el = $(elSel);
                if (a.length && !a.data('active')) {
                    //a.data({active: true});
                    a.trigger('active');
                } else if (el.length && links[i].indexOf('#') != 0) {
                    //el.data({active: true});
                    el.trigger('active');
                }
            };
            for (var i = 0; i < links.length && links[i] != ''; i++) {
                var aSel = 'a[href="#' + links[i] + '"]';
                elSel = '#' + links[i] + ', .' + links[i];
                if ($(aSel).length > 0 || $(elSel).length > 0) {
                    algorithmActivateLink(links, aSel, elSel, i);
                } else {
                    $(document).on('lazyLoadReceived', {links: links, aSel: aSel, elSel: elSel, i: i}, function (event) {
                        algorithmActivateLink(event.data.links, event.data.aSel, event.data.elSel, event.data.i);
                    });
                }

            }
            rHelper.attr('href', '#' + hash)
        } else {
            //console.log('���������� ������ �������� �� ������ ��������.');
        }
    });
});