<?
namespace Bit\LocalityFormatter\Api;

use \Bitrix\Main\Engine\Controller;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Localization\Loc;

class Formatter extends Controller
{

    public function formatPhoneAction(){

        $country = '';
        $countryStrong = false;
        $formatFromNational = false;
        $request = $this->getRequest();
        $phone = $request->get('phone');

        if($request->get('country'))
            $country = $request->get('country');

        if($request->get('country_strong') == 'Y')
            $countryStrong = true;

        if($request->get('format_from_national') == 'Y')
            $formatFromNational = true;

        $result = array(
            'request_params' => array(
                'phone' => $phone,
                'country' => $country,
                'country_strong' => $countryStrong,
            )
        );

        $result['checked_countries'] = array();

        if($country)
            $result['checked_countries'][] = $country;

		$options = Option::getForModule('bit.localityformatter');

        $parsedResult = array();

        $parsedPhone = \Bit\LocalityFormatter\PhoneFormatter::parsePhone($phone, $country);

        if($formatFromNational && $parsedPhone->isValid()){

            $parsedPhone = \Bit\LocalityFormatter\PhoneFormatter::parsePhone($parsedPhone->getNationalNumber(), $country);

        }


        if(!!$options['primary_countries'] && !$parsedPhone->isValid() && !$countryStrong) {

            $countries = array();

            $countries = array_map('trim', array_filter(explode(PHP_EOL, $options['primary_countries'])));

            $countriesCount = 0;

            while(!$parsedPhone->isValid() && isset($countries[$countriesCount])){

                $parsedPhone = \Bit\LocalityFormatter\PhoneFormatter::parsePhone($phone, $countries[$countriesCount]);

                $result['checked_countries'][] = $countries[$countriesCount];

                $countriesCount ++;

            }

        }

        

        $result['original_phone'] = $phone;
        $result['phone_valid'] = $parsedPhone->isValid();
        $result['phone_country'] = $parsedPhone->getCountry();
        $result['phone_type'] = $parsedPhone->getNumberType();
        $result['phone_national'] = $parsedPhone->getNationalNumber();
        $result['phone_format'] = $parsedPhone->format(\Bitrix\Main\PhoneNumber\Format::INTERNATIONAL, true);



        return $result;

    }

    public function getCountriesAction(){

        $result = array();

        $CountryFormatter = new \Bit\LocalityFormatter\CountryFormatter;
        $countires = $CountryFormatter->all();

        if($countires){

            foreach($countires as $country){
                $result[] = array(
                    'NAME' => $country['name'],
                    'VALUE' => $country['alpha2'],
                );
            }

        }

        return ['items' => $result];

    }

}
