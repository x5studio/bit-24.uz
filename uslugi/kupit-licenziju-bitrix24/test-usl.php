<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Первый БИТ - мы гарантируем лучшие условия приобретения облачных и коробочных лицензий Битрикс24. Ознакомиться с нашим предложением ...");
$APPLICATION->SetPageProperty("keywords", "лицензия, облако, коробка, битрикс24, bitrix24, компания, команда, корпоративный портал, проект, проект+, enterprise, первый бит");
$APPLICATION->SetPageProperty("title", "Купить лицензию Битрикс24 от крупнейшего партнера - Первый БИТ");
$APPLICATION->SetTitle("Купить лицензию Битрикс24");


?><section class="buy-license">
<div class="container">
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#cloud" aria-controls="cloud" role="tab" data-toggle="tab">Облачная версия</a></li>
		<li role="presentation"><a href="#box" aria-controls="box" role="tab" data-toggle="tab">Коробочная версия</a> </li>
	</ul>
	 <!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="cloud">
							 <?$APPLICATION->IncludeComponent(
                                "buy.tariff",
                                "",
                                Array(
                                    "CACHE_TIME" => "360000",
                                    "CACHE_TYPE" => "A",
                                    "IBLOCK_ID" => "10",
									"ACTIVE_PRICE" => "PRICEFORYEAR",
									"EXCLUDE_PROPS" => array('DESC', 'ICON'),
									"SHOW_ELEMENT_NAME" => true,
									"ADDITIONAL_PRICES" => array(
										"PRICEFORMONTH" => "на 1 месяц",
										"PRICEFORTHREEMONTHS" => "на 3 месяца",
										"PRICEFORHALFAYEAR" => "на полгода",
										"PRICEFORYEAR" => "на год",
										"PRICEFORTWOYEARS" => "на 2 года",
									),
									'BUTTON_TEXT' => 'Купить'
                                )
                            );?>
			<div class="row oboznachenia">
				<div class="col-md-3">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
					нет
				</div>
				<div class="col-md-3">
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					базовый набор возможностей
				</div>
				<div class="col-md-3">
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					расширенный
				</div>
				<div class="col-md-3">
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
					профессиональный
				</div>
			</div>
			<? //$APPLICATION->IncludeFile("/uslugi/kupit-licenziju-bitrix24/.inc.info_4.php", Array(), Array("MODE" =>	"html")); ?>
			<?$APPLICATION->IncludeComponent(
				"bezr:form.result.new.befsend",
				"licence",
				Array(
					"ALERT_ADD_SHARE" => "N",
					"BTN_CALL_FORM" => ".btn_order",
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"CHAIN_ITEM_LINK" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CITY_QUESTION_CODE" => "CITY",
					"COMPANY_IBLOCK_ID" => IB_CONTACTS,
					"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
					"COMPONENT_MARKER" => "licence_form",
					"EDIT_URL" => "",
					"EMAIL_ONLY_FROM_RESIPIENTS" => "",
					"EMAIL_QUESTION_CODE" => "EMAIL",
					"HIDE_FIELDS" => array(0=>"CITY",1=>"OFFICE",),
					"HIDE_PRIVACY_POLICE" => "Y",
					"IGNORE_CUSTOM_TEMPLATE" => "N",
					"LIST_URL" => "",
					"MESS" => array("THANK_YOU"=>"Спасибо, Ваша заявка принята.","WAIT_CALL"=>"Мы свяжемся с Вами в течение 30 минут в рабочее время.","TITLE_FORM"=>"Подбор лицензии",),
					"OFFICE_QUESTION_CODE" => "OFFICE",
					"PRODUCT_QUESTION_CODE" => "PRODUCT",
					"SEF_MODE" => "N",
					"SHOW_FORM_DESCRIPTION" => "N",
					"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
					"SUCCESS_URL" => "",
					"TO_QUESTION_CODE" => "TO",
					"TO_QUESTION_RESIPIENTS" => "",
					"USE_EXTENDED_ERRORS" => "Y",
					"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
					"WEB_FORM_ID" => 2
				)
			);?>
			<div class="row">
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="cloud2">
						 <? $APPLICATION->IncludeFile("/uslugi/kupit-licenziju-bitrix24/.inc.info_1.php", Array(), Array("MODE" =>
									"html")); ?>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="cloud2">
						 <? $APPLICATION->IncludeFile("/uslugi/kupit-licenziju-bitrix24/.inc.info_6.php", Array(), Array("MODE" =>
									"html")); ?>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="cloud2">
						 <? $APPLICATION->IncludeFile("/uslugi/kupit-licenziju-bitrix24/.inc.info_5.php", Array(), Array("MODE" =>
									"html")); ?>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="cloud2">
						 <? $APPLICATION->IncludeFile("/uslugi/kupit-licenziju-bitrix24/.inc.info_2.php", Array(), Array("MODE" =>
									"html")); ?>
					</div>
				</div>
				<div class="col-md-4 col-sm-12 col-xs-12">
					<div class="cloud2">
						 <? $APPLICATION->IncludeFile("/uslugi/kupit-licenziju-bitrix24/.inc.info_3.php", Array(), Array("MODE" =>
									"html")); ?>
					</div>
				</div>
			</div>
</div>
		<div role="tabpanel" class="tab-pane clearfix" id="box">
			 <?$APPLICATION->IncludeComponent(
	"buy.tariff",
	"",
	Array(
		"CACHE_TIME" => "360000",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "11"
	)
);?> <!-- --><!-- --><!-- --><!-- --><!-- --><!-- --><!-- --><!-- --><!-- --><!-- --><!-- --><!-- -->
            <div class="row clearfix"></div>
			<?$APPLICATION->IncludeComponent(
				"bezr:form.result.new.befsend",
				"licence",
				Array(
					"ALERT_ADD_SHARE" => "N",
					"BTN_CALL_FORM" => ".btn_order",
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"CHAIN_ITEM_LINK" => "",
					"CHAIN_ITEM_TEXT" => "",
					"CITY_QUESTION_CODE" => "CITY",
					"COMPANY_IBLOCK_ID" => IB_CONTACTS,
					"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
					"COMPONENT_MARKER" => "licence_form",
					"EDIT_URL" => "",
					"EMAIL_ONLY_FROM_RESIPIENTS" => "",
					"EMAIL_QUESTION_CODE" => "EMAIL",
					"HIDE_FIELDS" => array(0=>"CITY",1=>"OFFICE",),
					"HIDE_PRIVACY_POLICE" => "Y",
					"IGNORE_CUSTOM_TEMPLATE" => "N",
					"LIST_URL" => "",
					"MESS" => array("THANK_YOU"=>"Спасибо, Ваша заявка принята.","WAIT_CALL"=>"Мы свяжемся с Вами в течение 30 минут в рабочее время.","TITLE_FORM"=>"Подбор лицензии",),
					"OFFICE_QUESTION_CODE" => "OFFICE",
					"PRODUCT_QUESTION_CODE" => "PRODUCT",
					"SEF_MODE" => "N",
					"SHOW_FORM_DESCRIPTION" => "N",
					"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
					"SUCCESS_URL" => "",
					"TO_QUESTION_CODE" => "TO",
					"TO_QUESTION_RESIPIENTS" => "",
					"USE_EXTENDED_ERRORS" => "Y",
					"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
					"WEB_FORM_ID" => 2
				)
			);?>
			<table class="table table-responsive col-md-12 hidden-sm hidden-xs" style="margin-top:20px">
			<thead>
			<tr>
				<th colspan="4" style="border-top:1px solid #eeedf2;text-align: center; font-size: 36px;">
					Все возможности
				</th>
			</tr>
			<tr>
				<th colspan="1" class="red" style="border-left:0px;">
				</th>
				<th colspan="1" style="border-top:1px solid #eeedf2;">
					CRM
				</th>
				<th colspan="1" style="border-top:1px solid #eeedf2;">
					Корпоративный портал
				</th>
				<th colspan="1" style="border-top:1px solid #eeedf2;">
					Энтерпрайз
				</th>
			</tr>
			<tr>
				<td colspan="4" class="section-header" style="font-size: 24px; text-align: center">
					Для всех редакций
				</td>
			</tr>
			</thead>
			<tbody>
			<tr>
				<th colspan="4" class="section-header">
					Коммуникации
				</th>
			</tr>
			<tr>
				<th scope="row">
					Живая лента
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Чат и видеозвонки
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Чат с поддержкой 24
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Телефония
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Network
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					HR
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Мобильное приложение
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Бизнес-инструменты
				</th>
			</tr>
			<tr>
				<th scope="row">
					Бизнес-пользователи
				</th>
				<td>
					12
				</td>
				<td>
					от 50
				</td>
				<td>
					от 1000
				</td>
			</tr>
			<tr>
				<th scope="row">
					CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Задачи и Проекты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Календари
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Диск
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Открытые линии
				</th>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
			<tr>
				<th scope="row">
					Бизнес-процессы
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Учет рабочего времени
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Отчеты по рабочему времени
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Рабочие отчеты руководителю
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Cобрания и планерки
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Внешние пользователи (экстранет)
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Управление задачами и проектами
				</th>
			</tr>
			<tr>
				<th scope="row">
					Управление задачами, проектами
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Диаграмма Ганта
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Контроль сроков
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Делегирование
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Отчеты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Учет времени по задачам
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Шаблоны задач
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Чек-листы
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Задачи из Живой ленты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Занятость сотрудников
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Зависимости между задачами в диаграмме Ганта
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Пользовательские поля
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					CRM: клиенты и продажи
				</th>
			</tr>
			<tr>
				<th scope="row">
					Face-трекер
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Чат-трекер
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Канбан
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Старт CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Отчеты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					База контактов и клиентов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Управление сделками
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Отправка коммерческих предложений
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Работа со счетами
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Счета онлайн
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Каталог товаров
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Импорт данных
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Письма клиентам
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Планирование работы
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Аналитические отчеты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с телефонией
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с интернет-магазином
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					неограниченно
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					неограниченно
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					неограниченно
				</td>
			</tr>
			<tr>
				<th scope="row">
					Воронка продаж
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Мультиворонки (направления сделок)
				</th>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
			<tr>
				<th scope="row">
					Email-трекер
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Загрузка истории писем в Email-трекере
				</th>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
			<tr>
				<th scope="row">
					CRM-формы
				</th>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
				<td>
					неограниченно
				</td>
			</tr>
			<tr>
				<th scope="row">
					Виджет на сайт: чат, обратный звонок, форма
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Роботы CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					везде
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					везде
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i><i class="fa fa-plus-circle" aria-hidden="true"></i><br>
					везде
				</td>
			</tr>
			<tr>
				<th scope="row">
					Конвертация между сделками, счетами, предложениями
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Расширенный поиск дубликатов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					История всех изменений с возможностью восстановления
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Журнал доступа сотрудников к данным в CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Просмотр больше 5 000 записей в CRM <br>
					<small>Общее число записей неограничено во всех тарифах</small>
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Обзвон по списку клиентов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Редактирование прав доступа в Открытых линиях
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Автоматизация бизнес-процессов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с 1С
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Рассылки по базе клиентов
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Продажа услуг
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					И другие полезные возможности
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Телефония
				</th>
			</tr>
			<tr>
				<th scope="row">
					Аренда виртуального номера телефона (городского и федерального 8 800)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Привязка собственного номера телефона (для исходящих звонков)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Неограниченное число входящих линий
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Маршрутизация звонков
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Перенаправление во время вызова
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Встроенная интеграция с CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Подключение своей АТС
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Подключение телефонного аппарта
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Внутренний номер для сотрудников
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Настройка по рабочему времени
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Голосовые сообщения для приветствия и автоответчика
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Запись разговора (до 100 в месяц)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Запись всех звонков без ограничений (больше 100 в месяц)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Одновременный звонок на всех свободных сотрудников в очереди
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Оценка разговора клиентом
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Распределение прав доступа к звонкам и настройкам телефонии
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Определение источника звонка по номеру телефона (для CRM)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Аналитика и отчеты по звонкам
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Голосовое меню
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					И другие полезные возможности скоро
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Планирование и учет рабочего времени
				</th>
			</tr>
			<tr>
				<th scope="row">
					Календари (общие и персональные
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Планировщик коллективных встреч и собраний
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Синхронизация календарей с MS Outlook, Google, iOS, MacOS, Android
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Рабочие отчеты руководителю
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Учет рабочего времени
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Собрания и планерки
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Резервирование переговорных
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					HR: управление персоналом
				</th>
			</tr>
			<tr>
				<th scope="row">
					Визуальное управление структурой компании
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Список сотрудников, контакты
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Синхронизация контактов с MS Outlook, Google, iOS, MacOS, Android
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Телефонный справочник
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Персональная страница сотрудника
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Права доступа сотрудников к информации
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					График отсутствий сотрудников
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Объявления с подтверждением прочтения
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Инструменты мотивации, геймификация
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Пульс компании (упрощает внедрение Битрикс24)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Обучение и тестирование сотрудников
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Новости компании
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Подписка на новости компании
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Сервис сбора идей
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Доска объявлений компании
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Внутренние заявки
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Просмотр зарплатных листков и отпусков
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Служба техподдержки сотрудников
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Автоматизация бизнес-процессов компании
				</th>
			</tr>
			<tr>
				<th scope="row">
					Управление бизнес-процессами (визуальный конструктор)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Бизнес-процессы в CRM
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Бизнес-процессы в общих документах
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Универсальные списки (для автоматизации своих рабочих процессов, например, документооборота)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Документооборот
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Командировки и отпуска
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Утверждение счетов
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Send&amp;Save. Архив почты на портале
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Многодепартаментность
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Приложения для Битрикс24
				</th>
			</tr>
			<tr>
				<th scope="row">
					Мобильное приложение для iPhone, iPad
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Мобильное приложение для Android (телефон и планшет)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Десктоп-приложение для Windows и MacOS
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Каталог веб-приложений для «облака»
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Rest API для создания приложений
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Каталог веб-приложений для «коробки»
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Открытый API и исходный код
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Интеграции
				</th>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с MS Office, MS Office Online
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с GoogleDocs
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с MS Outlook (контакты, календари)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с Google (контакты, календари)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с MacOS, iOS, Android
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Подключение внешних АТС
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Коннектор к MS Exchange Server 2007/2010
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с MS Exchange Web Mail
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Коннектор к MS SharePoint
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Интеграция с «1С:ЗУП»
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Active Directory/LDAP Интегратор + NTLM
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Настройки
				</th>
			</tr>
			<tr>
				<th scope="row">
					Аудит, логи
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Масштабирование
				</th>
			</tr>
			<tr>
				<th scope="row">
					Веб-кластер
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Виртуальная машина (полностью настроенное и протестированное серверное окружение для оптимальной работы Битрикс24)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					Безопасность и надежность
				</th>
			</tr>
			<tr>
				<th scope="row">
					SSL-сертификат
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Проактивная защита (WAF - Web Application Firewall)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Разграничение прав доступа пользователей
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Ограничение доступа по IP
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Персональный генератор одноразовых паролей (OTP)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Облачный бекап
				</th>
				<td>
					10 Гб
				</td>
				<td>
					от 20 Гб
				</td>
				<td>
					от 60 Гб
				</td>
			</tr>
			<tr>
				<th colspan="4" class="section-header">
					А также
				</th>
			</tr>
			<tr>
				<th scope="row">
					Свой домен
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Администрирование корпоративного портала
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Управление контентом (визуальный редактор страниц)
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Расширенное управление правами доступа
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Варианты дизайна
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Кастомизация дизайна
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Разработка собственной бизнес-логики
				</th>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Контроллер для интеграции с внешним сайтом
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
					Анализ и статистика посещений
				</th>
				<td>
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
				<td>
					<i class="fa fa-plus-circle" aria-hidden="true"></i>
				</td>
			</tr>
			<tr>
				<th scope="row">
				</th>
				<td>
 <button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Купить коробку : CRM" data-toggle="modal">Купить</button>
				</td>
				<td>
 <button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Купить коробку : Корпоративный портал 1" data-toggle="modal">Купить</button>
				</td>
				<td>
 <button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Купить коробку : Корпоративный портал 2" data-toggle="modal">Купить</button>
				</td>
			</tr>
			</tbody>
			</table>
		</div>
	</div>

 </section>
<?php
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
	"WEB_FORM_ID" => 13,	// ID веб-формы
	"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
	"COMPANY_IBLOCK_ID" => IB_CONTACTS,
	"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
	"EMAIL_QUESTION_CODE" => "EMAIL",
	"CITY_QUESTION_CODE" => "CITY",
	"OFFICE_QUESTION_CODE" => "OFFICE",
	"TO_QUESTION_RESIPIENTS" => "",
	"EMAIL_ONLY_FROM_RESIPIENTS" => "",
	"PRODUCT_QUESTION_CODE" => "PRODUCT",
	"TO_QUESTION_CODE" => "TO",
	"IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
	"USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
	"SEF_MODE" => "N",	// Включить поддержку ЧПУ
	"CACHE_TYPE" => "A",	// Тип кеширования
	"CACHE_TIME" => "3600",	// Время кеширования (сек.)
	"LIST_URL" => "",	// Страница со списком результатов
	"EDIT_URL" => "",	// Страница редактирования результата
	"SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
	"CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
	"CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
	"VARIABLE_ALIASES" => array(
		"WEB_FORM_ID" => "WEB_FORM_ID",
		"RESULT_ID" => "RESULT_ID",
	),
	"ALERT_ADD_SHARE" => "N",
	"HIDE_PRIVACY_POLICE" => "Y",
	"BTN_CALL_FORM" => ".btn-buy",
	"HIDE_FIELDS" => array(
		0 => "CITY",
		1 => "OFFICE",
		2 => "TYPE",
	),
	"COMPONENT_MARKER" => "feedform",
	"SHOW_FORM_DESCRIPTION" => "N",
	"MESS" => array(
		"THANK_YOU" => "Спасибо, Ваша заявка принята.",
		"WAIT_CALL" => "Мы свяжемся с Вами в течение 2-х часов.",
		"TITLE_FORM" => "Заказать услугу2",
	)
),
	false,
	array(
		"HIDE_ICONS" => "Y"
	)
);
?>
	<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
