<?

namespace Bit\AdditionalOptimizer\Image;

use Bit\AdditionalOptimizer\Image\Type\Original;
use Bit\AdditionalOptimizer\Image\Type\Optimized;
use Bit\AdditionalOptimizer\Image\Type\WebP;
use Bit\AdditionalOptimizer\Image\Type\LightVersion;



class ImageElement{


    private $useWebp = false;
    private $useOptimize = false;
    private $useLightVersion = false;

    private $imagePath;
    private $imagePathInfo = false;
    private $imageExist = false;
    private $additionalParams = array();

    public function __construct($imagePath, $useOptimize = true, $useWebp = true, $useLightVersion = true, $additionalParams = array()) {

        $this->imagePath = $imagePath;

        if(file_exists($imagePath))
        {

            $this->imageExist = true;

        }
        elseif(file_exists($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $imagePath))
        {

            $this->imagePath = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $imagePath;
            $this->imageExist = true;

        }

        $this->useWebp = (bool) $useWebp;
        $this->useOptimize = (bool) $useOptimize;
        $this->useLightVersion = (bool) $useLightVersion;
        $this->additionalParams = $additionalParams;

    }


    public function getActualImagePath() {

        if($this->getImageExist()) {

            $imageTypes = [];

            $actualPath = $this->imagePath;


            // порядок вызова важен
            if($this->useWebp) $imageTypes[] = new WebP();
            if($this->useLightVersion) $imageTypes[] = new LightVersion();


            if($imageTypes){

                try {

                    $originalCopy = new Original($actualPath);

                    // без копии оригинала ничего не делаем
                    if(!$originalCopy->getImageExist()) {
                        
                        if($originalCopy->createImage()) {

                            if($this->useOptimize) {
                                // принудительно оптимизируем
                                $forceOptimize = new Optimized($actualPath);

                                if($forceOptimize->createImage(['max_width' => 1280])) $actualPath = $forceOptimize->getImagePath();
                            }

                        }

                    }
                    
                    if($originalCopy->getImageExist()){

                        foreach ($imageTypes as $type) {
                            
                            $type->setImagePath($actualPath);

                            if(!$type->getImageExist()) {

                                if($type->createImage()) {

                                    $actualPath = $type->getImagePath();

                                }
                            
                            } else {

                                $actualPath = $type->getImagePath();

                            }


                        }

                    }

                } catch (\Exception $e) {
    
                   //print_r($e->getMessage());
    
                }

            } 
            
            return $this->getClearImagePath($actualPath);

        } else {

            // пусть браузер с этим разбирается
            return $this->getClearImagePath($this->imagePath);

        }
        

    }

    public function restoreOriginalImage() {
        
        if($this->getImageExist()) {

            $imageTypes = [];

            $this->imagePath;

            $imageTypes[] = new Optimized();
            $imageTypes[] = new WebP();
            $imageTypes[] = new LightVersion();

            if($imageTypes){

                try {

                    $originalCopy = new Original($this->imagePath);
                    
                    if($originalCopy->getImageExist()){

                        foreach ($imageTypes as $type) {
                            
                            $type->setImagePath($this->imagePath);

                            $type->removeImage();

                        }

                        if($originalCopy->resotreImage()) {

                            $originalCopy->removeImage();

                        }

                    }

                } catch (\Exception $e) {
    
                  // print_r($e->getMessage());
    
                }

            } 
            
            return $this->getClearImagePath($this->imagePath);

        } else {

            return false;

        }
        

    }

    public function getImageExist() {
        
        return $this->imageExist;

    }

    public function getImagePath() {
        
        return $this->imagePath;

    }

    public function getClearImagePath($imagePath = '') {
        
        if(!$imagePath) $imagePath = $this->imagePath;

        // remove root path
        return str_replace($_SERVER['DOCUMENT_ROOT'], '', \Bitrix\Main\IO\Path::normalize ($imagePath));

    }


    public function getImagePathInfo() {
        
        if($this->imageExist) {

            if(!$this->imagePathInfo) $this->imagePathInfo = pathinfo($this->imagePath);
        
        }

        return $this->imagePathInfo;

    }



}
