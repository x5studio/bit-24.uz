<?

namespace Bit\AdditionalOptimizer\Image;

use Symfony\Component\Process\ExecutableFinder;


class BinHelper{

    const OS_UNKNOWN = 'unknown';
    const OS_WIN = 'win';
    const OS_LINUX = 'linux';
    const OS_OSX = 'macos';

    const BIT_32 = 'x32';
    const BIT_64 = 'x64';

    const BIN_PATH = 'bin';

    /**
     * @return string
     */
    static public function getExecBinPath($extension) {

        if($extension){

            $additionalPath = __DIR__ . 
            DIRECTORY_SEPARATOR . self::BIN_PATH . 
            DIRECTORY_SEPARATOR . $extension . 
            DIRECTORY_SEPARATOR . self::getOS() . 
            DIRECTORY_SEPARATOR . self::getOsVersion();

            $finder = new ExecutableFinder;

            return $finder->find($extension, '', [$additionalPath]);

        }

        return '';

    }


    /**
     * @return string
     */
    static public function getOS() {

        switch (true) {

            case stristr(PHP_OS, 'DAR'): return self::OS_OSX;
            case stristr(PHP_OS, 'WIN'): return self::OS_WIN;
            case stristr(PHP_OS, 'LINUX'): return self::OS_LINUX;
            default : return self::OS_UNKNOWN;

        }

    }

    /**
     * @return string
     */
    static public function getOsVersion() {
        
        if(PHP_INT_SIZE === 4)  
            return self::BIT_32;
        else 
            return self::BIT_64;

    }
     
}
