if (typeof define === "function" && define.amd) {
    define([], function () {
        function SpoilerColor(container) {
            var _this = this;
            this.container = $(container);
            this.control = this.container.find('.j-spoiler-control');
            this.content = this.container.find('.j-spoiler-content').hide();
            this.close = this.container.find('.j-spoiler-close');
            this.ajaxLoad = "N";
            this.ajaxLoadSelector = "";
            this.ajaxLoadScript = "";
            if(this.container.data('ajax-load-detail') == 'Y') {
            	this.ajaxLoad = "Y";
            	this.ajaxLoadSelector = this.container.data('ajax-load-selector');
            	this.ajaxLoadScript = this.container.data('ajax-load-script');
            	if(this.ajaxLoadSelector.length == 0 || this.ajaxLoadScript.length == 0) {
            		this.ajaxLoad = "N";
            	}
            }

            this.control.bind({
                mouseenter: function () {
                    _this.control.stop().animate({opacity: 1}, {queue: false, duration: 300});
                },
                mouseleave: function () {
                    _this.control.stop().animate({opacity: 0}, {queue: false, duration: 200});
                },
                click: function () {
                	if(_this.ajaxLoad == 'Y' && _this.content.find(_this.ajaxLoadSelector).length > 0 && _this.content.find(_this.ajaxLoadSelector).html().length == 0) {
                    	//если найден элемент, куда помещать подгруженный аксом код, и если в этом элементе пусто, то делаем подгрузку аяксом.
                    	//Если в элементе уже есть текст, значит его клиент уже открывал и текст подгрузили аяксом, еще раз подгружать не надо, просто его отображаем	
                    	$.ajax({
		                    type: "GET",
		                    url: _this.ajaxLoadScript,
		                    data: "id=" + _this.control.data("id-client-widget") + "&select-prop=" + _this.control.data("select-prop"),
		                    dataType: 'html',
		                    success: function (content) {
								if(content.length > 0) {
									_this.content.find(_this.ajaxLoadSelector).html('<img class="floader" src="/bitrix/templates/main/img/loader.gif"><div class="wrapper-ajax-widjet"></div>');
									_this.content.find(_this.ajaxLoadSelector).find('.wrapper-ajax-widjet').css('display','none');
									_this.content.find(_this.ajaxLoadSelector).find('.wrapper-ajax-widjet').html(content);
								}
		                    },
		                    complete: function () {
		                    	//делаем проверку загрузились ли изображения, которые отдаются аяксом,если все изображения загрузились, то отображаем контент, подгружнный аяксом, а до этого, он в display:none
		                    	var total_images = _this.content.find("img").length;
								var images_loaded = 0;
								_this.content.find("img").each(function() {
							        var fakeSrc = $(this).attr('src');
							        $("<img/>").attr("src", fakeSrc).css('display', 'none').load(function() {
							            images_loaded++;
							            if (images_loaded >= total_images) {
							                _this.content.find("img").show();
							                _this.content.find(_this.ajaxLoadSelector).find('.wrapper-ajax-widjet').css('display','block');
							                _this.content.find(_this.ajaxLoadSelector).find('img.floader').css('display','none');
							            }
							        });
							    });
		                    	var h = _this.control.height();
			                    _this.control.height(h).stop().fadeOut(200);
			                    _this.content.toggle();
			                    _this.container.toggleClass('j-spoiler-color-opened');
			                    return false;
		                    }
		                });
                    } else {
	                	var h = _this.control.height();
	                    _this.control.height(h).stop().fadeOut(200);
	                    _this.content.toggle();
	                    _this.container.toggleClass('j-spoiler-color-opened');
	                    return false;
                    }
                }
            });
            this.close.click(function () {
                _this.control.show();
                _this.content.hide();
                _this.container.removeClass('j-spoiler-color-opened');
                return false;
            });
        }

        return SpoilerColor;
    });
}