$(function() {
    $('.faq-list__spoiler-btn').on('click', function() {
        $(this).parent('.faq-list__spoiler').toggleClass('active').children('.faq-list__spoiler-content').slideToggle();
    });
});