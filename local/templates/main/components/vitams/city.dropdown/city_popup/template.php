<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $APPLICATION;

$geo = PB\Geo\CAllGeo::getInstance();
use PB\Main\Loc;

if(empty($arParams['AJAX_FOLDER'])) {
    $arParams['AJAX_FOLDER'] = $templateFolder."/ajax.php";
}

?>
<div id="<?= $arParams["HTML_LIST_ID"] ?>" class="city-list-popup j-city-list j-city-popup-content" data-url-template="<?= $arParams["SEF_URL_TEMPLATES"]["section"] ?>" data-local-choose="<?= $arParams["LINK_LOCAL_CHOOSE"] ?>" data-link-subdomain="<?= $arParams["LINK_WITH_SUBDOMAIN"] ?>" data-region-host="<?= $_SESSION['REGION']['HOST'] ?>" data-link-baseurl="<?= $arParams["LINK_IN_BASEURL"] ?>" data-base-domains="<?= CUtil::PhpToJSObject($geo->getBaseDomains(), false, true, true) ?>" data-city-code-to-www="<?= CUtil::PhpToJSObject($geo->getParam('CITY_CODE2WWW'), false, true, true) ?>" data-domain-alias="<?= CUtil::PhpToJSObject($geo->getParam('ALIAS_DOMAIN'), false, true, true) ?>" data-sef-mode="<?= $arParams["SEF_MODE"] ?>" data-aliase-vars="<?= (!empty($arParams["VARIABLE_ALIASES"])) ? htmlspecialchars(json_encode($arParams["VARIABLE_ALIASES"]), ENT_QUOTES, 'UTF-8') : "" ?>" data-region-cur="<?= $_SESSION["REGION"]["CODE"] ?>" data-sef-folder="<?=$arParams['SEF_FOLDER']?>" data-load-city-ajax="Y" data-ajax-file="<?=$arParams['AJAX_FOLDER']?>"  data-smart-redirect="<?=$arParams['SMART_REDIRECT']?>" data-is-confirm-dropdown="<?=$arParams['IS_CONFIRM_DROPDOWN']?>" data-ui-city-prioritet="<?=implode(",",$arParams['UI_CITY_PRIORITET'])?>">
    <div class="content-popup-city">
        <div class="city-list-close j-city-close"></div>
        <div class="h2"><?=Loc::getMessage("PB_GEO_TEMPLATE_CITY_POPUP_CHOISE_REGION");?></div>
        <?
        if($arParams['AJAX_MODE'] === 'Y') {
            $APPLICATION->RestartBuffer();
        }
        if($arParams['LOAD_CITY_AJAX'] == 'Y') {
            ?>
            <?if($_SESSION['REGION']['COUNTRY_CODE'] != $arResult['FIRST_COUNTRY_CODE']) {
                ?><div id="FIRST_POSITION_COUNTRY"></div>
            <?}?>
            <div class="h3"><?=$arResult['RUSSIA_ARRAY']['NAME']?></div>
            <?if(count($arResult['CITY_PRIORITET']) > 0 || count($arResult['CITY_MSK_OBL']) >0): $arResult['COUNT_RUSSIA_COLUMN'] = $arResult['COUNT_RUSSIA_COLUMN']-1;?>
                <div class="city-list-column">
                    <?if(count($arResult['CITY_PRIORITET']) > 0){
                        foreach ($arResult['CITY_PRIORITET'] as $city){
                            ?><p><a item-id="<?= $city['ID']; ?>" href="javascript:void(0)" rel="nofollow" data-code="<?= $city['CODE'] ?>" data-country-code="<?=$arResult['RUSSIA_ARRAY']['CODE']?>" data-smart-city="<?=$city['UF_SMART_CITY']?>"><?= $city['NAME']; ?></a></p><?
                        }?>
                    <?}?>
                    <?if(count($arResult['CITY_MSK_OBL']) > 0) {
                        ?><div class="strong mo-title">
                        <?if ($arParams["SHOW_LINK_MO"] == "Y" && $arResult['MSK_OBL']):?>
                            <a item-id="<?= $arResult['MSK_OBL']['ID']; ?>" href="javascript:void(0)" rel="nofollow" data-code="<?= $arResult['MSK_OBL']['CODE'] ?>" data-country-code="<?= $arResult['RUSSIA_ARRAY']['CODE'] ?>" data-smart-city="<?=$arResult['MSK_OBL']['UF_SMART_CITY']?>"><?= $arResult['MSK_OBL']['NAME']; ?></a>
                        <?else:?>
                            <?=$arResult['MSK_OBL_NAME']?>
                        <?endif;?>
                        </div><?
                        foreach ($arResult['CITY_MSK_OBL'] as $city){
                            ?><p><a item-id="<?= $city['ID']; ?>" href="javascript:void(0)" rel="nofollow" data-code="<?= $city['CODE'] ?>" data-country-code="<?= $arResult['RUSSIA_ARRAY']['CODE'] ?>" data-smart-city="<?=$city['UF_SMART_CITY']?>"><?= $city['NAME']; ?></a></p><?
                        }
                    }?>
                </div>
            <?endif;?>
            <?
            $countRussiaCityInColumn = ceil(count($arResult['RUSSIA_ARRAY']['CITY'])/$arResult['COUNT_RUSSIA_COLUMN']);
            $x = 0;
            foreach ($arResult['RUSSIA_ARRAY']['CITY'] as $city) {
                if($x == 0 || $x%$countRussiaCityInColumn == 0) {
                    ?><div class="city-list-column"><?
                }

                ?><p><a item-id="<?= $city['ID']; ?>" href="javascript:void(0)" rel="nofollow" data-code="<?= $city['CODE'] ?>" data-country-code="<?= $arResult['RUSSIA_ARRAY']["CODE"] ?>" data-smart-city="<?=$city['UF_SMART_CITY']?>"><?= $city['NAME']; ?></a></p><?
                if(($x+1)%$countRussiaCityInColumn == 0 || $x == (count($arResult['RUSSIA_ARRAY']['CITY'])-1)) {
                    ?></div><?
                }
                $x++;
            }
            ?>
            <div class="clearfix"></div>
            <?if($_SESSION['REGION']['COUNTRY_CODE'] != $arResult['FIRST_COUNTRY_CODE']) {
                ?><div class="j-deffered"><div to="FIRST_POSITION_COUNTRY"><?
            }?>
            <?foreach ($arResult['SELECT'] as $country) {
                ?>
                <div class="city-list-column">
                    <div class="h3"><?=$country['NAME']?></div>
                    <?
                    foreach ($country['CITY'] as $city) {
if($city['ID']=="184"){
                            $country["CODE"]="kz";
                        }
                        ?><p><a item-id="<?= $city['ID']; ?>" href="javascript:void(0)" rel="nofollow" data-code="<?= $city['CODE'] ?>" data-country-code="<?=$country["CODE"] ?>" data-smart-city="<?=$city['UF_SMART_CITY']?>"><?= $city['NAME']; ?></a></p><?
                    }
                    ?>
                </div>
                <?
            }?>
            <div class="clearfix"></div>
            <?if($_SESSION['REGION']['COUNTRY_CODE'] != $arResult['FIRST_COUNTRY_CODE']) {
                ?></div></div><?
            }?>
            <?
        }
        if($arParams['AJAX_MODE'] === 'Y') {
            die();
        }
        ?>
    </div>
</div>