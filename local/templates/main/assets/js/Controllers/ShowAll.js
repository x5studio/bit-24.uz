if (typeof define === "function" && define.amd) {
    define([], function () {
        function ShowAll(container) {
            var _this = this;
            this.container = $(container);
            this.href = '.' + this.container.attr('href');
            this.addContainers = $('.j-show-all-add[href="' + this.container.attr('href') + '"]');
            this.containers = this.container.add(this.addContainers);
            this.containers.click(function () {
                $(_this.href).toggle();
                _this.containers.toggleClass('dotted-active').find('span').toggle();
                return false;
            });
        }

        return ShowAll;
    });
}