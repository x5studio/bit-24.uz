$(document).ready(function(){
    $(document).on('click', 'form[name="QUESTION"] input[type="submit"]', function() {
        var m = jQuery(this).closest('form');
        var fio = m.find('input[data-var="NAME"]').val();
        var phone = m.find('input[data-var="PHONE"]').val();
        var mail = m.find('input[data-var="EMAIL"]').val();
        var ct_site_id = '34885';
        var sub = 'Остались вопросы';
        var ct_send = false;

        if (fio != 'Имя *' && phone != 'Телефон *'){
            ct_send = true
        };

        var ct_data = {
            fio: fio,
            phoneNumber: phone,
            subject: sub,
            email: mail,
            sessionId: window.call_value
        };

        if (!!phone && !!fio && !!ct_send){
            jQuery.ajax({
                url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
                dataType: 'json', type: 'POST', data: ct_data, async: false
            });
        }
    });
});
