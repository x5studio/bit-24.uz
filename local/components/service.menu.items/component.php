<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */




if(!isset($arParams['CACHE_TIME']))
	$arParams['CACHE_TIME'] = 36000000;

$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);

$arParams['DEPTH_LEVEL'] = intval($arParams['DEPTH_LEVEL']);
if($arParams['DEPTH_LEVEL']<=0)
	$arParams['DEPTH_LEVEL']=1;

if($this->StartResultCache()) {
	CModule::IncludeModule('iblock');



	//Получим элементы
	$arSelect = Array('ID', 'NAME','DETAIL_PAGE_URL', 'IBLOCK_SECTION_ID', 'PROPERTY_CLASS_ICON');
	$arFilter = Array(
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		'ACTIVE' => 'Y',
		array(
			'LOGIC' => 'OR',
			array('SECTION_ID' => $arSectionId),
			array('SECTION_ID' => false),
		),
		array(
			'LOGIC' => 'OR',
			array('PROPERTY_CITY' => $_SESSION["REGION"]["ID"]),
			array('PROPERTY_CITY' => false),
		),
	);
	$arOrder = Array('SORT' => 'ASC');
	$res = CIBlockElement::GetList($arOrder, $arFilter, false, false, $arSelect);
	while ($ob = $res->GetNextElement()){

		$arFields = $ob->GetFields();

		$arResult[] = Array(
			$arFields['NAME'],
			$arFields['DETAIL_PAGE_URL'],
			Array(),
			Array("CLASS"=> $arFields["PROPERTY_CLASS_ICON_VALUE"]),
			""
		);
	}
	$this->EndResultCache();
}
return $arResult;
?>
