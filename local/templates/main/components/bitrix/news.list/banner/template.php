<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arItem */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult["ITEMS"])):?>
    <?$arItem = reset($arResult["ITEMS"]);?>
    <?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <section
        id="<?=$this->GetEditAreaId($arItem['ID']);?>"
        class="portal-banner"
        style="background-image: url(<?=$arItem["DETAIL_PICTURE"]["SRC"]?>);"
    >
        <div class="container clearfix">
            <div class="intro_descr">
                <h1 class="intro_title">
                    <?=$arItem["PREVIEW_TEXT"]?>
                    <br>
                    <span class="medium_font"><?=$arItem["DISPLAY_PROPERTIES"]["SPAN_CLASS_MEDIUM_FONT"]["DISPLAY_VALUE"]?></span>
                </h1>
                <p><?=$arItem["DETAIL_TEXT"]?></p>
                <a href="#" class="btn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку"
                ><?=$arItem["DISPLAY_PROPERTIES"]["BUTTON_CAPTION"]["DISPLAY_VALUE"]?></a>
            </div>
            <div class="banner-right-img">
                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="">
            </div>
        </div>
    </section>
<?endif;?>

