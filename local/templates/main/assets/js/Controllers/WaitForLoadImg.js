// ожидание загрузки изображений/фреймов etc...
// передаём картинки которых надо дождаться и калбак который надо вызвать
if (typeof define === "function" && define.amd) {
    define([], function () {
        return function($els, callback)
        {
            if($els.length)
            {
                var
                    counter = $els.length,
                    decCounter = function(){
                        if(!(--counter))
                        {
                            callback();
                        }
                    };

                $els.each(function () {
                    if(this.complete)
                    {
                        decCounter();
                    };
                });

                $els.on('load', function(){
                    decCounter();
                })
            }else{
                callback();
            }
        };
    })
}