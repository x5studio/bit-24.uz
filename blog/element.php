<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$APPLICATION->SetAdditionalCss("/blog_new/styles.css");
$APPLICATION->AddHeadScript('/local/templates/main/assets/js/owl.carousel.min.js');
$APPLICATION->SetAdditionalCSS("/local/templates/main/assets/css/owl.carousel.min.css");
$APPLICATION->SetAdditionalCSS("/local/templates/main/assets/css/owl.theme.default.min.css");
?>
<? $APPLICATION->IncludeComponent("blog.detail", "blog_main", Array(
    "CACHE_TIME" => "3600",    // Время кеширования (сек.)
    "CACHE_TYPE" => "N",    // Тип кеширования
    "ELEMENT_CODE" => $arParams["ELEMENT_CODE"],    // Символьный код элемента
    "IBLOCK_ID" => $arParams["IBLOCK_ID"],    // Инфоблок
    "SECTION_CODE" => $arParams["SECTION_CODE"],    // Символьный код раздела
    "SELECT_PROP" => [
        "ID",
        "NAME",
        "PREVIEW_TEXT",
        "DETAIL_TEXT",
        "PREVIEW_PICTURE",
        "DETAIL_PICTURE",
        "DETAIL_PAGE_URL",
        "SHOW_COUNTER",
        "ACTIVE_FROM",
        "IBLOCK_SECTION_ID",
        "PROPERTY_FEEDBACK_BTN_TITLE",
        "PROPERTY_FEEDBACK_TOP_TEXT",
        "PROPERTY_TAG",
        "PROPERTY_AUTHOR",
        "PROPERTY_AUTHOR_ROLE",
        "PROPERTY_AUTHOR_IMG",
        "PROPERTY_SIMILAR_ARTICLES",
        "PROPERTY_FORM_TITLE",
        'TAG',
        'SHOWS',
        'CODE',
        'PROPERTY_TIME_READ',
        'PROPERTY_SB_LINK',
        'PROPERTY_SB_LINK_2',
        'PROPERTY_SB_PIC_BG'
    ],
    "H1_AS_NAME" => 'N' // Устанавливать h1 как имя элемента, если есть seo
),
    false
); ?>
