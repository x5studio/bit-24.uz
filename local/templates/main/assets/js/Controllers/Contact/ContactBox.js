/**
 * Created by psbezrukov on 22.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/Contact/AContactBoxView', 'tools/tools'], function ($, AContactBoxView) {
        function ContactBox(container, options) {
            this.container = $(container);
            this.options = $.extend({}, ContactBox.defaultOptions, this.container.data(), options);

            this.contMng = null;
            this.dfViewManager = null;
        }

        ContactBox.defaultOptions = {};

        ContactBox.prototype.getContactManager = function () {
            return this.contMng;
        };

        ContactBox.prototype.setContactManager = function (contactManager) {
            this.contMng = contactManager;
        };

        ContactBox.prototype.getCityFilter = function () {
            return this.obCityFilter;
        };

        ContactBox.prototype.setCityFilter = function (obCityFilter) {
            this.obCityFilter = obCityFilter;
        };

        ContactBox.prototype.getOfficeFilter = function () {
            return this.obOfficeFilter;
        };

        ContactBox.prototype.setOfficeFilter = function (obOfficeFilter) {
            this.obOfficeFilter = obOfficeFilter;
        };

        ContactBox.prototype.getViewManager = function () {
            return this.dfViewManager;
        };

        ContactBox.prototype.setViewManager = function (dfViewManager) {
            this.dfViewManager = dfViewManager;
        };

        ContactBox.prototype.createViewManager = function (mOffices) {
            var o = this;
            var cnt = count(mOffices);
            var tplSuffix = cnt;
            if (cnt < 1 || cnt >= 10) {
                tplSuffix = 'n';
            } else if (cnt >= 4 && cnt < 10) {
                tplSuffix = '4';
            }
            var dfViewManager = $.Deferred();
            if (cnt > 0) {
                require(['Controls/Contact/ContactBoxView_' + tplSuffix], function (ContactBoxView) {
                    var obViewManager = new ContactBoxView(o.container);
                    obViewManager.setModel('offices', mOffices);
                    obViewManager.setContactManager(o.getContactManager());
                    dfViewManager.resolve(obViewManager);
                });
            } else {
                dfViewManager.resolve();
            }

            o.setViewManager(dfViewManager.promise());

            return dfViewManager.promise();
        };

        ContactBox.prototype.init = function () {
            var o = this;

            o.options['officeFilter'] = dataString2Json(o.options['officeFilter']);
            if (o.options['isRandom']) {
                o.mOffices = o.getContactManager().getOfficePowered(
                    o.getOfficeFilter(),
                    o.options['officeFilter']
                );
            } else {
                o.mOffices = o.getContactManager().getOfficeList(
                    o.getOfficeFilter(),
                    o.options['officeFilter']
                );
            }

            if (!o.getViewManager()) {
                o.createViewManager(o.mOffices);
            }
            o.getViewManager().done(function (obViewManager) {
                if (obViewManager instanceof AContactBoxView) {
                    obViewManager.render();
                }
                o.container.find('.loader').remove();
                o.container.trigger(new $.Event('contactBox.init', {}));
            });

        };
        return ContactBox;
    });
}