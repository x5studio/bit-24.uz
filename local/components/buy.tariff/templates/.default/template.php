<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="tariffs-wrapper">

    <table class="table table-responsive hidden-sm hidden-xs tariffs <? if (count($arParams["CUSTOM_LABELS"])): ?>with-custom-labels<? endif; ?>">
        <thead>
        <? if (count($arParams["CUSTOM_LABELS"]) > 1): ?>
            <tr class="custom_labels">
                <? foreach ($arParams["CUSTOM_LABELS"] as $costomLabel): ?>
                    <th><?= $costomLabel ?></th>
                <? endforeach; ?>
            </tr>
        <? endif; ?>
        <? if (count($arResult["SECTION"]) > 1): ?>
            <tr>
                <th class="red" style="border-left:0px;"><?= $arParams['ALL_TARIFFS_TITLE'] ?></th>
                <? foreach ($arResult["SECTION"] as $arSection): ?>
                    <th colspan="<?= count($arSection["ITEMS"]) ?>" style="border-top:1px solid #eeedf2;"><?= $arSection["NAME"] ?></th>
                <? endforeach; ?>
            </tr>
        <? endif; ?>
        <? if ($arParams['SHOW_ELEMENT_NAME']): ?>

            <tr>
                <? if (count($arResult["SECTION"]) <= 1): ?>
                    <th class="red" style="border-left:0px;"><?= $arParams['ALL_TARIFFS_TITLE'] ?></th>
                <? endif; ?>
                <? foreach ($arResult["SECTION"] as $arSection): ?>
                    <? foreach ($arSection["ITEMS"] as $arItem): ?>
                        <th style="border-top:1px solid #eeedf2;"><?= $arItem["NAME"] ?></th>
                    <? endforeach; ?>
                <? endforeach; ?>
            </tr>
        <? endif; ?>
        </thead>
        <tbody>
        <? foreach ($arResult["PROPERTY"] as $arProperty): ?>
            <? if ($arProperty["CODE"] == 'PRICE'): ?>
                <tr>
                    <th scope="row">
                        <?= $arProperty["NAME"] ?>
                        <? if (!empty($arParams['ADDITIONAL_PRICES'])): ?>
                            <br>
                            <select class="form-control additional_price_control">
                                <? foreach ($arParams['ADDITIONAL_PRICES'] as $key => $price): ?>
                                    <option data-class="<?= $key ?>" value="<?= $key ?>"><?= $price ?></option>
                                <? endforeach ?>
                            </select>
                        <? endif; ?>
                    </th>
                    <?
                    foreach ($arResult["SECTION"] as $arSection):
                        foreach ($arSection["ITEMS"] as $arItem):?>
                            <? $value = $arItem["PROPERTY"][$arProperty["CODE"]]["VALUE"]; ?>
                            <? if ($arItem["PROPERTY"]["PRICE_NEW"]["VALUE"] != ""):?>
                                <td class="price_col">
                                    <span class="old_cena"><?= $value ?></span>
                                    <br/>
                                    <span class="real_price"><?= $arItem["PROPERTY"]["PRICE_NEW"]["VALUE"] ?></span>
                                </td>
                            <? else:?>
                                <td class="price_col">
                                    <? if (empty($arParams['ADDITIONAL_PRICES'])):?>
                                        <span class="real_price"><?= $value ?></span>
                                    <? else:?>
                                        <span class="additional_wrapper">
										<span class="real_price"><?= $value ?></span>
									</span>
                                        <div class="prices_box">

                                            <? foreach ($arParams['ADDITIONAL_PRICES'] as $key => $price):?>
                                                <? if (!!($arItem['PROPERTY'][$key]['VALUE'])):?>

                                                    <span class="additional <?= $key ?>">
													<span class="real_price"><?= $arItem['PROPERTY'][$key]['VALUE'] ? $arItem['PROPERTY'][$key]['VALUE'] : $value ?></span>
													<? if ($arItem['PROPERTY'][$key]['VALUE'] != $value && $arItem['PROPERTY'][$key]['VALUE'] != ""):?>
                                                        <br/><span class="old_cena"><?= $value ?></span>
                                                    <?endif ?>
												</span>
                                                <?endif; ?>
                                            <?endforeach; ?>
                                        </div>
                                    <?endif; ?>
                                </td>
                            <?endif;?>
                        <?endforeach;?>
                    <?endforeach;?>
                </tr>
            <? endif; ?>

            <? if (strpos($arProperty["CODE"],'PRICE') === false && $arProperty["CODE"] != "CITY" && !in_array($arProperty["CODE"],$arParams['EXCLUDE_PROPS'])): ?>
                <tr>
                    <th scope="row"><?= $arProperty["NAME"] ?></th>
                    <?
                    foreach ($arResult["SECTION"] as $arSection):
                        foreach ($arSection["ITEMS"] as $arItem):
                            $value = $arItem["PROPERTY"][$arProperty["CODE"]]["VALUE"];
                            if ($arProperty["USER_TYPE"] == 'SASDCheckboxNum'): ?>
                                <td><i class="fa fa-<?= ($value) ? 'plus' : 'minus'; ?>-circle" aria-hidden="true"></i></td>
                            <? elseif ($arProperty["PROPERTY_TYPE"] == 'L'): ?>
                                <? if ($value >= 0 && $value <= 3): ?>
                                    <td>
                                        <? if ($value == 0): ?>
                                            <i class="fa fa-<?= ($value) ? 'plus' : 'minus'; ?>-circle" aria-hidden="true"></i>
                                        <? else: ?>
                                            <? $i = 1; ?>
                                            <? while ($i <= $value): ?>
                                                <i class="fa fa-<?= ($value) ? 'plus' : 'minus'; ?>-circle" aria-hidden="true"></i>
                                                <? $i += 1; ?>
                                            <? endwhile; ?>
                                        <? endif; ?>
                                    </td>
                                <? else: ?>
                                    <td><?= $value ?></td>
                                <? endif; ?>
                            <? else: ?>
                                <td><?= $value ?></td>
                            <? endif;?>
                        <?endforeach;?>
                    <?endforeach;?>
                </tr>
            <? endif; ?>
        <? endforeach; ?>
        <tr>
            <th scope="row"></th>
            <?foreach ($arResult["SECTION"] as $arSection):?>
                <?foreach ($arSection["ITEMS"] as $key => $arItem):?>
                    <td>
                        <? if (!isset($arParams['BUTTON_IS_LINK'][$key])):?>
                            <button type="button" class="btn btn-buy" data-target="#advanced"
                                    data-form-field-type="<?= $arParams['BUTTON_TEXT'] ?> :Тариф Б24 <?= $arItem["NAME"] ?>, <?= $arSection["NAME"]
                                    ?>" data-toggle="modal"><?= str_replace(' ', '<br>',
                                    $arParams['BUTTON_TEXT']) ?></button>
                        <? else:?>
                            <noindex>
                                <a href="<?= is_array($arParams['BUTTON_IS_LINK']) ? $arParams['BUTTON_IS_LINK'][$key] : $arParams['BUTTON_IS_LINK'] ?>"
                                   class="btn btn-buy"><?= str_replace(' ', '<br>', $arParams['BUTTON_TEXT']) ?></a>
                            </noindex>
                        <?endif; ?>
                    </td>
                <?endforeach;?>
            <?endforeach;?>
        </tr>
        </tbody>
    </table>

    <!-- Mobile version-->

    <? if (!$arParams['SHOW_ELEMENT_NAME']): ?>
        <? foreach ($arResult["SECTION"] as $key => $arSection): ?>
            <table class="table table-responsive hidden-lg hidden-md <? if (count($arParams["CUSTOM_LABELS"])): ?>with-custom-labels<? endif; ?>">
                <thead>
                <? if (isset($arParams["CUSTOM_LABELS"][$key + 1]) && !empty($arParams["CUSTOM_LABELS"][$key + 1])): ?>
                    <tr class="custom_labels">
                        <th colspan="2"><?= $arParams["CUSTOM_LABELS"][$key + 1] ?></th>
                    </tr>
                <? endif; ?>
                <tr>
                    <th colspan="2">

                        <?= $arSection["NAME"] ?>

                    </th>
                </tr>
                </thead>
                <tbody>
                <? foreach ($arResult["PROPERTY"] as $arProperty):
                    if ($arProperty["CODE"] == "PRICE_NEW" || $arProperty["CODE"] == "CITY") {
                        continue;
                    }
                    ?>
                    <tr>
                        <th scope="row"><?= $arProperty["NAME"] ?></th>
                        <td>
                            <? if ($arProperty["USER_TYPE"] == 'SASDCheckboxNum'): ?>
                                <i class="fa fa-<?= ($arSection["UF_" . $arProperty["CODE"]]) ? 'plus' : 'minus'; ?>-circle"
                                   aria-hidden="true"></i>
                            <? else: ?>
                                <?= $arSection["UF_" . $arProperty["CODE"]] ?>
                            <? endif; ?>
                        </td>
                    </tr>
                <? endforeach; ?>
                <tr>
                    <td colspan="2">
                        <? if (!isset($arParams['BUTTON_IS_LINK'][$key])): ?>
                            <button type="button" class="btn btn-buy" data-target="#advanced"
                                    data-form-field-type="<?= $arParams['BUTTON_TEXT'] ?> :Тариф Б24 <?= $arItem["NAME"] ?>, <?= $arSection["NAME"]
                                    ?>" data-toggle="modal"><?= str_replace(' ', '<br>',
                                    $arParams['BUTTON_TEXT']) ?></button>
                        <? else: ?>
                            <a href="<?= is_array($arParams['BUTTON_IS_LINK']) ? $arParams['BUTTON_IS_LINK'][$key] : $arParams['BUTTON_IS_LINK'] ?>"
                               class="btn btn-buy"><?= str_replace(' ', '<br>', $arParams['BUTTON_TEXT']) ?></a>
                        <? endif; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        <? endforeach; ?>
    <? else: ?>
        <? foreach ($arResult["SECTION"] as $arSection): ?>
            <? foreach ($arSection["ITEMS"] as $key => $arItem): ?>
                <table class="table table-responsive hidden-lg hidden-md <? if (count($arParams["CUSTOM_LABELS"])): ?>with-custom-labels<? endif; ?>">
                    <thead>
                    <? if (isset($arParams["CUSTOM_LABELS"][$key + 1]) && !empty($arParams["CUSTOM_LABELS"][$key + 1])): ?>
                        <tr class="custom_labels">
                            <th colspan="2"><?= $arParams["CUSTOM_LABELS"][$key + 1] ?></th>
                        </tr>
                    <? endif; ?>
                    <tr>
                        <th><?= $arItem["NAME"] ?></th>
                        <th>
                            <? foreach ($arResult["PROPERTY"] as $arProperty): ?>
                                <? if ($arProperty["CODE"] == 'PRICE'): ?>
                                    <? if (!empty($arParams['ADDITIONAL_PRICES'])): ?>
                                        <select class="form-control additional_price_control">
                                            <? foreach ($arParams['ADDITIONAL_PRICES'] as $key => $price): ?>
                                                <option data-class="<?= $key ?>"
                                                        value="<?= $key ?>"><?= $price ?></option>
                                            <? endforeach ?>
                                        </select>

                                    <? endif; ?>
                                <? endif; ?>
                            <? endforeach ?>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($arResult["PROPERTY"] as $arProperty): ?>
                        <? if ($arProperty["CODE"] == 'PRICE'): ?>
                            <tr>
                                <th scope="row">
                                    <span class="custom_labels-header"><?= $arProperty["NAME"] ?></span>
                                </th>
                                <? $value = $arItem["PROPERTY"][$arProperty["CODE"]]["VALUE"]; ?>
                                <? if ($arItem["PROPERTY"]["PRICE_NEW"]["VALUE"] != ""): ?>
                                    <td class="price_col">
                                        <span class="old_cena"><?= $value ?></span><br/>
                                        <span class="real_price"><?= $arItem["PROPERTY"]["PRICE_NEW"]["VALUE"] ?></span>
                                    </td>
                                <? else: ?>
                                    <td class="price_col">
                                        <? if (empty($arParams['ADDITIONAL_PRICES'])): ?>
                                            <span class="real_price"><?= $value ?></span>
                                        <? else: ?>
                                            <span class="additional_wrapper">
												<span class="real_price"><?= $value ?></span>
											</span>
                                            <div class="prices_box">
                                                <? foreach ($arParams['ADDITIONAL_PRICES'] as $key => $price): ?>
                                                    <? if (!!($arItem['PROPERTY'][$key]['VALUE'])): ?>
                                                        <span class="additional <?= $key ?>">
															<span class="real_price"><?= $arItem['PROPERTY'][$key]['VALUE'] ? $arItem['PROPERTY'][$key]['VALUE'] : $value ?></span>
															<? if ($arItem['PROPERTY'][$key]['VALUE'] != $value && $arItem['PROPERTY'][$key]['VALUE'] != ""): ?>
                                                                <br/><span class="old_cena"><?= $value ?></span>
                                                            <? endif ?>
														</span>
                                                    <? endif; ?>
                                                <? endforeach ?>
                                            </div>
                                        <? endif; ?>
                                    </td>
                                <? endif; ?>

                            </tr>
                        <? endif; ?>
                        <? if (strpos($arProperty["CODE"],'PRICE') === false && $arProperty["CODE"] != "CITY" && !in_array($arProperty["CODE"],$arParams['EXCLUDE_PROPS'])): ?>
                            <tr class="<?=in_array($arProperty["CODE"],$arParams['SHOW_MOBILE_PROP'])?'':'table__hidengs'?>">
                                <th scope="row"><?= $arProperty["NAME"] ?></th>
                                <?$value = $arItem["PROPERTY"][$arProperty["CODE"]]["VALUE"];
                                if ($arProperty["USER_TYPE"] == 'SASDCheckboxNum'): ?>
                                    <td><i class="fa fa-<?= ($value) ? 'plus' : 'minus'; ?>-circle" aria-hidden="true"></i></td>
                                <? elseif ($arProperty["PROPERTY_TYPE"] == 'L'): ?>
                                    <? if ($value >= 0 && $value <= 3): ?>
                                        <td>
                                            <? if ($value == 0): ?>
                                                <i class="fa fa-<?= ($value) ? 'plus' : 'minus'; ?>-circle" aria-hidden="true"></i>
                                            <? else: ?>
                                                <? $i = 1; ?>
                                                <? while ($i <= $value): ?>
                                                    <i class="fa fa-<?= ($value) ? 'plus' : 'minus'; ?>-circle" aria-hidden="true"></i>
                                                    <? $i += 1; ?>
                                                <? endwhile; ?>
                                            <? endif; ?>
                                        </td>
                                    <? else: ?>
                                        <td><?= $value ?></td>
                                    <? endif; ?>
                                <? else: ?>
                                    <td><?= $value ?></td>
                                <? endif; ?>
                            </tr>
                        <? endif; ?>
                    <? endforeach; ?>
                    <tr><td colspan="2"><a href="#" class="table__hidengs-toggle">Показать все характеристики</a></td></tr>
                    <tr>
                        <td colspan="2">
                            <? if (!isset($arParams['BUTTON_IS_LINK'][$key])): ?>
                                <button type="button" class="btn btn-buy" data-target="#advanced"
                                        data-form-field-type="<?= $arParams['BUTTON_TEXT'] ?> :Тариф Б24 <?= $arItem["NAME"] ?>, <?= $arSection["NAME"]
                                        ?>" data-toggle="modal"><?= str_replace(' ', '<br>',
                                        $arParams['BUTTON_TEXT']) ?></button>
                            <? else: ?>
                                <a href="<?= is_array($arParams['BUTTON_IS_LINK']) ? $arParams['BUTTON_IS_LINK'][$key] : $arParams['BUTTON_IS_LINK'] ?>"
                                   class="btn btn-buy"><?= str_replace(' ', '<br>', $arParams['BUTTON_TEXT']) ?></a>
                            <? endif; ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            <? endforeach; ?>
        <? endforeach; ?>
    <? endif; ?>
    <? if (!empty($arParams['ADDITIONAL_PRICES'])): ?>
        <script>
            $(function () {
                $(document).on('change', 'select.additional_price_control', function () {
                    let month_type = $(this).val();

                    let prices_box = $(".prices_box");
                    for (let i = 0; i < prices_box.length; i++) {

                        let set_value = $(prices_box[i]).find("span." + month_type).html();

                        $(prices_box[i]).siblings('.additional_wrapper').html(set_value);

                        $('select.additional_price_control').not($(this)).val(month_type)

                    }
                });

                $('select.additional_price_control option[data-class="<?=$arParams['ACTIVE_PRICE']?>"]').prop('selected', true);
                $('select.additional_price_control').change();
            });
        </script>
    <? endif; ?>
</div>