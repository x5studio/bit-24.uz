<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок",
			"TYPE" => "STRING",
			"DEFAULT" => 9,
		),
		"SECTION_CODE" => Array(
			"PARENT" => "BASE",
			"NAME" => "Символьный код раздела",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"AJAX_MODE" => array(),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600)
	),
);
?>
