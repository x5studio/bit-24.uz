function ouibounce(el, config) {
	var config = config || {},
		aggressive = config.aggressive || false,
		sensitivity = setDefault(config.sensitivity, 20),
		timer = setDefault(config.timer, 1000),
		timerUltimate = setDefault(config.timerUltimate, false),
		onBeforeFire = config.onBeforeFire || function () {
				return false;
			},
		cookieExpire = setDefaultCookieExpire(config.cookieExpire) || '',
		cookieDomain = config.cookieDomain ? ';domain=' + config.cookieDomain : '',
		sitewide = config.sitewide === true ? ';path=/' : '',
		_html = document.getElementsByTagName('html')[0],
		timerOut = setDefault(config.timerOut, 10);

	var o = this;
	var root = $(el);
	var modal = $('.modal', root);
	var disableKeydown = false;

	function setDefault(_property, _default) {
		return typeof _property === 'undefined' ? _default : _property;
	}

	function setDefaultCookieExpire(days) {
		// transform days to milliseconds
		var ms = days * 24 * 60 * 60 * 1000;

		var date = new Date();
		date.setTime(date.getTime() + ms);

		return "; expires=" + date.toGMTString();
	}


	function attachOuiBounce() {
		var $_html = $(_html);
		$_html.on('mouseleave', handleMouseleave);
		$_html.on('mouseenter', handleMouseenter);
		$_html.on('keydown', handleKeydown);
	}

	function handleMouseleave(e) {
		if (e.clientY > sensitivity || (checkCookieValue('viewedOuibounceModal', 'true') && !aggressive))return;
		if(timerUltimate) {
			timeoutID = setTimeout(function () {
				fire();
			}, timerOut);
		}
	}

	function handleMouseenter(e)
	{
		if (typeof timeoutID != 'undefined') clearTimeout(timeoutID);
	}


	function handleKeydown(e) {
		if (disableKeydown || checkCookieValue('viewedOuibounceModal', 'true') && !aggressive) return;
		else if (!e.metaKey || e.keyCode != 76) return;

		disableKeydown = true;
		if(timerUltimate) {
			fire();
		}
		//track(['�������� ������ (Popup)', 'Popup', '������� ����', 1]);
	}

	function checkCookieValue(cookieName, value) {
		// cookies are separated by '; '
		var cookies = document.cookie.split('; ').reduce(function (prev, curr) {
			// split by '=' to get key, value pairs
			var el = curr.split('=');

			// add the cookie to fn object
			prev[el[0]] = el[1];

			return prev;
		}, {});

		return cookies[cookieName] === value;
	}

	function fire() {
		// You can use ouibounce without passing an element
		// https://github.com/carlsednaoui/ouibounce/issues/30
		var res = onBeforeFire();

		if(res == false){
			if (el) el.style.display = 'block';
			root.trigger('callback');
			disable();
		}
		else{
			disable();
			disableKeydown = false;
			init();
		}

	}

	function disable(options) {
		var options = options || {};
		var $_html = $(_html);

		if (typeof timerUltimateID !== 'undefined') {
			clearTimeout(timerUltimateID);
		}

		// you can pass a specific cookie expiration when using the OuiBounce API
		// ex: _ouiBounce.disable({ cookieExpire: 5 });
		if (typeof options.cookieExpire !== 'undefined') {
			cookieExpire = setDefaultCookieExpire(options.cookieExpire);
		}

		// you can pass use sitewide cookies too
		// ex: _ouiBounce.disable({ cookieExpire: 5, sitewide: true });
		if (options.sitewide === true) {
			sitewide = ';path=/';
		}

		// you can pass a domain string when the cookie should be read subdomain-wise
		// ex: _ouiBounce.disable({ cookieDomain: '.example.com' });
		if (typeof options.cookieDomain !== 'undefined') {
			cookieDomain = ';domain=' + options.cookieDomain;
		}

		document.cookie = 'viewedOuibounceModal=true' + cookieExpire + cookieDomain + sitewide;

		// remove listeners
		$_html.off('mouseleave', handleMouseleave);
		$_html.off('mouseenter', handleMouseenter);
		$_html.off('keydown', handleKeydown);
	}

	function initPickerTimeOnSite()
	{
		var startDate = new Date();
		$(window).unload(function(){
			var endDate = new Date();
			var diff = Date.parse(endDate) - Date.parse(startDate);
			diff = parseInt(diff);

			var popupSID = getCookie('callbackPopupSID');
			if(popupSID == BX.bitrix_sessid())
			{
				var prevDiff = getCookie('callbackPopupPickerDiff') || 0;
				diff += parseInt(prevDiff);
			}
			else
			{
				setCookie('callbackPopupSID', BX.bitrix_sessid(), false, '/', cookieDomain);
			}
			setCookie('callbackPopupPickerDiff', diff, false, '/', cookieDomain);
		});
	}

	function setModalMargin()
	{
		var rDisplay = root.css('display');
		var rVisibility = root.css('visibility');
		root.css({
			display: 'block',
			visibility: 'hidden'
		});
		var modalHeight = modal.height();
		root.css({
			display: rDisplay,
			visibility: rVisibility
		});
		modal.css({
			marginTop: '-' + (modalHeight/2) + 'px'
		});
	}

	function init()
	{
		setTimeout(attachOuiBounce, timer);

		if(timerUltimate)
		{
			timerUltimateID = setTimeout(function(){
				if (disableKeydown || checkCookieValue('viewedOuibounceModal', 'true') && !aggressive) return;
				disableKeydown = true;
				fire();
				//track(['�������� ������ (Popup)', 'Popup', '����� �������', 1]);
			}, timerUltimate);
		}

		initPickerTimeOnSite();

		root.on('modal.refresh', function(){
			setModalMargin();
		});
		setModalMargin();
	}

	function on(eventName, callback)
	{
		root.on(eventName, callback)
	}

	init();

	return {
		fire: fire,
		disable: disable,
		on: on
	};
}
