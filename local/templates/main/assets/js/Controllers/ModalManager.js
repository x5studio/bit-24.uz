if (typeof define === "function" && define.amd) {
    define([], function () {

        function ModalManager() {
            this.root = $('body');
            this.init();
        }

        ModalManager.prototype.hasOpen = function () {
            var o = this;
            var modalOpenVal = JSON.parse(o.root.attr('data-modal-open'));

            return modalOpenVal;
        }

        ModalManager.prototype.open = function () {
            var o = this;
            o.root.attr('data-modal-open', true);
        }

        ModalManager.prototype.close = function()
        {
            var o = this;
            o.root.attr('data-modal-open', false);
        }

        ModalManager.prototype.init = function () {
            var o = this;
            if(typeof o.root.data('modalOpen') == "undefined"){
                o.root.attr('data-modal-open', false);
            }
        }

        var obModalManager = new ModalManager();
        return obModalManager;
    });
}
