<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use \Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["ELEMENT_CODE"] = trim($arParams["ELEMENT_CODE"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
if (empty($arParams["ELEMENT_CODE"]))
{
	$this->AbortResultCache();
	if (Loader::includeModule('iblock'))
	{
		\Bitrix\Iblock\Component\Tools::process404(
			""
			, true
			, true
			, true
			, ""
		);
	}
	return;
}

if ($this->StartResultCache())
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

    $filterHelper = new \PB\Main\FilterHelper();
	$rsElement = CIBlockElement::GetList(
		array(
		    'PROPERTY_CITY' => 'DESC',
            'SORT' => 'ASC'
        ),
        $filterHelper->make(
            array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ACTIVE" => "Y",
                "CODE" => $arParams["ELEMENT_CODE"],
                "SECTION_CODE" => $arParams["SECTION_CODE"],
                "SECTION_GLOBAL_ACTIVE" => "Y"
            ),
            array(
                'IS_CITY' => 'Y',
            )
        ),
		false,
		array("nTopCount" => 1),
		array("ID", "NAME", "DETAIL_TEXT", "PREVIEW_PICTURE", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_FORMAT", "PROPERTY_PRICE", "PROPERTY_COUNT", "IBLOCK_SECTION_ID")
	);

	if ($arResult = $rsElement->GetNext())
	{

		$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arParams["IBLOCK_ID"], $arResult["ID"]);
		$arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();

		$arFile = array();
		$arFile[] = $arResult["PREVIEW_PICTURE"];
		$arFile[] = $arResult["DETAIL_PICTURE"];

		$rsNavi = \CIBlockSection::GetNavChain(
			$arParams["IBLOCK_ID"],
			$arResult["IBLOCK_SECTION_ID"],
			array("ID", "NAME", "SECTION_PAGE_URL")
		);
		while ($arNavi = $rsNavi->GetNext())
			$arResult["NAVI"][] = $arNavi;

		$arResult["IMAGES"] = Helper::getImages($arFile);

	} else
	{
		$this->AbortResultCache();
		if (Loader::includeModule('iblock'))
		{
			\Bitrix\Iblock\Component\Tools::process404(
				""
				, true
				, true
				, true
				, ""
			);
		}
		return;
	}

	$this->SetResultCacheKeys(array("ID", "NAME", "IPROPERTY_VALUES", "NAVI", "DETAIL_PAGE_URL"));
	$this->IncludeComponentTemplate();
}
if (isset($arResult["ID"]))
{

	$arTitleOptions = null;

	if (CModule::IncludeModule("iblock"))
		CIBlockElement::CounterInc($arResult["ID"]);

	if ($arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"] != "")
		$APPLICATION->SetPageProperty("title", $arResult["IPROPERTY_VALUES"]["ELEMENT_META_TITLE"], $arTitleOptions);
	else
		$APPLICATION->SetPageProperty("title", $arResult["NAME"], $arTitleOptions);

	if ($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != "")
		$APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"], $arTitleOptions);
	else
		$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);

	if ($arResult["IPROPERTY_VALUES"]["ELEMENT_META_KEYWORDS"] != "")
		$APPLICATION->SetPageProperty("keywords", $arResult["IPROPERTY_VALUES"]["ELEMENT_META_KEYWORDS"], $arTitleOptions);
	if ($arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"] != "")
		$APPLICATION->SetPageProperty("description", $arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"], $arTitleOptions);

	foreach ($arResult["NAVI"] as $arNavi)
		$APPLICATION->AddChainItem($arNavi["NAME"], $arNavi["SECTION_PAGE_URL"]);
	$APPLICATION->AddChainItem($arResult["NAME"], $arResult["DETAIL_PAGE_URL"]);
}