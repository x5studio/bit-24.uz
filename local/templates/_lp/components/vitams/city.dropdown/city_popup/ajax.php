<?
if(isset($_REQUEST['smart_redirect']) && $_REQUEST['smart_redirect'] == 'Y') {
	define('SMART_REDIRECT','Y');
	define('REDIRECT','N');
}

if(isset($_REQUEST['action']) && $_REQUEST['action']=='city-load') {
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	$UI_CITY_PRIORITET = explode(",",$_REQUEST['ui_prioritet']);
	
	$APPLICATION->IncludeComponent("vitams:city.dropdown", "city_popup",
        Array(
            "IBLOCK_TYPE" => IBT_CONTACTS,
            "IBLOCK_ID" => IB_CONTACTS,
            "DROPDOWN" => $_REQUEST['dropdown'], // for cache only
            "SECTION_CODE" => "",
            "SECTION_URL" => "",
            "COUNT_ELEMENTS" => "Y",
            "TOP_DEPTH" => "1",
            "SECTION_FIELDS" => "",
            "FILTER_NAME" => "cityFilter",
            "SECTION_USER_FIELDS" => "",
            "ADD_SECTIONS_CHAIN" => "Y",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_NOTES" => "",
            "CACHE_GROUPS" => "N",
            "CACHE_FILTER" => "Y",
            "LINK_IN_BASEURL" => "Y",
            "LINK_WITH_SUBDOMAIN" => "Y",
            "SEF_MODE" => "N",
            "SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
            "SEF_URL_TEMPLATES" => array(),
            "HTML_LIST_ID" => "header-city-list",
            "HTML_TITLE" => $HTML_TITLE,
            "SHOW_MOSCOW" => $_SESSION['SHOW_MOSCOW'],
            "COUNTRY_CODE" => $_SESSION['GEOIP']['country_code'],
            "PAGER_TEMPLATE" => "",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            'UI_CITY_STRONG' => Array(),
            'UI_CITY_PRIORITET' => $UI_CITY_PRIORITET,
            "AJAX_MODE"=>"Y"
        ), false,
        Array("HIDE_ICONS" => "Y")
    );
}
?>