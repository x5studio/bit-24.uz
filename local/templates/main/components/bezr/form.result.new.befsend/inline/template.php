<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$PARENT = $this->__component->__parent; ?>

<form id="general2page-form-<?=$arResult["SOULT_FORM"]?>"
      class="j-form-page j-form_inline-errors fbox"
      data-salt-form="<?= $arResult["SOULT_FORM"] ?>"
      <?= !empty($arParams['KEEP_ERRORS']) && $arParams['KEEP_ERRORS'] === 'Y' ? ' data-keep-errors="1"' : ''; ?>
      name="<?= $arResult["arForm"]["SID"] ?>" action="<?= POST_FORM_ACTION_URI ?>"
      method="POST" enctype="multipart/form-data">
    <input type="hidden" name="sessid" value=""/>
    <input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>"/>

    <? $arHiddenID = Array(); ?>
    <? $k = 0;
    foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):$k++; ?>
        <?
        // TEXT EMAIL ...
        if (in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], array('text', 'email'))):?>
            <div
                    class="text form-group has-feedback<? if (strlen($arResult["FORM_ERRORS"][$FIELD_SID]) > 0): ?> has-error<?endif;
                    ?> ">
                <input
                        name="form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
                        type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE" ? "tel" : $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"<? if (!empty($arQuestion['STRUCTURE'][0]['FIELD_PARAM']))
                { ?>
                    data-type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] ?>"<?
                } ?>
                        value="<?= $arResult['arrVALUES'][$FIELD_SID] ?: null ?>"
                        placeholder="<?= $arQuestion["CAPTION"] ?>"
                        class="form-control "
                />
                <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            </div>
            <?
        // DROPDOWN
        elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'dropdown')://__($arQuestion)
            ?>
            <div class="text"><?= $arQuestion["HTML_CODE"] ?></div>
            <?
        // TEXTAREA
        elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'textarea'):?>
            <div class="text"><?= $arQuestion["HTML_CODE"] ?></div>
            <?
        // FILE
        elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'file'):?>
            <label><?= $arQuestion["CAPTION"] ?></label>
            <? if ($arQuestion["REQUIRED"] == "Y"): ?><?= $arResult["REQUIRED_SIGN"]; ?><? endif; ?>
            <div class="text" style="height: 30px; line-height: 30px"><?= $arQuestion["HTML_CODE"] ?></div>
        <? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'):$k--; ?>
            <? array_push($arHiddenID, $FIELD_SID); ?>
        <? endif; ?>
    <? endforeach; ?>

    <? // SUBMIT ?>
    <div class="form-group has-feedback"><input type="submit" onclick="yaCounter45927957.reachGoal('expert-send'); gtag ('send', 'event', { 'event_category': 'expert-send', 'send'}); return true;" class="btn btn-blocks top-button" name="web_form_submit"
                                                value="<?= $arParams['MESS']['SUBMIT_BTN'] ?: (strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"></div>
    <?php if ($arParams['HIDE_PRIVACY_POLICE'] != 'Y'): ?>
        <div class="policy-text">Нажимая кнопку, я принимаю условия <a href="https://bit-24.ru/oferta/" target="_blank">Оферты</a> по использованию сайта и согласен с <a href="https://bit-24.ru/politika-konfidencialnosti/" target="_blank">Политикой конфиденциальности</a></div>
    <?php endif; ?>
    <?
    // HIDDENS
    if (!empty($arHiddenID))
    {
        $k--;
        foreach ($arHiddenID AS $hiddenID)
        {
            echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
        }
    } ?>
    <? if (!empty($arParams['COMPONENT_MARKER'])): ?>
        <input type="hidden" name="COMPONENT_MARKER" value="<?= $arParams['COMPONENT_MARKER'] ?>">
    <? endif; ?>
</form>