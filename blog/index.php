<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Расскажем, что нового в Битрикс24 и как этим пользоваться");
$APPLICATION->SetPageProperty("title", "Статьи по внедрению CRM Битрикс24");
$APPLICATION->SetTitle("Блог");
?>


<?if ($APPLICATION->GetCurPage() == '/blog/'):?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "blogs",
        array(
            "ADD_SECTIONS_CHAIN" => "Y",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "COUNT_ELEMENTS" => "Y",
            "FILTER_NAME" => "sectionsFilter",
            "IBLOCK_ID" => "2",
            "IBLOCK_TYPE" => "blog",
            "SECTION_CODE" => "",
            "SECTION_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SECTION_ID" => "",
            "SECTION_URL" => "",
            "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
            ),
            "SHOW_PARENT_NAME" => "Y",
            "TOP_DEPTH" => "1",
            "VIEW_MODE" => "LINE",
            "COMPONENT_TEMPLATE" => "blogs"
        ),
        false
    );?>
<?else:?>
    <?$APPLICATION->IncludeComponent(
        "blog",
        "",
        array(
            "IBLOCK_ID" => "2",
            "SEF_FOLDER" => "/blog/",
            "SEF_MODE" => "Y",
            "SEF_URL_TEMPLATES" => array(
                "sections" => "",
                "section" => "#SECTION_CODE_PATH#/",
                "element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
            )
        ),
        false
    );?>
<?endif;?>

    <!-- Marquiz script start -->
    <script src="https://script.marquiz.ru/v1.js" type="application/javascript"></script>
    <!-- Marquiz script end -->
    <div style="visibility: hidden;" class="marquiz-pops marquiz-pops_position_bottom-left marquiz-pops_blicked marquiz-pops_shadowed marquiz-pops_position" ><a class="marquiz-pops__body" href="#popup:marquiz_5de6172e4734030045bdb536" data-marquiz-pop-text-color="#ffffff" data-marquiz-pop-background-color="#d34085" data-marquiz-pop-svg-color="#fff" data-marquiz-pop-close-color="#fff" data-marquiz-pop-color-pulse="rgba(211, 64, 133, 0.4)" data-marquiz-pop-color-pulse-alpha="#d34085" data-marquiz-pop-delay="2s" data-marquiz-pop="true"><span class="marquiz-pops__icon"></span><span class="marquiz-pops__content"><span class="marquiz-pops__content-title">Пройти тест</span><span class="marquiz-pops__content-text">&laquo;Рассчитать стоимость внедрения&raquo;</span></span></a></div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>