<?php

namespace Sprint\Migration;


class Version20200910131138 extends Version
{
    protected $description = "Свойство для блога Загрузка картинки с подложкой";

    protected $moduleVersion = "3.16.3";

    /**
     * @return bool|void
     * @throws Exceptions\HelperException
     */
    public function up()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('BLOG', 'blog');
        $helper->Iblock()->saveProperty($iblockId, array(
            'NAME' => 'Картинка для фона на слайдер',
            'ACTIVE' => 'Y',
            'SORT' => '1000',
            'CODE' => 'SB_PIC_BG',
            'DEFAULT_VALUE' => '',
            'PROPERTY_TYPE' => 'F',
            'ROW_COUNT' => '1',
            'COL_COUNT' => '30',
            'LIST_TYPE' => 'L',
            'MULTIPLE' => 'N',
            'XML_ID' => null,
            'FILE_TYPE' => '',
            'MULTIPLE_CNT' => '5',
            'LINK_IBLOCK_ID' => '0',
            'WITH_DESCRIPTION' => 'N',
            'SEARCHABLE' => 'N',
            'FILTRABLE' => 'N',
            'IS_REQUIRED' => 'N',
            'VERSION' => '2',
            'USER_TYPE' => null,
            'USER_TYPE_SETTINGS' => null,
            'HINT' => 'Размер банера 1280x420',
        ));

    }

    public function down()
    {
        $helper = $this->getHelperManager();
        $iblockId = $helper->Iblock()->getIblockIdIfExists('BLOG', 'blog');
        $helper->Iblock()->deletePropertyIfExists($iblockId, 'SB_PIC_BG');
    }
}
