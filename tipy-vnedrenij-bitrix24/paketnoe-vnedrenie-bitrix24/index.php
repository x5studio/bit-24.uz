<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("form_title", "Заказать пакет внедрения");
$APPLICATION->SetPageProperty("description", "Пакетное внедрение Битрикс24 в работу Вашей компании. Сроки, стоимость и результат услуги по пакетному внедрению.");
$APPLICATION->SetPageProperty("keywords", "пакетное внедрение битрикс24");
$APPLICATION->SetPageProperty("title", "Пакетное внедрение Битрикс24, стоимость внедрения CRM Битрикс24");
$APPLICATION->SetTitle("Пакетное внедрение Битрикс24");
?>
<div class="packagebt">
<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/paketnoe-vnedrenie-bitrix24/.inc.block_3.php", Array(), Array("MODE" =>
    "html")); ?>

<? $APPLICATION->IncludeComponent(
    "implementation.package",
    "",
    Array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "IBLOCK_ID" => "33"
    )
); ?>

<? $APPLICATION->IncludeComponent(
	"client.by.id",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"EXPERTISE_ID" => 32,
		"IBLOCK_ID" => "8"
	)
); ?>

<div class="crm-block__seo">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="material2">
                    <div class="include">
                        <h2>Что получаете в результате?</h2>
                        <p>По завершении работы вы получаете полностью настроенный в рамках услуги и готовый к работе корпоративный портал. Пакетное внедрение CRM Битрикс24 позволяет:</p>
                        <ul>
                            <li>Оптимизировать бизнес-процессы.</li>
                            <li>Увеличить конверсию и стимулировать продажи.</li>
                            <li>Снизить нагрузку и увеличить более эффективную работу сотрудников.</li>
                            <li>Предотвратить количество ошибок, потерю данных или лидов при работе.</li>
                        </ul>
                        <p>Вы также можете заказать индивидуальную настройку Б24 — поможем решить задачу любой сложности.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
</div>
