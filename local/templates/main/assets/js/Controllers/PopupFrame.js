if (typeof define === "function" && define.amd) {
    define(['Controls/Responsive', 'fancybox'], function (obResponsive) {

        function PopupFrame(button, options) {
            var o = this;
            this.button = $(button);
            var dataOptions = this.button.data();
            this.options = $.extend(true, {}, PopupFrame.defaultOptions, options, dataOptions);
            this.options.fbOptions.afterLoad = function () {
                this.context = o;
                $.proxy(o.options.fbOptions.afterLoad, this);
            };
            this.button.data('popupFrame', this);

            this.init();
        }

        PopupFrame.defaultOptions = {
            fbOptions: {
                padding: 0,
                margin: 0,
                autoSize: true,
                closeBtn: true,
                openEffect: 'none',
                closeEffect: 'none',
                scrollOutside: false,
                tpl: {
                    wrap: '<div class="fancybox-wrap popup-frame" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',
                    closeBtn: '<a title="Close" class="fancybox-item fancybox-close popup-close" href="javascript:void(0);">закрыть<i class="fa fa-times"></i></a>'
                },
                afterLoad: PopupFrame.afterLoad
            }
        };

        PopupFrame.prototype.setOptions = function (options) {
            if (typeof options != 'object') {
                throw new TypeError();
            }
            $.extend(this.options, options);
        };

        PopupFrame.prototype.afterLoad = function () {

        };

        PopupFrame.prototype.init = function () {
            var o = this;
            var fbOptions = $.extend(true, {}, o.options.fbOptions);

            if ($.inArray(obResponsive.getCurType(), ['xsmall', 'small']) >= 0) {
                fbOptions.topRation = 0;
            } else {
                fbOptions.topRation = 0.5;
            }

            this.button.fancybox(fbOptions);
        };

        return PopupFrame;
    });
}