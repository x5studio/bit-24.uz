<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle($arResult["NAME"]);
?>


<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "main",
    array(
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "main"
    ),
    false
); ?>
<!---- грохнуть при переносе ------>
<link href="/local/templates/main/assets/css/owl.carousel.min.css?15954718643351" type="text/css"  rel="stylesheet" />
<link href="/local/templates/main/assets/css/owl.theme.default.min.css?15954718641013" type="text/css"  rel="stylesheet" />
<script type="text/javascript" src="/local/templates/main/assets/js/owl.carousel.min.js?159547186544342"></script>
<!---- грохнуть при переносе ------>

<style>
    .breadcrumb {
        font-size: 14px;
        line-height: 16px;
        color: #FFFFFF;
    }
    .breadcrumb .link-style,
    .breadcrumb a {
        font-size: 14px;
        line-height: 16px;
        color: #ACB5BD;
    }

    @media (min-width: 1140px) {
        .breadcrumb-block .container {
            width: 786px;
        }
    }
</style>

<div class="blog-detail blog-detailm">
    <div class="blog-detail__header">
        <div class="container">
            <div class="row">
                <div class="blog-detail__header-caption"><h1>Работа с Битрикс24.Диск.Диск.Диск.Диск</h1></div>
                <div class="blog-detail__header-dop">
                    <div class="blog-detail__header-date">04 августа 2020</div>
                    <div class="blog-detail__header-time">5 минут</div>
                    <div class="blog-detail__header-view">345</div>
                </div>
            </div>
        </div>
    </div>
    <div class="blog-detail__body">
        <div class="container">
            <div class="row">
                <div class="blog-detail__u-a">
                    <div class="col-sm-7 col-md-8 col-xs-12">
                        <div class="blog_anchors-caption">Содержание</div>
                        <ul class="blog_anchors">
                            <li><a href="#">Виды интеграций</a></li>
                            <li><a href="#">Интеграция с сайтом на Битрикс</a></li>
                            <li><a href="#">Интеграция с сайтом на Wordpress</a></li>
                            <li><a href="#">Еще способы</a></li>
                            <li><a href="#">Итог</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-1 col-md-1 col-xs-12"></div>
                    <div class="col-sm-4 col-md-3 col-xs-12 fix178px">
                        <div class="blog-detail__author">
                            <div class="blog-detail__author-avatar">
                                <img src="/markup/news-detail/img/avatar.jpg">
                                <div class="blog-detail__author-status"></div>
                            </div>
                            <div class="blog-detail__author-avatar-wrapper">
                                <div class="blog-detail__author-name">Жуковский<br /> Александр</div>
                                <div class="blog-detail__author-position">Специалист по внедрению</div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <p class="blog-detail__pbig blog-detail__pborder"><strong>Сегодня нет универсального программного продукта, который мог бы решать абсолютно все задачи бизнеса от начала и до конца. Однако этот недостаток компенсирует интеграция: учетные системы, которыми компании пользуются для управления ресурсами в офлайне, можно связать с корпоративным порталом, объединяющим в себе CRM, менеджер задач, корпоративную соцсеть, конструктор бизнес-процессов и ряд других полезных бизнесу инструментов.</strong></p>
                <h2>Возможности интеграции H2</h2>
                <p>12 ноября вышел новый продукт 1С-Битрикс, который объединяет возможности Битрикс24 и «1С-Битрикс: Управление сайтом».</p>
                <p>«1С-Битрикс24: Интернет-магазин + CRM» поможет принимать и обрабатывать заказы в единой системе. Платформа включает в себя все инструменты продаж, такие как: интернет-магазины, CRM-инструменты и многое другое.</p>
                <div class="mbot30">
                    <strong>Обычно резервные копии делаются по трём причинам:</strong>
                    <ul>
                        <li>защита от неумышленного повреждения — например, когда кто-то случайно удалил инфоблок или заказ;</li>
                        <li>защита от умышленного повреждения — когда сайт взломали и намеренно удалили с него что-либо;</li>
                        <li>защита от выхода из строя оборудования — компьютеры всё-таки физические объекты, и у них иногда умирают жесткие диски со всей информацией.</li>
                    </ul>
                </div>
                <img src="/markup/news-detail/img/news1.jpg" alt="">
                <h3>Данные для загрузки и обмена H3</h3>
                <div class="mbot30">
                    <strong>Обычно резервные копии делаются по трём причинам:</strong>
                    <ol>
                        <li>защита от неумышленного повреждения — например, когда кто-то случайно удалил инфоблок или заказ;
                            <ol>
                                <li>защита от неумышленного повреждения — например, когда кто-то случайно удалил инфоблок или заказ;</li>
                                <li>защита от неумышленного повреждения — например, когда кто-то случайно удалил инфоблок или заказ;</li>
                            </ol>
                        </li>
                        <li>защита от умышленного повреждения — когда сайт взломали и намеренно удалили с него что-либо;
                            <ol>
                                <li>защита от неумышленного повреждения — например, когда кто-то случайно удалил инфоблок или заказ;</li>
                                <li>защита от неумышленного повреждения — например, когда кто-то случайно удалил инфоблок или заказ;</li>
                            </ol>
                        </li>
                        <li>защита от выхода из строя оборудования — компьютеры всё-таки физические объекты, и у них иногда умирают жесткие диски со всей информацией.</li>
                    </ol>
                </div>
                <div class="bslider blog-detail__slider">
                    <div class="owl-carousel bslider__wrapper blog-detail__slider-wrapper">
                        <div class="bslider__item">
                            <img src="/markup/news-detail/img/slider1.jpg">
                        </div>
                        <div class="bslider__item">
                            <img src="/markup/news-detail/img/news1.jpg">
                        </div>
                        <div class="bslider__item">
                            <img src="/markup/news-detail/img/slider1.jpg">
                        </div>
                        <div class="bslider__item">
                            <img src="/markup/news-detail/img/news1.jpg">
                        </div>
                    </div>
                    <div class="bslider__counter"></div>
                </div>
                <script>
                    jQuery('.blog-detail__slider-wrapper').on('initialized.owl.carousel changed.owl.carousel', function(e) {
                        if (!e.namespace)  {
                            return;
                        }
                        var carousel = e.relatedTarget;
                        var c = carousel.relative(carousel.current())+1;
                        var a = carousel.items().length;
                        jQuery('.bslider__counter').html('<span>'+c+'</span> / '+' ' + a);
                        }).owlCarousel({
                            items: 1,
                            loop:true,
                            margin:0,
                            nav:true,
                            dots:false,
                        });
                </script>
                <h4>Данные для загрузки и обмена H4</h4>
                <p>Менеджер создает в <a href="#">CRM «Сделку»</a>, при этом он должен проверить наличие товара на складе, уточнить его цену и характеристики в учетной системе, затем выставить счет и проконтролировать оплату (для последнего нужно запросить информацию у бухгалтерии).<br />Если в компании настроена интеграция, менеджер может работать только в «Битрикс24» — там он увидит остатки товаров и оплату счетов. Кроме того, он сможет сразу сформировать накладную или счет и отправить ее клиенту. Сделки закрываются быстрее, а значит растет прибыль.</p>
                <div class="blog-detail__two">
                    <div class="col-sm-6 col-md-7 col-xs-6 blog-detail__col-sm">
                        <p>В библиотеках доступны все исходные элементы, на которых построены наши интерфейсы. Ориентируясь на эти примеры, вы можете разобраться, как организовывать сложные структуры на Auto Layout, где есть вложенные заменяемые части. Это будет полезно при построении собственных библиотек компонентов.</p>
                    </div>
                    <div class="col-sm-1 col-md-1 col-xs-1 blog-detail__col-sm"></div>
                    <div class="col-sm-5 col-md-4 col-xs-5 blog-detail__col-sm">
                        <p class="blog-detail__psmall blog-detail__pborder blog-detail__red fix178px">Основное преимущество, которое дает интеграция, — отмена двойного ввода данных в разных системах. </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="blog-detail__fblock">
                    Основное преимущество, которое дает интеграция, — отмена двойного ввода данных в разных системах. Это значит, что увеличится скорость и точность процессов, а следовательно, и эффективность вашего бизнеса.
                </div>
                <p>Если у вас используется нестандартная или устаревшая конфигурация, для обмена дополнительными данными потребуется доработка существующих модулей или разработка их с нуля. То есть недостаточно иметь «1С» — важно знать ее конфигурацию, особенности и совместимость с иными программными продуктами издателя.</p>
                <blockquote>
                    <p>Основное преимущество, которое дает интеграция, — отмена двойного ввода данных в разных системах. Это значит, что увеличится скорость и точность процессов, а следовательно, и эффективность вашего бизнеса.</p>
                    <div class="blog-detail__user">
                        <div class="blog-detail__user-avatar">
                            <img src="/markup/news-detail/img/avatar.jpg">
                        </div>
                        <div class="blog-detail__user-body">
                            <div class="blog-detail__user-name">Жуковский Александр</div>
                            <div class="blog-detail__user-position">Специалист по внедрению</div>
                        </div>
                    </div>
                </blockquote>
                <div class="blog-detail__two">
                    <div class="col-sm-6 col-md-7 col-xs-12">
                        <p>Какие способы повышают мотивацию персонала и создают спокойную рабочую атмосферу в коллективе? Размер оплаты труда может быть фиксированным или гибким, главное – обсудить все вопросы насчет денег еще на стадии заключения трудового договора с новым сотрудником и достичь взаимопонимания по поводу механизма расчета и оплаты труда. Важно не только грамотно оценить соотношение объема работы и заработной платы, но и регулярно пересматривать сложившуюся пропорцию, чтоб поддерживать уровень оплаты труда на достойном, без занижений уровне.</p>
                    </div>
                    <div class="col-sm-1 col-md-1 col-xs-12"></div>
                    <div class="col-sm-5 col-md-4 col-xs-12 fix178px">
                        <div class="blog-detail__counter">
                            <div class="blog-detail__counter-value">2 000</div>
                            <div class="blog-detail__counter-name">посещений в день</div>
                        </div>
                        <p class="blog-detail__psmall">Ориентируясь на эти примеры, вы можете разобраться, как организовывать сложные структуры на Auto Layout, где есть вложенные заменяемые части. Это будет полезно при построении собственных библиотек компонентов.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="blog-detail__notice">
                    <div class="blog-detail__notice-wrapper">
                    Основное преимущество, которое дает интеграция, — отмена двойного ввода данных. Это значит, что увеличится скорость и точность процессов, а следовательно, и эффективность вашего бизнеса.
                    </div>
                </div>
                <div class="bslider blog-detail__slider2">
                    <div class="owl-carousel bslider__wrapper blog-detail__slider-wrapper2">
                        <div class="bslider__item">
                            <img src="/markup/news-detail/img/slider1.jpg">
                            <div class="bslider__item-text">1 Для обмена данными компания «1С-Битрикс» разработала специальные модули обмена. Обратите внимание: модули разработаны </div>
                        </div>
                        <div class="bslider__item">
                            <img src="/markup/news-detail/img/news1.jpg">
                            <div class="bslider__item-text">2 Для обмена данными компания «1С-Битрикс» разработала специальные модули обмена. Обратите внимание: модули разработаны  Для обмена данными компания «1С-Битрикс» разработала специальные модули обмена. Обратите внимание: модули разработаны </div>
                        </div>
                        <div class="bslider__item">
                            <img src="/markup/news-detail/img/slider2.jpg">
                            <div class="bslider__item-text">3 Для обмена</div>
                        </div>
                        <div class="bslider__item">
                            <img src="/markup/news-detail/img/slider1.jpg">
                            <div class="bslider__item-text">4 Для обмена данными компания «1С-Битрикс» разработала специальные модули обмена. Обратите внимание: модули разработаны </div>
                        </div>
                        <div class="bslider__item">
                            <img src="/markup/news-detail/img/news1.jpg">
                            <div class="bslider__item-text">5 Для обмена данными компания «1С-Битрикс» разработала специальные модули обмена. Обратите внимание: модули разработаны </div>
                        </div>
                    </div>
                    <div class="bslider__counter2"></div>
                </div>
                <script>
                    jQuery('.blog-detail__slider-wrapper2').on('initialized.owl.carousel changed.owl.carousel', function(e) {
                        if (!e.namespace)  {
                            return;
                        }
                        var carousel = e.relatedTarget;
                        var c = carousel.relative(carousel.current())+1;
                        var a = carousel.items().length;
                        jQuery('.bslider__counter2').html('<span>'+c+'</span> / '+' ' + a);
                    }).owlCarousel({
                        items: 1,
                        loop:true,
                        margin:0,
                        nav:true,
                        dots:false,
                    });
                </script>
                <p>В Битриксе есть автоматическое создание резервных копий. <a href="#">Если у Битрикса</a> активная лицензия, он может загружать свои бэкапы в свое же облако. Минус — в зависимости от лицензии есть ограничения по месту в этом облаке, и поэтому в нем хранится только несколько последних бэкапов.</p>
                <p>Чаще всего мы настраиваем бэкапы на уровне сервера: бэкап может развернуть только администратор сервера, а админ сайта (заказчик) — нет. Если заказчик хочет иметь возможность разворачивать бэкап самостоятельно (сразу подозревает, что у него не очень квалифицированные контент-менеджеры, которые могут всё поломать), мы настраиваем автоматическое резервное копирование через Битрикс, и тогда бэкап можно развернуть в несколько кликов.</p>
                <hr>
                <p>
                    <strong>Дизайн-системой VKUI пользуется не только наша команда, но и большое сообщество разработчиков и дизайнеров мини-приложений. Они создают сервисы, к которым можно перейти со второй вкладки мобильного клиента VK, — а таких мини-аппов уже больше 16 тысяч.</strong>
                    <br />
                    <a href="#" class="btn btn-default mbot30">Заказать интеграцию</a>
                </p>
                <div class="mbot50">
                    <strong>Обычно резервные копии делаются по трём причинам:</strong>
                    <ul class="ul-line">
                        <li>планировщик задач Antodo,</li>
                        <li>приложение Космос,</li>
                        <li>сервис для тренировок в зале и дома FitQuest,</li>
                        <li>приложение для изучения правил дорожного движения ПДД 2020.</li>
                    </ul>
                </div>
                <div class="blog-detail__blue">
                    <strong class="blog-detail__pbig">Они создают сервисы, к которым можно перейти со второй мобильного клиента VK, — а таких мини-аппов уже больше 16 тысяч.</strong>
                    <br />
                    <a href="#" class="btn btn-default">Заказать интеграцию</a>
                </div>
                <p>
                    <strong>MX-запись (Mail Exchange)<br /></strong>
                    Обычно мы делаем бэкапы так, чтобы они автоматически хранились на серверах минимум две недели. Раз в неделю делается полный бекап (мастер-бекап), в остальные 6 дней пересохраняются только изменённые файлы относительно этого мастера. Благодаря этому в любой день из этого промежутка всегда можно было откатиться к копии сайта.
                </p>
            </div>
        </div>
    </div>
</div>

<div class="form-feed-back">
    <div class="container">
        <div class="row">
            <div class="form-feed-back__wrapper">
                <form>
                    <div class="form-feed-back__block">
                        <div class="form-feed-back__bc">
                            <div class="form-feed-back__caption">Остались вопросы?</div>
                            <div class="form-feed-back__text">Заполните данные и мы с Вами свяжемся!</div>
                        </div>
                        <div class="form-feed-back__it">
                            <div class="form-feed-back__item">
                                <input name="form_text_17" type="text" data-type="REQUIRED"	value="" placeholder="Имя *" class="form-control" />
                            </div>
                            <div class="form-feed-back__item">
                                <input name="form_text_18" type="text" data-type="REQUIRED_FROM_GROUP PHONE_NO_REQUIRED" value="" placeholder="Телефон *" class="form-control" />
                            </div>
                            <div class="form-feed-back__item">
                                <input name="form_text_19" type="text" data-type="REQUIRED_FROM_GROUP EMAIL_NO_REQUIRED"	value="" placeholder="E-mail *" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="form-feed-back__block form-feed-back__block-revert">
                        <div class="form-feed-back__but">
                            <input type="submit" onclick="yaCounter45927957.reachGoal('expert-send');gtag('event', 'send', {'event_category': 'expert', 'event_label': 'send'}); return true;" class="btn btn-default" name="web_form_submit" value="Оставить заявку">
                        </div>
                        <div class="form-feed-back__mess">
                            Нажатием кнопки я принимаю условия <a href="/oferta/">Оферты</a> и согласен с <a href="/politika-konfidencialnosti/">Политикой конфиденциальности</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="blog-detail">
    <div class="blog-detail__body">
        <div class="container">
            <div class="row">
                <div class="blog-detail__tags">
                    <a href="#">Внедрение Битрикс24</a>
                    <a href="#">Работа с Битрикс24</a>
                    <a href="#">Интеграции Битрикс24</a>
                    <a href="#">Интеграция Битрикс24</a>
                    <a href="#">Акции</a>
                    <a href="#">Новости</a>
                    <a href="#">Все о тарифах Битрикс24</a>
                    <a href="#">Все о CRM</a>
                </div>
                <div class="blog-detail__share">
                    <!--
                <script type="text/javascript">(function() {
                        if (window.pluso)
                            if (typeof window.pluso.start == "function")
                                return;
                        if (window.ifpluso == undefined) {
                            window.ifpluso = 1;
                            var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                            s.type = 'text/javascript';
                            s.charset = 'UTF-8';
                            s.async = true;
                            s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                            var h = d[g]('body')[0];
                            h.appendChild(s);
                        }
                    })();</script>
                <div class="pluso" data-background="transparent"
                     data-options="medium,square,line,horizontal,counter,theme=04"
                     data-services="vkontakte,facebook,odnoklassniki,moimir"></div>

                -->
                    <div class="blog-detail__share-caption">Поделилось:  57 человек</div>
                    <div class="blog-detail__share-block">
                        <a class="blog-detail__share-url" href="#"><img src="/local/templates/main/assets/img/icons/fb.svg" alt=""></a>
                        <a class="blog-detail__share-url" href="#"><img src="/local/templates/main/assets/img/icons/tv.svg" alt=""></a>
                        <a class="blog-detail__share-url" href="#"><img src="/local/templates/main/assets/img/icons/ins.svg" alt=""></a>
                        <a class="blog-detail__share-url" href="#"><img src="/local/templates/main/assets/img/icons/vk.svg" alt=""></a>
                        <a class="blog-detail__share-url" href="#"><img src="/local/templates/main/assets/img/icons/yt.svg" alt=""></a>
                    </div>
                </div>
                <div class="blog-detail__back">
                    <a class="blog-detail__back-url" href="#">вернуться в список материалов</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bslider articles-slider">
    <div class="container">
        <div class="row">
            <div class="articles-slider__heading">Предлагаемые статьи</div>
            <div class="articles-slider__counter"></div>
            <div class="owl-carousel articles-slider__items">
                <div class="articles-slider__item">
                    <div class="articles-slider__img">
                        <a href="#"><img class="articles-slider__image" src="/markup/news-detail/img/articles-slider1.jpg"></a>
                    </div>
                    <div class="articles-slider__caption">
                        <a class="articles-slider__caption-url" href="#">Интеграция Битрикс24 с 1С</a>
                    </div>
                </div>
                <div class="articles-slider__item">
                    <div class="articles-slider__img">
                        <a href="#"><img class="articles-slider__image" src="/markup/news-detail/img/articles-slider2.jpg"></a>
                    </div>
                    <div class="articles-slider__caption">
                        <a class="articles-slider__caption-url" href="#">CRM - что это такое простыми словами</a>
                    </div>
                </div>
                <div class="articles-slider__item">
                    <div class="articles-slider__img">
                        <a href="#"><img class="articles-slider__image" src="/markup/news-detail/img/articles-slider3.jpg"></a>
                    </div>
                    <div class="articles-slider__caption">
                        <a class="articles-slider__caption-url" href="#">Возможности CRM-маркетинга Битрикс24</a>
                    </div>
                </div>
                <div class="articles-slider__item">
                    <div class="articles-slider__img">
                        <a href="#"><img class="articles-slider__image" src="/markup/news-detail/img/articles-slider1.jpg"></a>
                    </div>
                    <div class="articles-slider__caption">
                        <a class="articles-slider__caption-url" href="#">Интеграция Битрикс24 с 1С</a>
                    </div>
                </div>
                <div class="articles-slider__item">
                    <div class="articles-slider__img">
                        <a href="#"><img class="articles-slider__image" src="/markup/news-detail/img/articles-slider2.jpg"></a>
                    </div>
                    <div class="articles-slider__caption">
                        <a class="articles-slider__caption-url" href="#">CRM - что это такое простыми словами</a>
                    </div>
                </div>
                <div class="articles-slider__item">
                    <div class="articles-slider__img">
                        <a href="#"><img class="articles-slider__image" src="/markup/news-detail/img/articles-slider3.jpg"></a>
                    </div>
                    <div class="articles-slider__caption">
                        <a class="articles-slider__caption-url" href="#">Возможности CRM-маркетинга Битрикс24</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery('.articles-slider__items').on('initialized.owl.carousel changed.owl.carousel', function(e) {
        if (!e.namespace)  {
            return;
        }
        var carousel = e.relatedTarget;
        var c = carousel.relative(carousel.current())+1;
        var a = carousel.items().length;
        jQuery('.articles-slider__counter').html('<span>'+c+'</span> / '+' ' + a);
    }).owlCarousel({
        items: 3,
        loop:true,
        margin:16,
        nav:true,
        dots:false,
        autoWidth:false,
        responsive : {
            0 : {
                items: 1,
                dots:true,
                autoWidth:true,
            },
            480 : {
                items: 1,
                autoWidth:true,
            },
            768 : {
                items: 2,
                autoWidth:true,
            },
            960 : {
                items: 3,
                autoWidth:false,
            }
        }
    });
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
