<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//delayed function must return a string
if (empty($arResult))
    return "";

$strReturn = '';

$strReturn .= '<ul class="breadcrumbs">';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++)
{
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    $strReturn .= '<li>';
    if ($index == $itemSize-1) {
        $strReturn .= '<span class="breadcrumbs__link">'.$title.'</span>';
    } else {
        $strReturn .= '<a class="breadcrumbs__link" href="'.$arResult[$index]["LINK"].'">'.$title.'</a>';
    }
    $strReturn .= '</li>';
}
$strReturn .= '</ul>';

return $strReturn;