<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$smartBase = ($arParams["SEF_URL_TEMPLATES"]["section"]? $arParams["SEF_URL_TEMPLATES"]["section"]: "#SECTION_ID#/");
$arDefaultUrlTemplates404 = array(
	"sections" => "",
	"section" => "#SECTION_ID#/",
	"element" => "#SECTION_ID#/#ELEMENT_ID#/",
	"compare" => "compare.php?action=COMPARE",
);

$arDefaultVariableAliases404 = array();

$arDefaultVariableAliases = array();

$arComponentVariables = array(
	"SECTION_ID",
	"SECTION_CODE",
	"ELEMENT_ID",
	"ELEMENT_CODE",
);

$arVariables = array();

$engine = new CComponentEngine($this);
if (\Bitrix\Main\Loader::includeModule('iblock'))
{
	$engine->addGreedyPart("#SECTION_CODE_PATH#");
	$engine->setResolveCallback(array("CIBlockFindTools", "resolveComponentEngine"));
}
$arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates($arDefaultUrlTemplates404, $arParams["SEF_URL_TEMPLATES"]);
$arVariableAliases = CComponentEngine::MakeComponentVariableAliases($arDefaultVariableAliases404, $arParams["VARIABLE_ALIASES"]);

$componentPage = $engine->guessComponentPath(
	$arParams["SEF_FOLDER"],
	$arUrlTemplates,
	$arVariables
);

$b404 = false;
if(!$componentPage)
{
	$componentPage = "sections";
	$b404 = true;
}

if($componentPage == "section")
{
	if (isset($arVariables["SECTION_ID"]))
		$b404 |= (intval($arVariables["SECTION_ID"])."" !== $arVariables["SECTION_ID"]);
	else
		$b404 |= !isset($arVariables["SECTION_CODE"]);
}

if($b404 && CModule::IncludeModule('iblock'))
{
	$folder404 = str_replace("\\", "/", $arParams["SEF_FOLDER"]);
	if ($folder404 != "/")
		$folder404 = "/".trim($folder404, "/ \t\n\r\0\x0B")."/";
	if (substr($folder404, -1) == "/")
		$folder404 .= "index.php";
	if ($folder404 != $APPLICATION->GetCurPage(true))
	{
		\Bitrix\Iblock\Component\Tools::process404(
			""
			,true
			,true
			,true
			,"/404.php"
		);
	}
}

$arVariables["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
$arVariables["SEF_FOLDER"] = $arParams["SEF_FOLDER"];
if($componentPage == "sections")
	$APPLICATION->IncludeFile($arParams["SEF_FOLDER"]."sections_gruzdev.php", $arVariables, array("MODE"=>"php"));
elseif($componentPage == "section")
	$APPLICATION->IncludeFile($arParams["SEF_FOLDER"]."section_gruzdev.php", $arVariables, array("MODE"=>"php"));
elseif($componentPage == "element")
	$APPLICATION->IncludeFile($arParams["SEF_FOLDER"]."element_gruzdev.php", $arVariables, array("MODE"=>"php"));
else{
	\Bitrix\Iblock\Component\Tools::process404("",true,true,true,"/404.php");
	return;
}
