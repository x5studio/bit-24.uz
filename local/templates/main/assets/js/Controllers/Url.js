if (typeof define === "function" && define.amd) {
    define([], function () {
        function Url(par) {
            if(typeof par === 'string' )
            {
                this.parse(par);
            }else{
                this.parse(par.toString())
            }
        }

        Url.prototype.parse = function (uri) {
            var
                arUrl = uri.split('?'),
                protocolHostAndHash = arUrl[0].split('#'),
                hash = protocolHostAndHash[1],
                arProtocolAndHost = protocolHostAndHash[0].split('//'),
                hostAndPath = arProtocolAndHost.length === 1 ? arProtocolAndHost[0] : arProtocolAndHost[1],
                protocol = arProtocolAndHost.length > 1 ? arProtocolAndHost[0] : undefined,
                queryString = arUrl[1],
                host = hostAndPath.split('/')[0],
                path = hostAndPath.slice(host.length)
            ;
            this.error =
                ( arUrl.length > 2 )
                || ( protocolHostAndHash.length > 2 )
                || ( arProtocolAndHost.length > 2 )
                || ( protocol && (protocol.slice(-1) !== ':') )
            ;

            this.params = {};

            if(queryString) {
                params = queryString.split('&');

                for (var i = 0; i < params.length; i++) {
                    var
                        par = params[i].split('=');

                    this.params[par[0]] = par[1];
                }
            }

            this.protocol = protocol ? protocol.slice(0, -1) : '';
            this.host = host ? host : '';
            this.hash = hash ? hash : '';
            this.path = path ? path : '';
        };

        Url.prototype.getQueryString = function(){
            var
                queryString = '';

            for(var key in this.params)
            {
                if(this.params.hasOwnProperty(key))
                {
                    queryString += key + '=' + this.params[key]+'&';
                }
            }

            if(queryString){
                queryString = queryString.slice(0,-1);
            }

            return queryString;
        };

        Url.prototype.getUrl = function () {
            if(this.error)
            {
                return '';
            }


            var
                queryString = this.getQueryString(),
                result = '';

            result += this.protocol ? this.protocol+'://' : '';
            result += this.host ? this.host : '';
            result += this.path ? this.path : '';
            result += queryString ? '?'+queryString : '';
            result += this.hash ? '#'+this.hash : '';

            return result;
        };

        return Url;
    })
}