<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="mattop">
	<h2>Шаг 2 — Настройка портала</h2>
	<h3>Создание надстроек над базовым функционалом Битрикс24&nbsp;<br>
	 По нашему опыту, это требуется для большинства компаний</h3>
</div>
<div class="include">
	<h3>Что входит:</h3>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<p>
				 На этапе настройки портала мы производим&nbsp;доработку и оптимизацию функционала Битрикс24 под действующие процессы вашей компании по техническому заданию и плану внедрения, выработанному на Шаге №1.&nbsp;
			</p>
			<p>
 <br>
			</p>
			<p>
				 Результатом данного шага является настроенный и доработанный корпоративный портал Битрикс24&nbsp;под конкретные задачи и процессы вашей компании.
			</p>
		</div>
	</div>
</div>
<div class="bticon">
	<div class="row">
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="bt">
 <img src="/local/templates/main/assets/img/bt/009.png" class="img-responsive">
				<p>
					 Интеграция с формами на сайте
				</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="bt2">
 <img src="/local/templates/main/assets/img/bt/010.png" class="img-responsive">
				<p>
					 Интеграция с 1С
				</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="bt3">
 <img src="/local/templates/main/assets/img/bt/011.png" class="img-responsive">
				<p>
					 Загрузка базы лидов, сделок, контактов и&nbsp;контрагентов в CRM
				</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="bt2">
 <img src="/local/templates/main/assets/img/bt/012.png" class="img-responsive">
				<p>
					 Настройка карточек CRM
				</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="bt">
 <img src="/local/templates/main/assets/img/bt/013.png" class="img-responsive">
				<p>
					 Настройка бизнес-процессов Битрикс24
				</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="bt">
 <img src="/local/templates/main/assets/img/bt/014.png" class="img-responsive">
				<p>
					 Миграция облачного Битрикс24 в "коробку"
				</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="bt">
 <img src="/local/templates/main/assets/img/bt/015.png" class="img-responsive">
				<p>
					 Подключение на сайт виджета Битрикс24
				</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="bt2">
 <img src="/local/templates/main/assets/img/bt/016.png" class="img-responsive">
				<p>
					 Интеграция с телефонией
				</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12">
			<div class="bt4">
 <a href="/uslugi/" title="link">Посмотреть все услуги</a>
			</div>
		</div>
	</div>
</div>
 <br>