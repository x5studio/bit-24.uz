<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_BLOG_ID"] = intval($arParams["IBLOCK_BLOG_ID"]);
$arParams["IBLOCK_TAG_ID"] = intval($arParams["IBLOCK_TAG_ID"]);
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
$arParams["TAG"] = trim($arParams["TAG"]);
$arParams["SEF_FOLDER"] = '/blog/';


if ($this->StartResultCache())
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arResult["URL"] = $arParams["SEF_FOLDER"];

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_BLOG_ID"],
		"ACTIVE" => "Y"
	);
	if (strlen($arParams["SECTION_CODE"]) > 0)
	{
		$arFilter["SECTION_CODE"] = $arParams["SECTION_CODE"];
		$arResult["URL"] .= $arParams["SECTION_CODE"] . '/';
	}


	$arTag = array();

	$rsElement = \CIBlockElement::GetList(
		array(),
		$arFilter,
		array("PROPERTY_TAG")
	);
	while ($arElement = $rsElement->GetNext())
	{
		$arTag[] = $arElement["PROPERTY_TAG_VALUE"];
	}

	if (count($arTag))
	{
		$rsTag = \CIBlockElement::GetList(
			array("NAME" => "ASC"),
			array("IBLOCK_ID" => $arParams["IBLOCK_TAG_ID"], "ACTIVE" => "Y", "=ID" => $arTag),
			false,
			false,
			array("ID", "NAME", "CODE")
		);
		while ($arTag = $rsTag->GetNext())
		{
			$arResult["TAG"][$arTag["CODE"]] = $arTag;
		}
	}

	if (strlen($arParams["TAG"]) > 0)
	{
		if (!empty($arResult["TAG"][$arParams["TAG"]]))
		{
			$arResult["ID"] = $arResult["TAG"][$arParams["TAG"]]["ID"];
		} else
		{
			$this->AbortResultCache();
			\Bitrix\Iblock\Component\Tools::process404(
				""
				, true
				, true
				, true
				, "/404.php"
			);
			return;
		}
	}

	$this->SetResultCacheKeys(array("ID"));
	$this->IncludeComponentTemplate();
}
if (isset($arResult["ID"]))
{
	global $TAG_ID;
	$TAG_ID = $arResult["ID"];
}