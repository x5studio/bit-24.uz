<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$showCarousel = count($arResult["ITEMS"]) > 3;
?>

<? if (count($arResult["ITEMS"]) > 0): ?>
	<section class="projects">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12<?
				if (count($arResult["ITEMS"]) <= 3) {
					echo ' projects__no-carousel-list';
				}
				?>">
					<span class="h1">Выполненные проекты в отрасли</span>
					<? if ($showCarousel): ?>
						<div id="pCarousel" class="carousel carousel-showmanymoveone slide" data-ride="carousel">
							<!-- Wrapper for slides -->
							<div class="carousel-inner">
							<?php endif; ?>
							<?
							foreach ($arResult["ITEMS"] as $cell => $arItem):
								if ($showCarousel): ?>
									<div class="item<? if (!$cell): ?> active<? endif; ?>">
								<? endif; ?>
								<div class="col-lg-4 col-xs-12 col-md-4 col-sm-6">
									<div class="project1">
										<div class="p-img">
											<img src="<?= $arResult["IMAGES"][$arItem["PICTURE"]] ?>" class="img-responsive">
										</div>
										<a href="<?= $arItem["SECTION_PAGE_URL"] ?>" title="<?= $arItem["NAME"] ?>">
											<span class="text-left projеct-title"><?= $arItem["NAME"] ?></span>
										</a>
										<p><?= $arItem["DESCRIPTION"] ?></p>
									</div>
								</div>
								<? if ($showCarousel): ?>
									</div>
								<?endif;
							endforeach; ?>

							<? if ($showCarousel): ?>
							</div>
							<!-- Left and right controls -->
							<div class="hidden-xs">
								<a class="left carousel-control" href="#pCarousel" role="button" data-slide="prev">
									<i class="fa fa-angle-left" aria-hidden="true"></i>
									<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#pCarousel" role="button" data-slide="next">

									<i class="fa fa-angle-right" aria-hidden="true"></i>
									<span class="sr-only">Next</span>
								</a>
							</div>
							<a class="left carousel-control hidden-sm hidden-lg hidden-md col-xs-6" href="#pCarousel" role="button"
							   data-slide="prev">
								<i class="fa fa-long-arrow-left" aria-hidden="true"></i>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control hidden-sm hidden-lg hidden-md col-xs-6" href="#pCarousel" role="button"
							   data-slide="next">
								<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
								<span class="sr-only">Next</span>
							</a>
						</div>
					<? endif; ?>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>