/**
 * Created by Павел on 29.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/Contact/AContactBoxView'], function ($, AContactBoxView) {
        function ContactBoxView_3(item, options) {
            AContactBoxView.apply(this, arguments);
            this.options = $.extend(true, {}, this.options, ContactBoxView_3.defaultOptions, options);
        }

        extend(ContactBoxView_3, AContactBoxView);
        ContactBoxView_3.defaultOptions = {};

        ContactBoxView_3.prototype.compileOfficeItem = function (item, mOffice) {
            ContactBoxView_3.superclass.compileOfficeItem.apply(this, arguments);
            var o = this;
            var realOffice = o.getContactManager().getRealOfficeByID(mOffice['ID']);
            try {
                if (realOffice["PROPERTIES"]["ICON_METRO"]["VALUE"]) {
                    item.find('.box-office-name').addClass('metro metro-' + realOffice["PROPERTIES"]["ICON_METRO"]["VALUE"]);
                }
            } catch (e) {
            }
        };

        return ContactBoxView_3;
    });
}