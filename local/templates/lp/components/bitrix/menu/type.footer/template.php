<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
<h4 class="col-md-12 col-sm-12 hidden-xs">Типы внедрения Битрикс24</h4>
<nav class="col-md-12 hidden-sm hidden-xs">
	<ul class="list-unstyled">
		<? foreach ($arResult as $arItem): ?>
		<li><a href="<?= $arItem["LINK"] ?>" title=<?= $arItem["TEXT"] ?>"><?= $arItem["TEXT"] ?></a></li>
<? endforeach ?>
	</ul>
</nav>
<? endif ?>