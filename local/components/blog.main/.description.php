<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Блог. На главную",
	"DESCRIPTION" => "Блог. На главную",
	"ICON" => "/images/news_line.gif",
	"SORT" => 115,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "blog"
		)
	),
);

?>