<?php

//Теги
$page = $APPLICATION->GetCurPage(false);
$arFilter = [
	'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	'ACTIVE' => 'Y'
];
$rsSection = CIBlockSection::GetList(
		['SORT' => 'ASC'], $arFilter, false, ['*'], false
);
$arSections = [];
while ($arSection = $rsSection->GetNext(true, false)) {
	$arSections[$arSection['ID']] = $arSection;
	if ($arResult['SECTION']['SECTION_PAGE_URL'] == $arSection['SECTION_PAGE_URL'] || $page == $arSection['SECTION_PAGE_URL']) {
		$arSections[$arSection['ID']]['CURRENT'] = 'Y';
	}
}
$arResult['SECTIONS'] = $arSections;
