<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


if (
    $arParams['PRODUCT_VERSION']=='Облачная' && $arResult['IBLOCK_SECTION_ID'] != EDITIONS_OBL_SECTION_ID
    || $arParams['PRODUCT_VERSION']=='Коробочная' && $arResult['IBLOCK_SECTION_ID'] != EDITIONS_KOROB_SECTION_ID
) {
    \Bitrix\Iblock\Component\Tools::process404("", true, true, true);
}

