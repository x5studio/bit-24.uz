<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<?
// старт крошки

$cacheId = 'blog_categories';
$cache_dir = '/blog_categories';
$obCache = new CPHPCache;

if ($obCache->InitCache(3600, $cacheId, $cache_dir)) {
    $blogCategories = $obCache->GetVars();
}
elseif ($obCache->StartDataCache()) {
    $blogCategories = [];
    $rsItems = CIBlockSection::GetList(
        ['SORT' => 'ASC'],
        [
            'IBLOCK_ID' => 2,
            'ACTIVE' => 'Y',
        ],
        false,
        ['SECTION_PAGE_URL']
    );
    while($arItem = $rsItems->GetNext()) {
        $blogCategories[] = $arItem['SECTION_PAGE_URL'];
    }
    global $CACHE_MANAGER;
    $CACHE_MANAGER->StartTagCache($cache_dir);
    $CACHE_MANAGER->RegisterTag('iblock_id_2');
    $CACHE_MANAGER->EndTagCache();
    $obCache->EndDataCache($blogCategories);
}
else {
    $blogCategories = [];
}

if (strpos($APPLICATION->GetCurPage(),'blog') === false || $APPLICATION->GetCurPage() === '/blog/' || $APPLICATION->GetCurPage() === '/blog/search/' || in_array($APPLICATION->GetCurPage(), $blogCategories, true)) {
    $breadcrumbTemplate = 'main';
}
else {
    $breadcrumbTemplate = 'blog';
}
?>

<hr class="my-0">
<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb", $breadcrumbTemplate, array(
    "PATH" => "",
    "SITE_ID" => "s1",
    "START_FROM" => "0",
    "COMPONENT_TEMPLATE" => "main"
), false
); ?>
<?$APPLICATION->AddHeadString('<style>
    .breadcrumb-block, .breadcrumb {
        background-color: #fff;
    }
</style>');?>
<?if (!$GLOBALS['IS_BLOG_ROOT_OR_SECTION']):?>
    <section class="line-header line-header__section_1">
        <div class="container">
            <div class="row">
                <div class="title-page">
                    <h1 class="title-page__h1"><? $APPLICATION->ShowTitle(true) ?></h1>
                </div>
            </div>
        </div>
    </section>
<?endif;?>

