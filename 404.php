<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define('ERROR_404', 'Y');
define('MAIN_TEMPLATE_PATH', '/local/templates/main');

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Ошибка 404");
$APPLICATION->AddChainItem("404");
Bitrix\Main\Page\Asset::getInstance()->addCss(MAIN_TEMPLATE_PATH."/assets/css/page404.css", true);
?>
<div class="page404__wrapper container">
    <div class="page404__title">
        Запрашиваемая вами страница<br/>
        не существует или была удалена
    </div>
    <div class="page404__back">
        <a class="page404__back-btn btn btn-default" href="/">Перейти на главную</a>
    </div>
</div>
<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>