$(document).ready(function () {
        jQuery(document).on('submit', '.sb_blog_feedback', function (e) {
            e.preventDefault();
            var m = jQuery(this).closest('form');
            var fio = $($('[name="QUESTION"] .form-feed-back__item input')[0]).val();
            var phone = $($('[name="QUESTION"] .form-feed-back__item input')[1]).val();
            var mail = $($('[name="QUESTION"] .form-feed-back__item input')[2]).val();
            var ct_site_id = '34885';
            var sub = 'Остались вопросы?';

            var comment = 'Ссылка на блог: ' + document.location.href;
            var ct_send = false;
            if (fio != 'Имя' && phone != 'Телефон') {
                ct_send = true
            }
            var ct_data = {
                fio: fio,
                phoneNumber: phone,
                subject: sub,
                email: mail,
                comment: comment,
                sessionId: window.call_value
            };
            if (!!phone && !!fio && !!mail && !!ct_send) {
                jQuery.ajax({
                    url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/' + ct_site_id + '/register/',
                    dataType: 'json', type: 'POST', data: ct_data, async: false
                });
                window.location.reload();
            }
        });
        jQuery('.blog-detail__slider-wrapper').on('initialized.owl.carousel changed.owl.carousel', function (e) {
            if (!e.namespace) {
                return;
            }
            var carousel = e.relatedTarget;
            var c = carousel.relative(carousel.current()) + 1;
            var a = carousel.items().length;
            jQuery('.bslider__counter').html('<span>' + c + '</span> / ' + ' ' + a);
        }).owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            nav: true,
            dots: false,
        });
        jQuery('.blog-detail__slider-wrapper2').on('initialized.owl.carousel changed.owl.carousel', function (e) {
            if (!e.namespace) {
                return;
            }
            var carousel = e.relatedTarget;
            var c = carousel.relative(carousel.current()) + 1;
            var a = carousel.items().length;
            jQuery('.bslider__counter2').html('<span>' + c + '</span> / ' + ' ' + a);
        }).owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            nav: true,
            dots: false,
        });
        jQuery('.articles-slider__items').on('initialized.owl.carousel changed.owl.carousel', function (e) {
            if (!e.namespace) {
                return;
            }
            var carousel = e.relatedTarget;
            var c = carousel.relative(carousel.current()) + 1;
            var a = carousel.items().length;
            jQuery('.articles-slider__counter').html('<span>' + c + '</span> / ' + ' ' + a);
        }).owlCarousel({
            items: 3,
            loop: true,
            margin: 16,
            nav: true,
            dots: false,
            autoWidth: false,
            responsive: {
                0: {
                    items: 1,
                    dots: true,
                    autoWidth: true,
                },
                480: {
                    items: 1,
                    autoWidth: true,
                },
                768: {
                    items: 2,
                    autoWidth: true,
                },
                960: {
                    items: 3,
                    autoWidth: false,
                }
            }
        });


        var sb_count_puls = $('.pluso-counter b').val();
        if(sb_count_puls){
            $('.sb_count_puls').html(sb_count_puls)
        }else{
            $('.sb_count_puls').html(0)

        }

        /*Удаление из верстки блога "Купить Б24"*/
        if($('.Form-action').length){$('.Form-action').remove();}
    });


$(function () {
    var images = $(".blog-detail-content img");
    var link = "";
    images.each(function (indx, element) {
        link = $(element).attr("src");
        if ($(element).closest("a").length < 1) {
            $(element).wrap("<a data-fancybox href='" + link + "'></a>");
        }
    });

    $(".read-later").on('click', function (e) {
        e.preventDefault();

        var sbsForm = $(this).closest(".blog-subscribe").find(".blog-subscribe-form");
        if (sbsForm.is(":visible")) {
            sbsForm.slideUp();
        } else {
            sbsForm.slideDown();
        }
    });
});