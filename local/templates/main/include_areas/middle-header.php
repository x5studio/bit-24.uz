<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

    <hr class="my-0">
<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb", "main", array(
    "PATH" => "",
    "SITE_ID" => "s1",
    "START_FROM" => "0",
    "COMPONENT_TEMPLATE" => "main"
), false
); ?>
<?php
$APPLICATION->AddHeadString('<style>
    .breadcrumb-block, .breadcrumb {
        background-color: #fff;
    }
</style>');
