<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<div class="container">
	<div class="row row-offcanvas row-offcanvas-left">
		<?$APPLICATION->IncludeFile($arParams["SEF_FOLDER"]."sidebar.php", $arParams, array("MODE"=>"php"));?>
		<div class="col-md-9 col-xs-12 col-sm-12">
			<?
			global $TAG_ID;
			$APPLICATION->IncludeComponent(
				"blog.list",
				"",
				Array(
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"COUNT" => "8",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"DISPLAY_TOP_PAGER" => "N",
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => "blog",
					"PAGER_TITLE" => "Новости",
					"SECTION_CODE" => "",
					"TAG_ID" => $TAG_ID
				)
			);?>
		</div><!--/.col-xs-12.col-sm-9-->
	</div>
</div>
