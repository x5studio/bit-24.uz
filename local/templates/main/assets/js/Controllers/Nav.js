if (typeof define === "function" && define.amd) {
    define([], function () {
        function Nav(container) {
            var _this = this;
            this.container = $(container);
            this.li = this.container.find('.nav-sub');
            this.a = this.container.find('.nav-sub .nav-link');
            this.active = false;
            this.container.click(function (e) {
                var el = $(e.target);
                if (el.is('.nav-active .nav-link')) {
                    el.parents('.nav-active').removeClass('nav-active');
                    _this.active = false;
                    $(document).unbind('mouseup', 'keyup');
                    return false;
                } else if (el.is('.nav-link')) {

                    var item = el.parents('.nav-item').find(".nav-sub-content .j-slider1");
                    require(['Controls/Slider1'], function (Slider1) {
                        new Slider1(item);
                    });
                    _this.NavClose();
                    el.parents('.nav-item').addClass('nav-active');
                    _this.active = el;
                    $(document).bind({
                        mouseup: function (e) {
                            if ($(e.target).parents('.nav-sub-content').length === 0 && !$(e.target).is('.nav-sub-content') && !$(e.target).is('.nav-link')) {
                                _this.NavClose();
                                $(document).unbind('mouseup', 'keyup');
                            }
                        },
                        keyup: function (e) {
                            if (e.keyCode == '27') {
                                _this.NavClose();
                                $(document).unbind('mouseup', 'keyup');
                            }
                        }
                    });
                    return false;
                }
            });
        }

        Nav.prototype.NavClose = function () {
            var _this = this;
            if (_this.active) {
                _this.active.parents('.nav-item').removeClass('nav-active');
            }
        };

        return Nav;
    });
}
