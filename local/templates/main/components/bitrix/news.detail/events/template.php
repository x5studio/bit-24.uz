<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);

$PARENT = $this->__component->__parent;
$dateTimestamp = MakeTimeStamp($arResult['FIELDS']['ACTIVE_TO']);
?>

<?php if (empty($arResult)) { return; } ?>

<div class="events-single__featured">
    <div class="container">
        <div class="events-single__featured-wrapper" style="background-image: url('<?php echo $arResult['DETAIL_PICTURE']['SRC']; ?>');">
            <div class="events-single__featured-type"><?php echo $arResult['PROPERTIES']['TYPE']['VALUE']; ?></div>
            <div class="events-single__featured-title"><?php echo $arResult['NAME']; ?></div>
            <div class="events-single__featured-date">
                <div class="events-single__featured-date-num"><?php echo $arResult['DATE_DAY']; ?></div>
                <div class="events-single__featured-date-month"><?php echo $arResult['DATE_MONTH']; ?></div>
            </div>
            <div class="events-single__featured-city">Место проведения: <br/><?php echo $arResult['CITY']; ?></div>
            <div class="events-single__featured-actions">
                <?if($dateTimestamp >= time()):?><button type="button" class="events-single__featured-action-btn events-single__btn events-single__btn_pink event__feedback-modal-btn" data-form-field-title="<?php echo $arResult['NAME']; ?>" data-form-field-to="<?php echo $arResult['PROPERTIES']['EMAIL']['VALUE']; ?>">Записаться</button><?endif;?>
                <div class="events-single__featured-price"><?= ($arResult['EVENT_STATUS'] == 1) ? $arResult['PRICE'] : "Бесплатно";?></div>
            </div>
        </div>
    </div>
</div>

<div class="events-single__info">
    <div class="container">
        <div class="events-single__info-wrapper">
            <div class="events-single__info-block events-single__info-block_place">
                <div class="events-single__info-block-title">Место</div>
                <div class="events-single__info-small-text">г <?php echo $arResult['CITY']; ?>, <?php echo $arResult['PROPERTIES']['ADDRESS']['VALUE']; ?></div>
                <div class="events-single__info-tiny-text"><br/><a href="#events-single__map" class="events-single__go-to-cart-btn">Посмотреть на карте</a></div>
            </div>
            <div class="events-single__info-block events-single__info-block_length">
                <div class="events-single__info-block-title">Продолжительность</div>
                <div class="events-single__info-big-text"><?php echo $arResult['PROPERTIES']['LENGTH']['VALUE']; ?></div>
                <div class="events-single__info-tiny-text"><br/><?php echo $arResult['PROPERTIES']['BREAK_TEXT']['VALUE']; ?></div>
            </div>
            <div class="events-single__info-block events-single__info-block_start">
                <div class="events-single__info-block-title">Начало:</div>
                <div class="events-single__info-big-text"><?php echo $arResult['DATE_DATE']; ?> г <br><?php echo $arResult['DATE_TIME']; ?> </div>
            </div>
        </div>
    </div>
</div>

<div class="events-single__details">
    <div class="container">
        <div class="events-single__details-description">
            <div class="events-single__block-title">Программа</div>
            <div class="events-single__details-description-text">
                <?php echo $arResult['DETAIL_TEXT']; ?>
            </div>
        </div>
        <div class="events-single__details-cost">
            <div class="events-single__details-cost-title">Стоимость</div>
            <div class="events-single__details-cost-price">
                <div class="events-single__details-cost-price-val"><?= ($arResult['EVENT_STATUS'] == 1) ? $arResult['PRICE'] : "Бесплатно";?></div>
                <?if($dateTimestamp >= time()):?><button type="button" class="events-single__details-apply-btn events-single__btn events-single__btn_pink event__feedback-modal-btn" data-form-field-title="<?php echo $arResult['NAME']; ?>" data-form-field-to="<?php echo $arResult['PROPERTIES']['EMAIL']['VALUE']; ?>">Записаться</button><?endif;?>
            </div>
            <?/*if ($arResult['EVENT_STATUS'] == 1):?>
            <div class="events-single__details-cost-text">
                Краткий текст о способах оплаты <br/>
                данного семинара в несколько строк <br/>
                на сайте с нюансами и т.д
            </div>
            <div class="events-single__details-cost-logos">
                <img src="<?php echo $templateFolder; ?>/images/visa_logo.png"><img src="<?php echo $templateFolder; ?>/images/yandex_logo.png"><img src="<?php echo $templateFolder; ?>/images/mastercard_logo.png">
            </div>
            <?endif;*/?>
        </div>
    </div>
</div>
<div id="events-single__map" class="events-single__map">
    <div class="events-single__map-info">
        <div class="events-single__block-title">Место проведения</div>
        <div class="events-single__map-info-text">
            <?php echo $arResult['CITY']; ?>, <?php echo $arResult['PROPERTIES']['ADDRESS']['VALUE']; ?><br/>
            <b><?php echo $arResult['PROPERTIES']['PHONE']['VALUE']; ?></b>
        </div>

        <!-- <div class="events-single__map-info-alarm">При себе необходимо иметь паспорт или права</div> -->

    </div>
    <div class="events-single__map-container" data-coords='{"lon": "<?php echo $arResult['COORDS'][0]; ?>", "lat": "<?php echo $arResult['COORDS'][1]; ?>"}' data-icon_path="<?php echo $templateFolder; ?>/images/yandex_map_marker_icon.png">

    </div>
</div>

<div class="events-single__apply">
    <div class="container">
        <div class="events-single__apply-info">
            <div class="events-single__apply-info-title events-single__block-title">Записаться на мероприятие <br/>«<?php echo $arResult['NAME']; ?>»</div>
            <div class="events-single__apply-text">
                <?php echo $arResult['DATE_DATE']; ?> г, <?php echo $arResult['DATE_TIME']; ?><br/>
                г <?php echo $arResult['CITY']; ?> <?php echo $arResult['PROPERTIES']['ADDRESS']['VALUE']; ?>
            </div>
            <div class="events-single__apply-policy-text">Нажатием кнопки я принимаю условия Оферты по использованию сайта <br/>и согласен с <a href="/politika-konfidencialnosti/" target="_blank">Политикой конфиденциальности</a></div>
        </div>
        <div class="events-single__apply-form" data-event-title="<?php echo $arResult['NAME']; ?>" data-event-to="<?php echo $arResult['PROPERTIES']['EMAIL']['VALUE']; ?>">
            <div id="WIDGET_FEEDBACK_INLINE_FORM"></div>
        </div>
    </div>
</div>