<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); 
global $myTitle;
?>

<div class="events-single__wrapper">
    <?
    $APPLICATION->IncludeComponent(
        "bitrix:news.detail",
        "events",
        array(
            "IBLOCK_TYPE" => IBT_EVENTS,
            "IBLOCK_ID"	=> IB_EVENTS,
            "ELEMENT_CODE" =>$arResult['VARIABLES']['ELEMENT_CODE'],
            "CHECK_DATES" =>	"N",
            "FIELD_CODE" =>	array(
                0	=>	"ACTIVE_TO",
                1	=>	"",
            ),
            "PROPERTY_CODE"	=>	array(
                "*"
            ),
            "AJAX_MODE"	=>	"N",
            "AJAX_OPTION_SHADOW" =>	"N",
            "AJAX_OPTION_JUMP" =>	"N",
            "AJAX_OPTION_STYLE"	=>	"N",
            "AJAX_OPTION_HISTORY" =>	"N",
            "CACHE_TYPE" =>	"A",
            "CACHE_TIME" =>	"3600",
            "DISPLAY_PANEL"	=>"N",
            "SET_TITLE"	=> "Y",
            "INCLUDE_IBLOCK_INTO_CHAIN"	=> "Y",
            'ADD_ELEMENT_CHAIN' => 'Y',
            "ADD_SECTIONS_CHAIN" =>	"N",
            "ACTIVE_DATE_FORMAT" =>	"d.m.Y",
            "USE_PERMISSIONS" => "N",
        ),
        $component
    );
    ?>

    <div class="events-single__recommended">
        <div class="container">
            <div class="events-single__block-title">Рекомендуемые мероприятия</div>
            <?php
            global $arEventsFilter;
            $arEventsFilter = array(
                '>DATE_ACTIVE_TO' => new DateTime(),
                'PROPERTY_FEATURED_VALUE' => 'Да',
                '!CODE' => $arResult['VARIABLES']['ELEMENT_CODE'],
            );
            $APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "events",
                Array(
                    "CACHE_TIME" => "86400",
                    "CACHE_TYPE" => "A",
                    "NEWS_COUNT" => 3,
                    'FIELD_CODE' => array('NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DATE_ACTIVE_TO', 'DETAIL_PAGE_URL'),
                    'PROPERTY_CODE' => array('TYPE'),
                    "DISPLAY_BOTTOM_PAGER" => "N",
                    "DISPLAY_TOP_PAGER" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => "blog",
                    "PAGER_TITLE" => "Мероприятия",
                    "IBLOCK_ID" => IB_EVENTS,
                    'USE_FILTER' => 'Y',
                    'FILTER_NAME' => 'arEventsFilter',
                    'DISPLAY_SUBSCRIBE_FORM' => 'N',
                    "INCLUDE_IBLOCK_INTO_CHAIN"	=> "N",
                    'HIDE_APPLY_FORM' => 'Y',
                    'ACTIVE_DATE_FORMAT' => 'd F Y, D. H:i',
                ),
                $component
            );
            ?>
        </div>
    </div>

</div>

<?
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
    "WEB_FORM_ID" => FORM_EVENTS_APPLY,	// ID веб-формы
    "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
    "COMPANY_IBLOCK_ID" => IB_CONTACTS,
    "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
    "EMAIL_QUESTION_CODE" => "EMAIL",
    "CITY_QUESTION_CODE" => "CITY",
    "OFFICE_QUESTION_CODE" => "OFFICE",
    "TO_QUESTION_RESIPIENTS" => "",
    "EMAIL_ONLY_FROM_RESIPIENTS" => "",
    "PRODUCT_QUESTION_CODE" => "PRODUCT",
    "TO_QUESTION_CODE" => "TO",
    "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
    "USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
    "SEF_MODE" => "N",	// Включить поддержку ЧПУ
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CACHE_TIME" => "3600",	// Время кеширования (сек.)
    "LIST_URL" => "",	// Страница со списком результатов
    "EDIT_URL" => "",	// Страница редактирования результата
    "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
    "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
    "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
    "VARIABLE_ALIASES" => array(
        "WEB_FORM_ID" => "WEB_FORM_ID",
        "RESULT_ID" => "RESULT_ID",
    ),
    "ALERT_ADD_SHARE" => "N",
    "HIDE_PRIVACY_POLICE" => "Y",
    "BTN_CALL_FORM" => ".event__feedback-modal-btn",
    "HIDE_FIELDS" => array(
        0 => "CITY",
        1 => "OFFICE",
        2 => "TYPE",
    ),
    "COMPONENT_MARKER" => "events_apply_form",
    "SHOW_FORM_DESCRIPTION" => "N",
    "MESS" => array(
        "THANK_YOU" => "Спасибо, Ваша заявка принята.",
        "WAIT_CALL" => "Мы свяжемся с Вами в течение 2-х часов.",
        "TITLE_FORM" => "Заказать услугу",
    )
),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
);

$APPLICATION->SetTitle($myTitle);
?>
