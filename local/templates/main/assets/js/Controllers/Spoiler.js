if (typeof define === "function" && define.amd) {
    define(['Controls/Responsive'], function (obResponsive) {
      function Spoiler(container) {
          var _this = this;
          this.container = $(container);
          this.control = this.container.find('.j-spoiler-control').eq(0);
          this.content = this.container.find('.j-spoiler-content').eq(0);
          this.speedAnimate = null;
          this.accordeonMode = false;
          this.desktopDisabled = (this.container.data('desktop-disabled') === "Y") ? true : false;

          this.spoilerActivate = function () {
              if(_this.accordeonMode){
                  _this.container.parent('.j-spoiler-root').find('.j-spoiler').each(function(){
                      if( !$(this).is(_this.container) ){
                          if($(this).find('.j-spoiler-control').hasClass('dotted-active')){
                              $(this).find('.j-spoiler-control').removeClass('dotted-active')
                          }

                          if($(this).hasClass('open')){
                              if(_this.speedAnimate > 0) {
                                  $(this).find('.j-spoiler-content').slideUp(_this.speedAnimate);
                              } else {
                                  $(this).find('.j-spoiler-content').hide();
                              }
                              $(this).removeClass('open');
                          }
                      }
                  });
              }

              _this.control.toggleClass('dotted-active');
              if(_this.speedAnimate > 0) {
                      if(_this.content.is(':hidden')) {
                          _this.content.slideDown(_this.speedAnimate);
                      } else {
                          _this.content.slideUp(_this.speedAnimate);
                      }
              } else {
                  _this.content.toggle();
              }

              _this.container.toggleClass('open');
              return false;
          }

          if($.isNumeric(this.container.data('animate'))) {
              this.speedAnimate = parseInt(this.container.data('animate'),10);
          }

          if(this.container.data('accordeon') === 'Y') {
              this.accordeonMode = true;
          }

          if (!this.control.hasClass('dotted-active')) {
              this.content.hide();
          }

          if(!this.desktopDisabled) {
              _this.control.on("click", function () {
                  _this.spoilerActivate();
              });
          } else {
              obResponsive.on(['xsmall', 'small'], 'in', function () {
                  _this.content.css("display", "none");
                  _this.control.on("click", function () {
                      _this.spoilerActivate();
                  });
              });
              obResponsive.on(['medium', 'large'], 'in', function () {
                  _this.control.off("click");
                  _this.content.css("display", "block");
              });
          }
      }
    return Spoiler;
  });
}
