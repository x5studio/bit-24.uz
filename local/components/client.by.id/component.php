<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
if (is_array($arParams["EXPERTISE_ID"]))
{
	$arParams["EXPERTISE_ID"] = array_filter($arParams["EXPERTISE_ID"]);
	if (count($arParams["EXPERTISE_ID"]) == 0)
	{
		$this->AbortResultCache();
		return;
	}
} else
{
	$arParams["EXPERTISE_ID"] = intval($arParams["EXPERTISE_ID"]);

	if ($arParams["EXPERTISE_ID"] <= 0)
	{
		$this->AbortResultCache();
		return;
	}
}


if ($this->StartResultCache())
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"=UF_EXPERTISE" => $arParams["EXPERTISE_ID"]
	);

	$arFile = array();

	$rsSection = \CIBlockSection::GetList(
		array("SORT" => "ASC", "NAME" => "ASC"),
		$arFilter,
		false,
		array("ID", "NAME", "PICTURE", "SECTION_PAGE_URL", "DESCRIPTION")
	);
	while ($arSection = $rsSection->GetNext())
	{

		$arFile[] = $arSection["PICTURE"];

		$arResult["ITEMS"][] = $arSection;
	}

	$arResult["IMAGES"] = Helper::getImages($arFile);

	$this->IncludeComponentTemplate();
}