<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
    $date = strtotime(date('d:m:Y h:i:s'));
?>
<? if (!empty($arResult["ITEMS"])): ?>
	<section class="Main-Slider">
<!--		<div id="carousel-vertical" class="carousel slide vertical" data-interval="3000" data-ride="carousel"> -->
		<div id="carousel-vertical" class="carousel slide carousel-fade">

			<!-- Indicators -->
			<ol class="carousel-indicators hidden-xs">
				<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
					<li data-target="#carousel-vertical" data-slide-to="<?= $cell ?>"<?
					if (!$cell) echo ' class="active"'; ?>><span></span></li>
				<? endforeach; ?>
			</ol>
			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox" style="background:#000000;">
				<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
                    <?php
                        $date = strtotime(date('d.m.Y h:i:s'));
                        $from = strtotime($arItem['DATE_ACTIVE_FROM']);
                        $to = strtotime($arItem['DATE_ACTIVE_TO']);
                    ?>
                    <?if($date>$from && $date<$to):?>
                        <a target="_blank" href="<?= $arItem["CODE"] ?>" class="item<? if (!$cell) echo ' active'; ?>">
                            <div class="banner<?= ($cell + 1) ?>">
								<div class="fill <?if($arItem['PROPERTY_CLASS_ADD_VALUE']){echo $arItem['PROPERTY_CLASS_ADD_VALUE'];}?>" style="background-image:url('<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>');"></div>
                                <div class="container">
                                    <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                        <div class="carousel-caption main">
                                            <div class="slide-info pull-left col-xs-7">
                                                <!--<h2><?/*= $arItem["~NAME"] */?></h2>-->
                                                <?/*= $arItem["PREVIEW_TEXT"] */?>
                                                <? if (strlen($arItem["CODE"]) > 0): ?>
                                                    <!--<div class="btn btn-more">
                                                        <a href="<?/*= $arItem["CODE"] */?>">Подробнее</a><span>
                                                        <img src="<?/*= SITE_TEMPLATE_PATH */?>/assets/img/btnbg.png" alt="..."></span>
                                                    </div>-->
                                                <? endif; ?>
                                            </div>
                                            <div class="productimg col-xs-5">
                                                <img src="<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>" alt="<?= $arItem["~NAME"] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    <?endif?>
				<? endforeach; ?>
			</div>
			<!-- Controls -->
			<!--<div class="container">
				<div class="row">
					<div class="col-md-offset-8 col-lg-2 hidden-sm col-xs-2"></div>
					<div class="col-md-2 col-lg-2 hidden-sm col-xs-10">
						<div class="clcontrols">
							<a class="hidden-sm left carousel-control" href="#carousel-vertical" role="button" data-slide="prev">
								<i class="hidden-sm hidden-md hidden-lg fa fa-angle-left" aria-hidden="true"></i>
								<img class="hidden-xs" src="<?/*= SITE_TEMPLATE_PATH */?>/assets/img/up.png" alt="...">
								<span class="sr-only">Previous</span>
							</a>
							<div class="num hidden-sm hidden-xs">01</div>
							<a class="hidden-sm right carousel-control" href="#carousel-vertical" role="button" data-slide="next">
								<i class="hidden-sm hidden-md hidden-lg fa fa-angle-right" aria-hidden="true"></i>
								<img class="hidden-xs" src="<?/*= SITE_TEMPLATE_PATH */?>/assets/img/down.png" alt="...">
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>-->
		</div>
	</section>







<? endif; ?>