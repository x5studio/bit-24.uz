<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult["ITEMS"])): ?>
	<section class="four-crm-block">
		<div class="container">
			<div class="row">
				<h2>Внедрение Битрикс24 CRM <span>за простых 4 шага</span></h2>
                <?$i=1;?>
				<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<div class="block<?= ($cell != 3) ? '2' : '4'; ?> block-sm-<?=$i;?>">
							<h3><?= $arItem["NAME"] ?></h3>
							<h4>0<?= ($cell + 1) ?></h4>
							<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
						</div>
					</div>
                    <?$i++;?>
				<? endforeach; ?>
			</div>
		</div>
	</section>
	<? foreach ($arResult["ITEMS"] as $cell => $arItem):
		$class = 'step-crm';
		if ($cell == 1) $class = 'step2';
		elseif ($cell == 3) $class = 'step4';
		?>
		<section class="<?= $class ?>">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="material<?= $cell ? '2' : '' ?>">
							<div class="mattop">
								<h2>Шаг <?= ($cell + 1) ?> — <?= $arItem["NAME"] ?></h2>
								<h3><?= $arItem["PROPERTY_SUBNAME_VALUE"] ?></h3>
							</div>
							<div class="include">
								<h3>Что входит:</h3>
								<div class="row">
									<?= $arItem["PREVIEW_TEXT"] ?>
								</div>
							</div>
							<div class="step-bottom step-bottom_has-icons">
								<div class="row">
									<div class="col-md-4 col-sm-12 col-xs-12 block-f">
										<h3>Формат:</h3>
										<p><?= $arItem["PROPERTY_FORMAT_VALUE"] ?></p>
										<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/impl/com.png" class="img-responsive">
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12 block-c">
										<h3>Цена:</h3>
										<?= $arItem["~PROPERTY_PRICE_VALUE"]["TEXT"] ?>
										<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/impl/wallet.png" class="img-responsive">
									</div>
									<div class="col-md-4 col-sm-12 col-xs-12 block-s">
										<h3>Срок:</h3>
										<h4><?= $arItem["PROPERTY_TIME_VALUE"] ?></h4>
										<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/impl/cal.png" class="img-responsive">
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
	<? endforeach; ?>
<? endif; ?>