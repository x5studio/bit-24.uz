/**
 * Created by psbezrukov on 22.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(["Controls/Contact/ContactManager"], function (ContactManager) {
        var ContactManagerSingle = (function () {
            var instance;

            function ConstructSingletone() {
                if (instance) {
                    return instance;
                }
                if (this && this.constructor === ConstructSingletone) {
                    instance = this;
                } else {
                    return new ConstructSingletone();
                }

                ContactManager.apply(this, arguments);
            }

            extend(ConstructSingletone, ContactManager);

            return ConstructSingletone;
        }());

        return ContactManagerSingle;
    });
}