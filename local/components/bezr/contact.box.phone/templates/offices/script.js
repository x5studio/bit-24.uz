function ChangeHref(p){
    this.root = $(p);
    arLinks = new Array();
    arUrls = new Array();
    arPhones = new Array();
    arPhonesStr = new Array();
    arNameStr = new Array();
    this.root.find('td').each(function(){
        var a = $(this).find('.box-office-link');
        var phoneStr = $(this).find('.box-office-phone');
        var nameStr = $(this).find('.box-office-name');

        if(typeof a.attr('href') != 'undefined'){
            var url = a.attr('href');
        }
        else{
            var url = $('.a-name').attr('href');
        }

        var phone = phoneStr.text();
        arLinks.push(a);
        arUrls.push(url);
        arPhones.push(phone);
        arPhonesStr.push(phoneStr);
        arNameStr.push(nameStr);
    });

    var pushPhone, pushUrl;

    this.PushPhone = function(){
        for (var a in arLinks) {
            pushPhone = 'tel:' + arPhones[a];
            arPhonesStr[a].wrapAll('<a href="' + pushPhone + '" class="a-phone"></a>');
            arNameStr[a].wrapAll('<a href="' + arUrls[a] + '" class="a-name"></a>');
            arLinks[a].replaceWith(function(index, oldHTML){
                return $('<span class="box-office-link">').html(oldHTML);
            });
        }
    }

    this.PushUrl = function(){
        for (var k in arLinks) {
            pushUrl = arUrls[k];
            var phoneStr = arPhonesStr[k];
            var nameStr = arNameStr[k];
            if(phoneStr.parent().hasClass('a-phone')){
                phoneStr.unwrap();
            }
            if(nameStr.parent().hasClass('a-name')){
                nameStr.unwrap();
            }
            arLinks[k].replaceWith(function(index, oldHTML){
                return $('<a href="' + pushUrl + '" class="box-office-link">').html(oldHTML);
            });
        }
    }
}

function SyncOfficesItems(p)
{
	this.root = $(p.root);
	this.selects = $('.soi-select');

	this.modelSort = [];

	this.init()
}
SyncOfficesItems.prototype.init = function()
{
	var o = this;

	o.root.find('.box-offices-item').each(function(){
		var item = $(this);
		o.modelSort.push(item.data('itemId'));
	});

	var parentId = $('body').data('cityId');
	o.selects.each(function(){
		var obItem = new SyncOfficesItem({
			root: this,
			parentId: parentId
		});
		obItem.sortItemsByArray(o.modelSort);
	});
}

function SyncOfficesItem(p)
{
	this.p = p;
	this.root = $(p.root);
}
SyncOfficesItem.prototype.findItems = function()
{
	var o = this;
	return o.root.find('.cusel-scroll-pane span[parent-id=' + o.p.parentId + ']');
}

SyncOfficesItem.prototype.sortItemsByArray = function(modelSort)
{
	var o = this;
	var items = o.findItems();
	items.sort(function(a, b){
		var cA = $(a).attr('val');
		var cB = $(b).attr('val');
		var miA = modelSort.indexOf(parseInt(cA));
		var miB = modelSort.indexOf(parseInt(cB));

		if (miA < miB) {
			return -1;
		} else if(miA > miB) {
			return 1;
		}
		return 0;
	});
	var parent = items.parent();
	items.detach().appendTo(parent);
}

require(['Controllers/Loc'], function(Loc) {
    var l = Loc.getWithPrefix('PB_CONTACTS_CT_CB_OFF_JS_');

    function DoubleOffices(p) {
        var open = 'box-offices-opened';
        var text = $(p.button, p.root).text();
        p.close.css({opacity: 0, display: 'none'});
        setTimeout(function () {
            $(p.button, p.root).click(function () {
                var b = $(this);
                if (p.root.hasClass(open)) {
                    $('.box-content .contacts-bg').show();
                    $('.box-offices-control').hide();
                    p.close.hide().css({opacity: 0});
                    $('.box-content', p.root).css('min-height', 140);
                    p.root.removeClass(open);
                    $('.box-offices-control span').text(window.officetext);
                } else {
                    setTimeout(function () {
                        p.close.show().animate({opacity: 1}, 500);
                    }, 250);
                    var size = $('.box-offices-item', p.root).size();
                    size = (size * 75) / 2;
                    size = size > 140 ? size : 140;
                    $('.box-content', p.root).css('min-height', size);
                    setTimeout(function () {
                        p.root.addClass(open);
                        window.officetext = text;
                        $(b).text(l('SLIDE_DOWN'));
                    }, 200);
                }
            });
        }, 100)
    }

    $(function() {
        var initializeOfficesCtrl = function ()
        {
            $('.j-box-offices').on('jBoxOffices.init', function(){
                if($('.box-offices-item').size() > 4) {
                    new DoubleOffices({
                        root: $('.double-office'),
                        button: $('.box-offices-control span, .box-content > .close'),
                        close: $('.box-content > .close')
                    });
                    $('.box-content .contacts-bg .phone-bg').click(function() {
                        track([l('ACTIONS'), l('CLICK'), l('MOSCOW_CBLOCK'), 1]);
                        $('.box-content .contacts-bg').hide();
                        $('.box-offices-control span').show().trigger('click');
                        $('.box-offices-control').show();
                    });
                }

                $('.office-email').click(function(e) {
                    e.preventDefault();
                    document.location = "mailto:" + $(this).html();
                });
            });
            var cuSelDeferred = $.Deferred();
            if ($(document).data('jSelectInit') == true) {
                cuSelDeferred.resolve();
            } else {
                $(document).on('jSelect.init', function () {
                    cuSelDeferred.resolve();
                });
            }
            cuSelDeferred.done(function () {
                $('.j-sync-offices-items').each(function(){
                    var item = $(this);
                    new SyncOfficesItems({
                        root: item
                    });
                });
            });

            var boxOfficeObj=$('.box-offices-item');
            var count_offices=boxOfficeObj.size();
            var select_object="span.box-office-name";
            if(count_offices == 0) { select_object = 'span.box-office-name';}
            if(count_offices == 1) { select_object = 'li.office-name a'; }
            if(count_offices == 2) { select_object = 'div.office-name'; }
            if(count_offices >= 3 && count_offices < 10) { select_object = 'span:not(.box-office-phone)'; }
            var officesAr=new Array();
            $(boxOfficeObj).find(select_object).each(function(){ officesAr.push($(this).text()); });
            track([l('CBLOCK'), l('OFFICE_SHOW'), officesAr.join("|"), {'nonInteraction': 1}]);
        };

        compositeFinished(function(){
            initializeOfficesCtrl();
        });
    });
});

require(['main'], function() {
	var boxOfficesContainer = $('#contact-right-box-container');
	var citySelectContainer = $('.header-city-select');
	var mobileHeaderContainer = $('.mobile-header-wrapper');
	var boxOfficesOrigPos = $('.offices-box .boxes').add('.content>.width>.col-r .boxes');
	var citySelectOrigPos = $('.header .width');
    var tableOfficeCol = $('.table-office-col');
   	require(['Controls/Responsive'], function(obResponsive) {
		obResponsive.on(['xsmall', 'small'], 'in', function () {
			if(!boxOfficesContainer.prev().hasClass('mobile-header-wrapper')) {
				boxOfficesContainer.insertAfter(mobileHeaderContainer);
				citySelectContainer.insertAfter(boxOfficesContainer);
			}
            new ChangeHref(tableOfficeCol).PushPhone();
		});
		obResponsive.on(['medium', 'large'], 'in', function () {
			if(!boxOfficesContainer.parent().hasClass('boxes')) {
				boxOfficesContainer.prependTo(boxOfficesOrigPos);
				citySelectContainer.prependTo(citySelectOrigPos);
			}
            new ChangeHref(tableOfficeCol).PushUrl();
		});
	});
});