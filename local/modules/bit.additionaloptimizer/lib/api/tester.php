<?
namespace Bit\LocalityFormatter\Api;

use \Bitrix\Main\Engine\Controller;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Localization\Loc;

class Tester extends Controller
{

    public function formatPhoneAction(){

        $country = '';
        $request = $this->getRequest();
        $phone = $request->get('phone');
        $country = $request->get('country');

        
		$options = Option::getForModule('bit.localityformatter');

        $html = '<div style="text-align:left;">';
        $parsedResult = array();

        

        $parsedPhone = \Bit\LocalityFormatter\PhoneFormatter::parsePhone($phone, $country);



        if(!!$options['primary_countries'] && !$parsedPhone->isValid()) {

            $countries = array();

            $countries = array_map('trim', array_filter(explode(PHP_EOL, $options['primary_countries'])));

            $countriesCount = 0;

            

            while(!$parsedPhone->isValid() && isset($countries[$countriesCount])){

                $parsedPhone = \Bit\LocalityFormatter\PhoneFormatter::parsePhone($phone, $countries[$countriesCount]);

                $countriesCount ++;

            }

        }


        $parsedResult['Valid'] = $parsedPhone->isValid() ? 'Yes' : 'No' ;

        if($parsedPhone->isValid()) {

            $countryFormatterInstanse = \Bit\LocalityFormatter\CountryFormatter::getInstance();

            $full_country = '';

            $countryResult = $countryFormatterInstanse->getCountry($parsedPhone->getCountry());

            if($countryResult) $full_country = $countryResult['name'] . " (".$countryResult['alpha2'].")";

            $parsedResult['Country'] = $full_country ;

        }
            

        $parsedResult['Number type'] = $parsedPhone->getNumberType() ;
        $parsedResult['National number'] = $parsedPhone->getNationalNumber() ;
        $parsedResult['International number'] = $parsedPhone->format(\Bitrix\Main\PhoneNumber\Format::INTERNATIONAL, true) ;
        $parsedResult['National prefix'] = $parsedPhone->getNationalPrefix();

        foreach( $parsedResult as $key => $value){
            $html .= $key. ': '. $value.'<br>';
        }

        $html .= '</div>';




        return ['html' => $html];

    }

}
