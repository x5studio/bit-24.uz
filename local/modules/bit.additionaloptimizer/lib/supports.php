<?

namespace Bit\AdditionalOptimizer;

use Bitrix\Main\Config\Option;

class Supports{


	static function checkWebP(){
    
        $result = false;
        
        $server = \Bitrix\Main\Context::getCurrent()->getServer();

        $http_accept = $server->get('HTTP_ACCEPT');

        if($http_accept) {

            
            if(strpos( $_SERVER['HTTP_ACCEPT'], 'webp' ) !== false) 
            {
                // браузер явно сообщает о поддержке
                $result = true;
            } 
            elseif (strpos( $_SERVER['HTTP_ACCEPT'], '*/*' ) !== false) 
            {
                // браузер сообщает о том что попытается справиться со всем
                $result = true;
            }


        }


        return $result;

	}


}
