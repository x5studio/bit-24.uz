<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	Bitrix\Main\Page\Asset;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

Asset::getInstance()->addString('<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>');

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
if (!isset($arParams["PARENT_SECTION"]))
	$arParams["PARENT_SECTION"] = 91;

$arParams["GEO_CURRENT"] = $_SESSION["REGION"]["ID"];

if ($this->StartResultCache(false, array($arNavigation)))
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y"
	);

	$rsSection = \CIBlockSection::GetList(
		array("SORT" => "ASC"),
		array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "SECTION_ID" => $arParams["PARENT_SECTION"]),
		false,
		array("ID", "NAME")
	);

	while ($arSection = $rsSection->GetNext())
	{
		$arResult["SECTION"][$arSection["ID"]] = $arSection;
	}

	$rsElement = \CIBlockElement::GetList(
		array("SORT" => "ASC"),
		array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "SECTION_ID" => array_keys($arResult["SECTION"])),
		false,
		false,
		array("ID", "NAME", "IBLOCK_SECTION_ID", "PROPERTY_phone", "PROPERTY_MAP", "PROPERTY_ADRESS", "PROPERTY_email")
	);
	while ($arElement = $rsElement->GetNext())
	{
		list($arElement["LAT"], $arElement["LON"]) = explode(",", $arElement["PROPERTY_MAP_VALUE"]);
		$arResult["SECTION"][$arElement["IBLOCK_SECTION_ID"]]["ITEMS"][] = $arElement;
	}
	$arResult["CURRENT"] = intval($arParams["GEO_CURRENT"]);
	if (empty($arResult["SECTION"][$arResult["CURRENT"]]))
	{
		reset($arResult["SECTION"]);
		$arResult["CURRENT"] = current($arResult["SECTION"])["ID"];
	}

	$this->IncludeComponentTemplate();
}