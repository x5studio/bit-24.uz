<?php

if(!empty($arResult['DETAIL_PICTURE'])) {
$arResult['DETAIL_PICTURE'] = CFile::ResizeImageGet(
    $arResult['DETAIL_PICTURE'],
    array("width" => 416, "height" => 466),
    BX_RESIZE_IMAGE_EXACT
);
}elseif(!empty($arResult['PREVIEW_PICTURE'])) {
    $arResult['PREVIEW_PICTURE'] = CFile::ResizeImageGet(
        $arResult['PREVIEW_PICTURE'],
        array("width" => 416, "height" => 466),
        BX_RESIZE_IMAGE_EXACT
    );
}
