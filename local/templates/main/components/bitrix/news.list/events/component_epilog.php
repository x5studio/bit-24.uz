<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Page\Asset::getInstance()->addString('<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>');
?>
<?php if (!empty($arResult['ELEMENTS']) && $arParams['DISPLAY_SUBSCRIBE_FORM'] == 'Y'): ?>
    <div class="j-deffered">
        <div to="WIDGET_SUBSCRIBE_INLINE_FORM">
            <?
            $APPLICATION->IncludeComponent(
                "bezr:form.result.new.befsend",
                "inline",
                array(
                    "WEB_FORM_ID" => FORM_EVENTS_SUBSCRIBE,	// ID веб-формы
                    "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
                    "COMPANY_IBLOCK_ID" => IB_CONTACTS,
                    "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
                    "EMAIL_QUESTION_CODE" => "EMAIL",
                    "CITY_QUESTION_CODE" => "CITY",
                    "OFFICE_QUESTION_CODE" => "OFFICE",
                    "TO_QUESTION_RESIPIENTS" => "",
                    "EMAIL_ONLY_FROM_RESIPIENTS" => "",
                    "PRODUCT_QUESTION_CODE" => "PRODUCT",
                    "TO_QUESTION_CODE" => "TO",
                    "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
                    "USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
                    "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                    "CACHE_TYPE" => "A",	// Тип кеширования
                    "CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "LIST_URL" => "",	// Страница со списком результатов
                    "EDIT_URL" => "",	// Страница редактирования результата
                    "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
                    "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
                    "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
                    "SHOW_TITLE" => "N",
                    "PLACEHOLDER_MODE" => 'Y',
                    "VARIABLE_ALIASES" => array(
                        "WEB_FORM_ID" => "WEB_FORM_ID",
                        "RESULT_ID" => "RESULT_ID",
                    ),
                    "ALERT_ADD_SHARE" => "N",
                    "HIDE_PRIVACY_POLICE" => "N",
                    "HIDE_FIELDS" => array(
                        0 => "CITY",
                        1 => "OFFICE",
                    ),
                    "COMPONENT_MARKER" => "subscribe-inline",
                    "SHOW_FORM_DESCRIPTION" => "N",
                    "MESS" => array(
                        "THANK_YOU" => "Спасибо за подписку!",
                        "WAIT_CALL" => "",
                        "TITLE_FORM" => "",
                        "SUBMIT_BTN" => "Подписаться"
                    )
                ),
                false,
                array(
                    "HIDE_ICONS" => "Y"
                )
            );
            ?>
        </div>
    </div>
<?php endif; ?>

<?php if (!empty($arResult['ITEMS']) && (empty($arParams['HIDE_APPLY_FORM']) || $arParams['HIDE_APPLY_FORM'] != 'Y')): ?>
    <div class="j-deffered">
        <div to="WIDGET_APPLY_FORM">
        <?
        $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
            "WEB_FORM_ID" => FORM_EVENTS_APPLY,	// ID веб-формы
            "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
            "COMPANY_IBLOCK_ID" => IB_CONTACTS,
            "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
            "EMAIL_QUESTION_CODE" => "EMAIL",
            "CITY_QUESTION_CODE" => "CITY",
            "OFFICE_QUESTION_CODE" => "OFFICE",
            "TO_QUESTION_RESIPIENTS" => "",
            "EMAIL_ONLY_FROM_RESIPIENTS" => "",
            "PRODUCT_QUESTION_CODE" => "PRODUCT",
            "TO_QUESTION_CODE" => "TO",
            "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
            "USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
            "SEF_MODE" => "N",	// Включить поддержку ЧПУ
            "CACHE_TYPE" => "A",	// Тип кеширования
            "CACHE_TIME" => "3600",	// Время кеширования (сек.)
            "LIST_URL" => "",	// Страница со списком результатов
            "EDIT_URL" => "",	// Страница редактирования результата
            "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
            "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
            "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
            "VARIABLE_ALIASES" => array(
                "WEB_FORM_ID" => "WEB_FORM_ID",
                "RESULT_ID" => "RESULT_ID",
            ),
            "ALERT_ADD_SHARE" => "N",
            "HIDE_PRIVACY_POLICE" => "Y",
            "BTN_CALL_FORM" => ".event__feedback-modal-btn",
            "HIDE_FIELDS" => array(
                0 => "CITY",
                1 => "OFFICE",
                2 => "TYPE",
            ),
            "COMPONENT_MARKER" => "events_apply_form",
            "SHOW_FORM_DESCRIPTION" => "N",
            "MESS" => array(
                "THANK_YOU" => "Спасибо, Ваша заявка принята.",
                "WAIT_CALL" => "Мы свяжемся с Вами в течение 2-х часов.",
                "TITLE_FORM" => "Заказать услугу",
            )
        ),
            false,
            array(
                "HIDE_ICONS" => "Y"
            )
        );
        ?>
        </div>
    </div>
<?php endif; ?>

<?php


if (!empty($arParams['DISPLAY_EVENTS_FILTER']) && $arParams['DISPLAY_EVENTS_FILTER'] == 'Y') {
    $cacheTime = 3600000;
    $cacheId = 'events_list_page_filter';
    $cacheDir = 'events_list_filter';
    $cache = \Bitrix\Main\Data\Cache::createInstance();

    if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
        $vars = $cache->getVars();
        $arResult['EVENTS_FILTER'] = $vars['eventsFilter'];
    } elseif ($cache->startDataCache()) {
        $arResult['EVENTS_FILTER'] = array();

        // Типы мероприятий
        $arTypes = array();
        $resTypes = CIBlockPropertyEnum::GetList(
            array(
                "SORT" => "ASC",
                "VALUE" => "ASC"
            ),
            array(
                'CODE' => 'TYPE',
                'IBLOCK_ID' => IB_EVENTS,
            )
        );
        while ($arType = $resTypes->Fetch()) {
            $arTypes[] = array(
                'NAME' => $arType['VALUE'],
                'ID' => $arType['ID'],
            );
        }
        $arResult['EVENTS_FILTER']['TYPES'] = $arTypes;

        // Города
        $arCities = array();
        $contactManagerObj = \PB\Contacts\ContactManager::getInstance();
        $resCities = $contactManagerObj->getCityList();
        $arResult['EVENTS_FILTER']['CITIES'] = $resCities;

        $cache->endDataCache(
            array(
                'eventsFilter' => $arResult['EVENTS_FILTER'],
            )
        );
    }

    // Даты мероприятий
    $arEventsFilter = array(
        'IBLOCK_ID' => IB_EVENTS,
        '>DATE_ACTIVE_TO' => new DateTime(),
    );

    if (!empty($_REQUEST['type'])) {
        if ($_REQUEST['type'] == 'free') {
            $arEventsFilter['PROPERTY_PRICE'] = false;
        } else {
            $arEventsFilter['PROPERTY_TYPE'] = $_REQUEST['type'];
        }
    }

    if (!empty($_REQUEST['city'])) {
        $arEventsFilter = \PB\Main\FilterHelper::make(
            $arEventsFilter,
            array('CITY' => $_REQUEST['city'])
        );
    }

    $arEventsDates = array();
    $resEvents = CIblockElement::GetList(
        array(),
        $arEventsFilter,
        false,
        false,
        array(
            'IBLOCK_ID',
            'ID',
            'DATE_ACTIVE_TO'
        )
    );
    while ($arEvent = $resEvents->Fetch()) {
        $arEventsDates[] = FormatDate('d.m.Y', MakeTimeStamp($arEvent['DATE_ACTIVE_TO']));
    }
    $arResult['EVENTS_FILTER']['DATES'] = $arEventsDates;

    $arResult['EVENTS_FILTER_VALUES'] = array(
        'type' => !empty($_REQUEST['type']) ? $_REQUEST['type'] : '',
        'city' => !empty($_REQUEST['city']) ? $_REQUEST['city'] : '',
        'date' => !empty($_REQUEST['date']) ? $_REQUEST['date'] : '',
		'price' => !empty($_REQUEST['price']) ? $_REQUEST['price'] : '',
    );
    ?>
    <div class="j-deffered">
        <div to="WIDGET_EVENTS_FILTER" class="events-list__filter" <?php foreach($arResult['EVENTS_FILTER_VALUES'] as $key => $value) { echo $value !== '' ? " data-$key=\"$value\"" : ''; } ?>>
            <div class="events-list__filter-type">
                <?php foreach ($arResult['EVENTS_FILTER']['TYPES'] as $arType): ?>
                    <a href="#" class="events-list__filter-type-btn events-type-btn<?php if($arResult['EVENTS_FILTER_VALUES']['type'] == $arType['ID']) { echo ' events-list__filter-type-btn_active'; } ?>" data-type="<?php echo $arType['ID']; ?>"><?php echo $arType['NAME']; ?></a>
                <?php endforeach; ?>
                <a href="#" class="events-list__filter-type-btn events-price-btn <?php if($arResult['EVENTS_FILTER_VALUES']['price'] == 'Бесплатно') { echo ' events-list__filter-type-btn_active'; } ?>" data-price="<?if($arResult['EVENTS_FILTER_VALUES']['price'] != 'Бесплатно'):?>Бесплатно<?endif;?>">Бесплатно</a>
            </div>
            <div class="events-list__filter-city">
                <select>
                    <option value="">Выберите город</option>
                    <?php foreach ($arResult['EVENTS_FILTER']['CITIES'] as $arCity): ?>
                        <option value="<?php echo $arCity['ID']; ?>"<?php if ($arResult['EVENTS_FILTER_VALUES']['city'] == $arCity['ID']) { echo ' selected'; } ?>><?php echo $arCity['NAME']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="events-list__filter-date" data-active-dates='<?php echo json_encode($arResult['EVENTS_FILTER']['DATES'], 256); ?>'>
                <button type="button" class="events-list__filter-date-btn"></button>
                <input type="text" name="event_date" class="events-list__filter-date-input">
            </div>
        </div>
    </div>
    <?php
}
