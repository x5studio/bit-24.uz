<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION, $USER;
use Bitrix\Main\Page\Asset;

global $obOfficeFilter, $obCityFilter;

if (CSite::InDir('/lp/bitrix24-krd/')){
	$_SESSION['REGION']['ID'] = 158;
	$_SESSION["REGION"]["CODE"] = 'krasnodar';
}

CUtil::InitJSCore();
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/template_scripts.js", true);

$contMng = ContactManager::getInstance();
$arOfficeList = $contMng->getOfficeList($obOfficeFilter, Array('IBLOCK_SECTION_ID' => $_SESSION['REGION']['ID']));

if (count($arOfficeList) == 0)
{
	$arOfficeList = $contMng->getOfficeList($obOfficeFilter, Array('CODE' => 'moskva'));
}
$cities = array_shift($arOfficeList);
$emailCity = $cities["PROPERTIES"]["email"]["VALUE"];
if (defined("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE'])) && constant("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE'])) != '')
{
	$emailCity .= ", " . constant("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE']));
}

$asset = Asset::getInstance();
$asset->addString('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
$asset->addString('<meta name="viewport" content="width=device-width, initial-scale=1">');
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/css/bootstrap.min.css");
$asset->addCss(MAIN_TEMPLATE_PATH . "/assets/js/vendor/font-awesome/css/font-awesome.min.css");
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/css/header.css");
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/css/footer.css");
$asset->addCss(MAIN_TEMPLATE_PATH . "/assets/js/vendor/fancybox/dist/jquery.fancybox.min.css");
$asset->addCss(MAIN_TEMPLATE_PATH . "/assets/css/nstyle.css");
$asset->addCss(MAIN_TEMPLATE_PATH . "/assets/css/nt.css");
$asset->addCss('https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500&subset=cyrillic');


$asset->addJs(MAIN_TEMPLATE_PATH . "/assets/js/vendor/jquery/dist/jquery.min.js");
$asset->addJs(MAIN_TEMPLATE_PATH . "/assets/js/vendor/jquery-animateNumber/jquery.animateNumber.min.js");
$asset->addJs(MAIN_TEMPLATE_PATH . "/assets/js/bootstrap.min.js");

$isMainPage = ($APPLICATION->GetCurDir() == '/');
?>
<!DOCTYPE html>
	<html lang="ru">
	<head>
		<? $APPLICATION->ShowHead() ?>
		<title><? $APPLICATION->ShowTitle() ?></title>
        <?$APPLICATION->IncludeFile('include_areas/pos_head.php');?>
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        <? $APPLICATION->IncludeFile('include_areas/requirejs.php') ?>

    </head>
<body>
<? $APPLICATION->ShowPanel(); ?>
	<!-- Top-block -->
<div class="n_scroll_block">
<?if (!CSite::InDir('/lp/akcia/')): ?>
	<section class="logo-block">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12 logo">
					<a href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/logo.png" alt="logo"></a>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6 addr">
					<?
					global $arContFilter, $arContMESS;
					$arContFilter['IBLOCK_SECTION_ID'] = $_SESSION['REGION']['ID'];
					$APPLICATION->IncludeComponent("bezr:contact.box", "phone.top", Array(
						"OFFICE_IBLOCK_ID" => IB_CONTACTS,
						"DIVISION_IBLOCK_ID" => IB_DIVISION,
						"PAGE_OFFICE_IBLOCK_ID" => IB_PAGE_OFFICE,
						"FILTER_NAME" => "arContFilter",
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "86400",
						"CACHE_FILTER" => "Y",
						"CACHE_GROUPS" => "N",
						"DETAIL_URL" => "/contacts/#SECTION_CODE#/#ELEMENT_CODE#/",
						"LIMIT_VIEW_ITEMS" => "2",
						"COMPONENT_MARKER" => $_SESSION["REGION"]["ID"],
						"MESS" => $arContMESS
					),
						false,
						array(
							"HIDE_ICONS" => "Y"
						)
					);
					?>
					<br>
						<?
						global $cityFilter;
						$cityFilter = Array(
							"CITY_CODE" => $_SESSION["REGION"]["CODE"],
						);
						$HTML_TITLE = '<img
									src="' . SITE_TEMPLATE_PATH . '/assets/img/map-marker.png" alt="marker"> #NAME#';
						$APPLICATION->IncludeComponent("vitams:city.dropdown", "city_popup", Array(
							"IBLOCK_TYPE" => IBT_CONTACTS,  // Тип инфоблока
							"IBLOCK_ID" => IB_CONTACTS,  // Инфоблок
							"DROPDOWN" => $_REQUEST["dropdown"],
							"SECTION_CODE" => "",  // Код раздела
							"SECTION_URL" => "",  // URL, ведущий на страницу с содержимым раздела
							"COUNT_ELEMENTS" => "Y",  // Показывать количество элементов в разделе
							"TOP_DEPTH" => "1",  // Максимальная отображаемая глубина разделов
							"SECTION_FIELDS" => "",  // Поля разделов
							"FILTER_NAME" => "cityFilter",
							"SECTION_USER_FIELDS" => "",  // Свойства разделов
							"ADD_SECTIONS_CHAIN" => "Y",  // Включать раздел в цепочку навигации
							"CACHE_TYPE" => "A",  // Тип кеширования
							"CACHE_TIME" => "36000000",  // Время кеширования (сек.)
							"CACHE_NOTES" => "",
							"CACHE_GROUPS" => "N",  // Учитывать права доступа
							"CACHE_FILTER" => "Y",
							"LINK_IN_BASEURL" => "Y",
							"LINK_WITH_SUBDOMAIN" => "Y",
							"SEF_MODE" => "N",
							"SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
							"SEF_URL_TEMPLATES" => "",
							"HTML_LIST_ID" => "header-city-list-desktop",
							"HTML_TITLE" => $HTML_TITLE,
							"SHOW_MOSCOW" => $_SESSION["SHOW_MOSCOW"],
							"COUNTRY_CODE" => $_SESSION["GEOIP"]["country_code"],
							"PAGER_TEMPLATE" => "",
							"DISPLAY_TOP_PAGER" => "N",
							"DISPLAY_BOTTOM_PAGER" => "N",
							"PAGER_TITLE" => "",
							"PAGER_SHOW_ALWAYS" => "N",
							"UI_CITY_STRONG" => "",
							"UI_CITY_PRIORITET" => array(
								0 => "67",
								1 => "68",
							),
							"SMART_REDIRECT" => $smartRedirect,
							"LINK_LOCAL_CHOOSE" => "Y"
						),
							false,
							array(
								"HIDE_ICONS" => "Y"
							)
						);
						?>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 ordering">
					<button type="button" class="btn btn-blocks top-button pull-right" data-toggle="modal"
									data-target="#advanced" data-form-field-type="Оставить заявку">Заказать звонок</button>

				</div>
			</div>
		</div>
	</section>
<?endif;?>
	<!-- menu-block -->
	<?if (!CSite::InDir('/lp/bitrix24/') && !CSite::InDir('/lp/bitrix24-krd/')  && !CSite::InDir('/lp/akcia/')): ?>
	<section class="menu-block">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<nav class="navbar">
						<div class="navbar-header">
							<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</button>
						</div>
						<div class="collapse navbar-collapse js-navbar-collapse">
							<ul class="nav navbar-nav list-inline col-xs-12 hidden-lg hidden-md hidden-sm">
								<?
								$HTML_TITLE = '<a href="##HTML_LIST_ID#" class="citya col-xs-12 hidden-lg hidden-md hidden-sm" data-toggle="dropdown"><img
									src="' . SITE_TEMPLATE_PATH . '/assets/img/map-marker.png" alt="marker">#NAME#</a>';
								$APPLICATION->IncludeComponent("vitams:city.dropdown", "city_popup_mobile", Array(
									"IBLOCK_TYPE" => IBT_CONTACTS,  // Тип инфоблока
									"IBLOCK_ID" => IB_CONTACTS,  // Инфоблок
									"DROPDOWN" => $_REQUEST["dropdown"],
									"SECTION_CODE" => "",  // Код раздела
									"SECTION_URL" => "",  // URL, ведущий на страницу с содержимым раздела
									"COUNT_ELEMENTS" => "Y",  // Показывать количество элементов в разделе
									"TOP_DEPTH" => "1",  // Максимальная отображаемая глубина разделов
									"SECTION_FIELDS" => "",  // Поля разделов
									"FILTER_NAME" => "cityFilter",
									"SECTION_USER_FIELDS" => "",  // Свойства разделов
									"ADD_SECTIONS_CHAIN" => "Y",  // Включать раздел в цепочку навигации
									"CACHE_TYPE" => "A",  // Тип кеширования
									"CACHE_TIME" => "36000000",  // Время кеширования (сек.)
									"CACHE_NOTES" => "",
									"CACHE_GROUPS" => "N",  // Учитывать права доступа
									"CACHE_FILTER" => "Y",
									"LINK_IN_BASEURL" => "Y",
									"LINK_WITH_SUBDOMAIN" => "Y",
									"SEF_MODE" => "N",
									"SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
									"SEF_URL_TEMPLATES" => "",
									"HTML_LIST_ID" => "header-city-list-mobile",
									"HTML_TITLE" => $HTML_TITLE,
									"SHOW_MOSCOW" => $_SESSION["SHOW_MOSCOW"],
									"COUNTRY_CODE" => $_SESSION["GEOIP"]["country_code"],
									"PAGER_TEMPLATE" => "",
									"DISPLAY_TOP_PAGER" => "N",
									"DISPLAY_BOTTOM_PAGER" => "N",
									"PAGER_TITLE" => "",
									"PAGER_SHOW_ALWAYS" => "N",
									"UI_CITY_STRONG" => "",
									"UI_CITY_PRIORITET" => array(
										0 => "67",
										1 => "68",
									),
									"SMART_REDIRECT" => $smartRedirect,
									"LINK_LOCAL_CHOOSE" => "Y"
								),
									false,
									array(
										"HIDE_ICONS" => "Y"
									)
								);
								?>
							</ul>
							<ul class="nav navbar-nav list-inline topmenu">
								<li class="dropdown mega-dropdown item1">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Услуги</a>
									<ul class="dropdown-menu mega-dropdown-menu">
										<? $APPLICATION->IncludeComponent("bitrix:menu", "service.header", Array(
											"ALLOW_MULTI_SELECT" => "N",  // Разрешить несколько активных пунктов одновременно
											"CHILD_MENU_TYPE" => "",  // Тип меню для остальных уровней
											"DELAY" => "N",  // Откладывать выполнение шаблона меню
											"MAX_LEVEL" => "1",  // Уровень вложенности меню
											"MENU_CACHE_GET_VARS" => array(  // Значимые переменные запроса
												0 => "",
											),
											"MENU_CACHE_TIME" => "3600",  // Время кеширования (сек.)
											"MENU_CACHE_TYPE" => "A",  // Тип кеширования
											"MENU_CACHE_USE_GROUPS" => "N",  // Учитывать права доступа
											"ROOT_MENU_TYPE" => "service",  // Тип меню для первого уровня
											"USE_EXT" => "Y",  // Подключать файлы с именами вида .тип_меню.menu_ext.php
										),
											false
										); ?>
										<li class="col-md-4 col-sm-4 col-xs-12">
											<? $APPLICATION->IncludeComponent("bitrix:menu", "type.header", Array(
												"ALLOW_MULTI_SELECT" => "N",  // Разрешить несколько активных пунктов одновременно
												"CHILD_MENU_TYPE" => "",  // Тип меню для остальных уровней
												"DELAY" => "N",  // Откладывать выполнение шаблона меню
												"MAX_LEVEL" => "1",  // Уровень вложенности меню
												"MENU_CACHE_GET_VARS" => array(  // Значимые переменные запроса
													0 => "",
												),
												"MENU_CACHE_TIME" => "3600",  // Время кеширования (сек.)
												"MENU_CACHE_TYPE" => "A",  // Тип кеширования
												"MENU_CACHE_USE_GROUPS" => "N",  // Учитывать права доступа
												"ROOT_MENU_TYPE" => "type",  // Тип меню для первого уровня
												"USE_EXT" => "N",  // Подключать файлы с именами вида .тип_меню.menu_ext.php
											),
												false
											); ?>
										</li>
									</ul>
								</li>
								<li class="dropdown mega-dropdown item2">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Отраслевая экспертиза</a>
									<? $APPLICATION->IncludeComponent(
										"expertise.menu",
										"header",
										Array(
											"CACHE_TIME" => "360000",
											"CACHE_TYPE" => "A",
											"IBLOCK_ID" => "3"
										)
									); ?>
								</li>
								<li class="item3"><a href="/vozmozhnosti-bitrix24/" title="Возможности Битрикс24">Возможности
										Битрикс24</a></li>
								<li class="item4"><a href="/blog/" title="Блог">Блог</a></li>
								<li class="item5"><a href="/nashi-klienty/" title="Наши клиенты">Наши клиенты</a></li>
								<li class="item6"><a href="/kontakty/" title="Контакты">Контакты</a></li>
							</ul>
						</div><!-- /.nav-collapse -->
					</nav>
				</div>
			</div>
		</div>
	</section>
    <?endif;?>
</div>
<?
$arBanners = array(
    array(
        "NAME" => "1100x200_b24.jpg",
        "LINK" => "https://bit-24.ru/lp/chempionat-skidok-v-bitrix24/",
    ),
    array(
        "NAME" => "b1100x200.jpg",
        "LINK" => "https://studiobit.ru/lp/chempionat-skidok-v-1s-bitriks/",
    )
);
$rand = rand (0, 1);
?>
<?if (!CSite::InDir('/lp/chempionat-skidok-v-bitrix24/') && !CSite::InDir('/lp/bitrix24/') && !CSite::InDir('/lp/bitrix24-krd/')): ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <br />
                <a href="<?=$arBanners[$rand]["LINK"]?>" target="_blank"><img src="/<?=$arBanners[$rand]["NAME"]?>" class="img-responsive center-block"></a>
                <br />
            </div>
        </div>
    </div>
<?endif;?>
<? if (!$isMainPage
    && !CSite::InDir('/lp/chempionat-skidok-v-bitrix24/')
    && !CSite::InDir('/lp/bitrix24/')
	&& !CSite::InDir('/lp/bitrix24-krd/')): ?>
	<section class="heading-block">
		<div class="container">
			<div class="row">
				<? if (CSite::InDir('/blog/')): ?>
					<div class="col-md-8 col-sm-12 col-xs-6">
						<h1><? $APPLICATION->ShowTitle(true) ?></h1>
					</div>
					<div class="col-md-4 hidden-sm col-xs-6">
						<form action="/blog/search/" method="get">
							<div class="input-group search">
								<input type="text" class="form-control" name="q" placeholder="Поиск">
								<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
							</div>
						</form>
					</div>
				<? elseif (CSite::InDir('/vozmozhnosti-bitrix24/')): ?>
					<div class="col-md-8 col-sm-12 col-xs-6">
						<h1><? $APPLICATION->ShowTitle(true) ?></h1>
					</div>
					<div class="col-md-4 col-sm-4 hidden-xs">
						<button type="button" class="btn btn-more hidden-xs" data-toggle="modal"
										data-target="#advanced" data-form-field-type="Возможности Битрикс Начать бесплатно"><a href="#">Начать
								бесплатно</a></button>
					</div>
				<? else: ?>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<h1><? $APPLICATION->ShowTitle(true) ?></h1>
					</div>
				<? endif; ?>
			</div>
		</div>
	</section>
	<? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "main", Array(
		"PATH" => "",  // Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
		"SITE_ID" => "s1",  // Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
		"START_FROM" => "0",  // Номер пункта, начиная с которого будет построена навигационная цепочка
	),
		false
	); ?>
<? endif; ?>

<? /*
<body class="page <? $APPLICATION->ShowProperty("page_class") ?>">
<? $APPLICATION->ShowPanel(); ?>
<div id="app">
  <header class="page__header" id="page-header" data-toggler=".expanded">
		<? $APPLICATION->IncludeComponent(
			"tds:banner.top",
			"",
			Array(
				"CACHE_TIME" => "3600",
				"CACHE_TYPE" => "A",
				"IBLOCK_ID" => "3"
			)
		); ?>
    <section class="header">
      <div class="container">
        <a class="logo" href="/"><img src="<?= SITE_TEMPLATE_PATH ?>/assets/images/logo.svg" alt="TDS"></a>
        <nav class="nav nav--main">
          <ul class="nav__list">
            <li class="nav__item nav__item--catalog"><a class="nav__link" href="javascript:;" data-toggle="cat-popover"><span
                    class="nav__icon"><i></i><i></i><i></i></span><span>Каталог оборудования</span></a>
              <div class="popover" id="cat-popover" data-dropdown data-close-on-click="true">
                <ul class="popover-wrapper">
									<? $APPLICATION->IncludeComponent(
										"tds:menu.catalog",
										"",
										Array(
											"CACHE_TIME" => "360000",
											"CACHE_TYPE" => "A",
											"IBLOCK_TYPE" => "catalog"
										)
									); ?>
									<? $APPLICATION->IncludeComponent(
										"bitrix:menu",
										"service",
										Array(
											"ALLOW_MULTI_SELECT" => "N",
											"CHILD_MENU_TYPE" => "",
											"DELAY" => "N",
											"MAX_LEVEL" => "1",
											"MENU_CACHE_GET_VARS" => "",
											"MENU_CACHE_TIME" => "3600",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_USE_GROUPS" => "N",
											"ROOT_MENU_TYPE" => "service",
											"USE_EXT" => "N",
											"COMPONENT_TEMPLATE" => ".default"
										),
										false
									); ?>
                </ul>
              </div>
            </li>
						<? $APPLICATION->IncludeComponent("bitrix:menu", "desktop.top", Array(
							"COMPONENT_TEMPLATE" => ".default",
							"ROOT_MENU_TYPE" => "top",  // Тип меню для первого уровня
							"MENU_CACHE_TYPE" => "A",  // Тип кеширования
							"MENU_CACHE_TIME" => "3600",  // Время кеширования (сек.)
							"MENU_CACHE_USE_GROUPS" => "Y",  // Учитывать права доступа
							"MENU_CACHE_GET_VARS" => "",  // Значимые переменные запроса
							"MAX_LEVEL" => "1",  // Уровень вложенности меню
							"CHILD_MENU_TYPE" => "",  // Тип меню для остальных уровней
							"USE_EXT" => "N",  // Подключать файлы с именами вида .тип_меню.menu_ext.php
							"DELAY" => "N",  // Откладывать выполнение шаблона меню
							"ALLOW_MULTI_SELECT" => "N",  // Разрешить несколько активных пунктов одновременно
						),
							false
						); ?>
          </ul>
        </nav>
        <div class="nav nav--shop">
          <ul class="nav__list">
            <li class="nav__item nav__item--favorites">
              <a class="nav__link" href="/favorite/">
                <span class="nav__icon"><?
									if ($cntFavorite):?><span class="badge"><?= $cntFavorite ?></span><? endif; ?>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16.02" height="14.19" viewBox="0 0 16.02 14.19">
                    <path
                        d="M15.91 3.75a4.28 4.28 0 0 0-4.19-3.76 5.63 5.63 0 0 1-7.41 0A4.28 4.28 0 0 0 .12 3.77c-.67 3.4 1.46 7.92 7.7 10.38a.56.56 0 0 0 .21 0 .51.51 0 0 0 .2 0c6.23-2.52 8.34-7.03 7.68-10.4zM8 12.37C3.15 10.38 1.46 6.9 2 4.31a2.86 2.86 0 0 1 2.71-2.55 5 5 0 0 1 3 1.23.49.49 0 0 0 .62 0 5 5 0 0 1 3-1.24 2.85 2.85 0 0 1 2.75 2.52c.5 2.56-1.17 6-6 8.06H8z"
                        fill="#262626"/>
                  </svg>
                </span>
                <span>Избранное</span>
              </a>
            </li>
            <li class="nav__item nav__item--compare">
              <a class="nav__link" href="/compare/">
                <span class="nav__icon"><?
									if ($cntCompare):?><span class="badge"><?= $cntCompare ?></span><? endif; ?>
                  <svg xmlns="http://www.w3.org/2000/svg" width="12.95" height="11.97" viewBox="0 0 12.95 11.97">
                      <path d="M5 12.01v-2h8v2zm0-7h8v2H5zm0-5h8v2H5zm-5 10h2.92v2H0zm0-5h2.92v2H0zm0-5h2.92v2H0z"
                            fill="#262626"/>
                  </svg>
                </span>
                <span>Сравнение</span>
              </a>
            </li>
            <li class="nav__item nav__item--trigger"><a class="nav__link" href="javascript:;" data-toggle="page-header"><span
                    class="nav__icon"><i></i><i></i><i></i></span></a></li>
            <li class="nav__item nav__item--cart">
              <a class="nav__link" href="#" data-toggle="cart-popover">
                <span class="nav__icon js-count-all">
                  <? if ($arBasket["ALL"]): ?><div class="badge"><?= $arBasket["ALL"] ?></div><? endif; ?>
                  <svg xmlns="http://www.w3.org/2000/svg" width="22.65" height="16.02" viewBox="0 0 22.65 16.02">
                    <path
                        d="M8.28 12.2h10a1.41 1.41 0 0 0 1.31-1.08l3-7.24a1.09 1.09 0 0 0-.14-1 1.45 1.45 0 0 0-1.17-.6h-16a1.48 1.48 0 0 0-1.18.6 1.12 1.12 0 0 0-.13 1l3 7.24a1.4 1.4 0 0 0 1.31 1.08zm9.83-1.57H8.46a.24.24 0 0 0 0-.07L5.64 3.81h15.29l-2.79 6.76v.07zM4.74 4.39a.81.81 0 0 0 .26 0 .78.78 0 0 0 .47-1l-.84-2.38a1.18 1.18 0 0 0-1.33-1H.78a.785.785 0 1 0 0 1.57h2.41L4 3.87a.78.78 0 0 0 .74.52zm5 9.08A1.28 1.28 0 1 0 11 14.75a1.28 1.28 0 0 0-1.28-1.28zm7.15 0a1.28 1.28 0 1 0 1.28 1.28 1.28 1.28 0 0 0-1.28-1.28z"/>
                  </svg>
                </span>
                <span>Заказы</span>
              </a>
              <div class="popover" id="cart-popover" data-dropdown data-hover="true" data-hover-pane="true">
                <ul class="popover-wrapper">
                  <li class="popover-group">
                    <ul class="popover-menu">
                      <li class="js-count-rent"><a href="/rent/"><span>Аренда</span><? if ($arBasket["RENT"]): ?><div class="badge"><?= $arBasket["RENT"] ?></div><? endif; ?></a></li>
                      <li class="js-count-buy"><a href="/buy/"><span>Покупка</span><? if ($arBasket["BUY"]): ?><div class="badge"><?= $arBasket["BUY"] ?></div><? endif; ?></a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
        </div>
        <div class="header-search">
          <a class="header-search__button" href="javascript:;" data-toggle="search-panel">
            <svg xmlns="http://www.w3.org/2000/svg" width="14.87" height="14.87" viewBox="0 0 14.87 14.87">
              <path
                  d="M8.12 0a6.75 6.75 0 0 0-5.39 10.8l-2.74 2.73 1.35 1.34 2.73-2.73A6.74 6.74 0 1 0 8.12 0zm0 11.59a4.85 4.85 0 1 1 4.87-4.84 4.85 4.85 0 0 1-4.87 4.85z"
                  fill="#262626"/>
            </svg>
          </a>
        </div>
        <div class="header-profile">
          <a class="header-profile__button" <? if (!$USER->IsAuthorized()): ?>data-popup data-src="#popup-sign-in"
             href="javascript:;" <? else: ?>href="/personal/"<? endif; ?>>
            <svg xmlns="http://www.w3.org/2000/svg" width="19.24" height="19.24" viewBox="0 0 19.24 19.24">
              <path
                  d="M16 0H3.24A3.25 3.25 0 0 0 0 3.25v6.38a.71.71 0 0 0 .71.71h10.87L10 11.89a.71.71 0 0 0 .5 1.21.7.7 0 0 0 .5-.21l2.77-2.77a.7.7 0 0 0 0-1L11 6.36a.71.71 0 0 0-1 0 .72.72 0 0 0 0 1l1.56 1.56H1.42V3.25a1.82 1.82 0 0 1 1.82-1.83H16a1.82 1.82 0 0 1 1.82 1.83V16A1.82 1.82 0 0 1 16 17.83H3.24A1.82 1.82 0 0 1 1.42 16v-1.66a.71.71 0 1 0-1.42 0V16a3.25 3.25 0 0 0 3.24 3.25H16A3.25 3.25 0 0 0 19.24 16V3.25A3.25 3.25 0 0 0 16 0z"
                  fill="#262626"/>
            </svg>
          </a></div>
      </div>
    </section>
		<? $APPLICATION->IncludeComponent(
			"bitrix:search.form",
			"top.desktop",
			Array(
				"PAGE" => "#SITE_DIR#search/",
			),
			false
		); ?>
		<? $APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"main",
			array(
				"COMPONENT_TEMPLATE" => "main",
				"START_FROM" => "0",
				"PATH" => "",
				"SITE_ID" => "s1"
			),
			false
		); ?>
    <section class="mobile-menu" id="mobile-menu" data-toggler=".opened">
      <div class="container">
				<? $APPLICATION->IncludeComponent(
					"bitrix:search.form",
					"top.mobile",
					Array(
						"PAGE" => "#SITE_DIR#search/",  // Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
					),
					false
				); ?>
        <div class="nav nav--shop">
          <ul class="nav__list">
            <li class="nav__item nav__item--favorites">
              <a class="nav__link" href="/favorite/">
                <span class="nav__icon"><?
									if ($cntFavorite):?><span class="badge"><?= $cntFavorite ?></span><?endif;
									?>
                  <svg xmlns="http://www.w3.org/2000/svg" width="16.02" height="14.19" viewBox="0 0 16.02 14.19">
                    <path
                        d="M15.91 3.75a4.28 4.28 0 0 0-4.19-3.76 5.63 5.63 0 0 1-7.41 0A4.28 4.28 0 0 0 .12 3.77c-.67 3.4 1.46 7.92 7.7 10.38a.56.56 0 0 0 .21 0 .51.51 0 0 0 .2 0c6.23-2.52 8.34-7.03 7.68-10.4zM8 12.37C3.15 10.38 1.46 6.9 2 4.31a2.86 2.86 0 0 1 2.71-2.55 5 5 0 0 1 3 1.23.49.49 0 0 0 .62 0 5 5 0 0 1 3-1.24 2.85 2.85 0 0 1 2.75 2.52c.5 2.56-1.17 6-6 8.06H8z"
                        fill="#262626"/>
                    </svg>
                </span>
                <span>Избранное</span>
              </a>
            </li>
            <li class="nav__item nav__item--compare">
              <a class="nav__link" href="/compare/">
                <span class="nav__icon"><?
									if ($cntCompare):?><span class="badge"><?= $cntCompare ?></span><? endif; ?>
                  <svg xmlns="http://www.w3.org/2000/svg" width="12.95" height="11.97" viewBox="0 0 12.95 11.97">
                    <path d="M5 12.01v-2h8v2zm0-7h8v2H5zm0-5h8v2H5zm-5 10h2.92v2H0zm0-5h2.92v2H0zm0-5h2.92v2H0z"
                          fill="#262626"/>
                  </svg>
                </span>
                <span>Сравнение</span>
              </a>
            </li>
            <li class="nav__item nav__item--auth"><a class="nav__link" data-popup data-src="#popup-sign-in"
                                                     href="javascript:;"><span class="nav__icon"><svg
                      xmlns="http://www.w3.org/2000/svg" width="19.24" height="19.24" viewBox="0 0 19.24 19.24">
  <path
      d="M16 0H3.24A3.25 3.25 0 0 0 0 3.25v6.38a.71.71 0 0 0 .71.71h10.87L10 11.89a.71.71 0 0 0 .5 1.21.7.7 0 0 0 .5-.21l2.77-2.77a.7.7 0 0 0 0-1L11 6.36a.71.71 0 0 0-1 0 .72.72 0 0 0 0 1l1.56 1.56H1.42V3.25a1.82 1.82 0 0 1 1.82-1.83H16a1.82 1.82 0 0 1 1.82 1.83V16A1.82 1.82 0 0 1 16 17.83H3.24A1.82 1.82 0 0 1 1.42 16v-1.66a.71.71 0 1 0-1.42 0V16a3.25 3.25 0 0 0 3.24 3.25H16A3.25 3.25 0 0 0 19.24 16V3.25A3.25 3.25 0 0 0 16 0z"
      fill="#262626"/>
</svg></span></a></li>
          </ul>
        </div>
        <div class="nav nav--mobile">
          <ul class="nav__list">
            <li class="nav__item">
              <div class="nav__link"><span>Каталог оборудования</span></div>
							<? $APPLICATION->IncludeComponent("bitrix:menu", "mobile.catalog", Array(
								"COMPONENT_TEMPLATE" => ".default",
								"ROOT_MENU_TYPE" => "catalog",  // Тип меню для первого уровня
								"MENU_CACHE_TYPE" => "A",  // Тип кеширования
								"MENU_CACHE_TIME" => "3600",  // Время кеширования (сек.)
								"MENU_CACHE_USE_GROUPS" => "Y",  // Учитывать права доступа
								"MENU_CACHE_GET_VARS" => "",  // Значимые переменные запроса
								"MAX_LEVEL" => "1",  // Уровень вложенности меню
								"CHILD_MENU_TYPE" => "",  // Тип меню для остальных уровней
								"USE_EXT" => "N",  // Подключать файлы с именами вида .тип_меню.menu_ext.php
								"DELAY" => "N",  // Откладывать выполнение шаблона меню
								"ALLOW_MULTI_SELECT" => "N",  // Разрешить несколько активных пунктов одновременно
							),
								false
							); ?>
            </li>
						<? $APPLICATION->IncludeComponent("bitrix:menu", "mobile.top", Array(
							"COMPONENT_TEMPLATE" => ".default",
							"ROOT_MENU_TYPE" => "top",  // Тип меню для первого уровня
							"MENU_CACHE_TYPE" => "A",  // Тип кеширования
							"MENU_CACHE_TIME" => "3600",  // Время кеширования (сек.)
							"MENU_CACHE_USE_GROUPS" => "Y",  // Учитывать права доступа
							"MENU_CACHE_GET_VARS" => "",  // Значимые переменные запроса
							"MAX_LEVEL" => "1",  // Уровень вложенности меню
							"CHILD_MENU_TYPE" => "",  // Тип меню для остальных уровней
							"USE_EXT" => "N",  // Подключать файлы с именами вида .тип_меню.menu_ext.php
							"DELAY" => "N",  // Откладывать выполнение шаблона меню
							"ALLOW_MULTI_SELECT" => "N",  // Разрешить несколько активных пунктов одновременно
						),
							false
						); ?>
          </ul>
        </div>
      </div>
    </section>
  </header>
  <main class="page__main">
*/ ?>