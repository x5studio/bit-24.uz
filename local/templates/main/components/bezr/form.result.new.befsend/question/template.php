<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

$PARENT = $this->__component->__parent; ?>






<div id="general-form-<?=$arResult["SOULT_FORM"]?>" data-salt-form="<?= $arResult["SOULT_FORM"] ?>"
     data-btn="<?=$arParams["~BTN_CALL_FORM"]?>"
     class="j-form-page fbox">

    <div class="container">
        <div class="row">
            <?
            /**********************************************************************************
             * form header
             **********************************************************************************/
            ?>

            <div class="form-feed-back__wrapper">
                <? if ($arResult["isFormErrors"] == "Y"): ?><?/*= $arResult["FORM_ERRORS_TEXT"]; */?><? endif; ?>

                <form name="<?= $arResult["arForm"]["SID"] ?>" action="<?= POST_FORM_ACTION_URI ?>"
                            method="POST" enctype="multipart/form-data" class="j-form j-form_inline-errors" data-keep-errors="1">
                    <input type="hidden" name="sessid" value=""/>
                    <input type="hidden" name="WEB_FORM_ID" value="<?= $arParams['WEB_FORM_ID'] ?>"/>

                    <div class="form-feed-back__block">
                        <div class="form-feed-back__bc">
                            <div class="form-feed-back__caption">
                                <?
                                $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/form_question_title.php', [], [
                                    'MODE' => 'php',
                                    'NAME' => 'Заголовок формы "Остались вопросы"'
                                ]);
                                ?>
                            </div>
                            <div class="form-feed-back__text">
                                <?
                                $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/form_question_text.php', [], [
                                    'MODE' => 'php',
                                    'NAME' => 'Описание формы "Остались вопросы"'
                                ]);
                                ?>
                            </div>
                        </div>

                        <div class="form-feed-back__it">
                            <? $arHiddenID = Array(); ?>
                            <? $k = 0;
                            foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):$k++; ?>
                                <?
                                // TEXT EMAIL ...
                                if (in_array($arQuestion['STRUCTURE'][0]['FIELD_TYPE'], array('text', 'email'))):?>
                                    <?
                                    $inputId = $arQuestion['STRUCTURE'][0]['ID'];
                                    $inputVar = CFormField::GetByID($inputId)->Fetch()['VARNAME'];
                                    ?>
                                    <div class="form-feed-back__item text form-group has-feedback<? if (strlen($arResult["FORM_ERRORS"][$FIELD_SID]) > 0): ?> has-error<?endif;?>">
                                        <input
                                             name="form_<?= $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>_<?= $arQuestion['STRUCTURE'][0]['ID'] ?>"
                                             type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] == "PHONE" ? "tel" : $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] ?>"
                                             data-type="<?= $arQuestion['STRUCTURE'][0]['FIELD_PARAM'] ?>"
                                             data-var="<?= $inputVar ?>"
                                             value=""
                                             placeholder="<?= $arQuestion["CAPTION"] ?>"
                                             class="form-control" />
                                    </div>
                                <? elseif ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'):$k--; ?>
                                    <? array_push($arHiddenID, $FIELD_SID); ?>
                                <? endif; ?>
                            <? endforeach; ?>
                        </div>
                    </div>

                    <?/* // SUBMIT ?>
                    <div class="form-group has-feedback"><input type="submit" class="btn btn-blocks top-button" name="web_form_submit"
                                 value="<?= $arParams['MESS']['SUBMIT_BTN'] ?: (strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"></div>
                    <div>Нажимая кнопку, я принимаю условия <a href="https://bit-24.ru/oferta/">Оферты</a> по использованию сайта и согласен с <a href="https://bit-24.ru/politika-konfidencialnosti/">Политикой конфиденциальности</a></div>
                    <?*/?>

                    <div class="form-feed-back__block form-feed-back__block-revert">
                        <div class="form-feed-back__but form-group has-feedback">
                            <input type="submit"
                                onclick="yaCounter45927957.reachGoal('expert-send');gtag('event', 'send', {'event_category': 'expert', 'event_label': 'send'}); return true;"
                                   class="btn btn-default"
                                   name="web_form_submit"
                                   value="<?= $arParams['MESS']['SUBMIT_BTN'] ?: (strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>">
                        </div>

                        <div class="form-feed-back__mess">
                            <?
                            $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . '/include_areas/form_question_privacy.php', [], [
                                'MODE' => 'php',
                                'NAME' => 'Политика конфиденциальности сайта у формы "Остались вопросы"'
                            ]);
                            ?>
                        </div>
                    </div>

                    <?
                    // HIDDENS
                    if (!empty($arHiddenID))
                    {
                        $k--;
                        foreach ($arHiddenID AS $hiddenID)
                        {
                            echo $arResult["QUESTIONS"][$hiddenID]["HTML_CODE"];
                        }
                    } ?>
                    <? if (!empty($arParams['COMPONENT_MARKER'])): ?>
                        <input type="hidden" name="COMPONENT_MARKER" value="<?= $arParams['COMPONENT_MARKER'] ?>">
                    <? endif; ?>
                </form>
            </div>
        </div>
    </div>
</div>