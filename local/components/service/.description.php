<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Услуги ЧПУ",
	"DESCRIPTION" => "Услуги ЧПУ",
	"ICON" => "/images/catalog.gif",
	"SORT" => 120,
	"PATH" => array(
		"ID" => "proman",
		"NAME" => "proman",
		"CHILD" => array(
			"ID" => "service",
			"NAME" => "Услуги"
		)
	),
);
?>