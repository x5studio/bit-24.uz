<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Индивидуальный подход для внедрения и запуска Битрикс24 в крупных организациях");
$APPLICATION->SetPageProperty("keywords", "крупные, проект, организация, Битрикс24, bitrix24, разработка");
$APPLICATION->SetPageProperty("title", "Разработка и внедрение Битрикс24 для крупных организаций ");
$APPLICATION->SetTitle("Разработка проектов на Битрикс24 для крупных организаций ");
?>
	<section class="first-bt-block">
		<div class="container">
			<div class="row">
				<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/proektnoe/.inc.block_1.php", Array(), Array("MODE" =>
					"html")); ?>
			</div>
		</div>
	</section>
	<!-- if block -->
	<section class="if-block">
		<div class="container">
			<div class="row">
				<h2>Вам подходит проектное внедрение Битрикс24, если:</h2>
				<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/proektnoe/.inc.block_2.php", Array(), Array("MODE" =>
					"html")); ?>
			</div>
		</div>
	</section>
<? $APPLICATION->IncludeComponent(
	"implementation.project",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "13"
	)
); ?>
<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/.inc.why.php", Array(), Array("MODE" =>
	"html")); ?>
<? $APPLICATION->IncludeComponent(
	"client.by.id",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"EXPERTISE_ID" => 34,
		"IBLOCK_ID" => "8"
	)
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>