<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arId = array();
foreach ($arResult["SEARCH"] as $arItem)
	$arId[] = $arItem["ITEM_ID"];

if (count($arId) > 0 && CModule::IncludeModule("iblock"))
{
	$arFile = array();
	$rsElement = CIBlockElement::GetList(
		array(),
		array("=ID" => $arId),
		false,
		false,
		array("ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PREVIEW_TEXT", "PREVIEW_PICTURE", "ACTIVE_FROM")
	);
	while ($arElement = $rsElement->GetNext())
	{

		$arFile[] = $arElement["PREVIEW_PICTURE"];
		$arResult["SECTIONS"][$arElement["IBLOCK_SECTION_ID"]] = "";

		$arElement["DISPLAY_ACTIVE_FROM"] = ToLower(FormatDate("d F Y", MakeTimeStamp($arElement["ACTIVE_FROM"])));

		$arResult["ITEMS"][$arElement["ID"]] = $arElement;
	}
	$arResult["IMAGES"] = \Proman\Helper::getImages($arFile);
	if (count($arResult["SECTIONS"]) > 0)
	{
		$rsSection = \CIBlockSection::GetList(
			array(),
			array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ID" => array_keys($arResult["SECTIONS"])
			),
			false,
			array("ID", "NAME", "SECTION_PAGE_URL"));
		while ($arSec = $rsSection->GetNext())
		{
			$arResult["SECTIONS"][$arSec["ID"]] = $arSec;
		}
	}
}