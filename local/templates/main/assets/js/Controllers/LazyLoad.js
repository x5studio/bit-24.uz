if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/AjaxBase'], function ($, AjaxBase) {

        function AjaxProvider() {
            AjaxBase.apply(this, arguments);
        }

        extend(AjaxProvider, AjaxBase);

        AjaxProvider.prototype.actionSuccess = function (data) {
            var o = this.scope.getOwner();
            if (typeof data == 'object' && !$.isEmptyObject(data)) {
                o.loadDataInserting(data);
            }

        };
        AjaxProvider.prototype.actionError = function (jqXHR) {
            var o = this.scope.getOwner();
            try {
                var data = $.parseJSON(jqXHR.responseText);
                if (typeof data == 'object' && !$.isEmptyObject(data)) {
                    o.loadDataInserting(data);
                }
            } catch (e) {

            }
        };

        function LazyLoad(containers, options) {
            this.containers = $(containers);
            this.options = $.extend(true, {}, LazyLoad.defaultOptions, options);
            this.ajaxProvider = new AjaxProvider({
                controllerUrl: this.options.controllerUrl,
                data:{
                    'LAZY_LOAD_ID': containers.data('cache-id')
                }
            });
            this.ajaxProvider.setOwner(this);
        }

        LazyLoad.defaultOptions = {
            controllerUrl: (function () {
                var href = window.location.href;
                href = href.replace(/^(?:\/\/|[^\/]+)*\//, "");
                href = '/ajax/lazy-load/' + href;
                return href;
            })()
        };

        LazyLoad.prototype.loadDataInserting = function (data) {
            $.event.trigger({
                type: 'lazyLoadReceivedBefore',
                data: data
            });

            for (var key in data) {

                if (!data.hasOwnProperty(key)) {
                    continue;
                }
                var value = data[key];
                if (key == '' || value == '') {
                    continue;
                }

                var itemSelector = 'div[data-name="' + key + '"]';
                var $item = this.containers.filter(itemSelector);
                if ($item.length > 0) {
                    $item.html(value);
                    $.event.trigger({
                        type: 'lazyLoadBlockUpdate',
                        contextName: key,
                        contextContainer: $item
                    });
                }
            }
            $.event.trigger({
                type: 'lazyLoadReceived',
                data: data
            });
        };

        LazyLoad.prototype.init = function () {
            this.ajaxProvider.send();
        };

        return LazyLoad;
    });
}