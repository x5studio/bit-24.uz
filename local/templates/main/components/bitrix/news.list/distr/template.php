<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(count($arResult['ITEMS']) > 5){
    $arResult['ITEMS'] = array_slice($arResult['ITEMS'], 0, 5);
}
$visible = true;
?>
<div class="col-md-12">
    <h2 class="why-heading white-heading">Сколько стоит</h2>
    <div class="card">
        <div class="row">
            <div class="col-lg-7 col-md-8">
                <div class="card-content">
                    <img class="portal-box-top box-img" src="<?=$arResult["ITEMS"][0]['PREVIEW_PICTURE']['SRC']?>" alt="" style="display: none">
                    <div class="card-heading bx-distr"><?=$arResult["ITEMS"][0]['PROPERTIES']['NAME_DISTR']['VALUE']?></div>
                    <p>Выберите количество пользователей</p>
                    <div class="tabs-wrapper">
                        <div class="tabs price-tabs clearfix">

                        <?foreach($arResult["ITEMS"] as $arItem):?>
                        	<?
                        	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                        	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                        	?>
                            <div onclick="distr(this);"
                                 class="tab" data-name="<?= $arItem['PROPERTIES']['NAME_DISTR']['VALUE']?>"
                                 data-img="<?= $arItem['PREVIEW_PICTURE']['SRC']?>"
                                 id="<?=$this->GetEditAreaId($arItem['ID']);?>"
                            >
                                <span><?=$arItem['NAME']?></span>
                            </div>
                        <?endforeach;?>

                        </div>
                        <div class="tab-content">

                        <?foreach($arResult["ITEMS"] as $arItem):?>
                            <div class="tab-item" <?=!$visible ? 'style="display:none;"' : '';?>>
                                <div class="price-block">
                                    <?if($arItem['PROPERTIES']['OLD_PRICE']['VALUE']):?>
                                    <div class="card-portal-price old-price">
                                        <?= $arItem['PROPERTIES']['OLD_PRICE']['VALUE'] ?> ₽
                                    </div>
                                    <?endif;?>
                                    <div class="card-portal-price">
                                        <?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?> ₽
                                    </div>
                                </div>
                                <div class="btn-sect footbtn"><a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a></div>
                            </div>
                            <? $visible = false; ?>
                        <?endforeach;?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-4">
                <img class="portal-box box-img" src="<?=$arResult["ITEMS"][0]['PREVIEW_PICTURE']['SRC']?>" alt="">
            </div>
        </div>
    </div>
</div>

