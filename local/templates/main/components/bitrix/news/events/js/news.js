$(function() {

    // Filter
    var $events_list_filter = $('.events-list__filter');

    function applyEventsFilter() {
        var filterObj = $events_list_filter.data(),
            filterKeys = Object.keys(filterObj),
            filterArr = [];

        for (let k = 0; k < filterKeys.length; k++) {
            if (typeof filterObj[filterKeys[k]] !== 'undefined') {
                filterArr.push(filterKeys[k] + '=' +  filterObj[filterKeys[k]]);
            }
        }

        if (filterArr.length > 0) {
            document.location.href = '?' + filterArr.join('&');
        }
    }

    // Select2
    $('.events-list__filter-city select')
        .on('select2:select', function(e) {
            $events_list_filter.data('city', e.params.data.id);
            applyEventsFilter();
        })
        .select2(
            {
                minimumResultsForSearch: -1
            }
        );

    // Calendar
    var active_dates = JSON.parse($('.events-list__filter-date').attr('data-active-dates'));

    if (active_dates === null) {
        active_dates = [$('.events-list__filter-date').attr('data-active-dates')];
    }

    $('.events-list__filter-date-input').datepicker(
        {
            inline: true,
            dateFormat: 'dd.mm.yyyy',
            language: 'ru',
            position: 'right top',
            prevHtml: '<i class="fa fa-angle-left"></i>',
            nextHtml: '<i class="fa fa-angle-right"></i>',
            onRenderCell: function (date, cellType) {
                if (cellType == 'day') {
                    var dateFormatted = ('0' + date.getDate()).slice(-2) + '.' + ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear(),
                        dateNow = new Date(),
                        today = ('0' + dateNow.getDate()).slice(-2) + '.' + ('0' + (dateNow.getMonth() + 1)).slice(-2) + '.' + dateNow.getFullYear();

                    return {
                        disabled: dateFormatted !== today && active_dates.indexOf(dateFormatted) === -1
                    }
                }
            },
            onSelect(formattedDate, date, inst) {
                $events_list_filter.data('date', formattedDate);
                applyEventsFilter();
            }
        }
    );

    // Выбор типа события
    $('.events-type-btn').on('click', function(e) {
        e.preventDefault();

        $events_list_filter.data('type',$(this).data('type'));
        applyEventsFilter();
    });

    $('.events-price-btn').on('click', function(e) {
        e.preventDefault();

        $events_list_filter.data('price',$(this).data('price'));
        applyEventsFilter();
    });

    // Показ datepicker
    $(document).on('click', '.events-list__filter-date-btn', function(){
        $('.events-list__filter-date .datepicker-inline').fadeToggle();
    });

});