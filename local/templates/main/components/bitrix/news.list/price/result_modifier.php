<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$sectionIds = [];
foreach($arResult["ITEMS"] as $arItem) {
    $sectionId = $arItem['IBLOCK_SECTION_ID'];
    $sectionIds[] = $sectionId;
    $arResult["SECTIONS"][$sectionId]['ID'] = $sectionId;
    $arResult["SECTIONS"][$sectionId]['ITEMS'][] = $arItem;
}
unset($arResult["ITEMS"]);
$sectionIds = array_unique($sectionIds);

$dbRes = \CIBlockSection::GetList(
    [],
    [
        'ACTIVE' => 'Y',
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'ID' => $sectionIds,
    ],
    false,
    [
        'IBLOCK_ID', 'ID', 'NAME', 'CODE', 'SORT', 'TIMESTAMP_X'
    ]
);
$newSections = [];
while ($arRes = $dbRes->Fetch()) {
    $sectionId = $arRes['ID'];
    if (count($arResult["SECTIONS"][$sectionId]['ITEMS'])) {
        $newSections[] = array_merge(
            $arResult["SECTIONS"][$sectionId],
            $arRes
        );
    }
}
$arResult["SECTIONS"] = $newSections;

usort($arResult["SECTIONS"], function ($a, $b) {return strtotime($a['TIMESTAMP_X'])<strtotime($b['TIMESTAMP_X']);});
usort($arResult["SECTIONS"], function ($a, $b) {return $a['SORT']>$b['SORT'];});


$curPage = rtrim($APPLICATION->GetCurPage(), '/').'/';
$includeDirLocalPath = $curPage.'/include/';
$includeDirAbsPath = $_SERVER['DOCUMENT_ROOT'].$includeDirLocalPath;

$h2IncludeFileName = 'price__h2.php';

$arResult['INCLUDE_DIR_LOCAL_PATH'] = $includeDirLocalPath;
$arResult['INCLUDE_DIR_ABS_PATH'] = $includeDirAbsPath;
$arResult['H2_INCLUDE_FILE_NAME'] = $h2IncludeFileName;

$this->__component->SetResultCacheKeys([
    'INCLUDE_DIR_ABS_PATH',
]);

