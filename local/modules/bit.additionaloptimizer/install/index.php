<?

IncludeModuleLangFile(__FILE__);

use \Bitrix\Main\ModuleManager;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\EventManager;
use \Bitrix\Main\Application;
use \Bitrix\Main\IO\Directory;


Loc::loadMessages(__FILE__);

Class bit_additionaloptimizer extends CModule
{

    public $MODULE_ID;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $PARTNER_NAME;
    public $PARTNER_URI;
    public $errors;

    function __construct()
    {
        if(file_exists(__DIR__."/version.php")){

            $arModuleVersion = array();

            include_once(__DIR__."/version.php");

            $this->MODULE_ID = "bit.additionaloptimizer";
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = Loc::getMessage("BIT_ADO_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("BIT_ADO_DESCRIPTION");
            $this->PARTNER_NAME = Loc::getMessage('BIT_ADO_PARTNER_NAME');
            $this->PARTNER_URI = Loc::getMessage('BIT_ADO_PARTNER_URI');
        }
    }

    public function DoInstall(){

        global $APPLICATION;

        if(CheckVersion(ModuleManager::getVersion("main"), "17.00.00") && version_compare(PHP_VERSION, '7.1.0', '>=')){

            ModuleManager::registerModule($this->MODULE_ID);

            $this->InstallFiles();
            $this->InstallDB();
            $this->InstallEvents();

        }else{

            $APPLICATION->ThrowException(
                Loc::getMessage("BIT_ADO_INSTALL_ERROR_VERSION")
            );
        }

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("BIT_ADO_INSTALL_TITLE")." \"".Loc::getMessage("BIT_ADO_NAME")."\"",
            __DIR__."/step.php"
        );

        return false;
    }

    function DoUninstall()
    {
        global $APPLICATION;

        $this->UnInstallFiles();
        $this->UnInstallDB();
        $this->UnInstallEvents();

        ModuleManager::unRegisterModule($this->MODULE_ID);

        $APPLICATION->IncludeAdminFile(
            Loc::getMessage("BIT_ADO_UNINSTALL_TITLE")." \"".Loc::getMessage("BIT_ADO_NAME")."\"",
            __DIR__."/unstep.php"
        );
    }

    function InstallDB()
    {

        if(\Bitrix\Main\Loader::includeModule($this->MODULE_ID)){

            $db = Application::getConnection();

            // $Entity = LeadMessageTable::getEntity();
            // if (!$db->isTableExists($Entity->getDBTableName())) {
            //     $Entity->createDbTable();
            //
            // }

        };
        return true;
    }

    function UnInstallDB()
    {

        if(\Bitrix\Main\Loader::includeModule($this->MODULE_ID)){

            $db = Application::getConnection();

            // $Entity = LeadMessageTable::getEntity();
            // if ($db->isTableExists($Entity->getDBTableName())) {
            //      $db->dropTable($Entity->getDBTableName());
            // }

        };
        Option::delete($this->MODULE_ID);

        return true;
    }

    function InstallEvents()
    {
        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnBeforeProlog",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnBeforePrologHandler"
        );
        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnEndBufferContent",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnEndBufferContentHandler"
        );
        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnFileDelete",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnFileDeleteHandler"
        );
        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnFileSave",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnFileSaveHandler"
        );
        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnMakeFileArray",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnMakeFileArrayHandler"
        );
        EventManager::getInstance()->registerEventHandler(
            "main",
            "OnGetFileSRC",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnGetFileSRCHandler"
        );
        return true;
    }

    function UnInstallEvents()
    {
        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnBeforeProlog",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnBeforePrologHandler"
        );
        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnEndBufferContent",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnEndBufferContentHandler"
        );
        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnFileDelete",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnFileDeleteHandler"
        );
        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnFileSave",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnFileSaveHandler"
        );
        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnMakeFileArray",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnMakeFileArrayHandler"
        );
        EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnGetFileSRC",
            $this->MODULE_ID,
            "Bit\AdditionalOptimizer\Handlers",
            "OnGetFileSRCHandler"
        );

        return true;
    }

    function InstallFiles()
    {
        //$documentRoot = Application::getDocumentRoot();

        // CopyDirFiles(
        //     __DIR__ . '/files/',
        //     $documentRoot . '/crm/',
        //     true,
        //     true
        // );
        
        $binPath = realpath ( __DIR__ . '/../lib/image/bin/' );

        // позволяем бинарникам исполняться
        exec('chmod -R 755 '.$binPath);
        return true;
    }

    function UnInstallFiles()
    {
        //DeleteDirFilesEx('/crm/');

        return true;
    }
}
