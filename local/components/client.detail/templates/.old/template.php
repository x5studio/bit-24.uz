<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="first-block">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12"><?= $arResult["~UF_DESC"] ?></div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<img src="<?= $arResult["IMAGES"][$arResult["DETAIL_PICTURE"]] ?>" class="img-responsive center-block">
			</div>
		</div>
	</div>
</section>
<? if (strlen($arResult["UF_TOP_DESC"])): ?>
	<section class="blue-block">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="ribbon">
						<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/realestate/ribbon.png" class="img-responsive">
					</div>
					<p><?= $arResult["UF_TOP_DESC"] ?></p>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>
<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
	<? if ($cell % 2): ?>
		<section class="branches-white">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<h2><?= $arItem["~NAME"] ?></h2>
						<?= $arItem["PREVIEW_TEXT"] ?>
					</div>
					<? if ($arItem["PREVIEW_PICTURE"]): ?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
						</div>
					<? else: ?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="videobg center-block">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=
								$arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
							</div>
						</div>
					<? endif; ?>
				</div>
				<? if (!empty($arItem["DETAIL_TEXT"])): ?>
					<?= $arItem["DETAIL_TEXT"] ?>
				<? endif; ?>
			</div>
		</section>
	<? else: ?>
		<section class="branches-gray">
			<div class="container">
				<div class="row">
					<? if ($arItem["PREVIEW_PICTURE"]): ?>
						<div class="col-md-6 hidden-sm hidden-xs">
							<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h2><?= $arItem["~NAME"] ?></h2>
							<?= $arItem["PREVIEW_TEXT"] ?>
						</div>
						<div class="hidden-md col-sm-12 hidden-lg col-xs-12">
							<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
						</div>
					<? else: ?>
						<div class="col-md-6 hidden-sm hidden-xs">
							<div class="videobg center-block">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=
								$arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
							</div>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h2><?= $arItem["~NAME"] ?></h2>
							<?= $arItem["PREVIEW_TEXT"] ?>
						</div>
						<div class="hidden-md col-sm-12 hidden-lg col-xs-12 center-block">
							<div class="videobg center-block">
								<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=
								$arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
							</div>
						</div>
					<? endif; ?>
				</div>
				<? if (!empty($arItem["DETAIL_TEXT"])): ?>
					<?= $arItem["DETAIL_TEXT"] ?>
				<? endif; ?>
			</div>
		</section>
	<? endif; ?>
<? endforeach; ?>
<? if (strlen($arResult["UF_BOTTOM_DESC"])): ?>
	<section class="bottom-block"<?
	if ($arResult["UF_BOTTOM_BG"]): ?> style="background-image: url(<?= $arResult["IMAGES"][$arResult["UF_BOTTOM_BG"]] ?>);"<? endif; ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12"><?= $arResult["~UF_BOTTOM_DESC"] ?></div>
			</div>
		</div>
	</section>
<? endif; ?>