<?

use PB\Main\Loc;

if ($arResult["isFormErrors"] == "Y" ||
    ($_COOKIE["WEBFORM_" . $arResult["SOULT_FORM"]] && (empty($arParams['COMPONENT_MARKER']) ||
            $_REQUEST['marker'] == $arParams['COMPONENT_MARKER']) && $_REQUEST['WEB_FORM_ID'] == $arParams['WEB_FORM_ID'] && $_REQUEST['formresult'] == $arParams['FORM_RESULT_ADDOK'])
): ?>
    <?
    if ($arResult["isFormErrors"] == "Y") {
        $title = $arParams["MESS"]["ERROR"] ?: GetMessage("ERROR");
        $msg = $arParams["MESS"]["FORM_ERRORS_TEXT"] ?: $arResult["FORM_ERRORS_TEXT"];
    }
    if ($_REQUEST['formresult'] == $arParams['FORM_RESULT_ADDOK'] && $arResult["isFormErrors"] != "Y") {
        $title = $arParams["MESS"]["THANK_YOU"] ?: Loc::getMessage("PB_MAIN_BEFSEND_THANK_YOU");
        $msg = $arParams["MESS"]["WAIT_CALL"] ?: Loc::getMessage("PB_MAIN_BEFSEND_WAIT_CALL");
        //добавляем куку для блока "Не уходи"
        if ($arParams["BTN_CALL_FORM"] == "#before_exit_popup__data" && $arParams["DONT_RESHOW"]) {
           $host = parse_url($_SERVER['HTTP_HOST'], PHP_URL_HOST);
           $arHost = array_reverse(explode(".",$host));
           setcookie("ne_uhodi_used", true, time() + 360360000, "/", $arHost[1].".".$arHost[0]);
        }
    }
    $time = 10000;
    ?>
    <div id="popup">
        <div class="h3"><?= $title ?></div>
        <p>
            <?= $msg ?>
        </p>
        <? if ($arParams['ALERT_ADD_SHARE'] == "Y") {
            echo "<p>";
            $APPLICATION->IncludeComponent("bezr:empty", "social_share_ya", array("CACHE_TIME" => "N"));
            echo "</p>";
            $time = 10000;
        }
        foreach (GetModuleEvents("pb.main", "WriteComponentBannerPopup", true) as $arEvent) {        
        		ExecuteModuleEventEx($arEvent, array(&$this));        
        }
        ?>
    </div>
    <script type="text/javascript">
        (function(f){
            if(window.jQuery)
                $(document).ready(f);
            else
                document.addEventListener('DOMContentLoaded', f)
        })(
            function(){
                require(['main'], function () {
                    require(['Controls/Responsive', 'fancybox', 'cookie'], function (obResponsive) {
                        if ($.inArray(obResponsive.getCurType(), ['xsmall']) >= 0) {
                            var topRatio = 0;
                        } else {
                            var topRatio = 0.5;
                        }

                        var afterLoadFunction = function () {
                            $(document).trigger(
                                "form.result.popup.open",
                                [{
                                    'error': <?=$arResult["isFormErrors"] != "Y" ? 'false' : 'true';?>,
                                    'formId' : '<?=$arResult['arForm']['ID']?>',
                                    'formSalt': '<?=$arResult["SOULT_FORM"]?>'
                                }]
                            );
                            setTimeout(function () {
                                $.fancybox.close();
                            }, <?=$time?>);
                            deleteCookie("WEBFORM_<?=$arResult["SOULT_FORM"]?>","/");
                            <?if($arResult["isFormErrors"] != "Y"):?>
                            // emulate path for analytics target
                            var path = getCookie("trackStep");
                            if (path) {
                                trackStep(path + "/virtual/add-result-ok/");
                                deleteCookie("trackStep");
                            }
                            <?endif;?>
                        }

                        var
                            fancyBoxVersion = $.fancybox.version.split('.')[0];

                        if(fancyBoxVersion === '3') {
                            $.fancybox.open({
                                src  : '#popup',
                                type : 'inline',
                                opts : {
									 baseTpl:'<div class="fancybox-container fancybox-wrap " tabindex="-1" role="dialog"  ><div class="fancybox-bg"></div><div class="fancybox-skin" ><div class="fancybox-outer"><div class="fancybox-inner" ><div class="fancybox-stage"></div></div></div></div></div>',
                                    afterShow : afterLoadFunction
                                }
                            });
                        }else{
                            $.fancybox.open('#popup', {
                                padding: 0,
                                margin: 0,
                                topRatio: topRatio,
                                wrapCSS: 'inner-close',
                                afterLoad: afterLoadFunction
                            });
                        }
                    });
                });
                <?if($arResult["isFormErrors"] != "Y"):?>
                track(['<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_FORMS')?>', '<?=!empty($arParams['GA_SEND']['ACTION']) ? $arParams['GA_SEND']['ACTION'] : Loc::getMessage('PB_MAIN_BEFSEND_TRACK_SUCC')?>', '<?=$arResult['arForm']['NAME'] . " - " . $arResult['arForm']['ID']?>', 1]);
                <?
                if($arParams["EMAIL_ONLY_FROM_RESIPIENTS"] == 'N' && !empty($arParams["CITY_QUESTION_CODE"]) && !empty($arParams["OFFICE_QUESTION_CODE"]) && !empty($_REQUEST['RESULT_ID']) && empty($arParams['SERVICE_SECT_ID']))
                {
                $arAnswer = CFormResult::GetDataByID($_REQUEST['RESULT_ID'], array("OFFICE"), $arResultAns, $arAnswer2);
                if(!empty($arAnswer['OFFICE'][0]['USER_TEXT']))
                {
                ?> track(['<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_FORM_SEND')?>', '<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_OFFICE_FROM_QUEUE')?>', '<?=$arAnswer['OFFICE'][0]['USER_TEXT'] . " - " . $arResult['arForm']['ID']?>', 1]); <?
                }
                }
                ?>
                <?else:?>
                track(['<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_FORMS')?>', '<?=Loc::getMessage('PB_MAIN_BEFSEND_TRACK_ERROR')?>', '<?=$arResult['arForm']['NAME'] . " - " . $arResult['arForm']['ID']?>', 1]);
                <?endif;?>
            }
        )
    </script>
<? endif; ?>