if (typeof define === "function" && define.amd) {
    define([], function () {
        function ReCaptchaProvider() {
			if ($('.g-recaptcha[data-recaptcha-version="v3" ]').length > 0){
				var siteKey = $('.g-recaptcha[data-recaptcha-version="v3" ]').attr('data-sitekey');
				this.googleScriptReCaptcha = 'https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=' + siteKey + '&hl=ru';
			}else{
				this.googleScriptReCaptcha = 'https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit&hl=ru';
			}

       		this.init();
        }

        ReCaptchaProvider.prototype.init = function () {
            var o = this;
            var elemScriptGoogle = document.createElement('script');
		    elemScriptGoogle.type = 'text/javascript';
		    elemScriptGoogle.async = true;
		    elemScriptGoogle.defer = true;
		    elemScriptGoogle.src = o.googleScriptReCaptcha;
		    var elem = document.querySelector('script[nonce]');
		    var nonce = elem && (elem['nonce'] || elem.getAttribute('nonce'));
		    if (nonce) {
		        elemScriptGoogle.setAttribute('nonce', nonce);
		    }
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(elemScriptGoogle, s);
        };

        window.CaptchaCallback = function() { };

        return ReCaptchaProvider;
    });
}
