<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Подписка");
?>
	<section class="client">
		<div class="container">
			<div class="row">
				<? $APPLICATION->IncludeComponent(
					"bitrix:subscribe.edit",
					"",
					Array(
						"AJAX_MODE" => "N",
						"AJAX_OPTION_ADDITIONAL" => "",
						"AJAX_OPTION_HISTORY" => "N",
						"AJAX_OPTION_JUMP" => "N",
						"AJAX_OPTION_STYLE" => "Y",
						"ALLOW_ANONYMOUS" => "Y",
						"CACHE_TIME" => "3600",
						"CACHE_TYPE" => "A",
						"SET_TITLE" => "N",
						"SHOW_AUTH_LINKS" => "N",
						"SHOW_HIDDEN" => "N"
					)
				); ?>
			</div>
		</div>
	</section>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>