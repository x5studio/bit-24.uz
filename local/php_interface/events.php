<?php
$eventManager = \Bitrix\Main\EventManager::getInstance();
// Set Rating
//$eventManager->addEventHandler("","ReviewByProductOnAfterUpdate",array("\\Proman\\ReviewByProduct","OnAfterUpdate"));
//$eventManager->addEventHandler("","ReviewByProductOnDelete",array("\\Proman\\ReviewByProduct","OnDelete"));


$eventManager->addEventHandler("main","OnBeforeProlog", function(){
    
    // geo for bots
    if(\Bitrix\Main\Loader::includeModule('pb.geo')){
        include_once(\Bitrix\Main\Application::getDocumentRoot().'/local/php_interface/include/BotChecker.php');

        if(\BotChecker::isBot()){

            $app = \PB\Geo\CAllGeo::getInstance();
            $domainData = $app->getDomainData();
            $majorCity =  $app->getMajorCity($domainData['REGION']);

            
            $redirect = $app->saveGeo($majorCity);
            $_SESSION['REGION'] = array_merge(
                $app->getSection(array('ID' => $majorCity), true),
                $app->d
            );

            $_SESSION['NOT_CHECK_COOKIE'] = 'Y';

        }
    }
}, false, 10);