<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */
/** @global CCacheManager $CACHE_MANAGER */

use PB\Main\Loc;

/*************************************************************************
Processing of received parameters
 *************************************************************************/

global $obOfficeFilter;

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	global ${$arParams["FILTER_NAME"]};
	$arrFilter = ${$arParams["FILTER_NAME"]};
	if(!is_array($arrFilter))
		$arrFilter = array();
}

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

if(!isset($arParams["COMPONENT_MARKER"]))
	$arParams["COMPONENT_MARKER"] = $this->randString();

$arParams["CACHE_FILTER"]=$arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
	$arParams["CACHE_TIME"] = 0;

$arParams['IS_STOP_RANDOM'] = 'Y';

/*************************************************************************
Work with cache
 *************************************************************************/

if (!in_array($_SESSION['REGION']['ID'], $arParams['STOP_RANDOM'])) {
	$arParams["CACHE_TYPE"] = 'N';
}

$contMng = ContactManager::getInstance();

$arResult['ITEMS'] = $contMng->getOfficeList($obOfficeFilter, $arrFilter);
if (is_array($arResult['ITEMS'])) {
	$arParams['ELEMENTS'] = array_keys($arResult['ITEMS']);
}

if($this->StartResultCache(false, array($arrFilter, ($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()))))
{
    if(empty($arResult['ITEMS'])) {
        unset($arrFilter['IBLOCK_SECTION_ID']);
        $arrFilter["CODE"] = 'proletarskaya';
        $arResult['ITEMS'] = $contMng->getOfficeList($obOfficeFilter, $arrFilter);
    }

	foreach($arResult["ITEMS"] as $keyItem => &$arItem) {
		if($arItem["CODE"] == "lomonosovskaya") {
			$arItem["NAME"] = Loc::getMessage('PB_CONTACTS_CT_CB_LOMONOSOVSKAYA');
		}
		if($arItem["CODE"] == "proletarskaya") {
			$arItem["NAME"] = Loc::getMessage('PB_CONTACTS_CT_CB_PROLETARSKAYA');
		}
	}
	unset($arItem);

	/* handler */
	$CNT = count($arResult["ITEMS"])-2;
	if($CNT > 2) {
		$arResult['OFFICE_TEXT'] = Loc::getMessage('PB_CONTACTS_CT_CB_MORE').' '.declOfNum($CNT, array(Loc::getMessage('PB_CONTACTS_CT_CB_OFFICE'), Loc::getMessage('PB_CONTACTS_CT_CB_OFFICES_1'), Loc::getMessage('PB_CONTACTS_CT_CB_OFFICES_2')));
	}

// Если указан офис в параметре STOP_RANDOM, отключаем рандом жд.
	$STOP_RANDOM = 'N';
	foreach($arResult["ITEMS"] as $OFFICE) {
		if(is_array($arParams['STOP_RANDOM'])) {
			if(in_array($OFFICE['IBLOCK_SECTION_ID'], $arParams['STOP_RANDOM'])) {
				$STOP_RANDOM = 'Y'; break;
			}
		}
		if(intval($OFFICE['PROPERTIES']['POWER_PHONE']['VALUE']) > 0) {
			$_sorts[] = $OFFICE['PROPERTIES']['POWER_PHONE']['VALUE'];
		}
	}
	if($STOP_RANDOM == 'N' && count($_sorts) <= 0) {
		$STOP_RANDOM = 'Y';
	}

// Если все офисы в городе имеют один вес, отключаем рандом.
	/*
	if($STOP_RANDOM == 'N') {
		$_offices = array_unique($_sorts);
		if(count($_offices) == 1) {
			$STOP_RANDOM = 'Y';
		}
	}
	*/

// Получаем случайный офис, с учетом веса
// т.е. если у офиса вес больше других, он будет показываться чаще других

	//echo '<h2>$itemID</h2><pre>' . htmlspecialchars(print_r($arResult["ITEMS"], 1)) . '</pre>';
	if(count($arResult["ITEMS"]) >= 1 && $STOP_RANDOM == 'N' && count($_sorts) == 1) {
		usort($arResult["ITEMS"], function ($a, $b){

			$aRank = intval($a['PROPERTIES']['POWER_PHONE']['VALUE']) || 0;
			$bRank = intval($b['PROPERTIES']['POWER_PHONE']['VALUE']) || 0;

			return strcasecmp($bRank, $aRank);
		});
	}
	
	
	if(count($arResult["ITEMS"]) >= 2 && $STOP_RANDOM == 'N' && count($_sorts) >= 2) {

		$arParams['IS_STOP_RANDOM'] = 'N';

		/**
		 * Получаем случайный офис, с учетом веса
		 * @param $nodes
		 * @return mixed
		 */
		if(!function_exists('generateWeighedRandomValue')){
		function generateWeighedRandomValue($nodes) {
			$weights = array_values($nodes);
			$values = array_keys($nodes);
			$count = count($values);
			$i = 0;
			$n = 0;
			$num = mt_rand(0, array_sum($weights));
			while($i < $count) {
				$n += $weights[$i];
				if($n >= $num) {
					break;
				}
				$i++;
			}
			return $values[$i];
		}
		}

		/**
		 * Получаем имя из даты
		 * @return string
		 */
		if(!function_exists('getDateFolder')){
		function getDateFolder() {
			$date = new DateTime();
			$name = $date->format('Ym');
			return $name;
		}
		}

		/**
		 * Записываем статистику показа офисов
		 * @param $ID
		 */
		if(!function_exists('recordOffice')){
		function recordOffice($ID) {

			$debug = false;
			$IBLOCK = 29;
			$el = new CIBlockElement();

			$office = $el->GetList(array(),
				array(
					'IBLOCK_ID'=>$IBLOCK,
					'PROPERTY_OFFICE'=>$ID,
					'ACTIVE' => 'Y'
				), false, false,
				array(
					'ID',
					'NAME',
					'PROPERTY_COUNT'
				)
			)->Fetch();

			if(isset($office['PROPERTY_COUNT_VALUE'])) {

				// Если офис есть в статистике увеличиваем его счетчик
				$el->SetPropertyValues($office['ID'], $IBLOCK, ++$office['PROPERTY_COUNT_VALUE'], 'COUNT');
			} else {

				global $USER;
				$OFFICE = $el->GetByID($ID)->Fetch();
				$sc = new CIBlockSection();

				// Получаем|Создаем корневую категорию с датой для статистики
				$STAT_NAME = getDateFolder();
				$STAT_NAME_ = $sc->GetList(array(), array('IBLOCK_ID'=>$IBLOCK, 'NAME'=>$STAT_NAME))->Fetch();
				$STAT_ID = intval($STAT_NAME_['ID']);
				if(!$STAT_ID) {
					if(!$debug || $USER->GetID() == 204) {
						$STAT_ID = $sc->Add(array(
							"ACTIVE" => "Y",
							"IBLOCK_SECTION_ID" => false,
							"IBLOCK_ID" => $IBLOCK,
							"NAME" => $STAT_NAME
						));
					}
				}

				// Получаем|Создаем категорию
				$SECTION33 = $sc->GetByID($OFFICE['IBLOCK_SECTION_ID'])->Fetch();
				$SECTION = $sc->GetList(array(), array('IBLOCK_ID'=>$IBLOCK, 'CODE'=>$SECTION33['CODE']))->Fetch();
				$SECTION_ID = intval($SECTION['ID']);

				if(!$SECTION_ID) {
					if(!$debug || $USER->GetID() == 204) {
						$SECTION_ID = $sc->Add(array(
							"ACTIVE" => "Y",
							"IBLOCK_SECTION_ID" => $STAT_ID,
							"IBLOCK_ID" => $IBLOCK,
							"NAME" => $SECTION33['NAME'],
							"CODE" => $SECTION33['CODE']
						));
					}
				}

				// ADD RECORD
				if(!$debug || $USER->GetID() == 204) {
					$record = $el->Add(array(
						"MODIFIED_BY"    => $USER->GetID(),
						"IBLOCK_SECTION_ID" => $SECTION_ID,
						"IBLOCK_ID"      => $IBLOCK,
						"NAME"           => $OFFICE['NAME'],
						"ACTIVE"         => "Y",
						"PROPERTY_VALUES" => array(
							'OFFICE' => $ID,
							'COUNT' => 1
						)
					));
				}
			}
		}
		}

		/**
		 * Фильтр дублей офисов
		 * @param $arr
		 * @param $id_compare
		 *
		 * @return mixed
		 */
		if(!function_exists('second_office')){
		function second_office($arr, $id_compare) {
			$id = generateWeighedRandomValue($arr);
			if($id == $id_compare) {
				$id = second_office($arr, $id_compare);
			}
			
			return $id;
		}
		}

		// START
		$reArr = $arr = array();
		foreach($arResult["ITEMS"] as $arItem) {
			$reArr[$arItem['ID']] = $arItem;
			$arr[$arItem['ID']] = intval($arItem['PROPERTIES']['POWER_PHONE']['VALUE']);
		}
		$arResult['ITEMS'] = $reArr;

		$first_office['ID'] = generateWeighedRandomValue($arr);
		$second_office['ID'] = second_office($arr, $first_office['ID']);
		
		recordOffice($first_office['ID']);
		recordOffice($second_office['ID']);

		$first_office = $arResult['ITEMS'][$first_office['ID']];
		$second_office = $arResult['ITEMS'][$second_office['ID']];

		unset($arResult['ITEMS'][$first_office['ID']]);
		unset($arResult['ITEMS'][$second_office['ID']]);

		$office_block = array(
			$first_office,
			$second_office
		);

		$sort = array();
		array_push($sort, $first_office['SORT']);
		array_push($sort, $second_office['SORT']);

		array_unshift($arResult['ITEMS'], $second_office);
		array_unshift($arResult['ITEMS'], $first_office);

		// ПРОВЕРКА ФОРМУЛЫ - НЕ УДАЛЯТЬ!
		if($_REQUEST['STAT_OFFICE'] == 'Y') {
			foreach($arResult["ITEMS"] as $arItem) {
				$names[$arItem['ID']] = $arItem['NAME'];
			}

			for ($i = 0; $i < 10000; ++$i) {
				$values[$names[generateWeighedRandomValue($arr)]]++;
			}
			arsort($values);
			__($values);
		}
	}

	$this->IncludeComponentTemplate();

}
