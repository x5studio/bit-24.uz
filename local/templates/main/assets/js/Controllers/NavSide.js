if (typeof define === "function" && define.amd) {
    define(['easing'], function () {
        function NavSide(container) {
            this.container = $(container);
            this.dt = this.container.find('dt');
            this.dd = this.container.find('dd');
            this.dt.each(function () {
                var _this = $(this);
                var h = $(this).next('dd').height();
                if (!$(this).hasClass('nav-side-active')) {
                    $(this).next('dd').css('height', 0);
                }
                $(this).click(function () {
                    if ($(this).hasClass('nav-side-active')) {
                        $(this).removeClass('nav-side-active').next('dd').removeClass('nav-side-active').animate({height: 0}, 200, 'easeOutCubic');
                    } else {
                        if ($(this).siblings('dt.nav-side-active').length) {
                            $(this).siblings('dt.nav-side-active').removeClass('nav-side-active').next('dd').removeClass('nav-side-active').animate({height: 0}, 200, 'easeOutCubic', function () {
                                _this.addClass('nav-side-active').next('dd').addClass('nav-side-active').animate({height: h}, 400, 'easeOutCubic');
                            });
                        } else {
                            $(this).addClass('nav-side-active').next('dd').addClass('nav-side-active').animate({height: h}, 400, 'easeOutCubic');
                        }
                    }
                })
            });
        }

        return NavSide;
    });
}