<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	\Proman\Helper;

/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

/**
 * @var CBlogListComponent $this
 */


CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
$arParams["TAG"] = intval($arParams["TAG"]);

$arParams["COUNT"] = intval($arParams["COUNT"]);
if ($arParams["COUNT"] <= 0)
	$arParams["COUNT"] = 12;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"] == "Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"] != "N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] != "N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"] == "Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"] !== "N";

if ($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if ($arNavigation["PAGEN"] == 0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] > 0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
} else
{
	$arNavParams = array(
		"nTopCount" => $arParams["COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

if ($this->StartResultCache(false, array($arNavigation)))
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arEnumSite = \FirstBit\Util::getArrEnumValueByXmlID(SITE_ID, 'RELATED_SITE', $arParams['IBLOCK_ID']);
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		//"<=DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "SHORT")
		'ACTIVE_DATE' => 'Y',
        'PROPERTY_RELATED_SITE' => [false, $arEnumSite['ID']]
	);

	if (strlen($arParams["SECTION_CODE"]))
	{
		$rsSection = \CIBlockSection::GetList(
			array(),
			array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ACTIVE" => "Y",
				"CODE" => $arParams["SECTION_CODE"]
			),
			false,
			array("ID", "NAME", "SECTION_PAGE_URL", "UF_FORM_TITLE"));
		if ($arResult = $rsSection->GetNext())
		{
			$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arResult["ID"]);
			$arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();
			$arFilter["SECTION_ID"] = $arResult["ID"];
		}
	}

	if ($arParams["TAG"])
	{
		$arFilter["PROPERTY_TAG"] = $arParams["TAG"];
	}

	$arFile = array();

	$rsElement = \CIBlockElement::GetList(
		array("ACTIVE_FROM" => "DESC"),
		$arFilter,
		false,
		$arNavParams,
		array("ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PICTURE", "ACTIVE_FROM")
	);
	while ($arElement = $rsElement->GetNext())
	{

		$arFile[] = $arElement["PREVIEW_PICTURE"];
		$arFile[] = $arElement["DETAIL_PICTURE"];

		$arElement["DISPLAY_ACTIVE_FROM"] = ToLower(FormatDate("d F Y", MakeTimeStamp($arElement["ACTIVE_FROM"])));

		$arResult["ITEMS"][] = $arElement;
	}

    $arResult["SECTIONS"] = $this->GetFitSection($arFilter);
	$arResult["IMAGES"] = Helper::getImages($arFile);
	if (count($arResult["SECTIONS"]) > 0)
	{
		$rsSection = \CIBlockSection::GetList(
			array(),
			array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ID" => array_keys($arResult["SECTIONS"])
			),
			false,
			array("ID", "NAME", "SECTION_PAGE_URL"));
		while ($arSec = $rsSection->GetNext())
		{
			$arResult["SECTIONS"][$arSec["ID"]] = $arSec;
		}
	}

	$arResult["NAV_STRING"] = $rsElement->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
	$arResult["NAV_CACHED_DATA"] = $navComponentObject->GetTemplateCachedData();

	$this->SetResultCacheKeys(array("ID", "IPROPERTY_VALUES", "NAV_CACHED_DATA", "NAME", "SECTION_PAGE_URL", "~UF_FORM_TITLE"));
	$this->IncludeComponentTemplate();
}
if (isset($arResult["ID"]))
{
	$arTitleOptions = null;

	if (!empty($arResult["IPROPERTY_VALUES"]))
	{
		if ($arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"] != "")
			$APPLICATION->SetPageProperty("title", $arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"], $arTitleOptions);
		else
			$APPLICATION->SetPageProperty("title", $arResult["NAME"], $arTitleOptions);

		if ($arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"] != "" && $arParams['H1_AS_NAME'] != 'Y')
			$APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"], $arTitleOptions);
		else
		    if($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]){
                $APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arTitleOptions);
            }
            else{
			    $APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
            }

		if ($arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"] != "")
			$APPLICATION->SetPageProperty("keywords", $arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"], $arTitleOptions);
		if ($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"] != "")
			$APPLICATION->SetPageProperty("description", $arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"], $arTitleOptions);
	}

	$APPLICATION->AddChainItem($arResult["NAME"], $arResult["SECTION_PAGE_URL"]);
}
