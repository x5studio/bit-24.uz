<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Отраслевая экспертиза. На главную",
	"DESCRIPTION" => "Отраслевая экспертиза. На главную",
	"ICON" => "/images/news_line.gif",
	"SORT" => 100,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "expertise",
			"NAME" => "Отраслевая экспертиза"
		)
	),
);

?>