<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if ($arResult['ITEMS'])
{
    foreach ($arResult['ITEMS'] as $i => $arItem)
    {
        $arResult['ITEMS'][$i]["DISPLAY_ACTIVE_FROM"] = ToLower(FormatDate("d F Y", MakeTimeStamp($arItem["ACTIVE_FROM"])));
        $testimonialFile = CFile::GetFileArray($arItem['PROPERTIES']['TESTIMONIAL_FILE']['VALUE']);
        $arResult['ITEMS'][$i]['TESTIMONIAL_FILE_SRC'] = $testimonialFile ? $testimonialFile['SRC'] : null;
    }
}