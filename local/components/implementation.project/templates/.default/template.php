<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult["ITEMS"])): ?>
	<section class="phase-block">
		<div class="line">
			<div class="container">
				<h2>Этапы проектного внедрения <span>Битрикс24</span></h2>
				<div class="row">
					<? foreach ($arResult["ITEMS"] as $cell => $arItem):
						$line = ceil(($cell + 1) / 3);
						if ($line == 1):
							?>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="one">
									<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
									<p><?= $arItem["NAME"] ?></p>
									<h4><?= ($cell + 1) ?></h4>
								</div>
							</div>
							<?
						elseif ($line == 2):
							if ($cell % 3 == 2):
								?>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<div class="two">
										<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
										<p><?= $arItem["NAME"] ?></p>
										<h4><?= ($cell + 1) ?></h4>
									</div>
								</div>
								<div class="col-md-4 col-sm-4 hidden-xs">
									<div class="two">
										<img src="<?= $arResult["IMAGES"][$arResult["ITEMS"][$cell - 1]["PREVIEW_PICTURE"]] ?>"
												 class="img-responsive">
										<p><?= $arResult["ITEMS"][$cell - 1]["NAME"] ?></p>
										<h4><?= $cell ?></h4>
									</div>
								</div>
								<div class="col-md-4 col-sm-4 hidden-xs">
									<div class="two">
										<img src="<?= $arResult["IMAGES"][$arResult["ITEMS"][$cell - 2]["PREVIEW_PICTURE"]] ?>"
												 class="img-responsive">
										<p><?= $arResult["ITEMS"][$cell - 2]["NAME"] ?></p>
										<h4><?= ($cell - 1) ?></h4>
									</div>
								</div>
								<?
							else:
								?>
								<div class="hidden-lg hidden-md hidden-sm col-xs-12">
									<div class="two">
										<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
										<p><?= $arItem["NAME"] ?></p>
										<h4><?= ($cell + 1) ?></h4>
									</div>
								</div>
								<?
							endif;
						elseif ($line == 3):
							?>
							<div class="col-md-4 col-sm-4 col-xs-12">
								<div class="three">
									<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
									<p><?= $arItem["NAME"] ?></p>
									<h4><?= ($cell + 1) ?></h4>
								</div>
							</div>
							<?
						endif;
						if ($cell % 3 == 2) echo '</div><div class="row">';
					endforeach; ?>
					<div class="col-ffset-md-4 col-ffset-sm-4 hidden-xs"></div>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>