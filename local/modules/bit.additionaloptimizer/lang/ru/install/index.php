<?
$MESS["BIT_ADO_NAME"]	   = "Расширенная оптимизация";
$MESS["BIT_ADO_DESCRIPTION"]	   = "";
$MESS["BIT_ADO_INSTALL_ERROR_VERSION"] = "Версия ядра должна быть > 17.00 и версия PHP > 7.1.0";
$MESS["BIT_ADO_INSTALL_TITLE"]   		= "Установка модуля";
$MESS["BIT_ADO_UNINSTALL_TITLE"]   		= "Удаление модуля";
$MESS['BIT_ADO_PARTNER_NAME'] = 'Первый бит';
$MESS['BIT_ADO_PARTNER_URI'] = 'https://1cbit.ru/';
