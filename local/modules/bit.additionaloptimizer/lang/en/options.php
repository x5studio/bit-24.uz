<?
$MESS["BIT_ADO_OPTIONS_TAB_NAME"] 		 	           = "Settings";
$MESS["BIT_ADO_OPTIONS_TAB_COMMON"] 	 	           = "Base";
$MESS["BIT_ADO_OPTIONS_TAB_SWITCH_ON"]  	           = "Turn on module";
$MESS["BIT_ADO_OPTIONS_TAB_APPEARANCE"] 	           = "Base settings";
$MESS["BIT_ADO_OPTIONS_TAB_TEST"] 	                   = "Tester settings";
$MESS["BIT_ADO_OPTIONS_TAB_LOCALITY_FIELD_CONTACT"]    = "Contact Field with user location";
$MESS["BIT_ADO_OPTIONS_TAB_LOCALITY_FIELD_REGEX_CONTACT"] 	   = "Regex to parse contact user location";
$MESS["BIT_ADO_OPTIONS_TAB_LOCALITY_FIELD_LEAD"]        = "Lead Field with user location";
$MESS["BIT_ADO_OPTIONS_TAB_LOCALITY_FIELD_REGEX_LEAD"] 	   = "Regex to parse lead user location";
$MESS["BIT_ADO_OPTIONS_TAB_MESSAGE_TO_ACTIVITY"] 	   = "Message to entity activity";
$MESS["BIT_ADO_OPTIONS_TAB_PRIMARY_COUNTRY_FIELD"] 	   = "Primary countries list (Each on new line)";
$MESS["BIT_ADO_OPTIONS_TAB_TESTER"] 	               = "Field to test";
$MESS["BIT_ADO_OPTIONS_TAB_TESTER_COUNTRY"]            = "Country to test";
$MESS["BIT_ADO_OPTIONS_TAB_TESTER_BUTTON"]             = '<button class="run-test adm-btn adm-btn-save">RUN TEST</button>';

$MESS["BIT_ADO_OPTIONS_TAB_TESTER_USE"] 	            = "Enter Phone number and press Enter";
$MESS["BIT_ADO_OPTIONS_TAB_TESTER_RESULT"] 	            = "Test result";