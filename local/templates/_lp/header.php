<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

global $APPLICATION, $USER;
use Bitrix\Main\Page\Asset;

global $obOfficeFilter, $obCityFilter;

$obCityFilter = new ContactManagerFilter();
$obCityFilter->setGeneral(Array(
	'UF_HIDE_IN_CITY_LIST' => false,
));

$obOfficeFilter = new ContactManagerFilter();
$obOfficeFilter->setGeneral(Array('PROPERTY_HIDE_IN_CITY_LIST' => false));

$contMng = ContactManager::getInstance();
$arOfficeList = $contMng->getOfficeList($obOfficeFilter, Array('IBLOCK_SECTION_ID' => $_SESSION['REGION']['ID']));

if (count($arOfficeList) == 0)
{
	$arOfficeList = $contMng->getOfficeList($obOfficeFilter, Array('CODE' => 'moskva'));
}
$cities = array_shift($arOfficeList);
$emailCity = $cities["PROPERTIES"]["email"]["VALUE"];
if (defined("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE'])) && constant("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE'])) != '')
{
	$emailCity .= ", " . constant("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE']));
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <?$APPLICATION->ShowHead()?>
    <title><?$APPLICATION->ShowTitle()?></title>
</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>