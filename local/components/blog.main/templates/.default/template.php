<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<? if (!empty($arResult["ITEMS"])): ?>
	<section class="our-blog">
		<div class="container">
			<div class="row why-row">
				<div class="col-md-8 col-sm-8">
					<h2 class="blog-heading pull-left">Интересное про автоматизацию</h2>
				</div>
				<div class="col-md-4 col-sm-4 hidden-xs">
					<a href="/blog/" class="btn btn-blog pull-right">Посмотреть все</a>
				</div>
			</div>
			<div id="myCarousel" class="carousel slide hidden-md hidden-lg hidden-sm col-xs-12" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators col-xs-6">
					<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
						<li data-target="#myCarousel" data-slide-to="<?= $cell ?>"<? if (!$cell): ?>
							class="active"<? endif; ?>><span></span></li>
					<? endforeach; ?>
				</ol>
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
						<div class="carousel-item item<? if (!$cell): ?> active<? endif; ?>">
							<div class="blog1">
								<div class="b-img effect">
									<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive" alt="<?=
									$arItem["NAME"]
									?>"/>
									<div class="mask">
										<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="info"></a>
									</div>
								</div>
								<h4 class="text-left"><?= $arItem["NAME"] ?></h4>
								<div class="row row-text vertical-align">
									<div class="col-md-6 col-sm-6 col-xs-6 text text-left">
										<h5><a href="<?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["SECTION_PAGE_URL"] ?>"
											><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></a></h5>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6 date text-right">
										<span class="date"><i class="fa fa-clock-o"
																					aria-hidden="true"></i><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
									</div>
								</div>
								<p class="hidden-xs"><?= $arItem["PREVIEW_TEXT"] ?></p>
								<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" title="<?= $arItem["NAME"] ?>">Подробнее</a>
							</div>
						</div>
					<? endforeach; ?>
				</div>
				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
					<i class="fa fa-long-arrow-left" aria-hidden="true"></i>
					<span class="sr-only">Previous</span>
				</a>
				<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
					<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					<span class="sr-only">Next</span>
				</a>
			</div>
			<div class="container col-md-12 hidden-xs">
				<div class="row">
					<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
						<div class="col-sm-6 col-md-4 hidden-xs">
							<div class="blog1 pull-left">
								<div class="b-img effect">
									<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive" alt="<?=
									$arItem["NAME"]
									?>"/>
									<div class="mask">
										<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="info"></a>
									</div>
								</div>
								<div class="sect-heading"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><?= $arItem["NAME"] ?></a></div>
								<div class="row row-text vertical-align">
									<div class="col-md-6 col-sm-6 col-xs-6 text text-left">
										<div class="sect-title">
											<a href="<?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["SECTION_PAGE_URL"] ?>"
											><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></a>
										</div>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6 date text-right">
										<span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
									</div>
								</div>
								<noindex><p class="hidden-xs"><?= $arItem["PREVIEW_TEXT"] ?></p></noindex>
								<?/*<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" title="<?= $arItem["NAME"] ?>">Подробнее</a>*/?>
							</div>
						</div>
					<? endforeach; ?>
				</div>
			</div>
			<div class="hidden-lg hidden-md hidden-sm col-xs-12 detline">
				<a href="/blog/" class="btn btn-blog pull-right">Посмотреть все</a>
			</div>
		</div>
	</section>
	<?/*
	<script>
      $(document).ready(function () {
          $("#myCarousel").swiperight(function () {
              $("#myCarousel").carousel('prev');
          });
          $("#myCarousel").swipeleft(function () {
              $("#myCarousel").carousel('next');
          });
      });
	</script>
*/?>
<? endif; ?>