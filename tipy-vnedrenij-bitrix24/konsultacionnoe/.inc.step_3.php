<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<noindex>
<div class="mattop">
	<h2>Шаг 3 — обучение</h2>
	<h3>Персонализированное обучение <br>
	 Теория и практика на примере ваших реальных бизнес-кейсов</h3>
</div>
<div class="include">
	<h3>Что входит:</h3>
	<div class="row">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<ul>
				<li>Обучение — обязательный этап для успешного запуска Битрикс24 в компании;</li>
				<li>Обучение проводится по ролям и функциям тех пользователей, которые будут работать в дальнейшем с системой;</li>
			</ul>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12">
			<ul>
				<li>Обучение проводится как в очном, так и дистанционном формате;</li>
				<li>Простые пакеты обучения делают легче выбор среди них.</li>
			</ul>
		</div>
	</div>
</div>
<div class="step-bottom hidden-lg hidden-md hidden-xs col-sm-12 footbtn">
	<div class="row">
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Базовый</h3>
			<p>Для тех, кто хочет настроить рабочий стол Битрикс24 для своих сотрудников и провести базовое обучение работе с основными инструментами портала.</p>
 			<a href="#" data-target="#modal-more-4"
				 data-type="" data-toggle="modal">Подробнее</a>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h4>от 9 960 руб.</h4>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
 					<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
									data-form-field-type="Консультационное внедрение: Базовый">Купить</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Менеджер CRM</h3>
			<p>Включает в себя пакет "Базовый", а так же работу в блоке CRM — полный цикл ведения продажи.</p>
 			<a href="#" data-target="#modal-more-5"
				 data-type="" data-toggle="modal">Подробнее</a>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h4>от 9 960 руб.</h4>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
									data-form-field-type="Консультационное внедрение: Менеджер CRM">Купить</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Задачи и проекты</h3>
			<p>Включает в себя пакет "Базовый", а также работу с проектами и задачами.</p>
 			<a href="#" data-target="#modal-more-6"	data-type="" data-toggle="modal">Подробнее</a>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h4>от 96 000 руб</h4>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
 					<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
									data-form-field-type="Консультационное внедрение: Задачи и проекты">Купить</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Директор</h3>
			<p>Включает в себя инструменты настройки портала и аналитики деятельности компании. Все, что нужно для успешного управления организацией!</p>
			<a href="#" data-target="#modal-more-7"	data-type="" data-toggle="modal">Подробнее</a>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h4>от 96 000 руб</h4>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
									data-form-field-type="Консультационное внедрение: Директор">Купить</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Подготовка инструкции/регламента</h3>
			<p>Подготовим для Вас электронные версии инструкций и регламентов, применительно к бизнес-процессам, работающим в Вашей компании.</p>
			<a href="#" data-target="#modal-more-8"	data-type="" data-toggle="modal">Подробнее</a>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h4>от 6 000 руб</h4>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
									data-form-field-type="Консультационное внедрение: Подготовка инструкции/регламента">Купить</button>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Подготовка видеоинструкции</h3>
			<p>Подготовим для Вас и Ваших сотрудников наглядные инструкции по работе на портале.</p>
			<a href="#" data-target="#modal-more-9"	data-type="" data-toggle="modal">Подробнее</a>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<h4>от 8 000 руб</h4>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
									data-form-field-type="Консультационное внедрение: Подготовка видеоинструкции">Купить</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="bottom-slider footbtn">
	<div class="row">
		<div class="col-md-12 hidden-sm col-xs-12">
			<div id="bCarousel" class="carousel carousel-showmanymoveone slide" data-ride="carousel">
				 <!-- Indicators -->
				<ol class="carousel-indicators hidden-lg hidden-md hidden-sm">
					<li data-target="#bCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#bCarousel" data-slide-to="1"></li>
					<li data-target="#bCarousel" data-slide-to="2"></li>
					<li data-target="#bCarousel" data-slide-to="3"></li>
					<li data-target="#bCarousel" data-slide-to="4"></li>
					<li data-target="#bCarousel" data-slide-to="5"></li>
				</ol>
				 <!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<div class="col-lg-4 col-xs-12 col-md-4 col-sm-6">
							<div class="project1">
								<h3>Базовый</h3>
								<p>Для тех, кто хочет настроить рабочий стол Битрикс24 для своих сотрудников и провести базовое обучение работе с основными инструментами портала.</p>
 								<a href="#" data-target="#modal-more-4" data-type="" data-toggle="modal">Подробнее</a>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<h4>от 9 960 руб.</h4>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
														data-form-field-type="Консультационное внедрение: Базовый">Купить</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="col-lg-4 col-xs-12 col-md-4 col-sm-6">
							<div class="project1">
								<h3>Менеджер CRM</h3>
								<p>Включает в себя пакет "Базовый", а так же работу в блоке CRM — полный цикл ведения продажи.</p>
								<a href="#" data-target="#modal-more-5" data-type="" data-toggle="modal">Подробнее</a>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<h4>от 9 960&nbsp;руб.</h4>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
 										<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
														data-form-field-type="Консультационное внедрение: Менеджер CRM">Купить</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="col-lg-4 col-xs-12 col-md-4 hidden-sm">
							<div class="project1">
								<h3>Задачи и проекты</h3>
								<p>Включает в себя пакет "Базовый", а также работу с проектами и задачами.</p>
								<a href="#" data-target="#modal-more-6" data-type="" data-toggle="modal">Подробнее</a>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<h4>от 9 960 руб.</h4>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
 										<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
														data-form-field-type="Консультационное внедрение: Задачи и проекты">Купить</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="col-lg-4 col-xs-12 col-md-4 hidden-sm">
							<div class="project1">
								<h3>Директор</h3>
								<p>Включает в себя инструменты настройки портала и аналитики деятельности компании. Все, что нужно для успешного управления организацией!</p>
								<a href="#" data-target="#modal-more-7" data-type="" data-toggle="modal">Подробнее</a>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<h4>от 9 960 руб.</h4>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
														data-form-field-type="Консультационное внедрение: Директор">Купить</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="col-lg-4 col-xs-12 col-md-4 hidden-sm">
							<div class="project1">
								<h3>Подготовка инструкции/регламента</h3>
								<p>Подготовим для Вас электронные версии инструкций и регламентов, применительно к бизнес-процессам, работающим в Вашей компании.</p>
								<a href="#" data-target="#modal-more-8" data-type="" data-toggle="modal">Подробнее</a>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<h4>от 6 000 руб.</h4>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
														data-form-field-type="Консультационное внедрение: Подготовка инструкции/регламента">Купить</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="col-lg-4 col-xs-12 col-md-4 hidden-sm">
							<div class="project1">
								<h3>Подготовка видеоинструкции</h3>
								<p>Подготовим для Вас и Ваших сотрудников наглядные инструкции по работе на портале.</p>
								<a href="#" data-target="#modal-more-9" data-type="" data-toggle="modal">Подробнее</a>
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<h4>от 8 000 руб.</h4>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button type="button" class="btn btn-default submit" data-toggle="modal" data-target="#advanced"
														data-form-field-type="Консультационное внедрение: Подготовка видеоинструкции">Купить</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				 <!-- Left and right controls -->
				<div class="hidden-xs">
 <a class="left carousel-control" href="#bCarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#bCarousel" role="button" data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i> <span class="sr-only">Next</span> </a>
				</div>
 <a class="left carousel-control hidden-sm hidden-lg hidden-md col-xs-6" href="#bCarousel" role="button" data-slide="prev"> <i class="fa fa-long-arrow-left" aria-hidden="true"></i> <span class="sr-only">Previous</span> </a> <a class="right carousel-control hidden-sm hidden-lg hidden-md col-xs-6" href="#bCarousel" role="button" data-slide="next"> <i class="fa fa-long-arrow-right" aria-hidden="true"></i> <span class="sr-only">Next</span> </a>
			</div>
		</div>
	</div>
</div>
<br>
</noindex>