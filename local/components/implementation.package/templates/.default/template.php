<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult["ITEMS"])): ?>
	<? foreach ($arResult["ITEMS"] as $cell => $arItem):
		$class = 'step-crm';
		if ($cell == 1) $class = 'step2';
		elseif ($cell == 3) $class = 'step4';
		?>
		<section class="<?= $class ?>">
			<div class="container">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="material<?= $cell ? '2' : '' ?>">
							<div class="mattop">
                                <h3 class="mh3"><?= $arItem["NAME"] ?></h3>
								<div class="h3"><?= $arItem["PROPERTY_SUBNAME_VALUE"] ?></div>
							</div>
							<div class="include">
								<div class="row">
									<?= $arItem["PREVIEW_TEXT"] ?>
								</div>
							</div>
							<div class="step-bottom step-bottom_has-icons">
								<div class="row">
                                    <?php if ($arItem["~PROPERTY_PRICE_VALUE"]["TEXT"]) { ?>
									<div class="col-md-6 col-sm-12 col-xs-12 block-c">
                                        <div class="subheader"><?= $arItem["~PROPERTY_PRICE_VALUE"]["TEXT"] ?></div>
										<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/impl/wallet.png" class="img-responsive">
									</div>
                                    <?php } ?>
                                    <?php if ($arItem["PROPERTY_TIME_VALUE"]["TEXT"]) { ?>
									<div class="col-md-6 col-sm-12 col-xs-12 block-s">
										<div class="subheader"><?= $arItem["~PROPERTY_TIME_VALUE"]["TEXT"] ?></div>
										<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/impl/cal.png" class="img-responsive">
									</div>
                                    <?php } ?>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
	<? endforeach; ?>
<? endif; ?>