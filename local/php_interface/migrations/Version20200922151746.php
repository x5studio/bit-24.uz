<?php

namespace Sprint\Migration;


class Version20200922151746 extends Version
{
    protected $description = "task_21084 добавляет селектор на якори в ИБ \"Блог\" и \"Отраслевые решения\"";

    protected $moduleVersion = "3.16.3";

    protected $blogIblockId = 2;

    protected $industriesIblockId = 3;

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $this->blogUp();
        $this->industriesUp();
    }

    public function down()
    {
        $this->blogDown();
        $this->industriesDown();
    }

    protected function blogUp()
    {
        $rsItems = \CIBlockElement::GetList(
            ['ID' => 'DESC'],
            ['IBLOCK_ID' => $this->blogIblockId],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'PROPERTY_SB_LINK_2']
        );
        while($arItem = $rsItems->Fetch()) {
            if (strpos($arItem['PROPERTY_SB_LINK_2_VALUE']['TEXT'], '<a') !== false) {
                $arItem['PROPERTY_SB_LINK_2_VALUE']['TEXT'] = preg_replace('/<a([^>]*)>/', '<a$1 class="anchor">', $arItem['PROPERTY_SB_LINK_2_VALUE']['TEXT']);

                \CIBlockElement::SetPropertyValuesEx($arItem['ID'], false, array('SB_LINK_2' => $arItem['PROPERTY_SB_LINK_2_VALUE']['TEXT']));
            }
        }
    }

    protected function blogDown()
    {
        $rsItems = \CIBlockElement::GetList(
            ['ID' => 'DESC'],
            ['IBLOCK_ID' => $this->blogIblockId],
            false,
            false,
            ['ID', 'IBLOCK_ID', 'PROPERTY_SB_LINK_2'
            ]
        );
        while($arItem = $rsItems->Fetch()) {
            if (strpos($arItem['PROPERTY_SB_LINK_2_VALUE']['TEXT'], '<a') !== false) {
                $arItem['PROPERTY_SB_LINK_2_VALUE']['TEXT'] = str_replace(' class="anchor"', '', $arItem['PROPERTY_SB_LINK_2_VALUE']['TEXT']);

                \CIBlockElement::SetPropertyValuesEx($arItem['ID'], false, array('SB_LINK_2' => $arItem['PROPERTY_SB_LINK_2_VALUE']['TEXT']));
            }
        }
    }

    protected function industriesUp()
    {
        $rsItems = \CIBlockSection::GetList(
            ['ID' => 'ASC'],
            ['IBLOCK_ID' => $this->industriesIblockId],
            false,
            ['ID', 'UF_ANCHOR_LINKS']
        );
        while($arItem = $rsItems->Fetch()) {
            if (strpos($arItem['UF_ANCHOR_LINKS'], '<a') !== false) {
                $arItem['UF_ANCHOR_LINKS'] = preg_replace('/<a([^>]*)>/', '<a$1 class="anchor">', $arItem['UF_ANCHOR_LINKS']);

                global $USER_FIELD_MANAGER;
                $USER_FIELD_MANAGER->Update('IBLOCK_'.$this->industriesIblockId.'_SECTION', $arItem['ID'], ['UF_ANCHOR_LINKS'  => $arItem['UF_ANCHOR_LINKS']]);
            }
        }
    }

    protected function industriesDown()
    {
        $rsItems = \CIBlockSection::GetList(
            ['ID' => 'ASC'],
            ['IBLOCK_ID' => $this->industriesIblockId],
            false,
            ['ID', 'UF_ANCHOR_LINKS']
        );
        while($arItem = $rsItems->Fetch()) {
            if (strpos($arItem['UF_ANCHOR_LINKS'], '<a') !== false) {
                $arItem['UF_ANCHOR_LINKS'] = str_replace(' class="anchor"', '', $arItem['UF_ANCHOR_LINKS']);

                global $USER_FIELD_MANAGER;
                $USER_FIELD_MANAGER->Update('IBLOCK_'.$this->industriesIblockId.'_SECTION', $arItem['ID'], ['UF_ANCHOR_LINKS'  => $arItem['UF_ANCHOR_LINKS']]);
            }
        }
    }
}

