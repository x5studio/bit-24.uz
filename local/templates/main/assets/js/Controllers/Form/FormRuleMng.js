if (typeof define === "function" && define.amd) {
    define([], function () {

        function FormRuleMng(p) {
            this.p = p;
            this.rules = {};
            this.messages = {};
            this.noPlaceholderRuleTypes = ['EMAIL_NO_REQUIRED', 'PHONE_NO_REQUIRED', 'REQUIRED_FROM_GROUP'];
        }

        FormRuleMng.prototype.getRule = function (key) {
            var o = this;
            return o.rules[key]
        };

        FormRuleMng.prototype.getMessage = function (key) {
            var o = this;
            return o.messages[key]
        };

        FormRuleMng.prototype.addRule = function (key, rule, message) {
            var o = this;
            o.rules[key] = rule;
            o.messages[key] = message;
        };

        var obFormRuleMng = new FormRuleMng();

        obFormRuleMng.addRule('REQUIRED', {
            required: true
        }, {
            required: 'Обязательное поле для заполнения'
        });

        obFormRuleMng.addRule('NAME', {
            required: true
        }, {
            required: 'Не указано Ф.И.О.'
        });

        obFormRuleMng.addRule('PHONE', {
            required: true
        }, {
            required: 'Не указан телефон'
        });

        obFormRuleMng.addRule('EMAIL', {
            required: true,
            email: true
        }, {
            required: 'Поле email не заполнено',
            email: 'Нужен корректный email адрес!'
        });

        obFormRuleMng.addRule('EMAIL_NO_REQUIRED', {
            emailNoRequired: true
        }, {
            format: 'Нужен корректный email адрес!'
        });

        obFormRuleMng.addRule('PHONE_NO_REQUIRED', {
            phoneNoRequired: true
        }, {
            format: 'Неверный формат номера телефона!'
        });

        obFormRuleMng.addRule('REQUIRED_FROM_GROUP', {
            requiredFromGroup: true
        }, {
            required: 'Одно из группы полей обязательно для заполнения!'
        });

        obFormRuleMng.addRule('CAPTCHA', {
            required: true
        }, {
            required: 'Не введены символы с картинки'
        });

        obFormRuleMng.addRule('placeholder', {
            placeholder: true
        }, function (mainRule) {
            return {placeholder: obFormRuleMng.messages[mainRule] ? obFormRuleMng.messages[mainRule].required : obFormRuleMng.messages['REQUIRED'].required};
        });

        return obFormRuleMng;
    });
}
