<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (defined('WEB_FORM_ORDER_CALL')) {
    $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "general.onecol", array(
        "WEB_FORM_ID" => WEB_FORM_ORDER_CALL,
        "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
        "COMPANY_IBLOCK_ID" => IB_CONTACTS,
        "PRODUCT_IBLOCK_TYPE" => IBT_CATALOG,
        "PRODUCT_IBLOCK_ID" => IB_CATALOG,
        "CITY_QUESTION_CODE" => "CITY",
        "OFFICE_QUESTION_CODE" => "OFFICE",
        "PRODUCT_QUESTION_CODE" => "PRODUCT",
        "TO_QUESTION_CODE" => "TO",
        "TO_QUESTION_RESIPIENTS" => "",
        "EMAIL_ONLY_FROM_RESIPIENTS" => "N",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "Y",
        "SEF_MODE" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "360000",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => "",
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        ),
        "SHOW_FORM_ALL_STATUSES" => "Y",
        "BTN_CALL_FORM" => ".call-ord",
    ), false
    );
}