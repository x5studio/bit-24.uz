<?php if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (method_exists($this, 'setFrameMode')) {
	$this->setFrameMode(true);
}

if ($arResult['ACTION']['status']=='error') {
	ShowError($arResult['ACTION']['message']);
} elseif ($arResult['ACTION']['status']=='ok') {
	ShowNote($arResult['ACTION']['message']);
}
?>


<!--<button type="button" data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-default submit" id="free-subscrib">
<?/*=GetMessage("ASD_SUBSCRIBEQUICK_PODPISATQSA")*/?>
</button>-->
<button type="button" onclick="yaCounter45927957.reachGoal('feedback-click'); gtag('event', 'click',{'event_category': 'form', 'event_label': 'click'}); return true;" class="btn btn-blocks top-button pull-right" data-form-field-type="Оставить заявку 1">Оставить заявку</button>
<!-- Modal -->

<div class="free-subscribe-wrap">
    <span class="title">Бесплатная подписка</span>
    <p>Никакого спама - только качественный контент</p>
    <p id="asd_subscribe_res" style="display: none;"></p>
	<form action="<?= POST_FORM_ACTION_URI?>" method="post" id="asd_subscribe_form"  class="clearfix">
		<?= bitrix_sessid_post()?>
		<input type="hidden" name="asd_subscribe" value="Y" />
		<input type="hidden" name="charset" value="<?= SITE_CHARSET?>" />
		<input type="hidden" name="site_id" value="<?= SITE_ID?>" />
		<input type="hidden" name="asd_rubrics" value="<?= $arParams['RUBRICS_STR']?>" />
		<input type="hidden" name="asd_format" value="<?= $arParams['FORMAT']?>" />
		<input type="hidden" name="asd_show_rubrics" value="<?= $arParams['SHOW_RUBRICS']?>" />
		<input type="hidden" name="asd_not_confirm" value="<?= $arParams['NOT_CONFIRM']?>" />
		<input type="hidden" name="asd_key" value="<?= md5($arParams['JS_KEY'].$arParams['RUBRICS_STR'].$arParams['SHOW_RUBRICS'].$arParams['NOT_CONFIRM'])?>" />
        <input type="email" class="form-control" name="asd_email" placeholder="Ваше e-mail" value="" />
		<input type="submit" name="asd_submit" id="asd_subscribe_submit" class="btn btn-default submit" value="<?=GetMessage("ASD_SUBSCRIBEQUICK_PODPISATQSA")?>" />
	</form>
</div>