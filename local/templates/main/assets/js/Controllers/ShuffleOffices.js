if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/ShuffleWeight', 'Controls/Phone', 'tools/tools', 'cookie'], function ($, ShuffleWeight, Phone) {
        function ShuffleOffices(container, options) {
            this.container = $(container);
            var dataOptions = this.container.data();
            this.options = $.extend(true, {}, ShuffleOffices.defaultOptions, options, dataOptions);
            this.sampleRow = $('.sample-row', this.container)
                .removeClass('sample-row')
                .removeClass('hidden')
                .removeClass('invisible')
                .addClass('office-item')
                .detach();
            this.otherRow = $('.other-row', this.container);
            this.offices = this.options.offices;
            this.fixedOffices = [];
            this.sortedOffices = [];
            this.init();
        }

        ShuffleOffices.defaultOptions = {
            limitViewItems: 2
        };

        ShuffleOffices.prototype.sort = function (offices) {
            var o = this;
            var sortedOffices = [];
            var obSorter = new ShuffleWeight();
            for (var i in offices) {
                var office = offices[i];
                if (office.fixed) {
                    sortedOffices.push(office);
                } else {
                    obSorter.add(office);
                }
            }
            obSorter.sort("power");
            $.merge(sortedOffices, obSorter.getList());
            o.container.data('officesSorted', true);
            return sortedOffices;
        };

        ShuffleOffices.prototype.sortBySession = function (offices, arIdBySort) {
            var sortedOffices = [];
            for (var i in arIdBySort) {
                for (var x in offices) {
                    if (arIdBySort[i] == offices[x]['id']) {
                        sortedOffices.push(offices[x]);
                        break;
                    }
                }
            }
            if (sortedOffices.length > 0) {
                return sortedOffices
            }
            else {
                return offices;
            }
        }

        ShuffleOffices.prototype.renderSortedOffices = function (sortedOffices) {
            var o = this;
            for (var i in sortedOffices) {
                var sortedOffice = sortedOffices[i];
                if (o.container.find('.office-item').size() < o.options.limitViewItems) {
                    o.renderOffice(sortedOffice);
                } else {
                    break;
                }
            }
        };

        ShuffleOffices.prototype.renderOffice = function (sortedOffice) {
            var o = this;
            var officeRow = o.sampleRow.clone();
            officeRow.find('.name').each(function () {
                var a = $(this).children('i');
                $(this).html(a).append('<a href="' + sortedOffice.url + '">' + sortedOffice.name + '</a>');
            });
            var phoneText = '';
            officeRow.find('.phone').each(function () {
                var a = $(this).children('i');
                $(this).html(a).append(sortedOffice.phone);
                phoneText += sortedOffice.phone;
                if($(this).is("a")) {
                	$(this).attr('href','tel:+'+sortedOffice.phone.replace(/[^0-9]/g, ''));
                }
                if ($(this).hasClass('j-phone-shuffle')) {
                    $(this).each(function () {
                        var item = $(this);
                        require(['Controls/Phone'], function (Phone) {
                            new Phone(item);
                        });
                    });
                }
            });
            if (sortedOffice.iconMetro) {
                officeRow.find('.metro').css({
                    backgroundImage: "url(" + sortedOffice.iconMetro + ")"
                });
            } else {
                officeRow.find('.metro').detach();
            }
            $(document).trigger("set_contact_box_simple_data",[phoneText]);
            if (o.otherRow.size() > 0) {
                o.otherRow.before(officeRow);
            } else {
                o.container.append(officeRow);
            }
        };

        ShuffleOffices.prototype.init = function () {
            var o = this;
            o.fixedOffices = o.offices.filter(function (obOffice) {
                if (obOffice.fixed)
                    return true;
            });
            o.sortedOffices = o.offices.filter(function (obOffice) {
                if (!obOffice.fixed)
                    return true;
            });
            if (o.offices.length > 0) {
                var cookShuffleGet = getCookie('SHUFLE_OFFICE_SESS');
                if (cookShuffleGet != '') {
                    o.sortedOffices = o.sortBySession(o.sortedOffices, cookShuffleGet.split(","));
                }
                else {
                    o.sortedOffices = o.sort(o.sortedOffices);
                    var cookShuffleStr = "";
                    for (var x = 0; x < o.options.limitViewItems; x++) {
                        cookShuffleStr += o.sortedOffices[x]['id'];
                        if (o.options.limitViewItems > 1 && x < (o.options.limitViewItems - 1)) {
                            cookShuffleStr += ",";
                        }
                    }
                    if (cookShuffleStr != "") {
                        setCookie('SHUFLE_OFFICE_SESS', cookShuffleStr, null);
                    }
                }
            }
            o.offices = [];
            o.offices = o.offices.concat(o.fixedOffices);
            o.offices = o.offices.concat(o.sortedOffices);
            o.container.data('offices', o.offices);
            o.renderSortedOffices(o.sortedOffices);
            getInitial(o.container).resolve();
            o.container.trigger(new $.Event('contactBox.init', {
                source: /function (.{1,})\(/.exec((this).constructor.toString())[1] || ''
            }));
            if (o.sortedOffices.length > 0) {
                track(['Контакты', 'Показ офиса', o.sortedOffices[0].name, {'nonInteraction': 1}]);
            }
        };

        return ShuffleOffices;
    });
}