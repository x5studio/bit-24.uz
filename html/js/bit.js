$(document).ready(function() { 
  var navbarToggle = '.navbar-toggle'; // name of navbar toggle, BS3 = '.navbar-toggle', BS4 = '.navbar-toggler'  
  $('.dropdown, .dropup').each(function() {
    var dropdown = $(this),
      dropdownToggle = $('[data-toggle="dropdown"]', dropdown),
      dropdownHoverAll = dropdownToggle.data('dropdown-hover-all') || false;
    
    // Mouseover
    dropdown.hover(function(){
      var notMobileMenu = $(navbarToggle).size() > 0 && $(navbarToggle).css('display') === 'none';
      if ((dropdownHoverAll == true || (dropdownHoverAll == false && notMobileMenu))) { 
        dropdownToggle.trigger('click');
      }
    })
  });
  });
	
$('.navbar-toggle').click(function () {
	$('.navbar-toggle i').toggleClass('fa-bars fa-times');
});
var totalItems = $('.item').length;
var currentIndex = $('div.active').index() + 1;

$('#carousel-vertical').on('slid.bs.carousel', function() {
    currentIndex = $('div.active').index() + 1;
   $('.num').html(0+''+currentIndex+'');
});


$(document).ready(function () {
	$('[data-toggle="offcanvas"]').click(function () {
		$('.row-offcanvas').toggleClass('active')
	});
	
});

$('#pCarousel').carousel({ interval: 3000 });
	
	
$('.carousel-showmanymoveone .item').each(function(){
		var itemToClone = $(this);
		
		for (var i=1;i<3;i++) {
			itemToClone = itemToClone.next();
			
			if (!itemToClone.length) {
				itemToClone = $(this).siblings(':first');
			}
			
			itemToClone.children(':first-child').clone()
			.addClass("cloneditem-"+(i))
			.appendTo($(this));
		}
	});

	
function demoDisplay() {
    document.getElementById("foot").style.display = "none";
	document.getElementById("formaction").style.display = "none";
}

function demoVisibility() {
    document.getElementById("foot").style.display = "block";
	document.getElementById("formaction").style.display = "block";
}


var delta = 0;
var scrollThreshold = 5;
// Bind event handler
$(window).on(wheelEvent, function (e) {
    // Do nothing if we weren't scrolling the carousel
    var carousel = $('#carousel-vertical:hover');
    if (carousel.length === 0)  return;

    // Get the scroll position of the current slide
    var currentSlide = $(e.target).closest('.item')
    var scrollPosition = currentSlide.scrollTop();

    // --- Scrolling up ---
    if (e.originalEvent.detail < 0 || e.originalEvent.deltaY < 0 || e.originalEvent.wheelDelta > 0) {
        // Do nothing if the current slide is not at the scroll top
        if(scrollPosition !== 0) return;

        delta--;

        if ( Math.abs(delta) >= scrollThreshold) {
            delta = 0;
            carousel.carousel('prev');
        }
    }

    // --- Scrolling down ---
    else {
        // Do nothing if the current slide is not at the scroll bottom
        var contentHeight = currentSlide.find('> .content').outerHeight();
        if(contentHeight > currentSlide.outerHeight() && scrollPosition + currentSlide.outerHeight() !== contentHeight) return;

        delta++;
        if (delta >= scrollThreshold)
        {
            delta = 0;
            carousel.carousel('next');
        }
    }

    // Prevent page from scrolling
    return false;
});





if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    var itemsToWrap=$('.testItem');
    $('.parentTest').remove();
     var carousel='<div id="myCarousel" class="carousel slide hidden-md hidden-sm col-xs-12" data-ride="carousel"><div class="carousel-inner" role="listbox"></div>  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>    <span class="sr-only">Previous</span>  </a>  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>    <span class="sr-only">Next</span>  </a></div>';
    $(carousel).appendTo('.baseParent');
    var elementTowrap="";
    $.each($(itemsToWrap),function(index){
        var html=$(this).html();
        console.log(html);
        if(index==0)
            elementTowrap+='<div class="item active">'+$(this).html()+'</div>';
        else
            elementTowrap+='<div class="item">'+$(this).html()+'</div>';
    });
    $('.carousel-inner').append($(elementTowrap));
    $("#myCarousel").carousel("pause").removeData();
    $("#myCarousel").carousel();
}


//testing purpose
if($(window).width()<768)
{
    var itemsToWrap=$('.testItem');
    $('.parentTest').remove();
     var carousel='<div id="myCarousel" class="carousel slide hidden-md hidden-sm col-xs-12" data-ride="carousel"><div class="carousel-inner" role="listbox"></div>  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>    <span class="sr-only">Previous</span>  </a>  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>    <span class="sr-only">Next</span>  </a></div>';
    $(carousel).appendTo('.baseParent');
    var elementTowrap="";
    $.each($(itemsToWrap),function(index){
        var html=$(this).html();
        console.log(html);
        if(index==0)
            elementTowrap+='<div class="item active">'+$(this).html()+'</div>';
        else
            elementTowrap+='<div class="item">'+$(this).html()+'</div>';
    });
    $('.carousel-inner').append($(elementTowrap));
    $("#myCarousel").carousel("pause").removeData();
    $("#myCarousel").carousel();
    
}

$('#text-carousel').on('slide.bs.carousel',function(){
$('.carousel-inner').fadeOut(300);
})
$('#text-carousel').on('slid.bs.carousel',function(){
 $('.carousel-inner').fadeIn(600);
})



$('#pCarousel').carousel({ interval: 3000 });
	
	
$('.carousel-showmanymoveone .item').each(function(){
		var itemToClone = $(this);
		
		for (var i=1;i<3;i++) {
			itemToClone = itemToClone.next();
			
			if (!itemToClone.length) {
				itemToClone = $(this).siblings(':first');
			}
			
			itemToClone.children(':first-child').clone()
			.addClass("cloneditem-"+(i))
			.appendTo($(this));
		}
	});
