if (typeof define === "function" && define.amd) {
    define([], function () {

        function FormPopupOpen() {
            const getParamBtn = 'unique-btn';
            var buttonUniqueId = this.parseGetParams()[getParamBtn];
            if(buttonUniqueId != undefined) {
                var btnSelector = $(document).find("[data-"+getParamBtn+"='"+buttonUniqueId+"']");
                if(this.checkBtnSelector(btnSelector) == true) {
                    this.clickBtnSelector(btnSelector);
                }
            }

        }

        FormPopupOpen.prototype.checkBtnSelector = function (btnSelector) {
            if(btnSelector.length == 1) {
                return true; //имитируем клил, если нашли только одну кнопку с таким data параметром.
            } else {
                return false;
            }
        }

        FormPopupOpen.prototype.clickBtnSelector = function (btnSelector) {
            btnSelector.click();
        }

        FormPopupOpen.prototype.parseGetParams = function () {
            return window.location.search
                .replace('?','')
                .split('&')
                .reduce(
                    function(p,e){
                        var a = e.split('=');
                        p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
                        return p;
                    },
                    {}
                );
        }

        return FormPopupOpen;
    });
}