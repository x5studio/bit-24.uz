<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("form_title", "Заказать внедрение Битрикс24");
$APPLICATION->SetPageProperty("keywords_inner", "1C-Битрикс24 Интернет-магазин + CRM");

$APPLICATION->SetPageProperty("description", "Коробочный 1С-Битрикс24 Интернет магазин+CRM отличный инструмент для объединения всех каналов продаж в одной платформе. Заказывайте внедрение Битрикс24 Интернет магазин + CRM в компании Первый Бит!");
$APPLICATION->SetPageProperty("keywords", "1C-Битрикс24 Интернет-магазин + CRM");
$APPLICATION->SetPageProperty("title", "1C-Битрикс24 Интернет-магазин + CRM");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "N");
$APPLICATION->SetTitle("1C-Битрикс24 Интернет-магазин + CRM");
$APPLICATION->SetAdditionalCSS("/1c-bitrix24-store-crm/css/style.css");

$APPLICATION->AddHeadString('
<!-- Marquiz script start --> <script src="//script.marquiz.ru/v1.js" type="application/javascript"></script>  <!-- Marquiz script end -->
');
?>


<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "main",
    array(
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "main"
    ),
    false
); ?>

<section class="portal-banner">
    <div class="container clearfix">
        <div class="intro_descr">
            <h1 class="intro_title">
                1C-Битрикс24:<br>
                <span class="medium_font">Интернет-магазин + CRM</span>
            </h1>
            <p>Объедините все каналы продаж в одной eCommerce-платформе</p>
            <a href="#" class="btn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a>
        </div>
        <div class="banner-right-img">
            <img src="/upload/1c-bitrix24-store-crm/banner-pc.png" alt="">
        </div>
    </div>
</section>

<section class="portal-intro">
    <div class="container clearfix">
        <div class="portal-intro__block">
            <div class="portal-intro__caption"><h1>Платформа, объединяющая все преимущества CRM «Битрикс24» и «1С-Битрикс: Управление сайтом» для онлайн-продаж</h1></div>
            <div class="portal-intro__text portal-intro__text-border">Интернет-магазин –  не единственный канал продаж в сети. Около 70% всех заявок обеспечивают соцсети, онлайн-чаты сайта,  e-mail-рассылки, мессенджеры и звонки по телефону. Внедрив «1С-Битрикс24: Интернет-магазин + CRM» можно обрабатывать заявки со всех каналов в единой платформе.</div>
            <div class="portal-intro__text">Сэкономьте на интеграции с другими каналам. Новая платформа включает в себя:</div>
            <div class="portal-intro__list">
                <div class="portal-intro__list-block">
                    <div class="portal-intro__list-item">–   Сквозную аналитику</div>
                    <div class="portal-intro__list-item">–   Контакт-центр</div>
                    <div class="portal-intro__list-item">–   Интернет-магазин</div>
                </div>
                <div class="portal-intro__list-block">
                    <div class="portal-intro__list-item">–   CRM-систему</div>
                    <div class="portal-intro__list-item">–   CRM-маркетинг</div>
                    <div class="portal-intro__list-item">–   Центр продаж</div>
                </div>
            </div>
        </div>
        <div class="portal-intro__circle">
            <div class="portal-intro__wrapper">
                <div class="portal-intro__citem portal-intro__citem1" c="Интернет-магазин" t=""></div>
                <div class="portal-intro__citem portal-intro__citem2" c="Мобильное приложение" t=""></div>
                <div class="portal-intro__citem portal-intro__citem3" c="Онлайн-чаты" t=""></div>
                <div class="portal-intro__citem portal-intro__citem4" c="Мессенджеры (What’s App, Telegram, Viber)" t=""></div>
                <div class="portal-intro__citem portal-intro__citem5" c="Email-письма" t=""></div>
                <div class="portal-intro__citem portal-intro__citem6" c=" Социальные сети (ВКонтакте, Facebook, Instagram)" t=""></div>
                <div class="portal-intro__wrapper-body">
                   <div class="portal-intro__wrapper-caption big">100%</div>
                   <div class="portal-intro__wrapper-text">инструментов для продаж в одном продукте</div>
                </div>
            </div>
        </div>
        <script>
            jQuery(document).ready(function(){
                jQuery('.portal-intro__citem').mouseover(function() {
                    var c=jQuery(this).attr('c');
                    var t=jQuery(this).attr('t');
                    jQuery('.portal-intro__wrapper-body').fadeOut( "fast", function() {
                        jQuery('.portal-intro__wrapper-caption').html(c);
                        jQuery('.portal-intro__wrapper-text').html(t);
                        jQuery('.portal-intro__wrapper-caption').removeClass('big');
                        jQuery('.portal-intro__wrapper-body').fadeIn('fast');
                    });
                }).mouseout(function() {
                    jQuery('.portal-intro__wrapper-body').fadeOut( "fast", function() {
                        jQuery('.portal-intro__wrapper-caption').html('100%');
                        jQuery('.portal-intro__wrapper-text').html('инструментов для продаж в одном продукте');
                        jQuery('.portal-intro__wrapper-caption').addClass('big');
                        jQuery('.portal-intro__wrapper-body').fadeIn('fast');
                    });
                });
            });
        </script>
</div>
 </section> <section class="portal-than">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="portal-than__h1">
                <span>
                    Чем «1С-Битрикс: Интернет-магазин +CRM» отличается от других продуктов?
                </span>
                <div class="ordering">
 <a class="btn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку" href="#">заказать</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="portal-than__items row">
		<div class="col-md-4">
			<div class="portal-than__item portal-than__item1">
				<div class="portal-than__caption">
                    Обработка заявок непосредственно в системе
				</div>
				<div class="portal-than__text">
                    Принимайте и обрабатывайте заказы со всех каналов продаж в едином окне внутри CRM-системы.
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="portal-than__item portal-than__item2">
				<div class="portal-than__caption">
					Клиенты из мессенджеров и соц.сетей
				</div>
				<div class="portal-than__text">
					Продавайте в Instagram, ВКонтакте, Facebook, Avito, Telegram, Viber прямо из «1С-Битрикс24». Заказы из чатов попадают в логистику интернет-магазина и 1С
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="portal-than__item portal-than__item3">
				<div class="portal-than__caption">
                    Интеграция с системами 1С
				</div>
				<div class="portal-than__text">
                    Все заказы, контактная информация контрагентов и покупателей синхронизируются с 1С, CRM и сайтом в реальном времени.
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="portal-than__item portal-than__item4">
				<div class="portal-than__caption">
                    Сквозная аналитика
				</div>
				<div class="portal-than__text">
                    Встроенные инструменты для отслеживания эффективности рекламы. Помогут понять, какой рекламный канал приносит прибыль, а какой – убыточен
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="portal-than__item portal-than__item5">
				<div class="portal-than__caption">
                    Роботы для автоматизации продаж
				</div>
				<div class="portal-than__text">
                    С роботами можно сэкономить время и избавится от постоянных задач - формирования и отправки счетов, рассылки уведомлений, оформления доставок. В случае необходимости, на ответственного менеджера автоматически сформируется задача.
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="portal-than__item portal-than__item6">
				<div class="portal-than__caption">
                    Все возможности CMS «1С-Битрикс: Управление сайтом»
				</div>
				<div class="portal-than__text">
                    Наиболее востребованная CMS-система в сфере онлайн продаж. Можно формировать структуру магазина, каталога, ассортимента товаров в виде карточек. Поддерживает работу онлайн-чатов и касс.
				</div>
			</div>
		</div>
	</div>
	 <script>
            jQuery(document).ready(function(){
                jQuery('.portal-than__caption').click(function(){
                    var w=jQuery( document ).width();
                    if (w<=768) {
                        jQuery(this).toggleClass('active');
                        jQuery(this).parent().find('.portal-than__text').toggle('fast');
                    }
                });
            });
        </script>
	<div class="row">
		<div class="col-md-6">
			<div class="heading-wrapper">
				<h2 class="why-heading white-heading">
				НЕ ЗНАЕТЕ КАКАЯ <br>
				 ЛИЦЕНЗИЯ БИТРИКС 24 <br>
 <span class="blue-heading">НУЖНА ВАМ?</span> </h2>
				<p class="white-heading">
					 Заполните форму, мы изучим потребности вашего <br>
					 бизнеса и порекомендуем наиболее оптимальную <br>
					 и выгодную для вас лицензию
				</p>
			</div>
		</div>
		<div class="col-md-6">
            <? $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "b24", Array(
                        "WEB_FORM_ID" => 16,  // ID веб-формы
                        "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
                        "COMPANY_IBLOCK_ID" => IB_CONTACTS,
                        "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
                        "EMAIL_QUESTION_CODE" => "EMAIL",
                        "CITY_QUESTION_CODE" => "CITY",
                        "OFFICE_QUESTION_CODE" => "OFFICE",
                        "TO_QUESTION_RESIPIENTS" => "",
                        "EMAIL_ONLY_FROM_RESIPIENTS" => "",
                        "PRODUCT_QUESTION_CODE" => "PRODUCT",
                        "TO_QUESTION_CODE" => "TO",
                        "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
                        "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
                        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
                        "CACHE_TYPE" => "A",  // Тип кеширования
                        "CACHE_TIME" => "3600",  // Время кеширования (сек.)
                        "LIST_URL" => "",  // Страница со списком результатов
                        "EDIT_URL" => "",  // Страница редактирования результата
                        "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
                        "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
                        "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
                        "VARIABLE_ALIASES" => array(
                            "WEB_FORM_ID" => "WEB_FORM_ID",
                            "RESULT_ID" => "RESULT_ID",
                        ),
                        "ALERT_ADD_SHARE" => "N",
                        "HIDE_PRIVACY_POLICE" => "Y",
                        "BTN_CALL_FORM" => ".btn_order",
                        "STRONG_SHOW_CITY" => $STRONG_SHOW_CITY,
                        "STRONG_SHOW_OFFICE" => $STRONG_SHOW_OFFICE,
                        "HIDE_FIELDS" => array(
                            0 => "CITY",
                            1 => "OFFICE",
                        ),
                        "COMPONENT_MARKER" => "request",
                        "SHOW_FORM_DESCRIPTION" => "N",
                        "MESS" => array(
                            "THANK_YOU" => "Спасибо, Ваша заявка принята.",
                            "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
                            "TITLE_FORM" => "Оставить заявку",
                        )
                    ),
                        false,
                        array(
                            "HIDE_ICONS" => "Y"
                        )
                    ); ?>
        </div>
    </div>
</section>

<section class="portal-who">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><h2 class="portal-who-heading">Кому подходит?</h2></div>
        </div>
        <div class="portal-who-items clear-fix">
            <div class="portal-who-item">
                <div class="portal-who-item__counter">01.</div>
                <div class="portal-who-item__text">Есть интернет-магазин и нет Битрикс24</div>
            </div>
            <div class="portal-who-item">
                <div class="portal-who-item__counter">02.</div>
                <div class="portal-who-item__text">Есть интернет-магазин с коробочным или облачным Битрикс24</div>
            </div>
            <div class="portal-who-item">
                <div class="portal-who-item__counter">03.</div>
                <div class="portal-who-item__text">Есть интернет-магазин с другой подключенной CRM</div>
            </div>
            <div class="portal-who-item">
                <div class="portal-who-item__counter">04.</div>
                <div class="portal-who-item__text">Только планируете запуск онлайн-продаж и CRM</div>
            </div>
        </div>
    </div>
</section>

<section class="portal-banner2">
    <div class="container clearfix">
        <div class="intro_descr">
            <h1 class="intro_title">Стоимость новой лицензии<span>«1С-Битрикс24: <br />Интернет-магазин + CRM»</span></h1>
            <div class="portal-banner2__price">
                <div class="portal-banner2__price-body">
                    <div class="portal-banner2__price-value">79 200 ₽ <s>99 000 ₽</s></div>
                    <div class="portal-banner2__price-desc portal-banner2__up">Коробочная версия</div>
                </div>
                <div class="portal-banner2__price-butt">
                    <a href="#" class="btn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a>
                </div>
            </div>
            <div class="portal-banner2__cost">
                <div class="portal-banner2__cost-caption"><strong>Стоимость перехода с «1С-Битрикс»</strong></div>
                <div class="portal-banner2__cost-item">—  Малый бизнес - <strong>47 325 ₽</strong> <s>63 100 ₽</s></div>
                <div class="portal-banner2__cost-item">—  Бизнес - <strong>20 880 ₽</strong> <s>26 100 ₽</s></div>
            </div>
            <div class="portal-banner2__subt">
                <div class="portal-banner2__price-desc">Переход возможен с любых редакций «1С-Битрикс: Управление сайтом»</div>
            </div>
        </div>
        <div class="banner2-right-img">
            <img src="/upload/1c-bitrix24-store-crm/banner-pc.png" alt="">
        </div>
    </div>
</section>

<section class="portal-results">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><h2 class="why-heading white-heading">ПОЧЕМУ «1С-БИТРИКС: ИНТЕРНЕТ-МАГАЗИН + CRM» ?</h2></div>
        </div>
        <div class="clearfix">
            <div class="results-list">
                <div class="results-list__item"><p>В платформе есть все, чтобы наладить онлайн-торговлю</p></div>
                <div class="results-list__item"><p>Соответствие законодательству и ФЗ-54, ФЗ-152</p></div>
                <div class="results-list__item"><p>Экономия на интеграциях</p></div>
            </div>
            <div class="results-list">

                <div class="results-list__item"><p>Готовность обмена данных с 1С, Яндекс.Маркетом и платежными системами</p></div>
                <div class="results-list__item"><p>Позволяет работать с клиентами внутри одной системы</p></div>
                <div class="results-list__item"><p>Устойчивость к нагрузке и атакам</p></div>
            </div>
        </div>
    </div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>