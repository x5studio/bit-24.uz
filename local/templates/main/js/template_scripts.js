(function ($) {
    $(function () {
        $('body').on('click', function(e) {
            if($(e.target).closest('a.city').length == 0 && $(e.target).closest('#geo_confirm').length == 0) {
                $('#geo_confirm').hide();
            }
        });
    });
})(jQuery);