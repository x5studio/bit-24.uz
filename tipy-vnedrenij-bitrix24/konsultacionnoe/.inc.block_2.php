<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="col-md-4 col-sm-12 col-xs-12">
	<div class="block1">
		<h3>Повышение эффективности</h3>
 <img src="/local/templates/main/assets/img/bt/002.png" class="img-responsive">
		<p>
			 Для тех компаний, которые хотят&nbsp;повысить эффективность своего бизнеса за счет аудита и корректировок внутренних бизнес-процессов с использованием опыта и лучших практик в своей отрасли.
		</p>
	</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
	<div class="block1">
		<h3>Автоматизация работы</h3>
 <img src="/local/templates/main/assets/img/bt/003.png" class="img-responsive">
		<p>
			 Для тех, кому нужно автоматизировать один из участков деятельности компании: например, взаимоотношения с клиентами (CRM), внутрикорпоративное взаимодействие, управление задачами и проектами, документооборот.
		</p>
	</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
	<div class="block1">
		<h3>Персональный подход</h3>
 <img src="/local/templates/main/assets/img/bt/004.png" class="img-responsive">
		<p>
			 Для тех, у кого стоит задача&nbsp;разработки&nbsp;и последующего внедрения своего уникального бизнес-процесса или процедуры. Это актуально в случае отсутствия&nbsp;готового решения на рынке.
		</p>
	</div>
</div>
 <br>