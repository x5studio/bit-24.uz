if (typeof define === "function" && define.amd) {
    define(['Controls/WaitForLoadImg', 'bxslider'], function (WaitForLoadImg) {
        function BXSlider(container) {
            var o = this;
            this.container = $(container);
            this.slides = $('.slides', this.container);
            this.items = $('.slide', this.slides);
            this.btnNext = $('.flex-next', this.container);
            this.btnPrev = $('.flex-prev', this.container);

            this.moveSlides = this.container.data('move-slides');

            if(this.moveSlides === 'false')
                this.moveSlides = false;
            else if(!this.moveSlides)
                this.moveSlides = 3;

            this.minSlides = this.container.data('min-slides');

            if(this.minSlides === 'false')
                this.minSlides = false;
            else if(!this.moveSlides)
                this.minSlides = 3;


            this.init = function () {
                var width = 0;
                o.items.each(function () {
                    var itemWidth = $(this).outerWidth();
                    width += itemWidth;
                });
                var adsWidthItems = width / o.items.length;

                o.slides.bxSlider({
                    slideWidth: adsWidthItems,
                    moveSlides: this.moveSlides,
                    minSlides: this.minSlides,
                    maxSlides: 7,
                    slideMargin: 30,
                    infiniteLoop: true,
                    useCSS: false,
                    pager: false,
                    nextSelector: o.btnNext,
                    prevSelector: o.btnPrev,
                    touchEnabled : (navigator.maxTouchPoints > 0)
                });
            };

            var
                imgs = this.container.find('img');

            WaitForLoadImg(imgs, function()
            {
                o.init();
            });
        }

        return BXSlider;
    });
}
