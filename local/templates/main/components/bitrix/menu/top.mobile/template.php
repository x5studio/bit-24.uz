<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):
    ?>
<div class="topmenu-mobile__menu">
    <div class="cd-dropdown-wrapper">
        <a class="cd-dropdown-trigger dropdown-is-active" href="#0">Dropdown</a>
        <nav class="cd-dropdown dropdown-is-active">
            <ul class="cd-dropdown-content">
<?
$previousLevel = 0;
foreach($arResult as $arItem):
    $icon = !empty($arItem['PARAMS']['ICON']) ? $arItem['PARAMS']['ICON'] : false;
    $iconClass = !empty($arItem['PARAMS']['ICON_CLASS']) ? $arItem['PARAMS']['ICON_CLASS'] : false;
    ?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

        <li class="has-children">
            <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
            <ul class="cd-secondary-dropdown fade-out is-hidden">
	<?else:?>

            <li>
                <a href="<?=$arItem["LINK"]?>">
                    <? if ($icon || $iconClass) { ?>
                        <span class="menu-ico <? if ($iconClass) echo $iconClass; ?>" <? if ($icon) { ?>style="background: url(<?= $icon ?>) no-repeat left center"<? } ?>>
                    <? } ?>
                    <?=$arItem["TEXT"]?>
                    <? if ($icon || $iconClass) { ?>
                        </span>
                    <? } ?>
                </a>
            </li>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
                <li>
                    <div class="topmenu-mobile__cont">
                        <div class="topmenu-mobile__cont-phone"><?=$GLOBALS['OFFICE_PHONE'];?></div>
                        <div class="topmenu-mobile__cont-but">
                            <button type="button" onclick="yaCounter45927957.reachGoal('feedback-click'); gtag('event', 'click', {'event_category': 'form', 'event_label': 'click'}); return true;" class="btn btn-blocks top-button" data-form-field-type="Оставить заявку 1">Оставить заявку</button>
                        </div>
                    </div>
                </li>
            </ul>
        </nav>
    </div>
</div>
<?endif?>