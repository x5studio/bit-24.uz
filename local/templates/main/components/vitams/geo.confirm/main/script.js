$(function () {
    require(['Controllers/Loc', 'main'], function (Loc) {

        l = Loc.getWithPrefix('PB_GEO_CT_GC_DEFAULT_JS_');

        if (typeof frameCacheVars == 'undefined') {
            setTimeout(function () {
                new CustomGeo();
            }, 500);
        }

        BX.addCustomEvent("onFrameDataReceived", function (arEventParams) {
            setTimeout(function () {
                new CustomGeo();
            }, 300);
        });

    });

    function CustomGeo() {
        var _this = this;
        _this.init();
    }

    CustomGeo.prototype.init = function () {
        var _this = this;
        _this.geoConfirmWrapp = $('#geo-confirm-wrapp');
        _this.geoConfirm = $('#geo_confirm');
        _this.header = $('.header');
        _this.dfMoveConfirm = $.Deferred();

        require(['cookie'], function () {
            var cookiePrefix = 'BITRIX_SM_';
            if (getCookie(cookiePrefix + 'CONFIRM') == false) {
                _this.ShowPopUpConfirm();
            }
        });
    }

    CustomGeo.prototype.ShowPopUpConfirm = function (newCityData) {
        newCityData = newCityData || false;
        var _this = this;
        require(['main'], function () {
            require(['Controls/Responsive'], function (obResponsive) {
                obResponsive.on(['xsmall', 'small'], function () {
                    if (_this.geoConfirm.parent()[0] == _this.geoConfirmWrapp[0]) {
                        _this.geoConfirm.insertAfter(_this.header);
                    }
                    _this.dfMoveConfirm.resolve();
                });
                obResponsive.on(['medium', 'large'], function () {
                    if (_this.geoConfirm.parent()[0] != _this.geoConfirmWrapp[0]) {
                        _this.geoConfirm.prependTo(_this.geoConfirmWrapp);
                    }
                    _this.dfMoveConfirm.resolve();
                });
                _this.dfMoveConfirm.done(function () {
                    geoObject = new GeoConfirm({
                        root: '#geo_confirm',
                        confirm: '.confirm-btn',
                        close: '.close',
                        select: '#header-city-list-confirm',
                        new_city_data: newCityData
                    });
                })
            });
        });
    }


    function GeoConfirm(p) {

        var self = this;
        self.root = $(p.root);
        self.confirm = self.root.find(p.confirm);
        self.close = self.root.find(p.close);
        self.select = $(p.select);
        self.isSmartConfirm = self.root.data('smartConfirm');
        self.linkBaseurl = self.root.data('linkBaseurl');
        self.sefMode = self.root.data('sefMode');
        self.new_city_data = p.new_city_data;

        self.root.slideDown('fast');

        self.confirm.click(function (e) {
            e.preventDefault();
            cityConfirm = 'CURRENT';
            if (self.new_city_data != false) {
                cityConfirm = self.new_city_data.id;
            }
            self.send({
                'TYPE': 'CONFIRM',
                'CITY': cityConfirm
            }, function (data_ajax) {
                if (self.linkBaseurl == 'Y' && self.sefMode == 'N') {
                    if (cityConfirm == 'CURRENT') {
                        cityConfirm = JSON.parse(data_ajax.responseText).current_confirm;
                    }
                    self.setSmartChangeCityCook(cityConfirm, data_ajax);
                }
            });
            self.root.hide('fast');
        });

        self.close.click(function (e) {
            e.preventDefault();
            self.send({
                'TYPE': 'CONFIRM',
                'CITY': 'STAY_HERE'
            }, function (data_ajax) {
                self.setSmartChangeCityCook(JSON.parse(data_ajax.responseText).current_confirm, data_ajax);
            });
            self.root.hide('fast');
        });

        self.setSmartChangeCityCook = function (cityConfirm, data_ajax) {
            if (self.linkBaseurl == 'Y' && self.sefMode == 'N') {
                var valueSmartChangeCity = 0;
                if (self.root.find('a[data-item-id="' + cityConfirm + '"]').length > 0 && parseInt(self.root.find('a[data-item-id="' + cityConfirm + '"]').data('smart-city')) > 0) {
                    valueSmartChangeCity = cityConfirm;
                }
                else {
                    if (self.root.find('a[data-smart-city="' + cityConfirm + '"]').length > 0) {
                        self.root.find('a[data-smart-city="' + cityConfirm + '"]').each(function () {
                            if ($(this).attr("data-item-id") == cityConfirm) {
                                valueSmartChangeCity = cityConfirm;
                            }
                        });
                    } else {
                        valueSmartChangeCity = cityConfirm;
                    }
                }
                var cookiePrefix = 'BITRIX_SM_';
                var firstDot = window.location.hostname.indexOf('.');
                var domainForCook = window.location.hostname.substring(firstDot == -1 ? 0 : firstDot + 1);

                if(self.isSmartConfirm) domainForCook = '';

                deleteCookie(cookiePrefix + 'SMART_CHANGE_CITY', '/');
                setCookie(cookiePrefix + 'SMART_CHANGE_CITY', valueSmartChangeCity, null, '/', domainForCook);
                window.location.reload();
            }
        }

        self.select.on('click', 'a', function (e) {

            e.preventDefault();
            var a = $(this);

            self.send({
                'TYPE': 'CONFIRM',
                'CITY': a.attr('item-id'),
                'SMART_REDIRECT': self.select.data("smart-redirect")
            }, function (xhr) {
                self.select.hide('fast', function () {
                    self.root.hide('fast');
                    if (self.isSmartConfirm) {

                        self.setSmartChangeCityCook(a.attr('item-id'));
                        window.location.reload();

                    }
                    else {
                        window.location.href = a.attr('href');
                    }
                });
            });
            track([l('TRACK_1'), l('KEYUP_1'), l('CHOISE_1'), 1]);
        });

        self.send = function (data, callback) {
            $.ajax({
                type: 'POST',
                url: window.location.href,
                data: data,
                success: function (content) {
                    try {
                        if (content.status == 'ok') {
                            if (!self.isSmartConfirm) {
                                window.location.href = content.path + window.location.pathname + window.location.search;
                            }
                        }
                    } catch (e) {
                        //console.log(content);
                        //console.log(e);
                    }
                },
                beforeSend: function () {
                },
                complete: function (xhr) {
                    callback(xhr);
                }
            });
        }

        self.changeCityName = function (newName) {
            if (self.root.find("b.name").length > 0) {
                self.root.find("b.name").text(newName);
            }
        }

        if (self.new_city_data != false) {
            this.changeCityName(self.new_city_data.name);
        }
    }

    $(document).on("onMainInit", function (event) {

        var context = event.contextContainer;

        $('.geo-confirm__redirect-link', context).click(function (e) {
            e.preventDefault();

            require(['Controls/Url'], function (Url) {
                var
                    url = new Url(document.location),
                    host = url.host.split('.').slice(-2).reduce(function (summ, item) {
                        return summ + '.' + item;
                    });

                host = '.' + host;

                setCookie('BITRIX_SM_CONFIRM', 'Y', false, '/', host);

                location = e.currentTarget.href;
            });

            return false;
        })
    });
});