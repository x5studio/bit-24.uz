<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
foreach ($arResult["ITEMS"] as $cell => $arSection):
	$textCard = $arSection["UF_SHORT_DESCRIPTION"] ? $arSection["UF_SHORT_DESCRIPTION"] : $arSection["DESCRIPTION"];
	?>
	<div class="wr_information infor_<?= ($cell % 2) ? 'right' : 'left'; ?>">
		<div class="info_title">
			<h2><?= $arSection["NAME"] ?></h2>
			<div class="div_line"></div>
			<p><?= $textCard ?></p>
			<div class="div_flex">
				<a href="<?= $arSection["SECTION_PAGE_URL"] ?>">Подробнее</a>
				<img src="<?= $arResult["IMAGES"][$arSection["DETAIL_PICTURE"]] ?>" alt="<?= $arSection["NAME"] ?>"/>
			</div>
		</div>
	</div>
	<?
endforeach;