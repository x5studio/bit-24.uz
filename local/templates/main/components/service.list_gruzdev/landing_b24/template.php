<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>


<section class="our-service">
    <div class="container">
        <h2 class="pfd-h2"><?$APPLICATION->IncludeFile($arResult['INCLUDE_DIR_LOCAL_PATH'].$arResult['H2_INCLUDE_FILE_NAME'])?></h2>

        <ul class="services-cards-list">
            <?foreach ($arResult["ITEMS"] as $arItem):?>
                <li>
                    <picture>
                        <source type="image/webp" srcset="img/bg/card-bg.webp 1x, img/bg/card-bg@2x.webp 2x">
                        <img src="img/bg/card-bg.jpg" srcset="img/bg/card-bg.jpg 1x, img/bg/card-bg@2x.jpg 2x" alt="bg" width="368" height="320" loading="lazy">
                    </picture>

                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                        <div class="pfd-h3-card"><?=$arItem["NAME"]?></div>
                        <div class="services-cards-list__price">
                            <?=$arItem["PROPERTIES"]["PRICE"]?>
                        </div>

                        <p><?=$arItem["PROPERTIES"]["LANDING_PREVIEW_TEXT"]?></p>
                    </a>
                </li>
            <?endforeach;?>
        </ul>
    </div>
</section>

