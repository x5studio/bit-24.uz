<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$show = $request->getQuery('show');
$curLink = $APPLICATION->GetCurPage(false);
$linkMas = explode('/', $curLink);
foreach ($linkMas as $key => $link) {
    if ($link === '') {
        unset($linkMas[$key]);
    }
}
array_pop($linkMas);
$newLink='/'.implode($linkMas, '/').'/';
?>

<section class="similar-offer">
    <div class="similar-offer__inner container">
        <h2>Похожие предложения</h2>
        <div class="similar-offer__slider-container">
            <div class="similar-offer__swiper swiper-container">
                <div class="similar-offer__pagination-wrapper">
                </div>
                <div class="similar-offer__swiper-btn similar-offer__swiper-btn--prev">
                    <svg width="25" height="25">
                        <use xlink:href="<?= $this->GetFolder() ?>/img/sprite_auto.svg#swiper-prev-btn"></use>
                    </svg>
                </div>
                <div class="similar-offer__swiper-btn similar-offer__swiper-btn--next">
                    <svg width="25" height="25">
                        <use xlink:href="<?= $this->GetFolder() ?>/img/sprite_auto.svg#swiper-next-btn"></use>
                    </svg>
                </div>
                <div class="similar-offer__swiper-wrapper swiper-wrapper">

                    <? foreach ($arResult["ITEMS"] as $arItem): ?>
                        <div class="swiper-slide">

                            <!--                        var_dump($kik);?>-->
                            <div class="offer-card">
                                <? if ($arItem['PROPERTIES']['IN_STOCK']['VALUE'] == 1) { ?>  <p
                                        class="offer-card__available">В наличии</p><? } ?>
                                <a href="<?= $newLink.$arItem['CODE'].'/' ?>" class="offer-card__img">
                                    <? if (!empty($arItem['PREVIEW_PICTURE']['SRC'])) { ?>
                                        <img src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>"
                                             alt="<?= $arParams['PRODUCT_VERSION'] ?>">
                                    <? } elseif (!empty($arItem['DETAIL_PICTURE']['SRC'])) { ?>
                                        <img src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>"
                                             alt="<?= $arParams['PRODUCT_VERSION'] ?>">
                                    <? } else { ?>
                                        <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/bitrix-box.png"
                                             alt="<?= $arParams['PRODUCT_VERSION'] ?>">
                                    <? } ?>
                                </a>
                                <div class="offer-card__wrapper-text">
                                    <a href="<?= $newLink.$arItem['CODE'].'/' ?>" class="offer-card__title">
                                        <span class="visually-hidden">Лицензия </span><?= $arItem['NAME'] ?><span
                                                class="visually-hidden"> Лицензия
                Битрикс24. <?= $arItem['NAME'] ?></span>
                                    </a>
                                    <p class="offer-card__description"><?= $arItem['PREVIEW_TEXT'] ?></p>
                                    <p class="offer-card__user-count">
                                        Пользователи: <?= $arItem['PROPERTIES']['USER']['VALUE'] ?></p>

                                    <!--                                <ul class="offer-card__options">-->
                                    <!--                                    <li class="offer-card__option-item">Экстранет</li>-->
                                    <!--                                    <li class="offer-card__option-item">eCommerce-платформа</li>-->
                                    <!--                                </ul>-->
                                    <div class="offer-card__wrapper-price-btn">
                                        <div class="offer-card__wrapper-price">

                                            <? if (!empty($arItem['PROPERTIES']['KOROBKA_PRICE_NEW']['VALUE'])) { ?>
                                                <p class="offer-card__old-price">
                                                    <span><?= $arItem['PROPERTIES']['KOROBKA_PRICE']['VALUE'] ?></span>
                                                </p>
                                                <p class="offer-card__price"><?= $arItem['PROPERTIES']['KOROBKA_PRICE_NEW']['VALUE'] ?>
                                                    / 12 мес.</p>
                                            <? } else if (!empty($arItem['PROPERTIES']['KOROBKA_PRICE']['VALUE'])) { ?>
                                                <p class="offer-card__price"><?= $arItem['PROPERTIES']['KOROBKA_PRICE']['VALUE'] ?>
                                                    / 12 мес.</p>
                                                <?

                                            } else { ?>
                                                <? if (!empty($arItem['PROPERTIES']['PERIOD_NEW_PRICE_2YEAR']['VALUE'])) { ?>
                                                    <p class="offer-card__old-price">
                                                        <span><?= $arItem['PROPERTIES']['PERIOD_PRICE_2YEAR']['VALUE'] ?></span>
                                                    </p>
                                                    <p class="offer-card__price"><?= $arItem['PROPERTIES']['PERIOD_NEW_PRICE_2YEAR']['VALUE'] ?>
                                                        / 1 мес.</p>
                                                <? } else { ?>
                                                    <p class="offer-card__price"><?= $arItem['PROPERTIES']['PERIOD_PRICE_1YEAR']['VALUE'] ?>
                                                        / 1 мес.</p>
                                                <? }
                                            } ?>

                                        </div>
                                        <div class="offer-card__wrapper-order">
                                            <div class="footbtn">
                                                <button type="button" onclick=" return true;" id="do-order <?=$arItem['ID']?>" class="btn offer-card__link-order"
                                                        data-toggle="modal" data-target="#advanced"
                                                        data-form-field-type="Купить <?= $arItem['NAME'].' '.$arParams['PRODUCT_VERSION'] ?> на срок 1 год">Заказать
                                                </button>
                                            </div>
<!--                                            <div class="offer-card__link-order">Заказать</div>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="<?= SITE_TEMPLATE_PATH ?>/js/main.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/vendor.js"></script>