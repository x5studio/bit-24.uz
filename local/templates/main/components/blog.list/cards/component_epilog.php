<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;
//$APPLICATION->AddHeadString('<link rel="canonical" href="https://bit-24.ru/blog/">');

if ($arResult['~UF_FORM_TITLE']) {
	global $APPLICATION;
	$APPLICATION->SetPageProperty('form_title', $arResult['~UF_FORM_TITLE']);
}



if (isset($arResult['MY_CANONICAL_LINK']))
	$APPLICATION->AddHeadString('<link rel="canonical" href="'.$arResult['MY_CANONICAL_LINK'].'">');
?>
