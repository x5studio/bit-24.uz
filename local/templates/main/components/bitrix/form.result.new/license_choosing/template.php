<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponentTemplate $this */
?>

<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
<div class="portal-price-form-note"><?=$arResult["FORM_NOTE"]?></div>

<!--<pre>--><?//var_dump($arResult)?><!--</pre>-->
<div class="j-form-page" data-salt-form="<?= $this->GetEditAreaId('form') ?>">
    <?=$arResult["FORM_HEADER"]?>
<?
/***********************************************************************************
						form questions
***********************************************************************************/

foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion):
	if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'):
		echo $arQuestion["HTML_CODE"];
	else:
		if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):
			?><span class="error-fld" title="<?=htmlspecialcharsbx($arResult["FORM_ERRORS"][$FIELD_SID])?>"></span><?
		endif;
		if($FIELD_SID === 'PHONE') {
            echo "<input name='form_{$arQuestion['STRUCTURE'][0]['FIELD_TYPE']}_{$arQuestion['STRUCTURE'][0]['ID']}' " .
                "data-type='REQUIRED_FROM_GROUP PHONE_NO_REQUIRED' type='text' {$arQuestion['STRUCTURE'][0]['FIELD_PARAM']}>";
        } elseif ($FIELD_SID === 'EMAIL') {
            echo "<input name='form_{$arQuestion['STRUCTURE'][0]['FIELD_TYPE']}_{$arQuestion['STRUCTURE'][0]['ID']}' " .
                "data-type='REQUIRED_FROM_GROUP EMAIL_NO_REQUIRED' type='text' {$arQuestion['STRUCTURE'][0]['FIELD_PARAM']}>";
        } else {
            echo $arQuestion["HTML_CODE"];
        }
	endif;
endforeach;

if($arResult["isUseCaptcha"] == "Y"):
	?><b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b>
	<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
	<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />

	<?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?>
	<input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" />
<?
endif; // isUseCaptcha
?>

<input class="btn red-buttn" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />

<p>
	Нажимая кнопку «Отправить»,
	вы даете свое согласие на обработку
	<a href="/politika-konfidencialnosti/" target="_blank">персональных данных</a>
</p>
<?=$arResult["FORM_FOOTER"]?>
</div>
