$(document).ready(function() {

    $('.events-single__go-to-cart-btn').on('click', function(event) {
        event.preventDefault;

        $('html, body').animate({scrollTop: $($(this).attr('href')).offset().top - $('.n_scroll_block').outerHeight()}, 300)
    });

    if (typeof ymaps === 'object') {
        ymaps.ready(function () {
            var $map = $('.events-single__map-container'),
                coords = JSON.parse($map.attr('data-coords')),
                yandexMap = new ymaps.Map($map[0], {
                    center: [coords.lon, $(window).outerWidth() > 767 ? parseFloat(coords.lat) - 0.031 : coords.lat],
                    zoom: 14,
                    controls: ['zoomControl']
                });

            yandexMap.behaviors.disable('scrollZoom');

            yandexMap.geoObjects.add(
                new ymaps.Placemark(
                    [coords.lon, coords.lat],
                    {},
                    {
                        iconLayout: 'default#image',
                        iconImageHref: $map.data('icon_path'),
                        iconImageSize: [32, 38],
                        iconImageOffset: [-16, -19]
                    }
                )
            );
        });
    }

    $('#general2page-form-7events_apply_inline').find('[data-code="TITLE"]').val($('.events-single__apply-form').data('event-title'));
    $('#general2page-form-7events_apply_inline').find('[data-code="TO"]').val($('.events-single__apply-form').data('event-to'));

});