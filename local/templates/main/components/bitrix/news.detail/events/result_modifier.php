<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!empty($arResult))
{
    $arResult['PRICE'] = !empty($arResult['PROPERTIES']['PRICE']['VALUE']) ? number_format($arResult['PROPERTIES']['PRICE']['VALUE'], 0, '', ' ').' руб' : 'Бесплатно';

    $dateTimestamp = MakeTimeStamp($arResult['FIELDS']['ACTIVE_TO']);
    $arResult['DATE_DATE'] = ToLower(FormatDate('d F Y', $dateTimestamp));
    $arResult['DATE_TIME'] = ToLower(FormatDate("H:i", $dateTimestamp));
    $arResult['DATE_DAY'] = ToLower(FormatDate('d', $dateTimestamp));
    $arResult['DATE_MONTH'] = ToLower(FormatDate('F', $dateTimestamp));

    $coords = explode(',', $arResult['PROPERTIES']['COORDS']['VALUE']);
    $arResult['COORDS'] = count($coords) === 2 ? $coords : array('', '');

    $arCity = CIblockSection::GetByID($arResult['PROPERTIES']['CITY']['VALUE'])->GetNext();
    $arResult['CITY'] = !empty($arCity) ? $arCity['NAME'] : '';
	$arResult['EVENT_STATUS'] = 1;
    if ($arResult['PROPERTIES']['PRICE']['VALUE'] == "Бесплатно") {
		$arResult['EVENT_STATUS'] = 2;
	}
}

global $APPLICATION;

$cp = $this->__component; // объект компонента
if (is_object($cp)) {
    $cp->arResult["MY_TITLE"] = $arResult["NAME"];
    $cp->SetResultCacheKeys(array('MY_TITLE'));
    $arResult['MY_TITLE'] = $cp->arResult['MY_TITLE'];
}
$APPLICATION->SetTitle(arResult['MY_TITLE']);