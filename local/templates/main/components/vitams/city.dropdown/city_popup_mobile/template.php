<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $APPLICATION;
$geo = PB\Geo\CAllGeo::getInstance();

if(empty($arParams['AJAX_FOLDER'])) {
	$arParams['AJAX_FOLDER'] = $templateFolder."/ajax.php";
}
?>
<li id="<?= $arParams["HTML_LIST_ID"] ?>" class="city-list-popup j-city-list dropdown mega-dropdown city"
     data-url-template="<?= $arParams["SEF_URL_TEMPLATES"]["section"] ?>"
     data-local-choose="<?= $arParams["LINK_LOCAL_CHOOSE"] ?>"
     data-link-subdomain="<?= $arParams["LINK_WITH_SUBDOMAIN"] ?>"
     data-region-host="<?= $_SESSION['REGION']['HOST'] ?>"
     data-link-baseurl="<?= $arParams["LINK_IN_BASEURL"] ?>"
     data-base-domains="<?= CUtil::PhpToJSObject($geo->getBaseDomains(), false, true, true) ?>"
     data-city-code-to-www="<?= CUtil::PhpToJSObject($geo->getParam('CITY_CODE2WWW'), false, true, true) ?>"
     data-domain-alias="<?= CUtil::PhpToJSObject($geo->getParam('ALIAS_DOMAIN'), false, true, true) ?>"
     data-sef-mode="<?= $arParams["SEF_MODE"] ?>"
     data-aliase-vars="<?= (!empty($arParams["VARIABLE_ALIASES"])) ? htmlspecialchars(json_encode($arParams["VARIABLE_ALIASES"]), ENT_QUOTES, 'UTF-8') : "" ?>"
     data-region-cur="<?= $_SESSION["REGION"]["CODE"] ?>"
     data-sef-folder="<?=$arParams['SEF_FOLDER']?>" 
     data-load-city-ajax="<?=$arParams['LOAD_CITY_AJAX']?>" 
     data-ajax-file="<?=$arParams['AJAX_FOLDER']?>" 
     data-smart-redirect="<?=$arParams['SMART_REDIRECT']?>" 
     data-is-confirm-dropdown="<?=$arParams['IS_CONFIRM_DROPDOWN']?>" 
     data-ui-city-prioritet="<?=implode(",",$arParams['UI_CITY_PRIORITET'])?>">
	<?
	$selectRegion = $arResult['SELECT_CODE'];
	$arResult["HTML_TITLE"] = htmlspecialcharsback(CComponentEngine::MakePathFromTemplate($arParams["HTML_TITLE"], Array(
		"HTML_LIST_ID" => $arParams["HTML_LIST_ID"],
		"NAME" => $arResult["SELECT"][$arResult['CITY'][$selectRegion]]['CITY'][$selectRegion]['NAME'],
		"NAME_DATIVE" => $arResult["SELECT"][$arResult['CITY'][$selectRegion]]['CITY'][$selectRegion]['UF_NAME_DATIVE']
	)));
	echo $arResult["HTML_TITLE"];
	?>
	<ul class="dropdown-menu mega-dropdown-menu citymenu content-popup-city">
<?
if($arParams['AJAX_MODE'] === 'Y') {
	$APPLICATION->RestartBuffer();
}
if($arParams['LOAD_CITY_AJAX'] == 'Y') {
?>
	<?/*if(count($arResult['CITY_PRIORITET']) > 0 || count($arResult['CITY_MSK_OBL']) >0):
	// $arResult['COUNT_RUSSIA_COLUMN'] = $arResult['COUNT_RUSSIA_COLUMN']-1;?>
		<div class="city-list-column">
			<?if(count($arResult['CITY_PRIORITET']) > 0){
				foreach ($arResult['CITY_PRIORITET'] as $city){
					?><p><a item-id="<?= $city['ID']; ?>" href="javascript:void(0)" rel="nofollow" data-code="<?= $city['CODE'] ?>" data-country-code="<?=$arResult['RUSSIA_ARRAY']['CODE']?>" data-smart-city="<?=$city['UF_SMART_CITY']?>"><?= $city['NAME']; ?></a></p><?
				}?>
			<?}?>
			<?if(count($arResult['CITY_MSK_OBL']) > 0) {
				?><div class="strong mo-title"><?=$arResult['MSK_OBL_NAME']?></div><?
				foreach ($arResult['CITY_MSK_OBL'] as $city){
					?><p><a item-id="<?= $city['ID']; ?>" href="javascript:void(0)" rel="nofollow" data-code="<?= $city['CODE'] ?>" data-country-code="<?= $arResult['RUSSIA_ARRAY']['CODE'] ?>"><?= $city['NAME']; ?></a></p><?
				}
			}?>
		</div>
	<?endif;*/?>
	<?
		$countRussiaCityInColumn = ceil(count($arResult['RUSSIA_ARRAY']['CITY'])/$arResult['COUNT_RUSSIA_COLUMN']);
		$x = 0;
		foreach ($arResult['RUSSIA_ARRAY']['CITY'] as $city) {
			if($x == 0 || $x%$countRussiaCityInColumn == 0) {
				?><li class="col-md-4 col-sm-4 col-xs-12"><ul><?
			}
			?><li><a item-id="<?= $city['ID']; ?>" href="javascript:void(0)" rel="nofollow" data-code="<?= $city['CODE']
			?>" data-country-code="<?= $arResult['RUSSIA_ARRAY']["CODE"] ?>" data-smart-city="<?=$city['UF_SMART_CITY']?>"><?= $city['NAME']; ?></a></li><?
			if(($x+1)%$countRussiaCityInColumn == 0 || $x == (count($arResult['RUSSIA_ARRAY']['CITY'])-1)) {
				?></ul></li><?
			}
			$x++;
		}
	?>
<?
}
if($arParams['AJAX_MODE'] === 'Y') {
	die();
}
?> 	
	 </ul>
</li>