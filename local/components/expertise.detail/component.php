<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use \Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/branches.css");

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
if (empty($arParams["SECTION_CODE"]))
{
	$this->AbortResultCache();
	if (Loader::includeModule('iblock'))
	{
		\Bitrix\Iblock\Component\Tools::process404(
			""
			, true
			, true
			, true
			, ""
		);
	}
	return;
}

if (true)
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$rsSection = CIBlockSection::GetList(
		array(),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"CODE" => $arParams["SECTION_CODE"],
		),
		false,
		array("ID", "NAME", "DETAIL_PICTURE", "DESCRIPTION", "UF_ANCHOR_LINKS",
			"UF_EXPERT_ICONS", "UF_EXPERT_ICONS_DESC", "UF_SECOND_DESC",
			"UF_SECOND_DESC_TEXT", "UF_SECOND_DESC_IMG", "UF_TARIFS_DESC",
			"UF_TARIF_BOTTOM_DESC", "UF_TARIF_BOTTOM_TEXT", "UF_TARIF_BOTTOM_IMG",
			"UF_TOP_DESC", "UF_BOTTOM_DESC", "UF_BOTTOM_BG", "UF_UNDER_DESC", "UF_FORM_TITLE"),
		array("nTopCount" => 1)
	);
	if ($arResult = $rsSection->GetNext())
	{
		$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arResult["ID"]);
		$arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();

		$arFile = array();
		$arFile[] = $arResult["DETAIL_PICTURE"];
		if ($arResult["UF_BOTTOM_BG"])
			$arFile[] = $arResult["UF_BOTTOM_BG"];

		$rsElement = CIBlockElement::GetList(
			array("SORT" => "ASC"),
			array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ACTIVE" => "Y",
				"SECTION_ID" => $arResult["ID"]
			),
			false,
			false,
			array("ID", "NAME", "CODE", "PREVIEW_TEXT", "DETAIL_TEXT", "PREVIEW_PICTURE", "PROPERTY_YOUTUBE", "PROPERTY_SHOW_VIDEO")
		);
		while ($arElement = $rsElement->GetNext())
		{
			if ($arElement["PREVIEW_PICTURE"])
				$arFile[] = $arElement["PREVIEW_PICTURE"];

			$arResult["ITEMS"][] = $arElement;
		}

		$arResult["IMAGES"] = Helper::getImages($arFile);
	} else
	{
		$this->AbortResultCache();
		if (Loader::includeModule('iblock'))
		{
			\Bitrix\Iblock\Component\Tools::process404(
				""
				, true
				, true
				, true
				, ""
			);
		}
		return;
	}

	$this->SetResultCacheKeys(array("ID", "NAME", "IPROPERTY_VALUES", "SECTION_PAGE_URL", "~UF_FORM_TITLE"));
	$this->IncludeComponentTemplate();
}
if (isset($arResult["ID"]))
{

	$arTitleOptions = null;

	if ($arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"] != "")
		$APPLICATION->SetPageProperty("title", $arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"], $arTitleOptions);
	else
		$APPLICATION->SetPageProperty("title", $arResult["NAME"], $arTitleOptions);

	if ($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
		$APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arTitleOptions);
	else
		$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);

	if ($arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"] != "")
		$APPLICATION->SetPageProperty("keywords", $arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"], $arTitleOptions);
	if ($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"] != "")
		$APPLICATION->SetPageProperty("description", $arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"], $arTitleOptions);

	$APPLICATION->AddChainItem($arResult["NAME"], $arResult["SECTION_PAGE_URL"]);

	return $arResult["ID"];
}
