<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
	<li class="col-md-3 col-sm-3 col-xs-12">
		<ul class="mdm mdm-third">
			<li class="dropdown-header hidden-xs">Полный комплекс услуг по Битрикс24</li>
			<? foreach ($arResult as $cell=>$arItem):
				if($cell%2 == 1) continue;
				?>
				<li><a class="<?= !$arItem['ICON']? $arItem["PARAMS"]["CLASS"]:'' ?>" href="<?= $arItem["LINK"] ?>">
                        <? if($arItem['ICON']): ?>
                        <img src="<?= $arItem['ICON']['src']?>" alt="<?= $arItem["TEXT"]?>" class="mdm__image"/>
                        <?endif;?>
                        <div class="mdm__text"><?=$arItem["TEXT"] ?></div>
                       </a></li>
			<? endforeach ?>
		</ul>
	</li>
	<li class="col-md-4 col-sm-4 col-xs-12">
		<ul class="mdm mdm-second">
		<? foreach ($arResult as $cell=>$arItem):
			if($cell%2 == 0) continue;
			?>
			<li><a class="<?= !$arItem['ICON']? $arItem["PARAMS"]["CLASS"]:'' ?>" href="<?= $arItem["LINK"] ?>">
                    <? if($arItem['ICON']): ?>
                        <img src="<?= $arItem['ICON']['src']?>" alt="<?= $arItem["TEXT"]?>" class="mdm__image"/>
                    <?endif;?>
                    <div class="mdm__text"><?=$arItem["TEXT"] ?></div></a></li>
		<? endforeach ?>
		</ul>
	</li>
<? endif ?>
