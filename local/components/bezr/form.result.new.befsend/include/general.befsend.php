<?

use Bitrix\Main\Loader;
use PB\Contacts\ContactManager;
use PB\Main\RequestHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

//Адресат
if (!empty($arResult["arQuestions"][$arParams["TO_QUESTION_CODE"]])) {
    $TO = &$arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["TO_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["TO_QUESTION_CODE"]][0]["ID"]];
}

if ($arParams["TO_QUESTION_RESIPIENTS"] && !empty($arResult["arQuestions"][$arParams["TO_QUESTION_CODE"]])) {
    $TO .= $TO ? ", " : "";
    $TO .= $arParams["TO_QUESTION_RESIPIENTS"];
}

if (!empty($arResult["arQuestions"][$arParams["CITY_QUESTION_CODE"]]) || !empty($arResult["arQuestions"][$arParams["OFFICE_QUESTION_CODE"]])) {
    $contMng = ContactManager::getInstance();
}
// Страница
if (!empty($arResult["arQuestions"]["PAGE"])) {

    $page =  &$arResult["arrVALUES"]["form_" . $arResult["arAnswers"]["PAGE"][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"]["PAGE"][0]["ID"]];
    $page = trim($page);
    if (empty($page)) {
        $arResult["arrVALUES"]["form_" . $arResult["arAnswers"]["PAGE"][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"]["PAGE"][0]["ID"]] = RequestHelper::getCurrentScheme().'://' . $_SERVER['HTTP_HOST'] . $APPLICATION->GetCurPage();
    } elseif (strpos($page, 'http') !== 0) {
        $page = RequestHelper::getCurrentScheme() . '://' . $_SERVER['HTTP_HOST'] . $page;
    }
}

//Город
if (!empty($arResult["arQuestions"][$arParams["CITY_QUESTION_CODE"]])) {

    $cityID = $arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["CITY_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["CITY_QUESTION_CODE"]][0]["ID"]];

    if (!empty($cityID)) {
        $arCity = $contMng->getCityById($cityID);

        if (!empty($arCity)) {
            $arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["CITY_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["CITY_QUESTION_CODE"]][0]["ID"]] = $arCity["NAME"];
        }
    }

}


//Офис
if (!empty($arResult["arQuestions"][$arParams["OFFICE_QUESTION_CODE"]])) {

    $officeID = $arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["OFFICE_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["OFFICE_QUESTION_CODE"]][0]["ID"]];

    if (!empty($officeID)) {

        if ($officeID == 'queue') {
            global $obOfficeFilter;

            $officeRegion = empty($arCity) ? $_SESSION["REGION"]["ID"] : $arCity['ID'];
            $arOffice = $contMng->getQueueInstance()->getOfficeQueue($obOfficeFilter, array("IBLOCK_SECTION_ID" => $officeRegion),null,$page);
        } else {
            $arOffice = $contMng->getOfficeById($officeID);
        }

        if (!empty($arOffice)) {
            if ($arParams['EMAIL_ONLY_FROM_RESIPIENTS'] != "Y") {
                if (CModule::IncludeModule("pb.union")) {
                    $vCity = ($cityID)?$cityID:$arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["CITY_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["CITY_QUESTION_CODE"]][0]["ID"]];
                    $vOffice = $arOffice["ID"];
                    $checkedFeilds = \Union\Form\FormResult::checkCompetentOffice($arParams["WEB_FORM_ID"], $vCity, $page,$vOffice);

                    if ($checkedFeilds && $checkedFeilds["ID_OFFICE"] <> $vOffice) {
                        $arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["CITY_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["CITY_QUESTION_CODE"]][0]["ID"]] = $checkedFeilds["NAME_CITY"];
                        $arOffice["NAME"] = $checkedFeilds["NAME_OFFICE"];
                        $arOffice["PROPERTIES"][$arParams['PROPERTY_OFFICE_EMAIL']]["VALUE"] = $checkedFeilds["TO"];
                    }
                }
                $TO .= $TO ? ", " : "";
                $TO .= $arOffice["PROPERTIES"][$arParams['PROPERTY_OFFICE_EMAIL']]["VALUE"];
            }

            if (!empty($arResult["arQuestions"]['OFFICE_PHONE'])) {
                $arResult["arrVALUES"]["form_" . $arResult["arAnswers"]['OFFICE_PHONE'][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"]['OFFICE_PHONE'][0]["ID"]] = $arOffice["PROPERTIES"]["phone"]["VALUE"];
            }

            $arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["OFFICE_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["OFFICE_QUESTION_CODE"]][0]["ID"]] = $arOffice["NAME"];
        }
    }

}

if (!empty($TO)) {
    $arTO = explode(',', $TO);
    if (count($arTO) > 1) {
        TrimArr($arTO, true);
        $arTO = array_unique($arTO);
        $TO = implode(', ', $arTO);
    }
}


//Подписка
if (!empty($arResult["arQuestions"][$arParams["SUBSCRIBE_QUESTION_CODE"]])) {
    $CHECKBOX = &$arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["SUBSCRIBE_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["SUBSCRIBE_QUESTION_CODE"]][0]["ID"]];
    $EMAIL = &$arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["EMAIL_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["EMAIL_QUESTION_CODE"]][0]["ID"]];
    if (!empty($CHECKBOX) && !empty($EMAIL)) {
        require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/include/subscribe/smartresponders.api.php');
        SubscribeUser($EMAIL, $arResult);
    }
}

//Название услуги
if (isset($arResult["arQuestions"][$arParams["SERVICE_QUESTION_CODE"]])) {
    $SERVICE = &$arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["SERVICE_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["SERVICE_QUESTION_CODE"]][0]["ID"]];
    if (empty($SERVICE) && !empty($arParams["SERVICE_SECT_ID"])) {
        $section = CIBlockSection::GetByID($arParams["SERVICE_SECT_ID"])->GetNext();
        $arResult["arrVALUES"]["form_" . $arResult["arAnswers"][$arParams["SERVICE_QUESTION_CODE"]][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"][$arParams["SERVICE_QUESTION_CODE"]][0]["ID"]] = $section["NAME"];
    }
}


if (isset($_COOKIE['_ga']) &&
    empty($arResult["arrVALUES"]["form_" . $arResult["arAnswers"]["GA_CLIENT_ID"][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"]["GA_CLIENT_ID"][0]["ID"]])
) {
    list($version, $domainDepth, $cid1, $cid2) = preg_split('[\.]', $_COOKIE["_ga"], 4);
    $cid = $cid1 . '.' . $cid2;
    $arResult["arrVALUES"]["form_" . $arResult["arAnswers"]["GA_CLIENT_ID"][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"]["GA_CLIENT_ID"][0]["ID"]] = $cid;
}

// utm метки клиента

$arUtm = array();

foreach (\PB\Main\SeoHelper::getUtmNames() as $utmName) {
    $utmValue = \PB\Main\SeoHelper::getUtm($utmName);
    if ($utmValue) {
        $arUtm[$utmName] = $utmValue;
    }
}

$arResult[
    "arrVALUES"
][
    "form_" . $arResult["arAnswers"]["CLIENT_TAGS"][0]["FIELD_TYPE"] . "_" . $arResult["arAnswers"]["CLIENT_TAGS"][0]["ID"]
] = $arUtm ?
    serialize($arUtm)
    : '';