if (typeof define === "function" && define.amd) {
    define(['Controls/Loader'], function (Loader) {
        function AjaxBaseMaiden() {
            this.controllerUrl = 'ajax.php';

            this.init();
        }

        AjaxBaseMaiden.prototype.send = function (elDOM, type, data) {
            var o = this;
            var el = $(elDOM);
            var loader = o.createLoader(el);
            if (typeof data != 'object') {
                data = {}
            }
            $.ajax({
                global: false,
                url: o.controllerUrl,
                type: type,
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    loader.defLoader.notify('start');
                },
                success: function (data) {
                    o.actionSuccess(data, el);
                },
                complete: function () {
                    loader.defLoader.resolve();
                }
            });
        };

        AjaxBaseMaiden.prototype.actionSuccess = function (data, el) {};

        AjaxBaseMaiden.prototype.init = function (p) {};

        AjaxBaseMaiden.prototype.createLoader = function (el) {
            return new Loader({
                el: el
            });
        };

        return AjaxBaseMaiden;
    });
}