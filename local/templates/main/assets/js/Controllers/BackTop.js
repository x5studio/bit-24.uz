if (typeof define === "function" && define.amd) {
    define([], function () {
        function BackTop(container) {
            var o = this;
            this.container = $(container);

            this.container.click(function () {
                o.scrollTop();
            });
        }

        BackTop.prototype.Fading = function (obj) {
            if (obj.scrollTop() > obj.height()) {
                this.container.fadeIn();
            } else {
                this.container.fadeOut();
            }
        };

        BackTop.prototype.CallFading = function () {
            var _this = this;
            this.Fading($(window));
            $(window).scroll(function () {
                _this.Fading($(this));
            });
        };

        BackTop.prototype.scrollTop = function () {
            $('body,html').animate({
                scrollTop: 0
            }, 600);
            return false;
        };

        return BackTop;
    });
}