<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Разные подходы ко внедрению Битрикс24 в организацию. Быстрая настройка Битрикс24 CRM или запуск для крупных организаций.");
$APPLICATION->SetPageProperty("title", "Типы внедрений Битрикс24");
$APPLICATION->SetTitle("Типы внедрений Битрикс24");
?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 heading">
                <div class="title-page">
                    <h1 class="title-page__h1"><? $APPLICATION->ShowTitle(true) ?></h1>
                </div>
				<p>Выбор способа автоматизации бизнеса напрямую зависит от размера компании — внедрение корпоративного портала и CRM в крупной организации кардинально отличается от внедрения этих инструментов в организации с небольшим штатом. В компании «Первый БИТ» существует несколько подходов к внедрению Битрикс24, учитывающих размер организации и ее специфику.</p>
			</div>
			<div class="col-xs-12 bl_title">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-7 title">
						<h2>Универсальное внедрение Битрикс24 CRM</h2>
						<div class="div_line"></div>
						<p>Универсальный план внедрения Битрикс24 CRM позволяет настроить базовый функционал Битрикс24 и тем самым в рекордно короткие сроки повысить эффективность работы с клиентами.</p>
						<a href="/tipy-vnedrenij-bitrix24/universalnoe/">Подробнее</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-5 wr_title_img">
						<div class="wr_img">
							<div class="img_icons">
								<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/icons_1.jpg" alt=""/>
							</div>
							<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/title_img1.jpg" alt="" class="title_img"/>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 bl_title">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-5 hidden-xs wr_title_img wr_title_img2">
						<div class="wr_img">
							<div class="img_icons">
								<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/icons_2.jpg" alt=""/>
							</div>
							<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/title_img1.jpg" alt="" class="title_img"/>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-7 title">
						<h2>Консультационное внедрение</h2>
						<div class="div_line"></div>
						<p>Запуск системы по этапам дает возможность быстро стартовать работу в Битрикс24 с минимальными затратами. Кроме внедрения платформы происходит формирование или оптимизация бизнес-процессов.</p>
						<a href="/tipy-vnedrenij-bitrix24/konsultacionnoe/">Подробнее</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-5 hidden-md hidden-sm hidden-lg wr_title_img">
						<div class="wr_img">
							<div class="img_icons">
								<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/icons_2.jpg" alt=""/>
							</div>
							<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/title_img1.jpg" alt="" class="title_img"/>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 bl_title">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-7 title">
						<h2>Проектное внедрение</h2>
						<div class="div_line"></div>
						<p>Подход ориентирован, в первую очередь, на крупные организации. Это связано с отсутствием возможности внедрения платформы итерационно, путем автоматизации небольших участков.</p>
						<a href="/tipy-vnedrenij-bitrix24/proektnoe/">Подробнее</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-5 wr_title_img">
						<div class="wr_img">
							<div class="img_icons">
								<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/icons_3.jpg" alt=""/>
							</div>
							<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/title_img1.jpg" alt="" class="title_img"/>
						</div>
					</div>
				</div>
			</div>
            <div class="col-xs-12 bl_title">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-5 hidden-xs wr_title_img wr_title_img2">
                        <div class="wr_img">
                            <div class="img_icons">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/icons_4.png" alt=""/>
                            </div>
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/title_img1.jpg" alt="" class="title_img"/>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-7 title">
                        <h2>Пакетное внедрение Битрикс24</h2>
                        <div class="div_line"></div>
                        <p>Комплексный подход к внедрению, направленный на решение любых задач.</p>
                        <a href="/tipy-vnedrenij-bitrix24/paketnoe-vnedrenie-bitrix24/">Подробнее</a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-5 hidden-md hidden-sm hidden-lg wr_title_img">
                        <div class="wr_img">
                            <div class="img_icons">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/icons_4.png" alt=""/>
                            </div>
                            <img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/title_img1.jpg" alt="" class="title_img"/>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
<? $APPLICATION->IncludeComponent(
	"client.by.id",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"EXPERTISE_ID" => array(33, 34, 32),
		"IBLOCK_ID" => "8"
	)
); ?><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
