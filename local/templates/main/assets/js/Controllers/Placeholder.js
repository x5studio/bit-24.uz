if (typeof define === "function" && define.amd) {
    define([], function () {
        function Placeholder(container) {
            var _this = this;
            this.container = $(container);
            this.container.data('placeholder', this.container.attr('placeholder'));
            this.container.removeAttr('placeholder');

            this.container.on({
                focus: function () {
                    _this.hide();
                },
                focusout: function () {
                    _this.show();
                }
            });
            _this.show();
            $(window).on('load', function(){
                _this.show();
            });
        }

        Placeholder.prototype.show = function () {
            var _this = this;
            setTimeout(function () { // чтобы успела отработать маска поля mask, если она есть
                if ($.trim(_this.container.val()) == '' || $.trim(_this.container.val()) == _this.container.data('placeholder')) {
                    _this.container.addClass('placeholder').val(_this.container.data('placeholder'));
                }
            }, 200);
        };
        Placeholder.prototype.hide = function () {
            var _this = this;
            if ($.trim(_this.container.val()) == _this.container.data('placeholder')) {
                _this.container.removeClass('placeholder').val('');
            }
        };

        return Placeholder;
    });
}