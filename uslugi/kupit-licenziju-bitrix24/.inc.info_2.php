<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<h2>Когда нужна «Команда»</h2>
<div class="cloud-text">
	<p>
		 Численность вашей компании — больше 12 человек. Вы активно обмениваетесь файлами, а также работаете с договорами и другими документами.
	</p>
	<p>
		 Вы подключаете к работе над проектами различные группы лиц — партнеров, клиентов или поставщиков.
	</p>
	<p>
		 Вы являетесь благотворительной или некоммерческой организацией и имеете в штате много сотрудников.
	</p>
</div>
 <button type="button" class="btn btn-buy center-block" data-target="#advanced" data-form-field-type="Купить облако : Когда
 нужна «Команда»" data-toggle="modal">Купить</button>