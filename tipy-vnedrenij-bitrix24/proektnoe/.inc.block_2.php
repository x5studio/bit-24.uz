<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="col-md-3 col-sm-12 col-xs-12">
	<div class="block1">
 <img src="/local/templates/main/assets/img/pr/f001.png" class="img-responsive">
		<p>
			 Невозможно выделить небольшой участок для автоматизации, после чего&nbsp;сотрудники на этом участке могли бы уже работать в системе.
		</p>
	</div>
</div>
<div class="col-md-3 col-sm-12 col-xs-12">
	<div class="block1">
 <img src="/local/templates/main/assets/img/pr/f002.png" class="img-responsive">
		<p>
			 Потенциальный большой объем доработок для реализации необходимых функциональных требований.
		</p>
	</div>
</div>
<div class="col-md-3 col-sm-12 col-xs-12">
	<div class="block1">
 <img src="/local/templates/main/assets/img/pr/f003.png" class="img-responsive">
		<p>
			 Переход со старой самописной или сильно доработанной системы.
		</p>
	</div>
</div>
<div class="col-md-3 col-sm-12 col-xs-12">
	<div class="block1">
 <img src="/local/templates/main/assets/img/pr/f004.png" class="img-responsive">
		<p>
			 Объемные интеграции с другими учетными или информационными&nbsp;системами и сервисами.
		</p>
	</div>
</div>
 <br>