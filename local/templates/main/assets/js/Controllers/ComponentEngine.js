if (typeof define === "function" && define.amd) {
    define(['Controls/Loader'], function (Loader) {
        
        function ComponentEngine() {
            this.init();
        }

        ComponentEngine.prototype.makePathFromTemplate = function (path, fromReplace, toReplace) {
        	var locSearch = window.location.search;
        	if(locSearch.length > 0) {
        		if(locSearch.substr(0,1) == '?') {
        			locSearch = locSearch.substr(1);
        		}
        		var resultSeacrh = locSearch.split("&").filter(function(elem){
        			var x = elem.split("=");
        			if(x[0] != fromReplace) {
        				return true;
        			}
        		});
        		locSearch = resultSeacrh.join("&");
        		if(locSearch.length > 0) {
        			if(path.indexOf("?") >=0) {
	    				locSearch = "&" + locSearch;
	    			}
	    			else {
	    				locSearch = "?" + locSearch;
	    			}
        		}
        	}
            return path.replace("#" + fromReplace + "#", toReplace) + locSearch;
        };

        ComponentEngine.prototype.MakeComponentVariableAliases = function (defaultAliase, customAliase) {
            if (customAliase == "")    return defaultAliase;
            $.extend(false, customAliase, defaultAliase);
            return customAliase;
        };

        ComponentEngine.prototype.init = function (p) {
        };

        ComponentEngine.prototype.createLoader = function (el) {
            return new Loader({
                el: el
            });
        }

        return ComponentEngine;
    });
}