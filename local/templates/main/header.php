<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
global $APPLICATION, $USER;

use Bitrix\Main\Page\Asset;

global $obOfficeFilter, $obCityFilter;

CUtil::InitJSCore();
//Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/template_scripts.js", true);

$contMng = ContactManager::getInstance();
$arOfficeList = $contMng->getOfficeList($obOfficeFilter, Array('IBLOCK_SECTION_ID' => $_SESSION['REGION']['ID']));

if (count($arOfficeList) == 0) {
	$arOfficeList = $contMng->getOfficeList($obOfficeFilter, Array('CODE' => 'moskva'));
}
$cities = array_shift($arOfficeList);
$emailCity = $cities["PROPERTIES"]["email"]["VALUE"];
if (defined("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE'])) && constant("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE'])) != '') {
	$emailCity .= ", " . constant("ADD_EMAILS_VALUE_" . strtoupper($_SESSION['REGION']['CODE']));
}

$curPageClean = trim($APPLICATION->GetCurPage(), '/');
$curPageSegments = explode('/', $curPageClean);
if (
    reset($curPageSegments) == 'blog'
    && count($curPageSegments) <= 2
) {
    $GLOBALS['IS_BLOG_ROOT_OR_SECTION'] = true;

    if (count($curPageSegments) == 2) {
        $GLOBALS['IS_BLOG_SECTION'] = true;
    }
}
if ($APPLICATION->GetCurPage() == '/crm-bitrix24/') {
    $GLOBALS['IS_LANDING_B24'] = true;
}

$GLOBALS['IS_APPEND_ACTION_PAGE'] = \FirstBit\Util::isAppendActionPage();

$asset = Asset::getInstance();
$asset->addString('<meta http-equiv="X-UA-Compatible" content="IE=edge">');
$asset->addString('<meta name="viewport" content="width=device-width, initial-scale=1">');
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/css/bootstrap.min.css");
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/js/vendor/font-awesome/css/font-awesome.min.css");
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/css/header.css");
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/css/style.css");
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/css/footer.css");

$asset->addCss(MAIN_TEMPLATE_PATH . "/assets/js/vendor/fancybox/dist/jquery.fancybox.min.css");
$asset->addCss(MAIN_TEMPLATE_PATH . "/assets/js/vendor/normalize-css/normalize.css");
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/css/main.css");

$asset->addCss(MAIN_TEMPLATE_PATH . "/assets/css/nstyle.css");
$asset->addCss(MAIN_TEMPLATE_PATH . "/assets/css/nt.css");
$asset->addCss('https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500&subset=cyrillic');
$asset->addCss(SITE_TEMPLATE_PATH . "/assets/css/responsive.css");
$asset->addJs(SITE_TEMPLATE_PATH . "/assets/js/vendor/jquery/dist/jquery.min.js");
$asset->addJs(SITE_TEMPLATE_PATH . "/js/template_scripts.js");
$asset->addJs(MAIN_TEMPLATE_PATH . "/assets/js/vendor/jquery-animateNumber/jquery.animateNumber.min.js");
$asset->addJs(SITE_TEMPLATE_PATH . "/assets/js/bootstrap.min.js");

$asset->addCss(MAIN_TEMPLATE_PATH . "/assets/css/mega-dropdown.css");
$asset->addJs(SITE_TEMPLATE_PATH . "/assets/js/jquery.scrollTo.min.js");

if ($GLOBALS['IS_BLOG_ROOT_OR_SECTION'] || $GLOBALS['IS_LANDING_B24']) {
    Asset::getInstance()->addString('<link rel="preload" href="'.SITE_TEMPLATE_PATH.'/assets_new/fonts/roboto-regular.woff2" as="font" crossorigin="anonymous">');
    Asset::getInstance()->addString('<link rel="preload" href="'.SITE_TEMPLATE_PATH.'/assets_new/fonts/roboto-bold.woff2" as="font" crossorigin="anonymous">');

    $asset->addCss(MAIN_TEMPLATE_PATH . "/assets_new/css/style.css");

    if ($GLOBALS['IS_LANDING_B24']) {
        Asset::getInstance()->addString('<link rel="preload" href="'.SITE_TEMPLATE_PATH.'/assets_new/fonts/PFD-Light.woff2" as="font" crossorigin="anonymous">');
        Asset::getInstance()->addString('<link rel="preload" href="'.SITE_TEMPLATE_PATH.'/assets_new/fonts/PFD-Medium.woff2" as="font" crossorigin="anonymous">');
        Asset::getInstance()->addString('<link rel="preload" href="'.SITE_TEMPLATE_PATH.'/assets_new/fonts/PFD-Bold.woff2" as="font" crossorigin="anonymous">');
        Asset::getInstance()->addString('<link rel="preload" href="'.SITE_TEMPLATE_PATH.'/assets_new/fonts/HelveticaNeue-Medium.woff2" as="font" crossorigin="anonymous">');
    }
}

$isMainPage = ($APPLICATION->GetCurDir() == '/');
$headBanner = '';

ob_start();
$APPLICATION->IncludeComponent(
	"special.banner", "", Array(
	"IBLOCK_ID" => 24
	)
);
$headBanner = ob_get_contents();
ob_get_clean();
?>
<!DOCTYPE html>
<html lang="ru">
	<head>

		<title><? $APPLICATION->ShowTitle() ?></title>
		<? $APPLICATION->IncludeFile('include_areas/pos_head.php'); ?>
		<? $APPLICATION->ShowHead() ?>
        <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<? $APPLICATION->IncludeFile('include_areas/requirejs.php') ?>
		<!-- Google Tag Manager -->
		<script>(function(w, d, s, l, i) {
				w[l] = w[l] || [];
				w[l].push({'gtm.start':
							new Date().getTime(), event: 'gtm.js'});
				var f = d.getElementsByTagName(s)[0],
						j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
				j.async = true;
				j.src =
						'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
				f.parentNode.insertBefore(j, f);
			})(window, document, 'script', 'dataLayer', 'GTM-WH9FJTD');</script>
		<!-- End Google Tag Manager -->
	</head>

	<? if (!defined('CUSTOM_LP_TEMPLATE')): ?>

		<body style="<?= $headBanner ? 'padding-top:0;' : '' ?>">
            <?if ($GLOBALS['IS_BLOG_ROOT_OR_SECTION'] || $GLOBALS['IS_LANDING_B24']):?>
                <?include (__DIR__.'/include/resource.php')?>
            <?endif;?>
			<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WH9FJTD"
							  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<!-- End Google Tag Manager (noscript) -->
			<? $APPLICATION->ShowPanel(); ?>
			<!-- Top-block -->
			<?= $headBanner ?>
			<div class="<?= $headBanner ? '' : 'n_scroll_block' ?> top_scroll_nav header">
				<section class="header__block" <? /* if (CSite::InDir('/lp/akcia/')): ?>style="display:none;"<?endif; */ ?>>
					<div class="container">
						<div class="row">
                            <div class="header__wrapper">
                                <div class="col-md-3 col-sm-3 col-xs-12 header__city">
                                    <?
                                    global $cityFilter;
                                    $cityFilter = Array(
                                        "CITY_CODE" => $_SESSION["REGION"]["CODE"],
                                    );
                                    $HTML_TITLE = '<span>Ваш регион: </span><a href="##HTML_LIST_ID#" class="city hidden-xs" data-toggle="dropdown">#NAME#</a>';
                                    $APPLICATION->IncludeComponent("vitams:city.dropdown", "city_popup", Array(
                                        "IBLOCK_TYPE" => IBT_CONTACTS, // Тип инфоблока
                                        "IBLOCK_ID" => IB_CONTACTS, // Инфоблок
                                        "DROPDOWN" => $_REQUEST["dropdown"],
                                        "SECTION_CODE" => "", // Код раздела
                                        "SECTION_URL" => "", // URL, ведущий на страницу с содержимым раздела
                                        "COUNT_ELEMENTS" => "Y", // Показывать количество элементов в разделе
                                        "TOP_DEPTH" => "1", // Максимальная отображаемая глубина разделов
                                        "SECTION_FIELDS" => "", // Поля разделов
                                        "FILTER_NAME" => "cityFilter",
                                        "SECTION_USER_FIELDS" => "", // Свойства разделов
                                        "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
                                        "CACHE_TYPE" => "A", // Тип кеширования
                                        "CACHE_TIME" => "36000000", // Время кеширования (сек.)
                                        "CACHE_NOTES" => "",
                                        "CACHE_GROUPS" => "N", // Учитывать права доступа
                                        "CACHE_FILTER" => "Y",
                                        "LINK_IN_BASEURL" => "Y",
                                        "LINK_WITH_SUBDOMAIN" => "Y",
                                        "SEF_MODE" => "N",
                                        "SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
                                        "SEF_URL_TEMPLATES" => "",
                                        "HTML_LIST_ID" => "header-city-list-desktop",
                                        "HTML_TITLE" => $HTML_TITLE,
                                        "SHOW_MOSCOW" => $_SESSION["SHOW_MOSCOW"],
                                        "COUNTRY_CODE" => $_SESSION["GEOIP"]["country_code"],
                                        "PAGER_TEMPLATE" => "",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "PAGER_TITLE" => "",
                                        "PAGER_SHOW_ALWAYS" => "N",
                                        "UI_CITY_STRONG" => "",
                                        "UI_CITY_PRIORITET" => array(
                                            0 => "67",
                                            1 => "68",
                                        ),
                                        "SMART_REDIRECT" => $smartRedirect,
                                        "LINK_LOCAL_CHOOSE" => "Y",
                                    ), false, array(
                                            "HIDE_ICONS" => "Y"
                                        )
                                    );
                                    ?>
                                    <?$APPLICATION->IncludeComponent("vitams:geo.confirm", "main", Array(
                                        "SMART_REDIRECT" => $smartRedirect,
                                        "LINK_LOCAL_CHOOSE" => "Y",
                                        "TEMPLATE_CITY_DROPDOWN" => "city_popup",

                                    ));?>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 addr">
                                    <?
                                    global $arContFilter, $arContMESS;
                                    $arContFilter['IBLOCK_SECTION_ID'] = $_SESSION['REGION']['ID'];
                                    $APPLICATION->IncludeComponent("bezr:contact.box.phone", "phone.top", Array(
                                        "OFFICE_IBLOCK_ID" => IB_CONTACTS,
                                        "DIVISION_IBLOCK_ID" => IB_DIVISION,
                                        "PAGE_OFFICE_IBLOCK_ID" => IB_PAGE_OFFICE,
                                        "FILTER_NAME" => "arContFilter",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "86400",
                                        "CACHE_FILTER" => "Y",
                                        "CACHE_GROUPS" => "N",
                                        "DETAIL_URL" => "/contacts/#SECTION_CODE#/#ELEMENT_CODE#/",
                                        "LIMIT_VIEW_ITEMS" => "2",
                                        "COMPONENT_MARKER" => $_SESSION["REGION"]["ID"],
                                        "MESS" => $arContMESS
                                        ), false, array(
                                        "HIDE_ICONS" => "Y"
                                        )
                                    );
                                    ?>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-6 ordering">
                                    <button type="button" onclick="yaCounter45927957.reachGoal('feedback-click');
                                        gtag('event', 'click', {'event_category': 'form', 'event_label': 'click'});
                                        return true;" class="btn btn-blocks top-button pull-right" data-form-field-type="Оставить заявку 1">Оставить заявку</button>
                                </div>
                            </div>
						</div>

					</div>
				</section>
				<!-- menu-block -->
				<?
				if (!CSite::InDir('/lp/bitrix24/') && !CSite::InDir('/lp/akcia/')
				):
					?>
					<section class="menu-block">
						<div class="container">
							<div class="row menu-block__wrapper">
                                <div class="col-md-3 col-sm-3 col-xs-4 header__logo" itemscope="" itemtype="http://schema.org/Organization">
                                    <a href="/" itemprop="url"><img class="header__logo-image" src="/local/templates/main/assets/img/logo.svg" alt="logo" itemprop="logo"></a>
                                    <meta itemprop="name" content="Первый БИТ">
                                    <meta itemprop="address" content="<?=$_SESSION['REGION']['ADRESS']?>">
                                    <meta itemprop="telephone" content="<?=$_SESSION['REGION']['PHONE']?>">
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 addr addr-mobile">
                                    <?$APPLICATION->ShowViewContent('office_phone_in_span');?>
                                </div>
								<div class="col-md-8 col-sm-8 col-xs-2 header__menu">
									<nav class="navbar">
										<div class="navbar-header">
											<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
												<i class="fa fa-bars" aria-hidden="true"></i>
											</button>
										</div>
										<div class="collapse navbar-collapse js-navbar-collapse">
											<!--top menu comp-->
                                            <?
                                            $APPLICATION->IncludeComponent('bitrix:menu', 'top.desktop', Array(
                                                'ALLOW_MULTI_SELECT' => 'N', // Разрешить несколько активных пунктов одновременно
                                                'CHILD_MENU_TYPE' => 'left', // Тип меню для остальных уровней
                                                'DELAY' => 'N', // Откладывать выполнение шаблона меню
                                                'MAX_LEVEL' => '2', // Уровень вложенности меню
                                                'MENU_CACHE_GET_VARS' => array(// Значимые переменные запроса
                                                    0 => '',
                                                ),
                                                'MENU_CACHE_TIME' => '3600', // Время кеширования (сек.)
                                                'MENU_CACHE_TYPE' => 'A', // Тип кеширования
                                                'MENU_CACHE_USE_GROUPS' => 'N', // Учитывать права доступа
                                                'ROOT_MENU_TYPE' => 'top', // Тип меню для первого уровня
                                                'USE_EXT' => 'Y', // Подключать файлы с именами вида .тип_меню.menu_ext.php
                                            ), false
                                            );
                                            ?>
                                            <!--top menu comp-->
										</div><!-- /.nav-collapse -->
									</nav>
								</div>
                                <div class="col-md-1 col-sm-1 hidden-xs header__search">
                                    <a class="header__phone" href="tel:+74951341820"></a>
                                    <div class="header__search-ico"></div>
                                    <div class="header__search-form">
                                        <form action="/blog/search/" method="get">
                                            <input class="header__search-submit" type="button" value="">
                                            <input class="header__search-input" type="text" name="q" placeholder="Поиск по сайту">
                                        </form>
                                    </div>
                                </div>
							</div>
						</div>
                        <!--top menu table-->
                        <?
                        $APPLICATION->IncludeComponent('bitrix:menu', 'top.tablet', Array(
                            'ALLOW_MULTI_SELECT' => 'N', // Разрешить несколько активных пунктов одновременно
                            'CHILD_MENU_TYPE' => 'left', // Тип меню для остальных уровней
                            'DELAY' => 'N', // Откладывать выполнение шаблона меню
                            'MAX_LEVEL' => '2', // Уровень вложенности меню
                            'MENU_CACHE_GET_VARS' => array(// Значимые переменные запроса
                                0 => '',
                            ),
                            'MENU_CACHE_TIME' => '3600', // Время кеширования (сек.)
                            'MENU_CACHE_TYPE' => 'A', // Тип кеширования
                            'MENU_CACHE_USE_GROUPS' => 'N', // Учитывать права доступа
                            'ROOT_MENU_TYPE' => 'top', // Тип меню для первого уровня
                            'USE_EXT' => 'Y', // Подключать файлы с именами вида .тип_меню.menu_ext.php
                        ), false
                        );
                        ?>

                        <!--top menu table-->
                        <!--top menu mobile-->
                        <div class="topmenu-mobile">
                            <div class="topmenu-mobile__city">
                                <?
                                global $cityFilter;
                                $cityFilter = Array(
                                    "CITY_CODE" => $_SESSION["REGION"]["CODE"],
                                );
                                $HTML_TITLE = '<span>Ваш регион: </span><a href="##HTML_LIST_ID#" class="city" data-toggle="dropdown">#NAME#</a>';
                                $APPLICATION->IncludeComponent("vitams:city.dropdown", "city_popup", Array(
                                    "IBLOCK_TYPE" => IBT_CONTACTS, // Тип инфоблока
                                    "IBLOCK_ID" => IB_CONTACTS, // Инфоблок
                                    "DROPDOWN" => $_REQUEST["dropdown"],
                                    "SECTION_CODE" => "", // Код раздела
                                    "SECTION_URL" => "", // URL, ведущий на страницу с содержимым раздела
                                    "COUNT_ELEMENTS" => "Y", // Показывать количество элементов в разделе
                                    "TOP_DEPTH" => "1", // Максимальная отображаемая глубина разделов
                                    "SECTION_FIELDS" => "", // Поля разделов
                                    "FILTER_NAME" => "cityFilter",
                                    "SECTION_USER_FIELDS" => "", // Свойства разделов
                                    "ADD_SECTIONS_CHAIN" => "Y", // Включать раздел в цепочку навигации
                                    "CACHE_TYPE" => "A", // Тип кеширования
                                    "CACHE_TIME" => "36000000", // Время кеширования (сек.)
                                    "CACHE_NOTES" => "",
                                    "CACHE_GROUPS" => "N", // Учитывать права доступа
                                    "CACHE_FILTER" => "Y",
                                    "LINK_IN_BASEURL" => "Y",
                                    "LINK_WITH_SUBDOMAIN" => "Y",
                                    "SEF_MODE" => "N",
                                    "SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
                                    "SEF_URL_TEMPLATES" => "",
                                    "HTML_LIST_ID" => "header-city-list-mobile",
                                    "HTML_TITLE" => $HTML_TITLE,
                                    "SHOW_MOSCOW" => $_SESSION["SHOW_MOSCOW"],
                                    "COUNTRY_CODE" => $_SESSION["GEOIP"]["country_code"],
                                    "PAGER_TEMPLATE" => "",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "PAGER_TITLE" => "",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "UI_CITY_STRONG" => "",
                                    "UI_CITY_PRIORITET" => array(
                                        0 => "67",
                                        1 => "68",
                                    ),
                                    "SMART_REDIRECT" => $smartRedirect,
                                    "LINK_LOCAL_CHOOSE" => "Y",
                                ), false, array(
                                        "HIDE_ICONS" => "Y"
                                    )
                                );
                                ?>
                            </div>
                            <div class="topmenu-mobile__search">
                                <form>
                                    <input type="text" placeholder="Поиск по сайту">
                                </form>
                            </div>
                            <?
                            $APPLICATION->IncludeComponent('bitrix:menu', 'top.mobile', Array(
                                'ALLOW_MULTI_SELECT' => 'N', // Разрешить несколько активных пунктов одновременно
                                'CHILD_MENU_TYPE' => 'left', // Тип меню для остальных уровней
                                'DELAY' => 'N', // Откладывать выполнение шаблона меню
                                'MAX_LEVEL' => '2', // Уровень вложенности меню
                                'MENU_CACHE_GET_VARS' => array(// Значимые переменные запроса
                                    0 => '',
                                ),
                                'MENU_CACHE_TIME' => '3600', // Время кеширования (сек.)
                                'MENU_CACHE_TYPE' => 'A', // Тип кеширования
                                'MENU_CACHE_USE_GROUPS' => 'N', // Учитывать права доступа
                                'ROOT_MENU_TYPE' => 'top', // Тип меню для первого уровня
                                'USE_EXT' => 'Y', // Подключать файлы с именами вида .тип_меню.menu_ext.php
                            ), false
                            );
                            ?>
                        </div>
                        <!--top menu mobile-->
					</section>

				<? endif; ?>
			</div>
			<?
            if ((CSite::InDir('/uslugi/') && !CSite::InDir('/uslugi/kupit-licenziju-bitrix24/')) ||
                CSite::InDir('/blog/') ||
                CSite::InDir('/tipy-vnedrenij-bitrix24/')):

                if (CSite::InDir('/blog/')) {
                    include __DIR__ . '/include_areas/blog-header.php';
                } else {
                    include __DIR__ . '/include_areas/middle-header.php';
                }
            elseif (!$isMainPage && !CSite::InDir('/lp/chempionat-skidok-v-bitrix24/') &&
                !CSite::InDir('/lp/bitrix24/') && !CSite::InDir('/lp/autoservice/') &&
                !CSite::InDir('/lp/medicine/') && !CSite::InDir('/lp/turism/') &&
                !CSite::InDir('/lp/krasota/') && !CSite::InDir('/lp/akcia/') &&
                !CSite::InDir('/udalenka/') && !CSite::InDir('/test/') &&
                !CSite::InDir('/korporativniy-portal-bitrix24/') &&
                !$GLOBALS['IS_LANDING_B24'] &&
                !CSite::InDir('/1c-bitrix24-store-crm/')
			):
				?>
                <hr class="my-0">
                <? $APPLICATION->IncludeComponent(
                "bitrix:breadcrumb", "main", array(
                "PATH" => "",
                "SITE_ID" => "s1",
                "START_FROM" => "0",
                "COMPONENT_TEMPLATE" => "main"
            ), false
            ); ?>

				<section class="line-header line-header__section">
					<div class="container">
						<div class="row">
							<? if (CSite::InDir('/vozmozhnosti-bitrix24/')): ?>
								<div class="col-md-8 col-sm-12 col-xs-6">
                                    <div class="title-page">
                                        <h1 class="title-page__h1"><? $APPLICATION->ShowTitle(true) ?></h1>
                                    </div>
								</div>
								<div class="col-md-4 col-sm-4 hidden-xs">
									<a href="https://auth2.bitrix24.net/create/?user_lang=ru&client_id=site.53889571c72cf8.19427820&referer=27270&second_referer=landing&ga=GA1.2.750621446.1531219303&bx_utm_content=zen.yandex.com&bx_utm_source=yandex&bx_utm_medium=cpc&bx_utm_campaign=bitrix_rsa_crm"
                                       class="btn btn-default btn-1 hidden-xs" target="_blank">Начать бесплатно</a>
								</div>
							<? else: ?>
								<div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="title-page">
                <? if (!CSite::InDir('/oblachnaya-versiya-bitrix24/') && !CSite::InDir('/korobochnaya-versiya-bitrix24/') ): ?>
                                        <h1 class="title-page__h1"><? $APPLICATION->ShowTitle(true) ?></h1>
                <? endif; ?>
                                    </div>
								</div>
							<? endif; ?>
						</div>
					</div>
				</section>
			<? endif; ?>
		<? endif; ?>

        <?if ($GLOBALS['IS_BLOG_ROOT_OR_SECTION']):?>
            <main class="new-design">
        <?endif;?>

