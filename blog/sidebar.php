<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<div class="col-xs-12 col-sm-6 col-md-3 sidebar-offcanvas" id="sidebar">
	<div class="navbar">
		<nav role="navigation">
			<?$APPLICATION->IncludeComponent("bitrix:menu", "blog", Array(
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
					"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
					"DELAY" => "N",	// Откладывать выполнение шаблона меню
					"MAX_LEVEL" => "1",	// Уровень вложенности меню
					"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
						0 => "",
					),
					"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"MENU_CACHE_TYPE" => "A",	// Тип кеширования
					"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
					"ROOT_MENU_TYPE" => "section",	// Тип меню для первого уровня
					"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
				),
				false
			);?>
			<?$APPLICATION->IncludeComponent(
				"blog.tag",
				"",
				Array(
					"CACHE_TIME" => "3600",
					"CACHE_TYPE" => "A",
					"IBLOCK_BLOG_ID" => $arParams["IBLOCK_ID"],
					"IBLOCK_TAG_ID" => "1",
					"SECTION_CODE" => $arParams["SECTION_CODE"],
					"TAG" => $_REQUEST["tag"]
				)
			);?>
			<?$APPLICATION->IncludeComponent("asd:subscribe.quick.form", "blog", Array(
				"FORMAT" => "html",	// Формат подписки
				"INC_JQUERY" => "N",	// Подключить jQuery
				"NOT_CONFIRM" => "N",	// Подписывать без подтверждения
				"RUBRICS" => array(	// Подписывать на рубрики
					0 => "1",
				),
				"SHOW_RUBRICS" => "N",	// Показывать рубрики
			),
				false
			);?>
		</nav>
	</div>
</div>
<!-- Main-content -->
<div class="hidden-lg hidden-md hidden-xs col-sm-6 col-xs-12 filter">
	<a href="#" class="nav-toggle" data-toggle="offcanvas" onclick="demoDisplay()">Разделы блога<i class="fa fa-filter" aria-hidden="true"></i></a>
</div>
<div class="hidden-lg hidden-md col-sm-6 hidden-xs">
	<form action="/blog/search/" method="get">
		<div class="input-group search">
			<input type="text" name="q" class="form-control" placeholder="Поиск">
			<span class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
		</div>
	</form>
</div>