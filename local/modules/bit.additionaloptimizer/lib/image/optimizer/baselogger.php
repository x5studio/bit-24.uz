<?php

namespace Bit\AdditionalOptimizer\Image\Optimizer;

use Bit\AdditionalOptimizer\Main;

class BaseLogger implements LoggerInterface
{
    public function emergency($message, array $context = [])
    {
        Main::logger('[emergency] '. $message);
    }

    public function alert($message, array $context = [])
    {
        Main::logger('[alert] '. $message);
    }

    public function critical($message, array $context = [])
    {
        Main::logger('[critical] '. $message);
    }

    public function error($message, array $context = [])
    {
        Main::logger('[error] '. $message);
    }

    public function warning($message, array $context = [])
    {
        Main::logger('[warning] '. $message);
    }

    public function notice($message, array $context = [])
    {
        Main::logger('[notice] '. $message);
    }

    public function info($message, array $context = [])
    {
        Main::logger('[info] '. $message);
    }

    public function debug($message, array $context = [])
    {
        Main::logger('[debug] '. $message);
    }

    public function log($level, $message, array $context = [])
    {
        Main::logger('[log] '. $message);
    }
}
