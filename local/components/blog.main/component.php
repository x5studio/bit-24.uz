<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;


if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
$arParams["COUNT"] = 3;

if ($this->StartResultCache())
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}


	$arFile = array();

	$rsElement = \CIBlockElement::GetList(
		array("ACTIVE_FROM" => "DESC"),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"PROPERTY_ONMAIN" => "Y",
			//"<DATE_ACTIVE_FROM" => ConvertTimeStamp(time(), "SHORT"),
			'ACTIVE_DATE' => 'Y'
		),
		false,
		array('nTopCount' => $arParams["COUNT"]),
		array("ID", "NAME", "IBLOCK_SECTION_ID", "DETAIL_PAGE_URL", "PREVIEW_TEXT", "PREVIEW_PICTURE", "ACTIVE_FROM")
	);
	while ($arElement = $rsElement->GetNext())
	{

		$arFile[] = $arElement["PREVIEW_PICTURE"];
		$arResult["SECTIONS"][$arElement["IBLOCK_SECTION_ID"]] = "";

		$arElement["DISPLAY_ACTIVE_FROM"] = ToLower(FormatDate("d F Y", MakeTimeStamp($arElement["ACTIVE_FROM"])));

		$arResult["ITEMS"][] = $arElement;
	}

	$arResult["IMAGES"] = Helper::getImages($arFile);
	if (count($arResult["SECTIONS"]) > 0)
	{
		$rsSection = \CIBlockSection::GetList(
			array(),
			array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ID" => array_keys($arResult["SECTIONS"])
			),
			false,
			array("ID", "NAME", "SECTION_PAGE_URL"));
		while ($arSec = $rsSection->GetNext())
		{
			$arResult["SECTIONS"][$arSec["ID"]] = $arSec;
		}
	}

	$this->IncludeComponentTemplate();
}