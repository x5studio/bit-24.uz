<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}

$arParams["COUNT"] = intval($arParams["COUNT"]);
if ($arParams["COUNT"] <= 0)
	$arParams["COUNT"] = 6;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"] == "Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"] != "N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] != "N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"] == "Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"] !== "N";

if ($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if ($arNavigation["PAGEN"] == 0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] > 0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
} else
{
	$arNavParams = array(
		"nTopCount" => $arParams["COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

if ($this->StartResultCache(false, array($arNavigation)))
{
	if (!CModule::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y"
	);

	if (!empty($arParams['FRONTPAGE_STYLE']) && $arParams['FRONTPAGE_STYLE'] == 'Y')
    {
        $arFilter['!UF_SHOW_ON_FRONTPAGE'] = false;
    }

	$arFile = array();

	$rsSection = \CIBlockSection::GetList(
		array("SORT" => "ASC", "NAME" => "ASC"),
		$arFilter,
		false,
		array("ID", "NAME", "PICTURE", "SECTION_PAGE_URL", "DESCRIPTION", "UF_SHOW_ON_FRONTPAGE"),
		$arNavParams
	);
	while ($arSection = $rsSection->GetNext())
	{

		$arFile[] = $arSection["PICTURE"];

		$arResult["ITEMS"][] = $arSection;
	}

	$arResult["IMAGES"] = Helper::getImages($arFile);

    if (!empty($arParams['FRONTPAGE_STYLE']) && $arParams['FRONTPAGE_STYLE'] == 'Y')
    {
        $arResult['NAV_STRING'] = '';
    }
    else
    {
        $arResult["NAV_STRING"] = $rsSection->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
    }

	$this->IncludeComponentTemplate();
}