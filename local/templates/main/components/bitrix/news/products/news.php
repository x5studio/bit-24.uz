<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$order=$request->getQuery('order');
$sort =$request->getQuery('sort');
$show =$request->getQuery('show');
$city = $request->getCookieRaw('BITRIX_SM_SMART_CHANGE_CITY');
$uri = new \Bitrix\Main\Web\Uri($request->getRequestUri());
if(empty($city) || $city==NULL){
    $city = $request->getCookieRaw('BITRIX_SM_CONFIRM');
    if(empty($city) || $city==NULL){
        $city=$_SESSION['GEOIP']['country']['id'];
    }
}
global $arrFilter;
$arrFilter['PROPERTY_CITY']=$city;
?>
<main>
    <section class="goods">
        <div class="goods__wrapper-title">
            <h1 class="container"><?=$arParams['PAGER_TITLE']?></h1>
        </div>
        <div class="goods__inner container">
            <div class="goods__controls">
                <div class="goods__control">
                    <label class="goods__label-select-sort">
                        <?if($arParams['PRODUCT_VERSION']=='Коробочная'){?>
                            <select class="goods__select-sort" onchange="window.location.href = this.options[this.selectedIndex].id">
                                <option value="popular" <?if($sort=='shows' && $order=='DESC'){?>selected <?}?> id="<?=$uri->addParams(array("sort"=>"shows","order"=>"DESC"));?>">Сначала популярные</option>
                                <option value="increase-price" <?if($sort=='PROPERTY_KOROBKA_PRICE' && $order=='ASC'){?>selected <?}?>id="<?=$uri->addParams(array("sort"=>"PROPERTY_KOROBKA_PRICE","order"=>"ASC"));?>">Цена по возрастанию</option>
                                <option value="decrease-price" <?if($sort=='PROPERTY_KOROBKA_PRICE' && $order=='DESC'){?>selected <?}?> id="<?=$uri->addParams(array("sort"=>"PROPERTY_KOROBKA_PRICE","order"=>"DESC"));?>">Цена по убыванию</option>
                            </select>
                        <?}else{?>
                        <select class="goods__select-sort" onchange="window.location.href = this.options[this.selectedIndex].id">
                            <option value="popular" <?if($sort=='shows' && $order=='DESC'){?>selected <?}?> id="<?=$uri->addParams(array("sort"=>"shows","order"=>"DESC"));?>">Сначала популярные</option>
                            <option value="increase-price" <?if($sort=='PROPERTY_PERIOD_PRICE_1YEAR' && $order=='ASC'){?>selected <?}?>id="<?=$uri->addParams(array("sort"=>"PROPERTY_PERIOD_PRICE_1YEAR","order"=>"ASC"));?>">Цена по возрастанию</option>
                            <option value="decrease-price" <?if($sort=='PROPERTY_PERIOD_PRICE_1YEAR' && $order=='DESC'){?>selected <?}?> id="<?=$uri->addParams(array("sort"=>"PROPERTY_PERIOD_PRICE_1YEAR","order"=>"DESC"));?>">Цена по убыванию</option>
                        </select>
                        <?}?>
                    </label>
                </div>
                <div class="goods__control">
                   <a href="<?=$uri->addParams(array("show"=>"list"));?>"> <button class="goods__type goods__type--list <?if($show=='list'){?>goods__type--active<?}?>" data-name="list" type="button">
                        <svg width="18" height="14">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite_auto.svg#list-item"></use>
                        </svg>
                    </button>
                   </a>
                    <a href="<?=$uri->addParams(array("show"=>"block"));?>"> <button class="goods__type goods__type--block <?if(empty($show)||$show=='block'){?>goods__type--active<?}?>" data-name="block" type="button">
                        <svg width="18" height="18">
                            <use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/img/sprite_auto.svg#block-item"></use>
                        </svg>
                    </button>
                    </a>
                </div>
            </div>
<?php

$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"",
	Array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"NEWS_COUNT" => $arParams["NEWS_COUNT"],
		"SORT_BY1" => $sort,
		"SORT_ORDER1" => $order,
//		"SORT_BY2" => $arParams["SORT_BY2"],
//		"SORT_ORDER2" => $arParams["SORT_ORDER2"],
		"FIELD_CODE" => $arParams["LIST_FIELD_CODE"],
		"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"IBLOCK_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"],
		"DISPLAY_PANEL" => $arParams["DISPLAY_PANEL"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"INCLUDE_IBLOCK_INTO_CHAIN" => $arParams["INCLUDE_IBLOCK_INTO_CHAIN"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"DISPLAY_DATE" => $arParams["DISPLAY_DATE"],
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => $arParams["DISPLAY_PICTURE"],
		"DISPLAY_PREVIEW_TEXT" => $arParams["DISPLAY_PREVIEW_TEXT"],
		"PREVIEW_TRUNCATE_LEN" => $arParams["PREVIEW_TRUNCATE_LEN"],
		"ACTIVE_DATE_FORMAT" => $arParams["LIST_ACTIVE_DATE_FORMAT"],
		"USE_PERMISSIONS" => $arParams["USE_PERMISSIONS"],
		"GROUP_PERMISSIONS" => $arParams["GROUP_PERMISSIONS"],
		"FILTER_NAME" => 'arrFilter',
		"HIDE_LINK_WHEN_NO_DETAIL" => $arParams["HIDE_LINK_WHEN_NO_DETAIL"],
		"CHECK_DATES" => $arParams["CHECK_DATES"],
        "PARENT_SECTION"=>$arParams['PARENT_SECTION'],
        'TIP_DESCR'=>$arParams['TIP_DESCR'],
        'PRODUCT_VERSION'=>$arParams['PRODUCT_VERSION']
	),
	$component
);?>
    </section>

</main>