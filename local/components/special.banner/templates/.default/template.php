<?if(!empty($arResult["ITEM"])):?>
<?
$mobileImage = CFile::GetPath($arResult["ITEM"]["PREVIEW_PICTURE"]);
$Image = CFile::GetPath($arResult["ITEM"]["DETAIL_PICTURE"]);
?>
    <div class="custom_banner">
        <a href="<?=($arResult["ITEM"]["PROPERTY_URL_VALUE"])?$arResult["ITEM"]["PROPERTY_URL_VALUE"]:'javascript:void(0)'?>">
            <picture>
                <?if($mobileImage):?>
                    <source srcset="<?=$mobileImage?>" media="(max-width: 768px)">
                <?endif;?>
                <source srcset="<?=$Image?>">
                <img src="<?=$Image?>" alt="<?=$arResult["ITEM"]["NAME"]?>">
            </picture>
        </a>
        <i class="fa fa-times custom_banner_close_button" aria-hidden="true" data-banner-id="<?=$arResult["ITEM"]["ID"]?>"></i>
    </div>
<?endif?>
