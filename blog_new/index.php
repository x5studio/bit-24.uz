<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Блог по Битрикс24 от команды первый бит");
$APPLICATION->SetTitle("Блог по Битрикс24 от команды первый бит");
$APPLICATION->SetAdditionalCss("/blog_new/styles.css");
$APPLICATION->SetAdditionalCss("/blog_new/js/slick/slick.css");
$APPLICATION->AddHeadScript("/blog_new/js/slick/slick.min.js");
?>


<div class="blog-articles-wrap">
	<div class="container">
		<div class="row">

			<article class="blog-article long-article with-bg col-sm-8 blog-carousel">
				
				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/1_article.png)">
					<span class="title">Новое весеннее Обновление для мобильного приложения Битрикс24</span>
					<div class="tag-date-wrap">
						<span class="tag deep-blue">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>

				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/1_article.png)">
					<span class="title">Новое весеннее Обновление для мобильного приложения Битрикс24</span>
					<div class="tag-date-wrap">
						<span class="tag deep-blue">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>

				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/1_article.png)">
					<span class="title">Новое весеннее Обновление для мобильного приложения Битрикс24</span>
					<div class="tag-date-wrap">
						<span class="tag deep-blue">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>

				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/1_article.png)">
					<span class="title">Новое весеннее Обновление для мобильного приложения Битрикс24</span>
					<div class="tag-date-wrap">
						<span class="tag deep-blue">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>

				<div class="blog-carousel-control">
					<div class="left-arrow arrow">
						<img src="/blog_new/images/carousel_arrow.png" />
					</div>
					<span class="current-slide">1<span class="grey-color">/4</span></span>
					<div class="right-arrow arrow">
						<img src="/blog_new/images/carousel_arrow.png" />
					</div>
				</div>		

			</article>

			<article class="blog-article with-bg col-sm-4">
				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/2_article.png)">
					<span class="title">Обновление для мобильного приложения Битрикс24</span>
					<div class="tag-date-wrap">
						<span class="tag light-blue">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>
			</article>

			<article class="blog-article with-bg col-sm-4">
				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/3_article.png)">
					<span class="title">КАК ВНЕДРЯТЬ БИТРИКС24 – ОСНОВНЫЕ ПОДХОДЫ, ЭТАПЫ, РЕКОМЕНДАЦИИ</span>
					<div class="tag-date-wrap">
						<span class="tag white">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>
			</article>

			<article class="blog-article with-bg col-sm-4">
				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/4_article.png)">
					<span class="title">Обновление для мобильного приложения Битрикс24</span>
					<div class="tag-date-wrap">
						<span class="tag deep-blue">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>
			</article>

			<article class="blog-article transparent-bg col-sm-4">
				<a href="#" class="article-content">
					<span class="title">Как использовать новую CRM в Битрикс24, кроме продаж</span>
					<p>Онлайн-кассы – «горячая» тема:
					розничные продавцы по крупицам
					собирают информацию, чтобы понять,
					как модернизировать </p>
					<div class="tag-date-wrap">
						<span class="tag pink">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>
			</article>
			</div>

	</div>

	<section class="subscribe-news text-center">
		<div class="subscribe-news-form-wrap">
			<div class="corner-bg">
				<span class="title">Бесплатная подписка на новости бит24.ru</span>
				<p>Никакого спама - только максимально полезные статьи, написанные с любовью</p>
				<form class="clearfix" action="">
					<input type="text" placeholder="E-mail" class="form-control" />
					<button class="btn btn-default submit">Отправить</button>
				</form>
				<p class="agree-notice">Нажимая кнопку Вы соглашаетесь получать от нас полезные письма о инструментах Битрикс24.</p>
			</div>
		</div>
	</section>

	<div class="container">
		<div class="row">

			<article class="blog-article transparent-bg col-sm-4">
				<a href="#" class="article-content">
					<span class="title">Как использовать новую CRM в Битрикс24, кроме продаж</span>
					<p>Онлайн-кассы – «горячая» тема:
					розничные продавцы по крупицам
					собирают информацию, чтобы понять,
					как модернизировать </p>
					<div class="tag-date-wrap">
						<span class="tag deep-blue">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>
			</article>

			<article class="blog-article long-article with-bg col-sm-8">
				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/5_article.png)">
					<span class="title">Новое весеннее Обновление для мобильного <br/>приложения Битрикс24</span>
					<div class="tag-date-wrap">
						<span class="tag deep-blue">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>
			</article>

			<article class="blog-article long-article with-bg col-sm-8">
				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/6_article.png)">
					<span class="title">Как использовать новую CRM в Битрикс24, <br/>кроме продаж</span>
					<div class="tag-date-wrap">
						<span class="tag deep-blue">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>
			</article>

			<article class="blog-article with-bg col-sm-4">
				<a href="#" class="article-content" style="background-image: url(/blog_new/images/articles_previews/7_article.png)">
					<span class="title">КАК ВНЕДРЯТЬ БИТРИКС24 – ОСНОВНЫЕ ПОДХОДЫ, ЭТАПЫ, РЕКОМЕНДАЦИИ</span>
					<div class="tag-date-wrap">
						<span class="tag white">Интернет-магазин</span>
						<span class="date">14 апреля 2017</span>
					</div>
				</a>
			</article>

		</div>

		<nav class="pagination-row clearfix">
			<a class="previous hidden-xs" href="#"><span aria-hidden="true"><< </span><span class="hidden-xs" aria-hidden="true">Предыдущая</span></a>
			<ul class="pagination">
					<li><a class="active">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><span class="dots">...<span></li>
					<li><a href="#">17</a></li>
			</ul>
			<a class="next hidden-xs" href="#"><span class="hidden-xs" aria-hidden="true">Следующая</span><span aria-hidden="true"> >></span></a>
		</nav>

	</div>
</div>

<script>
	$(document).ready( function(){
		var $status = $('.current-slide');
		var $slickElement = $('.blog-carousel');

		$slickElement.on('init reInit', function(event, slick, currentSlide, nextSlide){
		    $status.html('<span class="current-slide">1<span class="grey-color">/' + slick.slideCount + '</span></span>');
		});

		$slickElement.on('afterChange', function(event, slick, currentSlide, nextSlide){
		    var i = currentSlide + 1;
		    $status.html('<span class="current-slide">' + i + '<span class="grey-color">/' + slick.slideCount + '</span></span>');
		});

		$('.blog-carousel').slick({
			slide: '.article-content',
			prevArrow: '.blog-carousel-control .left-arrow',
			nextArrow: '.blog-carousel-control .right-arrow'
		});
	} );
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>