<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
// $cityName - город из квиза
// $formData - данные формы для "WEB_FORM_ID" = 1

use PB\Contacts\ContactManager;

    // ID веб-формы
    $arParams = array(
		"WEB_FORM_ID" => 1,  // ID веб-формы
		"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
		"COMPANY_IBLOCK_ID" => IB_CONTACTS,
		"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
		"EMAIL_QUESTION_CODE" => "EMAIL",
		"CITY_QUESTION_CODE" => "CITY",
        "OFFICE_QUESTION_CODE" => "OFFICE",
		"TO_QUESTION_RESIPIENTS" => "",
		"EMAIL_ONLY_FROM_RESIPIENTS" => "",
		"PRODUCT_QUESTION_CODE" => "PRODUCT",
		"TO_QUESTION_CODE" => "TO",
		"USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
		"ALERT_ADD_SHARE" => "N",
		"HIDE_PRIVACY_POLICE" => "Y",
		"COMPONENT_MARKER" => "request",
    );

    file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] TO_FORM -- '.print_r($cityName, true)."\n\n", FILE_APPEND);
    file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] TO_FORM -- '.print_r($formData, true)."\n\n", FILE_APPEND);

    if (CModule::IncludeModule("form")) {
        


        global $obOfficeFilter;

        $contMng = ContactManager::getInstance();


        $arCity = $contMng->getCityList();
        
        $currentCity = array();
        
		foreach($arCity as $City){
			if(strpos(strtolower($City['NAME']), strtolower($cityName)) !== false){
				$currentCity = $City;
			}
        }
        
        if(!$currentCity) {

            $currentCity = $contMng->getCityByCode('msk');

        }

        $currentCityID = $currentCity['ID'];

        $arOffice = $contMng->getQueueInstance()->getOfficeQueue($obOfficeFilter, array("IBLOCK_SECTION_ID" => $currentCityID),null, '');


        $arForm = CForm::GetByID($arParams["WEB_FORM_ID"])->Fetch();
        $arParams["WEB_FORM_ID"] = CForm::GetDataByID($arParams["WEB_FORM_ID"], $arResult["arForm"], $arResult["arQuestions"], $arResult["arAnswers"], $arResult["arDropDown"], $arResult["arMultiSelect"], 'N');


        $arResult['arrVALUES'] = array(
            'form_text_1' => '',//имя
            'form_text_2' => '',//телефон
            'form_text_3' => '',//почта
            'form_hidden_4' => $currentCityID, // ид города
            'form_hidden_5' => 'queue', //офис
            'form_hidden_6' => '', //юнион
            'form_hidden_7' => ($arOffice) ? $arOffice['PROPERTIES']['email']['VALUE'] : '',//почта офиса
            'form_hidden_8' => '',
            'form_hidden_27' => '',
            'form_hidden_30' => '',
            'form_hidden_94' => '',
            'form_hidden_84' => '',
            'form_hidden_167' => '',
        );
        
        $arResult['arrVALUES'] = array_merge($arResult['arrVALUES'], $formData);
        $arResult['arrOrigVALUES'] = $arResult['arrVALUES'];


        include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/bezr/form.result.new.befsend/include/general.befsend.php");

        $vCity = $arResult['arrOrigVALUES']["form_{$arResult["arAnswers"]["CITY"][0]['FIELD_TYPE']}_{$arResult["arAnswers"]["CITY"][0]['ID']}"];
        $vOffice = $arResult['arrOrigVALUES']["form_{$arResult["arAnswers"]["OFFICE"][0]['FIELD_TYPE']}_{$arResult["arAnswers"]["OFFICE"][0]['ID']}"];

        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] TO_FORM -- '.print_r($arResult, true)."\n\n", FILE_APPEND);

        if ($RESULT_ID = CFormResult::Add($arParams["WEB_FORM_ID"], $arResult["arrVALUES"])) {
            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] RESULT_ID -- '.print_r($RESULT_ID, true)."\n\n", FILE_APPEND);

            // send email notifications
            CFormResult::SetEvent($RESULT_ID);

            if (CModule::IncludeModule("pb.union") && \Union\Form\FormResult::checkSendCrm($arResult)) {
                \Union\Form\FormResult::Mail($RESULT_ID, $arResult['SEND_MAIL_ONLY'], $arParams["WEB_FORM_ID"], $vCity, $APPLICATION->GetCurDir(), $arResult["arAnswers"]["UNIONRESULT"][0]["ID"]);
            } else {
                if (!empty($arResult['SEND_MAIL_ONLY'])) {
                    foreach ($arResult['SEND_MAIL_ONLY'] AS $mailTmplItem) {
                        CFormResult::Mail($RESULT_ID, $mailTmplItem);
                    }
                } else {
                    CFormResult::Mail($RESULT_ID);
                }
            }

            
            $arFilterSid = array("IBLOCK_SECTION_ID" => $vCity);
            
            $sid = $contMng->getQueueInstance()->getQueueSID($obOfficeFilter, $arFilterSid);
            
            $cursor = ($sid)?$contMng->getQueueInstance()->getCursorQueue($sid):"";
            // global $strError;

            // if(!$strError) $strError = 'Что-то пошло не так!';
            // file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] TO_FORM -- '.print_r($strError, true)."\n\n", FILE_APPEND);

        } else {

            global $strError;

            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] TO_FORM -- '.print_r($strError, true)."\n\n", FILE_APPEND);

        }


        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.date('d-m-Y').'-'.'webhooks'.'.txt', '['.date('d-m-Y H:i:s').'] TO_FORM -- '.print_r('---------------------------', true)."\n\n", FILE_APPEND);

    }
