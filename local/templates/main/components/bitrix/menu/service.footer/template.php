<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)): ?>
<span class="col-md-12 col-sm-12 hidden-xs h4">Услуги</span>
<nav class="col-md-12 hidden-sm hidden-xs">
	<ul class="list-unstyled">
		<? foreach ($arResult as $arItem): ?>
		<li><a href="<?= $arItem["LINK"] ?>" title="<?= $arItem["TEXT"] ?>"><?= $arItem["TEXT"] ?></a></li>
<? endforeach ?>
	</ul>
</nav>
<? endif ?>