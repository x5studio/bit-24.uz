/**
 * Created by psbezrukov on 22.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/ShuffleWeight', 'tools/tools'], function ($, ShuffleWeight) {
        function ContactManager(container, options) {
            this.container = $(container);
            this._arCity = [];
            this._arCityIDs = {};
            this._arOffice = [];
            this._arOfficeIDs = {};
            this._arAllOffice = [];
            this._arAllOfficeIDs = {};
            this._arRandOffice = null;
            this.options = $.extend({}, ContactManager.defaultOptions, options);
        }

        ContactManager.TYPE_DIVISION = 'D';
        ContactManager.TYPE_OFFICE = 'O';
        ContactManager.defaultOptions = {};

        ContactManager.prototype.addData = function (key, arData) {
            var o = this;
            if (!$.inArray(key, ['city', 'office', 'allOffice'])) {
                throw new ReferenceError('Ошибочный ключ данных');
            }
            key = '_ar' + ucwords(key);
            for (var i in arData) {
                var itemData = arData[i];
                if (!o[key + 'IDs'].hasOwnProperty(itemData['ID'])) {
                    o[key].push(itemData);
                    o[key + 'IDs'][itemData['ID']] = o[key].length - 1;
                }
            }
        };

        ContactManager.prototype.getCities = function () {
            return this._arCity
        };

        ContactManager.prototype.getOffices = function () {
            return this._arOffice
        };

        ContactManager.prototype.getAllOfficeList = function () {
            return this._arAllOffice
        };

        ContactManager.prototype.getOfficeByID = function ($ID, isAll) {
            var o = this;
            if (typeof isAll == 'undefined') {
                isAll = false;
            }
            if (isAll) {
                var keyOffice = o._arAllOfficeIDs[$ID];
                return o.getAllOfficeList()[keyOffice];
            } else {
                var keyOffice = o._arOfficeIDs[$ID];
                return o.getOffices()[keyOffice];
            }
        };

        ContactManager.prototype.getRealOfficeByID = function ($ID) {
            var o = this;
            var office = o.getOfficeByID($ID);
            if (office['TYPE'] == ContactManager.TYPE_DIVISION && typeof office['PROPERTIES']['OFFICE'] == 'object') {
                var realOffice = o.getOfficeByID(office['PROPERTIES']['OFFICE']['VALUE'], true);
                if (typeof realOffice != 'undefined') {
                    office = realOffice;
                }
            }
            return office;
        };

        ContactManager.prototype.getOfficeList = function ($obContFilter, $arFilter) {
            var o = this;
            if ($obContFilter) {
                var $arOffice = $obContFilter.dataFilter(o.getOffices(), $arFilter);
            }
            else {
                $arOffice = o.getOffices();
            }

            return $arOffice;
        };

        ContactManager.prototype.getOfficePowered = function ($obContFilter, $arFilter) {
            var o = this;
            if (typeof $arFilter["IBLOCK_SECTION_ID"] == 'undefined') {
                throw new ReferenceError('В $arFilter не передается IBLOCK_SECTION_ID');
            }
            else {
                if (o._arRandOffice == null) {
                    var $arOffice = o.getOfficeList($obContFilter, $arFilter);
                    var obSorter = new ShuffleWeight();
                    var $arPower = [];
                    for (var i in $arOffice) {
                        var $arItem = $arOffice[i];
                        $arPower[$arItem['ID']] = $arItem['PROPERTIES']['POWER']['VALUE'];
                        obSorter.add($arItem);
                    }
                    if ($.inArray("", $arPower) == -1) {
                        obSorter.sort(function (item) {
                            return item['PROPERTIES']['POWER']['VALUE']
                        });
                        o._arRandOffice = obSorter.getList();
                    }
                    else {
                        o._arRandOffice = $arOffice;
                    }
                }

                return o._arRandOffice;
            }
        };

        return ContactManager;
    });
}