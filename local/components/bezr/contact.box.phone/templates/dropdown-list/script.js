require(['Controllers/Loc'], function (Loc) {
    var l = Loc.getWithPrefix('PB_CONTACTS_CT_CB_DL_JS_');

    require(['main'], function () {
        var initializeDoubleOffices = function () {
            $('.j-box-offices').on('jBoxOffices.init', function () {
                if ($('.box-offices-item').size() > 4) {
                    new DoubleOffices({
                        root: $('.double-office'),
                        button: $('.box-offices-control span, .box-content > .close'),
                        close: $('.box-content > .close')
                    });
                    $('.box-content .contacts-bg .phone-bg').click(function () {
                        track([l('ACTIONS'), l('CLICK'), l('MOSCOW_CBLOCK'), 1]);
                        $('.box-content .contacts-bg').hide();
                        $('.box-offices-control span').show().trigger('click');
                        $('.box-offices-control').show();
                    });
                }
                $('.office-email').click(function (e) {
                    e.preventDefault();
                    document.location = "mailto:" + $(this).html();
                });
            });

            var boxOfficeObj = $('.box-offices-item');
            var officesAr = new Array();
            $(boxOfficeObj).find('span:not(.box-office-phone)').each(function () {
                officesAr.push($(this).text());
            });
            track([l('CBLOCK'), l('OFFICE_SHOW'), officesAr.join("|"), {'nonInteraction': 1}]);

        };
        compositeFinished(function () {
            $('.j-box-offices').trigger("contactBox.init");
            initializeDoubleOffices();
        });
    });

    function DoubleOffices(p) {
        var open = 'box-offices-opened';
        var text = $(p.button, p.root).text();
        p.close.css({opacity: 0, display: 'none'});
        setTimeout(function () {
            $(p.button, p.root).click(function () {
                var b = $(this);
                if (p.root.hasClass(open)) {
                    $('.box-content .contacts-bg').show();
                    $('.box-offices-control').hide();
                    p.close.hide().css({opacity: 0});
                    $('.box-content', p.root).css('min-height', '');
                    p.root.removeClass(open);
                    $('.box-offices-control span').text(window.officetext);
                } else {
                    setTimeout(function () {
                        p.close.show().animate({opacity: 1}, 500);
                    }, 250);
                    var size = $('.box-offices-item', p.root).size();
                    size = (size * 75) / 2;
                    size = size > 140 ? size : 140;
                    $('.box-content', p.root).css('min-height', size);
                    setTimeout(function () {
                        p.root.addClass(open);
                        window.officetext = text;
                        $(b).text(l('SLIDE_DOWN'));
                    }, 200);
                }
            });
        }, 100)
    }
});