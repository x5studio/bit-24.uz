if (typeof define === "function" && define.amd) {
    define([], function () {
        function SimpleTabs(p) {
            var o = this;
            this.root = $(p.root);
            this.tabNav = $(p.tabNav, this.root);
            this.tabNavItems = $('.tab-item', this.tabNav);

            this.tabContentItems = $('.tab-content-item', this.root);
            this.hidden = false;
            if(this.root.data('initial-hide-content') == 'Y') { this.hidden = true; }
            if(this.root.data('autoplay') && this.root.data('autoplay') > 0) {
                this.autoplaySpeed =  this.root.data('autoplay')
            }

            this.timer = null;



            this.makeActive = function (el) {
                var item = $(el);
                var contentItem = $(item.attr('href'));
                o.tabNavItems.removeClass('active');
                item.parent(o.tabNavItems).addClass('active');
                o.tabContentItems.not(contentItem).hide();
                contentItem.show();
            };

            this.changeSlides = function (el) {
                var item = $(el);
                var contentItem = $(item.attr('href'));
                // var delay = o.autoplaySpeed;
                var delay = 8000;
                var page = 0;
                var totalPages = o.tabNavItems.length;


                if(delay > 0) {
                    o.timer = setInterval(function tick() {
                        o.makeActive(o.tabNavItems.eq(page).find('a'));
                        if(page < totalPages - 1) {
                            page++;
                        } else {
                            page = 0;
                        }
                    }, delay);
                }
            }

            this.init = function () {

                var active = o.tabNavItems.filter('.active');
                if ( !active.length ) {
                    active = o.tabNavItems.first();
                }

                o.makeActive(active.children('a'));
                if(this.root.data('autoplay') > 0){
                    o.changeSlides(active.children('a'));
                }

                if(this.hidden) o.tabContentItems.hide();

                o.tabNavItems.find('a').click(function (e) {
                    e.preventDefault();
                    o.makeActive(this);
                    clearInterval(o.timer);
                    return false;
                });

            };

            this.init();
        }

        return SimpleTabs;
    });
}
