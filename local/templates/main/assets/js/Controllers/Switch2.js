if (typeof define === "function" && define.amd) {
  define(['router'], function () {
    function Switch2(container) {
      var _this = this;
      this.container = $(container);
      this.toggler = this.container.find('.switch1, .switch2, .switch3');
      this.control = this.container.find('a');

      $(window).load(function () {
        $("#contact-map").show();
      });
      this.control.bind('active', function (e) {
        e.stopPropagation();
        if (!$(this).is('.switch-active')) {
          var i = _this.control.index($(this)),
            id = $(this).attr('href');

          $(this).addClass('switch-active').data('active', true);
          _this.contentItems.hide();
          if (id == "#contact-map") {
            $("#contact-map").removeClass("map-hidden");
          } else {
            $(id).show();
          }

          var flex = $(id).find('.flexslider');
          flex.each(function () {
            if ($(this).data('flexslider')) {
              $(this).data('flexslider').resize();
            }
          });
          if (i == 0) {
            _this.container.removeClass('toggle');
            $("#contact-map").hide();
          } else {
            _this.container.addClass('toggle');
            $("#contact-map").show();
          }

          i = (i == 0) ? 1 : 0;
          _this.control.eq(i).removeClass('switch-active').data('active', false);

          if (_this.container.data('model') !== 'undefined') {
            var model = _this.container.data('model');
            var modelEl = _this.container.find('#' + model);
            var value = $(this).data('value');
            modelEl.val(value);
          }

          _this.container.trigger("switch2.change", [{
            control: $(this)
          }]);
          rHelperData(_this.control.eq(i));
        } else {
          _this.control.not('.switch-active').trigger('active');
        }
      });
      this.control.click(function (e) {
        var $this = $(this);
        if (!$this.is('.switch-active')) {
          $this.trigger('active');
          rHelperClick($this);
        }
        if (this.hasAttribute('for')) {
          $('#' + $this.attr('for')).click();
        }
        return false;
      });
      this.toggler.click(function () {
        _this.control.not('.switch-active').click();
      });

      if (!this.control.filter('.switch-active').length) {
        this.control.eq(0).addClass('switch-active');
      }
      var _this = this,
        contentItems = '',
        active = this.control.filter('.switch-active').eq(0);
      _this.active = jQuery();

      this.control.each(function (i) {
        var control = $(this),
          id = control.attr('href');
        if (i != 0) contentItems += ',';
        contentItems += id;
        if (control.data('active')) {
          active = control;
        }
        control.data({'active': false}).removeClass('switch-active');
      });
      $(contentItems).hide();
      _this.contentItems = $(contentItems);
      active.trigger('active');

    }

    return Switch2;
  });
}