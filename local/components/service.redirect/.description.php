<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Редирект на элемент",
	"DESCRIPTION" => "Редирект на элемент",
	"ICON" => "/images/news_line.gif",
	"SORT" => 130,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "service",
			"NAME" => "Услуги"
		)
	),
);

?>