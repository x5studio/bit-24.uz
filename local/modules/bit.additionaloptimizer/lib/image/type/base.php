<?

namespace Bit\AdditionalOptimizer\Image\Type;

class Base implements TypeInterface {

    protected $name = '{name}';

    protected $cache = [];
    protected $pathToImage;
    protected $additionalParams = [];

    public function __construct(string $pathToImage = '') {

        if($pathToImage) {

            $this->setImagePath($pathToImage);
        }

    }

    public function setImagePath(string $pathToImage) {

        $this->cache = [];

        $this->pathToImage = $pathToImage;

    }

    public function setAdditionalParams(array $additionalParams = []) {

        $this->additionalParams = $additionalParams;

    }

    public function getName() {

        if(!$this->cache['compiled_name']) $this->cache['compiled_name'] = $this->compileName();

        return $this->cache['compiled_name'];

    }

    public function getImageExist() {

        if(!$this->cache['exist']) $this->cache['exist'] = file_exists($this->getImagePath());

        return $this->cache['exist'];

    }

    public function getImagePath($includeName = true) {

        if(!$this->pathToImage) throw new \Exception("Original image not set");

        if(!$this->cache['original_pathinfo']) $this->cache['original_pathinfo'] = pathinfo($this->pathToImage);

        $path = $this->cache['original_pathinfo']['dirname'] . DIRECTORY_SEPARATOR;

        if($includeName) $path .= $this->getName();
        
        return $path;

    }



    public function createImage() {

        if(!$this->pathToImage) throw new \Exception("Original image not set");

        return true;

    }
    
    public function removeImage() {

        $result = unlink($this->getImagePath());

        $this->cache = [];

        return $result;

    }

    protected function compileName() {

        if(!$this->pathToImage) throw new \Exception("Original image not set");

        if(!$this->cache['original_pathinfo']) $this->cache['original_pathinfo'] = pathinfo($this->pathToImage);

        return str_replace('{name}', $this->cache['original_pathinfo']['basename'], $this->name);
        

    }

}