<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
//$this->setFrameMode(true);
if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

if ($_REQUEST["PAGEN_1"] && $_REQUEST["PAGEN_1"] > $arResult["NavPageCount"]) SetCurPage404();
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

ob_start();
?>

<div class="pager">
    <div class="width">
        <div class="pagination" data-pagecount="<?= $arResult["NavPageCount"] ?>" data-endpage="<?=$arResult["nEndPage"]?>" data-startpage="<?=$arResult["nStartPage"]?>" data-url-path="<?= $arResult["sUrlPath"] ?>">
            <ul>
                <? if ($arResult["bDescPageNumbering"] !== true): ?>

                    <? if ($arResult["NavPageNomer"] > 1): ?>

                        <? if ($arResult["bSavePage"]): ?>
                            <li>
                                <a class="page-prev"
                                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                            </li>
                            <li>
                                <a class="page-first"
                                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a>
                            </li>
                        <? else: ?>
                            <? if ($arResult["NavPageNomer"] > 2): ?>
                                <li>
                                    <a class="page-prev"
                                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                </li>
                            <? else: ?>
                                <li>
                                    <a class="page-prev"
                                       href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                </li>
                            <? endif ?>
                            <? if ($arResult["nStartPage"] != 1): ?>
                                <li>
                                    <a class="page-first"
                                       href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
                                </li>
                                <li><span>...</span></li>
                            <? endif; ?>
                        <? endif ?>

                    <? endif ?>

                    <? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>

                        <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                            <li class="active">
                                <span><?= $arResult["nStartPage"] ?></span>
                            </li>
                        <? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
                            <li>
                                <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
                            </li>
                        <? else: ?>
                            <li>
                                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
                            </li>
                        <? endif ?>
                        <? $arResult["nStartPage"]++ ?>
                    <? endwhile ?>

                    <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
                        <? if ($arResult["nEndPage"] != $arResult["NavPageCount"]): ?>
                            <li>
                                <span>...</span>
                            </li>
                            <li>
                                <a class="page-last"
                                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a>
                            </li>
                        <? endif; ?>
                        <li>
                            <a class="page-next"
                               href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                        </li>
                    <? endif ?>

                <? endif ?>

            </ul>
        </div>

        <?php
        $paging = ob_get_contents();
        $rHelper = RequestHelper::getInstance();

        $paging = $rHelper->getPagination($paging);
        ob_end_clean();
        echo $paging;
        ?>

        <? if ($arResult["NavPageCount"] != $arResult["NavPageNomer"]): ?>
            <div class="show-more zebra" data-endpage="<?= $arResult["NavPageCount"] ?>"
                 data-navnum="<?= $arResult["NavNum"] ?>" data-nav-record-count="<?= $arResult["NavRecordCount"] ?>"
                 data-show-count="<?= SHOW_COUNT !== "SHOW_COUNT" ? SHOW_COUNT : "true" ?>"
                 data-curpage="<?= $arResult["NavPageNomer"] ?>" data-nav-page-size="<?= $arResult["NavPageSize"] ?>">
                <a href="#" id="shmr-link"
                   class="dotted"><?= HTML_SHOW_AJAX !== "HTML_SHOW_AJAX" ? HTML_SHOW_AJAX : "�������� ���" ?></a>
                <img src="/images/loader17.gif" class="elem-loader hidden"/>
                <a href="#" id="hdmr-link"
                   class="dotted hidden"><?= HTML_HIDE_AJAX !== "HTML_HIDE_AJAX" ? HTML_HIDE_AJAX : "������ ��������" ?></a>
            </div>
        <? endif; ?>

        <button class="grey j-print"><img src="<?=$templateFolder?>/img/printer.png"> �����������</button>
    </div>
</div>