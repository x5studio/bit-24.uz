if (typeof define === "function" && define.amd) {
    define(['Controls/Form/FormMng',
        'Controls/Responsive',
        'module',
        'Controls/ModalManager',
        'Controls/Form/FormButtonData',
        'fancybox',
        'cookie',
        'router'], function (FormMng, obResponsive, module, obModalManager, obFormButtonData) {

        function FormPopupMng(p) {
            FormMng.apply(this, arguments);
            var o = this;
            this.selectorBtn = o.root.data('btn');
            if (!this.selectorBtn) {
                throw o.root.id + ": data-btn is empty"
            }

            this.btn = $(this.selectorBtn);
            this.btnClose = $('.j-close', o.root);
            this.buttonData = new obFormButtonData(o);
        }

        extend(FormPopupMng, FormMng);

        FormPopupMng.prototype.getGlobalName = function () {
            var o = this;
            return 'formGeneral' + o.saltForm
        };

        FormPopupMng.prototype.initOpenHandler = function () {
            var o = this;
            $(document).on("click", o.selectorBtn, function () {
                o.open(this);
                return false;
            });
        };

        FormPopupMng.prototype.init = function () {
            var o = this;
            FormPopupMng.superclass.init.apply(o);
            o.initMaskedInput();
            o.initValidate();
            o.initOpenHandler();
            o.trimEmail();


            $(document).on("click", o.btnClose.selector, function () {
                $('.error', o.root).each(function () {
                    $(this).removeClass('error').parent().addClass('error');
                });
            });
            o.root.on('active', function () {
                if (!o.root.data('open')) {
                    o.btn.click();
                }
            });
            if (!o.root.data('open') && o.root.data('active')) {
                o.btn.click();
            }


            o.root.data('initFormPopupMng', true);
        };

        /**
         btn - селектор кнопки, которую нажали для вызова попап
         */
        FormPopupMng.prototype.open = function (e_btn) {
            var o = this;
            o.locked = (o.btn.attr('data-locked-mode') !== "Y") ? false : true;
            o.autosize = function(){
                var win = $(window);
                if(o.locked && win.width() < 500) {
                    return false;
                }
                return true;
            }
            obModalManager.open();
            if ($.inArray(obResponsive.getCurType(), ['xsmall', 'small']) >= 0) {
                var topRatio = 0;
            } else {
                topRatio = 0.5;
            }

            var btnOffset = o.btn.offset();
            if(btnOffset){
              if(o.root.find("input[name='OFFSET']").length){
                o.root.find("input[name='OFFSET']").val(Math.ceil(btnOffset.top));
              }else{
                o.root.find('form').append('<input type="hidden" name="OFFSET" value="' + Math.ceil(btnOffset.top) + '" />');
              }
            }

          var fancyParams={
                padding: 0,
                margin: 0,
                autoSize: o.autosize(),
                closeBtn: false,
                topRatio: topRatio,
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    overlay: {
                        locked: o.locked
                    }},
                afterLoad:  function () {o.onAfterLoad(e_btn)},
                afterClose:  function () {o.afterClose(e_btn)}
            };



            $.fancybox.open( o.root, fancyParams);

            if(o.root.hasClass('hidden')) {
                o.root.removeClass('hidden');
            }


        };

        FormPopupMng.prototype.onAfterLoad = function (e_btn) {
            var o = this;
            o.Form.trigger('fancy');
            o.root.data('open', true);
            o.root.data('active', true);
            // add custom close button
            o.btnClose.click(function () {
                $.fancybox.close();
            });

            // add j-required for input
            $('div.j-required', o.root).each(function () {
                $(this).removeClass('j-required');
                $('input', this).addClass('j-required');
            });

            // emulate path for analytics target
            var path = location.pathname.replace("index.php", "") + this.root.attr("id");
            var pathSuffix = module.config().formOpenPathSuffix || "/virtual/open-form/";
            trackStep(path + pathSuffix);
            setCookie("trackStep", path); // for add result ok

            o.buttonData.prepareForm(o.root, e_btn);

            o.afterLoad({targetBtn: e_btn});
            o.setFormTitleInField(e_btn);

            o.root.trigger('afterFormLoad', [{'button': e_btn}]);
        };

        FormPopupMng.prototype.setFormTitleInField = function(btn){
            var o = this;
            if(o.root.find('input[data-code="FORM_TITLE"]').length > 0) {

                var formTitle = '';
                if ($(btn).data('form-title') != undefined && $(btn).data('form-title').length > 0) {
                    formTitle = $(btn).data('form-title');
                }
                if (formTitle == '') {
                    var selectorTitle = '.popup-title .title-text';
                    if ($(btn).data('selector-title') != undefined && $(btn).data('selector-title').length > 0) {
                        selectorTitle = $(btn).data('selector-title');
                    }
                    if (o.root.find($(selectorTitle)).length > 0) {
                        formTitle = o.root.find($(selectorTitle)).text();
                    }
                }
                o.root.find('input[data-code="FORM_TITLE"]').val(formTitle);
            }
        }

        FormPopupMng.prototype.afterClose = function () {
            var o = this;
            o.root.data('open', false);
            o.root.data('active', false);
            rHelperClick(o.root, 'id');
            obModalManager.close();
        }
        return FormPopupMng;
    });
}
