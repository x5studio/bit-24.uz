<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);

$PARENT = $this->__component->__parent; ?>

<?php if (empty($arResult['ITEMS'])) 
{ ?>
	<div class="container">
		<div class="empty-events">В данном городе нет мероприятия</div>
	</div>
	<?
	return; 
} ?>

<?php
for ($i = 0, $count = count($arResult['ITEMS']); $i < $count; $i++):
    $arItem = $arResult['ITEMS'][$i];
    ?>
    <?php if ($i % 3 === 0): ?>
    <div class="events-list__items">
        <div class="container">
    <?php endif; ?>
    <div class="events-list__item">
        <div class="events-list__item-image">
            <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>"><img src="<?php echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt=""></a>
        </div>
        <div class="events-list__item-title">
            <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>"><?php echo $arItem['NAME']; ?></a>
        </div>
        <div class="events-list__item-type"><?php echo $arItem['PROPERTIES']['TYPE']['VALUE']; ?></div>
        <div class="events-list__item-date"><?php echo $arItem['DATE']; ?></div>
        <div class="events-list__item-description">
            <?php echo $arItem['PREVIEW_TEXT']; ?>
        </div>
        <div class="events-list__item-actions">
    <?if(MakeTimeStamp($arItem['DATE_ACTIVE_TO']) >= time()):?><button type="button" class="events-list__item-actions-btn events-list__btn events-list__btn_pink event__feedback-modal-btn" data-form-field-title="<?php echo $arItem['NAME']; ?>" data-form-field-to="<?php echo $arItem['PROPERTIES']['EMAIL']['VALUE']; ?>">Записаться</button><?endif;?>
            <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="events-list__item-actions-readmore-btn events-list__item-actions-btn">Подробнее</a>
        </div>
    </div>
    <?php if (($i + 1) % 3 === 0 || ($i == $count - 1 && ($i + 1) % 3 !== 0)): ?>
        </div>
    </div>
    <?php endif; ?>
    <?php if ($count <= 3 && $i == $count - 1 && $arResult["NAV_STRING"] && $arParams['DISPLAY_BOTTOM_PAGER'] == 'Y'): ?>
    <div class="events-list__pagination">
        <div class="container">
            <?php echo $arResult["NAV_STRING"]; ?>
        </div>
    </div>
<?php endif; ?>
    <?php if (!empty($arResult['ITEMS']) && $arParams['DISPLAY_SUBSCRIBE_FORM'] == 'Y' && (($count > 3 && $i == 2) || ($count <= 3 && $i == $count - 1))): ?>
    <div class="events-list__subscribe">
        <div class="container">
            <div class="events-list__subscribe-title">Подпишитесь на уведомления <br/>о новых мероприятиях</div>
            <div class="events-list__subscribe-form">
                <div class="events-list__subscribe-form-title">Актуальные мероприятия в вашем городе</div>
                <div id="WIDGET_SUBSCRIBE_INLINE_FORM"></div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php endfor; ?>

<?php if ($count > 3 && $arResult["NAV_STRING"] && $arParams['DISPLAY_BOTTOM_PAGER'] == 'Y'): ?>
    <div class="events-list__pagination">
        <div class="container">
            <?php echo $arResult["NAV_STRING"]; ?>
        </div>
    </div>
<?php endif; ?>

<?php if (empty($arParams['HIDE_APPLY_FORM']) || $arParams['HIDE_APPLY_FORM'] != 'Y'): ?>
    <div id="WIDGET_APPLY_FORM"></div>
<?php endif; ?>
