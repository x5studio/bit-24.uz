if (typeof define === "function" && define.amd) {
    define(['tools/transition-scroll-to'], function () {

        function NavTabsAnchor(container) {
            this.containerTabs = $(container);
            this.init();
        }

        NavTabsAnchor.prototype.scrollToTop = function (e) {
            var $target = $(e.target);
            transitionScrollTo({
                y: $target.offset().top + 1
            });
        };

        NavTabsAnchor.prototype.toggleActiveTab = function () {
            var $this = $(this);
            if ($this.parent('li').hasClass('active')) {
                $(document).one('click.bs.tab.data-api', '[data-toggle="tab"]', function () {
                    $this.parent('li').removeClass('active');
                    window.location = $this.attr("href");
                });
            }
        };
        NavTabsAnchor.prototype.init = function () {
            var o = this;
            if (window.location.href.indexOf("#") > 0) {
                var hashPrice = window.location.hash;
                o.containerTabs.find('a[href="' + hashPrice + '"]').tab('show');
            }
            $(document).on('shown.bs.tab', function () {
                o.scrollToTop.apply(this, arguments);
                if (arguments.length > 0) {
                    window.location = arguments[0].target.hash;
                }
            });
            $(document).on('mouseup.bs.tab.data-api', '[data-toggle="tab"]', function () {
                o.toggleActiveTab.apply(this, arguments);
            });
        };

        return NavTabsAnchor;
    });
}