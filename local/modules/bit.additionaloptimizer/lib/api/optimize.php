<?
namespace Bit\AdditionalOptimizer\Api;

use \Bitrix\Main\Engine\Controller;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Localization\Loc;
use Bit\AdditionalOptimizer\Image\ImageElement;
use Bit\AdditionalOptimizer\Supports;

class Optimize extends Controller
{

    public function manualAction($path = '', $limit = 5, $offcet = 0){

        $result = 0;

        $resultFiles = [];

        $options = Option::getForModule('bit.additionaloptimizer');

        

        if($path && file_exists($_SERVER['DOCUMENT_ROOT'].$path) && strpos($path, '/bitrix/') === false){

            $path = $_SERVER['DOCUMENT_ROOT'].$path;

            $files = $this->getDirFiles($path, $offcet, $limit);

            foreach ($files as $key => $file) {

				$additionalParams = [];

				$imageElement = new ImageElement($file, ($options['image_optimize_on'] == 'Y'), ($options['image_webp_on'] == 'Y' && Supports::checkWebP()), ($options['image_lazyload_on'] == 'Y'), $additionalParams);

                if($_SERVER['DOCUMENT_ROOT'].$imageElement->getActualImagePath() != $file) {

                    $result ++ ;
                    $resultFiles[] = $imageElement->getActualImagePath();


                }
                
            }

        }

        return ['optimized' => $result, 'resultFiles' => $resultFiles];

    }

    public function getDirFiles($path, $offcet = 0, $limit = 5){

        $files = [];

        if(file_exists($path)){

            $rii = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path));
            $Regex = new \RegexIterator($rii, '/^.+\.(jpg|png)$/i', \RecursiveRegexIterator::GET_MATCH);
        
            foreach(new \LimitIterator($Regex, $offcet, $limit) as $file) {
        
                $files[] = $file[0];
        
            }
        }

        return $files;

    }

}
