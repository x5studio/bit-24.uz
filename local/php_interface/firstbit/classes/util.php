<?php

namespace FirstBit;

class Util
{
    static function ReturnIncludeFile($path)
    {
        ob_start();
        include $path;
        return ob_get_clean();
    }

    static function getArrEnumValueByXmlID($xmlID, $code, $iblockID)
    {
        \CModule::IncludeModule('iblock');
        $obResult = \CIBlockPropertyEnum::GetList([], ['XML_ID' => $xmlID, 'CODE' => $code, 'IBLOCK_ID' => $iblockID]);
        return $obResult->GetNext();
    }

    static function getArrEnumValueUserFieldByXmlID($xmlID, $propertyID)
    {
        $obResult = \CUserFieldEnum::GetList([], ['USER_FIELD_ID' => $propertyID,  'XML_ID' => $xmlID]);
        return $obResult->GetNext();
    }

    static function addToHeadCanonical()
    {
        global $APPLICATION;
        $APPLICATION->AddHeadString(
            '<link rel="canonical" href="https://' .
            $_SERVER['HTTP_HOST'] .  $APPLICATION->GetCurPage('', array_keys($_GET)) .
            '">');
    }

    public static function isAppendActionPage($url='')
    {
        global $APPLICATION;

        if (!$url) {
            $url = $APPLICATION->GetCurPage();
        }

        $appendActionPages = [
            '/blog/aktsii/novogodnie-skidki-na-bitrix24/',
        ];

        if (in_array($url, $appendActionPages)) {
            return true;
        }

        return false;
    }

}
