<?
\Bitrix\Main\Loader::includeModule('form');

// Получаем ID необходимой веб-формы.
$WEB_FORM_QUESTION_ID = 0;
$FORM_SID = 'QUESTION';

$rsForm = \CForm::GetBySID($FORM_SID);
if($arForm = $rsForm->fetch()) {
    $WEB_FORM_QUESTION_ID = intval($arForm['ID']);
}
?>

<? $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "question", Array(
    "WEB_FORM_ID" => $WEB_FORM_QUESTION_ID,  // ID веб-формы
    "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
    "COMPANY_IBLOCK_ID" => IB_CONTACTS,
    "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
    "EMAIL_QUESTION_CODE" => "EMAIL",
    "CITY_QUESTION_CODE" => "CITY",
    "OFFICE_QUESTION_CODE" => "OFFICE",
    "TO_QUESTION_RESIPIENTS" => "",
    "EMAIL_ONLY_FROM_RESIPIENTS" => "",
    "PRODUCT_QUESTION_CODE" => "PRODUCT",
    "TO_QUESTION_CODE" => "TO",
    "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
    "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
    "SEF_MODE" => "N",  // Включить поддержку ЧПУ
    "CACHE_TYPE" => "N",  // Тип кеширования
    "CACHE_TIME" => "3600",  // Время кеширования (сек.)
    "LIST_URL" => "",  // Страница со списком результатов
    "EDIT_URL" => "",  // Страница редактирования результата
    "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
    "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
    "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
    "VARIABLE_ALIASES" => array(
        "WEB_FORM_ID" => "WEB_FORM_ID",
        "RESULT_ID" => "RESULT_ID",
    ),
    "ALERT_ADD_SHARE" => "N",
    "HIDE_PRIVACY_POLICE" => "Y",
    "BTN_CALL_FORM" => ".btn_order",
    "HIDE_FIELDS" => array(
        0 => "CITY",
        1 => "OFFICE",
    ),
    "COMPONENT_MARKER" => "question",
    "SHOW_FORM_DESCRIPTION" => "N",
    "MESS" => array(
        "THANK_YOU" => "Спасибо, Ваша заявка принята.",
        "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
        "TITLE_FORM" => "Остались вопросы?",
    )
),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
); ?>