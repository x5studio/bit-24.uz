<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "CRM Битрикс24 для автоматизации бизнеса, работы отдела продаж и улучшения качества коммуникации с клиентами.");
$APPLICATION->SetTitle("CRM Битрикс24 - лицензии и цены, преимущества для бизнеса");
$APPLICATION->SetAdditionalCSS(MAIN_TEMPLATE_PATH . "/assets_new/css/landing_b24_style.css");

$APPLICATION->AddHeadString('
<!-- Marquiz script start --> <script src="https://script.marquiz.ru/v1.js" type="application/javascript"></script>  <!-- Marquiz script end -->
');
?>


<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/banner.php')?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/opportunities.php')?>

    <main class="new-design">
        <?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/advantages.php')?>
        <?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/price.php')?>
        <?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/services.php')?>
    </main>

<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/clients.php')?>
<?include ($_SERVER['DOCUMENT_ROOT'].SITE_TEMPLATE_PATH.'/include/parts/form_question.php')?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>