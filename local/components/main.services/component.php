<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
$arParams["COUNT"] = 3;

if ($this->StartResultCache())
{
	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFile = array();

	$rsElement = \CIBlockElement::GetList(
		array("SORT" => "ASC"),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y"
		),
		false,
		array("nTopCount" => $arParams["COUNT"]),
		array("ID", "NAME", "CODE", "PREVIEW_TEXT", "PREVIEW_PICTURE", "DETAIL_PICTURE"));
	while ($arElement = $rsElement->GetNext())
	{
		$arFile[] = $arElement["DETAIL_PICTURE"];
		$arFile[] = $arElement["PREVIEW_PICTURE"];
		$arResult["ITEMS"][] = $arElement;
	}

	$arResult["IMAGES"] = Helper::getImages($arFile);

	$this->IncludeComponentTemplate();
}