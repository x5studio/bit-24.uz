<?php
if(!empty($arResult['PREVIEW_PICTURE'])) {
    $arResult['PREVIEW_PICTURE'] = CFile::ResizeImageGet(
        $arResult['PREVIEW_PICTURE'],
        array("width" => 295, "height" => 335),
        BX_RESIZE_IMAGE_EXACT
    );
}
