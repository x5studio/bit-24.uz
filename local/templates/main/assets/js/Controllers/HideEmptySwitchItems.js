if (typeof define === "function" && define.amd) {
    define([], function () {
        
        function HideEmptySwitchItems(container) {
            var o = this;
            this.container = $(container);
            this.cItems = $("a", this.container);

            this.init = function () {
                o.cItems.each(function () {
                    var containerSelector = this.getAttribute("href");
                    if (typeof containerSelector != "string" || containerSelector[0] != '#') {
                        return;
                    }
                    var containerItem = $(containerSelector);
                    if (
                        containerItem.text().indexOf('Пустой результат') + 1 ||
                        $.trim($(containerSelector+' > *').not('.section-title').text()).length == 0 ||
                        containerItem.children().length <= 1 || containerItem.find('.zero').length > 0
                    ) {
                        $(this).hide();
                    } else {
                    	if($(this).is(":hidden")) {
                    		$(this).show();
                    	}
                    }
                });
            };

            this.init();
        }

        return HideEmptySwitchItems;
    });
}