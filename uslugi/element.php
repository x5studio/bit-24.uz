<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="container">
	<div class="row row-offcanvas row-offcanvas-left">
		<div class="col-xs-12 col-sm-6 col-md-3 sidebar-offcanvas" id="sidebar">
			<div class="side-menu">
				<nav class="navbar navbar-default" role="navigation">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<div class="brand-wrapper hidden-lg hidden-md">
							<p class="h2">Фильтр услуг </p>
							<!-- Hamburger -->
							<button type="button" class="btn btn-close hidden-lg hidden-md" data-toggle="offcanvas" data-target="#sidebar">
								<i class="fa fa-times" aria-hidden="true"></i>
							</button>
						</div>
					</div>
					<!-- Left Menu -->
					<div class="side-menu-container">
						<?$APPLICATION->IncludeComponent("bitrix:menu", "service.sidebar", Array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "N",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "serviceinner",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						),
							false
						);?>
					</div><!-- /.navbar-collapse -->
				</nav>
			</div>
		</div>
		<!-- Main-content -->
		<div class="hidden-lg hidden-md col-sm-6 col-xs-12 filter">
			<button type="button" class="nav-toggle" data-toggle="offcanvas" data-target="#sidebar"><a href="#" title="link">Услуги</a><i class="fa fa-filter" aria-hidden="true"></i></button>
		</div>
		<!-- не работает поиск -->
		<!-- <div class="hidden-lg hidden-md col-sm-6 hidden-xs">
			<form action="#">
				<div class="input-group search">
					<input type="text" class="form-control" placeholder="Поиск">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
				</div>
			</form>
		</div> -->
		<div class="col-md-9 col-sm-12 col-xs-12">
			<div class="main-service">
				<div class="service1 pull-right">
					<?$APPLICATION->IncludeComponent(
						"service.detail",
						"",
						Array(
							"CACHE_TIME" => "3600",
							"CACHE_TYPE" => "A",
							"ELEMENT_CODE" => $arParams["ELEMENT_CODE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"SECTION_CODE" => $arParams["SECTION_CODE"]
						)
					);?>
				</div>
			</div>
		</div>
	</div>
</div>
<?
global $APPLICATION;

if (strpos($APPLICATION->GetCurDir(), '/uslugi/konsultacii/') !== false) {
	$WEB_FORM_ID = 10;
} elseif (strpos($APPLICATION->GetCurDir(), '/uslugi/konsalting-po-bitrix24/') !== false) {
	$WEB_FORM_ID = 10;
} elseif (strpos($APPLICATION->GetCurDir(), '/uslugi/reglamenty-i-instrukcii/') !== false) {
	$WEB_FORM_ID = 11;
} elseif (strpos($APPLICATION->GetCurDir(), '/uslugi/obuchenie-bitrix24/') !== false) {
	$WEB_FORM_ID = 12;
} else {
	$WEB_FORM_ID = 9;
}

$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
	"WEB_FORM_ID" => $WEB_FORM_ID,	// ID веб-формы
	"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
	"COMPANY_IBLOCK_ID" => IB_CONTACTS,
	"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
	"EMAIL_QUESTION_CODE" => "EMAIL",
	"CITY_QUESTION_CODE" => "CITY",
	"OFFICE_QUESTION_CODE" => "OFFICE",
	"TO_QUESTION_RESIPIENTS" => "",
	"EMAIL_ONLY_FROM_RESIPIENTS" => "",
	"PRODUCT_QUESTION_CODE" => "PRODUCT",
	"TO_QUESTION_CODE" => "TO",
	"IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
	"USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
	"SEF_MODE" => "N",	// Включить поддержку ЧПУ
	"CACHE_TYPE" => "A",	// Тип кеширования
	"CACHE_TIME" => "3600",	// Время кеширования (сек.)
	"LIST_URL" => "",	// Страница со списком результатов
	"EDIT_URL" => "",	// Страница редактирования результата
	"SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
	"CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
	"CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
	"VARIABLE_ALIASES" => array(
		"WEB_FORM_ID" => "WEB_FORM_ID",
		"RESULT_ID" => "RESULT_ID",
	),
	"ALERT_ADD_SHARE" => "N",
	"HIDE_PRIVACY_POLICE" => "Y",
	"BTN_CALL_FORM" => ".btn-buy",
	"HIDE_FIELDS" => array(
		0 => "CITY",
		1 => "OFFICE",
		2 => "TYPE",
	),
	"COMPONENT_MARKER" => "feedform",
	"SHOW_FORM_DESCRIPTION" => "N",
	"MESS" => array(
		"THANK_YOU" => "Спасибо, Ваша заявка принята.",
		"WAIT_CALL" => "Мы свяжемся с Вами в течение 2-х часов.",
		"TITLE_FORM" => "Заказать услугу2",
	)
),
	false,
	array(
		"HIDE_ICONS" => "Y"
	)
);
?>