<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$call_phone = ($_SESSION["REGION"]["CODE"]=="chelyabinsk")?"call_phone_1":"";		
?>
<div class="top-phone <?=$call_phone?> <?if(!empty($arParams["HTML_PHONE_VIEW"])){ echo $arParams["HTML_PHONE_VIEW"];}?>" data-name='<?=$arResult["ITEMS"][0]['NAME']?>'>
    <?=$arResult["ITEMS"][0]['PROPERTIES']['phone']['VALUE'];?>
</div>