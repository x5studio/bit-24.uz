if (typeof define === "function" && define.amd) {
    define([], function () {
        function ReCaptcha(container) {
        	this.container = $(container);
        	this.gRecaptcha = this.container.find(".g-recaptcha");
            this.siteKey = this.gRecaptcha.attr('data-sitekey');
			      this.gRecaptchaVer = this.gRecaptcha.attr('data-recaptcha-version');
			      this.saltValue = this.container.closest("[data-salt-form]").attr("data-salt-form");
            if(this.siteKey != '') {
        		this.init();
            }
        }

        ReCaptcha.prototype.init = function () {
            var o = this;
            if(o.gRecaptcha.length > 0) o.gRecaptcha = o.gRecaptcha[0];

			if (typeof o.gRecaptchaVer !== typeof undefined && o.gRecaptchaVer !== false && o.gRecaptchaVer == "v3") {
			     grecaptcha.ready(function() {
					return grecaptcha.execute(o.siteKey, {action: o.saltValue}).then(function(token) {
					   return $("<input>").attr({
							name: "token",
							id: "token",
							type: "hidden",
							value: token
						}).appendTo(o.container);

					   onSubmitAfterCaptchaValidate();
					});
				});
			}else{
			  var widgetIdRecaptcha = grecaptcha.render(o.gRecaptcha,{'sitekey' : o.siteKey, 'callback' : onSubmitAfterCaptchaValidate, 'size' : 'invisible'});

			  o.container.data('recaptcha-widget-id',widgetIdRecaptcha);

			  grecaptcha.reset(widgetIdRecaptcha);
			  grecaptcha.execute(widgetIdRecaptcha);
			}

            o.container.parents('form').addClass('validate-recaptcha');
        };

        window.onSubmitAfterCaptchaValidate = function () {
            var FormCheck = $(document).find(".validate-recaptcha");

            $(document).find(".validate-recaptcha").removeClass('validate-recaptcha');
            if(FormCheck.length > 0) {
                FormCheck[0].submit();
            }
        };

        return ReCaptcha;
    });
}
