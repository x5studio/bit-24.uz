<?
define("CONTACT_ALL_CITY", "Y");
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");



$APPLICATION->IncludeComponent(
    'pb.main:jivosite.webhook',
    '',
    array(
        'WEB_FORM' => 'CONSULTANT_LEAD',
        'OFFICE_FILTER' => new \PB\Contacts\ContactManagerFilter(),
        	/**
		 * https://developers.google.com/analytics/devguides/collection/protocol/v1/
		 * https://ga-dev-tools.appspot.com/hit-builder/
		 */
		'DATA_GA_EVENT' => array(
			'v' => 1,  // API Version.
			'tid' =>'UA-118715209-1',  // tracking ID / Property ID.
			'dh' => \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getHttpHost(),
			'cid' => '555', // set in method sendGAEvent()
			't' => 'event',  // event hit type.
			'ec' => 'Формы',  // event category.
			'ea' => 'Успешно отправлены',  // event action.
			'el' => 'Прелиды из чата Jivosite',  // event label.
			'ev' => 1,  // event value, must be an integer
		),
        // 'TEST' => 'Y',
        // 'TEST_NAME' => 'CHAT_FINISHED_WORK'
    )
);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");