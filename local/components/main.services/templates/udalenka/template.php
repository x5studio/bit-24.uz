<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult["ITEMS"])): ?>
	<section class="benefits">
		<div class="container-fluid">
			<div class="row-fluid">
                <?$i = 1;?>
				<? foreach ($arResult["ITEMS"] as $arItem): ?>
					<div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 text-center">
						<figure class="effect-julia">
							<img src="<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>" alt="<?= $arItem["NAME"] ?>"
									 class="hidden-xs"/>
							<figcaption>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive fig_icon
								center-block" alt="<?= $arItem["NAME"] ?>">
								<div class="service_height60"><h3><a href="<?= $arItem["CODE"] ?>"><?= $arItem["NAME"] ?></a></h3></div>
								<p class="hidden-xs"><?= $arItem["PREVIEW_TEXT"] ?></p>
                                <?/*if ($i == 4):?>
								    <button type="button" class="btn btn-more hidden-xs" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Подробнее</button>
                                <?else:*/?>
                                    <a class="btn btn-more hidden-xs" href="<?= $arItem["CODE"] ?>">Подробнее</a>
                                <?//endif;?>
							</figcaption>
						</figure>
					</div>
                <?$i++;?>
				<? endforeach; ?>

				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h2 class="fields-heading">Какой тип внедрения подойдет моей компании?</h2>
					<p class="text-center">
						<a href="#" class="btn top-button btn_order" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Получить консультацию</a>
					</p>
				</div>

			</div>
		</div>
	</section>
<? endif; ?>