if (typeof define === "function" && define.amd) {
    define(['router'], function () {
        function Unloader(options) {
            this.options = $.extend(true, {}, Unloader.defaultOptions, options);
            this.init();
        }

        Unloader.defaultOptions = {
            message: "Вы уверены, что хотите покинуть данную страницу?"
        };

        Unloader.prototype.unload = function (e) {
            var o = e.data.owner;
            if (typeof e == "undefined") {
                e = window.event;
            }
            if (e) {
                e.returnValue = o.options.message;
            }
            return o.options.message;
        };

        Unloader.prototype.resetUnload = function (e) {
            var o = e.data.owner;

            $(window).off('beforeunload', o.unload);

            setTimeout(function () {
                $(window).on('beforeunload', {owner: o}, o.unload);
            }, 1000);
        };

        Unloader.prototype.init = function () {
            var o = this;
            $(window).on('beforeunload', {owner: o}, o.unload);

            $('a').on('click', {owner: o}, o.resetUnload);

            $(document).on('click', 'input[type="submit"], button[type="submit"]', {owner: o}, o.resetUnload);
            $(document).on('submit', 'form', {owner: o}, o.resetUnload);

            window.BX.addCustomEvent("onAjaxSuccess", function () {
                $(window).off('beforeunload', o.unload);
            });

        };
        return Unloader;
    });
}