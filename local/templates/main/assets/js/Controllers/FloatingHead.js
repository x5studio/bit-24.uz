/**
 * � ������� �� FloatingBox �������� ������ ����������� ����������
 * (���������� ����� ���� ���� ������� �����)
 * */
if (typeof define === "function" && define.amd) {
    define(['jquery'], function ($) {
        function FloatingHead(container) {
            this.floatingHead = $('.fixedhead', container);
            this.container = container;
            this.initialOffsetTop = this.floatingHead.offset().top;
            this.document = $(document);
            this.scrollTop = this.document.scrollTop();

            this.init();
        };
        FloatingHead.prototype.isFloating = function () {
            return this.document.scrollTop() > this.initialOffsetTop && this.document.scrollTop() < this.initialOffsetTop + this.container.height() - this.floatingHead.height();
        };
        FloatingHead.prototype.toggleFloating = function()
        {
            if(this.isFloating()){
                if(!this.floatingHead.hasClass('fixedhead-active'))
                {
                    this.floatingHead.addClass('fixedhead-active');
                    this.floatingHead.trigger('floatShow', ['show']);
                }
            }else{
                if(this.floatingHead.hasClass('fixedhead-active'))
                {
                    this.floatingHead.removeClass('fixedhead-active');
                    this.floatingHead.trigger('floatHide', ['hide']);
                }
            }
        };

        FloatingHead.prototype.init = function()
        {
            this.toggleFloating();
            $(window).on('scroll', BX.proxy(this.toggleFloating, this));
        };

        return FloatingHead;
    });
}