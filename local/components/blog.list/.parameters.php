<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок",
			"TYPE" => "STRING",
			"DEFAULT" => 2,
		),
		"SECTION_CODE" => Array(
			"PARENT" => "BASE",
			"NAME" => "CODE раздела",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"TAG_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "ID тэга",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"COUNT" => Array(
			"PARENT" => "BASE",
			"NAME" => "Количество элементов на странице",
			"TYPE" => "STRING",
			"DEFAULT" => 16,
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);
CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("T_IBLOCK_DESC_PAGER_NEWS"), true, true);
?>
