/**
 * Created by psbezrukov on 28.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/Contact/ContactManager'], function ($, ContactManager) {

        function ContactManagerFilter(container, options) {
            this.container = $(container);
            this.options = $.extend({}, ContactManagerFilter.defaultOptions, options);

            this.dfSetGeneral = $.Deferred();
            this.contMng = null;
            this.obGeneralFilter = {};
        }

        ContactManagerFilter.defaultOptions = {};

        ContactManagerFilter.prototype.getContactManager = function () {
            return this.contMng;
        };

        ContactManagerFilter.prototype.setContactManager = function (contactManager) {
            this.contMng = contactManager;
        };


        ContactManagerFilter.prototype.doneSetGeneral = function (fn) {
            var o = this;
            if (typeof fn != 'function') {
                throw new TypeError();
            }
            o.dfSetGeneral.done(fn);
            return o.dfSetGeneral.promise();
        };

        ContactManagerFilter.prototype.setGeneral = function (key, value) {
            var o = this;
            if (typeof value == 'undefined') {
                value = null;
            }
            if (typeof key == 'object') {
                $.extend(o.obGeneralFilter, key)
            }
            else {
                o.obGeneralFilter[key] = value;
            }
            if (o.dfSetGeneral.state() == 'pending') {
                o.dfSetGeneral.resolve(o.obGeneralFilter);
            }
        };

        ContactManagerFilter.prototype.getGeneral = function (key) {
            var o = this;
            if (typeof key == 'undefined') {
                return o.obGeneralFilter;
            } else {
                return o.obGeneralFilter[key];
            }
        };

        ContactManagerFilter.prototype.deleteGeneral = function (key) {
            var o = this;
            if (typeof key == 'undefined') {
                o.obGeneralFilter = {};
                return true;
            } else {
                if (o.obGeneralFilter.hasOwnProperty(key)) {
                    delete o.obGeneralFilter[key];
                    return true;
                }
            }
            return false;
        };

        ContactManagerFilter.prototype.dataFilter = function ($arData, obFilter) {
            var o = this;
            if (typeof obFilter == 'undefined') {
                obFilter = {};
            }
            obFilter = $.extend({}, o.getGeneral(), obFilter);

            if (obFilter) {
                var $arFilteredData = [];
                for (var $keyFieldData in $arData) {
                    if (!$arData.hasOwnProperty($keyFieldData)) {
                        continue;
                    }
                    var $fieldData = $arData[$keyFieldData];
                    var $bItemFiltered = false;
                    for (var $keyFieldFilter in obFilter) {
                        if (!obFilter.hasOwnProperty($keyFieldFilter)) {
                            continue;
                        }
                        var $bFiltered = false;
                        var $fieldFilter = obFilter[$keyFieldFilter];
                        var $negativeFilter = false;
                        var isNegativeFilter = $keyFieldFilter.match(/^!(.*)/i);
                        if (isNegativeFilter) {
                            $negativeFilter = true;
                            $keyFieldFilter = isNegativeFilter[1];
                        }
                        var isPropertyFieldFilter = $keyFieldFilter.match(/^PROPERTY_(.+)/i);
                        switch ($keyFieldFilter) {
                            case 'IBLOCK_SECTION_ID':
                            case 'PROPERTY_HIDE_IN_CITY_LIST':
                                if ($fieldData['TYPE'] == ContactManager.TYPE_DIVISION) {
                                    var $arFieldOfficeData = o.contMng.getRealOfficeByID($fieldData['ID']);
                                    if ($arFieldOfficeData['TYPE'] == ContactManager.TYPE_DIVISION) {
                                        continue;
                                    }
                                } else if ($fieldData['TYPE'] == ContactManager.TYPE_OFFICE) {
                                    $arFieldOfficeData = $fieldData;
                                }
                                break;
                            default:
                                $arFieldOfficeData = $fieldData;
                        }

                        if ($keyFieldFilter == 'IBLOCK_SECTION_ID') {
                            if ($.inArray($fieldFilter, $arFieldOfficeData['ACTIVE_ADDITIONAL_GROUPS']) < 0) {
                                $bFiltered = true;
                            }
                        }
                        else if ($keyFieldFilter == 'PROPERTY_HIDE_IN_CITY_LIST') {
                            if ($fieldData['TYPE'] == ContactManager.TYPE_OFFICE && $arFieldOfficeData['IS_PAGE_OFFICE'] != 'Y') {
                                if ($arFieldOfficeData['PROPERTIES']['HIDE_IN_CITY_LIST']['VALUE'] != $fieldFilter) {
                                    $bFiltered = true;
                                }
                            }
                        }
                        else if (isPropertyFieldFilter) {
                            if ($arFieldOfficeData['PROPERTIES'][isPropertyFieldFilter[1]]['VALUE'] != $fieldFilter) {
                                $bFiltered = true;
                            }
                        }
                        else if ($fieldFilter instanceof Array && $fieldFilter.length > 0) {
                            if ($.inArray($arFieldOfficeData[$keyFieldFilter], $fieldFilter) < 0) {
                                $bFiltered = true;
                            }
                        }
                        else {
                            if ($arFieldOfficeData[$keyFieldFilter] != $fieldFilter) {
                                $bFiltered = true;
                            }
                        }
                        if ($negativeFilter) {
                            $bFiltered = !$bFiltered;
                        }
                        if (!$bItemFiltered) {
                            $bItemFiltered = $bFiltered;
                        }
                    }
                    if (!$bItemFiltered) {
                        $arFilteredData[$keyFieldData] = $fieldData;
                    }
                }
            }
            else {
                $arFilteredData = $arData;
            }

            return $arFilteredData;
        };

        return ContactManagerFilter;
    });
}