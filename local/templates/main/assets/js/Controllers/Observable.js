/**
 * Created by ����� on 28.09.2015.
 */
if (typeof define === "function" && define.amd) {
    define([], function () {

        function Observable() {
            this.listeners = {}
        }

        Observable.prototype.addListener = function (event, callBack) {
            if (!this.listeners[event])
                this.listeners[event] = [];
            this.listeners[event].push(callBack);
        };

        Observable.prototype.removeListener = function (event, callBack) {
            if (this.listeners[event]) {
                for (var i in this.listeners[event]) {
                    if (this.listeners[event][i] === callBack)
                        this.listeners[event][i].splice(i, 1);
                }
            }
        };

        Observable.prototype.triggerEvent = function (event, args) {
            if (this.listeners[event]) {
                for (var i in this.listeners[event]) {
                    this.listeners[event][i].apply(this, args);
                }
            }
        };

        return Observable;
    });
}