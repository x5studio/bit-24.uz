<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arResult["ITEMS"] = array_values($arResult["ITEMS"]);


if($arResult["ITEMS"][0]['ID']) {
    $res = CIBlockElement::GetProperty($arResult["ITEMS"][0]['IBLOCK_ID'], $arResult["ITEMS"][0]['ID'], "sort", "asc", array("CODE" => "ADRESS"));
    if ($r = $res->GetNext()) {
        $arResult["ITEMS"][0]['PROPERTIES']['adress'] = $r;
    }
}