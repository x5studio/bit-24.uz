<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


foreach (array_keys($_GET) as $key) {
    if (strpos($key, 'PAGEN_') !== false) {
        $APPLICATION->AddHeadString('<link href="https://bit-24.ru/blog/" rel="canonical" />');

        break;
    }
}


if ($GLOBALS['IS_BLOG_SECTION']) {
    $APPLICATION->AddChainItem($arResult['FIRST_SECTION']['NAME'], $arResult['FIRST_SECTION']['SECTION_PAGE_URL']);


    $sectionId = $arResult['FIRST_SECTION']['ID'];
    $iblockId = $arParams['IBLOCK_ID'];

    $ipropSectionValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($iblockId, $sectionId);
    $arSEO = $ipropSectionValues->getValues();
    if ($arSEO['SECTION_META_TITLE'] != false) {
        $APPLICATION->SetPageProperty("title", $arSEO['SECTION_META_TITLE']);
    }
    if ($arSEO['SECTION_META_KEYWORDS'] != false) {
        $APPLICATION->SetPageProperty("keywords", $arSEO['SECTION_META_KEYWORDS']);
    }
    if ($arSEO['SECTION_META_DESCRIPTION'] != false) {
        $APPLICATION->SetPageProperty("description", $arSEO['SECTION_META_DESCRIPTION']);
    }
}

