<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$arInfo = [
	"CURRENT" => $arResult["CURRENT"],
	"GEO" => array(),
	"NAME" => array()
];
?>
<div class="container hidden-lg hidden-md hidden-sm map-city js-city-name-wrap">
	<h2><?= $arResult["SECTION"][$arResult["CURRENT"]]["NAME"] ?></h2>
</div>
<nav class="navbar">
	<div class="navbar-header">
		<div class="hidden-lg hidden-md hidden-sm col-xs-12 filter">
			<button class="navbar-toggle1" type="button" data-toggle="collapse" data-target=".js-navbar-collapse1"><a href="#"
																																																								title="link">Выбрать
					город</a><i class="fa fa-filter" aria-hidden="true"></i></button>
		</div>
	</div>
	<div class="collapse navbar-collapse js-navbar-collapse1">
		<ul class="nav navbar-nav list-inline col-xs-12 hidden-lg hidden-md hidden-sm">
			<? foreach ($arResult["SECTION"] as $arSection): ?>
				<li><a href="#city_<?= $arSection["ID"] ?>" data-toggle="tab" class="js-change-city"
							 data-id="<?= $arSection["ID"] ?>"><?= $arSection["NAME"] ?></a></li>
			<? endforeach; ?>
		</ul>
	</div>
</nav>
<section class="maps" id="js-map-wrap"></section>
<section class="contacts-block">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<div class="tabs-left">
					<ul class="nav nav-tabs hidden-xs">
						<? foreach ($arResult["SECTION"] as $arSection): ?>
							<li<? if ($arSection["ID"] == $arResult["CURRENT"]): ?> class="active"<? endif; ?>>
								<a href="#city_<?= $arSection["ID"] ?>" data-toggle="tab" class="js-change-city"
									 data-id="<?= $arSection["ID"] ?>"><?= $arSection["NAME"] ?></a>
							</li>
						<? endforeach; ?>
					</ul>
					<div class="tab-content">
						<? foreach ($arResult["SECTION"] as $arSection):
							$arInfo["NAME"][$arSection["ID"]] = $arSection["NAME"];
							?>
							<div class="tab-pane<?= ($arSection["ID"] == $arResult["CURRENT"]) ? ' active' : ''; ?>"
									 id="city_<?= $arSection["ID"] ?>">
								<? foreach ($arSection["ITEMS"] as $arItem):
									$arInfo["GEO"][$arSection["ID"]][] = array(
										"NAME" => $arItem["NAME"],
										"ADDRESS" => $arItem["PROPERTY_ADRESS_VALUE"],
										"PHONE" => $arItem["PROPERTY_PHONE_VALUE"],
										"EMAIL" => $arItem["PROPERTY_EMAIL_VALUE"],
										"LAT" => $arItem["LAT"],
										"LON" => $arItem["LON"]
									);
									?>
									<div class="address col-xs-12"  itemscope itemtype="http://schema.org/Organization">
										<p><strong itemprop="name"><?= $arItem["NAME"] ?></strong></p>
										<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><?= $arItem["PROPERTY_ADRESS_VALUE"] ?></p>
										<p><img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/contact/call.png"><a
													href="tel:<?= $arItem["PROPERTY_PHONE_VALUE"] ?>" itemprop="telephone"><?= $arItem["PROPERTY_PHONE_VALUE"] ?></a>
										</p>
										<p class="mailaddress"><img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/contact/envelope.png"><a
													href="mailto:<?= $arItem["PROPERTY_EMAIL_VALUE"] ?>" itemprop="email"><?= $arItem["PROPERTY_EMAIL_VALUE"] ?></a>
										</p>
									</div>
								<? endforeach; ?>
							</div>
						<? endforeach; ?>
					</div><!-- /tab-content -->
				</div><!-- /tabbable -->
			</div><!-- /col -->
		</div><!-- /row -->
	</div><!-- /container -->
</section>
<script type="text/javascript">
	var Contacts = <? echo CUtil::PhpToJSObject($arInfo, false, true); ?>
</script>