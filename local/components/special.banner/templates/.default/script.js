$( document ).ready(function() {
    $(".custom_banner_close_button").on("click", function () {
        BX.setCookie(
            "close_banner_"+this.getAttribute( "data-banner-id" ),
            "true"
        );
        $(".custom_banner").hide();
    })
});