<?php

namespace Bit\AdditionalOptimizer\Image\Optimizer\Optimizers;

use Bit\AdditionalOptimizer\Image\Optimizer\Image;

class Optipng extends BaseOptimizer
{
    public $binaryName = 'optipng';

    public function canHandle(Image $image): bool
    {
        return $image->mime() === 'image/png';
    }
}
