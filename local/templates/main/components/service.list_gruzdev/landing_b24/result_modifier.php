<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


$allItemsIds = [];
foreach ($arResult["ITEMS"] as $arItem) {
    $allItemsIds[] = $arItem['ID'];
}

$dbRes = \CIBlockElement::GetList(
    [],
    [
        'IBLOCK_ID' => $arParams['IBLOCK_ID'],
        'ID' => ($allItemsIds ?: -1),
    ],
    false,
    false,
    [
        'IBLOCK_ID', 'ID', 'NAME', 'CODE', 'PROPERTY_PRICE', 'PROPERTY_LANDING_PREVIEW_TEXT',
    ]
);
$allItemsProperties = [];
while ($arRes = $dbRes->Fetch()) {
    $itemProperties = [];
    $itemProperties['PRICE'] = $arRes['PROPERTY_PRICE_VALUE'];
    $itemProperties['LANDING_PREVIEW_TEXT'] = $arRes['PROPERTY_LANDING_PREVIEW_TEXT_VALUE']['TEXT'];

    $allItemsProperties[$arRes['ID']] = $itemProperties;
}

foreach ($arResult["ITEMS"] as &$arItem) {
    $arItem['PROPERTIES'] = $allItemsProperties[$arItem['ID']];
}


$curPage = rtrim($APPLICATION->GetCurPage(), '/').'/';
$includeDirLocalPath = $curPage.'/include/';
$includeDirAbsPath = $_SERVER['DOCUMENT_ROOT'].$includeDirLocalPath;

$h2IncludeFileName = 'services__h2.php';

$arResult['INCLUDE_DIR_LOCAL_PATH'] = $includeDirLocalPath;
$arResult['INCLUDE_DIR_ABS_PATH'] = $includeDirAbsPath;
$arResult['H2_INCLUDE_FILE_NAME'] = $h2IncludeFileName;

$this->__component->SetResultCacheKeys([
    'INCLUDE_DIR_ABS_PATH',
]);

