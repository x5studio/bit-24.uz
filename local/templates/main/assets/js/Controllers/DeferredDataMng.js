if (typeof define === "function" && define.amd) {
    define([], function () {
        function DeferredDataMng(container) {
            var o = this;
            this.root = $(container);

            this.root.children().each(function () {
                var item = $(this);
                var toID = item.attr('to');
                var toItem = $('#' + toID);
                var itemTitle = toItem.data('title');
                if(typeof itemTitle != 'undefined'){
                    var arTitle = itemTitle.split(':');
                }
                if (toID.length > 0 && toItem.length > 0) {
                    try {
                        if(typeof arTitle != 'undefined'){
                            item.find('.'+arTitle[1]).text(arTitle[0]);
                        }
                        var p = toItem.parent();
                        toItem.replaceWith(item.html());
                        item.trigger('jDefEvent');
                    } catch (e) {
                        return;
                    }
                }
            });

            this.root.remove();
        }

        return DeferredDataMng;
    });
}