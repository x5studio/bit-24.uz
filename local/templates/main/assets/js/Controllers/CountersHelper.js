if (typeof define === "function" && define.amd) {
    define(['jquery'], function ($) {
        function CountersHelper() {
            this.counters = {}
            this.goalsData = {}
        }

        CountersHelper.prototype.sendGoal = function(goal) {
            this.yandexSendGoal(goal);
            this.googleSendGoal(goal);
        }

        CountersHelper.prototype.yandexSendGoal = function(goal) {
            let goalData = this.getGoalData(goal, 'yandex');

            if (goalData && typeof this.counters.yandex === 'object') {
                this.counters.yandex.reachGoal(goalData.code);
            }
        }

        CountersHelper.prototype.googleSendGoal = function(goal) {
            let goalData = this.getGoalData(goal, 'google');

            if (goalData && typeof gtag === 'function') {
                gtag('event', goalData.eventAction, {'event_category': goalData.eventCategory});
            }
        }

        CountersHelper.prototype.getGoalData = function(goal, counter) {
            return typeof this.goalsData[goal] !== 'undefined' ? (this.goalsData[goal][counter] || null) : null;
        }

        return CountersHelper;
    });
}