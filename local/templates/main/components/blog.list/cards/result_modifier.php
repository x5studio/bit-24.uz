<?php
global $APPLICATION;

$canonicalURL = 'https://bit-24.ru/blog/';

//Теги
$page = $APPLICATION->GetCurPage(false);
$arFilter = [
	'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	'ACTIVE' => 'Y',
    'ID' => array_keys($arResult['SECTIONS']),
];
$rsSection = CIBlockSection::GetList( ['SORT' => 'ASC'], $arFilter, false, ['*'], false);
$arSections = [];
while ($arSection = $rsSection->GetNext(true, false)) {
	$arSections[$arSection['ID']] = $arSection;
	if ($arResult['SECTION']['SECTION_PAGE_URL'] == $arSection['SECTION_PAGE_URL'] || $page == $arSection['SECTION_PAGE_URL']) {
		$arSections[$arSection['ID']]['CURRENT'] = 'Y';
		$canonicalURL = 'https://' . $_SERVER['HTTP_HOST'] . $arSection['SECTION_PAGE_URL'];
	}
}
$arResult['SECTIONS'] = $arSections;

//Каноническая ссылка для списка статей блога
$cp = $this->__component; // объект компонента
if (is_object($cp)) {
    $cp->arResult["MY_CANONICAL_LINK"] = $canonicalURL;
    $cp->SetResultCacheKeys(array('MY_CANONICAL_LINK'));
    $arResult['MY_CANONICAL_LINK'] = $cp->arResult['MY_CANONICAL_LINK'];
}
