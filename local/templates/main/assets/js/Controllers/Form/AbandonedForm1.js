if (typeof define === "function" && define.amd) {
    define([], function () {
        function AbandonedForm(container, options) {
            this.form = container;
            this.formId = +this.form.find("input[name='WEB_FORM_ID']").val();
            this.formOnly = $("body").data("abandon-form-only");

            this.toInit = !(!!this.formOnly && this.formOnly.length>0 && this.formOnly.indexOf(this.formId)==-1);

            this.options = $.merge(["PHONE", "EMAIL"], ["NAME"]);
            this.type = "form";
            if (this.form.attr("id") == "ORDER_FORM_ID_NEW")    this.type = "basket";
            this.formSubmit = false;
        }

        AbandonedForm.defaultOptions = {
            triggerFields: ["PHONE", "EMAIL"]
        };
        AbandonedForm.prototype.init = function () {
            var o = this;
            if(!o.toInit)
                return false;

            o.form.submit(function () {
                o.formSubmit = true;
                return false;
            });
            $(window).on('beforeunload', function () {
                if (o.check()) {
                    o.send();
                }
            });
        }
        AbandonedForm.prototype.check = function () {
            var o = this;
            var flag_send = false;
            if (!o.formSubmit) {
                for (var i = 0; i < AbandonedForm.defaultOptions.triggerFields.length; i++) {
                    var temp_input = o.form.find("input[data-type='" + AbandonedForm.defaultOptions.triggerFields[i] + "']");
                    if (temp_input.length > 0 && temp_input.val().length > 0) {
                        if (AbandonedForm.defaultOptions.triggerFields[i] == 'EMAIL') {
                            temp_input.rules("add", {
                                email: true
                            });
                            if (temp_input.valid()) {
                                flag_send = true;
                                break;
                            }
                        }
                        else  //���� �������
                        {
                            var RegPhone = /^(((\+7|\+38))[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{5,10}$/;
                            if (RegPhone.test(temp_input.val())) {
                                flag_send = true;
                                break;
                            }
                        }
                    }
                }
            }
            return flag_send;
        }
        AbandonedForm.prototype.send = function () {
            var o = this;
            var post_ar = [];
            for (var i = 0; i < o.options.length; i++) {
                if (o.form.find("[data-type='" + o.options[i] + "']").length > 0) {
                    var node = {};
                    node[o.options[i]] = o.form.find("[data-type='" + o.options[i] + "']").val();
                    post_ar.push(node);
                }
            }
            var officeId;
            if(o.type == "form"){
                officeId = o.form.find('input[id *= "Office"]:hidden').val();
            }
            else if	(o.type == "basket"){
                officeId = o.form.find("#order-props-office").val();
            }
            if (post_ar.length > 0) {
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    async: false,
                    url: '/ajax/abandonedForm.php',
                    data: {
                        idOffice: officeId,
                        post_ar: post_ar,
                        idForm: o.formId,
                        type: o.type,
                        url: location.protocol + '//' + location.host + location.pathname
                    },
                    success: function (data) {

                    }
                });
            }
        }
        return AbandonedForm;
    });
}