<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("form_title", "Заказать внедрение Битрикс24");
$APPLICATION->SetPageProperty("keywords_inner", "Корпоративный портал Битрикс24");

$APPLICATION->SetPageProperty("description", "Корпоративный портал Битрикс24 идеально подходит для совместной работы над проектами, постановки и контроля исполнения задач, оценки эффективности работы компании");
$APPLICATION->SetPageProperty("keywords", "Корпоративный портал Битрикс24");
$APPLICATION->SetPageProperty("title", "Корпоративный портал Битрикс24 - платформа для совместной работы сотрудников");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "N");
$APPLICATION->SetTitle("Корпоративный портал Битрикс24");
$APPLICATION->SetAdditionalCSS("/korporativniy-portal-bitrix24/css/style.css");

$APPLICATION->AddHeadString('
<!-- Marquiz script start --> <script src="//script.marquiz.ru/v1.js" type="application/javascript"></script>  <!-- Marquiz script end -->
');
?>


<? $APPLICATION->IncludeComponent(
    "bitrix:breadcrumb",
    "main",
    array(
        "PATH" => "",
        "SITE_ID" => "s1",
        "START_FROM" => "0",
        "COMPONENT_TEMPLATE" => "main"
    ),
    false
); ?>

<section class="portal-banner">
    <div class="container clearfix">
        <div class="intro_descr">
            <h1 class="intro_title">
                Разработка и внедрение<br> корпоративного портала (Intranet)<br>
                <span class="medium_font">на базе Битрикс24</span>
            </h1>
            <p>Единое решение для совместной работы сотрудников и оптимизации бизнес-процессов</p>
            <a href="#" class="btn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a>
        </div>
        <div class="banner-right-img">
            <img src="img/banner-pc.png" alt="">
        </div>
    </div>
</section>

<section class="opportunities">
    <div class="container">
        <div class="row tabs-wrapper">
            <div class="col-md-5">
                <h2 class="why-heading">Возможности<br> порталов</h2>
                <ul class="tabs">
                    <li class="tab"><span>01.</span>CRM для продаж и работы с клиентами</li>
                    <li class="tab"><span>02.</span>HR – управление персоналом</li>
                    <li class="tab"><span>03.</span>Работа над проектами и задачам</li>
                    <li class="tab"><span>04.</span>Коммуникация внутри компани</li>
                    <li class="tab"><span>05.</span>Сервисы для совместной работы</li>
                    <li class="tab"><span>06.</span>Автоматизация рутинных действий</li>
                    <li class="tab"><span>07.</span>Интеграция с учетными системами</li>
                </ul>
            </div>
            <div class="col-md-7">
                <div class="tab-outer">
                    <div class="tab-content">
                        <div class="tab-item">
                            <div class="card">
                                <div class="card-num">01.</div>
                                <div class="card-heading">CRM для продаж и работы с клиентами</div>
                                <div class="desc">
                                    <ul>
                                        <li>Лиды и продажи</li>
                                        <li>Коммуникация с клиентами</li>
                                        <li>CRM-маркетинг</li>
                                        <li>AI скоринг сделок</li>
                                        <li>Карточка клиента с историей</li>
                                        <li>Сквозная аналитика</li>
                                        <li>Автоматизация продаж</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-item" style="display:none;">
                            <div class="card">
                                <div class="card-num">02.</div>
                                <div class="card-heading">HR – управление персоналом</div>
                                <div class="desc">
                                    <ul>
                                        <li>Подбор персонала</li>
                                        <li>Приглашение и увольнение сотрудников</li>
                                        <li>Обучение и адаптация</li>
                                        <li>Данные о сотрудниках</li>
                                        <li>Структура компании</li>
                                        <li>Мероприятия</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-item" style="display:none;">
                            <div class="card">
                                <div class="card-num">03.</div>
                                <div class="card-heading">Работа над проектами и задачами</div>
                                <div class="desc">
                                    <ul>
                                        <li>Проекты</li>
                                        <li>Задачи</li>
                                        <li>Рабочие группы</li>
                                        <li>Учет рабочего времени</li>
                                        <li>Диаграмма Ганта</li>
                                        <li>Канбан</li>
                                        <li>Отчеты руководителю</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-item" style="display:none;">
                            <div class="card">
                                <div class="card-num">04.</div>
                                <div class="card-heading">Коммуникация внутри компании</div>
                                <div class="desc">
                                    <ul>
                                        <li>Живая лента</li>
                                        <li>Чат</li>
                                        <li>Видеосообщения</li>
                                        <li>Опросы</li>
                                        <li>Почта</li>
                                        <li>Новости</li>
                                        <li>Календарь</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-item" style="display:none;">
                            <div class="card">
                                <div class="card-num">05.</div>
                                <div class="card-heading">Сервисы для совместной работы</div>
                                <div class="desc">
                                    <ul>
                                        <li>Бронирование переговорных</li>
                                        <li>Планировщик встреч</li>
                                        <li>Совместная работа над документами</li>
                                        <li>Облачный Диск</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-item" style="display:none;">
                            <div class="card">
                                <div class="card-num">06.</div>
                                <div class="card-heading">Автоматизация рутинных действий</div>
                                <div class="desc">
                                    <ul>
                                        <li>Заказ справок и канцелярии</li>
                                        <li>Оформление больничных и отпусков</li>
                                        <li>График присутствия</li>
                                        <li>Согласование и выставление счетов</li>
                                        <li>Чат-боты</li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="tab-item" style="display:none;">
                            <div class="card">
                                <div class="card-num">07.</div>
                                <div class="card-heading">Интеграция с учетными системами</div>
                                <div class="desc">
                                    <ul>
                                        <li>Миграция из других CRM</li>
                                        <li>Импорт контактов</li>
                                        <li>Интеграция с 1С, Microsoft, SAP, BI</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="usefulness">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="why-heading blue-heading">Кому полезно</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <h4 class="white-heading">«1С-Битрикс24» — один из самых популярных корпоративных порталов в мире. <br>Им пользуются более 3 миллионов компаний.</h4>
            </div>
            <div class="col-md-2 footbtn">
                <a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a>
            </div>
        </div>
    </div>
</section>

<div class="useful-cards">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card wide-card top-card">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card-content">
                                <div class="card-heading">Руководителям</div>
                                <p>
                                    Контролировать сотрудников и получать от них обратную связь, собирать
                                    бизнес-аналитику (продажи, выполнение планов, текучка кадров, финансовые
                                    показатели и т.д.)
                                </p>
                                <img class="card-icon" src="img/ic1.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="perso-img"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row d-flex small-cards align-items-stretch">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content">
                        <div class="card-heading">IT-специалистам</div>
                        <p>
                            Предоставлять единый доступ к IT-
                            ресурсам компании, оказывать тех
                            поддержку через helpdesk- системы,
                            использовать адресную книгу с поиском
                            по ФИО, номеру, должности
                        </p>
                        <img class="card-icon" src="img/ic2.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content">
                        <div class="card-heading">HR-специалистам</div>
                        <p>
                            Проводить адаптацию, обучение
                            и аттестацию персонала, делать рассылки
                            и опросы, размещать должностные
                            инструкции, регламенты.
                        </p>
                        <img class="card-icon" src="img/ic3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-content">
                        <div class="card-heading">Менеджерам по продажам</div>
                        <p>
                            Вести клиентов в одной системе, хранить
                            все звонки, письма, чаты с клиентами
                            на сайте и в соцсетях внутри карточки
                            и т.д.
                        </p>
                        <img class="card-icon" src="img/ic4.png" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card wide-card bottom-card">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card-content">
                                <div class="card-heading">Для всех</div>
                                <p>
                                    Получать новости компании, общаться, контролировать задачи, использовать офисные сервисы:
                                    заказ справок, оформление отпусков, командировок, больничных, заказ канцелярии, бронирование
                                    переговорных и т.д.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="icon-img footbtn">
                                <a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="portal-price">
    <div class="container">
        <div class="row">
            <?$APPLICATION->IncludeComponent(
                "bitrix:news.list",
                "distr",
                array(
                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_URL" => "",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "FIELD_CODE" => array(
                        0 => "",
                        1 => "",
                    ),
                    "FILTER_NAME" => "",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "32",
                    "IBLOCK_TYPE" => "-",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "INCLUDE_SUBSECTIONS" => "Y",
                    "MESSAGE_404" => "",
                    "NEWS_COUNT" => "5",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PARENT_SECTION" => "",
                    "PARENT_SECTION_CODE" => "",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "PROPERTY_CODE" => array(
                        0 => "PRICE",
                        1 => "OLD_PRICE",
                        2 => "NAME_DISTR",
                    ),
                    "SET_BROWSER_TITLE" => "Y",
                    "SET_LAST_MODIFIED" => "N",
                    "SET_META_DESCRIPTION" => "Y",
                    "SET_META_KEYWORDS" => "Y",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "Y",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "ACTIVE_FROM",
                    "SORT_BY2" => "SORT",
                    "SORT_ORDER1" => "DESC",
                    "SORT_ORDER2" => "ASC",
                    "STRICT_SECTION_CHECK" => "N",
                    "COMPONENT_TEMPLATE" => "distr"
                ),
                false
            );?>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="heading-wrapper">
                    <h2 class="why-heading white-heading">
                        НЕ ЗНАЕТЕ КАКАЯ <br>ЛИЦЕНЗИЯ БИТРИКС 24 <br>
                        <span class="blue-heading">НУЖНА ВАМ?</span>
                    </h2>
                    <p class="white-heading">
                        Заполните форму, мы изучим потребности вашего <br>
                        бизнеса и порекомендуем наиболее оптимальную <br>
                        и выгодную для вас лицензию
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <? $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "b24", Array(
                    "WEB_FORM_ID" => 16,  // ID веб-формы
                    "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
                    "COMPANY_IBLOCK_ID" => IB_CONTACTS,
                    "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
                    "EMAIL_QUESTION_CODE" => "EMAIL",
                    "CITY_QUESTION_CODE" => "CITY",
                    "OFFICE_QUESTION_CODE" => "OFFICE",
                    "TO_QUESTION_RESIPIENTS" => "",
                    "EMAIL_ONLY_FROM_RESIPIENTS" => "",
                    "PRODUCT_QUESTION_CODE" => "PRODUCT",
                    "TO_QUESTION_CODE" => "TO",
                    "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
                    "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
                    "SEF_MODE" => "N",  // Включить поддержку ЧПУ
                    "CACHE_TYPE" => "A",  // Тип кеширования
                    "CACHE_TIME" => "3600",  // Время кеширования (сек.)
                    "LIST_URL" => "",  // Страница со списком результатов
                    "EDIT_URL" => "",  // Страница редактирования результата
                    "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
                    "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
                    "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
                    "VARIABLE_ALIASES" => array(
                        "WEB_FORM_ID" => "WEB_FORM_ID",
                        "RESULT_ID" => "RESULT_ID",
                    ),
                    "ALERT_ADD_SHARE" => "N",
                    "HIDE_PRIVACY_POLICE" => "Y",
                    "BTN_CALL_FORM" => ".btn_order",
                    "STRONG_SHOW_CITY" => $STRONG_SHOW_CITY,
                    "STRONG_SHOW_OFFICE" => $STRONG_SHOW_OFFICE,
                    "HIDE_FIELDS" => array(
                        0 => "CITY",
                        1 => "OFFICE",
                    ),
                    "COMPONENT_MARKER" => "request",
                    "SHOW_FORM_DESCRIPTION" => "N",
                    "MESS" => array(
                        "THANK_YOU" => "Спасибо, Ваша заявка принята.",
                        "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
                        "TITLE_FORM" => "Оставить заявку",
                    )
                ),
                    false,
                    array(
                        "HIDE_ICONS" => "Y"
                    )
                ); ?>
            </div>
        </div>
    </div>
</section>


<section class="portal-help">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="why-heading">Как мы поможем?</h2>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-num">01.</div>
                    <div class="card-desc">
                        <img class="card-icon" src="img/ic6.png" alt="">
                        <img class="card-arrow" src="img/ic10.png" alt="">
                        <div class="card-heading">Моделирование</div>
                        <div class="card-text">
                            Проанализируем ваш бизнес <br>
                            и составим план внедрения <br>
                            Битрикс24.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-num">02.</div>
                    <div class="card-desc">
                        <img class="card-icon" src="img/ic7.png" alt="">
                        <div class="card-heading">Разработка</div>
                        <div class="card-text">
                            Установка и программная разработка <br>
                            портала под ваши бизнес-процессы, <br>
                            интеграция с другими системами
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-num">03.</div>
                    <div class="card-desc">
                        <img class="card-icon" src="img/ic8.png" alt="">
                        <img class="card-arrow" src="img/ic10.png" alt="">
                        <div class="card-heading">Обучение</div>
                        <div class="card-text">
                            Очно или дистанционно обучим работе <br>
                            в системе по ролям и функциям, которые <br>
                            будут использоваться в работе.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-num">04.</div>
                    <div class="card-desc">
                        <img class="card-icon" src="img/ic9.png" alt="">
                        <div class="card-heading">Сопровождение</div>
                        <div class="card-text">
                            Проконтролируем корректную работу <br>
                            всех бизнес-процессов, проконсультируем <br>
                            пользователей по функционалу.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="portal-results">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="why-heading blue-heading">Каких ждать результатов?</h2>
            </div>
        </div>

        <div class="row results-list">
            <div class="col-md-6">
                <p>Сократиться время на поиск документов, коллег, так как все будет собрано в одном месте</p>
            </div>
            <div class="col-md-6">
                <p>Сотрудники смогут совместно работать над проектами</p>
            </div>
            <div class="col-md-6">
                <p>Новым сотрудникам будет проще адаптироваться и давать первые результаты</p>
            </div>
            <div class="col-md-6">
                <p>Ускорятся типовые процессы как согласование, заказ справок, бронирование и др.</p>
            </div>
            <div class="col-md-6">
                <p>Затраченное время на задачи, загруженность и эффективность сотрудников будут перед глазами</p>
            </div>
            <div class="col-md-6">
                <p>Вырастет вовлеченность персонала в корпоративную жизнь</p>
            </div>
            <div class="col-md-6">
                <p>Качество предоставляемых услуг и производства вырастут</p>
            </div>
            <div class="col-md-6">
                <p>Руководителям будет проще контролировать поручения и бизнес-показатели</p>
            </div>
        </div>

    </div>
</section>


<section class="main-clients portal-clients">
    <div class="container">
        <div class="row why-row">
            <div class="col-md-6 col-sm-10">
                <h2 class="why-heading pull-left">Убедитесь сами</h2>
            </div>
            <div class="col-md-6 col-sm-2 hidden-xs">
                <a href="/nashi-klienty/" class="btn btn-why pull-right">Подробнее</a>
            </div>
        </div>
    </div>
    <?php
    $APPLICATION->IncludeComponent(
        "client.list",
        "",
        Array(
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "COUNT" => 6,
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "IBLOCK_ID" => "8",
            'FRONTPAGE_STYLE' => 'Y',
        )
    );
    ?>
    <div class="hidden-lg hidden-md hidden-sm col-xs-12 detline">
        <a class="btn btn-why" href="/nashi-klienty/">Подробнее</a>
    </div>
</section>


<section class="portal-partner">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="why-heading white-heading">Крупнейший федеральный <br>партнер «1С-Битрикс»</h2>
            </div>
        </div>
        <div class="row d-flex portal-partner-numbers">
            <div class="col-lg-4 portal-partner-num1">
                <div class="team-card">
                    <div class="team-num">200</div>
                    <div class="team-title">Человек в команде</div>
                    <div class="team-desc">
                        Среди которых: штатные <br>
                        и сертифицированны <br>
                        специалисты
                    </div>
                </div>
            </div>
            <div class="col-lg-4 portal-partner-num1">
                <div class="team-card">
                    <div class="team-num">22</div>
                    <div class="team-title">Года на рынке</div>
                    <div class="team-desc">
                        Успешно занимаемся <br>
                        автоматизацией в России <br>
                        и странах СНГ
                    </div>
                </div>
            </div>
            <div class="col-lg-8 portal-partner-num2">
                <div class="team-card">
                    <div class="team-num">250 000+</div>
                    <div class="team-title">Успешных внедрений</div>
                    <div class="team-desc">
                        Автоматизированных систем
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div style="visibility: hidden;" class="marquiz-pops marquiz-pops_position_bottom-left marquiz-pops_blicked marquiz-pops_shadowed marquiz-pops_position" ><a class="marquiz-pops__body" href="#popup:marquiz_5de6172e4734030045bdb536" data-marquiz-pop-text-color="#ffffff" data-marquiz-pop-background-color="#d34085" data-marquiz-pop-svg-color="#fff" data-marquiz-pop-close-color="#fff" data-marquiz-pop-color-pulse="rgba(211, 64, 133, 0.4)" data-marquiz-pop-color-pulse-alpha="#d34085" data-marquiz-pop-delay="2s" data-marquiz-pop="true"><span class="marquiz-pops__icon"></span><span class="marquiz-pops__content"><span class="marquiz-pops__content-title">Пройти тест</span><span class="marquiz-pops__content-text">&laquo;Рассчитать стоимость внедрения&raquo;</span></span></a></div>


<script>
    $(function() {

        $(".portal-price .tab-item").not(":first-child").hide();
        $(".portal-price .tab:first-child").addClass("active");

        $(".portal-price .tabs-wrapper .tab").click(function() {
            $(this).closest(".tabs-wrapper").find(".tab").removeClass("active").eq($(this).index()).addClass("active");
            $(this).closest(".tabs-wrapper").find(".tab-item").hide().eq($(this).index()).fadeIn();
        }).eq(0).addClass("active");

        if ($(window).width() > 650) {
            $(".opportunities .tab-item").not(":first-child").hide();
            $(".opportunities .tab:first-child").addClass("active");

            $(".opportunities .tabs-wrapper .tab").click(function() {
                $(this).closest(".tabs-wrapper").find(".tab").removeClass("active").eq($(this).index()).addClass("active");
                $(this).closest(".tabs-wrapper").find(".tab-item").hide().eq($(this).index()).fadeIn();
            }).eq(0).addClass("active");
        } else {
            $(".opportunities .tabs").find(".tab").wrap("<div class='drop-tab'></div>");
            $(".opportunities .tabs-wrapper").find(".tab").removeClass("active");
            $(".opportunities .tab-content").find(".tab-item").hide().each(function(indx, element){
                $(element).appendTo(".drop-tab:eq("+indx+")");
            });
            $(".opportunities .tabs-wrapper .drop-tab").click(function() {
                $(this).closest(".tabs-wrapper").find(".tab-item").slideUp();
                $(this).closest(".tabs-wrapper").find(".tab").removeClass("active");

                if(!$(this).hasClass("expanded")) {
                    $(this).addClass("expanded");
                    $(this).find(".tab").addClass("active");
                    $(this).find(".tab-item").slideDown();
                } else {
                    $(this).closest(".tabs-wrapper").find(".drop-tab").removeClass("expanded");
                }
            });
        }
    });
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
