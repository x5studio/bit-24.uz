<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Блог ЧПУ",
	"DESCRIPTION" => "Блог ЧПУ",
	"ICON" => "/images/catalog.gif",
	"SORT" => 100,
	"PATH" => array(
		"ID" => "proman",
		"NAME" => "proman",
		"CHILD" => array(
			"ID" => "blog",
			"NAME" => "Блог"
		)
	),
);
?>