if (typeof define === "function" && define.amd) {
    define([], function () {

        function Loc () {
            this.cssClassName = '.j-loc';

            this.init();
        }

        Loc.prototype.init = function () {
            $(document).on('onMainInit', BX.proxy(
                this.onMainInit,
                this
            ));

            this.parseEls($(this.cssClassName));
        };

        Loc.prototype.onMainInit = function (event) {
            var context = $(event.contextContainer);

            var els = $(this.cssClassName, context);
            this.parseEls(els);
        };

        Loc.prototype.parseEls = function ($els) {
            for(var i=0; i<$els.length ; i++)
            {
                this.parseBlock($els.eq(i))
            }

        };

        Loc.prototype.parseBlock = function($el)
        {
            BX.message($el.data('jsMess'));
        };

        Loc.prototype.getMessage = function (id) {
            return BX.message(id)
        };

        Loc.prototype.getWithPrefix = function (prefix) {
            var
                Loc = this;

            return function(id){
                return Loc.getMessage(prefix+id);
            }
        };

        return new Loc();
    })
}