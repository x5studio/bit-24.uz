<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CMain $APPLICATION */
?>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118715209-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118715209-1');
</script>

<!-- END Global site tag (gtag.js) - Google Analytics -->

<script>
    /**
     * Track events for analytics
     * http://goo.gl/tOCbJq
     * @param p
     * Пример: track(['Действие', 'Клик', 'Контактный блок', 1]);
     */
    window.track = function (p) {
        //_gaq.push(['_trackEvent', p[0], p[1], p[2], p[3], p[4]]);
        //ga('send', 'event', p[0], p[1], p[2], p[3]);
    }

    /**
     * Track step
     * http://goo.gl/uhNB5C
     * @param path
     * Пример: trackStep('/ga-ajax/soft/open/form/');
     */
    window.trackStep = function (path) {
        try {
            console.log(path);
        } catch (e) {
        }
        //_gaq.push(['_trackPageview', path]);
        //ga('send', 'pageview', path);
    };

    $(document).on('form.result.popup.open', function () { // отправка формы
        try {
            gtag('event',
                'page_view',
                {
                    'send_to': 'AW-817589391',
                    'dynx_pagetype': location.toString()
                }
            );
        }catch(e){
            console.error(e)
        }
    })
</script>



<meta name="yandex-verification" content="8b63111ccc673a23" />
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45927957 = new Ya.Metrika({
                    id:45927957,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45927957" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<meta name="yandex-verification" content="8b63111ccc673a23" />
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '134766647184493');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=134766647184493&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->