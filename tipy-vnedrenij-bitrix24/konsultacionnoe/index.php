<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Пошаговый план внедрение корпоративного портала с гарантированным результатом! Простые шаги - аналитика, настройка, обучение, сопровождение. Первый Бит - заходите, обсудим ваш проект.");
$APPLICATION->SetPageProperty("keywords", "консалтинг битрикс24");
$APPLICATION->SetPageProperty("title", "Аналитика и настройка корпоративного портала Битрикс24");
$APPLICATION->SetTitle("Консалтинг по Битрикс24 - аналитика, настройка, обучение, результат!");
?>
	<section class="first-bt-block">
		<div class="container">
			<div class="row">
				<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/konsultacionnoe/.inc.block_1.php", Array(), Array("MODE" =>
					"html")); ?>
			</div>
		</div>
	</section>
	<!-- Three block -->
	<section class="three-block">
		<div class="container">
			<div class="row">
				<h2>Консультационный подход выбирают</h2>
				<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/konsultacionnoe/.inc.block_2.php", Array(), Array("MODE" =>
					"html")); ?>
			</div>
		</div>
	</section>
	<!-- Four block -->
	<section class="four-blue-block">
		<div class="container">
			<div class="row">
				<h2>Консультационное внедрение <span>корпоративного портала</span> осуществляется поэтапно</h2>
				<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/konsultacionnoe/.inc.block_3.php", Array(), Array("MODE" =>
					"html")); ?>
			</div>
		</div>
	</section>
	<!-- Steps -->
	<section class="step-bt">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="material">
						<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/konsultacionnoe/.inc.step_1.php", Array(), Array("MODE" =>
							"html")); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="step2">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="material2">
						<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/konsultacionnoe/.inc.step_2.php", Array(), Array("MODE" =>
							"html")); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="step3">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="material2">
						<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/konsultacionnoe/.inc.step_3.php", Array(), Array("MODE" =>
							"html")); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="step4">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="material2">
						<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/konsultacionnoe/.inc.step_4.php", Array(), Array("MODE" =>
							"html")); ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/.inc.why.php", Array(), Array("MODE" =>
	"html")); ?>
<? $APPLICATION->IncludeComponent(
	"client.by.id",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"EXPERTISE_ID" => 33,
		"IBLOCK_ID" => "8"
	)
); ?>
<? for ($i = 1; $i <= 9; $i++): ?>
	<div id="modal-more-<?= $i ?>" class="modal more-modal" tabindex="-1" role="dialog" aria-labelledby="advancedSort"
			 aria-hidden="true">
		<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/konsultacionnoe/.inc.more_".$i.".php", Array(), Array("MODE" =>
			"html")); ?>
	</div>
<? endfor; ?>

<?php
 $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
    "WEB_FORM_ID" => 13,    // ID веб-формы
    "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
    "COMPANY_IBLOCK_ID" => IB_CONTACTS,
    "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
    "EMAIL_QUESTION_CODE" => "EMAIL",
    "CITY_QUESTION_CODE" => "CITY",
    "OFFICE_QUESTION_CODE" => "OFFICE",
    "TO_QUESTION_RESIPIENTS" => "",
    "EMAIL_ONLY_FROM_RESIPIENTS" => "",
    "PRODUCT_QUESTION_CODE" => "PRODUCT",
    "TO_QUESTION_CODE" => "TO",
    "IGNORE_CUSTOM_TEMPLATE" => "N",    // Игнорировать свой шаблон
    "USE_EXTENDED_ERRORS" => "Y",    // Использовать расширенный вывод сообщений об ошибках
    "SEF_MODE" => "N",    // Включить поддержку ЧПУ
    "CACHE_TYPE" => "A",    // Тип кеширования
    "CACHE_TIME" => "3600",    // Время кеширования (сек.)
    "LIST_URL" => "",    // Страница со списком результатов
    "EDIT_URL" => "",    // Страница редактирования результата
    "SUCCESS_URL" => "",    // Страница с сообщением об успешной отправке
    "CHAIN_ITEM_TEXT" => "",    // Название дополнительного пункта в навигационной цепочке
    "CHAIN_ITEM_LINK" => "",    // Ссылка на дополнительном пункте в навигационной цепочке
    "VARIABLE_ALIASES" => array(
        "WEB_FORM_ID" => "WEB_FORM_ID",
        "RESULT_ID" => "RESULT_ID",
    ),
    "ALERT_ADD_SHARE" => "N",
    "HIDE_PRIVACY_POLICE" => "Y",
    "BTN_CALL_FORM" => ".btn-buy",
    "HIDE_FIELDS" => array(
        0 => "CITY",
        1 => "OFFICE",
        2 => "TYPE",
    ),
    "COMPONENT_MARKER" => "feedform",
    "SHOW_FORM_DESCRIPTION" => "N",
    "MESS" => array(
        "THANK_YOU" => "Спасибо, Ваша заявка принята.",
        "WAIT_CALL" => "Мы свяжемся с Вами в течение 2-х часов.",
        "TITLE_FORM" => "Заказать услугу2",
    )
),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
);
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>