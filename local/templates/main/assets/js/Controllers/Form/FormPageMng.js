if (typeof define === "function" && define.amd) {
    define(['Controls/Form/FormMng',
        'fancybox',
        'cookie',
        'router'], function (FormMng) {

        function FormPageMng(p) {
            FormMng.apply(this, arguments);
        }

        extend(FormPageMng, FormMng);

        FormPageMng.prototype.getGlobalName = function () {
            var o = this;
            return 'formGeneral' + o.saltForm
        }

        FormPageMng.prototype.checkHideSelectOffice = function () {
        	var o = this;
        	var selectOfficeText = o.root.find(".selectOfficeFormHidden");
        	if(selectOfficeText.length > 0 && selectOfficeText.data('hide-office-text') == 'Y') {
        		selectOfficeText.find('a').click(function(){
        			$(this).parent().hide();
        		});
        	}
        }

        FormPageMng.prototype.setFormTitleInField = function(){
            var o = this;
            if(o.root.find('input[data-code="FORM_TITLE"]').length > 0) {
                var formTitle = 'inline form';
                var tidata=o.root.data('title-text');
                if (tidata != '')
                    formTitle=tidata;
                o.root.find('input[data-code="FORM_TITLE"]').val(formTitle);
            }
        }

        FormPageMng.prototype.setFormOffset = function(){
          var o = this;
          var formOffset = o.root.offset();
          if(formOffset){
            if(o.root.find('input[name="OFFSET"]').length > 0) {
              o.root.find("input[name='OFFSET']").val(Math.ceil(formOffset.top));
            }else{
              o.root.find('form').append('<input type="hidden" name="OFFSET" value="' + Math.ceil(formOffset.top) + '" />');
            }
          }
        }

        FormPageMng.prototype.init = function () {
            var o = this;
            FormPageMng.superclass.init.apply(o);
            o.initMaskedInput();
            o.initValidate();
            o.trimEmail();
            o.checkHideSelectOffice();
            o.setFormTitleInField();
            o.setFormOffset();
        }


        return FormPageMng;
    });
}
