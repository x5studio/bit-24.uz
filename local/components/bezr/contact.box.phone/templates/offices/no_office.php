<div class="box-offices box-office-single">
	<?
	if($arParams['SHOW_FREE_REGION'] == 'Y') {
		$HTML_TITLE = '';
	} else {
		$HTML_TITLE = '<div class="box-title h2">' . \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_OFFICE_IN') . ' <a href="##HTML_LIST_ID#" class="city j-city"><b>#NAME_DATIVE#</b></a></div>';
	}
	global $contFilter;
	$APPLICATION->IncludeComponent("vitams:city.dropdown", "city_simple",
		Array(
			"IBLOCK_TYPE" => IBT_CONTACTS,
			"IBLOCK_ID" => IB_CONTACTS,
			"SECTION_CODE" => "",
			"SECTION_URL" => "",
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "1",
			"SECTION_FIELDS" => "",
			"FILTER_NAME" => "cityFilter",
			"SECTION_USER_FIELDS" => "",
			"ADD_SECTIONS_CHAIN" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_NOTES" => "",
			"CACHE_GROUPS" => "Y",
			"LINK_IN_BASEURL" => "Y",
			"LINK_WITH_SUBDOMAIN" => "Y",
			"SEF_MODE" => "N",
			"SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
			"HTML_LIST_ID" => "header-city-list-inner",
			"HTML_TITLE" => $HTML_TITLE,
			"BY_CACHE_SUBDOMAIN_PAGE" => $_SESSION["REGION"]["CODE"],
			"SHOW_MOSCOW" => $_SESSION['SHOW_MOSCOW'],
			'UI_CITY_STRONG' => $arParams['CITY_DROPDOWN_UI_CITY_STRONG'],
			'UI_CITY_SPACE' => $arParams['CITY_DROPDOWN_UI_CITY_SPACE'],
		)/*, false,
		Array("HIDE_ICONS"=>"Y")*/
	);?>

	<div class="box-free-region">
		<?if(!empty($contFilter['MESS_NO_OFFICE'])):?>
			<?=$contFilter['MESS_NO_OFFICE']?>
		<?else:?>
			<div class="wrap-fr">
				<div class="name-fr"><?php echo \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_FR_NAME'); ?></div>
				<div class="phone-fr">8 800 700 59 55</div>
				<div class="desc-fr"><?php echo \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_FR_DESC'); ?></div>
			</div>
		<?endif;?>
	</div>
</div>