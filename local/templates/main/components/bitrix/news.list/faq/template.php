<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//echo '<pre>';
//var_dump($arResult['SECTIONS']);
?>
<div class="faq__content">
	<div class="faq-list__items">
		<? if ($arResult['SECTIONS']): ?>
			<? foreach ($arResult['SECTIONS'] as $arSection): ?>
				<div class="faq-list__item faq-list__spoiler">
					<button type="button" class="faq-list__item-btn faq-list__spoiler-btn"><?= $arSection['NAME']; ?></button>
					<div class="faq-list__item-content faq-list__spoiler-content">
						<div class="faq-list__item-content-blocks">
							<? if ($arSection['CHILD']): ?>
								<? foreach ($arSection['CHILD'] as $arSectionChild): ?>
									<div class="faq-list__item-content-block faq-list__spoiler">
										<button type="button" class="faq-list__item-content-block-btn faq-list__spoiler-btn"><?= $arSectionChild['NAME']; ?></button>
										<? if ($arResult['SECTION_ITEMS'][$arSectionChild['ID']]): ?>
											<div class="faq-list__item-content-block-links faq-list__spoiler-content">
												<? foreach ($arResult['SECTION_ITEMS'][$arSectionChild['ID']] as $arItem): ?>
													<a href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $arItem['NAME']; ?></a><br/>
												<? endforeach; ?>
											</div>
										<? endif; ?>
									</div>
								<? endforeach; ?>
							<? endif; ?>
							<? if ($arResult['SECTION_ITEMS'][$arSection['ID']]): ?>
								<div class="faq-list__item-content-block-links">
									<? foreach ($arResult['SECTION_ITEMS'][$arSection['ID']] as $arItem): ?>
										<a href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $arItem['NAME']; ?></a><br/>
									<? endforeach; ?>
								</div>
							<? endif; ?>
						</div>
					</div>
				</div>
			<? endforeach; ?>
		<? endif; ?>
	</div>
</div>