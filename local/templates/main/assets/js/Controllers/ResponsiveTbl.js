/**
 * Created by PSBezrukov on 29.05.2015.
 */
if (typeof define === "function" && define.amd) {
    define([], function () {
        function ResponsiveTbl(p) {
            var o = this;

            o.root = $(p.root);
            o.obResponsive = p.obResponsive;

            o.rows = [];

            o.init();
        }

        ResponsiveTbl.prototype.makePrevRow = function () {
            return $("<div></div>").addClass('resp-prev').html("<span></span>");
        };

        ResponsiveTbl.prototype.makeNextRow = function () {
            return $("<div></div>").addClass('resp-next').html("<span></span>");
        };

        ResponsiveTbl.prototype.activeRow = function (obRow) {
            var o = this;
            for (var i in o.rows) {
                var itemObRow = o.rows[i];
                if (itemObRow.row[0] != obRow.row[0]) {
                    o.deactiveRow(itemObRow);
                } else {
                    itemObRow.row.addClass('active');
                    o.setRowHeight(itemObRow, obRow.group2);
                }
            }
        };

        ResponsiveTbl.prototype.deactiveRow = function (obRow) {
            var o = this;
            obRow.row.removeClass('active');
            o.setRowHeight(obRow, obRow.group1);
        };

        ResponsiveTbl.prototype.setRowHeight = function (obRow, groupItems) {
            var o = this;

            var maxHeight = 0;
            groupItems.each(function () {
                var item = $(this);
                var itemHeight = item.outerHeight();
                if (itemHeight > maxHeight) {
                    maxHeight = itemHeight;
                }
            });

            if (maxHeight > 0) {
                maxHeight += 1;
            } else {
                maxHeight = 'auto';
            }
            obRow.row.animate({height: maxHeight}, 500);
        };

        ResponsiveTbl.prototype.addRow = function (row) {
            var o = this;

            var obRow = {};
            obRow.row = $(row);

            var prev = o.makePrevRow();
            var next = o.makeNextRow();

            var respGroup1 = obRow.row.find('.resp-cell-1');
            respGroup1.last().append(next);
            next.on('touchstart click', function () {
                o.activeRow(obRow);
                return false;
            });
            obRow.group1 = respGroup1;

            var respGroup2 = obRow.row.find('.resp-cell-2');
            respGroup2.first().prepend(prev);
            prev.on('touchstart click', function () {
                o.deactiveRow(obRow);
                return false;
            });
            obRow.group2 = respGroup2;

            respGroup2.insertAfter(respGroup1.last());

            o.rows.push(obRow);
        };

        ResponsiveTbl.prototype.init = function () {
            var o = this;

            o.root.find('.resp-row').each(function () {
                o.addRow(this)
            });

        };

        return ResponsiveTbl;
    });
}