if (typeof define === "function" && define.amd) {
    define(['owlcarousel2'], function () {
        function OwlCarousel2(container) {
            var _this = this;
            this.container = $(container);

            var paramsOwl = new Object();
            _this.callbackOnChange = (this.container.data('actionOnChange')) ? new Function("e", this.container.data('actionOnChange')) : false;
            _this.callbackOnInit = (this.container.data('actionOnInit')) ? new Function("e", this.container.data('actionOnInit')) : false;



            //Назначение событий прокрутки
            if(_this.container.data('stopvideo') == 'Y') {
                paramsOwl['onTranslated'] = function (event) {
                    //Ищем все видео
                    var allVideos = $(event.target).find('iframe');

                    //Обходим и заставляем заткнуться
                    $( allVideos ).each(function() {
                        $( this )[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
                    });

                };

            }

            var loop = true;
            if(_this.container.data('loop') == 'N') { loop = false; }
            paramsOwl['loop'] = loop;

            var dots = true;
            if(_this.container.data('dots') == 'N') { dots = false; }
            paramsOwl['dots'] = dots;

            var nav = true;
            if(_this.container.data('nav') == 'N') { nav = false; }
            paramsOwl['nav'] = nav;

            var autoplay = true;
            if(_this.container.data('autoplay') == 'N') { autoplay = false; }
            paramsOwl['autoplay'] = autoplay;

            var autoHeight = false;
            if(_this.container.data('autoHeight') == 'Y') { autoHeight = true; }
            paramsOwl['autoHeight'] = autoHeight;

            var pullDrag = false;
            if(_this.container.data('pullDrag') == 'Y') { pullDrag = true; }
            paramsOwl['pullDrag'] = pullDrag;

            var freeDrag = false;
            if(_this.container.data('freeDrag') == 'Y') { freeDrag = true; }
            paramsOwl['freeDrag'] = freeDrag;

            var slideBy = 1;
            if(_this.container.data('slideBy') != '' && $.isNumeric(_this.container.data('slideBy'))) { slideBy = _this.container.data('slideBy'); }
            paramsOwl['slideBy'] = slideBy;

            var autoplaySpeed = 700;
            if(_this.container.data('autoplaySpeed') != '' && $.isNumeric(_this.container.data('autoplaySpeed'))) { autoplaySpeed = _this.container.data('autoplaySpeed'); }
            paramsOwl['autoplaySpeed'] = autoplaySpeed;

            var autoplayTimeout = 5000;
            if(_this.container.data('autoplayTimeout') != '' && $.isNumeric(_this.container.data('autoplayTimeout'))) { autoplayTimeout = _this.container.data('autoplayTimeout'); }
            paramsOwl['autoplayTimeout'] = autoplayTimeout;

            var autoplayHoverPause = false;
            if(_this.container.data('autoplayHoverPause') == 'Y') { autoplayHoverPause = true; }
            paramsOwl['autoplayHoverPause'] = autoplayHoverPause;

            var navSpeed = 700;
            if(_this.container.data('navSpeed') != '' && $.isNumeric(_this.container.data('navSpeed'))) { navSpeed = _this.container.data('navSpeed'); }
            paramsOwl['navSpeed'] = navSpeed;

            var dotsSpeed = null;
            if(_this.container.data('dotsSpeed') != '' && $.isNumeric(_this.container.data('dotsSpeed'))) { dotsSpeed = _this.container.data('dotsSpeed'); paramsOwl['dotsSpeed'] = dotsSpeed; }

            var margin = null;
            if(_this.container.data('margin') != '' && $.isNumeric(_this.container.data('margin'))) { margin = _this.container.data('margin'); paramsOwl['margin'] = margin; }

            var items = 1;
            if(_this.container.data('items') != '' && $.isNumeric(_this.container.data('items'))) { items = _this.container.data('items'); }
            paramsOwl['items'] = items;

            var center = false;
            if(_this.container.data('center') == 'Y') { center = true; }
            paramsOwl['center'] = center;

            var mouseDrag = true;
            if(_this.container.data('mouseDrag') == 'N') { mouseDrag = false; }
            paramsOwl['mouseDrag'] = mouseDrag;

            var touchDrag = true;
            if(_this.container.data('touchDrag') == 'N') { touchDrag = false; }
            paramsOwl['touchDrag'] = touchDrag;

            var pullDrag = true;
            if(_this.container.data('pullDrag') == 'N') { pullDrag = false; }
            paramsOwl['pullDrag'] = pullDrag;

            var freeDrag = false;
            if(_this.container.data('freeDrag') == 'Y') { freeDrag = true; }
            paramsOwl['freeDrag'] = freeDrag;

            var stagePadding = null;
            if(_this.container.data('stagePadding') != '' && $.isNumeric(_this.container.data('stagePadding'))) { stagePadding = _this.container.data('stagePadding'); paramsOwl['stagePadding'] = stagePadding; }

            var autoWidth = false;
            if(_this.container.data('autoWidth') == 'Y') { autoWidth = true; }
            paramsOwl['autoWidth'] = autoWidth;

            var startPosition = null;
            if(_this.container.data('startPosition') != '' && $.isNumeric(_this.container.data('startPosition'))) { startPosition = _this.container.data('startPosition'); paramsOwl['startPosition'] = startPosition; }

            var lazyLoad = false;
            if(_this.container.data('lazyLoad') == 'Y') { lazyLoad = true; }
            paramsOwl['lazyLoad'] = lazyLoad;

            var itemElement = 'div';
            if(_this.container.data('itemElement') != '' && $.type(_this.container.data('itemElement') === 'string')) {
              itemElement = _this.container.data('itemElement');
              paramsOwl['itemElement'] = itemElement;
            }

            var stageElement = 'div';
            if(_this.container.data('stageElement') != '' && $.type(_this.container.data('stageElement') === 'string')) {
              stageElement = _this.container.data('stageElement');
              paramsOwl['stageElement'] = stageElement;
            }

            var navContainer = false;
            if(_this.container.data('navContainer') != '' && $.type(_this.container.data('navContainer') === 'string')) {
              navContainer = _this.container.data('navContainer');
              paramsOwl['navContainer'] = navContainer;
            }

            var dotsContainer = false;
            if(_this.container.data('dotsContainer') != '' && $.type(_this.container.data('dotsContainer') === 'string')) {
              dotsContainer = _this.container.data('dotsContainer');
              paramsOwl['dotsContainer'] = dotsContainer;
            }




                paramsOwl['responsive'] = new Object();
            var flagResp = false;

            if($.type(_this.container.data('responsive')) != 'undefined') {
            	var tmp = _this.container.data('responsive').split(";");
            	for(var i=0 ; i<tmp.length; i++) {
	            	if(tmp[i].length > 0) {
	            		var tmp2 = tmp[i].split("=");
	            		if(tmp2.length == 2) {
	            			paramsOwl['responsive'][tmp2[0]] = new Object();
	            			paramsOwl['responsive'][tmp2[0]]['items'] = tmp2[1];
	            			flagResp = true;
	            		}
	            	}
            	}
            }else if($.type(_this.container.data('responsiveExtended')) != 'undefined') {
              var responsiveParams = _this.container.data('responsiveExtended');
              for (var prop in responsiveParams) {
                paramsOwl['responsive'][prop] = new Object();
                for(var key in responsiveParams[prop]) {
                  paramsOwl['responsive'][prop][key] = responsiveParams[prop][key];
                }
              }
            }

            if(_this.container.data('random') == 'Y') {
              paramsOwl['onInitialize'] = function(element){
                _this.container.children().sort(function(){
                  return Math.round(Math.random()) - 0.5;
                }).each(function(){
                  $(this).appendTo(_this.container);
                });
              }
            }

            if(_this.callbackOnInit) {
                paramsOwl['onInitialize'] = _this.callbackOnInit;
            }

            _this.container.owlCarousel(paramsOwl);

            if(_this.callbackOnChange) {
                _this.container.on('changed.owl.carousel', function(e) {
                     _this.callbackOnChange(e);
                });
            };

            var prevCustomButton = false;
            if(_this.container.data('customPrevButton') != '') {
                prevCustomButton = _this.container.data('customPrevButton');
                $(prevCustomButton).on('click', function(){
                    _this.container.trigger("prev.owl.carousel");
                });
            };

            var nextCustomButton = false;
            if(_this.container.data('customNextButton') != '') {
                nextCustomButton = _this.container.data('customNextButton');
                $(nextCustomButton).on('click', function(){
                    _this.container.trigger("next.owl.carousel");
                });
            };






        }

        return OwlCarousel2;
    });
}
