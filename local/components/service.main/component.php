<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;


if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}

if ($this->StartResultCache())
{
	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFile = array();

	$rsSection = \CIBlockSection::GetList(
		array("SORT" => "ASC"),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y",
			"DEPTH_LEVEL" => 1
		),
		false,
		array("ID", "NAME", "SECTION_PAGE_URL", "DESCRIPTION", "DETAIL_PICTURE", "UF_SHORT_DESCRIPTION"));
	while ($arSection = $rsSection->GetNext())
	{
		$arFile[] = $arSection["DETAIL_PICTURE"];
		$arResult["ITEMS"][] = $arSection;
	}

	$arResult["IMAGES"] = Helper::getImages($arFile);

	$this->IncludeComponentTemplate();
}