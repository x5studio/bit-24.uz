<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); 
global $APPLICATION;

$cacheId = 'menu_iblock_3';
$cache_dir = SITE_ID . '/bitrix/menu/topmenu';
$obCache = new CPHPCache;

if ($obCache->InitCache(3600, $cacheId, $cache_dir)) {
    $aMenuLinksExt = $obCache->GetVars();
}
elseif ($obCache->StartDataCache()) {
    $siteIdField = CUserTypeEntity::GetList(['ID' => 'ASC'], ['ENTITY_ID' => 'IBLOCK_3_SECTION', 'FIELD_NAME' => 'UF_RELATED_SITE'])->Fetch()['ID'];

    $siteId = CUserFieldEnum::GetList([], ['USER_FIELD_ID' => $siteIdField, 'XML_ID' => SITE_ID])->Fetch()['ID'];

    $rsItems = CIBlockSection::GetList(
        ['SORT' => 'ASC'],
        [
            'IBLOCK_ID' => 3,
            'ACTIVE' => 'Y',
            'UF_RELATED_SITE' => $siteId
        ],
        false,
        ['ID', 'NAME', 'CODE', 'SECTION_PAGE_URL', 'UF_RELATED_SITE', 'UF_ICON_MENU', 'UF_CLASS_ICON']
    );

    while($arItem = $rsItems->GetNext()) {
        $menuItem = [$arItem['NAME'], $arItem['SECTION_PAGE_URL'], [], [], ""];

        if (!empty($arItem['UF_ICON_MENU'])) {
            $menuItem[3]['ICON'] = CFile::GetPath($arItem['UF_ICON_MENU']);
        }

        if (!empty($arItem['UF_CLASS_ICON'])) {
            $menuItem[3]['ICON_CLASS'] = $arItem['UF_CLASS_ICON'];
        }

        $aMenuLinksExt[] = $menuItem;
    }

    global $CACHE_MANAGER;
    $CACHE_MANAGER->StartTagCache($cache_dir);
    $CACHE_MANAGER->RegisterTag('iblock_id_3');
    $CACHE_MANAGER->RegisterTag('bitrix:menu');
    $CACHE_MANAGER->EndTagCache();
    $obCache->EndDataCache($aMenuLinksExt);
}
else {
    $aMenuLinksExt = [];
}

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>