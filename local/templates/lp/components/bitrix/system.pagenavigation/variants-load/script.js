$(document).on("onMainInit ajaxFilter.send", function () {
    require(['Controls/AjaxElementsLoader', 'Controls/Paging/PagingStoreSingle', 'Controls/Paging/InlinePaging'], function (AjaxElementsLoader, PagingStoreSingle, InlinePaging) {
        $('.j-pagination-content').each(function(){
            var item = $(this);
            if(!item.data('clientsLoader'))
            {
                var clientsLoader = new AjaxElementsLoader({
                    root : this, // ������ (ul)
                    elementsShower : '#shmr-link', // ������ ��� ������
                    showCount : false,
                    elementsHider : '#hdmr-link',
                    processImg : '.elem-loader',
                    controlsBox : '.show-more',
                    elLoadedCSS: '.added-page'
                });

                clientsLoader.init();

                item.data('clientsLoader', clientsLoader);
            }
        });

        var obPagingStore = new PagingStoreSingle();
        var obInlinePaging = new InlinePaging(obPagingStore, {
            'root': $('.pagination')
        });

        obInlinePaging.init();
    });

});



