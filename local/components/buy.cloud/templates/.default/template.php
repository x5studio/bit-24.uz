<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <label for="price">Цена за месяц при покупке</label>
            <select class="form-control" id="cloud_price">
                <option data-class="PRICEFORMONTH">на 1 месяц</option>
                <option data-class="PRICEFORTHREEMONTHS">на 3 месяца</option>
                <option data-class="PRICEFORHALFAYEAR">на полгода</option>
                <option data-class="PRICEFORYEAR">на год</option>
                <option data-class="PRICEFORTWOYEARS">на 2 года</option>
            </select>
        </div>
    </div>
</div>
<script>
$(function() {
    $(document).on('change','select#cloud_price',function(){
        let month_type = $(this).val();
        let month_value = "";
        switch (month_type){
            case "на 1 месяц":
                month_value = "PRICEFORMONTH";
                break;
            case "на 3 месяца":
                month_value = "PRICEFORTHREEMONTHS";
                break;
            case "на полгода":
                month_value = "PRICEFORHALFAYEAR";
                break;
            case "на год":
                month_value = "PRICEFORYEAR";
                break;
            case "на 2 года":
                month_value = "PRICEFORTWOYEARS";
                break;
        }

        let prices_box = $(".prices_box");
        for(let i=0; i<prices_box.length; i++){
            let set_value = $(prices_box[i]).find("span." + month_value).html();
            $(prices_box[i]).siblings('.additional_wrapper').html(set_value);
        }
    });

    $('select#cloud_price option[data-class="<?=$arParams['ACTIVE_PRICE']?>"]').prop('selected', true);
    $('select#cloud_price').change();
});
</script>
<div class="row">
<? foreach ($arResult["ITEMS"] as $arItem): ?>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="cloud1 on-main block-buy-cloud">
			<h2><?= $arItem["NAME"] ?></h2>
			<?= $arItem["PREVIEW_TEXT"] ?>
            <?if($arItem["PROPERTY_PRICE_NEW_VALUE"] == ""):?>
			    <span class="additional_wrapper">
                    <?= $arItem["PROPERTY_PRICE_VALUE"] ?>
                </span>
                <div class="prices_box">
                    <span class="additional PRICEFORMONTH">
                        <?= $arItem['PROPERTY_PRICEFORMONTH_VALUE'] ? $arItem['PROPERTY_PRICEFORMONTH_VALUE'] : $arItem["PROPERTY_PRICE_VALUE"]?>
                        <?if($arItem['PROPERTY_PRICEFORMONTH_VALUE'] != $arItem["PROPERTY_PRICE_VALUE"] && $arItem['PROPERTY_PRICEFORMONTH_VALUE'] != ""):?>
                            <br/><span class="old_cena"><?=$arItem["PROPERTY_PRICE_VALUE"]?></span>
                        <?endif?>
                    </span>
                    <span class="additional PRICEFORTHREEMONTHS">
                        <?= $arItem['PROPERTY_PRICEFORTHREEMONTHS_VALUE'] ? $arItem['PROPERTY_PRICEFORTHREEMONTHS_VALUE'] : $arItem["PROPERTY_PRICE_VALUE"]?>
                        <?if($arItem['PROPERTY_PRICEFORTHREEMONTHS_VALUE'] != $arItem["PROPERTY_PRICE_VALUE"] && $arItem['PROPERTY_PRICEFORTHREEMONTHS_VALUE'] != ""):?>
                            <br/><span class="old_cena"><?=$arItem["PROPERTY_PRICE_VALUE"]?></span>
                        <?endif?>
                    </span>
                    <span class="additional PRICEFORHALFAYEAR">
                        <?= $arItem['PROPERTY_PRICEFORHALFAYEAR_VALUE'] ? $arItem['PROPERTY_PRICEFORHALFAYEAR_VALUE'] : $arItem["PROPERTY_PRICE_VALUE"]?>
                        <?if($arItem['PROPERTY_PRICEFORHALFAYEAR_VALUE'] != $arItem["PROPERTY_PRICE_VALUE"] && $arItem['PROPERTY_PRICEFORHALFAYEAR_VALUE'] != ""):?>
                            <br/><span class="old_cena"><?=$arItem["PROPERTY_PRICE_VALUE"]?></span>
                        <?endif?>
                    </span>
                    <span class="additional PRICEFORYEAR">
                        <?= $arItem['PROPERTY_PRICEFORYEAR_VALUE'] ? $arItem['PROPERTY_PRICEFORYEAR_VALUE'] : $arItem["PROPERTY_PRICE_VALUE"]?>
                        <?if($arItem['PROPERTY_PRICEFORYEAR_VALUE'] != $arItem["PROPERTY_PRICE_VALUE"] && $arItem['PROPERTY_PRICEFORYEAR_VALUE'] != ""):?>
                            <br/><span class="old_cena"><?=$arItem["PROPERTY_PRICE_VALUE"]?></span>
                        <?endif?>
                    </span>
                    <span class="additional PRICEFORTWOYEARS">
                        <?= $arItem['PROPERTY_PRICEFORTWOYEARS_VALUE'] ? $arItem['PROPERTY_PRICEFORTWOYEARS_VALUE'] : $arItem["PROPERTY_PRICE_VALUE"]?>
                        <?if($arItem['PROPERTY_PRICEFORTWOYEARS_VALUE'] != $arItem["PROPERTY_PRICE_VALUE"] && $arItem['PROPERTY_PRICEFORTWOYEARS_VALUE'] != ""):?>
                            <br/><span class="old_cena"><?=$arItem["PROPERTY_PRICE_VALUE"]?></span>
                        <?endif?>
                    </span>
                </div>
            <?else:?>
                <span style="padding-bottom: 0; line-height: 10px;"><s><?= $arItem["PROPERTY_PRICE_VALUE"] ?></s></span><br />
                <span style="padding-top:0;line-height: 10px; color: #324abd; font-weight: bold;"><?= $arItem["PROPERTY_PRICE_NEW_VALUE"] ?></span>
            <?endif;?>
			<button type="button" class="btn btn-buy center-block" data-target="#advanced" data-form-field-type="Купить облако : <?=
			$arItem["NAME"] ?>" data-toggle="modal">Оставить заявку</button>
			<!-- <a href="#" data-target="#advanced" data-form-field-type="Заказать демонстрацию : <?=$arItem["NAME"] ?>" data-toggle="modal">Заказать демонстрацию</a>-->
		</div>
	</div>
<? endforeach; ?>
</div>
<style>
    span.additional{display:none;}
    span.old_cena{text-decoration: line-through!important;font-size: 26px!important;color: #ccc!important;padding-left: 0;line-height:0px;}
</style>
