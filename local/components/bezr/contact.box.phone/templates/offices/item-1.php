<div class="box-offices box-office-single">
	<?

	$APPLICATION->IncludeComponent("vitams:city.dropdown", "city_simple",
		Array(
			"IBLOCK_TYPE" => IBT_CONTACTS,
			"IBLOCK_ID" => IB_CONTACTS,
			"SECTION_CODE" => "",
			"SECTION_URL" => "",
			"COUNT_ELEMENTS" => "Y",
			"TOP_DEPTH" => "1",
			"SECTION_FIELDS" => "",
			"FILTER_NAME" => "cityFilter",
			"SECTION_USER_FIELDS" => "",
			"ADD_SECTIONS_CHAIN" => "Y",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_NOTES" => "",
			"CACHE_GROUPS" => "Y",
			"LINK_IN_BASEURL" => "Y",
			"LINK_WITH_SUBDOMAIN" => "Y",
			"SEF_MODE" => "N",
			"SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
			"HTML_LIST_ID" => "header-city-list-inner",
			"HTML_TITLE" => '<div class="box-title h2"><div class="short">' . \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_CONTACTS') . '</div><div class="full">' . \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_OFFICE_IN') . ' <a href="##HTML_LIST_ID#" class="city j-city"><b>#NAME_DATIVE#</b></a></div></div>',
			"BY_CACHE_SUBDOMAIN_PAGE" => $_SESSION["REGION"]["CODE"],
			"SHOW_MOSCOW" => $_SESSION['SHOW_MOSCOW'],
			"COUNTRY_CODE" => $_SESSION['GEOIP']['country_code'],
			'UI_CITY_STRONG' => $arParams['CITY_DROPDOWN_UI_CITY_STRONG'],
			'UI_CITY_SPACE' => $arParams['CITY_DROPDOWN_UI_CITY_SPACE'],
		)
	);?>

	<div class="box-content">
		<?$arItem = array_shift($arResult["ITEMS"]);?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'],
				CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => PB\Main\Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<?if($_SESSION["REGION"]["CODE"]=="chelyabinsk") {
				$call_phone = "call_phone_1";
			}?>
				<ul class="box-offices-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-item-id="<?=$arItem['ID']?>">
					<li class="office-name">
                        <?if($arItem["CODE"] == 'ckk'):?>
                            <?=$arItem['NAME']?>
                        <?else:?>
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
                        <?endif;?>
                    </li>
					<li class="office-phone <?=$call_phone?>"><?=$arItem['PROPERTIES']['phone']['VALUE']?></li>
					<?foreach($arItem['PROPERTIES']['WORK_TIME']['VALUE'] as $k=>$TIME):?>
					<li class="office-phone-icn"><?=$TIME?> <?=$arItem['PROPERTIES']['WORK_TIME']['DESCRIPTION'][$k]?></li>
					<?endforeach;?>
					<?php
					$email = ($arItem['PROPERTIES']['VIEW_EMAIL']['VALUE'])?$arItem['PROPERTIES']['VIEW_EMAIL']['VALUE']:$arItem['PROPERTIES']['email']['VALUE'];
					?>
					<?if($email):?>
					<li class="office-email"><a href="mailto://<?=$email?>"><?=$email?></a></li>
					<?endif;?>
                    <?if($arParams['HIDE_CALLBACK'] != 'Y'):?>
                    <li class="link call-ord"><div class="dotted"><?php echo \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_ORDER_CALL'); ?></div></li>
                    <?endif;?>
                    <script>
                        $('.box-offices .box-content > a').click(function(event){
                            var target = $(event.target);
                            if(target.is('li.link')){
                                event.preventDefault();
                            }
                        });
                    </script>
				</ul>
			</a>
	</div>
</div>