<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult)):
	$isShow = false; $isOpen = false; $cnt = 1;
	?>
	<ul class="nav navbar-nav">
		<? foreach ($arResult as $arItem)
		{
			if ($arItem["DEPTH_LEVEL"] == 1)
			{
				$isShow = $arItem["SELECTED"];
			} elseif ($isShow && $arItem["IS_PARENT"])
			{
				if($isOpen):?>
							</ul>
						</div>
					</div>
				</li>
				<?endif;?>
				<li class="panel panel-default dropdown">
					<a data-toggle="collapse" href="#dropdown-lvl<?=$cnt?>"<?if($arItem["SELECTED"])
						:?> aria-expanded="true"<?endif;?>><?=$arItem["TEXT"]?></a>
					<!-- Dropdown level 1 -->
					<div id="dropdown-lvl<?=$cnt++?>" class="panel-collapse collapse<?if($arItem["SELECTED"])
						:?> in<?endif;?>">
						<div class="panel-body">
							<ul class="nav navbar-nav">
				<?
				$isOpen = true;
			} elseif ($isShow && !$arItem["IS_PARENT"])
			{
				?>
				<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
				<?
			}
		}
		if($isOpen):?>
			</ul>
			</div>
			</div>
			</li>
		<?endif;?>
	</ul>
<? endif ?>