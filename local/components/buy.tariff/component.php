<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["SHOW_ELEMENT_NAME"] = ($arParams["SHOW_ELEMENT_NAME"]) ? (bool) $arParams["SHOW_ELEMENT_NAME"] : false;
$arParams["EXCLUDE_PROPS"] = ($arParams["EXCLUDE_PROPS"]) ? $arParams["EXCLUDE_PROPS"] : array();
$arParams["ADDITIONAL_PRICES"] = ($arParams["ADDITIONAL_PRICES"]) ? $arParams["ADDITIONAL_PRICES"] : array();
$arParams["BUTTON_TEXT"] = ($arParams["BUTTON_TEXT"]) ? $arParams["BUTTON_TEXT"] : 'Купить';
$arParams["BUTTON_IS_LINK"] = ($arParams["BUTTON_IS_LINK"]) ? $arParams["BUTTON_IS_LINK"] : false;
$arParams["ALL_TARIFFS_TITLE"] = ($arParams["ALL_TARIFFS_TITLE"]) ? $arParams["ALL_TARIFFS_TITLE"] : 'Тарифы';
$arParams["CUSTOM_LABELS"] = ($arParams["CUSTOM_LABELS"]) ? $arParams["CUSTOM_LABELS"] : array();

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}

if ($this->StartResultCache())
{
	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$rsSection = \CIBlockSection::GetList(
		array("SORT" => "ASC"),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y"
		),
		false,
		array("ID", "NAME", "UF_*")
	);
	while ($arSection = $rsSection->GetNext())
		$arResult["SECTION"][$arSection["ID"]] = $arSection;

    $filterHelper = new \PB\Main\FilterHelper();
	$rsElement = \CIBlockElement::GetList(
		array("SORT" => "ASC"),
        $filterHelper->make(
            array(
                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                "ACTIVE" => "Y"
            ),
            array(
                'IS_CITY' => 'Y',
            )
        ),
		false,
		false,
		array("ID", "NAME", "IBLOCK_SECTION_ID", "IBLOCK_ID"));
	while ($obElement = $rsElement->GetNextElement())
	{
		$arElement = $obElement->GetFields();
		$arElement["PROPERTY"] = $obElement->GetProperties();
		$arResult["SECTION"][$arElement["IBLOCK_SECTION_ID"]]["ITEMS"][] = $arElement;
	}

	$rsProperty = \CIBlockProperty::GetList(
		array("SORT" => "ASC"),
		array("ACTIVE" => "Y", "IBLOCK_ID" => $arParams["IBLOCK_ID"])
	);
	while($arProperty = $rsProperty->GetNext())
		$arResult["PROPERTY"][] = $arProperty;

	$this->IncludeComponentTemplate();
}