<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Клиенты. Связанный список",
	"DESCRIPTION" => "Блог. Связанный список",
	"ICON" => "/images/news_line.gif",
	"SORT" => 110,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "client",
			"NAME" => "Клиенты"
		)
	),
);

?>