<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION, $USER;
?>



<?if ($GLOBALS['IS_BLOG_ROOT_OR_SECTION']):?>
    </main>
<?endif;?>


<?
if (!CSite::InDir('/uslugi/kupit-licenziju-bitrix24/') && (!defined('ERROR_404') || ERROR_404 != 'Y')) {
    if (CSite::InDir('/dobavlenie-lidov/')) {
        $WEB_FORM_ID = 4;
        $STRONG_SHOW_CITY = 'Y';
        $STRONG_SHOW_OFFICE = 'Y';
    } elseif (CSite::InDir('/dobavlenie-lidov2/')) {
        $WEB_FORM_ID = 5;
        $STRONG_SHOW_CITY = 'Y';
        $STRONG_SHOW_OFFICE = 'Y';
    } else {
        $WEB_FORM_ID = 3;
        $STRONG_SHOW_CITY = 'N';
        $STRONG_SHOW_OFFICE = 'N';
    }
    if (!CSite::InDir('/tipy-vnedrenij-bitrix24/ekspress-vnedrenie-za-7-dnej/') && !CSite::InDir('/oblachnaya-versiya-bitrix24/') && !CSite::InDir('/korobochnaya-versiya-bitrix24/')) {
        if (!$GLOBALS['IS_BLOG_ROOT_OR_SECTION'] && !$GLOBALS['IS_LANDING_B24']) {
            $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "request", Array(
                "WEB_FORM_ID" => $WEB_FORM_ID,  // ID веб-формы
                "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
                "COMPANY_IBLOCK_ID" => IB_CONTACTS,
                "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
                "EMAIL_QUESTION_CODE" => "EMAIL",
                "CITY_QUESTION_CODE" => "CITY",
                "OFFICE_QUESTION_CODE" => "OFFICE",
                "TO_QUESTION_RESIPIENTS" => "",
                "EMAIL_ONLY_FROM_RESIPIENTS" => "",
                "PRODUCT_QUESTION_CODE" => "PRODUCT",
                "TO_QUESTION_CODE" => "TO",
                "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
                "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
                "SEF_MODE" => "N",  // Включить поддержку ЧПУ
                "CACHE_TYPE" => "A",  // Тип кеширования
                "CACHE_TIME" => "3600",  // Время кеширования (сек.)
                "LIST_URL" => "",  // Страница со списком результатов
                "EDIT_URL" => "",  // Страница редактирования результата
                "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
                "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
                "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
                "VARIABLE_ALIASES" => array(
                    "WEB_FORM_ID" => "WEB_FORM_ID",
                    "RESULT_ID" => "RESULT_ID",
                ),
                "ALERT_ADD_SHARE" => "N",
                "HIDE_PRIVACY_POLICE" => "Y",
                "BTN_CALL_FORM" => ".btn_order",
                "STRONG_SHOW_CITY" => $STRONG_SHOW_CITY,
                "STRONG_SHOW_OFFICE" => $STRONG_SHOW_OFFICE,
                "HIDE_FIELDS" => array(
                    0 => "CITY",
                    1 => "OFFICE",
                ),
                "COMPONENT_MARKER" => "request",
                "SHOW_FORM_DESCRIPTION" => "N",
                "MESS" => array(
                    "THANK_YOU" => "Спасибо, Ваша заявка принята.",
                    "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
                    "TITLE_FORM" => "Оставить заявку",
                )
            ),
                false,
                array(
                    "HIDE_ICONS" => "Y"
                )
            );
        }
    }
}
?>
<section class="footer">
	<!-- top-footer -->
	<div class="top-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 tfooter1">
					<div class="col-md-12 col-sm-12<?/* hidden-xs*/?> footimg">
						<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/wlogo.png" class="pull-left" alt="">
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 footbtn">
						<button type="button" onclick="yaCounter45927957.reachGoal('feedback-click'); gtag('event', 'click', {'event_category': 'form', 'event_label': 'click'}); return true;" class="btn btn-default" data-toggle="modal"
										data-target="#advanced" data-form-field-type="Оставить заявку">Оставить заявку
						</button>
					</div>
					<div class="col-md-12 col-sm-12<?/* hidden-xs*/?> contacts pull-left">
						<p>Контактный телефон:<br><span><?$APPLICATION->ShowViewContent('office_phone');?></span></p>
                        <div class="contacts__social-links">
                            <a href="https://vk.com/studiobitru" target="_blank"><i class="fa fa-vk" aria-hidden="true"></i></a>
                            <a href="https://www.facebook.com/studiobit/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6<?/* hidden-xs*/?> tfooter2">
					<? $APPLICATION->IncludeComponent("bitrix:menu", "service.footer", Array(
						"ALLOW_MULTI_SELECT" => "N",  // Разрешить несколько активных пунктов одновременно
						"CHILD_MENU_TYPE" => "",  // Тип меню для остальных уровней
						"DELAY" => "N",  // Откладывать выполнение шаблона меню
						"MAX_LEVEL" => "1",  // Уровень вложенности меню
						"MENU_CACHE_GET_VARS" => array(  // Значимые переменные запроса
							0 => "",
						),
						"MENU_CACHE_TIME" => "3600",  // Время кеширования (сек.)
						"MENU_CACHE_TYPE" => "A",  // Тип кеширования
						"MENU_CACHE_USE_GROUPS" => "N",  // Учитывать права доступа
						"ROOT_MENU_TYPE" => "service",  // Тип меню для первого уровня
						"USE_EXT" => "Y",  // Подключать файлы с именами вида .тип_меню.menu_ext.php
					),
						false
					); ?>
				</div>
				<div class="col-md-3 col-sm-6<?/* hidden-xs*/?>s tfooter3">
					<? $APPLICATION->IncludeComponent(
						"expertise.menu",
						"footer",
						Array(
							"CACHE_TIME" => "360000",
							"CACHE_TYPE" => "A",
							"IBLOCK_ID" => "3"
						)
					); ?>
				</div>
				<div class="col-md-3 col-sm-6<?/* hidden-xs*/?> tfooter4">
					<? $APPLICATION->IncludeComponent("bitrix:menu", "type.footer", Array(
						"ALLOW_MULTI_SELECT" => "N",  // Разрешить несколько активных пунктов одновременно
						"CHILD_MENU_TYPE" => "",  // Тип меню для остальных уровней
						"DELAY" => "N",  // Откладывать выполнение шаблона меню
						"MAX_LEVEL" => "1",  // Уровень вложенности меню
						"MENU_CACHE_GET_VARS" => array(  // Значимые переменные запроса
							0 => "",
						),
						"MENU_CACHE_TIME" => "3600",  // Время кеширования (сек.)
						"MENU_CACHE_TYPE" => "A",  // Тип кеширования
						"MENU_CACHE_USE_GROUPS" => "N",  // Учитывать права доступа
						"ROOT_MENU_TYPE" => "type",  // Тип меню для первого уровня
						"USE_EXT" => "N",  // Подключать файлы с именами вида .тип_меню.menu_ext.php
					),
						false
					); ?>
					<span class="col-md-12 col-sm-12 hidden-xs h4"><a href="/vozmozhnosti-bitrix24/" title="Возможности Битрикс24">Возможности
							Битрикс24</a></span>
					<span class="col-md-12 col-sm-12 hidden-xs h4"><a href="/blog/" title="Блог">Блог</a></span>
					<span class="col-md-12 col-sm-12 hidden-xs h4"><a href="/nashi-klienty/" title="Наши клиенты">Наши клиенты</a></span>
					<span class="col-md-12 col-sm-12 hidden-xs h4"><a href="/o-kompanii/" title="О компании">О компании</a></span>
					<span class="col-md-12 col-sm-12 hidden-xs h4"><a href="/kontakty/" title="Контакты">Контакты</a></span>
				</div>
			</div>
		</div>
	</div>
	<!-- bottom-footer -->
	<div class="bottom-footer">
		<div class="container">
			<div class="row vertical-align">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/___include___/footer.copyright.php", Array(), Array("MODE" => "html")); ?>
				</div>
				<div class="col-md-6 col-sm-6<?/* hidden-xs*/?>">
					<div class="social_block pull-right">
						<? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/___include___/footer.social.php", Array(), Array("MODE" => "html")); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
		"WEB_FORM_ID" => FORM_FEEDBACK,	// ID веб-формы
		"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
		"COMPANY_IBLOCK_ID" => IB_CONTACTS,
		"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
        "PHONE_QUESTION_CODE" => "PHONE",
		"EMAIL_QUESTION_CODE" => "EMAIL",
		"CITY_QUESTION_CODE" => "CITY",
		"OFFICE_QUESTION_CODE" => "OFFICE",
		"TO_QUESTION_RESIPIENTS" => "",
		"EMAIL_ONLY_FROM_RESIPIENTS" => "",
		"PRODUCT_QUESTION_CODE" => "PRODUCT",
		"TO_QUESTION_CODE" => "TO",
		"IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
		"USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
		"SEF_MODE" => "N",	// Включить поддержку ЧПУ
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"LIST_URL" => "",	// Страница со списком результатов
		"EDIT_URL" => "",	// Страница редактирования результата
		"SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
		"CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
		"CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		),
		"ALERT_ADD_SHARE" => "N",
		"HIDE_PRIVACY_POLICE" => "Y",
		"BTN_CALL_FORM" => ".btn_order, .ordering .btn, .footbtn .btn, .intro_descr .btn, .price-table__wrapper .btn, .topmenu-mobile__cont-but button, .tariffs-wrapper .btn-buy",
		"HIDE_FIELDS" => array(
			0 => "CITY",
			1 => "OFFICE",
			2 => "TYPE",
		),
		"COMPONENT_MARKER" => "feedform",
		"SHOW_FORM_DESCRIPTION" => "N",
		"MESS" => array(
			"THANK_YOU" => "Спасибо, Ваша заявка принята.",
			"WAIT_CALL" => "Мы свяжемся с Вами в течение 2-х часов.",
			"TITLE_FORM" => "Заказать услугу",
		)
	),
		false,
	array(
		"HIDE_ICONS" => "Y"
	)
	);
?>

<?if ($GLOBALS['IS_BLOG_ROOT_OR_SECTION'] || $GLOBALS['IS_LANDING_B24']):?>
    <script src="<?=SITE_TEMPLATE_PATH?>/assets_new/js/vendor.min.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/assets_new/js/main.min.js"></script>
<?endif;?>

<!-- <script>
       (function(w,d,u){
               var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
               var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
       })(window,document,'https://cdn.bitrix24.ru/b12737516/crm/site_button/loader_1_ru61fi.js');
</script> -->

<!-- calltouch -->

<script type="text/javascript">
(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n](function(){console.log(Date.now())});w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)};s.type="text/javascript";s.async=true;s.src="https://mod.calltouch.ru/init.js?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","u9a5cjls");
</script>

<script>
jQuery(document).on('click', 'form[name="FEEDBACK"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_1"]').val();
	var phone = m.find('input[name="form_text_2"]').val();
	var mail = m.find('input[name="form_text_3"]').val(); 
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Оставить заявку';
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
		window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>
<script>
jQuery(document).on('click', 'form[name="LICENCE"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_9"]').val();
	var phone = m.find('input[name="form_text_10"]').val();
	var mail = m.find('input[name="form_text_11"]').val(); 
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Не знаете какая лицензия вам нужна';
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>
<script>
jQuery(document).on('click', 'form[name="FEEDBACK_mVTWg"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_143"]').val(); 
	var phone = m.find('input[name="form_text_144"]').val();
	var mail = m.find('input[name="form_text_145"]').val();
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Купить Битрикс24';
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>
<script>
jQuery(document).on('click', 'form[name="REQUEST"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_17"]').val();
	var phone = m.find('input[name="form_text_18"]').val();
	var mail = m.find('input[name="form_text_19"]').val();
	var comment = m.find('textarea[name="form_textarea_25"]').val();
	var ct_site_id = '34885';
	var sub = 'Консультация эксперта';
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>

<script>
jQuery(document).on('click', 'form[name="CONSULTATION"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_74"]').val();
	var phone = m.find('input[name="form_text_75"]').val();
	var mail = m.find('input[name="form_text_76"]').val();
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Отраслевая экспертиза';
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>
<script>
jQuery(document).on('click', 'form[name="FEEDBACK_mZjwL"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_131"]').val();
	var phone = m.find('input[name="form_text_132"]').val();
	var mail = m.find('input[name="form_text_133"]').val();
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Директория «Обучение»';
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>
<script>
jQuery(document).on('click', 'form[name="FEEDBACK_uwvLL"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_119"]').val();
	var phone = m.find('input[name="form_text_120"]').val();
	var mail = m.find('input[name="form_text_121"]').val();
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Директория «Документирование»';
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>
<script>
jQuery(document).on('click', 'form[name="FEEDBACK_k571s"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_107"]').val();
	var phone = m.find('input[name="form_text_108"]').val();
	var mail = m.find('input[name="form_text_109"]').val();
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Директория «Консалтинг»';
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>
<script>
jQuery(document).on('click', 'form[name="FEEDBACK_4gnUy"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_95"]').val();
	var phone = m.find('input[name="form_text_96"]').val();
	var mail = m.find('input[name="form_text_97"]').val();
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Настройка Битрикс24';
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>
<script>
jQuery(document).on('click', 'form[name="EVENTS_APPLY"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var fio = m.find('input[name="form_text_71"]').val();
	var phone = m.find('input[name="form_text_62"]').val();
	var mail = m.find('input[name="form_text_72"]').val();
	if (/Имя/.test(fio)){ fio = ''; } if (/Телефон/.test(phone)){ phone = ''; } if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Запись на мероприятие';
	var ct_data = {
		fio: fio,
		phoneNumber: phone,
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!phone && !!fio;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>
<script>
jQuery(document).on('click', 'form[name="EVENTS_SUBSCRIBE"] input[type="submit"]', function() {
	var m = jQuery(this).closest('form');
	var mail = m.find('input[name="form_text_53"]').val();
	if (/E-mail/.test(mail)){ mail = ''; }
	var ct_site_id = '34885';
	var sub = 'Подписка на мероприятия';
	var ct_data = {
		subject: sub,
		email: mail,
		sessionId: window.call_value
	};
	var ct_valid = !!mail;
	console.log(ct_data,ct_valid);
	if (ct_valid && !window.ct_snd_flag){
	window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
		jQuery.ajax({
			url: 'https://api-node13.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
			dataType: 'json', type: 'POST', data: ct_data, async: false
		});
	}
});
</script>

<script type="text/javascript">
window.addEventListener('message', function(e) {
    var data = {}, 
        _ctreq = function(data, sid = 34885, nid = 13) {
            var request = window.ActiveXObject?new ActiveXObject("Microsoft.XMLHTTP"):new XMLHttpRequest(),
            post_data = Object.keys(data).reduce(function(a, k) {if(!!data[k]){a.push(k + '=' + encodeURIComponent(data[k]));}return a}, []).join('&'),
            url = 'https://api-node'+nid+'.calltouch.ru/calls-service/RestAPI/'+sid+'/requests/orders/register/';
            request.open("POST", url, true); request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); request.send(post_data);
        };
    try { data = JSON.parse(e.data); } catch (err){ } if(!data.action) return;
    if (data.action == 'event' && data.eventName == 'send'){
        var fio = !!data.value[0].LEAD_NAME ? data.value[0].LEAD_NAME : !!data.value[0].CONTACT_NAME ? data.value[0].CONTACT_NAME : '',
        phone = !!data.value[0].LEAD_PHONE ? data.value[0].LEAD_PHONE : !!data.value[0].CONTACT_PHONE ? data.value[0].CONTACT_PHONE : '',
        email = !!data.value[0].LEAD_EMAIL ? data.value[0].LEAD_EMAIL : !!data.value[0].CONTACT_EMAIL ? data.value[0].CONTACT_EMAIL : '',
        comment = !!data.value[0].DEAL_COMMENTS ? data.value[0].DEAL_COMMENTS : !!data.value[0].LEAD_COMMENTS ? data.value[0].LEAD_COMMENTS : '',
        sub = 'Открытые линии',
        ct_data = {fio: fio, phoneNumber: phone, email: email, comment: comment, subject: sub, sessionId: window.call_value};
        if (!!phone || !!email) _ctreq(ct_data);
    }
});
</script>

<script type='text/javascript'>
var _ctreq_jivo = function(sub) {
	var sid = '34885';
	var jc = jivo_api.getContactInfo(); var fio = ''; var phone = ''; var email = '';
	if (!!jc.client_name){fio = jc.client_name;} if (!!jc.phone){phone = jc.phone;} if (!!jc.email){email = jc.email;}
	var ct_data = { fio: fio, phoneNumber: phone, email: email, subject: sub, requestUrl: location.href, sessionId: window.call_value };
	var request = window.ActiveXObject?new ActiveXObject("Microsoft.XMLHTTP"):new XMLHttpRequest();
	var post_data = Object.keys(ct_data).reduce(function(a, k) {if(!!ct_data[k]){a.push(k + '=' + encodeURIComponent(ct_data[k]));}return a}, []).join('&');
	var url = 'https://api.calltouch.ru/calls-service/RestAPI/'+sid+'/requests/orders/register/';
	request.open("POST", url, true); request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); request.send(post_data);
}
window.jivo_onIntroduction = function() { _ctreq_jivo('JivoSite посетитель оставил контакты'); }
window.jivo_onCallStart = function() { _ctreq_jivo('JivoSite обратный звонок'); }
window.jivo_onOfflineFormSubmit = function() { _ctreq_jivo('JivoSite оффлайн заявка'); }
window.jivo_onMessageSent = function() { ct('goal','jivo_ms'); } 
</script>
<!-- calltouch -->


<div class="scroll-top__button">
	<span>&uarr;</span>
</div>
<script>

(function(){

	var toTopButton = document.querySelector('.scroll-top__button');

	function smoothScroll(){
		var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;
		if (currentScroll > 80) {
			window.requestAnimationFrame(smoothScroll);
			window.scrollTo (0,currentScroll - (currentScroll/5));
		}
	}

	function scrollControl() {
		var currentScroll = document.documentElement.scrollTop || document.body.scrollTop;

		if (currentScroll > 80) {
			toTopButton.classList.add('active');
		} else {
			toTopButton.classList.add('active');
		}
	}
	scrollControl();
	window.addEventListener('scroll', scrollControl);
	toTopButton.addEventListener('click', smoothScroll);

})();

</script>
</body>
</html>