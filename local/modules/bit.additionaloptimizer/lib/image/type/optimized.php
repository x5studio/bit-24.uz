<?

namespace Bit\AdditionalOptimizer\Image\Type;

use Bit\AdditionalOptimizer\Image\Optimizer\OptimizerChain;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Jpegoptim;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Jpegtran;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Optipng;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Magick;
use Bit\AdditionalOptimizer\Image\Optimizer\Optimizers\Pngquant;
use Bit\AdditionalOptimizer\Image\BinHelper;

class Optimized extends Base {

    public function createImage($additionalParams = []) {

        if(!$this->pathToImage) throw new \Exception("Original image not set");

        $optimizerChain = new OptimizerChain;

        $additionalParams = !$additionalParams ? $this->additionalParams : $additionalParams;

        if(isset($additionalParams['max_width']) || isset($additionalParams['max_height'])){
                    
            $maxParam = '-resize ';
            $maxParam .= isset($additionalParams['max_width']) ? (int)$additionalParams['max_width'] : '';
            $maxParam .= 'x';
            $maxParam .= isset($additionalParams['max_height']) ? (int)$additionalParams['max_height'] : '';
            $maxParam .= '\>';

            $optimizerChain->addOptimizer((new Magick([
                $maxParam,
            ]))->setBinaryPath(BinHelper::getExecBinPath('magick')));
        }

        // $optimizerChain->addOptimizer((new Jpegoptim([
        //     '--max=85',
        //     '--strip-all',
        //     '--all-progressive',
        // ]))->setBinaryPath(BinHelper::getExecBinPath('jpegoptim')));

        $optimizerChain->addOptimizer((new Jpegtran([
            '-copy none',
            '-optimize',
            //'-quality 85',
            '-progressive',
        ]))->setBinaryPath(BinHelper::getExecBinPath('jpegtran')));

        $optimizerChain->addOptimizer((new Pngquant([
            '--quality=85',
            '--force',
        ]))->setBinaryPath(BinHelper::getExecBinPath('pngquant')));


        $optimizerChain->addOptimizer((new Optipng([
            '-i0',
            '-o2',
        ]))->setBinaryPath(BinHelper::getExecBinPath('optipng')));


        $optimizerChain->optimize($this->pathToImage);

        $this->cache = [];


        return $this->getImageExist();

    }

}