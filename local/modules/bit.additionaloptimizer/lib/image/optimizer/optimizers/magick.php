<?
namespace Bit\AdditionalOptimizer\Image\Optimizer\Optimizers;

use Bit\AdditionalOptimizer\Image\Optimizer\Image;


class Magick extends BaseOptimizer
{
    public $binaryName = 'magick';

    public function canHandle(Image $image): bool
    {   
        //ImageMagick works with many image formats
        return true;
    }

    public function getCommand(): string
    {
        
        $OutputFile = $this->imagePath;

        foreach ($this->options as $key => $option) {
            
            if(preg_match('#\.[a-zA-Z]+$#', $option)) {

                $OutputFile = $option;
                unset($this->options[$key]);
                
            }

        }

        $optionString = implode(' ', $this->options);

        return "\"{$this->binaryPath}{$this->binaryName}\" " . escapeshellarg($this->imagePath) . " {$optionString}"
            .' '.$OutputFile;
    }
}
