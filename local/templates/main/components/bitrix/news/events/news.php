<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?php
$componentPath = MAIN_TEMPLATE_PATH.'/components/'.$component->GetRelativePath().'/'.$component->GetTemplateName();
\Bitrix\Main\Page\Asset::getInstance()->addCss($componentPath."/css/datepicker.min.css", true);
\Bitrix\Main\Page\Asset::getInstance()->addCss($componentPath."/css/select2.min.css", true);
\Bitrix\Main\Page\Asset::getInstance()->addCss($componentPath."/css/news.css", true);
\Bitrix\Main\Page\Asset::getInstance()->addJs($componentPath."/js/datepicker.min.js", true);
\Bitrix\Main\Page\Asset::getInstance()->addJs($componentPath."/js/select2.min.js", true);
\Bitrix\Main\Page\Asset::getInstance()->addJs($componentPath."/js/news.js", true);

if (!isset($_REQUEST['city'])) {
	$_REQUEST['city'] = $_SESSION['REGION']['ID'];
}
?>

<div class="events-list__wrapper">

    <?php
    $arOrder = array('DATE_ACTIVE_TO' => 'DESC');
    $arFilter = array(
        'IBLOCK_ID' => IB_EVENTS,
        'ACTIVE' => 'Y',
        'PROPERTY_FEATURED_VALUE' => 'Да'
    );
//	if (!empty($_REQUEST['city'])) {
//		$arFilter = \PB\Main\FilterHelper::make(
//			$arEventsFilter,
//			array('CITY' => $_REQUEST['city'])
//		);
//	}
    $arNavStartParams = array('nTopCount' => 1);
    $arSelectFields = array(
        'IBLOCK_ID',
        'ID',
        'PROPERTY_TYPE',
        'NAME',
        'DATE_ACTIVE_TO',
        'PROPERTY_ADDRESS',
        'PROPERTY_CITY',
        'PROPERTY_EMAIL',
        'DETAIL_PAGE_URL',
        'DETAIL_PICTURE',
    );
    $cacheTime = 3600000;
    $cacheId = serialize(array_merge($arOrder, $arFilter, $arNavStartParams, $arSelectFields));
    $cacheDir = 'featured_events';
    $cache = \Bitrix\Main\Data\Cache::createInstance();

    if ($cache->initCache($cacheTime, $cacheId, $cacheDir))
    {
        $vars = $cache->getVars();
        $featuredEvent = $vars['featuredEvent'];
    }
    elseif ($cache->startDataCache())
    {
        $featuredEvent = CIBlockElement::GetList(
            $arOrder,
            $arFilter,
            false,
            $arNavStartParams,
            $arSelectFields
        )->GetNext();

        // Название города
        $contactManagerObj = \PB\Contacts\ContactManager::getInstance();
        $arCity = $contactManagerObj->getCityById($featuredEvent['PROPERTY_CITY_VALUE']);
        $featuredEvent['CITY_NAME'] = !empty($arCity) ? $arCity['NAME'] : null;

        if(!empty($_REQUEST['city']) && !is_array($_REQUEST['city'])){

            $arCountry = $contactManagerObj->getCountryByCityId($_REQUEST['city']);

            $_REQUEST['city'] = array(
                $_REQUEST['city'],
                $arCountry['ID']
            );
            //print_r($arCountry);print_r($_REQUEST['city']);
        }

        // Дата проведения
        $timestamp = MakeTimeStamp($featuredEvent['DATE_ACTIVE_TO']);
        $featuredEvent['EVENT_DAY'] = FormatDate("d", $timestamp);
        $featuredEvent['EVENT_MONTH'] = FormatDate("F", $timestamp);

        $cache->endDataCache(
            array(
                'featuredEvent' => $featuredEvent,
            )
        );
    }

    if (!empty($featuredEvent['ID'])): ?>
        <div class="events-list__featured">
            <div class="container">
                <div class="events-list__featured-type"><?php echo $featuredEvent['PROPERTY_TYPE_VALUE']; ?></div>
                <div class="events-list__featured-title"><?php echo $featuredEvent['NAME']; ?></div>
                <div class="events-list__featured-date">
                    <div class="events-list__featured-date-num"><?php echo $featuredEvent['EVENT_DAY']; ?></div>
                    <div class="events-list__featured-date-month"><?php echo $featuredEvent['EVENT_MONTH']; ?></div>
                </div>
                <?if($featuredEvent['CITY_NAME']):?><div class="events-list__featured-city">Место проведения: <br/><?php echo $featuredEvent['CITY_NAME']; ?></div><?endif?>
                <div class="events-list__featured-actions">
                <?if(MakeTimeStamp($featuredEvent['DATE_ACTIVE_TO']) >= time()):?><button type="button" class="events-list__featured-action-btn events-list__btn events-list__btn_pink event__feedback-modal-btn" data-form-field-title="<?php echo $featuredEvent['NAME']; ?>" data-form-field-to="<?php echo $featuredEvent['PROPERTY_EMAIL_VALUE']; ?>">Записаться</button><?endif;?>
                    <a href="<?php echo $featuredEvent['DETAIL_PAGE_URL']; ?>" class="events-list__featured-action-btn events-list__btn">Подробнее</a>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="events-list__filter-container">
        <div class="container">
            <div class="events-list__header">Расписание</div>
            <div id="WIDGET_EVENTS_FILTER"></div>
        </div>
    </div>
    <?php
    global $arEventsFilter;
    $arEventsFilter = array(
        'IBLOCK_ID' => IB_EVENTS,
        '>DATE_ACTIVE_TO' => new DateTime(),
    );
    $eventsDate = !empty($_REQUEST['date']) ? $_REQUEST['date'] : '';
    if (!empty($_REQUEST['type'])) {
        if ($_REQUEST['type'] == 'free') {
            $arEventsFilter['PROPERTY_PRICE'] = false;
        } else {
            $arEventsFilter['PROPERTY_TYPE'] = $_REQUEST['type'];
        }
    }
    if (!empty($_REQUEST['city'])) {

        
        $arEventsFilter = \PB\Main\FilterHelper::make(
            $arEventsFilter,
            array('PROPERTY_CITY' => $_REQUEST['city'])
        );
    }
    if (!empty($eventsDate)) {
        unset($arEventsFilter['>DATE_ACTIVE_TO']);
        $date = new \Bitrix\Main\Type\DateTime($eventsDate . ' 00:00:00', 'd.m.Y H:i:s');
        $arEventsFilter["><DATE_ACTIVE_TO"] = array($date->toString(), $date->add('1 day')->toString());
    }
    $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "events",
        Array(
            "CACHE_TIME" => "86400",
            "CACHE_TYPE" => "A",
            "NEWS_COUNT" => 6,
			"SORT_BY1" => "SORT",
			"SORT_ORDER1" => "ASC",
			"SORT_BY2" => "ID",
			"SORT_ORDER2" => "ASC",
            'FIELD_CODE' => array('NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'DATE_ACTIVE_TO', 'DETAIL_PAGE_URL'),
            'PROPERTY_CODE' => array('TYPE'),
            "DISPLAY_BOTTOM_PAGER" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => "blog",
            "PAGER_TITLE" => "Мероприятия",
            "IBLOCK_ID" => IB_EVENTS,
            'USE_FILTER' => 'Y',
            'FILTER_NAME' => 'arEventsFilter',
            'DISPLAY_SUBSCRIBE_FORM' => 'Y',
            'HIDE_APPLY_FORM' => 'Y',
            'DISPLAY_EVENTS_FILTER' => 'Y',
            'ACTIVE_DATE_FORMAT' => 'd F Y, D. H:i',
        ),
        $component
    );
    ?>

</div>

<?
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
    "WEB_FORM_ID" => FORM_EVENTS_APPLY,	// ID веб-формы
    "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
    "COMPANY_IBLOCK_ID" => IB_CONTACTS,
    "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
    "EMAIL_QUESTION_CODE" => "EMAIL",
    "CITY_QUESTION_CODE" => "CITY",
    "OFFICE_QUESTION_CODE" => "OFFICE",
    "TO_QUESTION_RESIPIENTS" => "",
    "EMAIL_ONLY_FROM_RESIPIENTS" => "",
    "PRODUCT_QUESTION_CODE" => "PRODUCT",
    "TO_QUESTION_CODE" => "TO",
    "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
    "USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
    "SEF_MODE" => "N",	// Включить поддержку ЧПУ
    "CACHE_TYPE" => "A",	// Тип кеширования
    "CACHE_TIME" => "3600",	// Время кеширования (сек.)
    "LIST_URL" => "",	// Страница со списком результатов
    "EDIT_URL" => "",	// Страница редактирования результата
    "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
    "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
    "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
    "VARIABLE_ALIASES" => array(
        "WEB_FORM_ID" => "WEB_FORM_ID",
        "RESULT_ID" => "RESULT_ID",
    ),
    "ALERT_ADD_SHARE" => "N",
    "HIDE_PRIVACY_POLICE" => "Y",
    "BTN_CALL_FORM" => ".event__feedback-modal-btn",
    "HIDE_FIELDS" => array(
        0 => "CITY",
        1 => "OFFICE",
        2 => "TYPE",
    ),
    "COMPONENT_MARKER" => "events_apply_form",
    "SHOW_FORM_DESCRIPTION" => "N",
    "MESS" => array(
        "THANK_YOU" => "Спасибо, Ваша заявка принята.",
        "WAIT_CALL" => "Мы свяжемся с Вами в течение 2-х часов.",
        "TITLE_FORM" => "Заказать услугу",
    )
),
    false,
    array(
        "HIDE_ICONS" => "Y"
    )
);
?>