require(['Controllers/CountersHelper'], function(CountersHelper) {
	window.CountersHelper = new CountersHelper();
	window.CountersHelper.counters = {
		yandex: window.yaCounter45927957
	}

	window.CountersHelper.goalsData = {
		'feedback_click': {
			'yandex': {
				code: 'feedback-click'
			},
			'google': {
				eventCategory: 'form',
				eventAction: 'click'
			}
		},
		'feedback_send': {
			'yandex': {
				code: 'feedback-send'
			},
			'google': {
				eventCategory: 'form',
				eventAction: 'send'
			}
		},
		'expert_send': {
			'yandex': {
				code: 'expert-send'
			},
			'google': {
				eventCategory: 'expert',
				eventAction: 'send'
			}
		},
		'podbor_licenzii': {
			'yandex': {
				code: 'podbor-licenzii'
			},
			'google': {
				eventCategory: 'podbor-licenzii',
				eventAction: 'send'
			}
		}
	}
});

if (typeof define === "function" && define.amd) {
	define(['jquery'], function() {

		var compositeFinished;
		(function() {
			var df = $.Deferred();
			if (window.frameCacheVars !== undefined) {
				BX.addCustomEvent("onFrameDataReceived", function() {
					df.resolve();
				});
			} else {
				df.resolve();
			}
			compositeFinished = function(fn) {
				if (typeof fn == 'function')
					df.done(fn);
				return df.promise();
			}
		})();

		function extend(Child, Parent) {
			var F = function() {
			};
			F.prototype = Parent.prototype;
			Child.prototype = new F();
			Child.prototype.constructor = Child;
			Child.superclass = Parent.prototype;
		}

		function getInitial(el, defName, eventTrigger, dataTrigger) {
			if (!(el instanceof jQuery)) {
				throw new Error("Invalid argument - is not jquery object");
			}
			if (typeof defName == 'undefined') {
				defName = 'initDeferred';
			}
			var defer = el.data(defName);
			if (!defer) {
				defer = $.Deferred();
				el.data(defName, defer);
			}
			el.on(eventTrigger, function() {
				defer.resolve();
			});
			if (el.data(dataTrigger)) {
				defer.resolve();
			}
			return defer;
		}

		function DeferredDataMng(container) {
			this.root = $(container);
			this.items = $();
			this.process = $.Deferred();
			this.pendings = [];
		}

		DeferredDataMng.prototype.move = function(item, toItem) {
			var itemTitle = toItem.data('title');
			if (typeof itemTitle != 'undefined') {
				var arTitle = itemTitle.split(':');
			}
			var toID = item.attr('to');
			if (typeof toID == 'undefined') {
				toID = item.data('to-deffered');
			}
			if (toID.length > 0 && toItem.length > 0) {
				try {
					if (typeof arTitle != 'undefined') {
						item.find('.' + arTitle[1]).text(arTitle[0]);
					}
					toItem.replaceWith(item);
				} catch (e) {
					return false;
				}
			}
		};

		DeferredDataMng.prototype.completeMove = function(item) {
			var o = this;
			for (var i = o.pendings.length; i--; ) {
				if (item.attr('to') == o.pendings[i].attr('to')) {
					o.pendings.splice(i, 1);
				}
			}
			if (o.pendings.length == 0) {
				o.process.resolve();
			}
		};

		DeferredDataMng.prototype.init = function() {
			var o = this;

			o.process.done(function() {
				o.root.remove();
			});

			this.root.children().each(function() {
				var item = $(this);
				var toID = item.attr('to');
				if (typeof toID == 'undefined') {
					toID = item.data('to-deffered');
				}
				if (typeof toID != 'string' || toID.length == 0) {
					return false;
				}
				o.items = o.items.add(item);
				var toItem = $('#' + toID);
				if (toItem.length > 0) {
					o.move(item, toItem);
				} else {
					o.pendings.push(item);
					$(document).on('lazyLoadReceived', function() {
						var toItem = $('#' + toID);
						o.move(item, toItem);
						o.completeMove(item);
					});
				}

			});

		};


		$(document).on("onMainInit", function(event) {

			var context = event.contextContainer;

			//Заполняем скрытый инпут форм
			var $sessid = $('form #sessid, form [name="sessid"]', context);
			if ($sessid.length > 0) {
				$sessid.val(BX.bitrix_sessid());
			}

			var $deffered = $('.j-deffered', context);
			if ($deffered.length > 0) {
				var obDeffered = new DeferredDataMng($deffered);
				obDeffered.init();
				context = $(context).add(obDeffered.items);
			}
		});

		$(function() {
			$.event.trigger({
				type: "onMainInit",
				initiatorEventType: 'ready',
				contextContainer: document
			});
		});
	})
}

$(document).ready(function() {
	if (typeof (BX.localStorage) != 'undefined') {
		arAllcorpOptions = BX.localStorage.get('arAllcorpOptions');
	}
});
$(document).ready(function() {
	require(['actual'], function() {
		$.fn.equalizeHeights = function(outer) {
			var maxHeight = this.map(function(i, e) {
				$(e).css('height', 'inherit');
				if (outer == true) {
					return $(e).actual('outerHeight');
				} else {
					return $(e).actual('height');
				}
			}).get();
			return this.height(Math.max.apply(this, maxHeight));
		}

		$.fn.sliceHeight = function(options) {
			function _slice(el) {
				if (options.slice) {
					for (var i = 0; i < el.length; i += options.slice) {
						$(el.slice(i, i + options.slice)).equalizeHeights(options.outer);
					}
				}
				if (options.lineheight) {
					el.each(function() {
						$(this).css("line-height", $(this).outerHeight() + "px");
					});
				}
			}

			var options = $.extend({
				slice: null,
				outer: false,
				lineheight: false
			}, options);
			var el = $(this);
			_slice(el);
			$(window).resize(function() {
				_slice(el);
			});
		}
	});



	// Responsive Menu Events
	var addActiveClass = false;

	$("#mainMenu li.dropdown > a, #mainMenu li.dropdown-submenu > a").on("click", function(e) {
		e.preventDefault();
		if ($(window).width() > 979)
			return;

		addActiveClass = $(this).parent().hasClass("resp-active");

		$("#mainMenu").find(".resp-active").removeClass("resp-active");

		if (!addActiveClass) {
			$(this).parents("li").addClass("resp-active");
		}
	});

	$(document).on('click', '.mega-menu .dropdown-menu', function(e) {
		e.stopPropagation()
	});

	$(document).on('click', '.mega-menu .dropdown-toggle.more-items', function(e) {
		e.preventDefault();
	});

	require(['flexslider'], function() {
		$(".flexslider:not(.thmb)").each(function() {
			var slider = $(this);
			var options;
			var defaults = {
				animationLoop: false,
				controlNav: false,
				directionNav: true
			}
			var config = $.extend({}, defaults, options, slider.data("plugin-options"));

			slider.flexslider(config).addClass("flexslider-init");

			if (config.controlNav)
				slider.addClass("flexslider-control-nav");

			if (config.directionNav)
				slider.addClass("flexslider-direction-nav");
		});
	});

	/* toggle */

	var $this = this,
			previewParClosedHeight = 25;

	$("section.toggle > label").prepend($("<i />").addClass("icon icon-plus"));
	$("section.toggle > label").prepend($("<i />").addClass("icon icon-minus"));
	$("section.toggle.active > p").addClass("preview-active");
	$("section.toggle.active > div.toggle-content").slideDown(350, function() {
	});

	$("section.toggle > label").click(function(e) {
		var parentSection = $(this).parent(),
				parentWrapper = $(this).parents("div.toogle"),
				previewPar = false,
				isAccordion = parentWrapper.hasClass("toogle-accordion");

		if (isAccordion && typeof (e.originalEvent) != "undefined") {
			parentWrapper.find("section.toggle.active > label").trigger("click");
		}

		parentSection.toggleClass("active");

		// Preview Paragraph
		if (parentSection.find("> p").get(0)) {
			previewPar = parentSection.find("> p");
			var previewParCurrentHeight = previewPar.css("height");
			previewPar.css("height", "auto");
			var previewParAnimateHeight = previewPar.css("height");
			previewPar.css("height", previewParCurrentHeight);
		}

		// Content
		var toggleContent = parentSection.find("> div.toggle-content");

		if (parentSection.hasClass("active")) {
			$(previewPar).animate({
				height: previewParAnimateHeight
			}, 350, function() {
				$(this).addClass("preview-active");
			});
			toggleContent.slideDown(350, function() {
			});
		} else {
			$(previewPar).animate({
				height: previewParClosedHeight
			}, 350, function() {
				$(this).removeClass("preview-active");
			});
			toggleContent.slideUp(350, function() {
			});
		}
	});

	/* /toogle */

	/* accordion */

	$('.accordion-head').on('click', function(e) {
		e.preventDefault();
		if ($(this).hasClass('accordion-open')) {
			$(this).addClass('accordion-close').removeClass('accordion-open');
		} else {
			$(this).addClass('accordion-open').removeClass('accordion-close');
		}
	})

	/* /accordion */

	/* progress bar */
	require(['flipclock', 'actual'], function() {
		$("[data-appear-progress-animation]").each(function() {
			var $this = $(this);
			$this.appear(function() {
				var delay = ($this.attr("data-appear-animation-delay") ? $this.attr("data-appear-animation-delay") : 1);
				if (delay > 1)
					$this.css("animation-delay", delay + "ms");
				$this.addClass($this.attr("data-appear-animation"));

				setTimeout(function() {
					$this.animate({
						width: $this.attr("data-appear-progress-animation")
					}, 1500, "easeOutQuad", function() {
						$this.find(".progress-bar-tooltip").animate({
							opacity: 1
						}, 500, "easeOutQuad");
					});
				}, delay);
			}, {accX: 0, accY: -50});
		});

		/* /progress bar */

		/* $("a[rel=tooltip]").tooltip();
		 $("span[data-toggle=tooltip]").tooltip();*/
	});

	$("select.sort").on("change", function() {
		location.href = $(this).val();
	})
	/*$(".side-menu .submenu > li.active > a").click(function(e){
	 e.preventDefault();
	 })*/
	setTimeout(function(th) {
		require(['actual'], function() {
			$('.catalog.group.list .item').each(function() {
				var th = $(this);
				if (th.find('.image').actual('height') > th.find('.text_info').height()) {
					th.find('.text_info .titles').height(th.find('.image').actual('height') - 61);
				}

			})
		});
	}, 700)

	/*item galery*/
	$('.thumbs .item a').on("click", function(e) {
		e.preventDefault();
		$('.thumbs .item').removeClass('current');
		$(this).closest('.item').toggleClass('current');
		$('.slides li' + $(this).attr('href')).addClass("current").siblings().removeClass("current");
	});

	var formMainStyled = $('form[name=main_styled_block_form]');
	var formId = formMainStyled.find('input[name=WEB_FORM_ID]').val();
	formMainStyled.attr("onsubmit", "yaCounter31646948.reachGoal('form'); ga('send', 'event', 'form', 'success', '" + formId + "'); return true;");

	function initializeDifferentList() {
		$('.j-city').each(function() {
			var item = $(this);
			require(['Controls/DifferentList'], function(DifferentList) {
				new DifferentList(item, ".width");
			});
		});
	}

	$(document).ready(function() {
		BX.ready(function() {
			initializeDifferentList();
		});
	});

	if ($('.j-leaping').length > 0) {
		require(["Controls/Leaping"], function(Leaping) {
			$('.j-leaping').each(function() {
				new Leaping(this);
			});
		});
	}

	$(document).ready(function() {
		$('.j-spoiler').each(function() {
			var item = $(this);
			require(['Controls/Spoiler'], function(Spoiler) {
				new Spoiler(item);
			});
		});
	});

	if ($('.nav-tabs-anchor.nav-tabs').length > 0) {
		require(["Controls/NavTabsAnchor"], function(Leaping) {
			$('.nav-tabs-anchor.nav-tabs').each(function() {
				new NavTabsAnchor(this);
			});
		});
	}

	$(window).resize(function() {
		$('html.bx-no-touch .jqmWindow.show').css('top', $(document).scrollTop() + ($(window).height() - $('.jqmWindow.show').height()) / 2 + 'px');
	})

	require(['jquery', 'maskedinput'], function($) {
		$.mask.definitions['d'] = '[0-9]';
	});

	// Javascript to enable link to tab
	var url = document.location.toString();
	if (url.match('#')) {

		if ($('a[href="#' + url.split('#')[1] + '"][data-toggle="tab"]').length > 0) {

			$('a[href="#' + url.split('#')[1] + '"][data-toggle="tab"]').tab('show');

			setTimeout(function() {
				window.scrollTo(0, 0);
			}, 0);
		}


	}

});
$(document).ready(function() {
	var $formPopup = $('.j-form-popup');
	var $forms = $('.j-form-popup, .j-form-page');
	if ($forms.length > 0) {
		$forms.each(function() {
			var formContext = $(this);
			formContext.on('jFormPage.init jFormPopup.init', function() {
				var $placeholders = $('[placeholder]', formContext);
				if ($placeholders.length > 0) {
					$placeholders.each(function() {
						var $item = $(this);
						require(['Controls/Placeholder'], function(Placeholder) {
							new Placeholder($item);
						});
					});
				}
			});
		});
	}

	if ($formPopup.length > 0) {
		$formPopup.each(function() {
			var $item = $(this);
			require(['Controls/Form/FormPopupMng'], function(FormPopupMng) {
				var obFormPopup = new FormPopupMng({
					root: $item
				});
				var globalName = obFormPopup.getGlobalName();
				window[globalName] = obFormPopup;
				obFormPopup.init();
				$item.trigger("jFormPopup.init");
			});
		});
	}

	if ($(".j-select").length > 0) {
		require(['cusel'], function() {
			var params = {
				changedEl: ".j-select",
				visRows: 6,
				scrollArrows: true
			};
			$('.j-select').each(function() {
				if ($(this).hasClass('j-select-smart') == false) {
					params.changedEl = '#' + $(this).attr('id');
					cuSel(params);
				}
			});
			$(document).data('jSelectInit', true);
			$(document).trigger("jSelect.init");
			//$('.j-select .cuselFrameRight').addClass('fa fa-caret-down');
		});
	} else {
		$(document).ready(function() {
			$(document).data('jSelectInit', true);
			$(document).trigger("jSelect.init");
		});
	}
	if ($(".j-select-smart").length > 0) {
		require(['cusel'], function() {
			$('.j-select-smart').each(function() {
				var $this = $(this);
				var options = $this.children();
				var params = {
					changedEl: '#' + $this.attr('id'),
					visRows: $this.data('visRows') || options.length || 6,
					scrollArrows: true
				};
				cuSel(params);
				//$('#' + $this.attr('id')).closest('.j-select-smart').find('.cuselFrameRight').addClass('fa fa-caret-down');
			});
		});
	}



	$('.j-form-page').each(function() {
		var item = $(this);
		require(['Controls/Form/FormPageMng'], function(FormPageMng) {
			var obFormPage = new FormPageMng({
				root: item
			});
			var globalName = obFormPage.getGlobalName();
			window[globalName] = obFormPage;
			obFormPage.init();
			item.trigger("jFormPage.init");
		});
	});

	$(document).on("jSelect.init", function() {
		$('.j-related-selects').each(function() {
			var item = $(this);
			require(['Controls/RelatedSelects'], function(RelatedSelects) {
				//       new RelatedSelects({
				//          select1: item
				//     });
			});
		});
	});


	$('.j-switch2').each(function() {
		var item = $(this);
		require(['Controls/Switch2'], function(Switch2) {
			new Switch2(item);
		});
	});
	$('.j-switch-multiple').each(function() {
		var item = $(this);
		require(['Controls/SwitchMultiple'], function(SwitchMultiple) {
			new SwitchMultiple(item);
		});
	});

	var $textScroll = $('.j-text-scroll');
	if ($textScroll.length > 0) {
		$textScroll.each(function() {
			var $item = $(this);
			require(['Controls/TextScroll'], function(TextScroll) {
				new TextScroll($item);
			});
		});
	}

});

function onLoadjqm(hash) {
	var name = $(hash.t).data('name'), top = $(document).scrollTop() + ($(window).height() - hash.w.height()) / 2 + 'px';
	top = (parseInt(top) < 0 ? 0 : top);
	$.each($(hash.t).get(0).attributes, function(index, attr) {
		if (/^data\-autoload\-(.+)$/.test(attr.nodeName)) {
			var key = attr.nodeName.match(/^data\-autoload\-(.+)$/)[1];
			var el = $('input[name="' + key.toUpperCase() + '"]');
			el.val($(hash.t).data('autoload-' + key)).attr('readonly', 'readonly');
			el.attr('title', el.val());
		}
	});
	if ($(hash.t).data('autohide')) {
		$(hash.w).data('autohide', $(hash.t).data('autohide'));
	}
	if (name == 'order_product') {
		$('input[name="PRODUCT"]').val($(hash.t).data('product')).attr('readonly', 'readonly').attr('title', $('input[name="PRODUCT"]').val());
	}
	if (name == 'question') {
		if ($(hash.t).data('product'))
			$('input[name="NEED_PRODUCT"]').val($(hash.t).data('product')).attr('readonly', 'readonly').attr('title', $('input[name="NEED_PRODUCT"]').val());
	}
	hash.w.addClass('show').css({'margin-left': '-' + hash.w.width() / 2 + 'px', 'top': top, 'opacity': 1});
}

function onHide(hash) {
	if ($(hash.w).data('autohide')) {
		eval($(hash.w).data('autohide'));
	}
	hash.w.css('opacity', 0).hide();
	hash.w.empty();
	hash.o.remove();
	hash.w.removeClass('show');
}

$.fn.jqmEx = function() {
	$(this).each(function() {
		var _this = $(this);
		var name = _this.data('name');
		if (!$('.' + name + '_frame').length) {
			var script = arAllcorpOptions['SITE_DIR'] + "ajax/form.php";
			$.each(_this.get(0).attributes, function(index, attr) {
				if (/^data\-param\-(.+)$/.test(attr.nodeName)) {
					var key = attr.nodeName.match(/^data\-param\-(.+)$/)[1];
					if (script == arAllcorpOptions['SITE_DIR'] + "ajax/form.php") {
						script += '?';
					} else {
						script += '&';
					}
					script += key + '=' + _this.data('param-' + key);
				}
			});

			if (_this.attr('disabled') != 'disabled') {
				$('body').find('.' + name + '_frame').remove();
				$('body').append('<div class="' + name + '_frame jqmWindow" style="width:500px"></div>');
				$('.' + name + '_frame').jqm({
					trigger: '*[data-name="' + name + '"]', onLoad: function(hash) {
						onLoadjqm(hash);
					}, onHide: function(hash) {
						onHide(hash);
					}, ajax: script
				});
			}
		}
	})
}



// require(['Controls/ReCaptchaProvider'], function (ReCaptchaProvider) {
// 	new ReCaptchaProvider();
// });


/* from bit.js */

$(document).ready(function() {
	var navbarToggle = '.navbar-toggle'; // name of navbar toggle, BS3 = '.navbar-toggle', BS4 = '.navbar-toggler'
	$('.dropdown, .dropup').each(function() {
		var dropdown = $(this),
				dropdownToggle = $('[data-toggle="dropdown"]', dropdown),
				dropdownHoverAll = dropdownToggle.data('dropdown-hover-all') || false;

		// Mouseover
		dropdown.hover(function() {
			var notMobileMenu = $(navbarToggle).length > 0 && $(navbarToggle).css('display') === 'none';
			if ((dropdownHoverAll == true || (dropdownHoverAll == false && notMobileMenu))) {
				dropdownToggle.trigger('click');
			}
		})
	});
});

$(document).ready(function() {

	$(".dropdown.mega-dropdown").mouseenter(function()
	{

	}).mouseleave(function() {
		$(this).removeClass('open');
		//console.log('111');
	});

});

$('.navbar-toggle').click(function() {
	$('.navbar-toggle i').toggleClass('fa-bars fa-times');
});

$(document).ready(function() {
	$('[data-toggle="offcanvas"]').click(function() {
		$('.row-offcanvas').toggleClass('active')
	});

	var totalItems = $('.item').length;
	var currentIndex = $('div.active').index() + 1;

	$('#carousel-vertical').on('slid.bs.carousel', function() {
		currentIndex = $('div.active').index() + 1;
		$('.num').html(0 + '' + currentIndex + '');
	});

	if ($('#pCarousel').length > 0) {
		$('#pCarousel').carousel({interval: 3000});
	}



	$('.carousel-showmanymoveone .item').each(function() {
		var itemToClone = $(this);

		for (var i = 1; i < 3; i++) {
			itemToClone = itemToClone.next();

			if (!itemToClone.length) {
				itemToClone = $(this).siblings(':first');
			}

			itemToClone.children(':first-child').clone()
					.addClass("cloneditem-" + (i))
					.appendTo($(this));
		}
	});

});




function demoDisplay() {
	document.getElementById("foot").style.display = "none";
	document.getElementById("formaction").style.display = "none";
}

function demoVisibility() {
	document.getElementById("foot").style.display = "block";
	document.getElementById("formaction").style.display = "block";
}


var delta = 0;
var scrollThreshold = 5;
// Bind event handler
/*
 $(window).on(wheelEvent, function (e) {
 // Do nothing if we weren't scrolling the carousel
 var carousel = $('#carousel-vertical:hover');
 if (carousel.length === 0)  return;

 // Get the scroll position of the current slide
 var currentSlide = $(e.target).closest('.item')
 var scrollPosition = currentSlide.scrollTop();

 // --- Scrolling up ---
 if (e.originalEvent.detail < 0 || e.originalEvent.deltaY < 0 || e.originalEvent.wheelDelta > 0) {
 // Do nothing if the current slide is not at the scroll top
 if(scrollPosition !== 0) return;

 delta--;

 if ( Math.abs(delta) >= scrollThreshold) {
 delta = 0;
 carousel.carousel('prev');
 }
 }

 // --- Scrolling down ---
 else {
 // Do nothing if the current slide is not at the scroll bottom
 var contentHeight = currentSlide.find('> .content').outerHeight();
 if(contentHeight > currentSlide.outerHeight() && scrollPosition + currentSlide.outerHeight() !== contentHeight) return;

 delta++;
 if (delta >= scrollThreshold)
 {
 delta = 0;
 carousel.carousel('next');
 }
 }

 // Prevent page from scrolling
 return false;
 });
 */




if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
	var itemsToWrap = $('.testItem');
	$('.parentTest').remove();
	var carousel = '<div id="myCarousel" class="carousel slide hidden-md hidden-sm col-xs-12" data-ride="carousel"><div class="carousel-inner" role="listbox"></div>  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>    <span class="sr-only">Previous</span>  </a>  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>    <span class="sr-only">Next</span>  </a></div>';
	$(carousel).appendTo('.baseParent');
	var elementTowrap = "";
	$.each($(itemsToWrap), function(index) {
		var html = $(this).html();
		console.log(html);
		if (index == 0)
			elementTowrap += '<div class="item active">' + $(this).html() + '</div>';
		else
			elementTowrap += '<div class="item">' + $(this).html() + '</div>';
	});
	$('.carousel-inner').append($(elementTowrap));
	$("#myCarousel").carousel("pause").removeData();
	$("#myCarousel").carousel();
}


//testing purpose
if ($(window).width() < 768)
{
	var itemsToWrap = $('.testItem');
	$('.parentTest').remove();
	var carousel = '<div id="myCarousel" class="carousel slide hidden-md hidden-sm col-xs-12" data-ride="carousel"><div class="carousel-inner" role="listbox"></div>  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>    <span class="sr-only">Previous</span>  </a>  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>    <span class="sr-only">Next</span>  </a></div>';
	$(carousel).appendTo('.baseParent');
	var elementTowrap = "";
	$.each($(itemsToWrap), function(index) {
		var html = $(this).html();
		console.log(html);
		if (index == 0)
			elementTowrap += '<div class="item active">' + $(this).html() + '</div>';
		else
			elementTowrap += '<div class="item">' + $(this).html() + '</div>';
	});
	$('.carousel-inner').append($(elementTowrap));
	$("#myCarousel").carousel("pause").removeData();
	$("#myCarousel").carousel();

}

$('#text-carousel').on('slide.bs.carousel', function() {
	$('.carousel-inner').fadeOut(300);
})
$('#text-carousel').on('slid.bs.carousel', function() {
	$('.carousel-inner').fadeIn(600);
})


/* from script.js*/

$(function() {
	$('[data-toggle="tooltip"]').tooltip();

	if (!$('.navbar-toggle').closest('.filter').length) {

		$('.navbar-toggle').click(function() {
			$('.n_scroll_block').toggleClass('n_scroll_block_open');
			//$('html').toggleClass('no_scroll_block');
		});
	}

	// Костыль для мобилки

	if ($('.crm-menu').length) {
		$('.heading-block').addClass('style-change');
		$('.heading-block').find('.row').addClass('fixed-mobile-menu');
	}

	$("input[type=file]").change(function() {
		var $el = $(this),
				fileName,
				$block = $el.parent().find("span.file-name");
		if ($el.val().lastIndexOf('\\')) {
			var i = $el.val().lastIndexOf('\\') + 1;
		} else {
			var i = $el.val().lastIndexOf('/') + 1;
		}
		fileName = $el.val().slice(i);
		if (fileName == '')
			fileName = 'Прикрепить файл';
		$block.text(fileName);
	});

	$(window).scroll(function() {
		var scroll = $(window).scrollTop();

		if (scroll >= 100) {
			$(".banner-block").slideUp();
		} else {
			$(".banner-block").slideDown();
		}
	});
})


window.onSessionCallValue = function(call_value) {

	if (typeof setCookie === "undefined") {

		function setCookie(name, value, expires, path, domain, secure) {
			if (!name)
				return false;
			var str = name + '=' + encodeURIComponent(value);

			if (expires)
				str += '; expires=' + expires.toGMTString();
			if (path)
				str += '; path=' + path;
			if (domain)
				str += '; domain=' + domain;
			if (secure)
				str += '; secure';

			document.cookie = str;
			return true;
		}

	}

	var pops = document.querySelectorAll('.marquiz-pops');
	for (var i = 0; i < pops.length; ++i) {
		pops[i].style.visibility = "visible";
	}

	setCookie('roistat_visit', call_value)

	if (typeof Marquiz !== "undefined") {

		Marquiz.init({id: "5de6172e4734030045bdb536", autoOpen: false, autoOpenFreq: "once", openOnExit: false});

	}

}


$(document).ready(function() {

	$('[name="WEB_FORM_ID"]').parents('form').on('submit', function(e) {
		//принудительная проверка формы ибо где то что то в валидации не работает
		var formIsValid = $(e.target).valid();

		// защита от многократного сабмита
		var formIsSubmitted = ($(e.target).data('issubmitted') == 'Y');

		if (formIsSubmitted || !formIsValid) {
			e.preventDefault();
		} else {
			$(e.target).data('issubmitted', 'Y');
		}

		fbq('track', 'send_form_bit24');
		yaCounter45927957.reachGoal('feedback-send');
		gtag('event', 'send', {'event_category': 'form'});

	});
	$('.table__toggle').on('click', function(e) {
		var id=$(this).attr('rel');
		$('.table__toggle'+id).fadeToggle('fast');
	});

	$('.header__search-ico').on('click', function(e) {
		$(this).toggleClass('header__search-close');
		$('.header__search-form').fadeToggle('fast');
	});
	$('.navbar-toggle').on('click', function(e) {
		var w=$(window).width();
		var c='.topmenu-tablet';
		if (w<768){
			c='.topmenu-mobile';
			$('body').toggleClass('topmenu-mobile__overflow');
		}
		if ($(this).find('i').hasClass('fa-times')) {
			$(c).slideDown('fast');
		} else {
			$(c).slideUp('fast');
		}
	});
    $('.cd-dropdown-content .has-children > a').on('click', function(e) {
        $(this).parent().find('.is-hidden').slideToggle('fast');
        $(this).toggleClass('open');
        if($(this).hasClass('open')) {
            e.preventDefault();
        }else {
            e.preventDefault();
        }
        //return false;
    });
	$('a.anchor').on('click', function(e) {
		var w=$(window).width();
		var t=-116;
		if (w<768){
			var t=-66;
		}
		if (w<360){
			var t=-56;
		}
		if ($('.custom_banner').length){
			var t=0;
		}
		var id=$(this).attr('href');
		$.scrollTo(id, 1000, {offset: function() { return {top:t}; }});
		return false;
	});
});