<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$selectRegion = $arResult['SELECT_CODE'];
$arResult["HTML_TITLE"] = htmlspecialcharsback(CComponentEngine::MakePathFromTemplate($arParams["HTML_TITLE"], Array(
    "HTML_LIST_ID" => $arParams["HTML_LIST_ID"],
    "NAME" => $arResult["SELECT"][$arResult['CITY'][$selectRegion]]['CITY'][$selectRegion]['NAME'],
    "NAME_DATIVE" => $arResult["SELECT"][$arResult['CITY'][$selectRegion]]['CITY'][$selectRegion]['UF_NAME_DATIVE']
)));
echo $arResult["HTML_TITLE"];
