<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<?if (count($arResult['SECTIONS']) > 0):?>
    <div class="faq__sidebar-cat-block faq__sidebar-cat-block_business">
        <div class="faq__sidebar-cat-block-title">Для разных бизнесов</div>
        <div class="faq__sidebar-cat-block-links">
			<?foreach($arResult['SECTIONS'] as $arSection):?>
                <div class="faq__sidebar-cat-block-link">
                    <a href="<?echo $arSection["SECTION_PAGE_URL"]?>"><?=$arSection["NAME"]?></a>
                </div>
			<?endforeach;?>
        </div>
    </div>
<?endif;?>