<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	Bitrix\Main\Page\Asset,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/crm.css");

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
$arParams["SECTION_CODE"] = trim($arParams["SECTION_CODE"]);
if (empty($arParams["SECTION_CODE"]) && Loader::IncludeModule("iblock"))
{
	$arSection = \CIBlockSection::GetList(
		array("SORT" => "ASC"),
		array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y"),
		false,
		array("ID", "SECTION_PAGE_URL"),
		array("nTopCount" => 1)
	)->GetNext();
	$this->AbortResultCache();
	LocalRedirect($arSection["SECTION_PAGE_URL"]);
}

//if ($this->StartResultCache()) {
	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFilter = array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "CODE" => $arParams["SECTION_CODE"]);

	$rsSection = \CIBlockSection::GetList(
		array("SORT" => "ASC"),
		$arFilter,
		false,
		array("ID", "NAME", "SECTION_PAGE_URL", "UF_BLOCK_TITLE", "UF_BLOCK_SUBTITLE")
	);
	if ($arResult = $rsSection->GetNext())
	{
		$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams["IBLOCK_ID"], $arResult["ID"]);
		$arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();

		$arFile = array();

		$rsSection = \CIBlockSection::GetList(
			array("SORT" => "ASC"),
			array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ACTIVE" => "Y"
			),
			false,
			array("ID", "NAME", "SECTION_PAGE_URL"));
		while ($arSection = $rsSection->GetNext())
		{
			$arSection["ACTIVE"] = ($arSection["ID"] == $arResult["ID"]);
			$arResult["SECTION"][] = $arSection;
		}

		$rsElement = CIBlockElement::GetList(
			array("SORT" => "ASC"),
			array(
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ACTIVE" => "Y",
				"SECTION_ID" => $arResult["ID"]
			),
			false,
			false,
			array("ID", "NAME", "PREVIEW_TEXT", "DETAIL_TEXT", "PREVIEW_PICTURE", "DETAIL_PICTURE",  "PROPERTY_YOUTUBE", "PROPERTY_TYPE")
		);
		while ($arElement = $rsElement->GetNext())
		{
			if ($arElement["PREVIEW_PICTURE"])
				$arFile[] = $arElement["PREVIEW_PICTURE"];
			if ($arElement["DETAIL_PICTURE"])
				$arFile[] = $arElement["DETAIL_PICTURE"];

			$arResult["ITEMS"][] = $arElement;
		}

	} else
	{
		$this->AbortResultCache();
		if (Loader::includeModule('iblock'))
		{
			\Bitrix\Iblock\Component\Tools::process404(
				""
				, true
				, true
				, true
				, ""
			);
		}
		return;
	}

	$arResult["IMAGES"] = Helper::getImages($arFile);

	$this->SetResultCacheKeys(array("ID", "NAME", "IPROPERTY_VALUES", "SECTION_PAGE_URL"));
	$this->IncludeComponentTemplate();
//}
if (isset($arResult["ID"]))
{

	$arTitleOptions = null;

	if ($arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"] != "")
		$APPLICATION->SetPageProperty("title", $arResult["IPROPERTY_VALUES"]["SECTION_META_TITLE"], $arTitleOptions);
	else
		$APPLICATION->SetPageProperty("title", $arResult["NAME"], $arTitleOptions);

	if ($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
		$APPLICATION->SetTitle($arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"], $arTitleOptions);
	else
		$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);

	if ($arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"] != "")
		$APPLICATION->SetPageProperty("keywords", $arResult["IPROPERTY_VALUES"]["SECTION_META_KEYWORDS"], $arTitleOptions);
	if ($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"] != "")
		$APPLICATION->SetPageProperty("description", $arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"], $arTitleOptions);

	$APPLICATION->AddChainItem($arResult["NAME"], $arResult["SECTION_PAGE_URL"]);

	return $arResult["ID"];
}