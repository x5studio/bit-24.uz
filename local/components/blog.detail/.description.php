<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Блог. Детально",
	"DESCRIPTION" => "Блог. Детально",
	"ICON" => "/images/news_line.gif",
	"SORT" => 110,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "blog"
		)
	),
);

?>