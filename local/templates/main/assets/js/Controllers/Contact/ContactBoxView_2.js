/**
 * Created by Павел on 29.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/Contact/AContactBoxView'], function ($, AContactBoxView) {
        function ContactBoxView_2(item, options) {
            AContactBoxView.apply(this, arguments);
            this.options = $.extend(true, {}, this.options, ContactBoxView_2.defaultOptions, options);
        }

        extend(ContactBoxView_2, AContactBoxView);
        ContactBoxView_2.defaultOptions = {};

        ContactBoxView_2.prototype.render = function () {
            ContactBoxView_2.superclass.render.apply(this, arguments);
        };

        return ContactBoxView_2;
    });
}