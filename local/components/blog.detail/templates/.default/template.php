<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="main-blog">
	<div class="content">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="blog-item pull-right">
					<? if ($arResult["DETAIL_PICTURE"]): ?>
						<img src="<?= $arResult["IMAGES"][$arResult["DETAIL_PICTURE"]] ?>" class="img-responsive"
								 alt="<?= $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] ?>" alt="<?= $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] ?>">
					<? endif; ?>
					<div class="row row-text vertical-align">
						<div class="col-md-6 col-sm-6 col-xs-12 text text-left">
							<? foreach ($arResult["TAG"] as $arTag): ?>
								<a href="<?= $arTag["URL"] ?>" class="btn btn-sm"><?= $arTag["NAME"] ?></a>
							<? endforeach; ?>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12 date text-right">
							<span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
							<span class="views"><i class="fa fa-eye" aria-hidden="true"></i><?= $arResult["SHOW_COUNTER"] ?></span>
						</div>
					</div>
					<div class="material"><?= $arResult["DETAIL_TEXT"] ?></div>
				</div>
				<div class="content-bottom">
					<div class="col-md-6 col-sm-6 col-xs-12 text text-left">
						<a class="btn btn-sm" onclick="javascript:history.back(); return false;">Вернуться назад</a>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12 date text-right">
						<script type="text/javascript">(function () {
                    if (window.pluso)if (typeof window.pluso.start == "function") return;
                    if (window.ifpluso == undefined) {
                        window.ifpluso = 1;
                        var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                        s.type = 'text/javascript';
                        s.charset = 'UTF-8';
                        s.async = true;
                        s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                        var h = d[g]('body')[0];
                        h.appendChild(s);
                    }
                })();</script>
						<div class="pluso" data-background="transparent"
								 data-options="medium,square,line,horizontal,counter,theme=04"
								 data-services="vkontakte,facebook,odnoklassniki,moimir"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>