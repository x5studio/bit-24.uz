<ul>
    <li><a href="/uslugi/integration/">Интеграция с 1С, сайтом, мессенджерами и т.д.</a></li>
    <li><a href="/uslugi/obuchenie-bitrix24/">Обучение работе в Битрикс24</a></li>
    <li><a href="/uslugi/tekhpodderzhka-bitrix24/">Техническая поддержка</a></li>
    <li><a href="/uslugi/business-process-bitrix24/">Автоматизация бизнес процессов</a></li>
    <li><a href="/uslugi/dorabotka-bitrix24/">Доработка Битрикс24</a></li>
    <li><a href="/uslugi/ustanovka-korobochnoy-versii-bitrix24/">Установка коробочной версии</a></li>
</ul>