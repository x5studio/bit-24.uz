/**
 * Created by Павел on 29.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/Contact/AContactBoxView'], function ($, AContactBoxView) {
        function ContactBoxView_1(item, options) {
            AContactBoxView.apply(this, arguments);
            this.options = $.extend(true, {}, this.options, ContactBoxView_1.defaultOptions, options);
        }

        extend(ContactBoxView_1, AContactBoxView);
        ContactBoxView_1.defaultOptions = {};

        ContactBoxView_1.prototype.compileOfficeItem = function (item, mOffice) {
            ContactBoxView_1.superclass.compileOfficeItem.apply(this, arguments);
            var o = this;
            var realOffice = o.getContactManager().getRealOfficeByID(mOffice['ID']);
            var boxName = item.find('.box-office-name');
            if (mOffice['VIEW_LINK'] != 'N' && mOffice['DETAIL_PAGE_URL']) {
                var link = $('<a>').attr('href', mOffice['DETAIL_PAGE_URL']).text(boxName.text());
                boxName.html(link);
            }
            try {
                var boxPhoneIcnTpl = item.find('.box-office-phone-icn');
                for (var i in realOffice['PROPERTIES']['WORK_TIME']['VALUE']) {
                    var itemWorkTime = realOffice['PROPERTIES']['WORK_TIME']['VALUE'][i];
                    if (realOffice['PROPERTIES']['WORK_TIME']['DESCRIPTION'][i]) {
                        itemWorkTime += " " + realOffice['PROPERTIES']['WORK_TIME']['DESCRIPTION'][i];
                    }
                    var $itemWorkTime = boxPhoneIcnTpl.clone().text(itemWorkTime);
                    boxPhoneIcnTpl.after($itemWorkTime);
                }
                boxPhoneIcnTpl.remove();
            } catch (e) {
            }
            try {
                var boxEmail = item.find('.box-office-email');
                var boxEmailLink = boxEmail.find('a');
                var emailView;
                if(mOffice['PROPERTIES']['VIEW_EMAIL']['VALUE'].length>0)	emailView=mOffice['PROPERTIES']['VIEW_EMAIL']['VALUE'];
                else emailView=mOffice['PROPERTIES']['email']['VALUE'];
                boxEmailLink.text(emailView).attr('href', "mailto:" + emailView);
            } catch (e) {
            }

        };

        ContactBoxView_1.prototype.makeOfficeItem = function () {
            throw new ReferenceError();
        };

        ContactBoxView_1.prototype.renderOfficeList = function () {
            throw new ReferenceError();
        };

        ContactBoxView_1.prototype.renderContent = function () {
            var o = this;
            var mOffices = o.getModel('offices');
            var $content = o.getView().find('.box-content');
            var $item = $content.find('.box-offices-item');
            for (var i in mOffices) {
                var mOffice = mOffices[i];
                o.compileOfficeItem($item, mOffice);
            }
            $item.removeClass('hidden');
            $content.on('click', '>a', function (event) {
                var target = $(event.target);
                if (target.is('li.link')) {
                    event.preventDefault();
                }
            });
            o.getView().trigger(new $.Event('contactBox.view.renderOfficeList', {}));
            o.getView().trigger(new $.Event('contactBox.view.renderContent', {}));
        };

        return ContactBoxView_1;
    });
}