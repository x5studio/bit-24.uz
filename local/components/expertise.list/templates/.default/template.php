<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult["ITEMS"])): ?>
	<section class="middle-block">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<p class="topp">Каждая сфера бизнеса обладает своей спецификой — набором и составом услуг, способом взаимодействия с клиентами, а также особенностями 
внутренних бизнес-процессов. Чтобы внедрение инструментов автоматизации действительно приводило к росту эффективности работы компании, а также к повышению прибыли,
необходимо эту специфику учитывать. Специалисты компании "Первый БИТ" осуществили более 200 внедрений Битрикс24 в совершенно различных отраслях и разработали конкретные решения
для каждой отрасли, максимально соответствующие потребностям входящих в нее компаний. </p>
					<section class="fields">
						<div class="container">
							<h2 class="fields-heading">за 200+ внедрений Битрикс24 мы накопили<br>обширную
								отраслевую экспертизу</h2>
							<div class="row row-centered">
								<? foreach ($arResult["ITEMS"] as $arItem): ?>
									<div class="col-md-25 col-sm-4 col-xs-12 ffli">
										<div class="<?= $arItem["UF_CLASS"] ?>">
											<a href="/otraslevaja-jekspertiza/<?= $arItem["CODE"] ?>/" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a>
										</div>
									</div>
								<? endforeach; ?>
								<?= str_repeat(
									'<div class="col-offset-md-25 col-offset-sm-12 col-offset-xs-12"></div>',
									(5 - count($arResult["ITEMS"]) % 5)
								); ?>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</section>
	<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
		<? if ($cell % 2): ?>
			<section class="branches-gray" id="<?= $arItem["CODE"] ?>">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 hidden-xs">
							<img src="<?= $arResult["IMAGES"][$arItem["PICTURE"]] ?>" class="img-responsive"
									 alt="<?= $arItem["NAME"] ?>">
						</div>
						<div class="col-md-6 col-sm-6">
							<h2><?= $arItem["NAME"] ?></h2>
							<div class="hidden-md hidden-sm hidden-lg col-xs-12">
								<img src="<?= $arResult["IMAGES"][$arItem["PICTURE"]] ?>" class="img-responsive"
										 alt="<?= $arItem["NAME"] ?>">
							</div>
							<p><?= $arItem["UF_DESC"] ?></p>
							<button type="button" class="btn btn-more"><a href="<?= $arItem["SECTION_PAGE_URL"]
								?>">Подробнее</a></button>
						</div>
					</div>
				</div>
			</section>
		<? else: ?>
			<section class="branches-white" id="<?= $arItem["CODE"] ?>">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<h2><?= $arItem["NAME"] ?></h2>
							<div class="hidden-md hidden-sm hidden-lg col-xs-12">
								<img src="<?= $arResult["IMAGES"][$arItem["PICTURE"]] ?>" class="img-responsive"
										 alt="<?= $arItem["NAME"] ?>">
							</div>
							<p><?= $arItem["UF_DESC"] ?></p>
							<button type="button" class="btn btn-more"><a href="<?= $arItem["SECTION_PAGE_URL"]
								?>">Подробнее</a></button>
						</div>
						<div class="col-md-6 col-sm-6 hidden-xs">
							<img src="<?= $arResult["IMAGES"][$arItem["PICTURE"]] ?>" class="img-responsive"
									 alt="<?= $arItem["NAME"] ?>">
						</div>
					</div>
				</div>
			</section>
		<? endif; ?>
	<? endforeach; ?>
<? endif; ?>