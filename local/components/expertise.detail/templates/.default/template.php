<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<style>
	.adv-title {
		color: #fff;
	}
	.adv-icon img {
		padding: 0;
		max-width: 100%;
		max-height: 60px;
		float: none;
		margin: 0 auto;
	}
	.adv-card {
		display: flex;
		flex-direction: column;
		padding: 25px 0;
		text-align: center;
	}
	.adv-title {
		color: #fff;
		line-height: normal;
		margin-top: 10px;
	}
	.adv-main-description {
		padding: 80px 0;
	}
	.adv-main-description .btn  {
		margin-top: 20px;
	}
	.adv-main-description p {
		margin-top: 30px;
	}
	.adv-prices p {
		margin: 30px 0;
		font-family: Roboto-Regular;
		font-size: 16px;
		line-height: 24px;
		color: #000;
		padding-right: 40px;
	}
	.adv-main-description p {
		margin: 30px 0;
		font-family: Roboto-Regular;
		font-size: 16px;
		line-height: 24px;
		color: #000;
		padding-right: 40px;
	}
	.adv-prices h2 {
		margin-top: 65px;
	}
	.adv-page-nav {
		list-style: none;
		padding: 0;
		margin: 30px 0;
	}
	.adv-page-nav li {
		position: relative;
		padding-left: 12px;
		margin-bottom: 2px;
	}
	.adv-page-nav li:before {
		content: "";
		display: inline-block;
		width: 6px;
		height: 6px;
		background-color: #ae127b;
		border-radius: 3px;
		padding: 0;
		margin-right: 5px;
		position: absolute;
		top: 7px;
		left: 0;
	}
	.adv-page-nav li a {
		color: #324abd;
		font-weight: bold;
	}
	.first-block {
		padding-bottom: 80px;
	}
	@media (max-width: 450px) {
		.adv-icon img {
			display: block !important;
		}
	}
	@media (max-width: 992px) {
		.adv-btn-wrapper {
			text-align: center;
			margin-bottom: 30px;
		}
	}
</style>

<section class="first-block">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12<? if (!empty($arResult["UF_UNDER_DESC"])): ?> personal<? endif; ?>">
				<?= $arResult["DESCRIPTION"] ?>
				<? if (!empty($arResult["UF_ANCHOR_LINKS"])): ?>
					<?= html_entity_decode($arResult["UF_ANCHOR_LINKS"]); ?>
				<? endif; ?>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<a href="<?= $arResult["IMAGES"][$arResult["DETAIL_PICTURE"]] ?>" data-fancybox>
					<img src="<?= $arResult["IMAGES"][$arResult["DETAIL_PICTURE"]] ?>" class="img-responsive center-block">
				</a>
			</div>
		</div>
        <br>
		<div class="row">
			<div class="col-md-12 text-center footbtn">
				<button type="button" onclick="yaCounter45927957.reachGoal('feedback-click'); return true;" class="btn btn-blocks top-button"
						data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Получить консультацию</button>
			</div>
		</div>
		<?= $arResult["~UF_UNDER_DESC"] ?>
	</div>
</section>

<? if (strlen($arResult["UF_TOP_DESC"])): ?>
	<section class="blue-block">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="ribbon">
						<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/realestate/ribbon.png" class="img-responsive">
					</div>
					<p><?= $arResult["UF_TOP_DESC"] ?></p>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>

<? if (is_array($arResult["UF_EXPERT_ICONS"])): ?>
	<section class="blue-block">
		<div class="container">
			<div class="row">
				<? foreach ($arResult["UF_EXPERT_ICONS"] as $key => $imgID): ?>
					<div class="col-md-2 col-sm-4">
						<div class="adv-card">
							<div class="adv-icon"><img src="<?= CFile::GetPath($imgID); ?>" class="img-responsive"></div>
							<div class="adv-title"><?= $arResult["UF_EXPERT_ICONS_DESC"][$key]; ?></div>
						</div>
					</div>
				<? endforeach; ?>
			</div>
		</div>
	</section>
<? endif; ?>

<? if ($arResult["UF_SECOND_DESC"] || $arResult["UF_SECOND_DESC_TEXT"]): ?>
	<section id="adv-functional" class="adv-main-description">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h2><?= $arResult["UF_SECOND_DESC"] ?></h2>
					<?= html_entity_decode($arResult["UF_SECOND_DESC_TEXT"]); ?>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>


<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
	<? if ($cell % 2): ?>
		<section class="branches-white"  <?php if($arItem['CODE']):?> id="element_<?= $arItem['CODE'] ?>"<?php endif;?>>
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<h3><?= $arItem["~NAME"] ?></h3>
						<?= $arItem["PREVIEW_TEXT"] ?>
					</div>
					<? if ($arItem["PREVIEW_PICTURE"]): ?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<a href="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" data-fancybox>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive" alt="">
							</a>
						</div>
					<? elseif ($arItem['PROPERTY_SHOW_VIDEO_VALUE'] === 'Y'): ?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="videobg center-block">
								<a data-fancybox href='https://www.youtube.com/embed/<?= $arItem["PROPERTY_YOUTUBE_VALUE"] ?>'>
									<iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
								</a>
							</div>
						</div>
					<? endif; ?>
					<? if (!empty($arItem["DETAIL_TEXT"])): ?>
						<?= $arItem["DETAIL_TEXT"] ?>
					<? endif; ?>
				</div>
			</div>
		</section>
	<? else: ?>
		<section class="branches-gray" <?php if($arItem['CODE']):?> id="element_<?= $arItem['CODE'] ?>"<?php endif;?>>
			<div class="container">
				<div class="row">
					<? if ($arItem["PREVIEW_PICTURE"]): ?>
						<div class="col-md-6 hidden-sm hidden-xs">
							<a href="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" data-fancybox>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
							</a>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h3><?= $arItem["~NAME"] ?></h3>
							<?= $arItem["PREVIEW_TEXT"] ?>
						</div>
						<div class="hidden-md col-sm-12 hidden-lg col-xs-12">
							<a href="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" data-fancybox>
								<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive">
							</a>
						</div>
					<? else: ?>
						<? if ($arItem['PROPERTY_SHOW_VIDEO_VALUE'] === 'Y'): ?>
							<div class="col-md-6 hidden-sm hidden-xs">
								<div class="videobg center-block">
									<a data-fancybox href='https://www.youtube.com/embed/<?= $arItem["PROPERTY_YOUTUBE_VALUE"] ?>'>
										<iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
									</a>
								</div>
							</div>
						<? endif; ?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<h3><?= $arItem["~NAME"] ?></h3>
							<?= $arItem["PREVIEW_TEXT"] ?>
						</div>
						<? if ($arItem['PROPERTY_SHOW_VIDEO_VALUE'] === 'Y'): ?>
							<div class="hidden-md col-sm-12 hidden-lg col-xs-12 center-block">
								<div class="videobg center-block">
									<a data-fancybox href='https://www.youtube.com/embed/<?= $arItem["PROPERTY_YOUTUBE_VALUE"] ?>'>
										<iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $arItem["PROPERTY_YOUTUBE_VALUE"] ?>"></iframe>
									</a>
								</div>
							</div>
						<? endif; ?>
					<? endif; ?>
					<? if (!empty($arItem["DETAIL_TEXT"])): ?>
						<?= $arItem["DETAIL_TEXT"] ?>
					<? endif; ?>
				</div>
			</div>
		</section>
	<? endif; ?>
	<?php if ($cell === 3): ?>
		<div id="WIDGET_CONSULTATION_FORM"></div>
	<?php endif; ?>
<? endforeach; ?>




<section id="adv-tarifs" class="buy-license" style="background-color:#f9f8fe;padding-bottom:15px;padding-top:15px;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="adv-prices">
					<h2>Тарифы и цены</h2>
					<?= html_entity_decode($arResult["UF_TARIFS_DESC"]); ?>
				</div>
			</div>
		</div>
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#cloud" aria-controls="cloud" role="tab" data-toggle="tab">Облачная версия</a></li>
			<li role="presentation"><a href="#box" aria-controls="box" role="tab" data-toggle="tab">Коробочная версия</a> </li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="cloud">
				<?
				$APPLICATION->IncludeComponent(
					"buy.tariff", "", Array(
					"ACTIVE_PRICE" => "PRICEFORTWOYEARS",
					"ADDITIONAL_PRICES" => array("PRICEFORMONTH" => "на 1 месяц", "PRICEFORTHREEMONTHS" => "на 3 месяца", "PRICEFORHALFAYEAR" => "на полгода", "PRICEFORYEAR" => "на год", "PRICEFORTWOYEARS" => "на 2 года",),
					"BUTTON_TEXT" => "Купить",
					"CACHE_TIME" => "360000",
					"CACHE_TYPE" => "A",
					"EXCLUDE_PROPS" => array('DESC', 'ICON', 'TYPE'),
					"IBLOCK_ID" => "10",
					"SHOW_ELEMENT_NAME" => true,
					"SHOW_MOBILE_PROP" => [
                        'PRICE',
                        'USERS',
                        'OFIS',
                        'CRM',
                        'TASKS_PROJECTS'
                    ],
//					'BUTTON_IS_LINK' => array('http://bitrix24.ru/create.php?p=27270')
					), $component
				);
				?>

				<div class="row oboznachenia">
					<div class="col-md-3">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
						нет
					</div>
					<div class="col-md-3">
						<i class="fa fa-plus-circle" aria-hidden="true"></i>
						базовый набор возможностей
					</div>
					<div class="col-md-3">
						<i class="fa fa-plus-circle" aria-hidden="true"></i>
						<i class="fa fa-plus-circle" aria-hidden="true"></i>
						расширенный
					</div>
					<div class="col-md-3">
						<i class="fa fa-plus-circle" aria-hidden="true"></i>
						<i class="fa fa-plus-circle" aria-hidden="true"></i>
						<i class="fa fa-plus-circle" aria-hidden="true"></i>
						профессиональный
					</div>
				</div>

			</div>
			<div role="tabpanel" class="tab-pane clearfix" id="box">
				<?
				$APPLICATION->IncludeComponent(
					"buy.tariff", "", Array(
					"ALL_TARIFFS_TITLE" => "Редакции",
					"CACHE_TIME" => "360000",
					"CACHE_TYPE" => "A",
					"EXCLUDE_PROPS" => array('TYPE'),
					"IBLOCK_ID" => "11"
					), $component
				);
				?>
			</div>
		</div>
	</div>
</section>


<? if ($arResult["UF_TARIF_BOTTOM_DESC"] || $arResult["UF_SECOND_DESC_TEXT"]): ?>
	<section id="adv-integration" class="adv-main-description">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<h2><?= $arResult["UF_TARIF_BOTTOM_DESC"] ?></h2>
					<? if ($arResult['UF_TARIF_BOTTOM_TEXT']): ?>
						<?= html_entity_decode($arResult["UF_TARIF_BOTTOM_TEXT"]); ?>
					<? endif; ?>
				</div>
			</div>
		</div>
	</section>
<? endif; ?>


<?php/*
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
	"WEB_FORM_ID" => 13, // ID веб-формы
	"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
	"COMPANY_IBLOCK_ID" => IB_CONTACTS,
	"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
	"EMAIL_QUESTION_CODE" => "EMAIL",
	"CITY_QUESTION_CODE" => "CITY",
	"OFFICE_QUESTION_CODE" => "OFFICE",
	"TO_QUESTION_RESIPIENTS" => "",
	"EMAIL_ONLY_FROM_RESIPIENTS" => "",
	"PRODUCT_QUESTION_CODE" => "PRODUCT",
	"TO_QUESTION_CODE" => "TO",
	"IGNORE_CUSTOM_TEMPLATE" => "N", // Игнорировать свой шаблон
	"USE_EXTENDED_ERRORS" => "Y", // Использовать расширенный вывод сообщений об ошибках
	"SEF_MODE" => "N", // Включить поддержку ЧПУ
	"CACHE_TYPE" => "A", // Тип кеширования
	"CACHE_TIME" => "3600", // Время кеширования (сек.)
	"LIST_URL" => "", // Страница со списком результатов
	"EDIT_URL" => "", // Страница редактирования результата
	"SUCCESS_URL" => "", // Страница с сообщением об успешной отправке
	"CHAIN_ITEM_TEXT" => "", // Название дополнительного пункта в навигационной цепочке
	"CHAIN_ITEM_LINK" => "", // Ссылка на дополнительном пункте в навигационной цепочке
	"VARIABLE_ALIASES" => array(
		"WEB_FORM_ID" => "WEB_FORM_ID",
		"RESULT_ID" => "RESULT_ID",
	),
	"ALERT_ADD_SHARE" => "N",
	"HIDE_PRIVACY_POLICE" => "Y",
	"BTN_CALL_FORM" => ".btn-buy",
	"HIDE_FIELDS" => array(
		0 => "CITY",
		1 => "OFFICE",
		2 => "TYPE",
	),
	"COMPONENT_MARKER" => "feedform",
	"SHOW_FORM_DESCRIPTION" => "N",
	"MESS" => array(
		"THANK_YOU" => "Спасибо, Ваша заявка принята.",
		"WAIT_CALL" => "Мы свяжемся с Вами в течение 2-х часов.",
		"TITLE_FORM" => "Заказать услугу2",
	)
	), false, array(
	"HIDE_ICONS" => "Y"
	)
);*/
?>



<? if (strlen($arResult["UF_BOTTOM_DESC"])): ?>
	<section class="bottom-block"<? if ($arResult["UF_BOTTOM_BG"]): ?> style="background-image: url(<?= $arResult["IMAGES"][$arResult["UF_BOTTOM_BG"]] ?>);"<? endif; ?>>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12"><?= $arResult["~UF_BOTTOM_DESC"] ?></div>
			</div>
		</div>
	</section>
<? endif; ?>
