<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<div class="testimonials__list">
    <div class="content">
        <div class="container">
            <?php if ($arResult["ITEMS"]): ?>
                <div class="row">
                    <div class="testimonials__items">
                        <? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
                        <div class="testimonials__item<?php echo !empty($arItem['PROPERTIES']["YOUTUBE_CODE"]['VALUE']) ? ' testimonials__item_video' : ''; ?>">
                            <div class="testimonials__item-info">
                                <div class="testimonials__item-logo">
                                    <img src="<?= $arItem["PREVIEW_PICTURE"]['SRC']; ?>" class="img-responsive" alt="<?= $arItem["NAME"]; ?>">
                                </div>
                                <div class="testimonials__item-company-name"><?php echo $arItem["NAME"]; ?></div>
                                <div class="testimonials__item-author-name"><?php echo $arItem['PROPERTIES']["AUTHOR_FIO"]['VALUE']; ?></div>
                                <div class="testimonials__item-author-position"><?php echo $arItem['PROPERTIES']["AUTHOR_POSITION"]['VALUE']; ?></div>
                            </div>
                            <div class="testimonials__item-content">
                                <?php if (!empty($arItem['PROPERTIES']["YOUTUBE_CODE"]['VALUE'])): ?>
                                    <div class="testimonials__item-video">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $arItem['PROPERTIES']["YOUTUBE_CODE"]['VALUE']; ?>?autoplay=0&showinfo=0&rel=0"></iframe>
                                    </div>
                                <?php endif; ?>
                                <div class="testimonials__item-text-wrapper">
                                    <div class="testimonials__item-text">
                                        <?php echo $arItem["PREVIEW_TEXT"] ?>
                                    </div>
                                    <div class="testimonials__item-date"><?= $arItem["DISPLAY_ACTIVE_FROM"]; ?></div>
                                    <?php if ($arItem["TESTIMONIAL_FILE_SRC"]): ?>
                                        <div class="testimonials__item-file">
                                            <a href="<?php echo $arItem["TESTIMONIAL_FILE_SRC"]; ?>" download>Скачать бланк отзыва</a>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <? endforeach; ?>
                    </div>
                    <?php if ($arResult["NAV_STRING"]): ?>
                        <div class="testimonial__pagination">
                            <?php echo $arResult["NAV_STRING"]; ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="row">
                    Раздел находится в стадии наполнения
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>