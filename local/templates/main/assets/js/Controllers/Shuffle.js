if (typeof define === "function" && define.amd) {
    define([], function () {
        function Shuffle() {
            this.items = [];
        }

        Shuffle.prototype.add = function (item) {
            this.items.push(item);
        };
        Shuffle.prototype.remove = function (itemId) {
            var o = this;
            for (var i in o.items) {
                var item = o.items[i];
                if (item.id == itemId) {
                    o.items.splice(i, 1);
                }
            }
        };
        Shuffle.prototype.sort = function () {

        };
        Shuffle.prototype.getList = function () {
            return this.items;
        };

        return Shuffle;
    });
}