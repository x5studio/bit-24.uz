<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Актуальные новости и экспертные статьи по Битрикс24 от команды Первый БИТ");
$APPLICATION->SetPageProperty("keywords", "Битрикс24, Битрикс, Bitrix24, Bitrix, bit24, Бит24, Битрикс новое, Битрикс24 новое, новое Битрикс, новое Битрикс24, Первый БИТ, статьи по Битрикс, статьи по Битрикс24, Битрикс24 инструкция, как работать в Битрикс24, как работать в Битрикс, Первый БИТ результаты, Первый БИТ блог, блог, Битрикс24 блог");
$APPLICATION->SetPageProperty("title", "Актуальные новости и опыт внедрения корпоративного портала Битрикс24 от компании Первый БИТ");
$APPLICATION->SetTitle("Блог по Битрикс24 от команды Первый БИТ");
?><?$APPLICATION->IncludeComponent(
	"blog", 
	"", 
	array(
		"IBLOCK_ID" => "2",
		"SEF_FOLDER" => "/blog/",
		"SEF_MODE" => "Y",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
		)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>