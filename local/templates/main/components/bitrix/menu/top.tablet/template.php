<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):
    ?>
<div class="topmenu topmenu-tablet">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4 topmenu-tablet__block">
                <ul>
<?
$previousLevel = 0;
foreach($arResult as $arItem):
    $icon = !empty($arItem['PARAMS']['ICON']) ? $arItem['PARAMS']['ICON'] : false;
    $iconClass = !empty($arItem['PARAMS']['ICON_CLASS']) ? $arItem['PARAMS']['ICON_CLASS'] : false;
    ?>
	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

    <?if ($arItem["PARAMS"]['TABLET_COLUMN_START'] === 'Y'):?>
            </ul>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 topmenu-tablet__block">
            <ul>
    <?endif?>

	<?if ($arItem["IS_PARENT"]):?>

        <li>
            <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
            <ul>
	<?else:?>

            <li>
                <a href="<?=$arItem["LINK"]?>">
                    <? if ($icon || $iconClass) { ?>
                        <span class="menu-ico <? if ($iconClass) echo $iconClass; ?>" <? if ($icon) { ?>style="background: url(<?= $icon ?>) no-repeat left center"<? } ?>>
                    <? } ?>
                    <?=$arItem["TEXT"]?>
                    <? if ($icon || $iconClass) { ?>
                        </span>
                    <? } ?>
                </a>
            </li>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?endif?>