<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>



<div class="modal-header"><i class="fa fa-times" style="float: right;" data-dismiss="modal" aria-hidden="true"></i>
	<h4>Подготовка инструкции/регламента</h4>
</div>
<div class="modal-body">
	<div class="material">
		<div class="questions">
			<h3>Для кого?</h3>
			<p>Для руководителей, который хотят формализовать работу сотрудников компании.</p>
		</div>
		<div class="questions">
			<h3>Что делаем?</h3>
			<p>Данная услуга носит индивидуальный характер. Это предложение для клиентов, которые проходили у нас этап аналитики, постановки и настройки бизнес-процессов.
Мы формализуем инструкции и регламенты, возникающие, как правило, на этапе аналитики, либо уже существующие в головах Ваших сотрудников, но ещё не зафиксированные в документе.

Если у Вас ещё нет ясного понимания того, как должны работать сотрудники – не беда! На этапе аналитики мы сформируем модель работы и напишем для Вас регламенты и инструкции с нуля.
			</p></div>
		<div class="questions">
			<h3>Результаты</h3>
			<p>Подготовленные документы в электронном формате.

</p>
		</div>
		<div class="service-bottom">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Формат:<img src="/local/templates/main/assets/img/service/com.png"
													class="img-responsive"></h3>
					<p>ДОКУМЕНТ</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Стоимость<img src="/local/templates/main/assets/img/service/wallet.png"
														class="img-responsive"></h3>
					<h4>от 6 000 р.</h4>
				</div>
			</div>
			<div class="buy">
				<button type="button" class="btn btn-buy" data-target="#advanced"
								data-form-field-type="Консультационное внедрение: Подготовка инструкции/регламента"
								data-toggle="modal" data-dismiss="modal"><a href="#" title="link">Купить</a></button>
			</div>
		</div>
	</div>
</div>