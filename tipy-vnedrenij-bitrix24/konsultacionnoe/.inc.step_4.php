<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="mattop">
	<h2>Шаг 4 — Сопровождение на этапе запуска</h2>
	<h3>Для гарантированного продолжения работы сотрудников в Битрикс24</h3>
</div>
<div class="include">
	<h3>Что входит:</h3>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<ul>
				<li>Контрольные звонки и заявки по всем рекламным источникам компании&nbsp;с целью контроля корректности занесения данных в Битрикс24;</li>
				<li>Контроль выполнения бизнес-процессов в системе;</li>
				<li>Контроль работоспособности системы и оперативное устранение ошибок и технических сбоев;</li>
				<li>Консультации пользователей по инструкциям и регламентам работы с системой Битрикс24.</li>
			</ul>
		</div>
	</div>
</div>
<div class="step-bottom step-bottom_has-icons">
	<div class="row">
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Формат:</h3>
 				<img src="/local/templates/main/assets/img/bt/com.png" class="img-responsive">
			<p>
				 Очно / удалённо
			</p>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Цена:</h3>
 <img src="/local/templates/main/assets/img/bt/wallet.png" class="img-responsive">
			<h4>20 000 руб.</h4>
		</div>
		<div class="col-md-4 col-sm-12 col-xs-12 block">
			<h3>Срок:</h3>
 <img src="/local/templates/main/assets/img/bt/cal.png" class="img-responsive">
			<h4>10 рабочих дней</h4>
		</div>
	</div>
</div>
 <br>