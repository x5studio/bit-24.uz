<?php

class CBlogListComponent extends CBitrixComponent
{
    function GetFitSection($arrFilter)
    {
        $arSections = [];
        unset($arrFilter['SECTION_ID']);
        $rsElement = \CIBlockElement::GetList(
            array("ACTIVE_FROM" => "DESC"),
            $arrFilter,
            false,
            false,
            array("ID", "NAME", "IBLOCK_SECTION_ID")
        );
        while ($arElement = $rsElement->GetNext())
        {
            $arSections[$arElement["IBLOCK_SECTION_ID"]] = "";
        }
        return $arSections;
    }
}
