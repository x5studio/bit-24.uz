<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

	<section class="client">
		<div class="container">
			<div class="row">
				<? foreach ($arResult["ITEMS"] as $arItem): ?>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="cblock">
							<figure class="imghvr-slide-down">
								<img src="<?= $arResult["IMAGES"][$arItem["PICTURE"]] ?>" class="img-responsive" alt="<?= $arItem["NAME"] ?>">
                                <?php if (empty($arParams['FRONTPAGE_STYLE']) || $arParams['FRONTPAGE_STYLE'] != 'Y'): ?>
								    <h4><?= $arItem["NAME"] ?></h4>
								    <p><?= $arItem["DESCRIPTION"] ?></p>
                                <?php endif; ?>
                                <figcaption>
                                    <a href="<?= $arItem["SECTION_PAGE_URL"] ?>">
                                        <div class="client-snippet-former-h4"><?= $arItem["NAME"] ?></div>
                                        <p><?= $arItem["DESCRIPTION"] ?></p>
                                        <div class="btn btn-client">Подробнее</div>
                                    </a>
                                </figcaption>
							</figure>
						</div>
					</div>
				<? endforeach; ?>
			</div>
		</div>
	</section>

<?= $arResult["NAV_STRING"] ?>