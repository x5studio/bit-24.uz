<?
$curPage = rtrim($APPLICATION->GetCurPage(), '/').'/';
$includeDirLocalPath = $curPage.'/include/';
$includeDirAbsPath = $_SERVER['DOCUMENT_ROOT'].$includeDirLocalPath;

$h2IncludeFileName = 'clients__h2.php';
$moreLinkCaptionIncludeFileName = 'clients__more_link_caption.php';

if (!file_exists($includeDirAbsPath)) {
    mkdir($includeDirAbsPath);
}
?>

<section class="main-clients portal-clients">
    <div class="container">
        <div class="row why-row">
            <div class="col-md-6 col-sm-10">
                <h2 class="why-heading pull-left"><?$APPLICATION->IncludeFile($includeDirLocalPath.$h2IncludeFileName)?></h2>
            </div>
            <div class="col-md-6 col-sm-2 hidden-xs">
                <a href="/nashi-klienty/" class="btn btn-why pull-right"><?$APPLICATION->IncludeFile($includeDirLocalPath.$moreLinkCaptionIncludeFileName)?></a>
            </div>
        </div>
    </div>
    <?php
    $APPLICATION->IncludeComponent(
        "client.list",
        "",
        Array(
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "COUNT" => 6,
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "IBLOCK_ID" => "8",
            'FRONTPAGE_STYLE' => 'Y',
        )
    );
    ?>
    <div class="hidden-lg hidden-md hidden-sm col-xs-12 detline">
        <a class="btn btn-why" href="/nashi-klienty/">Подробнее</a>
    </div>
</section>