<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<section class="blog">
    <div class="container container--blog">
        <?if (!$GLOBALS['IS_BLOG_SECTION']):?>
            <h1><?=$APPLICATION->GetTitle();?></h1>
        <?endif;?>

        <?foreach ($arResult['SECTIONS'] as $arSection):?>
            <article class="blog-item">
                <?if (!$GLOBALS['IS_BLOG_SECTION']):?>
                    <a href="<?=$arSection['SECTION_PAGE_URL'];?>"><?=$arSection['NAME'];?></a>
                <?else:?>
                    <h1><?=$arSection['NAME'];?></h1>
                <?endif;?>

                <ul class="blog-item__list">
                    <template class="blog-card">
                        <li class="blog-item__item">
                            <a class="blog-item__link" href="">
                                <!-- 1x: 563px; 2x: 1126px -->
                                <img class="blog-item__img" src="" srcset="" width="" height="" alt="">
                                <div class="img-filter"></div>
                                <span class="blog-item__text"></span>
                            </a>
                        </li>
                    </template>
                </ul>
                <div class="blog-item__more-wrapper">
                    <template class="blog-more">
                        <ul class="blog-item__more"></ul>
                    </template>
                </div>

                <div class="blog-item__btn-block" style="<?=($GLOBALS['IS_BLOG_SECTION'] ? 'display:none;' : '')?>">
                    <div class="blog-item__btn-container">
                        <?if (!$GLOBALS['IS_BLOG_SECTION']):?>
                            <a class="blog-item__btn btn btn--reset btn--border-blog" href="<?=$arSection['SECTION_PAGE_URL'];?>">
                                Показать все
                            </a>
                        <?endif;?>
                        <button type="button" class="btn btn--reset btn--border-blog blog-item__btn blog-item__btn--js">Еще</button>
                    </div>

                    <ul class="pagination"></ul>
                </div>
            </article>
        <?endforeach;?>

        <?if ($GLOBALS['IS_BLOG_SECTION']):?>
            <div class="blog-detail__back">
                <a class="blog-detail__back-url" href="/blog/">вернуться в список материалов</a>
            </div>
        <?endif;?>
    </div>
</section>

