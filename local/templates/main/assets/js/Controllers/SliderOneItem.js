if (typeof define === "function" && define.amd) {
    define(['bxslider'], function () {
        function SliderOneItem(container) {
            var o = this;
            this.container = $(container);
            this.slides = $('.slides', this.container);
            this.items = $('.slide', this.slides);
            this.btnNext = $('.flex-next', this.container);
            this.btnPrev = $('.flex-prev', this.container);

            this.init = function () {
                var width = 0;
                o.items.each(function () {
                    var itemWidth = $(this).outerWidth();
                    width += itemWidth;
                });
                var adsWidthItems = width / o.items.length;

                o.slides.bxSlider({
                    slideWidth: adsWidthItems,
                    moveSlides: 1,
                    minSlides: 1,
                    maxSlides: 1,
                    slideMargin: 30,
                    infiniteLoop: true,
                    useCSS: false,
                    pager: false,
                    nextSelector: o.btnNext,
                    prevSelector: o.btnPrev,
                    onSlideBefore: function($slideElement, oldIndex, newIndex) {
                    	try {
                    		o.onSlideBefore($slideElement, oldIndex, newIndex);
                    	}
                    	catch(e) {

                    	}
                    },
                    touchEnabled : (navigator.maxTouchPoints > 0)
                });
            };

            this.init();
        }

        return SliderOneItem;
    });
}
