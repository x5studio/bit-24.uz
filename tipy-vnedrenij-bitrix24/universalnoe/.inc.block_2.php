<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h2>Наше предложение подходит для&nbsp;широкой аудитории</h2>
<div class="col-md-4 col-sm-12 col-xs-12">
	<div class="block1">
		<h3>Коммуникации</h3>
 <img src="/local/templates/main/assets/img/impl/002.png" class="img-responsive">
		<p>
			 Компаний, которым необходимо общее корпоративное пространство для работы: взаимодействия, коммуникаций, документооборота.
		</p>
	</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
	<div class="block1">
		<h3>Эффективность</h3>
 <img src="/local/templates/main/assets/img/impl/003.png" class="img-responsive">
		<p>
			 Руководителям и сотрудникам, которым необходим удобный и эффективный инструмент для ведения клиентской базы и работы с ней.
		</p>
	</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12">
	<div class="block1">
		<h3>Прибыль</h3>
 <img src="/local/templates/main/assets/img/impl/004.png" class="img-responsive">
		<p>
			 Для собственников, которые хотят увеличить прибыль компании за счет повышения качества работы отдела продаж с помощью CRM.
		</p>
	</div>
</div>
 <br>