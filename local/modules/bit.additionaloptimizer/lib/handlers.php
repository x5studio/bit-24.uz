<?

namespace Bit\AdditionalOptimizer;

use Bitrix\Main\Config\Option;
use Bit\AdditionalOptimizer\Image\ImageElement;


class Handlers{

	static function OnMakeFileArrayHandler($ar, &$arFile) {
		//print_r($arFile);
	}

	static function OnGetFileSRCHandler($arFile) {
		//print_r($arFile);
	}

	static function OnEndBufferContentHandler(&$content) {
		
		$options = Option::getForModule('bit.additionaloptimizer');

		$content = preg_replace('#>\s*\n\s*<#', '><', $content);

		if($options && $options['switch_on'] == 'Y' && !defined( "ADMIN_SECTION" )){

			$pattern = '(\<\s*?img.*?src=["\'])(\/.+?)(["\'].*?\>)';

			$content = preg_replace_callback(
				'#'.$pattern.'#s',
				function ($matches) {
					if($matches[2]) {
						return $matches[1].self::getActualImage($matches[2]).$matches[3];
					} else {
						return $matches[0];
					}
					
				},
				$content
			);
			
		}

	}

	static function OnBeforePrologHandler() {
	
		ob_start (); ?>
				<script>
				// polyfills
				(function(e) {
					var matches = e.matches || e.matchesSelector || e.webkitMatchesSelector || e.mozMatchesSelector || e.msMatchesSelector || e.oMatchesSelector;
					!matches ? (e.matches = e.matchesSelector = function matches(selector) {
						var matches = document.querySelectorAll(selector);
						var th = this;
						return Array.prototype.some.call(matches, function(e) {
							return e === th;
						});
					}) : (e.matches = e.matchesSelector = matches);
				})(Element.prototype);

				(function(ELEMENT) {
					ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
					ELEMENT.closest = ELEMENT.closest || function closest(selector) {
						if (!this) return null;
						if (this.matches(selector)) return this;
						if (!this.parentElement) {return null}
						else return this.parentElement.closest(selector)
					};
				})(Element.prototype);

				</script>
				<style>
					img[src*="aolvmod."] {
						filter: blur(15px);
						transition: filter 0.2s ease-in;
					}
				</style>
				<script>
					(function(w){
						function throttle(func, wait, options) {
							var context, args, result;
							var timeout = null;
							var previous = 0;
							if (!options) options = {};
							var later = function() {
								previous = options.leading === false ? 0 : Date.now();
								timeout = null;
								result = func.apply(context, args);
								if (!timeout) context = args = null;
							};
								return function() {
									var now = Date.now();
									if (!previous && options.leading === false) previous = now;
									var remaining = wait - (now - previous);
									context = this;
									args = arguments;
									if (remaining <= 0 || remaining > wait) {
									if (timeout) {
										clearTimeout(timeout);
										timeout = null;
									}
									previous = now;
									result = func.apply(context, args);
									if (!timeout) context = args = null;
									} else if (!timeout && options.trailing !== false) {
									timeout = setTimeout(later, remaining);
									}
									return result;
							};
						}
						var imageOptimizeLoader = function(){

							this.webpSupport = false;
							this.observer = false;
							this.lazyLoadSrc = 'aolvmod.';
							this.webpSrc = '.webp';
							this.imageSelector = 'img[src*="' + this.lazyLoadSrc + '"]';


							this.checkWebpFeature('lossy', function(feature, isSupported){
								this.webpSupport = isSupported;
								if(!isSupported) this.imageSelector += ', img[src*="' + this.webpSrc + '"]';
							}.bind(this))

						}


						imageOptimizeLoader.prototype.checkImageVisible = function (node) {
							
							if(node) {

								var percentVisible = 30,
									rect = node.getBoundingClientRect(),
									windowHeight = (window.innerHeight || document.documentElement.clientHeight);

								return !(
									Math.floor(100 - (((rect.top >= 0 ? 0 : rect.top) / +-(rect.height / 1)) * 100)) < percentVisible ||
									Math.floor(100 - ((rect.bottom - windowHeight) / rect.height) * 100) < percentVisible
								)

							} else {

								return false;

							}


						}

						imageOptimizeLoader.prototype.checkImages = function () {
							
							var container = w.document.documentElement || w.document.body;

							var images = container.querySelectorAll(this.imageSelector);

							if(images) {

								for (var i in images) {

									var image = images[i];

									if (!(image instanceof HTMLElement)) continue;

									if(this.checkImageVisible(image)){
										
										this.loadImage(image);

									}

								}

							}

						}

						imageOptimizeLoader.prototype.loadImage = function (node) {

							var src = node.src;
							
							if(src) {

								var loadingSrc = src.replace(this.lazyLoadSrc, '');
								
								if(!this.webpSupport) loadingSrc = loadingSrc.replace(this.webpSrc, '');
								
								node.src = loadingSrc;
								
							}

						}

						imageOptimizeLoader.prototype.checkWebpFeature = function (feature, callback) {
							
							var kTestImages = {
								lossy: "UklGRiIAAABXRUJQVlA4IBYAAAAwAQCdASoBAAEADsD+JaQAA3AAAAAA",
								lossless: "UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA==",
								alpha: "UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAARBxAR/Q9ERP8DAABWUDggGAAAABQBAJ0BKgEAAQAAAP4AAA3AAP7mtQAAAA==",
								animation: "UklGRlIAAABXRUJQVlA4WAoAAAASAAAAAAAAAAAAQU5JTQYAAAD/////AABBTk1GJgAAAAAAAAAAAAAAAAAAAGQAAABWUDhMDQAAAC8AAAAQBxAREYiI/gcA"
							};
							var img = new Image();
							img.onload = function () {
								var result = (img.width > 0) && (img.height > 0);
								callback(feature, result);
							};
							img.onerror = function () {
								callback(feature, false);
							};
							img.src = "data:image/webp;base64," + kTestImages[feature];
						}

						imageOptimizeLoader.prototype.initObserver = function (container) {

							if(container){

								this.observer = new MutationObserver(function(mutations) {

									mutations.forEach(function(mutation) {

										for (var nodeIteralor in mutation.addedNodes) {

											var node =  mutation.addedNodes[nodeIteralor]

											if (!(node instanceof HTMLElement)) continue;

											if (node.matches(w.imageOptimizeLoader.imageSelector)) {

												w.imageOptimizeLoader.checkImages();

											}

										};
										
									});

								});

								this.observer.observe(container, {childList: true, subtree: true});
							}

						}

						w.imageOptimizeLoader = new imageOptimizeLoader();

						document.addEventListener("DOMContentLoaded", function(event) {

							var container = document.documentElement || document.body;

							w.imageOptimizeLoader.initObserver(container);
							
							w.imageOptimizeLoader.checkImages();

						});

						w.addEventListener('scroll', throttle(w.imageOptimizeLoader.checkImages, 300).bind(w.imageOptimizeLoader));
						w.addEventListener('resize', throttle(w.imageOptimizeLoader.checkImages, 300).bind(w.imageOptimizeLoader));
						
					})(window)
				</script>
		<?
		$string = ob_get_contents();

		ob_end_clean();
		
		$GLOBALS['APPLICATION']->AddHeadString($string);

	}


	public function getActualImage(string $path = ''){

		$options = Option::getForModule('bit.additionaloptimizer');

		if($options && $options['switch_on'] == 'Y'){

			if($path && strpos($path, '/bitrix/') === false) { 

				$additionalParams = [];

				$imageElement = new ImageElement($path, ($options['image_optimize_on'] == 'Y'), ($options['image_webp_on'] == 'Y' && Supports::checkWebP()), ($options['image_lazyload_on'] == 'Y'), $additionalParams);

				return $imageElement->getActualImagePath();

			}

		}

		return $path;

	}
}
