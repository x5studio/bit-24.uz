<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

?>
<div class="contact-box-dropdown">
	<div class="box-offices j-box-offices double-office">
		<div class="box-content">
			<div class="contacts-bg">
				<a class="phone-bg" href="#"><?php echo \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_DL_PHONES'); ?></a>
			</div>
			<div class="close"></div>
			<?$chunk = array_chunk($arResult["ITEMS"], 2);?>
			<div id="contact-table-box-container">
				<?$frame = $this->createFrame("contact-table-box-container", false)->begin('');?>
				<table class="table-office-col">
					<?foreach($chunk as $arRes):?>
						<tr>
							<?foreach($arRes as $arItem):?>
								<td class="office-col">
									<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
										<div class="box-offices-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                                            <span<?if(!empty($arItem["PROPERTIES"]["ICON_METRO"]["VALUE"])){ echo ' class="metro metro-'.$arItem["PROPERTIES"]["ICON_METRO"]["VALUE"].'"';}?>><?=$arItem['NAME']?></span>
											<strong class="box-office-phone <?if(!empty($arParams["HTML_PHONE_VIEW"])){ echo $arParams["HTML_PHONE_VIEW"];}?>">
												<?=$arItem['PROPERTIES']['phone']['VALUE']?>
											</strong>
										</div>
									</a>
								</td>
							<?endforeach;?>
						</tr>
					<?endforeach;?>
				</table>
				<div style="clear: both;"></div>
				<?if($arResult['OFFICE_TEXT']):?>
					<div class="box-offices-control"><span><?=$arResult['OFFICE_TEXT']?></span></div>
				<?endif;?>
				<?$frame->end();?>
			</div>
		</div>
	</div>
</div>