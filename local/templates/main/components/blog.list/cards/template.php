<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<style>
	.blog-articles-wrap article {
		height: 420px;
	}
	.blog-articles-wrap .article-content {
		height: 280px;
	}
	.blog-articles-wrap  .article-details {
		height: 140px;
		background-color: #EFF2F5;
		padding: 20px;
		position: relative;
	}
	.blog-articles-wrap .blog-article .title {
		/* position: static; */
		color: #324abd;
		display: block;
		font-size: 24px;
		overflow: hidden;
		height: 80%;
		position: relative;
		top: 0;
		left: 0;
	}
	.blog-articles-wrap .blog-article .title:after {
		content: "";
		position: absolute;
		left: 0;
		right: 0;
		bottom: 0;
		height: 22px;
		z-index: 2;
		background: rgba(255, 255, 255, 0);
		background: -webkit-linear-gradient(bottom, #eff2f5, rgba(255, 255, 255, 0)); /* Safari 5.1, iOS 5.0-6.1, Chrome 10-25, Android 4.0-4.3 */
		background: -moz-linear-gradient(bottom, #eff2f5, rgba(255, 255, 255, 0)); /* Firefox 3.6-15 */
		background: -o-linear-gradient(bottom, #eff2f5, rgba(255, 255, 255, 0)); /* Opera 11.1-12 */
		background: linear-gradient(to top, #eff2f5, rgba(255, 255, 255, 0)); /* Opera 15+, Chrome 25+, IE 10+, Firefox 16+, Safari 6.1+, iOS 7+, Android 4.4+ */
	}


	.blog-articles-wrap .date {
		margin-left: 0;
		color: #000;
		    position: absolute;
		bottom: 10px;
		margin-bottom: 0;
	}
	.blog-articles-wrap .date:before {
		content: url(/images/ico-clock-grey.png);
	}
	.blog-articles-wrap .tag-date-wrap {
		left: 20px;
		top: 20px;
	}
	.blog-articles-wrap .transparent-bg p {
		margin-top: 20px;
	}


	ul.sections {
		list-style: none;
		padding: 0;
		margin-bottom: 20px;
	}
	ul.sections li {
		display: inline-block;
		margin-right: 10px;
	}
	ul.sections li a {
		border-bottom: 1px dotted #324abd;
		font-weight: 500;
		color: #324abd;
	}
	ul.sections li a:hover {
		border-bottom: none;
		text-decoration: none;
		color: #ae127b;
	}
	ul.sections li span {
		color: #ae127b;
	}
</style>

<? if(is_array($arResult['SECTIONS'])): ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="sections">
					<? foreach ($arResult['SECTIONS'] as $arSection): ?>
						<li>
							<? if ($arSection['CURRENT'] != 'Y'): ?>
								<a href="<?= $arSection['SECTION_PAGE_URL'] ?>" title="<?= $arSection['NAME'] ?>"><?= $arSection['NAME'] ?></a>
							<? else: ?>
								<span><?= $arSection['NAME'] ?></span>
							<? endif; ?>
						</li>
					<? endforeach; ?>
				</ul>
			</div>
		</div>
	</div>
<? endif; ?>

<div class="container">
	<div class="row">

		<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
			<? if ($cell == 0): ?>
				<article class="blog-article long-article with-bg col-sm-8">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ? $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] : $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag deep-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>


			<? elseif ($cell == 1): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? elseif ($cell == 2): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? elseif ($cell == 3): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? endif; ?>



			<? if ($cell == 4): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? elseif ($cell == 5): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? elseif ($cell == 6): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? elseif ($cell == 7): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? endif; ?>
		<? endforeach; ?>
	</div>
</div>

<?
$APPLICATION->IncludeComponent("asd:subscribe.quick.form", "blog_list", Array(
	"FORMAT" => "html", // Формат подписки
	"INC_JQUERY" => "N", // Подключить jQuery
	"NOT_CONFIRM" => "N", // Подписывать без подтверждения
	"RUBRICS" => array(// Подписывать на рубрики
		0 => "1",
	),
	"SHOW_RUBRICS" => "N", // Показывать рубрики
	), false
);
?>

<div class="container">

	<div class="row">
		<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
			<? if ($cell == 8): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? elseif ($cell == 9): ?>
				<article class="blog-article long-article with-bg col-sm-8">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ? $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] : $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag deep-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? elseif ($cell == 10): ?>
				<article class="blog-article long-article with-bg col-sm-8">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ? $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] : $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag deep-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? elseif ($cell == 11): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
						</div>
					</a>
					<div class="article-details">
						<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="title"><?= $arItem["NAME"] ?></a>
						<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
					</div>
				</article>
			<? endif; ?>
		<? endforeach; ?>
	</div>


	<nav class="pagination-row clearfix">
		<?= $arResult["NAV_STRING"] ?>
		<? /*
		  <a class="previous hidden-xs" href="#"><span aria-hidden="true"><< </span><span class="hidden-xs" aria-hidden="true">Предыдущая</span></a>
		  <ul class="pagination">
		  <li><a class="active">1</a></li>
		  <li><a href="#">2</a></li>
		  <li><a href="#">3</a></li>
		  <li><a href="#">4</a></li>
		  <li><span class="dots">...<span></li>
		  <li><a href="#">17</a></li>
		  </ul>
		  <a class="next hidden-xs" href="#"><span class="hidden-xs" aria-hidden="true">Следующая</span><span aria-hidden="true"> >></span></a>
		 */ ?>
	</nav>

</div>
