if (typeof define === "function" && define.amd) {
    define(['module'], function (module) {
        function Responsive(p) {
            if (Responsive.instance) {
                return Responsive.instance
            }
            Responsive.instance = this;

            this.p = p;
            var o = this;

            this.document = $(document);

            this.curName = o.checkType();
            this.lastName = null;

            this.handlers = new ResponsiveHandler();

            this.init();
        }

        function ResponsiveHandler() {
            this.xsmall = [];
            this.small = [];
            this.medium = [];
            this.large = [];
        }

        var borderTypes = module.config().borderTypes;
        if (typeof borderTypes == 'object') {
            Responsive.prototype.borderTypes = borderTypes;
        } else {
            Responsive.prototype.borderTypes = {
                xsmall: [0, 767],
                small: [768, 991],
                medium: [992, 1199],
                large: [1200, 99999]
            };
        }

        Responsive.prototype.on = function (arType, method, fn, args, context) {
            var o = this;
            method = o.prepareMethod(method);
            if (typeof method != 'string') {
                return o.onBoth.apply(o, arguments);
            }
            arType = o.prepareType(arType);
            fn = o.prepareFn(fn);
            fn.args = args;
            fn.context = context;

            method = o.makeFnName(method);

            for (var i in arType) {
                var type = arType[i];
                if (type in o.handlers) {
                    var handler = {};
                    handler[method] = fn;
                    o.handlers[type].push(handler)
                } else {
                    throw new TypeError('type is not a correct value');
                }
            }
            if (method == "fnIn" && $.inArray(o.lastName, arType) >= 0) {
                fn.apply(fn.context || window, fn.args);
            }
        };

        Responsive.prototype.one = function (arType, method, fn, args, context) {
            var o = this;
            method = o.prepareMethod(method);
            if (typeof method != 'string') {
                return o.oneBoth.apply(o, arguments);
            }
            arType = o.prepareType(arType);
            fn = o.prepareFn(fn);

            var origFn = fn;
            origFn.args = args;
            origFn.context = context;
            var fn = function () {
                o.off(arType, method, fn);
                return origFn.apply(origFn.context || window, origFn.args);
            };

            o.on(arType, method, fn, args, context);
        };
        Responsive.prototype.off = function (arType, method, fn) {
            var o = this;
            method = o.prepareMethod(method);
            if (typeof method != 'string') {
                return o.offBoth.apply(o, arguments);
            }
            arType = o.prepareType(arType);
            fn = o.prepareFn(fn);

            method = o.makeFnName(method);

            for (var i in arType) {
                var type = arType[i];
                if (type in o.handlers) {
                    for (var j in o.handlers[type]) {
                        var item = o.handlers[type][j];
                        if (fn == item[method]) {
                            item[method] = undefined;
                        }
                        if (item.fnIn === item.fnOut && typeof item.fnIn == 'undefined') {
                            o.handlers[type].splice(j, 1);
                        }
                    }
                } else {
                    throw new TypeError('type is not a correct value');
                }
            }
        };

        Responsive.prototype.onBoth = function (arType, fnIn, fnOut) {
            var o = this;
            var fnBoth = o.prepareFnBoth(fnIn, fnOut);
            fnIn = fnBoth[0];
            fnOut = fnBoth[1];

            if (typeof fnIn == 'function') {
                o.on(arType, 'in', fnIn);
            }
            if (typeof fnOut == 'function') {
                o.on(arType, 'out', fnOut);
            }
        };

        Responsive.prototype.oneBoth = function (arType, fnIn, fnOut) {
            var o = this;
            var fnBoth = o.prepareFnBoth(fnIn, fnOut);
            fnIn = fnBoth[0];
            fnOut = fnBoth[1];

            if (typeof fnIn == 'function') {
                o.one(arType, 'in', fnIn);
            }
            if (typeof fnOut == 'function') {
                o.one(arType, 'out', fnOut);
            }
        };
        Responsive.prototype.offBoth = function (arType, fnIn, fnOut) {
            var o = this;
            var fnBoth = o.prepareFnBoth(fnIn, fnOut);
            fnIn = fnBoth[0];
            fnOut = fnBoth[1];

            if (typeof fnIn == 'function') {
                o.off(arType, 'in', fnIn);
            }
            if (typeof fnOut == 'function') {
                o.off(arType, 'out', fnOut);
            }
        };

        Responsive.prototype.makeFnName = function (method) {
            return 'fn' + method[0].toUpperCase() + method.substr(1);
        };

        Responsive.prototype.prepareType = function (arType) {
            if (typeof arType != 'string' && !(arType instanceof Array)) {
                throw new TypeError('arType is not a correct value');
            }
            if (typeof arType == 'string') {
                arType = [arType];
            }
            return arType;
        };

        Responsive.prototype.prepareMethod = function (method) {
            return method;
        };

        Responsive.prototype.prepareFn = function (fn) {
            if (typeof fn != 'function') {
                throw new TypeError('fn is not a correct value');
            }
            return fn;
        };

        Responsive.prototype.prepareFnBoth = function (fnIn, fnOut) {
            if (typeof fnIn != 'function' && typeof fnOut != 'function')
                throw new TypeError('fnIn and fnOut is not a correct value');
            return [fnIn, fnOut];
        };

        Responsive.prototype.checkType = function () {
            var o = this;
            var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
            for (var typeName in o.borderTypes) {
                var borderType = o.borderTypes[typeName];
                if (borderType[0] <= width && width <= borderType[1]) {
                    return typeName;
                }
            }
        };

        Responsive.prototype.do = function () {
            var o = this;
            var typeName = o.checkType();
            if (o.lastName != typeName) {
                o.curName = typeName;
                o.document.trigger("responsive." + typeName);
                o.lastName = typeName;
            }
        };

        Responsive.prototype.makeHandlers = function (arType, method) {
            var o = this;
            if (typeof arType == 'string') {
                arType = [arType];
            }
            method = o.makeFnName(method);
            for (var i in arType) {
                var type = arType[i];
                o.handlers[type].reverse();
                var j = o.handlers[type].length;
                while (j--) {
                    var item = o.handlers[type][j];
                    if (typeof item[method] == 'function') {
                        item[method].apply(item[method].context || window, item[method].args);
                    }
                }
                o.handlers[type].reverse();
            }
        };

        Responsive.prototype.getCurType = function () {
            return this.curName;
        };

        Responsive.prototype.getLastType = function () {
            return this.lastName;
        };

        Responsive.prototype.init = function () {
            var o = this;

            o.document.on('responsive.xsmall.small.medium.large', function (event) {
                if (o.lastName) {
                    o.makeHandlers(o.lastName, 'out');
                }
                o.makeHandlers(event.namespace, 'in');
            });

            $(window).on('resize', function () {
                o.do();
            });
            o.do();
        };

        var obResponsive = new Responsive;
        return obResponsive;
    });
}