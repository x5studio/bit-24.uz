if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/Observable', 'tools/tools'], function ($, Observable) {

        var items = [],
            dfSetItems = $.Deferred();

        function PagingStore(options) {
            Observable.apply(this, arguments);
            this.options = $.extend(true, {}, PagingStore.defaultOptions, options);
            this.init();
        }

        extend(PagingStore, Observable);

        PagingStore.defaultOptions = {};

        PagingStore.prototype.doneSetItems = function (fn) {
            if (typeof fn != 'function') {
                throw new TypeError();
            }
            dfSetItems.done(fn);
            return dfSetItems.promise();
        };

        PagingStore.prototype.findItemById = function (id) {
            return findItem(items, id);
        };

        PagingStore.prototype.getItems = function () {
            return items;
        };

        PagingStore.prototype.setItems = function (innerItems) {
            var self = this;
            items = innerItems;
            for (var i in items) {
                var item = items[i];
                self.triggerEvent('init', [item]);
            }

            if (dfSetItems.state() == 'pending') {
                dfSetItems.resolve(items);
            }
        };

        PagingStore.prototype.setItem = function (item) {
            var self = this;
            items.push(item);
            self.triggerEvent('init', [item]);
        };

        PagingStore.prototype.insertItem = function (item) {
            var self = this;

            items.push(item);
            self.triggerEvent('insert', [item]);
        };

        PagingStore.prototype.updateItem = function (props) {
            var self = this;
            items = [props];
            self.triggerEvent('update', items);
        };

        PagingStore.prototype.subscribeEvent = function () {
            var self = this;
            $(document).on('updateList', function (e, param) {

                if(items.length > 0){
                    self.updateItem(param.page);
                } else {
                    self.insertItem(param.page);
                }

            })
        };

        PagingStore.prototype.init = function () {
            this.subscribeEvent();
        };

        return PagingStore;
    });
}