if (typeof define === "function" && define.amd) {
    define(['Controls/Responsive', 'fancybox'], function (obResponsive) {

        function Popup(container, options) {
            var o = this;
            this.container = $(container);
            var dataOptions = this.container.data();
            this.options = $.extend(true, {}, Popup.defaultOptions, options, dataOptions);
            this.options.fbOptions.afterLoad = function () {
                this.context = o;
                $.proxy(o.options.fbOptions.afterLoad, this);
            };

            this.btnClose = $(this.options.closeSelector, this.container);
            this.title = $(this.options.titleSelector, this.container);
            this.content = $(this.options.contentSelector, this.container);

            this.model = {};
        }

        Popup.defaultOptions = {
            closeSelector: '.popup-close',
            titleSelector: '.popup-title',
            contentSelector: '.popup-content',
            fbOptions: {
                padding: 0,
                margin: 0,
                autoSize: true,
                closeBtn: false,
                topRatio: 0.5,
                openEffect: 'none',
                closeEffect: 'none',
                afterLoad: Popup.afterLoad
            }
        };

        Popup.prototype.setOptions = function (options) {
            if (typeof options != 'object') {
                throw new TypeError();
            }
            $.extend(this.options, options);
        };

        Popup.prototype.getView = function () {
            return this.container;
        };

        Popup.prototype.getModel = function () {
            return this.model;
        };

        Popup.prototype.setModel = function (item) {
            this.model = item;
            var view = this.getView();
            if (typeof view == 'object' && view.length > 0) {
                this.render();
            }
        };

        Popup.prototype.render = function () {
            var o = this;

            o.title.text(o.getModel()['title']);
            o.content.html(o.getModel()['content']);
        };

        Popup.prototype.show = function (options) {
            var o = this;

            var fbOptions = $.extend(true, {}, o.options.fbOptions, options);
            if ($.inArray(obResponsive.getCurType(), ['xsmall', 'small']) >= 0) {
                fbOptions['topRatio'] = 0;
            }
            $.fancybox.open(o.container, fbOptions);
        };

        Popup.prototype.afterLoad = function () {

        };

        Popup.prototype.hide = function () {
            $.fancybox.close();
        };

        Popup.prototype.remove = function () {
            this.getView().remove();
        };

        Popup.prototype.detach = function () {
            this.getView().detach();
        };

        Popup.prototype.init = function () {
            var o = this;
            o.btnClose.click(function () {
                o.hide();
            });
        };

        return Popup;
    });
}