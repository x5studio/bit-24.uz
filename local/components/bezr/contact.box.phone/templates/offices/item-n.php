<div class="box-offices j-box-offices double-office">
		<?
		$APPLICATION->IncludeComponent("vitams:city.dropdown", "city_simple",
			Array(
				"IBLOCK_TYPE" => IBT_CONTACTS,
				"IBLOCK_ID" => IB_CONTACTS,
				"SECTION_CODE" => "",
				"SECTION_URL" => "",
				"COUNT_ELEMENTS" => "Y",
				"TOP_DEPTH" => "1",
				"SECTION_FIELDS" => "",
				"FILTER_NAME" => "cityFilter",
				"SECTION_USER_FIELDS" => "",
				"ADD_SECTIONS_CHAIN" => "Y",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_NOTES" => "",
				"CACHE_GROUPS" => "Y",
				"LINK_IN_BASEURL" => "Y",
				"LINK_WITH_SUBDOMAIN" => "Y",
				"SEF_MODE" => "N",
				"SEF_FOLDER" => $APPLICATION->GetCurPageParam(false, array(), false),
				"HTML_LIST_ID" => "header-city-list-inner",
				"HTML_TITLE" => '<div class="box-title h2"><div class="short">' . \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_CONTACTS') . '</div><div class="full">' . \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_OFFICE_IN') . ' <a href="##HTML_LIST_ID#" class="city j-city"><b>#NAME_DATIVE#</b></a></div></div>',
				"BY_CACHE_SUBDOMAIN_PAGE" => $_SESSION["REGION"]["CODE"],
				"SHOW_MOSCOW" => $_SESSION['SHOW_MOSCOW'],
				"COUNTRY_CODE" => $_SESSION['GEOIP']['country_code'],
				'UI_CITY_STRONG' => $arParams['CITY_DROPDOWN_UI_CITY_STRONG'],
				'UI_CITY_SPACE' => $arParams['CITY_DROPDOWN_UI_CITY_SPACE'],
			)
		);?>
	<div class="box-content">
		<div class="contacts-bg">
			<a class="link-bg" href="/contacts/" target="_blank"><?//=$count?><?php echo \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_OUR_OFFICES'); ?></a>
			<a class="phone-bg" href="#"><?php echo \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_PHONES'); ?></a>
		</div>
		<div class="close"></div>
			<?$chunk = array_chunk($arResult["ITEMS"], 2);?>
			<table class="table-office-col">
			<?foreach($chunk as $arRes):?>
				<tr>
					<?foreach($arRes as $arItem):?>
						<?
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => PB\Main\Loc::getMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
						?>
						<td class="office-col">
							<?if($arItem["CODE"]=="proletarskaya"):?>
								<div class="box-office-main">
									<div class="strong"><?php echo \PB\Main\Loc::getMessage('PB_CONTACTS_CT_CB_OFF_MAIN_OFFICE'); ?></div>
								</div>
							<?endif;?>
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="box-office-link">
								<div class="box-offices-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>" data-item-id="<?=$arItem['ID']?>">
									<span class="box-office-name<?if(!empty($arItem["PROPERTIES"]["ICON_METRO"]["VALUE"])){ echo ' metro metro-'.$arItem["PROPERTIES"]["ICON_METRO"]["VALUE"];}?>"><?=$arItem['NAME']?></span>
									<span class="strong box-office-phone"><?=$arItem['PROPERTIES']['phone']['VALUE']?></span>
								</div>
							</a>
						</td>
					<?endforeach;?>
				</tr>
			<?endforeach;?>
			</table>
		<div style="clear: both;"></div>
		<?if($arResult['OFFICE_TEXT']):?>
			<div class="box-offices-control"><span><?=$arResult['OFFICE_TEXT']?></span></div>
		<?endif;?>
	</div>
</div>
