<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок",
			"TYPE" => "STRING",
			"DEFAULT" => "",
		),
		"SECTION_CODE" => Array(
			"PARENT" => "BASE",
			"NAME" => "Символьный код раздела",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"ELEMENT_CODE" => Array(
			"PARENT" => "BASE",
			"NAME" => "Символьный код элемента",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);
?>
