/**
 * Send goals data to google and yandex
 * @usage
 *  require(['Controllers/SeoTrackHelper'], function(SeoTrackHelper) {
        window.SeoTrackHelper = new SeoTrackHelper();
        window.SeoTrackHelper.counters = {
            yandex: window.yaCounter29316340
        }

        window.SeoTrackHelper.goalsData = {
            'open-5feedback':{
                'yandex': {
                    code: 'call-click'
                },
            },
            'send-5feedback':{
                'yandex': {
                    code: 'call-send'
                },
            },
            'send-4request':{
                'yandex': {
                    code: 'podbor-send'
                },
            },
            'open-default':{
                'yandex': {
                    code: 'feedback-click'
                },
            },
            'send-default':{
                'yandex': {
                    code: 'feedback-send'
                },
            }
        }
    });

 *****
 // subscribe to result popup event
 $(document).on("form.result.popup.open", function (event, resultArr) {
        if(!resultArr.error) {
            console.log('popop sended');
            window.SeoTrackHelper.sendGoal('send',resultArr.formSalt);
        }
    });
 // subscribe to open popup event
 $(document).on("afterFormLoad", function (event) {
        console.log('popop open');
        window.SeoTrackHelper.sendGoal('open',$(event.target).data('saltForm'));

    });

 */
if (typeof define === "function" && define.amd) {
    define(['jquery'], function ($) {
        function SeoTrackHelper() {
            this.counters = {}
            this.goalsData = {}
        }

        /**
         *
         * @param type [open|send] - open or send form
         * @param goal
         */
        SeoTrackHelper.prototype.sendGoal = function(type,goal) {
            this.yandexSendGoal(type,goal);
        }

        SeoTrackHelper.prototype.yandexSendGoal = function(type,goal) {
            let goalData = this.getGoalData(type,goal, 'yandex');

            if (goalData && typeof this.counters.yandex === 'object') {
                this.counters.yandex.reachGoal(goalData.code);
            }
        }

        SeoTrackHelper.prototype.getGoalData = function(type,goal, counter) {
            var ret=null;
            if(typeof this.goalsData[type+'-'+goal] !== 'undefined'){
                ret= this.goalsData[type+'-'+goal][counter];
            }else if(typeof this.goalsData[type+'-default'] !== 'undefined'){
                ret= this.goalsData[type+'-default'][counter];
            }else {
                console.log('default goals data is undefined');
            }
            return ret;
        }

        return SeoTrackHelper;
    });
}