<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Экспресс внедрение за 7 дней. На выбор предлагаем 2 пакета стоимость 38990 руб. и 65990 руб. Обращайтесь к нам!");
$APPLICATION->SetPageProperty("title", "Экспресс внедрение за 7 дней - специальное предложение от 38990 руб.");
$APPLICATION->SetTitle("Разработка проектов на Битрикс24 для крупных организаций ");
?>
    <link href="/local/templates/main/assets_new/css/landing_b24_style.css" type="text/css"  rel="stylesheet" />
    <script type="text/javascript" src="/local/templates/main/components/bitrix/news.list/opportunities/script.js"></script>
    <section class="line-header line-header__section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="title-page">
                        <h1 class="title-page__h1">Экспресс-внедрение Битрикс24</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="buy-license vnedrenie">
        <div class="container">
            <div class="buy-license__text-content buy-license__text-content-nonshadowmob mbot30">
                <p>Проанализируем и оптимизируем процесс ваших продаж, настроим систему и обучим Ваших сотрудников за 7 дней!</p>
            </div>
            <div class="buy-license__text tariffs-wrapper">
                <table class="table table-responsive tariffs ">
                    <thead>
                        <tr>
                            <th>Услуги</th>
                            <th>Пакет “Стандарт”</th>
                            <th>Пакет “Плюс”</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>Создание CRM Битрикс24</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Лицензия «Базовая»</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Лицензия «Стандартный»</th>
                        <td></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Добавление сотрудников в CRM</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Анализ текущей схемы продаж</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Создание эффективной схемы работы в Битрикс24</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Подключаем заявки с сайта и почты</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Подключение IP Телефонии<br />(записи звонков, аналитика по звонкам)</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Настройка воронки продаж</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Настройка карточки клиента<br />(Лиды,Сделки, Контакты, Компании)</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Инструкция для подключения личной почты к Битрикс24</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Обучение в формате Zoom/Skype</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Настройка прав доступа</th>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Загрузка клиентской базы</th>
                        <td></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Интеграция с мессенджерами и социальными сетями</th>
                        <td></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Автоматизация воронки продаж (автоформирование документов, отправка смс и писем.)</th>
                        <td></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th>Консультационная поддержка 1 мес</th>
                        <td></td>
                        <td><i class="fa fa-plus-circle" aria-hidden="true"></i></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td class="price_col"><span class="real_price">38 990 ₽</span></td>
                        <td class="price_col"><span class="real_price">65 990 ₽</span></td>
                    </tr>
                    <tr>
                        <th></th>
                        <td><button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Заказать Пакет Стандарт" data-toggle="modal">Заказать</button></td>
                        <td><button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Заказать Пакет Плюс" data-toggle="modal">Заказать</button></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="buy-license__text mtop20 mobile-mbot30">
                <div class="buy-license__caption">Этапы внедрения пакета “Стандарт” и “Плюс”:</div>
                <div class="buy-license__sub-caption">Моделирование воронки продаж:</div>
                <div class="tree">
                    <div class="tree__item vnedrenie-tree">
                        Изучение текущего процесса заказчика
                    </div>
                    <div class="tree__item vnedrenie-tree">
                        Определение ключевых процессов
                    </div>
                    <div class="tree__item vnedrenie-tree">
                        Создание максимально эффективной схемы работы в Битрикс24
                    </div>
                </div>
            </div>
            <section class="opportunities vnedrenie-slider">
                <div class="container">
                    <div class="row tabs-wrapper">
                        <div class="col-md-5">
                            <h2 class="why-heading">Настройка CRM:</h2>
                            <ul class="tabs">
                                <li class="tab active"><span>01.</span>Заведение сотрудников в CRM</li>
                                <li class="tab"><span>02.</span>Настройка карточек</li>
                                <li class="tab"><span>03.</span>Настройка типовых фильтров</li>
                                <li class="tab"><span>04.</span>Настройка стадий лидов</li>
                                <li class="tab"><span>05.</span>Подключение SIP-телефонии</li>
                                <li class="tab"><span>06.</span>Подключение заявки</li>
                                <li class="tab"><span>07.</span>Настройка прав</li>
                                <li class="tab"><span>08.</span>Создание инструкции</li>
                                <li class="tab"><span>09.</span>Подключение сервиса обратного звонка</li>
                                <li class="tab"><span>10.</span>Настройка мониторинга основных показателей</li>
                            </ul>
                        </div>
                        <div class="col-md-7">
                            <div class="tab-outer">
                                <div class="tab-content">
                                    <div class="tab-item">
                                        <div class="card">
                                            <div class="card-num">01.</div>
                                            <div class="card-heading">Заведение сотрудников в CRM</div>
                                            <div class="desc"><p>Заведение сотрудников в CRM. Занесение в структуру компании и назначение руководителей (до 25 пользователей)</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-item">
                                        <div class="card">
                                            <div class="card-num">02.</div><div class="card-heading">Настройка карточек</div>
                                            <div class="desc"><p>Настройка карточек:лиды, контакты, сделки, компании. Оставляем только необходимые поля для фиксирования необходимой информации о клиенте (не более 10 пользовательских полей)</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-item">
                                        <div class="card">
                                            <div class="card-num">03.</div>
                                            <div class="card-heading">Настройка типовых фильтров</div>
                                            <div class="desc"><p>Настройка типовых фильтров лидов, сделок, дел</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-item">
                                        <div class="card">
                                            <div class="card-num">04.</div>
                                            <div class="card-heading">Настройка стадий лидов</div>
                                            <div class="desc">Настройка стадий лидов и сделок</div>
                                        </div>
                                    </div>
                                    <div class="tab-item">
                                        <div class="card">
                                            <div class="card-num">05.</div>
                                            <div class="card-heading">Подключение SIP-телефонии</div>
                                            <div class="desc"><p>Подключаем штатную или внешнюю SIP-телефонию (при наличии приложения в Маркетплейс Битрикс24)</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-item" style=""><div class="card"><div class="card-num">06.</div>
                                            <div class="card-heading">Подключение заявки</div>
                                            <div class="desc"><p>Подключаем заявки по электронной почте</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-item">
                                        <div class="card">
                                            <div class="card-num">07.</div>
                                            <div class="card-heading">Настройка прав</div>
                                            <div class="desc"><p>Настройка прав доступа для 2 ролей (доп роль 1 час)</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-item">
                                        <div class="card">
                                            <div class="card-num">08.</div>
                                            <div class="card-heading">Создание инструкции</div>
                                            <div class="desc"><p>Напишем инструкцию для подключения почтовых ящиков сотрудников к Битрикс24</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-item">
                                        <div class="card">
                                            <div class="card-num">09.</div>
                                            <div class="card-heading">Подключение сервиса обратного звонка</div>
                                            <div class="desc"><p>Подключаем сервис обратного звонка, онлайн-чат, viber, telegram на один сайт компании, интегрируем с Битрикс24 CRM</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-item">
                                        <div class="card">
                                            <div class="card-num">10.</div>
                                            <div class="card-heading">Настройка мониторинга основных показателей</div>
                                            <div class="desc"><p>Настройка мониторинга основных показателей (crm аналитика в Битрикс24)</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
            </section>
            <div class="mtop80 mobile-mbot30 buy-license__training">
                <div class="buy-license__caption">Обучение работе в Битрикс24:</div>
                <div class="buy-license__training-items">
                    <div class="buy-license__training-item">
                        <div class="buy-license__training-counter">1</div>
                        <div class="buy-license__training-text">Основной интерфейс в системе</div>
                    </div>
                    <div class="buy-license__training-item">
                        <div class="buy-license__training-counter">2</div>
                        <div class="buy-license__training-text">Настройка главного меню и интерфейса в crm</div>
                    </div>
                    <div class="buy-license__training-item">
                        <div class="buy-license__training-counter">3</div>
                        <div class="buy-license__training-text">Настройка личной страницы и уведомлений</div>
                    </div>
                    <div class="buy-license__training-item">
                        <div class="buy-license__training-counter">4</div>
                        <div class="buy-license__training-text">Работа в CRM и почтой</div>
                    </div>
                    <div class="buy-license__training-item">
                        <div class="buy-license__training-counter">5</div>
                        <div class="buy-license__training-text">Базовая работа с Задачами и календарем</div>
                    </div>
                    <div class="buy-license__training-item">
                        <div class="buy-license__training-counter">6</div>
                        <div class="buy-license__training-text">Работы с облачным диском</div>
                    </div>
                </div>
                <div class="tariffs-wrapper tariffs-wrapper__button mtop40">
                    <button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Оставить заявку" data-toggle="modal">Оставить заявку</button>
                </div>
            </div>
        </div>
    </section>
    <section class="buy-license__results">
        <div class="container">
            <div class="buy-license__caption">Результат:</div>
            <div class="buy-license__results-items">
                <div class="buy-license__results-item">
                    Заявки из всех источников (телефония, мессенджеры, сайт, почта) попадают в единую систему.
                </div>
                <div class="buy-license__results-item">
                    Информация о клиенте и коммуникациях (в т.ч. записи звонков) автоматически заносится в карточку и не может быть удалена менеджером.
                </div>
                <div class="buy-license__results-item">
                    Выстроена воронка от первого обращения клиента до оплаты. Система сама напомнит менеджеру позвонить, написать письмо, выслать КП.
                </div>
                <div class="buy-license__results-item">
                    Вся аналитика доступна в 1 клик. Руководитель знает, сколько и откуда было заявок, на каком этапе находится сделка, средний чек и др.
                </div>
            </div>
        </div>
    </section>
<!--
    <div to="WIDGET_CONSULTATION_FORM" class="consultation">
        <div class="container">
            <div class="consultation__wrapper">
                <div class="consultation__inner">
                    <div class="consultation__title">Настроим Битрикс24 для вашего бизнеса</div>
                    <div class="consultation__subtitle">Ответим на любые вопросы и дадим полную консультацию. Бесплатно.</div>
                    <div class="consultation__form">
                        <?
                        $APPLICATION->IncludeComponent(
                            "bezr:form.result.new.befsend",
                            "inline",
                            array(
                                "WEB_FORM_ID" => FORM_CONSULTATION,	// ID веб-формы
                                "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
                                "COMPANY_IBLOCK_ID" => IB_CONTACTS,
                                "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
                                "EMAIL_QUESTION_CODE" => "EMAIL",
                                "CITY_QUESTION_CODE" => "CITY",
                                "OFFICE_QUESTION_CODE" => "OFFICE",
                                "TO_QUESTION_RESIPIENTS" => "",
                                "EMAIL_ONLY_FROM_RESIPIENTS" => "",
                                "TO_QUESTION_CODE" => "TO",
                                "IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
                                "USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
                                "SEF_MODE" => "N",	// Включить поддержку ЧПУ
                                "CACHE_TYPE" => "A",	// Тип кеширования
                                "CACHE_TIME" => "3600",	// Время кеширования (сек.)
                                "LIST_URL" => "",	// Страница со списком результатов
                                "EDIT_URL" => "",	// Страница редактирования результата
                                "SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
                                "CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
                                "CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
                                "SHOW_TITLE" => "N",
                                "PLACEHOLDER_MODE" => 'Y',
                                "VARIABLE_ALIASES" => array(
                                    "WEB_FORM_ID" => "WEB_FORM_ID",
                                    "RESULT_ID" => "RESULT_ID",
                                ),
                                "ALERT_ADD_SHARE" => "N",
                                "HIDE_PRIVACY_POLICE" => "N",
                                "HIDE_FIELDS" => array(
                                    0 => "CITY",
                                    1 => "OFFICE",
                                ),
                                "COMPONENT_MARKER" => "subscribe-inline",
                                "SHOW_FORM_DESCRIPTION" => "N",
                                "MESS" => array(
                                    "THANK_YOU" => "Спасибо, Ваш запрос принят.",
                                    "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
                                    "TITLE_FORM" => "",
                                    "SUBMIT_BTN" => "Отправить",
                                ),
                                'KEEP_ERRORS' => 'Y',
                            ),
                            false,
                            array(
                                "HIDE_ICONS" => "Y"
                            )
                        );
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    -->
<!--
    <section class="buy-license buy-license__composition">
        <div class="container">
            <div class="buy-license__caption">Состав:</div>
            <div class="composition__items">
                <div class="composition__item">
                    <div class="composition__ico"><img class="composition__image" src="/local/templates/main/assets/img/vnedrenie/composition1.svg"></div>
                    <div class="composition__text">Облачная лицензия «Команда» - 5990 руб./мес.</div>
                </div>
                <div class="composition__item">
                    <div class="composition__ico"><img class="composition__image" src="/local/templates/main/assets/img/vnedrenie/composition2.svg"></div>
                    <div class="composition__text">Моделирование - 2 ч.</div>
                </div>
                <div class="composition__item">
                    <div class="composition__ico"><img class="composition__image" src="/local/templates/main/assets/img/vnedrenie/composition3.svg"></div>
                    <div class="composition__text">Базовая настройка - 8 ч.</div>
                </div>
                <div class="composition__item">
                    <div class="composition__ico"><img class="composition__image" src="/local/templates/main/assets/img/vnedrenie/composition4.svg"></div>
                    <div class="composition__text">Обучение в формате Zoom/Skype - 2 ч.</div>
                </div>
            </div>
            <div class="composition__price">ИТОГО: 38 990 руб.</div>
        </div>
    </section>
    <section class="buy-license buy-license__whom">
        <div class="container">
            <div class="buy-license__text">
                <div class="buy-license__whom-wrapper">
                    <div class="buy-license__whom-img">
                        <img class="composition__image" src="/local/templates/main/assets/img/vnedrenie/whom.jpg">
                    </div>
                    <div class="buy-license__whom-body">
                        <div class="buy-license__caption">Для кого:</div>
                        <div class="buy-license__whom-text">
                            <div class="buy-license__whom-item">Компании, занимающиеся розничными продажами товаров или услуг, с численностью от 2 до 20 сотрудников отдела продаж (от 6 до 25 пользователей CRM).</div>
                            <div class="buy-license__whom-item">ЛПР чаще всего выступают РОП или Ген. директор компании, иногда - маркетолог.</div>
                            <div class="buy-license__whom-item">До того, как обратились к нам они либо вели учет в excel, либо в какой-то другой CRM системе.</div>
                            <div class="buy-license__whom-item">Конкретно клиент не знает, что и как ему нужно настроить, у него есть лишь боли и бизнес-задачи.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="buy-license buy-license__advantage">
        <div class="container">
            <div class="buy-license__caption">Преимущества предложения:</div>
            <div class="advantage__items">
                <div class="advantage__item">
                    <div class="advantage__counter">1</div>
                    <div class="advantage__body">Вашим проектом в первую очередь будут заниматься не программисты, а специалисты по увеличению и автоматизации продаж.</div>
                </div>
                <div class="advantage__item">
                    <div class="advantage__counter">2</div>
                    <div class="advantage__body">Выполним только ключевые работы, которые гарантированно принесут результат. (В продаже можно сослаться на закон Парето - 20% действий несут 80% результата)</div>
                </div>
                <div class="advantage__item">
                    <div class="advantage__counter">3</div>
                    <div class="advantage__body">Комплексный подход. В нашем предложении есть все, что Вам нужно, начиная от лицензии Битрикс 24, заканчивая обучением сотрудников.
                    </div>
                </div>
            </div>
        </div>
    </section>
    -->
<div class="buy-license" style="padding-top: 50px;">
<?
$WEB_FORM_QUESTION_ID=17;
if($WEB_FORM_QUESTION_ID > 0){?>
    <? $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "question", Array(
        "WEB_FORM_ID" => $WEB_FORM_QUESTION_ID,  // ID веб-формы
        "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
        "COMPANY_IBLOCK_ID" => IB_CONTACTS,
        "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
        "EMAIL_QUESTION_CODE" => "EMAIL",
        "CITY_QUESTION_CODE" => "CITY",
        "OFFICE_QUESTION_CODE" => "OFFICE",
        "TO_QUESTION_RESIPIENTS" => "",
        "EMAIL_ONLY_FROM_RESIPIENTS" => "",
        "PRODUCT_QUESTION_CODE" => "PRODUCT",
        "TO_QUESTION_CODE" => "TO",
        "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
        "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
        "SEF_MODE" => "N",  // Включить поддержку ЧПУ
        "CACHE_TYPE" => "N",  // Тип кеширования
        "CACHE_TIME" => "3600",  // Время кеширования (сек.)
        "LIST_URL" => "",  // Страница со списком результатов
        "EDIT_URL" => "",  // Страница редактирования результата
        "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
        "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
        "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        ),
        "ALERT_ADD_SHARE" => "N",
        "HIDE_PRIVACY_POLICE" => "Y",
        "BTN_CALL_FORM" => ".btn_order",
        "HIDE_FIELDS" => array(
            0 => "CITY",
            1 => "OFFICE",
        ),
        "COMPONENT_MARKER" => "question",
        "SHOW_FORM_DESCRIPTION" => "N",
        "MESS" => array(
            "THANK_YOU" => "Спасибо, Ваша заявка принята.",
            "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
            "TITLE_FORM" => "Остались вопросы?",
        )
    ),
        false,
        array(
            "HIDE_ICONS" => "Y"
        )
    );
    ?>
<?}?>
</div>
    <style>
        .breadcrumb-block, .breadcrumb {
            background-color: #f9f8fe;
        }
    </style>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>