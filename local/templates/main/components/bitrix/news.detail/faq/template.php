<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetTitle($arResult["NAME"]);
$templateData["MY_CHIAN_LAST_ELEMENT"] = $arResult["NAME"];
?>

<?/*<h1 class="faq__page-title"><?=$arResult["NAME"];?></h1>*/?>
    <div class="faq__content">
        <div class="faq-single__info">
            <span class="faq-single__info-date"><?=$arResult["DATE_ACTIVE_FROM"]?></span><span class="faq-single__info-views"><?=$arResult["SHOW_COUNTER"]?></span>
        </div>
		<?=$arResult["DETAIL_TEXT"];?>
        <div class="faq-single__content-footer">
            <div class="faq-single__back-link">
                <a href="/faq/">Вернуться назад</a>
            </div>
            <div class="faq-single__repost-links">
                <?/*
                <a href="#" class="faq-single__repost-link faq-single__repost-link_vk">1023</a>
                <a href="#" class="faq-single__repost-link faq-single__repost-link_facebook">570</a>
                <a href="#" class="faq-single__repost-link faq-single__repost-link_ok">371</a>
                <a href="#" class="faq-single__repost-link faq-single__repost-link_hz">91</a>
                */?>
            </div>
        </div>
    </div>