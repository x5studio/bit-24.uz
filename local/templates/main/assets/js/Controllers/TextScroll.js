if (typeof define === "function" && define.amd) {
    define(['jquery', 'easing', 'scrollTo'], function ($) {
        function TextScroll(container) {
            this.control = $(container);
            this.offset=20;
            if(this.control.data('offset') != '' && $.isNumeric(this.control.data('offset')))
                this.offset = this.control.data('offset');

            this.contentBlock = $(this.control.attr('href'));
            this.init();
        }

        TextScroll.prototype.init = function () {
            var o = this;
            if (o.contentBlock.length > 0) {
                o.control.show();
                this.control.click(function (event) {
                    return o.clickTextScroll(event);
                });
            } else {
                o.control.hide();
            }
        };

        TextScroll.prototype.clickTextScroll = function (event) {
            var o = this;
            o.control.trigger("jTextScrollClick", [o.control]);
            o.scrollToBlock();
            event.preventDefault();
            return false;
        };

        TextScroll.prototype.scrollToBlock = function () {
            var o = this;
            $('html, body').animate({
                    scrollTop: o.contentBlock.offset().top-this.offset
                },
                {
                    duration: 800,
                    easing: "easeOutCubic"
                }
            );
        };

        return TextScroll;
    });
}