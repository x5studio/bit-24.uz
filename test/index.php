<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty("description", "Корпоративный портал");
$APPLICATION->SetPageProperty("keywords", "Корпоративный портал");
$APPLICATION->SetPageProperty("title", "Корпоративный портал");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "N");
$APPLICATION->SetTitle("Корпоративный портал");
$APPLICATION->SetAdditionalCSS("/test/css/style.css");
?>


<? $APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"main",
	array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0",
		"COMPONENT_TEMPLATE" => "main"
	),
	false
); ?>

<section class="portal-banner">
	<div class="container clearfix">
		<div class="intro_descr">
			<h1 class="intro_title">
				Разработка и внедрение<br> корпоративного портала (Intranet)<br>
				<span class="medium_font">на базе Битрикс24</span>
			</h1>
			<p>Единое решение для совместной работы сотрудников и оптимизации бизнес-процессов</p>
			<a href="#" class="btn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a>
		</div>
		<div class="banner-right-img hidden-xs hidden-sm hidden-md">
			<img src="img/banner-pc.png" alt="">
		</div>
	</div>
</section>

<section class="opportunities">
	<div class="container">
		<div class="row tabs-wrapper">
			<div class="col-md-5">
				<h2 class="why-heading">Возможности<br> порталов</h2>
				<ul class="tabs">
					<li class="tab active"><span>01.</span>CRM для продаж и работы с клиентами</li>
					<li class="tab"><span>02.</span>HR – управление персоналом</li>
					<li class="tab"><span>03.</span>Работа над проектами и задачам</li>
					<li class="tab"><span>04.</span>Коммуникация внутри компани</li>
					<li class="tab"><span>05.</span>Сервисы для совместной работы</li>
					<li class="tab"><span>06.</span>Автоматизация рутинных действий</li>
					<li class="tab"><span>07.</span>Интеграция с учетными системами</li>
				</ul>
			</div>
			<div class="col-md-7">
				<div class="tab-outer">
					<div class="tab-content">
						<div class="tab-item">
							<div class="card">
								<div class="card-num">01.</div>
								<div class="card-heading">CRM для продаж и работы с клиентами</div>
								<div class="desc">
									<ul>
										<li>Лиды и продажи</li>
										<li>Коммуникация с клиентами</li>
										<li>CRM-маркетинг</li>
										<li>AI скоринг сделок</li>
										<li>Карточка клиента с историей</li>
										<li>Сквозная аналитика</li>
										<li>Автоматизация продаж</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-item" style="display:none;">
							<div class="card">
								<div class="card-num">02.</div>
								<div class="card-heading">HR – управление персоналом</div>
								<div class="desc">
									<ul>
										<li>Подбор персонала</li>
										<li>Приглашение и увольнение сотрудников</li>
										<li>Обучение и адаптация</li>
										<li>Данные о сотрудниках</li>
										<li>Структура компании</li>
										<li>Мероприятия</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-item" style="display:none;">
							<div class="card">
								<div class="card-num">03.</div>
								<div class="card-heading">Работа над проектами и задачами</div>
								<div class="desc">
									<ul>
										<li>Проекты</li>
										<li>Задачи</li>
										<li>Рабочие группы</li>
										<li>Учет рабочего времени</li>
										<li>Диаграмма Ганта</li>
										<li>Канбан</li>
										<li>Отчеты руководителю</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-item" style="display:none;">
							<div class="card">
								<div class="card-num">04.</div>
								<div class="card-heading">Коммуникация внутри компании</div>
								<div class="desc">
									<ul>
										<li>Живая лента</li>
										<li>Чат</li>
										<li>Видеосообщения</li>
										<li>Опросы</li>
										<li>Почта</li>
										<li>Новости</li>
										<li>Календарь</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="tab-item" style="display:none;">
							<div class="card">
								<div class="card-num">05.</div>
								<div class="card-heading">Сервисы для совместной работы</div>
								<div class="desc">
									<ul>
										<li>Бронирование переговорных</li>
										<li>Планировщик встреч</li>
										<li>Совместная работа над документами</li>
										<li>Облачный Диск</li>
										</ul>
								</div>
							</div>
						</div>
						<div class="tab-item" style="display:none;">
							<div class="card">
								<div class="card-num">06.</div>
								<div class="card-heading">Автоматизация рутинных действий</div>
								<div class="desc">
									<ul>
										<li>Заказ справок и канцелярии</li>
										<li>Оформление больничных и отпусков</li>
										<li>График присутствия</li>
										<li>Согласование и выставление счетов</li>
										<li>Чат-боты</li>
										</ul>

								</div>
							</div>
						</div>
						<div class="tab-item" style="display:none;">
							<div class="card">
								<div class="card-num">07.</div>
								<div class="card-heading">Интеграция с учетными системами</div>
								<div class="desc">
									<ul>
										<li>Миграция из других CRM</li>
										<li>Импорт контактов</li>
										<li>Интеграция с 1С, Microsoft, SAP, BI</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="usefulness">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="why-heading blue-heading">Кому полезно</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10">
				<h4 class="white-heading">«1С-Битрикс24» — один из самых популярных корпоративных порталов в мире. <br>Им пользуются более 3 миллионов компаний.</h4>
			</div>
			<div class="col-md-2 btn-sect footbtn">
				<a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a>
			</div>
		</div>
	</div>
</section>

<div class="useful-cards">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card wide-card top-card">
					<div class="row">
						<div class="col-md-8">
							<div class="card-content">
								<div class="card-heading">Руководителям</div>
								<p>
									Контролировать сотрудников и получать от них обратную связь, собирать
									бизнес-аналитику (продажи, выполнение планов, текучка кадров, финансовые
									показатели и т.д.)
								</p>
								<img class="card-icon" src="img/ic1.png" alt="">
							</div>
						</div>
						<div class="col-md-4">
							<div class="perso-img"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row d-flex small-cards align-items-stretch">
			<div class="col-md-4">
				<div class="card">
					<div class="card-content">
						<div class="card-heading">IT-специалистам</div>
						<p>
							Предоставлять единый доступ к IT-
							ресурсам компании, оказывать тех
							поддержку через helpdesk- системы,
							использовать адресную книгу с поиском
							по ФИО, номеру, должности 
						</p>
						<img class="card-icon" src="img/ic2.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-content">
						<div class="card-heading">HR-специалистам</div>
						<p>
							Проводить адаптацию, обучение
							и аттестацию персонала, делать рассылки
							и опросы, размещать должностные
							инструкции, регламенты.
						</p>
						<img class="card-icon" src="img/ic3.png" alt="">
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-content">
						<div class="card-heading">Менеджерам по продажам</div>
						<p>
							Вести клиентов в одной системе, хранить
							все звонки, письма, чаты с клиентами
							на сайте и в соцсетях внутри карточки
							и т.д. 
						</p>
						<img class="card-icon" src="img/ic4.png" alt="">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="card wide-card bottom-card">
					<div class="row">
						<div class="col-md-8">
							<div class="card-content">
								<div class="card-heading">Для всех</div>
								<p>
									Получать новости компании, общаться, контролировать задачи, использовать офисные сервисы:
									заказ справок, оформление отпусков, командировок, больничных, заказ канцелярии, бронирование
									переговорных и т.д.
								</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="icon-img btn-sect footbtn">
								<a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="portal-price">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="why-heading white-heading">Сколько стоит</h2>
				<div class="card">
					<div class="row">
						<div class="col-lg-7 col-md-8">
							<div class="card-content">
								<img class="portal-box-top" src="img/portal-box.png" alt="" style="display: none">
								<div class="card-heading">Битрикс24 Корпоративный портал</div>
								<p>Выберите количество пользователей</p>

								<div class="tabs-wrapper">
									<div class="tabs price-tabs clearfix">
										<div class="tab"><span>50</span></div>
										<div class="tab"><span>100</span></div>
										<div class="tab"><span>250</span></div>
										<div class="tab"><span>500</span></div>
										<div class="tab"><span>1000+</span></div>
									</div>
									<div class="tab-content">
										<div class="tab-item">
											<div class="card-portal-price">139 000 ₽</div>
											<div class="btn-sect footbtn"><a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a></div>
										</div>
										<div class="tab-item" style="display:none;">
											<div class="card-portal-price">199 000 ₽</div>
											<div class="btn-sect footbtn"><a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a></div>
										</div>
										<div class="tab-item" style="display:none;">
											<div class="card-portal-price">299 000 ₽</div>
											<div class="btn-sect footbtn"><a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a></div>
										</div>
										<div class="tab-item" style="display:none;">
											<div class="card-portal-price">499 000 ₽</div>
											<div class="btn-sect footbtn"><a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a></div>
										</div>
										<div class="tab-item" style="display:none;">
											<div class="card-portal-price">999 000 ₽</div>
											<div class="btn-sect footbtn"><a href="#" class="btn red-buttn" data-toggle="modal" data-target="#advanced" data-form-field-type="Оставить заявку">Заказать</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-5 col-md-4">
							<img class="portal-box" src="img/portal-box.png" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="heading-wrapper">
					<h2 class="why-heading white-heading">
						НЕ ЗНАЕТЕ КАКАЯ <br>ЛИЦЕНЗИЯ БИТРИКС 24 <br>
						<span class="blue-heading">НУЖНА ВАМ?</span>
					</h2>
					<p class="white-heading">
						Заполните форму, мы изучим потребности вашего <br>
						бизнеса и порекомендуем наиболее оптимальную <br>
						и выгодную для вас лицензию
					</p>
				</div>
			</div>
			<div class="col-md-6">

 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new", 
	"license_choosing", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => "15",
		"COMPONENT_TEMPLATE" => "license_choosing",
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
				<?/*<form action="#" method="post">
					<input type="text" placeholder="Имя">
					<input type="text" placeholder="Телефон">
					<input type="text" placeholder="E-mail">
					<div class="sbmt-wrapper">
						<input class="btn red-buttn" type="submit" value="Отправить">
						<p>
							Нажимая кнопку «Отправить»,
							вы даете свое согласие на обработку 
							<a href="/politika-konfidencialnosti/" target="_blank">персональных данных</a>
						</p>
					</div>
				</form>*/?>

			</div>
		</div>
	</div>
</section>


<section class="portal-help">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="why-heading">Как мы поможем?</h2>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-num">01.</div>
					<div class="card-desc">
						<img class="card-icon" src="img/ic6.png" alt="">
						<img class="card-arrow" src="img/ic10.png" alt="">
						<div class="card-heading">Моделирование</div>
						<div class="card-text">
							Проанализируем ваш бизнес <br>
							и составим план внедрения <br>
							Битрикс24.
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-num">02.</div>
					<div class="card-desc">
						<img class="card-icon" src="img/ic7.png" alt="">
						<div class="card-heading">Разработка</div>
						<div class="card-text">
							Установка и программная разработка <br>
							портала под ваши бизнес-процессы, <br>
							интеграция с другими системами
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-num">03.</div>
					<div class="card-desc">
						<img class="card-icon" src="img/ic8.png" alt="">
						<img class="card-arrow" src="img/ic10.png" alt="">
						<div class="card-heading">Обучение</div>
						<div class="card-text">
							Очно или дистанционно обучим работе <br>
							в системе по ролям и функциям, которые <br>
							будут использоваться в работе.
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-num">04.</div>
					<div class="card-desc">
						<img class="card-icon" src="img/ic9.png" alt="">
						<div class="card-heading">Сопровождение</div>
						<div class="card-text">
							Проконтролируем корректную работу <br>
							всех бизнес-процессов, проконсультируем <br>
							пользователей по функционалу.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="portal-results">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="why-heading blue-heading">Каких ждать результатов?</h2>
			</div>
		</div>

		<div class="row results-list">
			<div class="col-md-6">
				<p>Сократиться время на поиск документов, коллег, так как все будет собрано в одном месте</p>
			</div>
			<div class="col-md-6">
				<p>Сотрудники смогут совместно работать над проектами</p>
			</div>
			<div class="col-md-6">
				<p>Новым сотрудникам будет проще адаптироваться и давать первые результаты</p>
			</div>
			<div class="col-md-6">
				<p>Ускорятся типовые процессы как согласование, заказ справок, бронирование и др.</p>
			</div>
			<div class="col-md-6">
				<p>Затраченное время на задачи, загруженность и эффективность сотрудников будут перед глазами</p>
			</div>
			<div class="col-md-6">
				<p>Вырастет вовлеченность персонала в корпоративную жизнь</p>
			</div>
			<div class="col-md-6">
				<p>Качество предоставляемых услуг и производства вырастут</p>
			</div>
			<div class="col-md-6">
				<p>Руководителям будет проще контролировать поручения и бизнес-показатели</p>
			</div>
		</div>

	</div>
</section>


<section class="main-clients portal-clients">
    <div class="container">
        <div class="row why-row">
            <div class="col-md-6 col-sm-10">
                <h2 class="why-heading pull-left">Убедитесь сами</h2>
            </div>
            <div class="col-md-6 col-sm-2 hidden-xs">
                <a href="/nashi-klienty/" class="btn btn-why pull-right">Подробнее</a>
            </div>
        </div>
    </div>
    <?php
    $APPLICATION->IncludeComponent(
        "client.list",
        "",
        Array(
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "COUNT" => 6,
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "IBLOCK_ID" => "8",
            'FRONTPAGE_STYLE' => 'Y',
        )
    );
    ?>
    <div class="hidden-lg hidden-md hidden-sm col-xs-12 detline">
        <a class="btn btn-why" href="/nashi-klienty/">Подробнее</a>
    </div>
</section>


<section class="portal-partner">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="why-heading white-heading">Крупнейший федеральный <br>партнер «1С-Битрикс»</h2>
			</div>
		</div>
		<div class="row d-flex portal-partner-numbers">
			<div class="col-lg-4">
				<div class="team-card">
					<div class="team-num">200</div>
					<div class="team-title">Человек в команде</div>
					<div class="team-desc">
						Среди которых: штатные <br>
						и сертифицированны <br>
						специалисты
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="team-card">
					<div class="team-num">22</div>
					<div class="team-title">Года на рынке</div>
					<div class="team-desc">
						Успешно занимаемся <br>
						автоматизацией в России <br>
						и странах СНГ
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="team-card">
					<div class="team-num">250 000+</div>
					<div class="team-title">Успешных внедрений</div>
					<div class="team-desc">
						Автоматизированных систем
					</div>
				</div>
			</div>
		</div>
	</div>
</section>




<script>
	$(function() {
		//tabs
		$(".tab-item").not(":first-child").hide();
		$(".tab:first-child").addClass("active");
	
		$(".tabs-wrapper .tab").click(function() {
			$(this).closest(".tabs-wrapper").find(".tab").removeClass("active").eq($(this).index()).addClass("active");
			$(this).closest(".tabs-wrapper").find(".tab-item").hide().eq($(this).index()).fadeIn();
		}).eq(0).addClass("active");
	});
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
