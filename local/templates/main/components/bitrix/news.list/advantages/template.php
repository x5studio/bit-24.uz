<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if (count($arResult['SECTION']['PATH']) && count($arResult["ITEMS"])):?>
    <section class="advantages">
        <div class="container">
            <h2 class="pfd-h2"><?=reset($arResult['SECTION']['PATH'])['NAME']?></h2>
            <ul class="advantages-cards-list">
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="pfd-h3-card"><?=$arItem["NAME"];?></div>
                        <p><?=$arItem["PREVIEW_TEXT"];?></p>
                    </li>
                <?endforeach;?>
            </ul>
        </div>
    </section>
<?endif;?>

