if (typeof define === "function" && define.amd) {
    define(['router', 'easing', 'scrollTo'], function () {
        function SwitchText(container) {
            var _this = this,
                contentItems = '';

            this.container = $(container);
            this.index = this.container.index();
            this.control = this.container.find('a');
            this.control.bind('active', function (e, param) {
                if ($(this).data('active')) {
                    // кликнули по активному элементу, его и скрываем
                    $(this).removeClass('dotted-active').data('active', !$(this).data('active'));
                    $($(this).attr('href')).hide();
                    _this.active = jQuery();
                } else {
                    // кликнули по неактивному, показываем его, а предыдущий активный скрываем
                    if (typeof _this.active != 'undefined' && _this.active.length) {
                        _this.active.removeClass('dotted-active').data('active', !_this.active.data('active'));
                        $(_this.active.attr('href')).hide();

                        rHelperData(_this.active);
                    }

                    _this.active = $(this);
                    $(this).addClass('dotted-active').data('active', !$(this).data('active'));
                    $($(this).attr('href')).show();

                    if (!(!_this.container.hasClass("j-switch-text-scroll") || ($(this).hasClass('no-initialize-scroll') && typeof param == 'object' && param.initialize == true))) {
                        $(window).scrollTo(_this.control, {
                            duration: 200,
                            easing: 'easeOutCubic',
                            offset: -20
                        });

                    }
                }
            });
            this.control.click(function () {
                $(this).trigger('active');

                // emulate page for analytics
                var id = $(this).attr("href").substr(1);
                var page = "/virtual/ga-ajax" + location.pathname.replace("index.php", "") + id + "/";
                window.trackStep(page);

                // hash
                rHelperClick($(this));
                return false;
            });

            this.active = jQuery();
            var active = _this.control.filter('.dotted-active').eq(0);
            if (active.length > 0) {
                var activeInitialize = true;
            }

            this.control.each(function (i) {
                var control = $(this),
                    id = control.attr('href'),
                    close = $(id).find('.j-switch-text-close');

                close.click(function () {
                    control.click();
                    var controlScrollTop = control.offset().top,
                        pageScrollTop = $('html').scrollTop() + $('body').scrollTop();
                    if (pageScrollTop > controlScrollTop) {
                        $('html,body').scrollTop(controlScrollTop);
                    }
                });

                if (i != 0) contentItems += ',';
                contentItems += id;

                if (control.data('active')) {
                    active = control;
                    activeInitialize = false;
                }
                control.data({'active': false}).removeClass('dotted-active');
            });
            $(contentItems).hide();
            _this.contentItems = $(contentItems);

            if (active.length) {
                active.trigger('active', {initialize: activeInitialize});
            }
            container.data('init', true);
        }

        return SwitchText;
    });
}