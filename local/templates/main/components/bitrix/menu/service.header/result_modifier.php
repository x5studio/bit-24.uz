<?php

foreach ($arResult as $key => $arItem) {
    if ($arItem['PARAMS']['ELEMENT']['PROPERTY_MENU_ICON_VALUE']) {
        $arResult[$key]['ICON'] = CFile::ResizeImageGet($arItem['PARAMS']['ELEMENT']['PROPERTY_MENU_ICON_VALUE'],
            ['width' => 23, 'height' => 26], BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
    }
}
