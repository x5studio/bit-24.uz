if (typeof define === "function" && define.amd) {
    define([], function () {
        function UtmLog()
        {
            this.ajaxPath = '/';
        }

        UtmLog.prototype.execute = function () {
            if(location.search.length>1) {
                var
                    allParams = location.search.substr(1).split('&');
                for (var i = 0; i < allParams.length; i++) {
                    if (allParams[i].split('=')[0] === 'utm_source') {
                        $.post(
                            this.ajaxPath,
                            {
                                'utm_log_write':1
                            }
                        );
                    }
                }
            }
        };

        return UtmLog;
    })
}
