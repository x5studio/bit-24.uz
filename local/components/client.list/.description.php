<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Клиенты. Список",
	"DESCRIPTION" => "Блог. Список",
	"ICON" => "/images/news_line.gif",
	"SORT" => 100,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "client",
			"NAME" => "Клиенты"
		)
	),
);

?>