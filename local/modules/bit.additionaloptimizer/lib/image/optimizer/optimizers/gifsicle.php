<?php

namespace Bit\AdditionalOptimizer\Image\Optimizer\Optimizers;

use Bit\AdditionalOptimizer\Image\Optimizer\Image;

class Gifsicle extends BaseOptimizer
{
    public $binaryName = 'gifsicle';

    public function canHandle(Image $image): bool
    {
        return $image->mime() === 'image/gif';
    }
}
