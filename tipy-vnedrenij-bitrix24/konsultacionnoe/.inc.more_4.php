<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>



<div class="modal-header"><i class="fa fa-times" style="float: right;" data-dismiss="modal" aria-hidden="true"></i>
	<h4>Базовый</h4>
</div>
<div class="modal-body">
	<div class="material">
		<div class="questions">
			<h3>Для кого?</h3>
			<p>Для тех, кто хочет настроить рабочий стол Битрикс24 для своих сотрудников и провести базовое обучение работе с основными инструментами портала.</p>
		</div>
		<div class="questions">
			<h3>Что делаем?</h3>
			<p>На Данный формат обучения подойдёт тем, кто хочет получить концентрированную выжимку из типового функционала продукта. Мы расскажем Вашим сотрудникам что такое корпоративный портал, для чего он нужен, научим выполнять базовые действия в системе, ответим на возникающие вопросы.<br><br>

По итогам обучения для сотрудников работа с системой становится привычной, понятной и прозрачной.
	</p></div>
		<div class="questions">
			<h3>Результаты</h3>
			<p>Приобретаемые навыки и инструменты, которыми мы научим Вас пользоваться: Моя страница; Избранное; Живая лента; Бизнес-чат; Заявление на отпуск, больничный, отгул; Календарь; Задачи; Группы; Диск; Почта; Десктопное и мобильное приложения.</p>
		</div>
		<div class="service-bottom">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Формат:<img src="/local/templates/main/assets/img/service/com.png"
													class="img-responsive"></h3>
					<p>ВСТРЕЧА/SKYPE</p>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12 block">
					<h3>Стоимость<img src="/local/templates/main/assets/img/service/wallet.png"
														class="img-responsive"></h3>
					<h4>от 9 960 р.</h4>
				</div>
			</div>
			<div class="buy">
				<button type="button" class="btn btn-buy" data-target="#advanced"
								data-form-field-type="Консультационное внедрение: Базовый"
								data-toggle="modal" data-dismiss="modal"><a href="#" title="link">Купить</a></button>
			</div>
		</div>
	</div>
</div>