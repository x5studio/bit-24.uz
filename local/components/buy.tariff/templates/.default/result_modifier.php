<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();


if ($arParams['IBLOCK_ID'] == 10) {
    $codes = [
        'USERS',
        'CLOUD_SIZE',
        'OFIS',
        'TASKS_PROJECTS',
        'CRM',
        'SITES',
        'SKLAD',
        'SHOP',
        'MARKETING',
        'DOCS',
        'ANAITICS',
        'AUTO',
        'HR',
        'ADMINKA',
    ];

    $prepend = [];
    $propertyByCode = [];
    foreach ($arResult["PROPERTY"] as $arProperty) {
        $code = $arProperty['CODE'];

        if (in_array($code, $codes)) {
            $propertyByCode[$code] = $arProperty;
        } elseif (strpos($code, 'PRICE') !== false) {
            $prepend[] = $arProperty;
        }
    }
    $newProperties = $prepend;
    foreach ($codes as $code) {
        $newProperties[] = $propertyByCode[$code];
    }
    $arResult["PROPERTY"] = $newProperties;
}

