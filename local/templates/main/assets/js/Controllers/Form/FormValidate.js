if (typeof define === "function" && define.amd) {
    define(["Controls/Form/FormRuleMng", 'Controls/Url', 'validate', 'tools/tools'], function (obFormRuleMng, Url) {

        function FormValidate(p) {
            this.p = p;
            this.root = $(p.root);
            this.obForm = p.obForm;
            if (typeof p.obRules == 'object') {
                this.obRules = p.obRules;
            } else {
                throw  new TypeError('obRules is undefined');
            }
            this.rules = {};
            this.messages = {};
        }

        FormValidate.prototype.init = function () {
            var o = this;
            if (o.obRules.rules.length == 0) {
                return;
            }

            getInitial($(document), 'jSelectDeffered', 'jSelect.init', 'jSelectInit').done(function(){
                o.findItems();
                o.initValidate();
            });
        };

        FormValidate.prototype.findItems = function () {
            var o = this;
            o.root.find("[data-validate],[data-type],[class*=validate-]").each(function () {
                var item = $(this);
                var validateType = item.data('validate');
                if (typeof validateType == 'undefined') {
                    validateType = item.data('type');
                }
                if (typeof validateType == 'undefined') {
                    var itemClassString = item.attr('class');
                    if (typeof itemClassString != 'undefined' && itemClassString.length > 0) {
                        var validateMatches = itemClassString.match(/\b(validate-\w+)\b/gi);
                        if (validateMatches) {
                            validateType = [];
                            for (var indexMatch in validateMatches) {
                                validateType.push(validateMatches[indexMatch].substr(9));
                            }
                        }
                    }
                } else {
                    var validateTypeTmp = $.trim(validateType).split(/\s+|\s*,\s*/);
                    validateType = [];
                    for (var validIndex in validateTypeTmp) {
                        var validateTypeItem = validateTypeTmp[validIndex];
                        if (validateTypeItem.length > 0) {
                            validateType.push(validateTypeItem);
                        }
                    }
                }
                if (!(typeof validateType == 'object' && validateType.length > 0)) {
                    return;
                }
                if (this.hasAttribute('value')) {
                    var field = item;
                } else {
                    var field = item.find("input[type=hidden]");
                }

                o.rules[field.attr('name')] = {};
                o.messages[field.attr('name')] = {};
                var bNoPlaceholderRule = false;
                for (var validIndex in validateType) {
                    var validateTypeItem = validateType[validIndex];
                    $.extend(o.rules[field.attr('name')], o.obRules.rules[validateTypeItem]);
                    $.extend(o.messages[field.attr('name')], o.obRules.messages[validateTypeItem]);

                    if (obFormRuleMng.noPlaceholderRuleTypes.indexOf(validateTypeItem) !== -1)
                    {
                        bNoPlaceholderRule = true;
                    }
                }
                if (!$.isEmptyObject(o.rules[field.attr('name')]) && !bNoPlaceholderRule && (field.data('placeholder') || field.attr('placeholder'))) {
                    $.extend(o.rules[field.attr('name')], o.obRules.rules['placeholder']);
                    $.extend(o.messages[field.attr('name')], o.obRules.messages['placeholder'](validateType[0]));
                }
            });
        };

        FormValidate.prototype.initValidate = function () {
            var o = this,
                keepErrors = this.root.data('keep-errors');

            if (!$.isEmptyObject(o.rules)) {
                o.root.validate({
                    ignore: [],
                    focusInvalid: false, //ставит focus на элемент с ошибкой на submit
                    focusCleanup: true,
                    rules: o.rules,
                    messages: o.messages,
                    submitHandler: function (form) {
                        o.root.find('input[type^="submit"], .j-form-submit').attr("disabled","disabled");
                        o.root.find('.j-form-submit').css('pointer-events', 'none');
                        o.obForm.onSubmit();
                        if(o.root.find(".j-recaptcha").length > 0) {
                            event.preventDefault();
                            o.root.find(".j-recaptcha").each(function(){
                                var oo = this;
                                require(['Controls/ReCaptcha'], function (ReCaptcha) {
                                    new ReCaptcha(oo);
                                });
                            });

                        } else {
                            o.root.trigger('afterValidateSuccess');
                            form.submit();
                        }
                    },
                    onkeyup: function (element, event) {
                        var valid = $(element).valid();
                        if (valid && this.elementValue(element) != "") {
                            $(element).closest('.text').addClass("success");
                        }
                        else if (!valid || this.lastElement === "") {
                            $(element).closest('.text').removeClass("success");
                        }
                    },
                    highlight: function (element, errorClass, validClass) {
                        if (element.type === "radio") {
                            this.findByName(element.name).closest('.text').addClass(errorClass).removeClass(validClass);
                        } else {
                            $(element).closest('.text').addClass(errorClass).removeClass(validClass).removeClass('success');
                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        if (element.type === "radio") {
                            $(element).closest('.block-radiobox').removeClass('error');
                            this.findByName(element.name).closest('.text').removeClass(errorClass).addClass(validClass);
                        } else {
                            $(element).closest('.text').removeClass(errorClass).addClass(validClass);
                        }
                    },
                    errorPlacement: function (error, element) {
                        if (element.parent().hasClass('cusel')) {
                            element.parent().parent().attr('data-content', error.text());
                        }else if(element.closest('.block-radiobox').hasClass('block-radiobox')){
                            element.closest('.block-radiobox').addClass('error');
                            element.closest('.block-radiobox').attr('data-content', error.text());
                        } else {
                            element.parent().attr('data-content', error.text());
                        }

                        if (typeof keepErrors === 'undefined' || keepErrors != 1) {
                            if (o.errorTimer) {
                                clearTimeout(o.errorTimer);
                            }

                            o.errorTimer = setTimeout(
                                function () {
                                    o.root.find('.error').removeClass('error')
                                },
                                3000
                            );
                        }
                    }
                });

                // Cusel fix
                o.root.on('change', '.cusel input[type="hidden"]', function () {
                    var self = $(this);
                    if (self.val()) {
                        self.closest('.text').removeClass('error').addClass('success');
                    }
                });
            }
        };


        $.validator.addMethod("placeholder", function (value, element, params) {
            var el = $(element);
            var paramsTrim = (typeof params == "string") ? params.replace("(___)", "___") : params;

            if (!this.depend(paramsTrim, element)) {
                return "dependency-mismatch";
            }
            if (el.val() == el.data('placeholder')) {
                return this.optional(element) || false;
            } else return true;

        }, obFormRuleMng.getMessage('REQUIRED').required);
        $.validator.addClassRules('placeholder', {placeholder: false});
        //jQuery.validator.classRuleSettings.placeholder = { placeholder: false };

        $.validator.addMethod("requiredPhone", function (value, element) {
			if(!window.disableMask){
				//this way if we automatic detect user country by IP from pb.geo module
				if($('body').data('phone-mask'))
					endMask = $('body').data('phone-mask');
				else{
					// if we not use autodetect phone code
					var arHost = location.host.split('.');
					var subDomain = arHost[0];
					var baseDomain = arHost[arHost.length - 1];

					if (subDomain == 'dubai') {
						endMask = "+971 ddddddddd";
					} else  if (subDomain == 'montreal' || subDomain == 'toronto' ||  $('body').data('countryCode') == 'canada') {
						endMask = "+1 ddd ddd-dddd";
					} else if (baseDomain == 'ua' || $('body').data('countryCode') == 'ua') {
						endMask = "+38 (ddd) ddd-dddd";
					} else {
						endMask = '+7 (ddd) ddd-dddd';
					}
				}

				//RegExp.escape -custom fn in tools.js
				var mask = new RegExp(RegExp.escape(endMask));
				var maskResult = mask.test(value);

                let onlyDigits = value.replace(/\D/g,'');
                let countOk = true;
                let digitCount = [0,0,0,0,0,0,0,0,0,0];
                for (let i = 0; i < onlyDigits.length; i++) {
                    if (++digitCount[onlyDigits[i]] >= 6) {
                        countOk = false;
                        break;
                    }
                }

                return maskResult && countOk;
			}else{
				if(value !== ''){
				  return true;
				}else{
				  return false;
				}
			}

        }, obFormRuleMng.getMessage('PHONE').required);

        obFormRuleMng.addRule('PHONE', {
            requiredPhone: true
        });

        $.validator.addMethod("page", function (value, element, xz) {

            var
                inputUrl = new Url(value);

            if( inputUrl.error || (inputUrl.path.substr(-1) !== '/') ){
                return false;
            }

            if(window.location.hostname == inputUrl.host) {
                inputUrl.hash = '';
                element.value = inputUrl.getUrl();
                return true;
            }else{
                return false;
            }
        });

        $.validator.addMethod("page-exist", function (value, element) {
            if(window.event.type === 'keyup') // не проверяем на вводе - иначе тормоза.
            {
                return true;
            }

            var
                result = false;
            $.ajax(
                value,
                {
                    'async':false,
                    'method':'HEAD',
                    'complete':function (jqXHR, status) {
                        result = (status === 'success') || (status === 'notmodified');
                    }
                }
            );
            return result;
        });

        obFormRuleMng.addRule('PAGE_VALIDATE', {
            'page': true
        }, {
            'page': 'Ошибочный URL'
        });

        obFormRuleMng.addRule('PAGE_EXIST', {
            'page-exist': true
        },{
            'page-exist': 'Несушествуюшая страница'
        });

        $.validator.addMethod("emailNoRequired", function (value, element) {
            var $el = $(element);

            if ($el.val() && ($el.val() != $el.data('placeholder')))
            {
                return $.validator.methods.email.call(this, value, element);
            }
            else
            {
                return true;
            }
        }, obFormRuleMng.getMessage('EMAIL_NO_REQUIRED').format);

        $.validator.methods.email = function( value, element ) {
            let re = /^[0-9a-zA-Z_.\-]+@[0-9a-zA-Z_.\-]+\.[0-9a-zA-Z_.\-]+$/;

            return this.optional( element ) || re.test( value );
        };

        $.validator.addMethod("phoneNoRequired", function (value, element) {
            var $el = $(element);

            if ($el.val() && ($el.val() != $el.data('placeholder')))
            {
                return $.validator.methods.requiredPhone.call(this, value, element);
            }
            else
            {
                return true;
            }
        }, obFormRuleMng.getMessage('PHONE_NO_REQUIRED').format);

        $.validator.addMethod("requiredFromGroup", function (value, element) {
            var bRequired = false,
                $groupElements = $(element).closest('form').find('[data-type~="REQUIRED_FROM_GROUP"]');

            for (var i = 0; i < $groupElements.length; i++)
            {
                var $groupElement = $groupElements.eq(i);
                if ($groupElement.val() && $groupElement.val() != $groupElement.data('placeholder'))
                {
                    bRequired = true;

                    break;
                }
            }

            return bRequired;
        }, obFormRuleMng.getMessage('REQUIRED_FROM_GROUP').required);

        return FormValidate;
    });
}
