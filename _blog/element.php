<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="container">
	<div class="row row-offcanvas row-offcanvas-left">
		<? $APPLICATION->IncludeFile($arParams["SEF_FOLDER"] . "sidebar.php", $arParams, array("MODE" => "php")); ?>
		<div class="col-md-9 col-xs-12 col-sm-12">
			<div class="main-blog">
				<?$APPLICATION->IncludeComponent(
					"blog.detail",
					"",
					Array(
						"CACHE_TIME" => "3600",
						"CACHE_TYPE" => "A",
						"ELEMENT_CODE" => $arParams["ELEMENT_CODE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"SECTION_CODE" => $arParams["SECTION_CODE"]
					)
				);?>
			</div>
		</div><!--/.col-xs-12.col-sm-9-->
	</div>
</div>
