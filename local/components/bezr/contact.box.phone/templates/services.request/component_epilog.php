<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<!-- Форма: Оставить заявку -->
<?

$gWFServices = 'general_form'.$arParams["WEB_FORM_ID"];
global ${$gWFServices};
if(${$gWFServices} != "Y"):?>
	<?$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "general.onecol", array(
			"WEB_FORM_ID" => $arParams["WEB_FORM_ID"],
			"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
			"COMPANY_IBLOCK_ID" => IB_CONTACTS,
			"PRODUCT_IBLOCK_TYPE" => IBT_CATALOG,
			"PRODUCT_IBLOCK_ID" => IB_CATALOG,
			"SERVICE_SECT_ID" => $arParams['SERVICE_ID'],
			"CITY_QUESTION_CODE" => "CITY",
			"OFFICE_QUESTION_CODE" => "OFFICE",
			"PRODUCT_QUESTION_CODE" => "PRODUCT",
			"TO_QUESTION_CODE" => "TO",
			"TO_QUESTION_RESIPIENTS" => "",
			"EMAIL_ONLY_FROM_RESIPIENTS" => $arParams["EMAIL_ONLY_FROM_RESIPIENTS"] =="N" ? $arParams["EMAIL_ONLY_FROM_RESIPIENTS"] : "Y",
			"RESIPIENTS_SMART_SELECT" => "Y",
			"IGNORE_CUSTOM_TEMPLATE" => "N",
			"USE_EXTENDED_ERRORS" => "Y",
			"SEF_MODE" => "N",
			"CACHE_TYPE" =>  "A",
			"CACHE_TIME" =>  "360000",
			"LIST_URL" => "",
			"EDIT_URL" => "",
			"SUCCESS_URL" => "",
			"CHAIN_ITEM_TEXT" => "",
			"CHAIN_ITEM_LINK" => "",
			"VARIABLE_ALIASES" => array(
				"WEB_FORM_ID" => "WEB_FORM_ID",
				"RESULT_ID" => "RESULT_ID",
			),
			"BTN_CALL_FORM" => "#general-form-{$arParams["WEB_FORM_ID"]}-btn, .general-form-{$arParams["WEB_FORM_ID"]}-btn",
		),
		false
	);?>
	<?if(!empty($arParams['ANSWER_VALUES'])):?>
		<script type="text/javascript">
			<?foreach($arParams['ANSWER_VALUES'] AS $ANSWER_SID => $msg):?>
				<?
				$arTextarea = CFormField::GetList($arParams["WEB_FORM_ID"], 'N', $by = 's_sort', $order = 'asc', Array('SID' => $ANSWER_SID))->Fetch();
				$arAnswerTextarea = CFormAnswer::GetList($arTextarea['ID'], $by = 's_sort', $order = 'asc', Array())->Fetch();
				$fieldName = 'form_'.$arAnswerTextarea['FIELD_TYPE'].'_'.$arAnswerTextarea['ID'];
				?>
				$('[name=<?=$fieldName?>]').val('<?=$msg?>');
			<?endforeach?>
		</script>
	<?endif?>
	<?${$gWFServices} = "Y";
endif?>