<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if (!empty($arResult["IMAGES"][$arResult["DETAIL_PICTURE"]])):?>
<div class="material-img">
	<img src="<?= $arResult["IMAGES"][$arResult["DETAIL_PICTURE"]] ?>" class="img-responsive big">
</div>
<?else:?>
    <div class="material-img material-img_xs visible-lg-block visible-md-block">

        </br></br></br></br>
    </div>
<?endif;?>
<div class="material">
    <div class="title-page">
        <h1 class="title-page__h1"><?$APPLICATION->ShowTitle(true);?></h1>
    </div>
	<?= $arResult["DETAIL_TEXT"] ?>
	<div class="service-bottom">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12 block">
				<h3>Формат:<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/service/com.png" class="img-responsive"></h3>
				<p><?= $arResult["PROPERTY_FORMAT_VALUE"] ?></p>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 block">
				<h3>Количество<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/service/layer.png" class="img-responsive"></h3>
				<h4><?= $arResult["PROPERTY_COUNT_VALUE"] ?></h4>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 block">
				<h3>Стоимость<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/service/wallet.png" class="img-responsive"></h3>
				<h4><?= $arResult["PROPERTY_PRICE_VALUE"] ?></h4>
			</div>
		</div>
		<div class="buy">
			<button type="button" class="btn btn-buy" data-target="#advanced" data-form-field-type="Услуги : <?= $arResult["NAME"] ?>" data-toggle="modal"><a
						href="#"
																																																				title="link">Оставить заявку</a></button>
		</div>
	</div>
</div>
