<?

class BotChecker {

    static $bots = [
        'bot',
        'crawl',
        'archiver',
        'transcoder',
        'spider',
        'uptime',
        'validat',
        'fetcher',
        'get',
        'cron',
        'checker',
        'reader',
        'extractor',
        'monitoring',
        'analyzer',
        'scraper',
        'yandex',
        'google',
        'skype',
        'what',
        'tele',
        'yachoo',
        'url',
        'ads',
        'perl',
        'facebook',
        'twitter',
        'yeti',
        'java',
        'find',
        'dataprovider',
        'search',
        'rambler',
        'slurp',
        'mediapartners',
        'index',
        'stat',
        'metri',
        'aws',
        'online',
        'seo',
        'hunter',
        'ssl',
        'w3c'
    ];

    public function isBot($userAgent = null){

        if(!$userAgent) $userAgent = $_SERVER['HTTP_USER_AGENT'];
        if($userAgent){
            
            $regex = '#'.implode('|', self::$bots).'#i';
            return preg_match($regex, $userAgent);

        }

        return false;

    }


}