<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<section class="why_we">
	<div class="container">
		<div class="row">
			<h2>Почему именно мы</h2>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="wblock">
					<img src="<?= SITE_TEMPLATE_PATH?>/assets/img/impl/009.png" class="img-responsive">
					<p>Мы являемся лидерами
						по продажам и внедрению
						Битрикс24
					</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="wblock2">
					<img src="<?= SITE_TEMPLATE_PATH?>/assets/img/impl/010.png" class="img-responsive">
					<p>Обеспечим запуск, опираясь на опыт
						более чем <span>150 000</span> внедрений
						в различных отраслях
					</p>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="wblock3">
					<img src="<?= SITE_TEMPLATE_PATH?>/assets/img/impl/011.png" class="img-responsive">
					<p>Гарантируем лучшие условия
						приобретения
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
