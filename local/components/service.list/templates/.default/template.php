<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 heading">
			<?/*<span class="h2"><?=$arResult['NAME'] ?></span>*/?>
			<?if($arResult["DESCRIPTION"] ):?>
				<div class="p">
					<?= $arResult["DESCRIPTION"] ?>
				</div>
			<?endif;?>
		</div>

		<div class="col-xs-12 information">
			<? $globalCell = 0;
				foreach ($arResult["SECTIONS"] as $cell => $arSection):
					?>
					<div class="wr_information infor_<?= ($globalCell % 2) ? 'right' : 'left'; ?>">
						<div class="info_title">
							<h2><?= $arSection["NAME"] ?></h2>
							<div class="div_line"></div>
							<p><?= $arSection["UF_SHORT_DESCRIPTION"] ?></p>
							<div class="div_flex">
								<a href="<?= $arSection["SECTION_PAGE_URL"] ?>">Подробнее</a>
								<?if($arResult["IMAGES"][$arSection["DETAIL_PICTURE"]]):?><img src="<?= $arResult["IMAGES"][$arSection["DETAIL_PICTURE"]] ?>" alt="<?= $arSection["NAME"] ?>"/><?endif;?>
							</div>
						</div>
					</div>
					<?
					$globalCell ++;
				endforeach;?>
				<?foreach ($arResult["ITEMS"] as $cell => $arElement):
					?>
					<div class="wr_information infor_<?= ($globalCell % 2) ? 'right' : 'left'; ?>">
						<div class="info_title">
							<h2><?= $arElement["NAME"] ?></h2>
							<div class="div_line"></div>
							<p><?= $arElement["PREVIEW_TEXT"] ?></p>
							<div class="div_flex">
								<a href="<?= $arElement["DETAIL_PAGE_URL"] ?>">Подробнее</a>
								<?if($arResult["IMAGES"][$arSection["DETAIL_PICTURE"]]):?><img src="<?= $arResult["IMAGES"][$arElement["PREVIEW_PICTURE"]] ?>" alt="<?= $arElement["NAME"] ?>"/><?endif;?>
							</div>
						</div>
					</div>
					<?
					$globalCell ++;
				endforeach;?>
		</div>
	</div>
</div>
<br/><br/>
