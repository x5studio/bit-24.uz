<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
$APPLICATION->SetTitle($arResult["NAME"]);
?>
<style>
	.blog-detail-content .blog_anchors {
		list-style: none;
		padding: 0;
		margin: 30px 0;
	}
	.blog-detail-content .blog_anchors li {
		position: relative;
		padding-left: 12px;
		margin-bottom: 2px;
	}
	.blog-detail-content .blog_anchors li:before {
		content: "";
		display: inline-block;
		width: 6px;
		height: 6px;
		background-color: #ae127b;
		border-radius: 3px;
		padding: 0;
		margin-right: 5px;
		position: absolute;
		top: 7px;
		left: 0;
	}
	.blog-detail-content .blog_anchors li a {
		color: #324abd;
		font-weight: bold;
	}
	.blog-detail-content .blog_anchors ul li a {
		/*color: #000;*/
		font-weight: 400;
	}
	.blog-detail-content .blog_anchors ul li:before {
		content: "-";
		display: inline-block;
		color: #ae127b;
		padding: 0;
		margin-right: 5px;
		position: absolute;
		top: 0;
		left: 0;
		width: auto;
		height: auto;
		background-color: inherit;
		border-radius: 0;
	}
</style>
<div class="blog-detail-wrap">
	<div class="container">
		<div class="col-sm-7 col-md-8 col-xs-12 blog-detail-content-wrap">
			<? /* <h1><?= $arResult["NAME"] ?></h1> */ ?>
			<div class="blog-detail-content">
				<? /* if ($arResult["DETAIL_PICTURE"]): ?>
				  <img src="<?= $arResult["IMAGES"][$arResult["DETAIL_PICTURE"]] ?>" alt="<?= $arResult["NAME"] ?>">
				  <? endif; */ ?>
				<?= $arResult["DETAIL_TEXT"] ?>
			</div>

            <?
            $APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "question", Array(
                "WEB_FORM_ID" => WEB_FORM_QUESTION_ID,  // ID веб-формы
                "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
                "COMPANY_IBLOCK_ID" => IB_CONTACTS,
                "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
                "EMAIL_QUESTION_CODE" => "EMAIL",
                "CITY_QUESTION_CODE" => "CITY",
                "OFFICE_QUESTION_CODE" => "OFFICE",
                "TO_QUESTION_RESIPIENTS" => "",
                "EMAIL_ONLY_FROM_RESIPIENTS" => "",
                "PRODUCT_QUESTION_CODE" => "PRODUCT",
                "TO_QUESTION_CODE" => "TO",
                "IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
                "USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
                "SEF_MODE" => "N",  // Включить поддержку ЧПУ
                "CACHE_TYPE" => "A",  // Тип кеширования
                "CACHE_TIME" => "3600",  // Время кеширования (сек.)
                "LIST_URL" => "",  // Страница со списком результатов
                "EDIT_URL" => "",  // Страница редактирования результата
                "SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
                "CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
                "CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
                "VARIABLE_ALIASES" => array(
                    "WEB_FORM_ID" => "WEB_FORM_ID",
                    "RESULT_ID" => "RESULT_ID",
                ),
                "ALERT_ADD_SHARE" => "N",
                "HIDE_PRIVACY_POLICE" => "Y",
                "BTN_CALL_FORM" => "",
                "HIDE_FIELDS" => array(
                    0 => "CITY",
                    1 => "OFFICE",
                ),
                "COMPONENT_MARKER" => "question",
                "SHOW_FORM_DESCRIPTION" => "N",
                "MESS" => array(
                    "THANK_YOU" => "Спасибо, Ваша заявка принята.",
                    "WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
                    "TITLE_FORM" => "Остались вопросы?",
                )
            ),
                false,
                array(
                    "HIDE_ICONS" => "Y"
                )
            );
            ?>

			<? if ($arResult["PROPERTY_FEEDBACK_BTN_TITLE_VALUE"]): ?>
				<div class="blog-detail-feedback">
					<?= $arResult["PROPERTY_FEEDBACK_TOP_TEXT_VALUE"]; ?>
					<div class="text-center footbtn">
						<button type="button" onclick="yaCounter45927957.reachGoal('feedback-click'); return true;" class="btn btn-blocks top-button"
								data-toggle="modal" data-target="#advanced" data-form-field-type="<?= $arResult["PROPERTY_FEEDBACK_BTN_TITLE_VALUE"]; ?>">
									<?= $arResult["PROPERTY_FEEDBACK_BTN_TITLE_VALUE"]; ?>
						</button>
					</div>
				</div>
			<? endif; ?>
		</div>

		<div class="col-sm-5 col-md-4 col-xs-12 blog-detail-sidebar">

			<? /*
			  <div class="socnet-wrap">
			  <span class="title">Рассказать друзьям</span>
			  </div>

			  <div class="interesting-block">
			  <div class="title-block clearfix">
			  <span>Интересно</span>
			  <img src="/blog_new/images/detail/interesting_ico.png" alt="">
			  </div>
			  <div class="interesting-block-inner">
			  <p class="subtitle">Обзор всех функций нового Битрикс24.гонконг</p>
			  <p>Рассказываем о новых функциях, показанных 1 марта в рамках бизнес-встречи Битрикс24.Идея, в новой редакции Битркис24.Гонконг.</p>
			  <span class="date">30.12.2017</span>
			  </div>
			  </div>
			 */ ?>
			<div class="blog-subscribe">
				<button type="button" class="btn btn-blocks top-button read-later">Прочитать позже</button>
				<div class="blog-subscribe-form" style="display: none;">
					<?
					global $USER;
					if ($USER->isAdmin()) {
						$template = "blog_detail_modal";
					} else {
						$template = "blog_detail";
					}
					$APPLICATION->IncludeComponent("asd:subscribe.quick.form", 'blog_detail', Array(
						"FORMAT" => "html", // Формат подписки
						"INC_JQUERY" => "N", // Подключить jQuery
						"NOT_CONFIRM" => "N", // Подписывать без подтверждения
						"RUBRICS" => array(// Подписывать на рубрики
							0 => "1",
						),
						"SHOW_RUBRICS" => "N", // Показывать рубрики
						), false
					);
					?>
				</div>
			</div><br>

			<div class="tag-date-wrap">
				<? if ($arResult['PROPERTY_AUTHOR_VALUE'] != ""): ?>
					<? if (!empty($arResult['PROPERTY_AUTHOR_IMG_VALUE'])): ?>
						<img src="<?= $arResult["IMAGES"][$arResult['PROPERTY_AUTHOR_IMG_VALUE']] ?>" alt="">
					<? endif; ?>
					<div class="author-desc">
						<span class="author-name">Автор: <?= $arResult['PROPERTY_AUTHOR_VALUE'] ?></span>
						<? if (!empty($arResult['PROPERTY_AUTHOR_ROLE_VALUE'])): ?>
							<span class="author-role"><?= $arResult['PROPERTY_AUTHOR_ROLE_VALUE'] ?></span>
						<? endif; ?>
					</div>
				<? endif; ?>
				<span class="date"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
				<? foreach ($arResult["TAG"] as $arTag): ?>
					<span class="tag deep-blue"><?= $arTag["NAME"] ?></span>
				<? endforeach; ?>
			</div>

			<ul class="sections">
				<? foreach ($arResult['SECTIONS'] as $arSection): ?>
					<li>
						<? if ($arSection['CURRENT'] != 'Y'): ?>
							<a href="<?= $arSection['SECTION_PAGE_URL'] ?>" title="<?= $arSection['NAME'] ?>"><?= $arSection['NAME'] ?></a>
						<? else: ?>
							<span><?= $arSection['NAME'] ?></span>
						<? endif; ?>
					</li>
				<? endforeach; ?>
			</ul>
		</div>
	</div>

	<div class="content-bottom">
		<div class="container">
			<div class="col-sm-8 col-xs-12">
				<div class="col-md-6 col-sm-6 col-xs-12 text text-left">
					<!--                        <a class="btn btn-sm" onclick="javascript:history.back(); return false;">Вернуться назад</a>-->
					<script type="text/javascript">(function() {
							if (window.pluso)
								if (typeof window.pluso.start == "function")
									return;
							if (window.ifpluso == undefined) {
								window.ifpluso = 1;
								var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
								s.type = 'text/javascript';
								s.charset = 'UTF-8';
								s.async = true;
								s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
								var h = d[g]('body')[0];
								h.appendChild(s);
							}
						})();</script>
					<div class="pluso" data-background="transparent"
						 data-options="medium,square,line,horizontal,counter,theme=04"
						 data-services="vkontakte,facebook,odnoklassniki,moimir"></div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 date text-right">

				</div>
			</div>
		</div>
	</div>
	<!--Слайдер-->
	<? if (!empty($arResult['ARTICLES'])): ?>
		<script>
			$(document).ready(function() {
				$(".owl-carousel").owlCarousel({
					margin: 30,
					loop: false,
					nav: true,
					responsive: {
						0: {
							items: 1
						},
						600: {
							items: 2
						},
						1000: {
							items: 4
						}
					},
					avText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
				});
			});
		</script>
		<div class="content-bottom">

			<h3 class="fields-heading">Рекомендуемые статьи</h3>
			<div class="container">
				<!--<div class="owl-carousel row row-flex">
					<div class="owl-stage-outer">
						<div class="owl-stage">
				<? /* foreach ($arResult['ARTICLES'] as $arArticle): */ ?>
							<div class="item blog-slide">
								<a href="<? /* =$arArticle['DETAIL_PAGE_URL'] */ ?>">
									<img src="<? /* =$arArticle['PICTURE'] */ ?>" alt="<? /* =$arArticle['NAME'] */ ?>" >
									<div class="articles_slider_title">
										<span><? /* =$arArticle['NAME'] */ ?></span>
									</div>
								</a>
							</div>
				<? /* endforeach; */ ?>
						</div>
					</div>
					<div class="owl-controls">
						<div class="owl-nav">
							<div class="owl-prev"></div>
							<div class="owl-next"></div>
						</div>
						<div class="owl-dots">
							<div class="owl-dot active"><span></span></div>
							<div class="owl-dot"><span></span></div>
							<div class="owl-dot"><span></span></div>
						</div>
					</div>
				</div>-->
				<div class="owl-carousel owl-loaded">
					<div class="owl-stage-outer">
						<div class="owl-stage">
							<? foreach ($arResult['ARTICLES'] as $arArticle): ?>
								<div class="owl-item blog-slide">
									<a href="<?= $arArticle['DETAIL_PAGE_URL'] ?>">
										<img src="<?= $arArticle['PICTURE'] ?>" alt="<?= $arArticle['NAME'] ?>" >
										<div class="articles_slider_title">
											<span><?= $arArticle['NAME'] ?></span>
										</div>
									</a>
								</div>
							<? endforeach; ?>
						</div>
					</div>
					<div class="owl-nav">
						<div class="owl-prev"></div>
						<div class="owl-next"></div>
					</div>

				</div>
			</div>
		</div>
	<? endif; ?>
</div>

<script>
    $(function () {
	var images = $(".blog-detail-content img");
	var link = "";
	images.each(function (indx, element) {
		link = $(element).attr("src");
		if ($(element).closest("a").length < 1) {
			$(element).wrap("<a data-fancybox href='" + link + "'></a>");
		}
	});

	$(".read-later").on('click', function (e) {
		e.preventDefault();

		var sbsForm = $(this).closest(".blog-subscribe").find(".blog-subscribe-form");
		if(sbsForm.is(":visible")){
			sbsForm.slideUp();
		} else {
			sbsForm.slideDown();
		}
	});
    });
</script>
