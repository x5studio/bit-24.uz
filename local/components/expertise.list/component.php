<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Loader,
	Bitrix\Main\Page\Asset,
	\Proman\Helper;

/** @var CBitrixComponent $this */
/** @global CUser $USER */
global $USER;
/** @global CMain $APPLICATION */
global $APPLICATION;
/** @global CDatabase $DB */
global $DB;

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/assets/css/branches.css");

if (!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
if ($arParams["IBLOCK_ID"] <= 0)
{
	$this->AbortResultCache();
	ShowError("Не выбран инфоблок");
	return;
}
$arParams["COUNT"] = 3;

if ($this->StartResultCache())
{
	if (!Loader::IncludeModule("iblock"))
	{
		$this->AbortResultCache();
		ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
		return;
	}

	$arFile = array();

	$rsSection = \CIBlockSection::GetList(
		array("SORT" => "ASC"),
		array(
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"ACTIVE" => "Y"
		),
		false,
		array("ID", "NAME", "CODE", "SECTION_PAGE_URL", "PICTURE", "UF_CLASS", "UF_DESC"));
	while ($arSection = $rsSection->GetNext())
	{
		$arFile[] = $arSection["PICTURE"];
		$arResult["ITEMS"][] = $arSection;
	}

	$arResult["IMAGES"] = Helper::getImages($arFile);

	$this->IncludeComponentTemplate();
}