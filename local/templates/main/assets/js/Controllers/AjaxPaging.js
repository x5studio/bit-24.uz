if (typeof define === "function" && define.amd) {
    define([], function () {
        function AjaxPaging(container) {
            this.control = $(container);
            this.wrap = $(this.control.data('wrap'), this.control);
            this.pagination = this.control.find(".paging a");
            this.init();
        }

        AjaxPaging.prototype.init = function () {
            var o = this;
            this.pagination.click(function (event) {
                o.clickPaging(event, $(this));
            });
        };
        AjaxPaging.prototype.clickPaging = function (event, aObg) {
            event.preventDefault();
            var o = this;
            var tempHash = aObg.attr("href").split("#");
            var temp = aObg.attr("href").split("?");
            var mainUrl = (tempHash[0])?tempHash[0]:temp[0];
            var url = mainUrl + "?AJAX_PAGER=" + o.control.attr("id");
            if (temp[0].length > 0)    url += "&" + temp[1];
            $.ajax({
                type: "GET",
                url: url,
                success: function (content) {
                    o.wrap.html(content);
                    new AjaxPaging("#" + o.control.attr("id"));
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    o.wrap.html(textStatus);
                },
                beforeSend: function () {
                    o.wrap.html('<img class="floader" src="/bitrix/templates/main/img/loader.gif">');
                },
                complete: function () {

                }
            });
        };

        return AjaxPaging;
    });
}