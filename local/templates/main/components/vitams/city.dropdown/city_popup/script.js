$(function () {
    require(['main'], function () {
        require(['Controls/ComponentEngine', 'tools/tools', 'cookie'], function (ComponentEngine) {
            function CityDropdown(container) {
                this.container = container;
                this.options = this.prepareOptions(this.container.data());
                this.containerHref = $(container).find(".city-list-column a");
                this.containerTitle = this.container.prev("div.html_title");
                this.componentEngine = new ComponentEngine();
                this.active_flag = false;
                this.aliase_city = "CITY_CODE";
                this.pathUrl = window.location.pathname;
                this.smartRedirectClass = 'j-city-smart';
                this.init();
            }

            CityDropdown.prototype.prepareOptions = function (obData) {
                if (typeof obData == 'object') {
                    var jsonExp = new RegExp("^[\[\{\'\"]");
                    for (var i in obData) {
                        var item = obData[i];
                        if (typeof item == 'string' && jsonExp.test(item)) {
                            obData[i] = dataString2Json(item);
                        }
                    }
                    return obData;
                }
            };

            CityDropdown.prototype.init = function () {

                this.SetPath();
                this.CompileLinks();
                var o = this;
                //if(this.options.smartRedirect == 'Y' && this.options.isConfirmDropdown != 'Y') {
                this.containerHref.click(function(){
                    var _this = $(this);
                    var cookiePrefix = 'BITRIX_SM_';
                    if(o.options.smartRedirect == 'Y' && o.options.isConfirmDropdown != 'Y') {
                        setCookie(cookiePrefix + 'SMART_CHANGE_CITY', _this.attr("item-id"), null, '/');
                        window.location.reload();
                    }
                    if(o.options.linkBaseurl == 'Y' && o.options.sefMode == 'N') {
                        var valueSmartChangeCity = 0;
                        if(_this.data('smart-city') != '' || o.container.find('a[data-smart-city="'+_this.attr("item-id")+'"]').length > 0) {
                            valueSmartChangeCity = _this.attr("item-id");
                        }
                        var firstDot = window.location.hostname.indexOf('.');
                        domainForCook = window.location.hostname.substring(firstDot == -1 ? 0 : firstDot + 1);
                        //setCookie(cookiePrefix + 'SMART_CHANGE_CITY', valueSmartChangeCity, null, '/', domainForCook);

                        //var expires = new Date();
                        //expires.setDate(expires.getDate() + 3);
                        //setCookie(cookiePrefix + 'SAVE_CHANGE_CITY', _this.attr("item-id"), expires, '/', domainForCook);
                    }
                });

                //}
            };

            CityDropdown.prototype.SetPath = function () {
                var o = this;
                var temp = window.location.pathname.split("/");
                if (o.container.find(".city-list-column a[data-code='" + temp[temp.length - 2] + "']").length > 0) {
                    temp = temp.slice(0, temp.length - 2);
                    o.pathUrl = temp.join("/");
                    if (temp[temp.length - 1] != '/')    o.pathUrl += "/";
                }
                //TODO будет время переделать на события.
                if (o.pathUrl.indexOf('/school/education/') >= 0) {
                    o.pathUrl = "/school/education/";
                }
            };
            CityDropdown.prototype.CompileLinks = function () {
                var o = this;
                o.GetAliaseCity();
                o.containerHref.each(function () {
                    o.SetLink($(this));
                });

            };
            CityDropdown.prototype.SetLink = function (el) {
                var href_text = this.CompileLink(el);
                var cssClass = this.CompileCssClass(el);
                el.attr("href", href_text);
                el.addClass(cssClass);
            };
            CityDropdown.prototype.CompileLink = function (el) {
                var o = this;
                var code = el.data('code');
                var countryCode = el.data('country-code');
                var smartCity = parseInt(el.data('smart-city'));
                var codeForSefModeLink = code;
                var subDomain = "";
                var cityCode2Www = o.options['cityCodeToWww'];
                var href = '';
                if (o.options.localChoose == 'Y') {
                    href = 'javascript:void(0)';
                } else {
                    if(smartCity > 0) {
                        var smartCityElem = o.container.find('a[item-id="'+smartCity+'"]');
                        subDomain = smartCityElem.data('code');
                        countryCode = smartCityElem.data('country-code');
                        code = smartCityElem.data('code');
                        codeForSefModeLink = el.data('code');
                    }
                    if ($.inArray(code, cityCode2Www) >= 0) {
                        subDomain = "www";
                    }
                    else {
                        subDomain = code;
                    }
                    if (o.options.linkSubdomain == 'Y') {
                        href = this.GetSubDomainHref(countryCode, subDomain);
                    }
                    if(o.options.linkBaseurl == 'Y') {
                        if(o.options.sefMode == 'Y' && o.options.sefFolder.length > 0) {
                            href += this.componentEngine.makePathFromTemplate(o.options.sefFolder + o.options.urlTemplate, o.aliase_city, codeForSefModeLink);
                        }
                        else {
                            var urlPath = window.location.pathname;
                            if(urlPath.indexOf('/school/education/') >= 0) {
                                urlPath = '/school/education/';
                            }
                            href += urlPath;
                        }
                    }
                }
                return href;
            };
            CityDropdown.prototype.CompileCssClass = function (el) {
                var o = this;
                var cssClass = '';
                if (o.options.localChoose == 'Y') {
                    cssClass += 'j-new-choose';
                }
                if (o.options.smartRedirect == 'Y') {
                    cssClass += ' '+o.smartRedirectClass;
                }
                return cssClass;
            }
            CityDropdown.prototype.GetSubDomainHref = function (countryCode, subDomain) {
                var o = this;
                var arHost = o.options.regionHost.split(".");

                var baseDomains = o.options['baseDomains'];
                if (baseDomains.hasOwnProperty(countryCode)) {
                    arHost[arHost.length - 1] = baseDomains[countryCode];
                } else {
                    arHost[arHost.length - 1] = "ru";
                }

                var newHost = arHost.join(".");
                newHost = subDomain + "." + newHost;
                newHost = o.getDomainAlias(newHost);
                return location.protocol + "//" + newHost;
            };
            CityDropdown.prototype.GetAliaseCity = function () {
                var o = this;
                var defaultAliase = {
                    CITY_CODE: "CITY_CODE"
                };
                var customAliase = o.options['aliaseVars'];

                if (!o.active_flag && o.options.sefMode == 'Y') {
                    var newAliase = o.componentEngine.MakeComponentVariableAliases(defaultAliase, customAliase);
                    o.aliase_city = newAliase.CITY_CODE;
                    o.active_flag = true;
                }
            };
            CityDropdown.prototype.getDomainAlias = function (host) {
                var o = this;
                var aliasDomain = o.options['domainAlias'];
                if (typeof aliasDomain == 'object') {
                    for (var domain in aliasDomain) {
                        var alias = aliasDomain[domain];
                        if (domain == host) {
                            return alias;
                        }
                    }
                }
                return host;
            };



            function popupCityList(controller,listSettingsObj) {
                this.controller = controller;
                this.listSettingsObj = listSettingsObj;
                this.containerPopup = $(this.controller.attr("href"));
                this.cityList=new CityListLoader(listSettingsObj);
                this.init();
            }

            popupCityList.prototype.init = function () {
                var o = this;

                o.controller.click(function(){
                    if(!o.cityList.cityListIsLoaded&&!o.cityList.inProgress)
                        o.cityList.LoadCityListData();
                    o.openPopupCityList(o.controller, o.containerPopup);
                });
            }
            popupCityList.prototype.openPopupCityList = function (controller, containerPopup) {
                require(['Controls/Responsive', 'Controls/ModalManager', 'fancybox'], function (obResponsive, obModalManager) {
                    if ($.inArray(obResponsive.getCurType(), ['xsmall', 'small']) >= 0) {
                        var topRatio = 0;
                    } else {
                        topRatio = 0.5;
                    }
                    $.fancybox.open(containerPopup,{
                        padding: 0,
                        margin: 0,
                        height: 'auto',
                        width:'auto',
                        autoSize: false,
                        fitToView:false,
                        closeBtn: false,
                        topRatio: topRatio,
                        openEffect: 'none',
                        closeEffect: 'none',
                        afterLoad: function () {
                            controller.addClass("city-active");
                            containerPopup.find(".city-list-close").click(function () {
                                $.fancybox.close();
                            });
                        },
                        afterClose: function () {
                            controller.removeClass("city-active")
                        }
                    });
                });
            }

            /**
             * каждому компоненту свой загрузчик списка городов, согласно настройкам компонента
             * @param target - див с data аттрибутами настройки попапа списка городов
             * @constructor
             */
            function CityListLoader(target){
                this.cityListIsLoaded=false;
                this.inProgress=false;
                this.target=target;
            }

            /**
             * загружаем данные чрез ajax и помещаем полученый список в контейнер content-popup-city
             */
            CityListLoader.prototype.LoadCityListData= function () {
                var o=this.target;
                var context = o.context;
                var _this=this;
                _this.inProgress=true;
                $.ajax({
                    url: o.data("ajax-file"),
                    type: "POST",
                    data: "action=city-load&idcitypopup=" + $(this).attr("id") + "&ui_prioritet=" + o.data("ui-city-prioritet") + "&smart_redirect=" + o.data("smart-redirect"),
                    dataType: "html",
                    success: function (content) {
                        _this.cityListIsLoaded = true;
                        _this.inProgress=false;
                        $(".loader-city-img", context).remove();
                        o.find(".content-popup-city", context).append(content);
                        $(context).trigger('resize');
                    },
                    error: function () {
                        _this.inProgress=false;
                    },
                    complete: function () {
                        new CityDropdown(o);
                        $(document).trigger("loadCityCimple", [o]);
                        // актуально для Украины
                        if ($('.j-deffered', context).length > 0) {
                            require(['Controls/DeferredDataMng'], function (DeferredDataMng) {
                                new DeferredDataMng('.city-list-popup.j-city-list .j-deffered', context);
                            });
                        }
                    }
                });
            }

            function onInit (event){
                var context;
                if(event) context = event.contextContainer;
                $(".city-list-popup.j-city-list",context).each(function () {
                    var o = $(this);
                    var t = $("a[href='#"+o.attr("id")+"']",o.parent());
                    new popupCityList(t,o);
                });
            }
            // без LazyLoad
            onInit();
            // для LazyLoad
            $(document).on("onMainInit", function (event) {
                onInit(event);
            });
        })
    });
});