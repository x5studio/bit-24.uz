<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult["ITEMS"])): ?>
	<section class="why">
		<div class="container">
			<div class="row why-row">
				<div class="col-md-6 col-sm-10">
					<h2 class="why-heading pull-left">Функции Битрикс24</h2>
				</div>
				<div class="col-md-6 col-sm-2 hidden-xs">
					<noindex><a href="<?= $arParams["LINK_ALL"] ?>" class="btn btn-why pull-right">Подробнее</a></noindex>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row why__wrapper">
				<? foreach ($arResult["ITEMS"] as $arItem): ?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 why-block__item">
						<div class="why-block">
                            <div class="why-block__title"><?php echo $arItem['NAME']; ?></div>
                            <img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive"
                                 alt="<?= $arItem["NAME"] ?>">
							<p class="why-block__desc"><?= $arItem["PREVIEW_TEXT"] ?></p>
						</div>
					</div>
				<? endforeach; ?>
			</div>
		</div>
		<div class="hidden-lg hidden-md hidden-sm col-xs-12 detline">
			<a href="<?= $arParams["LINK_ALL"] ?>" class="btn btn-why">Подробнее</a>
		</div>
	</section>
<? endif; ?>