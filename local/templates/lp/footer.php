<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION, $USER;
?>
<?
if (!CSite::InDir('/lp/bitrix24/'))
{
	$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "request", Array(
		"WEB_FORM_ID" => 3,  // ID веб-формы
		"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
		"COMPANY_IBLOCK_ID" => IB_CONTACTS,
		"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
		"EMAIL_QUESTION_CODE" => "EMAIL",
		"CITY_QUESTION_CODE" => "CITY",
		"OFFICE_QUESTION_CODE" => "OFFICE",
		"TO_QUESTION_RESIPIENTS" => "",
		"EMAIL_ONLY_FROM_RESIPIENTS" => "",
		"PRODUCT_QUESTION_CODE" => "PRODUCT",
		"TO_QUESTION_CODE" => "TO",
		"IGNORE_CUSTOM_TEMPLATE" => "N",  // Игнорировать свой шаблон
		"USE_EXTENDED_ERRORS" => "Y",  // Использовать расширенный вывод сообщений об ошибках
		"SEF_MODE" => "N",  // Включить поддержку ЧПУ
		"CACHE_TYPE" => "A",  // Тип кеширования
		"CACHE_TIME" => "3600",  // Время кеширования (сек.)
		"LIST_URL" => "",  // Страница со списком результатов
		"EDIT_URL" => "",  // Страница редактирования результата
		"SUCCESS_URL" => "",  // Страница с сообщением об успешной отправке
		"CHAIN_ITEM_TEXT" => "",  // Название дополнительного пункта в навигационной цепочке
		"CHAIN_ITEM_LINK" => "",  // Ссылка на дополнительном пункте в навигационной цепочке
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		),
		"ALERT_ADD_SHARE" => "N",
		"HIDE_PRIVACY_POLICE" => "Y",
		"BTN_CALL_FORM" => ".btn_order",
		"HIDE_FIELDS" => array(
			0 => "CITY",
			1 => "OFFICE",
		),
		"COMPONENT_MARKER" => "request",
		"SHOW_FORM_DESCRIPTION" => "N",
		"MESS" => array(
			"THANK_YOU" => "Спасибо, Ваша заявка принята.",
			"WAIT_CALL" => "Мы свяжемся с Вами в течение 30 минут в рабочее время.",
			"TITLE_FORM" => "Оставить заявку",
		)
	),
		false,
		array(
			"HIDE_ICONS" => "Y"
		)
	);
}
?>
<section class="footer">
	<!-- top-footer -->
	<div class="top-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6 tfooter1">
					<div class="col-md-12 col-sm-12 hidden-xs footimg">
						<img src="<?= SITE_TEMPLATE_PATH ?>/assets/img/wlogo.png" class="pull-left">
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 footbtn">
                        <button type="button" class="btn btn-default" data-toggle="modal"
                                data-target="#advanced" data-form-field-type="Оставить заявку">Сделать заказ
                        </button>
					</div>
					<div class="col-md-12 col-sm-12 hidden-xs contacts pull-left">
						<p>Контактный телефон:<br><span><?$APPLICATION->ShowViewContent('office_phone');?></span>
						<p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 hidden-xs tfooter2">
					<? $APPLICATION->IncludeComponent("bitrix:menu", "service.footer", Array(
						"ALLOW_MULTI_SELECT" => "N",  // Разрешить несколько активных пунктов одновременно
						"CHILD_MENU_TYPE" => "",  // Тип меню для остальных уровней
						"DELAY" => "N",  // Откладывать выполнение шаблона меню
						"MAX_LEVEL" => "1",  // Уровень вложенности меню
						"MENU_CACHE_GET_VARS" => array(  // Значимые переменные запроса
							0 => "",
						),
						"MENU_CACHE_TIME" => "3600",  // Время кеширования (сек.)
						"MENU_CACHE_TYPE" => "A",  // Тип кеширования
						"MENU_CACHE_USE_GROUPS" => "N",  // Учитывать права доступа
						"ROOT_MENU_TYPE" => "service",  // Тип меню для первого уровня
						"USE_EXT" => "Y",  // Подключать файлы с именами вида .тип_меню.menu_ext.php
					),
						false
					); ?>
				</div>
				<div class="col-md-3 col-sm-6 hidden-xs tfooter3">
					<? $APPLICATION->IncludeComponent(
						"expertise.menu",
						"footer",
						Array(
							"CACHE_TIME" => "360000",
							"CACHE_TYPE" => "A",
							"IBLOCK_ID" => "3"
						)
					); ?>
				</div>
				<div class="col-md-3 col-sm-6 hidden-xs tfooter4">
					<? $APPLICATION->IncludeComponent("bitrix:menu", "type.footer", Array(
						"ALLOW_MULTI_SELECT" => "N",  // Разрешить несколько активных пунктов одновременно
						"CHILD_MENU_TYPE" => "",  // Тип меню для остальных уровней
						"DELAY" => "N",  // Откладывать выполнение шаблона меню
						"MAX_LEVEL" => "1",  // Уровень вложенности меню
						"MENU_CACHE_GET_VARS" => array(  // Значимые переменные запроса
							0 => "",
						),
						"MENU_CACHE_TIME" => "3600",  // Время кеширования (сек.)
						"MENU_CACHE_TYPE" => "A",  // Тип кеширования
						"MENU_CACHE_USE_GROUPS" => "N",  // Учитывать права доступа
						"ROOT_MENU_TYPE" => "type",  // Тип меню для первого уровня
						"USE_EXT" => "N",  // Подключать файлы с именами вида .тип_меню.menu_ext.php
					),
						false
					); ?>
					<h4 class="col-md-12 col-sm-12 hidden-xs"><a href="/vozmozhnosti-bitrix24/" title="Возможности Битрикс24">Возможности
							Битрикс24</a></h4>
					<h4 class="col-md-12 col-sm-12 hidden-xs"><a href="/blog/" title="Блог">Блог</a></h4>
					<h4 class="col-md-12 col-sm-12 hidden-xs"><a href="/nashi-klienty/" title="Наши клиенты">Наши клиенты</a></h4>
					<h4 class="col-md-12 col-sm-12 hidden-xs"><a href="/kontakty/" title="Контакты">Контакты</a></h4>
				</div>
			</div>
		</div>
	</div>
	<!-- bottom-footer -->
	<div class="bottom-footer">
		<div class="container">
			<div class="row vertical-align">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/___include___/footer.copyright.php", Array(), Array("MODE" => "html")); ?>
				</div>
				<div class="col-md-6 col-sm-6 hidden-xs">
					<div class="social_block pull-right">
						<? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/___include___/footer.social.php", Array(), Array("MODE" => "html")); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?
$APPLICATION->IncludeComponent("bezr:form.result.new.befsend", "feedback", Array(
		"WEB_FORM_ID" => FORM_FEEDBACK,	// ID веб-формы
		"COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
		"COMPANY_IBLOCK_ID" => IB_CONTACTS,
		"SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
		"EMAIL_QUESTION_CODE" => "EMAIL",
		"CITY_QUESTION_CODE" => "CITY",
		"OFFICE_QUESTION_CODE" => "OFFICE",
		"TO_QUESTION_RESIPIENTS" => "",
		"EMAIL_ONLY_FROM_RESIPIENTS" => "",
		"PRODUCT_QUESTION_CODE" => "PRODUCT",
		"TO_QUESTION_CODE" => "TO",
		"IGNORE_CUSTOM_TEMPLATE" => "N",	// Игнорировать свой шаблон
		"USE_EXTENDED_ERRORS" => "Y",	// Использовать расширенный вывод сообщений об ошибках
		"SEF_MODE" => "N",	// Включить поддержку ЧПУ
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"LIST_URL" => "",	// Страница со списком результатов
		"EDIT_URL" => "",	// Страница редактирования результата
		"SUCCESS_URL" => "",	// Страница с сообщением об успешной отправке
		"CHAIN_ITEM_TEXT" => "",	// Название дополнительного пункта в навигационной цепочке
		"CHAIN_ITEM_LINK" => "",	// Ссылка на дополнительном пункте в навигационной цепочке
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		),
		"ALERT_ADD_SHARE" => "N",
		"HIDE_PRIVACY_POLICE" => "Y",
		"BTN_CALL_FORM" => ".btn_order",
		"HIDE_FIELDS" => array(
			0 => "CITY",
			1 => "OFFICE",
			2 => "TYPE",
		),
		"COMPONENT_MARKER" => "feedform",
		"SHOW_FORM_DESCRIPTION" => "N",
		"MESS" => array(
			"THANK_YOU" => "Спасибо, Ваша заявка принята.",
			"WAIT_CALL" => "Мы свяжемся с Вами в течение 2-х часов.",
			"TITLE_FORM" => "Заказать услугу",
		)
	),
		false,
	array(
		"HIDE_ICONS" => "Y"
	)
	);
?>
</body>
</html>