<?php

namespace Sprint\Migration;


class Version20200917070605 extends Version
{
    protected $description = "task_21007 Убирает обязательность поля \"Иконка\" в ИБ \"Отраслевые решения\"";

    protected $moduleVersion = "3.16.3";

    /**
     * @throws Exceptions\HelperException
     * @return bool|void
     */
    public function up()
    {
        $iUserFieldId = \CUserTypeEntity::GetList(['ID' => 'ASC'], ['ENTITY_ID' => 'IBLOCK_3_SECTION', 'FIELD_NAME' => 'UF_ICON_MENU'])->Fetch()['ID'];

        $oUserTypeEntity = new \CUserTypeEntity();

        $oUserTypeEntity->Update($iUserFieldId, ['MANDATORY' => 'N']);
    }

    public function down()
    {
        $iUserFieldId = \CUserTypeEntity::GetList(['ID' => 'ASC'], ['ENTITY_ID' => 'IBLOCK_3_SECTION', 'FIELD_NAME' => 'UF_ICON_MENU'])->Fetch()['ID'];

        $oUserTypeEntity = new \CUserTypeEntity();

        $oUserTypeEntity->Update($iUserFieldId, ['MANDATORY' => 'Y']);
    }
}

