<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>
<div class="container">
	<div class="row">
		<article class="blog-article long-article with-bg col-sm-8 blog-carousel">
			<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
				<? if ($cell < 4): ?>
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0, 0, 0, 0.55), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>);">
						<span class="title"><?= $arItem["NAME"] ?></span>
						<div class="tag-date-wrap">
							<span class="tag deep-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
							<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</a>
				<? endif; ?>
			<? endforeach; ?>
			<div class="blog-carousel-control">
				<div class="left-arrow arrow">
					<img src="/blog_new/images/carousel_arrow.png" />
				</div>
				<span class="current-slide">1<span class="grey-color">/4</span></span>
				<div class="right-arrow arrow">
					<img src="/blog_new/images/carousel_arrow.png" />
				</div>
			</div>
		</article>
		<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
			<? if ($cell == 4): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0,0,0,0.9), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<span class="title"><?= $arItem["NAME"] ?></span>
						<div class="tag-date-wrap">
							<span class="tag light-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
							<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</a>
				</article>
			<? elseif ($cell == 5): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(45deg, rgb(0,2,125), rgba(1,32,173,0.7)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<span class="title"><?= $arItem["NAME"] ?></span>
						<div class="tag-date-wrap">
							<span class="tag white"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
							<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</a>
				</article>
			<? elseif ($cell == 6): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);">
						<span class="title"><?= $arItem["NAME"] ?></span>
						<div class="tag-date-wrap">
							<span class="tag deep-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
							<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</a>
				</article>
			<? elseif ($cell == 7): ?>
				<article class="blog-article transparent-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content">
						<span class="title"><?= $arItem["NAME"] ?></span>
						<p><?= $arItem["PREVIEW_TEXT"] ?></p>
						<div class="tag-date-wrap">
							<span class="tag deep-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
							<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</a>
				</article>
			<? endif; ?>
		<? endforeach; ?>
	</div>
</div>

<?
$APPLICATION->IncludeComponent("asd:subscribe.quick.form", "blog_list", Array(
	"FORMAT" => "html", // Формат подписки
	"INC_JQUERY" => "N", // Подключить jQuery
	"NOT_CONFIRM" => "N", // Подписывать без подтверждения
	"RUBRICS" => array(// Подписывать на рубрики
		0 => "1",
	),
	"SHOW_RUBRICS" => "N", // Показывать рубрики
	), false
);
?>

<div class="container">

	<div class="row">
		<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
			<? if ($cell == 8): ?>
				<article class="blog-article transparent-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content">
						<span class="title"><?= $arItem["NAME"] ?></span>
						<p><?= $arItem["PREVIEW_TEXT"] ?></p>
						<div class="tag-date-wrap">
							<span class="tag deep-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
							<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</a>
				</article>
			<? elseif ($cell == 9): ?>
				<article class="blog-article long-article with-bg col-sm-8">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>);">
						<span class="title"><?= $arItem["NAME"] ?></span>
						<div class="tag-date-wrap">
							<span class="tag deep-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
							<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</a>
				</article>
			<? elseif ($cell == 10): ?>
				<article class="blog-article long-article with-bg col-sm-8">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(25deg, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.08)) , url(<?= $arResult["IMAGES"][$arItem["DETAIL_PICTURE"]] ?>);">
						<span class="title"><?= $arItem["NAME"] ?></span>
						<div class="tag-date-wrap">
							<span class="tag deep-blue"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
							<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</a>
				</article>
			<? elseif ($cell == 11): ?>
				<article class="blog-article with-bg col-sm-4">
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="article-content" style="background-image:linear-gradient(45deg, rgb(0,2,125), rgba(1,32,173,0.7)) , url(<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>);"">
						<span class="title"><?= $arItem["NAME"] ?></span>
						<div class="tag-date-wrap">
							<span class="tag white"><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></span>
							<span class="date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</a>
				</article>
			<? endif; ?>
		<? endforeach; ?>
	</div>


	<nav class="pagination-row clearfix">
		<?= $arResult["NAV_STRING"] ?>
		<? /*
		  <a class="previous hidden-xs" href="#"><span aria-hidden="true"><< </span><span class="hidden-xs" aria-hidden="true">Предыдущая</span></a>
		  <ul class="pagination">
		  <li><a class="active">1</a></li>
		  <li><a href="#">2</a></li>
		  <li><a href="#">3</a></li>
		  <li><a href="#">4</a></li>
		  <li><span class="dots">...<span></li>
		  <li><a href="#">17</a></li>
		  </ul>
		  <a class="next hidden-xs" href="#"><span class="hidden-xs" aria-hidden="true">Следующая</span><span aria-hidden="true"> >></span></a>
		 */ ?>
	</nav>

</div>