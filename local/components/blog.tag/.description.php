<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Блог. Список тэгов",
	"DESCRIPTION" => "Блог. Список тэгов",
	"ICON" => "/images/news_line.gif",
	"SORT" => 120,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "proman",
		"CHILD" => array(
			"ID" => "blog"
		)
	),
);

?>