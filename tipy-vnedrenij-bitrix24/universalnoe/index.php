<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Универсальное внедрение CRM Битрикс24 за 4 шага от компании Первый Бит");
$APPLICATION->SetPageProperty("title", "Внедрение и запуск корпоративного портала и CRM Битрикс24");
$APPLICATION->SetTitle("Универсальный план внедрения CRM Битрикс24");
?>
	<section class="first-bt-block">
		<div class="container">
			<div class="row">
				<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/universalnoe/.inc.block_1.php", Array(), Array("MODE" =>
					"html")); ?>
			</div>
		</div>
	</section>
	<section class="three-crm-block">
		<div class="container">
			<div class="row">
				<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/universalnoe/.inc.block_2.php", Array(), Array("MODE" =>
					"html")); ?>
			</div>
		</div>
	</section>
<? $APPLICATION->IncludeComponent(
	"implementation.universal",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID" => "12"
	)
); ?>
<? $APPLICATION->IncludeFile("/tipy-vnedrenij-bitrix24/.inc.why.php", Array(), Array("MODE" =>
	"html")); ?>
<? $APPLICATION->IncludeComponent(
	"client.by.id",
	"",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"EXPERTISE_ID" => 32,
		"IBLOCK_ID" => "8"
	)
); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>