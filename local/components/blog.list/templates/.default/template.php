<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="main-blog">
	<div class="content">
		<div class="row">
			<? foreach ($arResult["ITEMS"] as $cell => $arItem): ?>
			<? if ($cell > 0 && $cell % 2 == 0): ?></div>
		<div class="row"><? endif; ?>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="blog1 pull-right">
					<div class="b-img effect">
						<img src="<?= $arResult["IMAGES"][$arItem["PREVIEW_PICTURE"]] ?>" class="img-responsive" alt="<?=
						$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>"/>
						<div class="mask">
							<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="info"></a>
						</div>
					</div>
					<h4 class="text-left"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" title="<?= $arItem["NAME"] ?>"><?= $arItem["NAME"] ?></a></h4>
					<div class="row row-text vertical-align">
						<div class="col-md-6 col-sm-6 col-xs-6 text text-left">
							<h5><a href="<?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["SECTION_PAGE_URL"] ?>"
								><?= $arResult["SECTIONS"][$arItem["IBLOCK_SECTION_ID"]]["NAME"] ?></a></h5>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 date text-right">
							<span class="date"><i class="fa fa-clock-o"
																		aria-hidden="true"></i><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></span>
						</div>
					</div>
					<p><?= $arItem["PREVIEW_TEXT"] ?></p>
					<a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" title="<?= $arItem["NAME"] ?>">Подробнее</a>
				</div>
			</div>
			<? endforeach; ?>
		</div>
		<?= $arResult["NAV_STRING"] ?>
	</div>
</div>