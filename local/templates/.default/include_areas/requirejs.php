<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var CMain $APPLICATION */
?>
<script type="text/javascript"
						src="<?= CUtil::GetAdditionalFileURL(MAIN_TEMPLATE_PATH . "/assets/js/vendor/requirejs/require.js", true) ?>"
						data-main="main"></script>
		<script type="text/javascript"><?
			$config = file_get_contents($_SERVER['DOCUMENT_ROOT'] . MAIN_TEMPLATE_PATH . '/assets/js/config.js');
			$configMin = preg_replace("(\s+|\r|\n)", ' ', $config);
			echo $configMin;
			?></script>