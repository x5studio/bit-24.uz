/**
 * Created by Павел on 29.01.2016.
 */
if (typeof define === "function" && define.amd) {
    define(['jquery', 'Controls/Contact/AContactBoxView', 'tools/tools'], function ($, AContactBoxView) {
        function ContactBoxView_n(item, options) {
            AContactBoxView.apply(this, arguments);
            this.options = $.extend(true, {}, this.options, ContactBoxView_n.defaultOptions, options);
        }

        extend(ContactBoxView_n, AContactBoxView);
        ContactBoxView_n.defaultOptions = {};

        ContactBoxView_n.prototype.compileOfficeItem = function (item, mOffice) {
            ContactBoxView_n.superclass.compileOfficeItem.apply(this, arguments);
            var o = this;
            var realOffice = o.getContactManager().getRealOfficeByID(mOffice['ID']);
            try {
                if (realOffice["PROPERTIES"]["ICON_METRO"]["VALUE"]) {
                    item.find('.box-office-name').addClass('metro metro-' + realOffice["PROPERTIES"]["ICON_METRO"]["VALUE"]);
                }
            } catch (e) {
            }
            if (mOffice['CODE'] == "proletarskaya") {
                item.find('.box-office-link').before('<div class="box-office-main"><strong>Центральный офис</strong></div>');
            }
        };

        ContactBoxView_n.prototype.renderOfficeList = function () {
            throw new ReferenceError();
        };

        ContactBoxView_n.prototype.renderOfficeTable = function (container) {
            var o = this;
            var mOffices = o.getModel('offices');
            container = $(container);

            var templateItem = o.getTemplate('offices-row', true);
            var cntRowItems = templateItem.find('td').size();
            var templateRow,
                j = 0,
                counter = 0;
            for (var i in mOffices) {
                var mOffice = mOffices[i];
                if (j == 0) {
                    templateRow = o.getTemplate('offices-row');
                }
                var $containerOffice = templateRow.find('td:eq(' + j + ')');
                var $officeItem = o.makeOfficeItem(mOffice);
                $containerOffice.append($officeItem.html());
                j++;
                counter++;
                if (j >= cntRowItems || mOffices.length == counter) {
                    j = 0;
                    container.append(templateRow);
                }
            }

            o.getView().trigger(new $.Event('contactBox.view.renderOfficeTable', {}));
        };

        ContactBoxView_n.prototype.renderContent = function () {
            var o = this;
            var $content = o.getView().find('.box-content');
            var $officeTable = $content.find('.offices-table');
            o.renderOfficeTable($officeTable);
            o.getView().trigger(new $.Event('contactBox.view.renderContent', {}));
        };

        return ContactBoxView_n;
    });
}