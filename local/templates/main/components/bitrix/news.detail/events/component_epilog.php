<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Page\Asset::getInstance()->addString('<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>');
?>
<div class="j-deffered">
    <div to="WIDGET_FEEDBACK_INLINE_FORM">
        <?
        $APPLICATION->IncludeComponent(
            "bezr:form.result.new.befsend",
            "inline",
            array(
                "WEB_FORM_ID" => FORM_EVENTS_APPLY,
                "COMPANY_IBLOCK_TYPE" => IBT_CONTACTS,
                "COMPANY_IBLOCK_ID" => IB_CONTACTS,
                "SUBSCRIBE_QUESTION_CODE" => "SUBSCRIBE",
                "EMAIL_QUESTION_CODE" => "EMAIL",
                "CITY_QUESTION_CODE" => "CITY",
                "OFFICE_QUESTION_CODE" => "OFFICE",
                "TO_QUESTION_RESIPIENTS" => '',
                "EMAIL_ONLY_FROM_RESIPIENTS" => "",
                "PRODUCT_QUESTION_CODE" => "PRODUCT",
                "TO_QUESTION_CODE" => "TO",
                "IGNORE_CUSTOM_TEMPLATE" => "N",
                "USE_EXTENDED_ERRORS" => "Y",
                "SEF_MODE" => "N",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "LIST_URL" => "",
                "EDIT_URL" => "",
                "SUCCESS_URL" => "",
                "CHAIN_ITEM_TEXT" => "",
                "CHAIN_ITEM_LINK" => "",
                "SHOW_TITLE" => "N",
                "PLACEHOLDER_MODE" => 'Y',
                "VARIABLE_ALIASES" => array(
                    "WEB_FORM_ID" => "WEB_FORM_ID",
                    "RESULT_ID" => "RESULT_ID",
                ),
                "ALERT_ADD_SHARE" => "N",
                "HIDE_PRIVACY_POLICE" => "Y",
                "HIDE_FIELDS" => array(
                    0 => "CITY",
                    1 => "OFFICE",
                    2 => "TYPE",
                ),
                "COMPONENT_MARKER" => "events_apply_inline",
                "SHOW_FORM_DESCRIPTION" => "N",
                "MESS" => array(
                    "THANK_YOU" => "Спасибо за заявку!",
                    "WAIT_CALL" => "Мы свяжемся с Вами в ближайшее время",
                    "TITLE_FORM" => "",
                )
            ),
            false,
            array(
                "HIDE_ICONS" => "Y"
            )
        );
        ?>
    </div>
</div>

<?
global $APPLICATION;
global $myTitle;
if (isset($arResult['MY_TITLE'])) {
	$myTitle = $arResult['MY_TITLE'];
}
?>