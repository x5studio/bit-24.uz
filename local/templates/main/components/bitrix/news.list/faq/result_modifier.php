<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arResult['SECTION'] = array();

$obSection = new \CIBlockSection();
$rsSection = $obSection->GetList(
	array(
		'DEPTH_LEVEL' => 'ASC',
		'SORT' => 'ASC',
	),
	array(
		'ACTIVE'    => 'Y',
		'IBLOCK_ID' => $arParams['IBLOCK_ID'],
		//'>=DEPTH_LEVEL' => 2,
	),
	false,
	array(
		'ID',
		'NAME',
		'IBLOCK_SECTION_ID',
		'DEPTH_LEVEL',
	)
);

while ($arSection = $rsSection->Fetch()) {
	if ($arSection['DEPTH_LEVEL']  == 1) {
		$arResult['SECTIONS'][$arSection['ID']] = $arSection;
	}
	if ($arSection['DEPTH_LEVEL']  == 2) {
		$arResult['SECTIONS'][$arSection['IBLOCK_SECTION_ID']]['CHILD'][] = $arSection;
	}
}

/*while ($arSection = $rsSection->Fetch()) {
	if ($arSection['DEPTH_LEVEL']  == 2) {
		$arResult['SECTIONS'][$arSection['ID']] = $arSection;
	}
	if ($arSection['DEPTH_LEVEL']  == 3) {
		$arResult['SECTIONS'][$arSection['IBLOCK_SECTION_ID']]['CHILD'][] = $arSection;
	}
}*/

//echo '<pre>';
//var_dump($arResult);

if ($arResult["ITEMS"]) {
	foreach ($arResult["ITEMS"] as $arItem) {
        $arResult['SECTION_ITEMS'][$arItem['IBLOCK_SECTION_ID']][] = $arItem;
    }
}

?>
