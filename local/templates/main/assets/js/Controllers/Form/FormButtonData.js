if (typeof define === "function" && define.amd) {
    define([], function () {
/**
* поддерживаются следующие data-атрибуты:
*
* data-form-title="Заголовок страницы"
* data-form-description="длинный текст под заголовком"
* data-form-submit="Текст на кнопке 'Отправить' "
* data-form-image="путь к картинке"
*
* так же есть "data-form-field-" он ставит значение полей в форме по датакоду. например:
*
* data-form-field-NAME="это значение попадёт в input с data-code='NAME' "
*/

        function _handler(){ // прототип обработчиков data-атрибутов
            this.rollbackData = null;
        }

        _handler.prototype.procced = function (form, data, button) { // функция обработки data-атрибута

        };

        _handler.prototype.rollback = function(form){ // функция отката измений, вызаваеться для очистки от предуших значений
            if(this.rollbackData)
            {
                this._rollback(form);
            }
            this.rollbackData = null;
        };

        _handler.prototype._rollback = function (form) {
            console && console.error('abstract method called');
        };


        function _html(selector) {
            this.selector = selector;
        }

        extend(_html,_handler);

        _html.prototype.procced = function (form, data) {
            var
                el = $(this.selector, form);

            this.rollbackData = el.html();
            el.html(data);
        };

        _html.prototype._rollback = function (form) {
            $(this.selector, form).html(this.rollbackData);
        };


        function _val(selector) {
            this.selector = selector;
        }

        extend(_val, _handler);

        _val.prototype.getElement = function (form) {
            return $(this.selector, form);
        };

        _val.prototype.procced = function (form, data) {
            var
                el = this.getElement(form);

            this.rollbackData = el.val();
            el.val(data);
        };

        _val.prototype._rollback = function (form) {
            this.getElement(form).val(this.rollbackData);
        };


        function _datacodeSimple(datacode) {
            this.datacode = datacode;
        }

        extend(_datacodeSimple, _val);

        _datacodeSimple.prototype.getElement = function (form) {
            return $('[data-code="'+this.datacode+'"]', form);
        };

        function _datacodeJson() {

        }

        extend(_datacodeJson, _handler);

        _datacodeJson.prototype.procced = function (form, data) {
            var
                d = JSON.parse(data), el;

            this.rollbackData = [];

            for(var datacode in d){
                if(d.hasOwnProperty(datacode)) {
                    el = $('[data-code="' + datacode + '"]', form);
                    this.rollbackData.push({
                        'val': el.val(),
                        'dataCode':datacode
                    });
                    el.val(d[datacode]);
                }
            }
        };

        _datacodeJson.prototype._rollback = function (form) {
            for(var i=0;i<this.rollbackData.length; i++)
            {
                var
                    d = this.rollbackData[i];

                $('[data-code="' + d.dataCode + '"]', form).val(d.val);
            }
        };


        function _img(selector) {
            this.selector = selector;
        }

        extend(_img, _handler);

        _img.prototype.procced = function(form, data)
        {
            var
                img = form.find(this.selector);

            if(img.length)
            {
                img = img.get(0);
                if(img.tagName === 'IMG')
                {
                    this.rollbackData = img.src;
                    img.src = data;
                }else{
                    this.rollbackData = img.style.backgroundImage;
                    img.style.backgroundImage = "url('"+data+"')";
                }
            }
        };

        _img.prototype._rollback = function (form) {
            var
                img = form.find(this.selector);

            if(img.length)
            {
                img = img.get(0);
                if(img.tagName === 'IMG')
                {
                    img.src = this.rollbackData;
                }else{
                    img.style.backgroundImage = this.rollbackData;
                }
            }
        };

        function FormButtonData(o) {

            //this.formObject = o;

            this.rulesIfExist =[ // правила выполняються если аттрибут присутсвует
                {
                    'dataName':'form-title',
                    'handler': new _html('.popup-title .title-text, .j-data-title')
                },
                {
                    'dataName':'form-description',
                    'handler': new _html('.j-data-description')
                },
                {
                    'dataName':'form-submit',
                    'handler': new _val('input[type="submit"]')
                },
                {
                    'dataName':'form-email',
                    'handler':new _datacodeSimple('EMAIL')
                },
                {
                    'dataName':'form-datacodes',
                    'handler':new _datacodeJson()
                },
                {
                    'dataName':'form-image',
                    'handler':new _img('.j-data-img')
                }
            ];

            this.alwaysRules = []; // правила выполняемые не зависимо от присутствия аттрибута
                                // (если надо к примеру ставить пустую строку если data атрибута нет)

            this.tmpRules = [];
        }

        FormButtonData.prototype.handlers = { // классы обработчиков для записи правил/наследования
            'base':_handler,
            'html':_html,
            'val':_val,
            'img':_img,
            'datacode':_datacodeSimple
        };

        FormButtonData.prototype.rollback = function (f, btn) {
            for(var i=0;i<this.rulesIfExist.length;i++)
            {
                this.rulesIfExist[i].handler.rollback(f);
            }

            for(i=0;i<this.alwaysRules.length;i++)
            {
                this.alwaysRules[i].handler.rollback(f);
            }

            for(i=0;i<this.tmpRules.length;i++)
            {
                this.tmpRules[i].rollback(f);
            }
            this.tmpRules = [];
        };

        FormButtonData.prototype.applyRules = function (form, button) {
            var
                attributes = button.data();

            for (var attrKey in attributes){
                if(attributes.hasOwnProperty(attrKey) && (attrKey.indexOf('formField') === 0)){
                    var
                        d = new _datacodeSimple((attrKey.slice(9)).toUpperCase());

                    d.procced(form, attributes[attrKey], button);

                    this.tmpRules.push(d);
                }
            }

            for(var i=0;i<this.rulesIfExist.length;i++){
                var
                    rule = this.rulesIfExist[i];

                if(typeof button.attr('data-'+rule.dataName) !== 'undefined'){
                    rule.handler.procced(form, button.attr('data-'+rule.dataName), button);
                }
            }

            for(i=0;i<this.alwaysRules.length;i++){
                rule = this.alwaysRules[i];

                var
                    v = button.attr('data-'+rule.dataName);

                rule.handler.procced(form, (typeof v === 'undefined') ? '' : v , button);
            }

        };


        FormButtonData.prototype.prepareForm = function (f, btn) {
            var
                button = $(btn),
                form = $(f);

            this.rollback(form, button);
            this.applyRules(form, button);
        };

        return FormButtonData;
    })
}