<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if (!empty($arResult["ITEMS"])): ?>
	<ul class="dropdown-menu mega-dropdown-menu">
		<? foreach ($arResult["ITEMS"] as $arItem): ?>
			<li class="col-md-3 col-sm-3 col-xs-12">
				<ul>
					<li><a href="<?= $arItem["SECTION_PAGE_URL"] ?>" class="top-menu-icon <?= $arItem["UF_CLASS_ICON"] ?>"><?= $arItem["NAME"] ?></a>
				</ul>
			</li>
		<? endforeach; ?>
	</ul>
<? endif; ?>