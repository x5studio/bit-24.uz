if (typeof define === "function" && define.amd) {
    define([], function () {
        function ListAdjust(p) {
            var o = this;
            this.root = $(p.root);
            this.numColItem = this.root.data('column');

            this.initializeDateHeight = function () {
                if (!o.root.is(':visible')) {
                    return false;
                }
                o.root.find('.list-item:first .j-adjust-row').each(function () {
                    var item = $(this);
                    var itemClass = $.trim(item.attr('class').replace('j-adjust-row', ''));
                    if (itemClass.length > 0) {
                        var arrItemClass = '.' + itemClass.replace(' ', '.');
                        o.setAdjustHeight(arrItemClass);
                    }

                });
                o.setAdjustHeight();
            };

            this.setAdjustHeight = function (childSelector) {
                this.resetAdjustHeight(childSelector);
                var isChild = (typeof childSelector != 'undefined') ? true : false;
                var items = o.root.find('.list-item');
                while (items.length) {
                    var curRowItems = $(items.splice(0, o.numColItem));
                    var maxHeight = 0;
                    curRowItems.each(function () {
                        var item = $(this);
                        if (isChild) {
                            var height = item.find(childSelector).height();
                        }
                        else {
                            var height = item.height();
                        }
                        if (height > maxHeight) {
                            maxHeight = height;
                        }
                    });
                    if (isChild) {
                        curRowItems.find(childSelector).css({height: maxHeight});
                    }
                    else {
                        curRowItems.css({height: maxHeight});
                    }

                }
            };

            this.resetAdjustHeight = function (childSelector) {
                var isChild = (typeof childSelector != 'undefined') ? true : false;
                var items = o.root.find('.list-item');
                if (isChild) {
                    items.find(childSelector).css({height: 'auto'});
                }
                else {
                    items.css({height: 'auto'});
                }
            };

            this.differedInitializeDateHeight = function (item) {
                var images = item.find('img');
                var imgDeferred = $.Deferred();
                var imagesComplete = 0;
                if (!images.length)
                    checkAll(imagesComplete);

                function checkAll(cnt) {
                    if (images.length == cnt) {
                        imgDeferred.resolve();
                    }
                }

                imgDeferred.done(function () {
                    o.initializeDateHeight();
                });

                images.each(function () {
                    if (this.complete) {
                        imagesComplete++;
                        checkAll(imagesComplete);
                    }
                    else {
                        var item = $(this);
                        item.on('load', function () {
                            imagesComplete++;
                            checkAll(imagesComplete);
                        });
                    }
                });
            };

            this.init = function () {
                o.root.on('updateList', function () {
                    o.differedInitializeDateHeight($(this));
                });
                o.differedInitializeDateHeight(o.root);
            };

            this.init();
        }

        return ListAdjust;
    });
}