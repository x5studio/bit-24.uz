<?
$MESS["BIT_ADO_OPTIONS_TAB_NAME"] 		 	           = "Настройки";
$MESS["BIT_ADO_OPTIONS_TAB_COMMON"] 	 	           = "Основные";

$MESS["BIT_ADO_OPTIONS_YES"] 	 	           = "Да";
$MESS["BIT_ADO_OPTIONS_NO"] 	 	           = "Нет";

$MESS["BIT_ADO_OPTIONS_TAB_SWITCH_ON"] 		 	           = "Включить модуль?";

$MESS["BIT_ADO_OPTIONS_TAB_OPTIMIZE"] 		 	           = "Оптимизация изображений";
$MESS["BIT_ADO_OPTIONS_TAB_OPTIMIZE_SWITCH"] 	 	           = "Использовать оптимизацию?";
$MESS["BIT_ADO_OPTIONS_TAB_OPTIMIZE_MODE"] 		 	           = "Режим";
$MESS["BIT_ADO_OPTIONS_TAB_OPTIMIZE_QUALITY"] 	 	           = "Качество";
$MESS["BIT_ADO_OPTIONS_TAB_OPTIMIZE_MAXWIDTH"] 		 	       = "Максимальная ширина";
$MESS["BIT_ADO_OPTIONS_TAB_OPTIMIZE_MAXHEIGHT"] 	 	       = "Максимальная высота";

$MESS["BIT_ADO_OPTIONS_TAB_WEBP"] 	 	           = "WEBP";
$MESS["BIT_ADO_OPTIONS_TAB_WEBP_SWITCH"] 		 	           = "Использовать webp?";
$MESS["BIT_ADO_OPTIONS_TAB_WEBP_MODE"] 	 	           = "Режим";

$MESS["BIT_ADO_OPTIONS_TAB_LAZYLOAD"] 		 	           = "Ленивая загрузка";
$MESS["BIT_ADO_OPTIONS_TAB_LAZYLOAD_SWITCH"] 	 	           = "Использовать ленивую загрузку?";
$MESS["BIT_ADO_OPTIONS_TAB_LAZYLOAD_MODE"] 	 	           = "Режим";


$MESS["BIT_ADO_OPTIONS_TAB_DEBUG"] 		 	           = "Отладка";
$MESS["BIT_ADO_OPTIONS_TAB_DEBUG_SWITCH"] 	 	           = "Включить?";

$MESS["BIT_ADO_OPTIONS_TAB_MANUAL_MODIF"] 	 	           = "Ручная обработка";