/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils_ie_fix__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);
/* harmony import */ var _modules_changes_more_blog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2);
/* harmony import */ var _modules_cards_height__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3);
/* harmony import */ var _modules_tabs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(4);
/* harmony import */ var _modules_tableHeight__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5);




 // Utils
// ---------------------------------

Object(_utils_ie_fix__WEBPACK_IMPORTED_MODULE_0__["ieFix"])(); // Modules
// ---------------------------------

Object(_modules_changes_more_blog__WEBPACK_IMPORTED_MODULE_1__["changesMoreBlog"])();
Object(_modules_cards_height__WEBPACK_IMPORTED_MODULE_2__["cardsHeight"])();
Object(_modules_tabs__WEBPACK_IMPORTED_MODULE_3__["tabs"])();
Object(_modules_tableHeight__WEBPACK_IMPORTED_MODULE_4__["tableHeight"])();

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ieFix", function() { return ieFix; });
/* eslint-disable */
var ieFix = function ieFix() {
  // Polyfills
  //---------------------------------
  // forEach
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = function (callback, thisArg) {
      thisArg = thisArg || window;

      for (var i = 0; i < this.length; i++) {
        callback.call(thisArg, this[i], i, this);
      }
    };
  } // includes


  if (!Array.prototype.includes) {
    Object.defineProperty(Array.prototype, 'includes', {
      value: function value(searchElement, fromIndex) {
        if (this == null) {
          throw new TypeError('"this" is null or not defined');
        }

        var o = Object(this);
        var len = o.length >>> 0;

        if (len === 0) {
          return false;
        }

        var n = fromIndex | 0;
        var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

        function sameValueZero(x, y) {
          return x === y || typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y);
        }

        while (k < len) {
          if (sameValueZero(o[k], searchElement)) {
            return true;
          }

          k++;
        }

        return false;
      }
    });
  } // matches


  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.matchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelector || Element.prototype.oMatchesSelector || Element.prototype.webkitMatchesSelector || function (s) {
      var matches = (this.document || this.ownerDocument).querySelectorAll(s);
      var i = matches.length; // eslint-disable-next-line no-empty

      while (--i >= 0 && matches.item(i) !== this) {}

      return i > -1;
    };
  } // closest


  if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
  }

  if (!Element.prototype.closest) {
    Element.prototype.closest = function (s) {
      var el = this;

      do {
        if (el.matches(s)) {
          return el;
        }

        el = el.parentElement || el.parentNode;
      } while (el !== null && el.nodeType === 1);

      return null;
    };
  } // prepend


  (function (arr) {
    arr.forEach(function (item) {
      if (item.hasOwnProperty("prepend")) {
        return;
      }

      Object.defineProperty(item, "prepend", {
        configurable: true,
        enumerable: true,
        writable: true,
        value: function prepend() {
          // eslint-disable-next-line prefer-rest-params
          var argArr = Array.prototype.slice.call(arguments);
          var docFrag = document.createDocumentFragment();
          argArr.forEach(function (argItem) {
            var isNode = argItem instanceof Node;
            docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
          });
          this.insertBefore(docFrag, this.firstChild);
        }
      });
    });
  })([Element.prototype, Document.prototype, DocumentFragment.prototype]); // append


  (function (arr) {
    arr.forEach(function (item) {
      if (item.hasOwnProperty("append")) {
        return;
      }

      Object.defineProperty(item, "append", {
        configurable: true,
        enumerable: true,
        writable: true,
        value: function append() {
          // eslint-disable-next-line prefer-rest-params
          var argArr = Array.prototype.slice.call(arguments);
          var docFrag = document.createDocumentFragment();
          argArr.forEach(function (argItem) {
            var isNode = argItem instanceof Node;
            docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
          });
          this.appendChild(docFrag);
        }
      });
    });
  })([Element.prototype, Document.prototype, DocumentFragment.prototype]); // before


  (function (arr) {
    arr.forEach(function (item) {
      if (item.hasOwnProperty("before")) {
        return;
      }

      Object.defineProperty(item, "before", {
        configurable: true,
        enumerable: true,
        writable: true,
        value: function before() {
          // eslint-disable-next-line prefer-rest-params
          var argArr = Array.prototype.slice.call(arguments);
          var docFrag = document.createDocumentFragment();
          argArr.forEach(function (argItem) {
            var isNode = argItem instanceof Node;
            docFrag.appendChild(isNode ? argItem : document.createTextNode(String(argItem)));
          });
          this.parentNode.insertBefore(docFrag, this);
        }
      });
    });
  })([Element.prototype, CharacterData.prototype, DocumentType.prototype]); // remove


  (function (arr) {
    arr.forEach(function (item) {
      if (item.hasOwnProperty("remove")) {
        return;
      }

      Object.defineProperty(item, "remove", {
        configurable: true,
        enumerable: true,
        writable: true,
        value: function remove() {
          if (this.parentNode !== null) {
            this.parentNode.removeChild(this);
          }
        }
      });
    });
  })([Element.prototype, CharacterData.prototype, DocumentType.prototype]); // startsWith


  if (!String.prototype.startsWith) {
    // eslint-disable-next-line no-extend-native
    Object.defineProperty(String.prototype, "startsWith", {
      value: function value(search, rawPos) {
        var pos = rawPos > 0 ? rawPos | 0 : 0;
        return this.substring(pos, pos + search.length) === search;
      }
    });
  } // Fixes
  //---------------------------------
  // ie download


  var ie11Download = function ie11Download(el) {
    if (el.href === "") {
      throw Error("The element has no href value.");
    }

    var filename = el.getAttribute("download");

    if (filename === null || filename === "") {
      var tmp = el.href.split("/");
      filename = tmp[tmp.length - 1];
    }

    el.addEventListener("click", function (evt) {
      evt.preventDefault();
      var xhr = new XMLHttpRequest();

      xhr.onloadstart = function () {
        xhr.responseType = "blob";
      };

      xhr.onload = function () {
        navigator.msSaveOrOpenBlob(xhr.response, filename);
      };

      xhr.open("GET", el.href, true);
      xhr.send();
    });
  };

  if (window.navigator.msSaveBlob) {
    var downloadLinks = document.querySelectorAll("a[download]");

    if (downloadLinks.length) {
      downloadLinks.forEach(function (el) {
        ie11Download(el);
      });
    }
  } // ie svg focus fix


  var unfocusableSvg = function unfocusableSvg() {
    if (!(!!window.MSInputMethodContext && !!document.documentMode)) {
      return;
    }

    var svg = document.querySelectorAll('svg');
    svg.forEach(function (el) {
      el.setAttribute('focusable', 'false');
    });
  };

  unfocusableSvg(); //ie footer nailing

  var ieFooterNailing = function ieFooterNailing() {
    var main = document.querySelector('main');
    var header = document.querySelector('.header');
    var footer = document.querySelector('.footer');
    var headerH;
    var footerH;
    var mainHMin;

    if (!main || !(!!window.MSInputMethodContext && !!document.documentMode)) {
      return;
    }

    var mainHeight = function mainHeight() {
      // eslint-disable-next-line no-unused-expressions
      header ? headerH = header.getBoundingClientRect().height : headerH = 0; // eslint-disable-next-line no-unused-expressions

      footer ? footerH = footer.getBoundingClientRect().height : footerH = 0;
      mainHMin = window.innerHeight;
      main.style.minHeight = mainHMin - (headerH + footerH) + 'px';
    };

    document.addEventListener('loadDOMContentLoaded', mainHeight());
    window.addEventListener('resize', mainHeight);
  };

  ieFooterNailing();
};



/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changesMoreBlog", function() { return changesMoreBlog; });
var changesMoreBlog = function changesMoreBlog() {
  window.onload = function () {
    var blogBtnBlock = document.querySelector('.blog-item__btn-block');
    var currentIndex;

    if (!blogBtnBlock) {
      return;
    }

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
      if (this.readyState === 4 && this.status === 200) {
        var blogsObj = JSON.parse(this.responseText);

        var _blogs = document.querySelectorAll('.blog-item');

        if (_blogs.length < 1) {
          return;
        }

        var createBlog = function createBlog(e, data) {
          var templateMore = e.querySelector('.blog-more');
          var m;
          var mEnd;
          var j;
          var jEnd;
          var maxStep;

          if (window.matchMedia('(max-width: 1023px)').matches) {
            m = 1;
            mEnd = m + 1;
          } else {
            m = 1;
            mEnd = m + 2;
          }

          j = 6;
          maxStep = (data.length - 6) / 2;
          jEnd = j + 5;
          var index = 0;
          var mores = Math.ceil((data.length - 6) / 5);

          var renderMore = function renderMore(all = false) {
            for (m; m < mEnd; m++) {
              var cloneMore = templateMore.content.cloneNode(true);
              var list = cloneMore.querySelector('.blog-item__more');
              list.classList.add('blog-item__more--open');

              if (all && (jEnd == 6+5) ) {
                jEnd -= 5;
                jEnd += maxStep;
              }

              for (j; j < jEnd; j++) {
                if (j > data.length - 1) {
                  break;
                }

                var mohs = data[j];
                list.insertAdjacentHTML('beforeEnd', '<li class="blog-item__item"><a class="blog-item__link" href="' + mohs.href + '">' + mohs.title + '</a></li>');
              }

              templateMore.parentNode.appendChild(cloneMore);
              index++;

              if (j + 5 > data.length) {
                j = j;
                jEnd = data.length;
              } else {
                j = j;
                jEnd = jEnd + (all ? maxStep : 5);
              }
            }
          };

          var btnMore = e.querySelector('.blog-item__btn--js');

          if (!btnMore) {
            return;
          }

          if ( (typeof blogCurSectionCode!="undefined") && blogCurSectionCode ) {
            renderMore(true);
          }

          btnMore.addEventListener('click', function () {
            if (m <= mores) {
              e.classList.add('blog-item--open');
              renderMore();

              if (window.matchMedia('(max-width: 1024px)').matches) {
                m = 1;
                mEnd = m + 1;
              } else {
                m = 1;
                mEnd = m + 2;
              }
            }

            if (index * 2 > mores + 1) {
              btnMore.style.display = 'none';
            }
          });
          var templateCard = e.querySelector('.blog-card');
          var pagination = e.querySelector('.pagination');
          var pages = Math.ceil(data.length / 6);

          if (pages <= 1) {
            btnMore.style.display = 'none';
          }

          var showPag = function showPag() {
            if (pages > 5) {
              pagination.insertAdjacentHTML('afterBegin', '<li class="pagination__item pagination__item--prew pagination__item--disabled"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-prew"/></svg></a></li>');

              for (var p = 0; p < 5; p++) {
                pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + (p + 1) + '</a></li>');

                var _paginationItems = pagination.querySelectorAll('.pagination__item');

                if (p === 1) {
                  _paginationItems[p].classList.add('pagination__item--current');
                }
              }

              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--no-hover"><span>...</span></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + pages + '</a></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--next"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-next"/></svg></a></li>');
            } else {
              pagination.insertAdjacentHTML('afterBegin', '<li class="pagination__item pagination__item--prew pagination__item--disabled"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-prew"/></svg></a></li>');

              for (var _p = 0; _p < pages; _p++) {
                pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + (_p + 1) + '</a></li>');

                var _paginationItems2 = pagination.querySelectorAll('.pagination__item');

                if (_p === 1) {
                  _paginationItems2[_p].classList.add('pagination__item--current');
                }
              }

              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--next"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-next"/></svg></a></li>');
            }
          };

          showPag();
          var i = 0;
          var end = 6;
          var paginationItems = pagination.querySelectorAll('.pagination__item');

          var renderBlog = function renderBlog() {
            var _loop = function _loop() {
              if (i >= data.length) {
                return "break";
              }

              var mohs = data[i];
              var clone = templateCard.content.cloneNode(true);
              var item = clone.querySelector('.blog-item__item');
              var link = clone.querySelector('.blog-item__link');
              var img = item.querySelector('.blog-item__img');
              var text = clone.querySelector('.blog-item__text');
              item.classList.add('shown');
              link.href = mohs.href;

              if (mohs.main === 'yes') {
                item.classList.add('blog-item__item--img');
                img.src = mohs.srcset;
                img.srcset = mohs.src;
                img.width = mohs.width;
                img.height = mohs.height;
                img.alt = mohs.alt;
              }

              text.textContent = mohs.title;
              setTimeout(function () {
                templateCard.parentNode.appendChild(clone);
              }, 500);
            };

            for (i; i < end; i++) {
              var _ret = _loop();

              if (_ret === "break") break;
            }
          };

          pagination.addEventListener('click', function (event) {
            if (event.target.classList.contains('pagination')) {
              return;
            }

            var ep = event.target;
            var currentI = ep.textContent;
            var currentIn = Number.parseInt(currentI);

            if (currentIn > 0) {
              currentIndex = currentIn;
            } else if (event.target.parentNode.parentNode.classList.contains('pagination__item--next')) {
              currentI = pagination.querySelector('.pagination__item--current').textContent;
              currentIn = Number.parseInt(currentI);

              if (currentIndex === pages) {
                currentIndex = currentIn;
              } else {
                currentIndex = currentIn + 1;
              }
            } else if (event.target.parentNode.parentNode.classList.contains('pagination__item--prew')) {
              currentI = pagination.querySelector('.pagination__item--current').textContent;
              currentIn = Number.parseInt(currentI);

              if (currentIndex === 1) {
                currentIndex = currentIn;
              } else {
                currentIndex = currentIn - 1;
              }
            }

            var currentPag = pagination.querySelector('.pagination__item--current');
            event.preventDefault();
            btnMore.style.display = 'none';
            var hideElem = e.querySelectorAll('.blog-item__item');
            hideElem.forEach(function (er) {
              er.classList.add('none');
              setTimeout(function () {
                er.parentNode.removeChild(er);
              }, 500);
            });
            paginationItems.forEach(function (evt) {
              evt.classList.remove('pagination__item--current');
            });
            ep.classList.add('pagination__item--current');

            while (pagination.firstChild) {
              pagination.removeChild(pagination.firstChild);
            }

            currentIn = currentIndex;

            if (currentIn >= 5 && currentIn <= pages - 3 && pages > 5) {
              pagination.insertAdjacentHTML('afterBegin', '<li class="pagination__item pagination__item--prew"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-prew"/></svg></a></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + 1 + '</a></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--no-hover"><span>...</span></li>');

              for (var p = currentIn - 1; p <= currentIn + 1; p++) {
                pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + p + '</a></li>');
              }

              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--no-hover"><span>...</span></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + pages + '</a></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--next"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-next"/></svg></a></li>');
              var qwes = pagination.querySelectorAll('.count');
              qwes.forEach(function (qw) {
                var qwText = qw.textContent;
                var qwIn = Number.parseInt(qwText);

                if (qwIn === currentIndex) {
                  qw.classList.add('pagination__item--current');
                }
              });
            } else if (currentIn > pages - 3 && pages > 5) {
              pagination.insertAdjacentHTML('afterBegin', '<li class="pagination__item pagination__item--prew"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-prew"/></svg></a></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + 1 + '</a></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--no-hover"><span>...</span></li>');

              for (var _p2 = pages - 3; _p2 < pages; _p2++) {
                pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + _p2 + '</a></li>');
              }

              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + pages + '</a></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--next"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-next"/></svg></a></li>');

              var _qwes = pagination.querySelectorAll('.count');

              _qwes.forEach(function (qw) {
                var qwText = qw.textContent;
                var qwIn = Number.parseInt(qwText);

                if (qwIn === currentIndex) {
                  qw.classList.add('pagination__item--current');
                }
              });
            } else if (currentIn < 5 && pages > 5) {
              pagination.insertAdjacentHTML('afterBegin', '<li class="pagination__item pagination__item--prew"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-prew"/></svg></a></li>');

              for (var _p3 = 0; _p3 < 5; _p3++) {
                pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + (_p3 + 1) + '</a></li>');
              }

              var _paginationItems3 = pagination.querySelectorAll('.count');

              _paginationItems3[currentIn - 1].classList.add('pagination__item--current');

              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--no-hover"><span>...</span></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + pages + '</a></li>');
              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--next"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-next"/></svg></a></li>');
            } else {
              pagination.insertAdjacentHTML('afterBegin', '<li class="pagination__item pagination__item--prew"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-prew"/></svg></a></li>');

              for (var _p4 = 0; _p4 < pages; _p4++) {
                pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item count"><a href="#">' + (_p4 + 1) + '</a></li>');
              }

              var _paginationItems4 = pagination.querySelectorAll('.count');

              _paginationItems4[currentIn - 1].classList.add('pagination__item--current');

              pagination.insertAdjacentHTML('beforeEnd', '<li class="pagination__item pagination__item--next"><a href="#"><svg x="0px" y="0px" width="24px" height="24px"><use xlink:href="#icon-arrow-next"/></svg></a></li>');
            }

            if (currentIn === 1) {
              if (pages > 1) {
                btnMore.style.display = 'block';
              } else {
                btnMore.style.display = 'none';
              }
              var pagPrew = pagination.querySelector('.pagination__item--prew');
              pagPrew.classList.add('pagination__item--disabled');
            } else if (currentIn > 1 && currentIn < pages) {
              var _pagPrew = pagination.querySelector('.pagination__item--prew');

              var pagNext = pagination.querySelector('.pagination__item--next');

              _pagPrew.classList.remove('pagination__item--disabled');

              pagNext.classList.remove('pagination__item--disabled');
            } else if (currentIn === pages) {
              var _pagNext = pagination.querySelector('.pagination__item--next');

              _pagNext.classList.add('pagination__item--disabled');
            }

            i = (currentIn - 1) * 6;
            end = i + 6;
            renderBlog();
          });
          renderBlog();
        };

        for (var n = 0; n < _blogs.length; n++) {
          var name = 'blog' + (n + 1);
          var data = blogsObj[name];
          var e = _blogs[n];
          createBlog(e, data);
        }
      }
    };

    xmlhttp.open('GET', '/ajax/blogs.php'+(typeof blogCurSectionCode!="undefined" ? '?sectioncode='+blogCurSectionCode : ''), true);
    xmlhttp.send();
    var blogs = document.querySelectorAll('.blog-item');

    if (blogs.length < 1) {
      return;
    }
  };
};



/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cardsHeight", function() { return cardsHeight; });
var cardsHeight = function cardsHeight() {
  // добавить класс same-height-js к ul для одинаковой высоты карточек
  var cards = document.querySelectorAll(".same-height-js");
  var viewport = screen.availWidth;
  var shift;

  var heightCalculate = function heightCalculate() {
    var _loop = function _loop(i) {
      var card = cards[i].querySelectorAll("li");
      var cardsArr = [];
      card.forEach(function (element) {
        setTimeout(function () {
          cardsArr.push(element.clientHeight);
        }, 300);
      });

      var getMaxOfArr = function getMaxOfArr(arr) {
        return Math.max.apply(null, cardsArr);
      };

      card.forEach(function (element) {
        element.style.height = "";
        setTimeout(function () {
          element.style.height = "".concat(getMaxOfArr(), "px");
        }, 300);
      });
    };

    for (var i = 0; i < cards.length; i++) {
      _loop(i);
    }
  };

  heightCalculate();
  window.addEventListener("resize", function () {
    shift = screen.availWidth - viewport;
    viewport = viewport + shift;

    if (shift === 0) {
      return;
    }

    heightCalculate();
  });
};



/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tabs", function() { return tabs; });
var tabs = function tabs() {
  var tabsContainer = document.querySelectorAll(".tabs-container-js");
  tabsContainer.forEach(function (element) {
    var tabButtons = element.querySelectorAll(".tab-button-js");
    var tabs = element.querySelectorAll(".tab-js");

    var _loop = function _loop(i) {
      tabButtons[i].addEventListener("click", function () {
        tabs.forEach(function (element) {
          element.classList.add("visually-hidden");
        });
        tabButtons.forEach(function (element) {
          element.classList.remove("is-active");
        });

        if (tabButtons[i].dataset.tabButton === tabs[tabButtons[i].dataset.tabButton].dataset.tab) {
          tabButtons[i].classList.add("is-active");
          tabs[tabButtons[i].dataset.tabButton].classList.remove("visually-hidden");
        }
      });
    };

    for (var i = 0; i < tabButtons.length; i++) {
      _loop(i);
    }
  });
};



/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tableHeight", function() { return tableHeight; });
var tableHeight = function tableHeight() {
  var table = document.querySelectorAll(".price-table");
  var buttons = document.querySelectorAll(".price__buttons");

  var heightClear = function heightClear() {
    var headers = document.querySelectorAll(".price-table h3");
    headers.forEach(function (element) {
      element.style.height = "";
    });
  };

  var calculate = function calculate() {
    heightClear();
    table.forEach(function (element) {
      var li = element.querySelectorAll("li > h3");
      var array = [];
      li.forEach(function (element) {
        array.push(element.offsetHeight);
      });

      var getMaxOfArr = function getMaxOfArr(arr) {
        return Math.max.apply(null, array);
      };

      li.forEach(function (element) {
        element.style.height = "".concat(getMaxOfArr(), "px");
      });
    });
  };

  buttons.forEach(function (element) {
    var button = element.querySelectorAll("button");
    button.forEach(function (element) {
      element.addEventListener("click", function () {
        calculate();
      });
    });
  });
  window.addEventListener("resize", function () {
    heightClear();
    calculate();
  });
};



/***/ })
/******/ ]);