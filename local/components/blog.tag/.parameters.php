<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

if(!CModule::IncludeModule("iblock"))
	return;

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"IBLOCK_TAG_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок с тэгами",
			"TYPE" => "STRING",
			"DEFAULT" => 1,
		),
		"IBLOCK_BLOG_ID" => Array(
			"PARENT" => "BASE",
			"NAME" => "Инфоблок с блогом",
			"TYPE" => "STRING",
			"DEFAULT" => 1,
		),
		"TAG" => Array(
			"PARENT" => "BASE",
			"NAME" => "Код тэга",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"SECTION_CODE" => Array(
			"PARENT" => "BASE",
			"NAME" => "CODE раздела",
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);
?>
