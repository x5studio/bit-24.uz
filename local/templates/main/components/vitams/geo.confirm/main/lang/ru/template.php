<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$MESS+=array(
    'PB_GEO_CT_GC_DEFAULT_JS_TRACK_1' => 'Выбор региона',
    'PB_GEO_CT_GC_DEFAULT_JS_KEYUP_1' => 'Нажатие',
    'PB_GEO_CT_GC_DEFAULT_JS_CHOISE_1' => 'Выбрать регион',
);
$MESS["PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_LOCATION_MSK_FULL"] = "Вы находитесь в <b>Москве</b> или в<br><b>Московской области</b>?";
$MESS["PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_LOCATION_MSK"] = "В Москве";
$MESS["PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_LOCATION_MSK_OBL"] = "В Мос. области";
$MESS["PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_YOUR_LOCATION"] = "Ваш регион определился как:";
$MESS["PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_TRUE"] = "Верно";
$MESS["PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_OR"] = "или";
$MESS["PB_GEO_COMPONENTS_GEO_CONFIRM_TPL_CHOISE_REGION_LOWER"] = "Изменить регион";

