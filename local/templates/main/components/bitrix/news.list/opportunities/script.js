$(document).ready(function () {
    if ($(window).width() > 650) {
        $(".opportunities .tab-item").not(":first-child").hide();
        $(".opportunities .tab:first-child").addClass("active");

        $(".opportunities .tabs-wrapper .tab").click(function() {
            $(this).closest(".tabs-wrapper").find(".tab").removeClass("active").eq($(this).index()).addClass("active");
            $(this).closest(".tabs-wrapper").find(".tab-item").hide().eq($(this).index()).fadeIn();
        }).eq(0).addClass("active");
    } else {
        $(".opportunities .tabs").find(".tab").wrap("<div class='drop-tab'></div>");
        $(".opportunities .tabs-wrapper").find(".tab").removeClass("active");
        $(".opportunities .tab-content").find(".tab-item").hide().each(function(indx, element){
            $(element).appendTo(".drop-tab:eq("+indx+")");
        });
        $(".opportunities .tabs-wrapper .drop-tab").click(function() {
            $(this).closest(".tabs-wrapper").find(".tab-item").slideUp();
            $(this).closest(".tabs-wrapper").find(".tab").removeClass("active");

            if(!$(this).hasClass("expanded")) {
                $(this).addClass("expanded");
                $(this).find(".tab").addClass("active");
                $(this).find(".tab-item").slideDown();
            } else {
                $(this).closest(".tabs-wrapper").find(".drop-tab").removeClass("expanded");
            }
        });
    }
});

