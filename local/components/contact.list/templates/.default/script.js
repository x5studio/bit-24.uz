$(function () {
    var mapYandex,
        arGeoCollection = [],
        maxZoom = 12;

    ymaps.ready(initMap);

    function SetZoom(sectionId) {
        mapYandex.setBounds(arGeoCollection[sectionId].getBounds(), {
            checkZoomRange: true,
            useMapMargin: true
        });
    }

    function setName(sectionId) {
        var $wrap = $(".js-city-name-wrap h2");
        $wrap.text(Contacts.NAME[sectionId]);
    }

    function initMap() {
        mapYandex = new ymaps.Map("js-map-wrap", {
            center: [55.75399399999374,57.62209300000001],
            zoom: maxZoom,
            controls: ['smallMapDefaultSet']
        },{
            maxZoom : maxZoom
        });
        mapYandex.margin.addArea({
            left: 0,
            top: 0,
            width: 150,
            height: 150
        });

        BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div class="ballon-info">' +
            '<h4>{{properties.name}}</h4>' +
            '<p class="ballon-info_address">{{properties.address}}</p>' +
            '<p class="ballon-info_phone"><a href="tel:{{properties.phone}}">{{properties.phone}}</a></p>' +
            '<p class="ballon-info_email"><a href="mailto:{{properties.email}}">{{properties.email}}</a></p>' +
            '</div>'
        );

        for (var sectionId in Contacts.GEO) {
            var geoCollection = new ymaps.GeoObjectCollection();
            for (var elementId in Contacts.GEO[sectionId]) {
                var arItem = Contacts.GEO[sectionId][elementId];
                var Placemark = new ymaps.Placemark(
                    [+arItem.LAT, +arItem.LON],
                    {
                        name: arItem.NAME,
                        address: arItem.ADDRESS,
                        phone: arItem.PHONE,
                        email: arItem.EMAIL
                    },
                    {
                        balloonContentLayout: BalloonContentLayout,
                        balloonPanelMaxMapArea: 0,
                        iconLayout: 'default#image',
                        iconImageHref: '/map-icon.png',
                        iconImageSize: [33, 38],
                        iconImageOffset: [-16, -38]
                    }
                );
                geoCollection.add(Placemark);;
            }
            mapYandex.geoObjects.add(geoCollection);
            arGeoCollection[sectionId] = geoCollection;
        }

        SetZoom(Contacts.CURRENT);

        mapYandex.behaviors.disable('scrollZoom');
    }

    $("html").on("click", ".js-change-city", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        SetZoom(id);
        setName(id)
    });
});